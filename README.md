# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* STM32 family microcontrollers framework
* Version 2.1


### How do I get set up? ###

* Configuration
- place all application dependent sources to src/app/<your_app_name>
- create src/app/<your_app_name>/Makefile.mk (use demo template)
- add <your_app_name> to src/app/Makefile.mk (copy from demo_app part)

* Dependencies:
- gnu make;
- ARM gnu gcc;
- ARM gnu gdb (optional);
- python 2.x (optional);
- st-utils (optional).

* How to compile
- make all
- make debug
- make release
- make debug_myapp
- make release_otherapp
...
* Deployment instructions
- make flash_myapp
- make flash_release_myapp
- make gdb_otherapp

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact