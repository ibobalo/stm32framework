#ifndef __CUSTOM_USB_CONF__H__
#define __CUSTOM_USB_CONF__H__

//define my

#define USE_USB_OTG_FS
#define USE_EMBEDDED_PHY
//#define USE_HOST_MODE
//#define USE_OTG_MODE

//Include original

#include "usb_conf_template.h"

// and redefine custom props

#undef USE_USB_OTG_HS

#ifdef USB_MAX_STR_DESC_SIZ
/* bug in library. better do not use */
# undef USB_MAX_STR_DESC_SIZ
#endif

#endif //__CUSTOM_USB_CONF__H__
