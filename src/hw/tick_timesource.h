#ifndef TICK_TIMESOURCE_H_INCLUDED
#define TICK_TIMESOURCE_H_INCLUDED

#include "mcu/mcu.h"
#include "util/timesource.h"

typedef unsigned int ticktime_t;
typedef int ticktime_delta_t;

typedef TTimeSourceTemplate<ticktime_t, ticktime_delta_t> CTickTimeSource;

template<>
inline TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_t TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetTimeNow() {return CHW_MCU::GetSysTick();};
template<>
inline TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_delta_t TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetDeltaFromNano(uint64_t ullNano) {return ullNano * CHW_MCU::GetSysTickFrequency() / 1000000000ULL;}
template<>
inline TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_delta_t TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetDeltaFromMilli(unsigned uiMilli) {return uiMilli * CHW_MCU::GetSysTickFrequency() / 1000;}
template<>
inline TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_delta_t TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetDeltaFromSeconds(float fSeconds) {return fSeconds * CHW_MCU::GetSysTickFrequency();}
template<>
inline uint64_t TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetNanoFromDelta(TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_delta_t td) {return td * 1000000000ULL / CHW_MCU::GetSysTickFrequency();}
template<>
inline unsigned TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetMilliFromDelta(TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_delta_t td) {return td * 1000 / CHW_MCU::GetSysTickFrequency();}
template<>
inline float TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::GetSecondsFromDelta(TTimeSourceTemplate<ticktime_t, ticktime_delta_t>::time_delta_t td) {return float(td) / CHW_MCU::GetSysTickFrequency();}

#endif // TICK_TIMESOURCE_H_INCLUDED
