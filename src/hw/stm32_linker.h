#ifndef STM32_LINKER_INCLUDED_H
#define STM32_LINKER_INCLUDED_H

#ifdef __cplusplus
extern "C" {
#endif

#define DeclareLinkerSymbol(name) extern int* __##name
#define GetLinkerSymbol(type, name) ((type)(&__##name))

DeclareLinkerSymbol(estack); /* end of RAM on AHB bus */
DeclareLinkerSymbol(sidata); /* start address for the initialization values of the .data section. defined in linker script */
DeclareLinkerSymbol(sdata);  /* start address for the .data section. defined in linker script */
DeclareLinkerSymbol(edata);  /* end address for the .data section. defined in linker script */
DeclareLinkerSymbol(sbss);   /* start address for the .bss section. defined in linker script */
DeclareLinkerSymbol(ebss);   /* end address for the .bss section. defined in linker script */
DeclareLinkerSymbol(init_array_start);
DeclareLinkerSymbol(init_array_end);

DeclareLinkerSymbol(ro_data_start);
DeclareLinkerSymbol(ro_data_end);

DeclareLinkerSymbol(isr_vector_start);
DeclareLinkerSymbol(isr_vector_end);

DeclareLinkerSymbol(flash_start);

DeclareLinkerSymbol(ram_start);
DeclareLinkerSymbol(ram_size);
DeclareLinkerSymbol(ram_isr_vector_start);
DeclareLinkerSymbol(ram_isr_vector_end);

DeclareLinkerSymbol(firmware_start);
DeclareLinkerSymbol(firmware_size);
DeclareLinkerSymbol(reserved_start);
DeclareLinkerSymbol(reserved_size);

#ifdef USE_PARAMS
DeclareLinkerSymbol(eeprom_data_start);
DeclareLinkerSymbol(eeprom_data_size);
#endif


constexpr unsigned AddrToPageNumber(unsigned uiAddr) // The page is the granularity of the erase operation
{
#if defined(STM32F4)
	return (
			(uiAddr < 0x10000) ? (uiAddr >> 14)      : // 4x16K pages
			(uiAddr < 0x20000) ? (4)                 : // 1x64K pages
					(5 + ((uiAddr - 0x20000) >> 17))   // Nx128K pages
	);
#else
	return (uiAddr > 10) / FLASH_PAGE;
#endif
}
constexpr unsigned PtrToPageNumber(const void* pAddr) // The page is the granularity of the erase operation
{
	return AddrToPageNumber((unsigned)pAddr - 0x08000000);
}

inline unsigned PageNumberToSectorNumber(unsigned uiPageNumber) // The sector is the granularity of the write protection
{
#if defined(STM32F4)
	return uiPageNumber;
#else
	return uiPageNumber * FLASH_PAGE / 4; // 4K per sector
#endif
}

#ifdef __cplusplus
}
#endif

#endif // STM32_LINKER_INCLUDED_H
