#include "dev_mcp6s91.h"

#include "stdafx.h"
#include "hw/hw.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include "hw/mcu/hw_spi.h"
#include "hw/tick_timeout.h"
#include "util/macros.h"
#include "util/utils.h"
#include "tick_process.h"
#include <stddef.h>
#include <string.h>

#ifndef MCP6S91_COUNT
# define MCP6S91_COUNT 1
#endif

#define MCP_CMD_SHUTDOWN 0x20
#define MCP_CMD_WRITE_GAIN 0x40
#define MCP_CMD_WRITE_CHANNEL 0x41

//#define MCP6S91_DEBUG_INFO(...) DEBUG_INFO("MCP:" __VA_ARGS__)
#define MCP6S91_DEBUG_INFO(...) {}

/*static*/
unsigned IMCP6S91::GetGainMultiplier(EGain eGain)
{
	static const unsigned auiMultipliers[] = {1,2,4,5,8,10,16,32};
	return auiMultipliers[eGain];
}

/*static*/
unsigned IMCP6S91::GetGainShift(EGain eGain)
{
	static const unsigned auiShifts[] = {0,1,2,0,3,0,4,5};
	return auiShifts[eGain];
}

/*static*/
bool IMCP6S91::MultiplierToGain(unsigned uiMultiplier, EGain& eRetGain)
{
	switch (uiMultiplier) {
	case 1:  eRetGain = EGain::GAIN_1;  return true;
	case 2:  eRetGain = EGain::GAIN_2;  return true;
	case 4:  eRetGain = EGain::GAIN_4;  return true;
	case 5:  eRetGain = EGain::GAIN_5;  return true;
	case 8:  eRetGain = EGain::GAIN_8;  return true;
	case 10: eRetGain = EGain::GAIN_10; return true;
	case 16: eRetGain = EGain::GAIN_16; return true;
	case 32: eRetGain = EGain::GAIN_32; return true;
	}
	return false;
}


class CSPIDeviceMCP6S91 :
		public IMCP6S91,
		public IDeviceSPI,
		public CTickProcess,
		private TValueNotifier<bool>,
		private TValueNotifier<IMCP6S91::EChannel>,
		private TValueNotifier<IMCP6S91::EGain>
{
public:
	typedef CTickProcess CParentProcess;
public:
//	virtual ~CSPIDeviceMCP6S91();

public: // IMCP6S91
	virtual void Init(ISPI* pSPI, IDigitalOut* pSelect);
	virtual void Finit();

	virtual void SetShutdownDoneNotificationTarget(IShutdownDoneEventNotificationTarget* pTarget);
	virtual void SetChannelDoneNotificationTarget(IChannelDoneEventNotificationTarget* pTarget);
	virtual void SetGainDoneNotificationTarget(IGainDoneEventNotificationTarget* pTarget);

	virtual void SetShutdown(bool bShutdown = true);
	virtual void SetInputChannel(EChannel eChannel);
	virtual void SetGain(EGain eGain);

public: // IDeviceSPI
	virtual bool GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const;
	virtual void Select();
	virtual void Release();
	virtual void OnSPIDone();
	virtual void OnSPIError();
	virtual void OnSPIIdle();

public: // CTickProcess
public: // TProcess<CTickTimeSource>
	virtual void SingleStep(CTimeType uiTime);

private:
	void Clear();
	bool SendSPICommand(uint8_t cmd, uint8_t data);
	void ContinueOnSPIIdle();

	ISPI*          m_pSPI;
	IDigitalOut*   m_pSelect;
	EChannel       m_eChannel;
	EGain          m_eGain;
	EOpDoneType    m_eCurrentOp;
	EChannel       m_eChannelSent;
	EGain          m_eGainSent;
	bool           m_bShutdown;
	bool           m_bBusy;
	bool           m_bError;
	bool           m_bShutdownSend;
	bool           m_bWakeupSend;
	bool           m_bChannelSelected;
	bool           m_bGainSet;
	uint8_t        m_SpiTxCmd[2];
	CChainedConstChunks m_TxChunk;
};

void CSPIDeviceMCP6S91::Init(ISPI* pSPI, IDigitalOut* pSelect)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::Init(pSPI, pSelect));
	CParentProcess::Init(&g_TickProcessScheduller);
	TValueNotifier<bool>::Init();
	TValueNotifier<IMCP6S91::EChannel>::Init();
	TValueNotifier<IMCP6S91::EGain>::Init();
	m_pSPI = pSPI;
	m_pSelect = pSelect;
	Clear();
	ContinueNow();
}

void CSPIDeviceMCP6S91::Finit()
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::Finit());
	Cancel();
	m_pSelect = NULL;
}

/*virtual*/
void CSPIDeviceMCP6S91::SetShutdownDoneNotificationTarget(IShutdownDoneEventNotificationTarget* pTarget)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::SetShutdownDoneNotificationTarget(pTarget));
	TValueNotifier<bool>::SetNotificationTarget(pTarget, NULL);
}
void CSPIDeviceMCP6S91::SetChannelDoneNotificationTarget(IChannelDoneEventNotificationTarget* pTarget)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::SetChannelDoneNotificationTarget(pTarget));
	TValueNotifier<IMCP6S91::EChannel>::SetNotificationTarget(pTarget, NULL);

}
void CSPIDeviceMCP6S91::SetGainDoneNotificationTarget(IGainDoneEventNotificationTarget* pTarget)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::SetGainDoneNotificationTarget(pTarget));
	TValueNotifier<IMCP6S91::EGain>::SetNotificationTarget(pTarget, NULL);
}

/*virtual*/
void CSPIDeviceMCP6S91::SetShutdown(bool bShutdown /*=true*/)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::SetShutdown(bShutdown));
	if (m_bShutdown != bShutdown) {
		m_bShutdown = bShutdown;
		if (bShutdown) {
			m_bShutdownSend = false;
			MCP6S91_DEBUG_INFO("Going to shutdown");
		} else {
			m_bWakeupSend = false;
			MCP6S91_DEBUG_INFO("Going to wakeup");
		}
		ContinueAsap();
	}
}

/*virtual*/
void CSPIDeviceMCP6S91::SetInputChannel(EChannel eChannel)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::SetInputChannel(eChannel));
	if (m_eChannel != eChannel) {
		m_eChannel = eChannel;
		m_bChannelSelected = false;
		ContinueAsap();
		MCP6S91_DEBUG_INFO("Channel to change, channel:", m_eChannel);
	}
}

/*virtual*/
void CSPIDeviceMCP6S91::SetGain(EGain eGain)
{
	IMPLEMENTS_INTERFACE_METHOD(IMCP6S91::SetGain(eGain));
	if (m_eGain != eGain) {
		m_eGain = eGain;
		m_bGainSet = false;
		ContinueAsap();
		MCP6S91_DEBUG_INFO("Gain to change, gain:", m_eGain);
	}
}

void CSPIDeviceMCP6S91::Clear()
{
	m_eChannel = CHANNEL_1;
	m_eGain = GAIN_1;
	m_bShutdown = false;
	m_bBusy = false;
	m_bError = false;
	m_bShutdownSend = false;
	m_bWakeupSend = false;
	m_bChannelSelected = false;
	m_bGainSet = false;
	m_TxChunk.Assign(m_SpiTxCmd, 2);
}

/*virtual*/
void CSPIDeviceMCP6S91::SingleStep(CTimeType uiTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(uiTime));
	if (!m_bBusy) {
		if (m_bError) {
			m_bShutdownSend = false;
			m_bWakeupSend = false;
			m_bChannelSelected = false;
			m_bGainSet = false;
			m_bError = false;
			MCP6S91_DEBUG_INFO("Recovering");
		}
		if (!m_bShutdown) {
			if (!m_bWakeupSend) {
				MCP6S91_DEBUG_INFO("Sending wakeup, gain:", m_eGain);
				m_eCurrentOp = OP_WAKEUP;
				m_eGainSent = m_eGain;
				m_bWakeupSend = m_bGainSet = SendSPICommand(MCP_CMD_WRITE_GAIN, (uint8_t)m_eGainSent);
			} else if (!m_bChannelSelected) {
				MCP6S91_DEBUG_INFO("Sending channel:", m_eChannel);
				m_eCurrentOp = OP_CHANNEL;
				m_eChannelSent = m_eChannel;
				m_bWakeupSend = m_bChannelSelected = SendSPICommand(MCP_CMD_WRITE_CHANNEL, (uint8_t)m_eChannelSent);
			} else if (!m_bGainSet) {
				MCP6S91_DEBUG_INFO("Sending gain:", m_eGain);
				m_eCurrentOp = OP_GAIN;
				m_eGainSent = m_eGain;
				m_bWakeupSend = m_bGainSet = SendSPICommand(MCP_CMD_WRITE_GAIN, (uint8_t)m_eGainSent);
			}
		} else {
			if (!m_bShutdownSend) {
				MCP6S91_DEBUG_INFO("Sending shutdown");
				m_eCurrentOp = OP_SHUTDOWN;
				m_bShutdownSend = SendSPICommand(MCP_CMD_SHUTDOWN, 0);
			}
		}
	} else {
		MCP6S91_DEBUG_INFO("Busy");
	}
}

/*virtual*/
bool CSPIDeviceMCP6S91::GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::GetSPIParams(uiMaximumBitrate, eMode, eBitsOrder, nBitsCount));

	bool bResult = false;
	if (uiMaximumBitrate > 1000000) {
		uiMaximumBitrate = 1000000;
		bResult = true;
	}
	if (eMode != ISPI::MODE_1_1) {
		eMode = ISPI::MODE_1_1;
		bResult = true;
	}
	if (eBitsOrder != ISPI::MSB_1st) {
		eBitsOrder = ISPI::MSB_1st;
		bResult = true;
	}
	if (nBitsCount != 8) {
		nBitsCount = 8;
		bResult = true;
	}
	return bResult;
}

/*virtual*/
void CSPIDeviceMCP6S91::Select()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Select());
	if (m_pSelect) {
		m_pSelect->Reset();
		MCP6S91_DEBUG_INFO("SPI Selected");
	}
}

/*virtual*/
void CSPIDeviceMCP6S91::Release()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Release());
	if (m_pSelect) {
		m_pSelect->Set();
		MCP6S91_DEBUG_INFO("SPI Released");
	}
}

/*virtual*/
void CSPIDeviceMCP6S91::OnSPIDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIDone());
	m_bBusy = false;
	MCP6S91_DEBUG_INFO("SPI done, op:", m_eCurrentOp);
	switch (m_eCurrentOp) {
	case OP_WAKEUP:
		TValueNotifier<bool>::NotifyValue(false);
		TValueNotifier<IMCP6S91::EGain>::NotifyValue(m_eGainSent);
		break;
	case OP_SHUTDOWN:
		TValueNotifier<bool>::NotifyValue(true);
		break;
	case OP_CHANNEL:
		TValueNotifier<IMCP6S91::EChannel>::NotifyValue(m_eChannelSent);
		break;
	case OP_GAIN:
		TValueNotifier<IMCP6S91::EGain>::NotifyValue(m_eGainSent);
		break;
	}
	ContinueNow();
}

/*virtual*/
void CSPIDeviceMCP6S91::OnSPIError()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIError());
	m_bError = true;
	m_bBusy = false;
	ContinueNow();
}

/*virtual*/
void CSPIDeviceMCP6S91::OnSPIIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIIdle());
	ContinueNow();
}

void CSPIDeviceMCP6S91::ContinueOnSPIIdle()
{
	MCP6S91_DEBUG_INFO("Scheduling callback");
	m_pSPI->RequestSPIIdleNotify(this);
}

bool CSPIDeviceMCP6S91::SendSPICommand(uint8_t cmd, uint8_t data)
{
	bool bResult;
	m_bBusy = true;
	m_SpiTxCmd[0] = cmd;
	m_SpiTxCmd[1] = data;
	if (m_pSPI->Start_SPIx_Session(this, &m_TxChunk, NULL)) {
		MCP6S91_DEBUG_INFO("SPI op started:", m_SpiTxCmd[0]);
		bResult = true;
	} else {
		m_bBusy = false;
		MCP6S91_DEBUG_INFO("SPI op postponed");
		ContinueOnSPIIdle();
		bResult = false;
	}
	return bResult;
}

CSPIDeviceMCP6S91 g_MCP6S91[MCP6S91_COUNT];

IMCP6S91* GetMCP6S91(unsigned uiIndex)
{
	ASSERTE(uiIndex < ARRAY_SIZE(g_MCP6S91));
	return &g_MCP6S91[uiIndex];
}
