#ifndef DEV_MCP6S91_H_INCLUDED
#define DEV_MCP6S91_H_INCLUDED

#include "interfaces/common.h"
#include "interfaces/gpio.h"
#include "interfaces/notify.h"

//extern
class ISPI;
class IDigitalOut;

class IMCP6S91
{
public:
	typedef enum {
		CHANNEL_1,
		CHANNEL_2,
		CHANNEL_3,
		CHANNEL_4,
	} EChannel;
	typedef enum {
		GAIN_1,
		GAIN_2,
		GAIN_4,
		GAIN_5,
		GAIN_8,
		GAIN_10,
		GAIN_16,
		GAIN_32
	} EGain;
	typedef enum {
		OP_SHUTDOWN,
		OP_WAKEUP,
		OP_CHANNEL,
		OP_GAIN,
	} EOpDoneType;
	typedef IValueNotificationTarget<bool> IShutdownDoneEventNotificationTarget;
	typedef IValueNotificationTarget<EChannel> IChannelDoneEventNotificationTarget;
	typedef IValueNotificationTarget<EGain> IGainDoneEventNotificationTarget;

	static unsigned GetGainMultiplier(EGain eGain);
	static unsigned GetGainShift(EGain eGain);
	static bool MultiplierToGain(unsigned uiMultiplier, EGain& eRetGain);

public: // IMCP6S91
	virtual void Init(ISPI* pSPI, IDigitalOut* pSelect) = 0;
	virtual void Finit() = 0;

	virtual void SetShutdownDoneNotificationTarget(IShutdownDoneEventNotificationTarget* pTarget) = 0;
	virtual void SetChannelDoneNotificationTarget(IChannelDoneEventNotificationTarget* pTarget) = 0;
	virtual void SetGainDoneNotificationTarget(IGainDoneEventNotificationTarget* pTarget) = 0;

	virtual void SetShutdown(bool bShutdown = true) = 0;
	virtual void SetInputChannel(EChannel eChannel) = 0;
	virtual void SetGain(EGain eGain) = 0;
};

IMCP6S91* GetMCP6S91(unsigned uiIndex = 0);

#endif //DEV_MCP6S91_H_INCLUDED
