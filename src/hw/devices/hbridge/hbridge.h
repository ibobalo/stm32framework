#ifndef HBRIDGE_H_INCLUDED
#define HBRIDGE_H_INCLUDED

#include "interfaces/hbridge.h"
#include "interfaces/timer.h"

class CHBridge:
	public IHBridge
{
public:
	void Init(ITimer* pTimerA, ITimerChannel* pPWMA, ITimer* pTimerB, ITimerChannel* pPWMB, unsigned uiFreq, bool bInverted);

public: // IValueReceiver
	virtual void SetValue(float tValue);

private:
	ITimerChannel* m_pPWMA;
	ITimerChannel* m_pPWMB;
	float          m_dScaleA;
	float          m_dScaleB;
};

#endif // HBRIDGE_H_INCLUDED
