#include "stdafx.h"
#include "hbridge.h"
#include "util/core.h"
#include "util/macros.h"
#include "hw/mcu/mcu.h"

void CHBridge::Init(ITimer* pTimerA, ITimerChannel* pPWMA, ITimer* pTimerB, ITimerChannel* pPWMB, unsigned uiFreq, bool bInverted)
{
	ASSERTE(pTimerA);
	ASSERTE(pPWMA);
	ASSERTE(pPWMB);
	uint64_t ullTimerPeriodClks = CHW_MCU::GetCLKFrequency() / uiFreq;
	pTimerA->SetPeriodClks(ullTimerPeriodClks);
	m_dScaleA = 1.0 / pTimerA->GetCounterMax();
	if (!pTimerB || pTimerB == pTimerA) {
		m_dScaleB = m_dScaleA;
	} else {
		pTimerB->SetPeriodClks(ullTimerPeriodClks);
	}
	m_pPWMA = pPWMA;
	m_pPWMB = pPWMB;
}

/*virtual*/
void CHBridge::SetValue(float tValue)
{
	if (tValue > 0.0) {
		m_pPWMA->SetValue(tValue * m_dScaleA);
		m_pPWMB->SetValue(0);
	} else if (tValue == 0.0) {
		m_pPWMA->SetValue(0);
		m_pPWMB->SetValue(0);
	} else {
		m_pPWMA->SetValue(0);
		m_pPWMB->SetValue(tValue * m_dScaleB);
	}
}
