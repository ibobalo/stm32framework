#include "stdafx.h"
#include "dev_hmc5883.h"
#include "hw/debug.h"
#include "hw/mcu/hw_i2c.h"
#include "hw/tick_timeout.h"
#include "util/macros.h"
#include "util/endians.h"
#include "tick_process.h"

#define INITIAL_DELAY             10
#define REPEAT_AFTER_ERROR_DELAY  3
#define REPEAT_AFTER_MISSED_DELAY 1000

// I2C Address is binary [110100Xw] X=AD0 w=1-read/0-write
#define HMC5883_ADDRESS    (0x3C >> 1) // I2C Bus Address
#define HMC5883_RA_CFG_A   0x00
#define HMC5883_RA_CFG_B   0x01
#define HMC5883_RA_MODE    0x02
#define HMC5883_RA_X_H     0x03
#define HMC5883_RA_X_L     0x04
#define HMC5883_RA_Z_H     0x05
#define HMC5883_RA_Z_L     0x06
#define HMC5883_RA_Y_H     0x07
#define HMC5883_RA_Y_L     0x08
#define HMC5883_RA_STATUS  0x09
#define HMC5883_RA_ID_A    0x0A
#define HMC5883_RA_ID_B    0x0B
#define HMC5883_RA_ID_C    0x0C

#define HMC5883_CFGA_MS_NORMAL   0x00
#define HMC5883_CFGA_MS_PBIAS    0x01
#define HMC5883_CFGA_MS_NBIAS    0x02

#define HMC5883_CFGA_RATE(rate)  (((uint8_t)(rate) & 0x07) << 2)
#define HMC5883_CFGA_AVG(avg)    (((uint8_t)(avg) & 0x03) << 5)
#define HMC5883_CFGB_GAIN(gain)  (((uint8_t)(gain) & 0x07) << 5)

#define HMC5883_MODE_CONTINUOUS  0x00
#define HMC5883_MODE_SINGLE      0x01
#define HMC5883_MODE_IDLE        0x03
#define HMC5883_MODE_HS_I2C      0x08

#define HMC5883_ID_A 'H'
#define HMC5883_ID_B '4'
#define HMC5883_ID_C '3'


#define HMC5883_DEBUG_INFO(...) DEBUG_INFO("MAG:" __VA_ARGS__)
//#define HMC5883_DEBUG_INFO(...) {}


class CI2CDeviceHMC5883 :
		public IDeviceHMC5883,
		public IDeviceI2C,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
	typedef IDeviceHMC5883::time_delta_t time_delta_t;
public:
//	virtual ~CI2CDeviceHMC5883();

	void Init(II2C* pI2C, MeasurementRate_t eMeasurementRate, Averaging_t eAvg, Gain_t eGain, time_delta_t tdRefreshPeriod);

public: // IDeviceHMC5883
	virtual const magnetic_3D_t& GetMagnetic() const;
	virtual bool SetMagneticCallback(MagneticCallback cb);

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

private:
	void ProcessInit();

private:
	II2C*                 m_pI2C;
	uint8_t               m_uiAddress;
	bool                  m_bCommOk;
	magnetic_3D_t         m_Magnetic;
	MagneticCallback      m_cbMagneticCallback;

	enum {
		OpIsInit_Start,
		OpIsInit_CHECK = OpIsInit_Start,
		OpIsInit_CFG_A,
		OpIsInit_CFG_B,
		OpIsInit_MODE,
		OpIsRead,
		OpIsNone,
	}         m_eCurrentOp;
	bool      m_bReinitRequired;
	bool      m_bInputReadRequired;
	unsigned  m_uiLastInputReadTime;

	MeasurementRate_t m_eMeasurementRate;
	Averaging_t       m_eAvg;
	Gain_t            m_eGain;
	CTimeout          m_toRefreshTimeout;
	bool              m_bSingleMeasureMode;
	bool              m_bHighSpeedI2C;

	uint8_t   m_uiRegAddressToRead;
	uint8_t   m_auiTempReadRegData[7];
	struct {
		uint8_t uiReg;
		uint8_t uiValue;
		void Assign(uint8_t _uiReg, uint8_t _uiValue) {uiReg = _uiReg; uiValue = _uiValue;};
	} m_tWriteRegPkt;

	CChainedConstChunks m_cccRegAddressToReadChunks;
	CChainedIoChunks    m_cicRegValueChunks;
	CChainedConstChunks m_cccWriteRegPktChunks;
};


void CI2CDeviceHMC5883::Init(II2C* pI2C, MeasurementRate_t eMeasurementRate, Averaging_t eAvg, Gain_t eGain, time_delta_t tdRefreshPeriod)
{
	CParentProcess::Init(&g_TickProcessScheduller);

	m_pI2C = pI2C;
	m_uiAddress = HMC5883_ADDRESS;
	m_bCommOk = false;
	m_eCurrentOp = OpIsInit_Start;
	m_bReinitRequired = true;
	m_bInputReadRequired = true;
	m_uiLastInputReadTime = 0;
	m_eMeasurementRate = eMeasurementRate;
	m_eAvg = eAvg;
	m_eGain = eGain;
	m_toRefreshTimeout.Init(tdRefreshPeriod);
	m_cbMagneticCallback = NullCallback();
	m_cccRegAddressToReadChunks.Assign((const uint8_t*)&m_uiRegAddressToRead, sizeof(m_uiRegAddressToRead));
	m_cccRegAddressToReadChunks.Break();
	m_cicRegValueChunks.Assign((uint8_t*)m_auiTempReadRegData, 1);
	m_cicRegValueChunks.Break();
	m_cccWriteRegPktChunks.Assign((uint8_t*)&m_tWriteRegPkt, sizeof(m_tWriteRegPkt));
	m_cccWriteRegPktChunks.Break();

	if (m_toRefreshTimeout.GetIsDefined()) {
		m_toRefreshTimeout.Start();
		ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
	}

	m_pI2C->Queue_I2C_Session(this);
}

const CI2CDeviceHMC5883::magnetic_3D_t& CI2CDeviceHMC5883::GetMagnetic() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceHMC5883::GetMagnetic());
	return m_Magnetic;
}
bool CI2CDeviceHMC5883::SetMagneticCallback(MagneticCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceHMC5883::SetMagneticCallback(cb));
	if (!m_cbMagneticCallback) {
		m_cbMagneticCallback = cb;
		return true;
	}
	return false;
}

void CI2CDeviceHMC5883::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CI2CDeviceHMC5883");

	if (m_eCurrentOp == OpIsNone) {
		if (m_toRefreshTimeout.GetIsDefined() && m_toRefreshTimeout.GetIsElapsed()) {
			m_bInputReadRequired = true;
		}

		if (m_bReinitRequired) {
			m_eCurrentOp = OpIsInit_Start;
			m_pI2C->Queue_I2C_Session(this);
		} else if (m_bInputReadRequired) {
			m_eCurrentOp = OpIsRead;
			m_pI2C->Queue_I2C_Session(this);
		} else if (m_toRefreshTimeout.GetIsDefined()) {
			ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
		}
	}
	PROCESS_DEBUG_INFO("<<< Process: CI2CDeviceHMC5883");
}

unsigned CI2CDeviceHMC5883::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_uiAddress;
}

bool CI2CDeviceHMC5883::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));

#define REG_READ(reg, ptr, size) { \
		m_uiRegAddressToRead = reg; \
		*ppRetChunksToSend = &m_cccRegAddressToReadChunks; \
		m_cicRegValueChunks.Assign((uint8_t*)ptr, size); \
		*ppRetChunksToRecv = &m_cicRegValueChunks; \
}
#define REG_WRITE(reg, val) { \
		m_tWriteRegPkt.Assign(reg, val); \
		*ppRetChunksToSend = &m_cccWriteRegPktChunks; \
		*ppRetChunksToRecv = NULL; \
}
	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		return false;
		break;
	case OpIsInit_CHECK:
		ASSERTE(m_bReinitRequired);
		REG_READ(HMC5883_RA_ID_A, m_auiTempReadRegData, 3);
		break;
	case OpIsInit_CFG_A:
		ASSERTE(m_bReinitRequired);
		REG_WRITE(HMC5883_RA_CFG_A, HMC5883_CFGA_MS_NORMAL | HMC5883_CFGA_RATE(m_eMeasurementRate) | HMC5883_CFGA_AVG(m_eAvg));
		break;
	case OpIsInit_CFG_B:
		ASSERTE(m_bReinitRequired);
		REG_WRITE(HMC5883_RA_CFG_B, HMC5883_CFGB_GAIN((unsigned)m_eGain));
		break;
	case OpIsInit_MODE:
		ASSERTE(m_bReinitRequired);
		REG_WRITE(HMC5883_RA_MODE, (m_bSingleMeasureMode?HMC5883_MODE_SINGLE:HMC5883_MODE_CONTINUOUS) | (m_bHighSpeedI2C?HMC5883_MODE_HS_I2C:0x00));
		break;
	case OpIsRead:
		ASSERTE(m_bInputReadRequired);
		REG_READ(HMC5883_RA_X_H, m_auiTempReadRegData, 7);
		m_toRefreshTimeout.Start();
		break;
	}
	return true;
}

void CI2CDeviceHMC5883::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());
	switch (m_eCurrentOp) {
		case OpIsNone: {
			ASSERT("invalid op");
		} break;
		case OpIsInit_CHECK: {
			const char* pID = (const char*) m_auiTempReadRegData;
			if (pID[0] == HMC5883_ID_A && pID[1] == HMC5883_ID_B && pID[2] == HMC5883_ID_C) {
				DEBUG_INFO("HMC5883 detected. Addr:", m_uiAddress);
				m_eCurrentOp = OpIsInit_CFG_A;
				m_pI2C->Queue_I2C_Session(this);
			} else {
				DEBUG_INFO("HMC5883 missed. Addr:", m_uiAddress);
				m_bCommOk = false;
				ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
				return;
			}
		} break;
		case OpIsInit_CFG_A: {
			m_eCurrentOp = OpIsInit_CFG_B;
			m_pI2C->Queue_I2C_Session(this);
		} break;
		case OpIsInit_CFG_B: {
			m_eCurrentOp = OpIsInit_MODE;
			m_pI2C->Queue_I2C_Session(this);
		} break;
		case OpIsInit_MODE: {
			m_bCommOk = true;
			m_bReinitRequired = false;
			m_eCurrentOp = OpIsNone;
			ContinueNow();
		} break;
		case OpIsRead: {
			const magnetic_3D_t* pData = (const magnetic_3D_t*) m_auiTempReadRegData;
			//const uint8_t* pStatus = (const uint8_t*) (pData + 1);
			m_eCurrentOp = OpIsNone;
			m_bInputReadRequired = false;
			m_toRefreshTimeout.Start();
			m_Magnetic.iX  = ntohs(pData->iX);
			m_Magnetic.iZ  = ntohs(pData->iZ);
			m_Magnetic.iY  = ntohs(pData->iY);
			if (m_cbMagneticCallback) m_cbMagneticCallback(m_Magnetic);
		} break;
	}
	if (m_bCommOk && (m_bInputReadRequired || m_bReinitRequired)) {
		ContinueNow();
	} else if (m_toRefreshTimeout.GetIsDefined()) {
		ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
	}
}

void CI2CDeviceHMC5883::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit_CHECK:
	case OpIsInit_CFG_A:
	case OpIsInit_CFG_B:
	case OpIsInit_MODE:
		m_bReinitRequired = true;
		break;
	case OpIsRead:
		m_bInputReadRequired = true;
		break;
	}

	if (e == I2C_ERROR_ACK || m_eCurrentOp == OpIsInit_CHECK) {
		DEBUG_INFO("HMC5883 missed. Addr:", m_uiAddress);
		m_bCommOk = false;
		ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
	} else {
		DEBUG_INFO("HMC5883 error");
		ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
	}

	m_eCurrentOp = OpIsNone;
}

void CI2CDeviceHMC5883::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
	ContinueNow();
}

CI2CDeviceHMC5883       g_HMC5883;

extern IDeviceHMC5883* AcquireHMC5883CompassInterface(
		II2C* pI2C,
		IDeviceHMC5883::MeasurementRate_t eRate = IDeviceHMC5883::RATE_75_0_Hz,
		IDeviceHMC5883::Averaging_t eAvg = IDeviceHMC5883::AVERAGE_8_SAMPLES,
		IDeviceHMC5883::Gain_t eGain = IDeviceHMC5883::GAIN_230_per_GS,
		IDeviceHMC5883::time_delta_t tdRefreshPeriod = 100
) {
	g_HMC5883.Init(pI2C, eRate, eAvg, eGain, tdRefreshPeriod);
	return &g_HMC5883;
}
