USE_I2C += $(if $(filter YES,$(USE_HMC5883)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_HMC5883)), $(addprefix src/hw/devices/hmc5883/, \
	dev_hmc5883.cpp \
))
