#ifndef DEV_HMC5883_H_INCLUDED
#define DEV_HMC5883_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include <stdint.h>

class IDeviceHMC5883
{
public:
	typedef CTickTimeSource::time_delta_t time_delta_t;
	typedef struct magnetic_3D { int16_t iX; int16_t iZ; int16_t iY; } magnetic_3D_t;
	typedef Callback<void (const magnetic_3D_t&)> MagneticCallback;
	typedef enum {
		RATE_0_75_Hz,
		RATE_1_5_Hz,
		RATE_3_0_Hz,
		RATE_7_5_Hz,
		RATE_15_0_Hz,
		RATE_30_0_Hz,
		RATE_75_0_Hz,
	} MeasurementRate_t;
	typedef enum {
		AVERAGE_NONE,
		AVERAGE_2_SAMPLES,
		AVERAGE_4_SAMPLES,
		AVERAGE_8_SAMPLES,
	} Averaging_t;
	typedef enum {
		GAIN_1370_per_GS,
		GAIN_1090_per_GS,
		GAIN_820_per_GS,
		GAIN_660_per_GS,
		GAIN_440_per_GS,
		GAIN_390_per_GS,
		GAIN_330_per_GS,
		GAIN_230_per_GS,
	} Gain_t;

public:
//	virtual ~IDeviceHMC5883();

public:
	virtual const magnetic_3D_t& GetMagnetic() const = 0;
	virtual bool SetMagneticCallback(MagneticCallback cb) = 0;
};

extern IDeviceHMC5883* AcquireHMC5883CompassInterface(
		II2C* pI2C,
		IDeviceHMC5883::MeasurementRate_t eRate,
		IDeviceHMC5883::Averaging_t eAvg,
		IDeviceHMC5883::Gain_t eGain,
		IDeviceHMC5883::time_delta_t tdRefreshPeriod
);

#endif //DEV_HMC5883_H_INCLUDED
