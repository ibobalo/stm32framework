#ifndef DEV_MPU6050_H_INCLUDED
#define DEV_MPU6050_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include <stdint.h>

class IDeviceMPU6050
{
public:
	typedef CTickTimeSource::time_delta_t time_delta_t;
	typedef enum {READ_GYRO_ACCE_TEMP, READ_GYRO, READ_ACCE, READ_TEMP, READ_GYRO_ACCE, READ_GYRO_ACCE_TEMP_EXTERNAL,} read_dataset_t;
	typedef struct angular_rates_3D { int16_t iX; int16_t iY; int16_t iZ; } angular_rates_3D_t;
	typedef Callback<void (const angular_rates_3D_t&)> AngularRatesCallback;
	typedef enum {ANGULAR_MAXRATE_250, ANGULAR_MAXRATE_500, ANGULAR_MAXRATE_1000, ANGULAR_MAXRATE_2000} angular_maxrate_t;
	typedef struct linear_rates_3D { int16_t iX; int16_t iY; int16_t iZ; } linear_rates_3D_t;
	typedef Callback<void (const linear_rates_3D_t&)> LinearRatesCallback;
	typedef enum {LINEAR_MAXRATE_2G, LINEAR_MAXRATE_4G, LINEAR_MAXRATE_8G, LINEAR_MAXRATE_16G} linear_maxrate_t;
	typedef Callback<void (int)> TemperatureCallback;
	typedef Callback<void (const uint8_t[])> ExtSensorsCallback;
public:
//	virtual ~IDeviceMPU6050();

	virtual void Init(
			II2C* pI2C,
			read_dataset_t eReadDataset,
			angular_maxrate_t eAngularMaxRate, unsigned uiGyroLoPassFilter,
			linear_maxrate_t eLinearMaxRate,
			time_delta_t tdRefreshPeriod,
			bool bAlternateAddress
	) = 0;
	virtual void InitExtSensor(unsigned uiBusAddress, unsigned uiRegAddress, unsigned uiSize, ExtSensorsCallback cb) = 0;

public: // angular rates (gyroscope)
	virtual const angular_rates_3D_t& GetAngularRates() const = 0;
	virtual bool SetAngularRatesCallback(AngularRatesCallback cb) = 0;
public: // linear rates (accelerometer)
	virtual const linear_rates_3D_t& GetLinearRates() const = 0;
	virtual bool SetLinearRatesCallback(LinearRatesCallback cb) = 0;
public: // temperature
	virtual unsigned GetTemperature() const = 0;
	virtual bool SetTemperatureCallback(TemperatureCallback cb) = 0;
};

IDeviceMPU6050* GetMPU6050Interface();

#endif //DEV_MPU6050_H_INCLUDED
