#include "stdafx.h"
#include "dev_mpu6050.h"
#include "hw/debug.h"
#include "hw/mcu/hw_i2c.h"
#include "tick_process.h"
#include "util/macros.h"
#include "util/endians.h"
#include "tick_timeout.h"

#define INITIAL_DELAY             10
#define REPEAT_AFTER_ERROR_DELAY  3
#define REPEAT_AFTER_MISSED_DELAY 1000

// I2C Address is binary [110100Xw] X=AD0 w=1-read/0-write
#define MPU6050_ADDRESS        0x68 // Address default (AD0 - low)
#define MPU6050_ADDRESS_ALT    0x69 // Address alternate (AD0 - high)
#define MPU6050_RA_XG_OFFS_TC  0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_RA_YG_OFFS_TC  0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_RA_ZG_OFFS_TC  0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_RA_X_FINE_GAIN 0x03 //[7:0] X_FINE_GAIN
#define MPU6050_RA_Y_FINE_GAIN 0x04 //[7:0] Y_FINE_GAIN
#define MPU6050_RA_Z_FINE_GAIN 0x05 //[7:0] Z_FINE_GAIN
#define MPU6050_RA_XA_OFFS_H 0x06 //[15:0] XA_OFFS
#define MPU6050_RA_XA_OFFS_L_TC 0x07
#define MPU6050_RA_YA_OFFS_H 0x08 //[15:0] YA_OFFS
#define MPU6050_RA_YA_OFFS_L_TC 0x09
#define MPU6050_RA_ZA_OFFS_H 0x0A //[15:0] ZA_OFFS
#define MPU6050_RA_ZA_OFFS_L_TC 0x0B
#define MPU6050_RA_XG_OFFS_USRH 0x13 //[15:0] XG_OFFS_USR
#define MPU6050_RA_XG_OFFS_USRL 0x14
#define MPU6050_RA_YG_OFFS_USRH 0x15 //[15:0] YG_OFFS_USR
#define MPU6050_RA_YG_OFFS_USRL 0x16
#define MPU6050_RA_ZG_OFFS_USRH 0x17 //[15:0] ZG_OFFS_USR
#define MPU6050_RA_ZG_OFFS_USRL 0x18
#define MPU6050_RA_SMPLRT_DIV 0x19
#define MPU6050_RA_CONFIG 0x1A
#define MPU6050_RA_GYRO_CONFIG 0x1B
#define MPU6050_RA_ACCEL_CONFIG 0x1C
#define MPU6050_RA_FF_THR 0x1D
#define MPU6050_RA_FF_DUR 0x1E
#define MPU6050_RA_MOT_THR 0x1F
#define MPU6050_RA_MOT_DUR 0x20
#define MPU6050_RA_ZRMOT_THR 0x21
#define MPU6050_RA_ZRMOT_DUR 0x22
#define MPU6050_RA_FIFO_EN 0x23
#define MPU6050_RA_I2C_MST_CTRL 0x24
#define MPU6050_RA_I2C_SLV0_ADDR 0x25
#define MPU6050_RA_I2C_SLV0_REG 0x26
#define MPU6050_RA_I2C_SLV0_CTRL 0x27
#define MPU6050_RA_I2C_SLV1_ADDR 0x28
#define MPU6050_RA_I2C_SLV1_REG 0x29
#define MPU6050_RA_I2C_SLV1_CTRL 0x2A
#define MPU6050_RA_I2C_SLV2_ADDR 0x2B
#define MPU6050_RA_I2C_SLV2_REG 0x2C
#define MPU6050_RA_I2C_SLV2_CTRL 0x2D
#define MPU6050_RA_I2C_SLV3_ADDR 0x2E
#define MPU6050_RA_I2C_SLV3_REG 0x2F
#define MPU6050_RA_I2C_SLV3_CTRL 0x30
#define MPU6050_RA_I2C_SLV4_ADDR 0x31
#define MPU6050_RA_I2C_SLV4_REG 0x32
#define MPU6050_RA_I2C_SLV4_DO 0x33
#define MPU6050_RA_I2C_SLV4_CTRL 0x34
#define MPU6050_RA_I2C_SLV4_DI 0x35
#define MPU6050_RA_I2C_MST_STATUS 0x36
#define MPU6050_RA_INT_PIN_CFG 0x37
#define MPU6050_RA_INT_ENABLE 0x38
#define MPU6050_RA_DMP_INT_STATUS 0x39
#define MPU6050_RA_INT_STATUS 0x3A
#define MPU6050_RA_ACCEL_XOUT_H 0x3B
#define MPU6050_RA_ACCEL_XOUT_L 0x3C
#define MPU6050_RA_ACCEL_YOUT_H 0x3D
#define MPU6050_RA_ACCEL_YOUT_L 0x3E
#define MPU6050_RA_ACCEL_ZOUT_H 0x3F
#define MPU6050_RA_ACCEL_ZOUT_L 0x40
#define MPU6050_RA_TEMP_OUT_H 0x41
#define MPU6050_RA_TEMP_OUT_L 0x42
#define MPU6050_RA_GYRO_XOUT_H 0x43
#define MPU6050_RA_GYRO_XOUT_L 0x44
#define MPU6050_RA_GYRO_YOUT_H 0x45
#define MPU6050_RA_GYRO_YOUT_L 0x46
#define MPU6050_RA_GYRO_ZOUT_H 0x47
#define MPU6050_RA_GYRO_ZOUT_L 0x48
#define MPU6050_RA_EXT_SENS_DATA_00 0x49
#define MPU6050_RA_EXT_SENS_DATA_01 0x4A
#define MPU6050_RA_EXT_SENS_DATA_02 0x4B
#define MPU6050_RA_EXT_SENS_DATA_03 0x4C
#define MPU6050_RA_EXT_SENS_DATA_04 0x4D
#define MPU6050_RA_EXT_SENS_DATA_05 0x4E
#define MPU6050_RA_EXT_SENS_DATA_06 0x4F
#define MPU6050_RA_EXT_SENS_DATA_07 0x50
#define MPU6050_RA_EXT_SENS_DATA_08 0x51
#define MPU6050_RA_EXT_SENS_DATA_09 0x52
#define MPU6050_RA_EXT_SENS_DATA_10 0x53
#define MPU6050_RA_EXT_SENS_DATA_11 0x54
#define MPU6050_RA_EXT_SENS_DATA_12 0x55
#define MPU6050_RA_EXT_SENS_DATA_13 0x56
#define MPU6050_RA_EXT_SENS_DATA_14 0x57
#define MPU6050_RA_EXT_SENS_DATA_15 0x58
#define MPU6050_RA_EXT_SENS_DATA_16 0x59
#define MPU6050_RA_EXT_SENS_DATA_17 0x5A
#define MPU6050_RA_EXT_SENS_DATA_18 0x5B
#define MPU6050_RA_EXT_SENS_DATA_19 0x5C
#define MPU6050_RA_EXT_SENS_DATA_20 0x5D
#define MPU6050_RA_EXT_SENS_DATA_21 0x5E
#define MPU6050_RA_EXT_SENS_DATA_22 0x5F
#define MPU6050_RA_EXT_SENS_DATA_23 0x60
#define MPU6050_RA_MOT_DETECT_STATUS 0x61
#define MPU6050_RA_I2C_SLV0_DO 0x63
#define MPU6050_RA_I2C_SLV1_DO 0x64
#define MPU6050_RA_I2C_SLV2_DO 0x65
#define MPU6050_RA_I2C_SLV3_DO 0x66
#define MPU6050_RA_I2C_MST_DELAY_CTRL 0x67
#define MPU6050_RA_SIGNAL_PATH_RESET 0x68
#define MPU6050_RA_MOT_DETECT_CTRL 0x69
#define MPU6050_RA_USER_CTRL 0x6A
#define MPU6050_RA_PWR_MGMT_1 0x6B
#define MPU6050_RA_PWR_MGMT_2 0x6C
#define MPU6050_RA_BANK_SEL 0x6D
#define MPU6050_RA_MEM_START_ADDR 0x6E
#define MPU6050_RA_MEM_R_W 0x6F
#define MPU6050_RA_DMP_CFG_1 0x70
#define MPU6050_RA_DMP_CFG_2 0x71
#define MPU6050_RA_FIFO_COUNTH 0x72
#define MPU6050_RA_FIFO_COUNTL 0x73
#define MPU6050_RA_FIFO_R_W 0x74
#define MPU6050_RA_WHO_AM_I 0x75

#define MPU6050_WHO_AM_I_IDENTIFIER 0x68

const struct {
	uint8_t reg;       uint8_t value;
} MPU6050_INIT_REGS[] = {
	{MPU6050_RA_SMPLRT_DIV,    0x07},  //Sets sample rate to 8000/1+7 = 1000Hz
	{MPU6050_RA_CONFIG,        0x00},  //Disable FSync, 256Hz DLPF
	{MPU6050_RA_GYRO_CONFIG,   0x08},  //Disable gyro self tests, scale of 500 degrees/s
	{MPU6050_RA_ACCEL_CONFIG,  0x00},  //Disable accel self tests, scale of +-2g, no DHPF
	{MPU6050_RA_FF_THR,        0x00},  //Freefall threshold of |0mg|
	{MPU6050_RA_FF_DUR,        0x00},  //Freefall duration limit of 0
	{MPU6050_RA_MOT_THR,       0x00},  //Motion threshold of 0mg
	{MPU6050_RA_MOT_DUR,       0x00},  //Motion duration of 0s
	{MPU6050_RA_ZRMOT_THR,     0x00},  //Zero motion threshold
	{MPU6050_RA_ZRMOT_DUR,     0x00},  //Zero motion duration threshold
	{MPU6050_RA_FIFO_EN,       0x00},  //Disable sensor output to FIFO buffer
	//AUX I2C setup
	{MPU6050_RA_I2C_MST_CTRL,  0x00},  //Sets AUX I2C to single master control, plus other config
	{MPU6050_RA_I2C_SLV0_ADDR, 0x00},  //Setup AUX I2C slaves
	{MPU6050_RA_I2C_SLV0_REG,  0x00},
	{MPU6050_RA_I2C_SLV0_CTRL, 0x00},

	{MPU6050_RA_INT_PIN_CFG,   0x00},  //Setup INT pin and AUX I2C pass through
	{MPU6050_RA_INT_ENABLE,    0x00},  //Enable data ready interrupt
	{MPU6050_RA_I2C_MST_DELAY_CTRL, 0x00}, //More slave config
	{MPU6050_RA_SIGNAL_PATH_RESET, 0x00},  //Reset sensor signal paths
	{MPU6050_RA_MOT_DETECT_CTRL, 0x00}, //Motion detection control
	{MPU6050_RA_USER_CTRL,     0x00}, //Disables FIFO, AUX I2C, FIFO and I2C reset bits to 0
	{MPU6050_RA_PWR_MGMT_1,    0x02}, //Sets clock source to gyro reference w/ PLL
	{MPU6050_RA_PWR_MGMT_2,    0x00}, //Controls frequency of wakeups in accel low power mode plus the sensor standby modes
};

#define MPU6050_DEBUG_INFO(...) DEBUG_INFO("ACC:" __VA_ARGS__)
//#define MPU6050_DEBUG_INFO(...) {}

class CI2CDeviceMPU6050 :
		public IDeviceMPU6050,
		public IDeviceI2C,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
	typedef IDeviceMPU6050::time_delta_t time_delta_t;
public:
//	virtual ~CI2CDeviceMPU6050();

public: // IDeviceMPU6050
	virtual void Init(
			II2C * pI2C,
			read_dataset_t eReadDataset,
			angular_maxrate_t eAngularMaxRate, unsigned uiGyroLoPassFilter,
			linear_maxrate_t eLinearMaxRate,
			time_delta_t tdRefreshPeriod,
			bool bAlternateAddress
	);
	virtual void InitExtSensor(unsigned uiBusAddress, unsigned uiRegAddress, unsigned uiSize, ExtSensorsCallback cb);

	virtual const angular_rates_3D_t& GetAngularRates() const;
	virtual bool SetAngularRatesCallback(AngularRatesCallback cb);
	virtual const linear_rates_3D_t& GetLinearRates() const;
	virtual bool SetLinearRatesCallback(LinearRatesCallback cb);
	virtual unsigned GetTemperature() const;
	virtual bool SetTemperatureCallback(TemperatureCallback cb);

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

private:
	void ProcessInit();

private:
	II2C *                m_pI2C;
	uint8_t               m_uiAddress;
	read_dataset_t        m_eReadDataset;
	angular_maxrate_t     m_eAngularMaxRate;
	unsigned              m_uiGyroLoPassFilter;
	linear_maxrate_t      m_eLinearMaxRate;
	bool                  m_bCommOk;
	angular_rates_3D_t    m_AngularRates;
	AngularRatesCallback  m_cbAngularRatesCallback;
	unsigned              m_uiTemperature;

	linear_rates_3D_t     m_LinearRates;
	LinearRatesCallback   m_cbLinearRatesCallback;
	TemperatureCallback   m_cbTemperatureCallback;

	enum {
		OpIsInit_Start,
		OpIsInit_CHECK = OpIsInit_Start,
		OpIsInit_INIT_REGS_N,
		OpIsReadAll,
		OpIsReadGyroAcceTemp,
		OpIsReadGyro,
		OpIsReadAcce,
		OpIsReadTemp,
		OpIsNone,
	}         m_eCurrentOp;
	unsigned  m_nInitRegIndex;
	bool      m_bReinitRequired;
	bool      m_bInputReadRequired;
	unsigned  m_uiLastInputReadTime;

	CTimeout  m_toRefreshTimeout;
	unsigned  m_nExternalSensorsBytes;

	uint8_t  m_uiRegAddressToRead;
	uint16_t m_auiTempReadRegData[7];
	uint8_t  m_auiTempExtSensors[26];
	struct {
		uint8_t uiReg;
		uint8_t uiValue;
		void Assign(uint8_t _uiReg, uint8_t _uiValue) {uiReg = _uiReg; uiValue = _uiValue;};
	} m_tWriteRegPkt;
	CChainedConstChunks m_cccRegAddressToReadChunks;
	CChainedIoChunks    m_cicRegValueChunks;
	CChainedConstChunks m_cccWriteRegPktChunks;
};



void CI2CDeviceMPU6050::Init(
		II2C * pI2C,
		read_dataset_t eReadDataset,
		angular_maxrate_t eAngularMaxRate, unsigned uiGyroLoPassFilter,
		linear_maxrate_t eLinearMaxRate,
		time_delta_t tdRefreshPeriod,
		bool bAlternateAddress
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::Init(pI2C, eReadDataset, eAngularMaxRate, uiGyroLoPassFilter, eLinearMaxRate, tdRefreshPeriod, bAlternateAddress));

	CParentProcess::Init(&g_TickProcessScheduller);

	m_pI2C = pI2C;
	m_uiAddress = bAlternateAddress ? MPU6050_ADDRESS_ALT : MPU6050_ADDRESS;
	m_eReadDataset = eReadDataset;
	m_eAngularMaxRate = eAngularMaxRate;
	m_uiGyroLoPassFilter = uiGyroLoPassFilter;
	m_eLinearMaxRate = eLinearMaxRate;
	m_nExternalSensorsBytes = 0;
	m_bCommOk = false;
	m_eCurrentOp = OpIsInit_Start;
	m_nInitRegIndex = 0;
	m_bReinitRequired = true;
	m_bInputReadRequired = true;
	m_uiLastInputReadTime = 0;
	m_toRefreshTimeout.Init(tdRefreshPeriod);
	m_cbAngularRatesCallback = NullCallback();
	m_cbLinearRatesCallback = NullCallback();
	m_cbTemperatureCallback = NullCallback();
	m_cccRegAddressToReadChunks.Assign((uint8_t*)&m_uiRegAddressToRead, sizeof(m_uiRegAddressToRead));
	m_cccRegAddressToReadChunks.Break();
	m_cicRegValueChunks.Assign((uint8_t*)m_auiTempReadRegData, 1);
	m_cicRegValueChunks.Break();
	m_cccWriteRegPktChunks.Assign((uint8_t*)&m_tWriteRegPkt, sizeof(m_tWriteRegPkt));
	m_cccWriteRegPktChunks.Break();

	if (m_toRefreshTimeout.GetIsDefined()) {
		m_toRefreshTimeout.Start();
		ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
	}

	m_pI2C->Queue_I2C_Session(this);
}
void CI2CDeviceMPU6050::InitExtSensor(unsigned uiBusAddress, unsigned uiRegAddress, unsigned uiSize, ExtSensorsCallback cb)
{
	// todo external sensors
	ASSERT("NOT IMPLEMENTED")
}


const CI2CDeviceMPU6050::angular_rates_3D_t& CI2CDeviceMPU6050::GetAngularRates() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::GetAngularRates());
	return m_AngularRates;
}
bool CI2CDeviceMPU6050::SetAngularRatesCallback(AngularRatesCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::SetAngularRatesCallback(cb));
	if (!m_cbAngularRatesCallback) {
		m_cbAngularRatesCallback = cb;
		return true;
	}
	return false;
}
const CI2CDeviceMPU6050::linear_rates_3D_t& CI2CDeviceMPU6050::GetLinearRates() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::GetLinearRates());
	return m_LinearRates;
}
bool CI2CDeviceMPU6050::SetLinearRatesCallback(LinearRatesCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::SetLinearRatesCallback(cb));
	if (!m_cbLinearRatesCallback) {
		m_cbLinearRatesCallback = cb;
		return true;
	}
	return false;
}
unsigned CI2CDeviceMPU6050::GetTemperature() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::GetTemperature());

	return m_uiTemperature;
}
bool CI2CDeviceMPU6050::SetTemperatureCallback(TemperatureCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceMPU6050::SetTemperatureCallback(cb));
	if (!m_cbTemperatureCallback) {
		m_cbTemperatureCallback = cb;
		return true;
	}
	return false;
}

void CI2CDeviceMPU6050::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	PROCESS_DEBUG_INFO(">>> Process: CI2CDeviceMPU6050");

	MPU6050_DEBUG_INFO("step. op:", m_eCurrentOp);

	if (m_eCurrentOp == OpIsNone) {
		if (m_bReinitRequired) {
			MPU6050_DEBUG_INFO("Init start");
			m_eCurrentOp = OpIsInit_Start;
			m_pI2C->Queue_I2C_Session(this);
		} else if (m_bInputReadRequired || (m_toRefreshTimeout.GetIsDefined() && m_toRefreshTimeout.GetIsElapsed())) {
			MPU6050_DEBUG_INFO("Read required");
			switch (m_eReadDataset) {
			case READ_GYRO_ACCE_TEMP_EXTERNAL:
				m_eCurrentOp = OpIsReadAll;
				break;
			case READ_GYRO_ACCE_TEMP:
				m_eCurrentOp = OpIsReadGyroAcceTemp;
				break;
			case READ_GYRO_ACCE:
				m_eCurrentOp = OpIsReadGyro;
				break;
			case READ_GYRO:
				m_eCurrentOp = OpIsReadGyro;
				break;
			case READ_ACCE:
				m_eCurrentOp = OpIsReadAcce;
				break;
			case READ_TEMP:
				m_eCurrentOp = OpIsReadTemp;
				break;
			}
			m_pI2C->Queue_I2C_Session(this);
		} else if (m_toRefreshTimeout.GetIsDefined()) {
			ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
			MPU6050_DEBUG_INFO("Nothing to do. Wake:", m_toRefreshTimeout.GetTimeLeft() + 1);
		}
	}

	PROCESS_DEBUG_INFO("<<< Process: CI2CDeviceMPU6050");
}

unsigned CI2CDeviceMPU6050::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_uiAddress;
}

bool CI2CDeviceMPU6050::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));

#define REG_READ(reg, ptr, size) { \
		m_uiRegAddressToRead = reg; \
		*ppRetChunksToSend = &m_cccRegAddressToReadChunks; \
		m_cicRegValueChunks.Assign((uint8_t*)ptr, size); \
		*ppRetChunksToRecv = &m_cicRegValueChunks; \
}
#define REG_WRITE(reg, val) { \
		m_tWriteRegPkt.Assign(reg, val); \
		*ppRetChunksToSend = &m_cccWriteRegPktChunks; \
		*ppRetChunksToRecv = NULL; \
}
	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		return false;
		break;
	case OpIsInit_CHECK:
		ASSERTE(m_bReinitRequired);
		MPU6050_DEBUG_INFO("Checking");
		REG_READ(MPU6050_RA_WHO_AM_I, m_auiTempReadRegData, 1);
		break;
	case OpIsInit_INIT_REGS_N:
		ASSERTE(m_bReinitRequired);
		ASSERTE(m_nInitRegIndex < ARRAY_SIZE(MPU6050_INIT_REGS));
		MPU6050_DEBUG_INFO("Configuring:", m_nInitRegIndex);
		REG_WRITE(MPU6050_INIT_REGS[m_nInitRegIndex].reg, MPU6050_INIT_REGS[m_nInitRegIndex].value);
		break;
	case OpIsReadAll:
		MPU6050_DEBUG_INFO("Reading all + ext. Len:", 14 + m_nExternalSensorsBytes);
		REG_READ(MPU6050_RA_ACCEL_XOUT_H, m_auiTempReadRegData, 14 + m_nExternalSensorsBytes);
		m_toRefreshTimeout.Start();
		break;
	case OpIsReadGyroAcceTemp:
		MPU6050_DEBUG_INFO("Reading all. Len:", 14);
		REG_READ(MPU6050_RA_ACCEL_XOUT_H, m_auiTempReadRegData, 14);
		m_toRefreshTimeout.Start();
		break;
	case OpIsReadGyro:
		MPU6050_DEBUG_INFO("Reading gyro. Len:", 6);
		REG_READ(MPU6050_RA_GYRO_XOUT_H, m_auiTempReadRegData, 6);
		m_toRefreshTimeout.Start();
		break;
	case OpIsReadAcce:
		MPU6050_DEBUG_INFO("Reading acc. Len:", 6);
		REG_READ(MPU6050_RA_ACCEL_XOUT_H, m_auiTempReadRegData, 6);
		m_toRefreshTimeout.Start();
		break;
	case OpIsReadTemp:
		MPU6050_DEBUG_INFO("Reading temp. Len:", 2);
		REG_READ(MPU6050_RA_TEMP_OUT_H, m_auiTempReadRegData, 2);
		m_toRefreshTimeout.Start();
		break;
	}
	return true;
}

void CI2CDeviceMPU6050::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());

	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit_CHECK:
		if (m_auiTempReadRegData[0] == MPU6050_WHO_AM_I_IDENTIFIER) {
			DEBUG_INFO("MPU6050 detected. Addr:", m_uiAddress);
			m_eCurrentOp = OpIsInit_INIT_REGS_N;
			m_nInitRegIndex = 0;
			m_pI2C->Queue_I2C_Session(this);
		} else {
			DEBUG_INFO("MPU6050 missed. Addr:", m_uiAddress);
			MPU6050_DEBUG_INFO("MPU6050 id expected." STR(MPU6050_WHO_AM_I_IDENTIFIER) ". Id:", m_auiTempReadRegData[0]);
			m_bReinitRequired = false;
			m_bInputReadRequired = false;
			m_eCurrentOp = OpIsNone;
			m_bCommOk = false;
			ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
			return;
		}
		break;
	case OpIsInit_INIT_REGS_N:
		++m_nInitRegIndex;
		if (m_nInitRegIndex < ARRAY_SIZE(MPU6050_INIT_REGS)) {
			m_pI2C->Queue_I2C_Session(this);
		} else {
			MPU6050_DEBUG_INFO("init done");
			m_bReinitRequired = false;
			m_bInputReadRequired = false;
			m_eCurrentOp = OpIsNone;
			m_bCommOk = true;
			m_toRefreshTimeout.Start();
		}
		break;
	case OpIsReadAll:
		m_eCurrentOp = OpIsNone;
		m_bInputReadRequired = false;
		m_toRefreshTimeout.Start();
		m_LinearRates.iX  = ntohs(m_auiTempReadRegData[0]);
		m_LinearRates.iY  = ntohs(m_auiTempReadRegData[1]);
		m_LinearRates.iZ  = ntohs(m_auiTempReadRegData[2]);
		m_uiTemperature   = ntohs(m_auiTempReadRegData[3]);
		m_AngularRates.iX = ntohs(m_auiTempReadRegData[4]);
		m_AngularRates.iY = ntohs(m_auiTempReadRegData[5]);
		m_AngularRates.iZ = ntohs(m_auiTempReadRegData[6]);
		MPU6050_DEBUG_INFO("Read all. X:", m_LinearRates.iX);
		// todo external sensors
		ASSERT("NOT IMPLEMENTED")
		if (m_cbAngularRatesCallback) m_cbAngularRatesCallback(m_AngularRates);
		if (m_cbLinearRatesCallback)  m_cbLinearRatesCallback(m_LinearRates);
		if (m_cbTemperatureCallback)  m_cbTemperatureCallback(m_uiTemperature);
		break;
	case OpIsReadGyroAcceTemp:
		m_eCurrentOp = OpIsNone;
		m_bInputReadRequired = false;
		m_toRefreshTimeout.Start();
		m_LinearRates.iX  = ntohs(m_auiTempReadRegData[0]);
		m_LinearRates.iY  = ntohs(m_auiTempReadRegData[1]);
		m_LinearRates.iZ  = ntohs(m_auiTempReadRegData[2]);
		m_uiTemperature   = ntohs(m_auiTempReadRegData[3]);
		m_AngularRates.iX = ntohs(m_auiTempReadRegData[4]);
		m_AngularRates.iY = ntohs(m_auiTempReadRegData[5]);
		m_AngularRates.iZ = ntohs(m_auiTempReadRegData[6]);
		MPU6050_DEBUG_INFO("Read all. X:", m_LinearRates.iX);
		if (m_cbAngularRatesCallback) m_cbAngularRatesCallback(m_AngularRates);
		if (m_cbLinearRatesCallback)  m_cbLinearRatesCallback(m_LinearRates);
		if (m_cbTemperatureCallback)  m_cbTemperatureCallback(m_uiTemperature);
		break;
	case OpIsReadGyro:
		if (m_eReadDataset == READ_GYRO_ACCE) {
			m_eCurrentOp = OpIsReadAcce;
			m_pI2C->Queue_I2C_Session(this);
		} else {
			m_eCurrentOp = OpIsNone;
		}
		m_bInputReadRequired = false;
		m_toRefreshTimeout.Start();
		m_AngularRates.iX = ntohs(m_auiTempReadRegData[0]);
		m_AngularRates.iY = ntohs(m_auiTempReadRegData[1]);
		m_AngularRates.iZ = ntohs(m_auiTempReadRegData[2]);
		if (m_cbAngularRatesCallback) m_cbAngularRatesCallback(m_AngularRates);
		MPU6050_DEBUG_INFO("Read gyro. X:", m_AngularRates.iX);
		break;
	case OpIsReadAcce:
		m_eCurrentOp = OpIsNone;
		m_bInputReadRequired = false;
		m_toRefreshTimeout.Start();
		m_LinearRates.iX  = ntohs(m_auiTempReadRegData[0]);
		m_LinearRates.iY  = ntohs(m_auiTempReadRegData[1]);
		m_LinearRates.iZ  = ntohs(m_auiTempReadRegData[2]);
		if (m_cbLinearRatesCallback)  m_cbLinearRatesCallback(m_LinearRates);
		MPU6050_DEBUG_INFO("Read acc. X:", m_LinearRates.iX);
		break;
	case OpIsReadTemp:
		m_eCurrentOp = OpIsNone;
		m_bInputReadRequired = false;
		m_uiTemperature   = ntohs(m_auiTempReadRegData[0]);
		if (m_cbTemperatureCallback)  m_cbTemperatureCallback(m_uiTemperature);
		MPU6050_DEBUG_INFO("Read temp. X:", m_uiTemperature);
		break;
	}
	if (m_bCommOk && (m_bInputReadRequired || m_bReinitRequired)) {
		ContinueNow();
	} else if (m_toRefreshTimeout.GetIsDefined()) {
		ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
	}
}

void CI2CDeviceMPU6050::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit_CHECK:
	case OpIsInit_INIT_REGS_N:
		m_bReinitRequired = true;
		break;
	case OpIsReadAll:
	case OpIsReadGyroAcceTemp:
	case OpIsReadGyro:
	case OpIsReadAcce:
	case OpIsReadTemp:
		m_bInputReadRequired = true;
		break;
	}

	if (e == I2C_ERROR_ACK || m_eCurrentOp == OpIsInit_CHECK) {
		DEBUG_INFO("MPU6050 missed. Addr:", m_uiAddress);
		m_bCommOk = false;
		m_bReinitRequired = true;
		ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
	} else {
		DEBUG_INFO("MPU6050 error");
		ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
	}

	m_eCurrentOp = OpIsNone;
}

void CI2CDeviceMPU6050::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
	ContinueNow();
}

CI2CDeviceMPU6050       MPU6050;

IDeviceMPU6050* GetMPU6050Interface() {return &MPU6050;};
