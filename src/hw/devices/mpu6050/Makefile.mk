USE_I2C += $(if $(filter YES,$(USE_MPU6050)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_MPU6050)), $(addprefix src/hw/devices/mpu6050/, \
	dev_mpu6050.cpp \
))
