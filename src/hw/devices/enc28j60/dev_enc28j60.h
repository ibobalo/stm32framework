#ifndef DEV_ENC28J60_H_INCLUDED
#define DEV_ENC28J60_H_INCLUDED

#include "interfaces/gpio.h"
#include "interfaces/spi.h"
#include "net/net_if_en.h"

/*
 * usage:
 * class CProtocolProcessor_Echo {
 * public:
 *  void Init(INetworkInterface pEn) {m_pEn = pEn; pEn->Init(NULL, BIND_MEM_CB(&CProtocolProcessor::OnPktReceived, this);};
 *  void CProtocolProcessor::OnPktReceived(unsigned uiPktDataSize) {m_uiRxSize = uiPktDataSize; ContinueAsap();};
 *  void CProtocolProcessor::OnTxDoneCallback(bool bTxSucceed)     {m_bBusy = false; ContinueAsap();};
 *  void SingleStep() {
 *  if (m_uiRxSize && !m_bBusy) {
 *    memcpy(   m_TgtMac,    m_pEn->GetRxedFrameSourceMacAddr(), 6);
 *    uint16_t uiFrameType = m_pEn->GetRxedFrameType();
 *    unsigned uiRxDataLen = m_pEn->GetRxedFrameSize();
 *    memcpy(   m_Buf,       m_pEn->GetRxedFrameData(), uiRxDataLen);
 *    m_pEn->ReleaseRxedFrame();
 *    if (PushTxPkt(m_TgtMac, m_Buf, uiFrameType, uiRxDataLen)) {m_bBusy = true; m_uiRxSize = 0;};
 *  }
 */

extern INetworkInterface* AcquireNetworkInterfaceENC28J60(ISPI* pSPI, IDigitalOut* pSelectDO, IDigitalOut* pResetDO, IDigitalIn* pInteruptDI);

#endif //DEV_ENC28J60_H_INCLUDED
