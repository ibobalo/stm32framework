#include "stdafx.h"
#include "dev_enc28j60.h"
#include "hw/hw.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include "hw/mcu/hw_spi.h"
#include "hw/tick_timeout.h"
#include "interfaces/notify.h"
#include "util/macros.h"
#include "util/utils.h"
#include "tick_process.h"
#include <stddef.h>
#include <string.h>

#define RX_PULL_PERIOD_MS      5
#define LINK_CHECK_PERIOD_MS   1000
#define SPI_SPEED           10000000
#define HARD_RESET_DURATION 3
#define HARD_RESET_DELAY    2
#define SOFT_RESET_DELAY    2
#define PHY_WAIT_RETRIES    10
#define PHY_WAIT_DELAY      1
#define TX_BUSY_DELAY       1
#define BANK_SELECT_RETRIES 2
#define READ_RETRIES        3
#define WRITE16_RETRIES     3
#ifdef DEBUG
# define VERIFY_SELECTED_BANK
# define VERIFY_WRITE16
# define VERIFY_WRITEPHY
#endif

#define ETHERNET_HEADER_LEN 14
#define ETHERNET_CRC_LEN 4


#define PHCON1_INIT_VALUE (PHCON1_PDPXMD)
#define PHCON2_INIT_VALUE (PHCON2_HDLDIS)
#define PHLCON_INIT_VALUE (PHLCON_LACFG_ON | PHLCON_LBCFG_TXRXLNK | PHLCON_LFRQ0 | PHLCON_STRCH)
#define EIE_INIT_VALUE    (0 /*| EIE_INTIE | EIE_RXERIE*/ /*| EIE_LINKIE*/)
#define PHIE_INIT_VALUE   (0 /*| PHIE_PLNKIE | PHIE_PGEIE*/)

//#define ENC28J60_DEBUG_INFO(...) DEBUG_INFO("LAN:" __VA_ARGS__)
#define ENC28J60_DEBUG_INFO(...) {}
//#define PEDANTIC_ERRORS(msg) ASSERT(msg)
#define PEDANTIC_ERRORS(msg) DEBUG_INFO("LAN:" msg)
//#define PARANOID_ERRORS(msg) ASSERT(msg)
#define PARANOID_ERRORS(msg) DEBUG_INFO("LAN:" msg)


#define ENC28J60_BUFSIZE	0x2000
#define ENC28J60_RXSIZE		0x1A00
#define ENC28J60_BUFEND		(ENC28J60_BUFSIZE-1)

#define ENC28J60_MAXFRAME	1500


#define ENC28J60_RXSTART	0
#define ENC28J60_RXEND		(ENC28J60_RXSIZE-1)
#define ENC28J60_TXSTART	ENC28J60_RXSIZE

#define ENC28J60_SPI_RCR	0x00
#define ENC28J60_SPI_RBM	0x3A
#define ENC28J60_SPI_WCR	0x40
#define ENC28J60_SPI_WBM	0x7A
#define ENC28J60_SPI_BFS	0x80
#define ENC28J60_SPI_BFC	0xA0
#define ENC28J60_SPI_SC		0xFF

#define ENC28J60_OPCODE_MASK 0xE0
#define ENC28J60_ADDR_MASK	0x1F
#define ENC28J60_COMMON_CR	0x1B

#define ENC28J60_IS_OPCODE(x) ( false \
		|| (x) == ENC28J60_SPI_RCR \
		|| (x) == ENC28J60_SPI_RBM \
		|| (x) == ENC28J60_SPI_WCR \
		|| (x) == ENC28J60_SPI_WBM \
		|| (x) == ENC28J60_SPI_BFS \
		|| (x) == ENC28J60_SPI_BFC \
		|| (x) == ENC28J60_SPI_SC \
)
/*
 * Main registers
 */

#define EIE 				0x1B
#define EIR 				0x1C
#define ESTAT 				0x1D
#define ECON2 				0x1E
#define ECON1 				0x1F


// Buffer read pointer
#define ERDPTL 				0x00
#define ERDPTH 				0x01
#define ERDPT				ERDPTL

// Buffer write pointer
#define EWRPTL 				0x02
#define EWRPTH 				0x03
#define EWRPT				EWRPTL

// Tx packet start pointer
#define ETXSTL 				0x04
#define ETXSTH 				0x05
#define ETXST				ETXSTL

// Tx packet end pointer
#define ETXNDL 				0x06
#define ETXNDH 				0x07
#define ETXND				ETXNDL

// Rx FIFO start pointer
#define ERXSTL 				0x08
#define ERXSTH 				0x09
#define ERXST				ERXSTL

// Rx FIFO end pointer
#define ERXNDL 				0x0A
#define ERXNDH 				0x0B
#define ERXND				ERXNDL

// Rx FIFO read pointer
#define ERXRDPTL 			0x0C
#define ERXRDPTH 			0x0D
#define ERXRDPT				ERXRDPTL

// Rx FIFO write pointer
#define ERXWRPTL 			0x0E
#define ERXWRPTH 			0x0F
#define ERXWRPT				ERXWRPTL

// DMA source block start pointer
#define EDMASTL 			0x10
#define EDMASTH 			0x11
#define EDMAST				EDMASTL

// DMA source block end pointer
#define EDMANDL 			0x12
#define EDMANDH 			0x13
#define EDMAND				EDMANDL

// DMA destination pointer
#define EDMADSTL 			0x14
#define EDMADSTH 			0x15
#define	EDMADST				EDMADSTL

// DMA checksum
#define EDMACSL 			0x16
#define EDMACSH 			0x17
#define EDMACS				EDMACSL


#define IN_BANK_2 |0x20
#define IN_BANK_3 |0x40
#define IN_BANK_4 |0x60

// Hash table registers
#define EHT0 				(0x00 IN_BANK_2)
#define EHT1 				(0x01 IN_BANK_2)
#define EHT2 				(0x02 IN_BANK_2)
#define EHT3 				(0x03 IN_BANK_2)
#define EHT4 				(0x04 IN_BANK_2)
#define EHT5 				(0x05 IN_BANK_2)
#define EHT6 				(0x06 IN_BANK_2)
#define EHT7 				(0x07 IN_BANK_2)

// Pattern match registers
#define EPMM0 				(0x08 IN_BANK_2)
#define EPMM1 				(0x09 IN_BANK_2)
#define EPMM2 				(0x0A IN_BANK_2)
#define EPMM3 				(0x0B IN_BANK_2)
#define EPMM4 				(0x0C IN_BANK_2)
#define EPMM5 				(0x0D IN_BANK_2)
#define EPMM6 				(0x0E IN_BANK_2)
#define EPMM7 				(0x0F IN_BANK_2)
#define EPMCSL 				(0x10 IN_BANK_2)
#define EPMCSH 				(0x11 IN_BANK_2)
#define EPMOL 				(0x14 IN_BANK_2)
#define EPMOH 				(0x15 IN_BANK_2)

// Wake-on-LAN interrupt registers
#define EWOLIE 				(0x16 IN_BANK_2)
#define EWOLIR 				(0x17 IN_BANK_2)

// Receive filters mask
#define ERXFCON 			(0x18 IN_BANK_2)

// Packet counter
#define EPKTCNT 			(0x19 IN_BANK_2)


#define HAS_DUMMY_BYTE | 0x80
// MAC control registers
#define MACON1 				(0x00 IN_BANK_3 HAS_DUMMY_BYTE)
#define MACON2 				(0x01 IN_BANK_3 HAS_DUMMY_BYTE)
#define MACON3 				(0x02 IN_BANK_3 HAS_DUMMY_BYTE)
#define MACON4 				(0x03 IN_BANK_3 HAS_DUMMY_BYTE)

// MAC Back-to-back gap
#define MABBIPG 			(0x04 IN_BANK_3 HAS_DUMMY_BYTE)

// MAC Non back-to-back gap
#define MAIPGL 				(0x06 IN_BANK_3 HAS_DUMMY_BYTE)
#define MAIPGH 				(0x07 IN_BANK_3 HAS_DUMMY_BYTE)

// Collision window & rexmit timer
#define MACLCON1 			(0x08 IN_BANK_3 HAS_DUMMY_BYTE)
#define MACLCON2 			(0x09 IN_BANK_3 HAS_DUMMY_BYTE)

// Max frame length
#define MAMXFLL 			(0x0A IN_BANK_3 HAS_DUMMY_BYTE)
#define MAMXFLH 			(0x0B IN_BANK_3 HAS_DUMMY_BYTE)
#define MAMXFL				MAMXFLL

// MAC-PHY support register
#define MAPHSUP 			(0x0D IN_BANK_3 HAS_DUMMY_BYTE)
#define MICON 				(0x11 IN_BANK_3 HAS_DUMMY_BYTE)

// MII registers
#define MICMD 				(0x12 IN_BANK_3 HAS_DUMMY_BYTE)
#define MIREGADR 			(0x14 IN_BANK_3 HAS_DUMMY_BYTE)

#define MIWRL 				(0x16 IN_BANK_3 HAS_DUMMY_BYTE)
#define MIWRH 				(0x17 IN_BANK_3 HAS_DUMMY_BYTE)
#define MIWR				MIWRL

#define MIRDL 				(0x18 IN_BANK_3 HAS_DUMMY_BYTE)
#define MIRDH 				(0x19 IN_BANK_3 HAS_DUMMY_BYTE)
#define MIRD				MIRDL

// MAC Address
#define MAADR1 				(0x00 IN_BANK_4 HAS_DUMMY_BYTE)
#define MAADR0 				(0x01 IN_BANK_4 HAS_DUMMY_BYTE)
#define MAADR3 				(0x02 IN_BANK_4 HAS_DUMMY_BYTE)
#define MAADR2 				(0x03 IN_BANK_4 HAS_DUMMY_BYTE)
#define MAADR5 				(0x04 IN_BANK_4 HAS_DUMMY_BYTE)
#define MAADR4 				(0x05 IN_BANK_4 HAS_DUMMY_BYTE)

// Built-in self-test
#define EBSTSD 				(0x06 IN_BANK_4)
#define EBSTCON 			(0x07 IN_BANK_4)
#define EBSTCSL 			(0x08 IN_BANK_4)
#define EBSTCSH 			(0x09 IN_BANK_4)
#define MISTAT 				(0x0A IN_BANK_4 HAS_DUMMY_BYTE)

// Revision ID
#define EREVID 				(0x12 IN_BANK_4)

// Clock output control register
#define ECOCON 				(0x15 IN_BANK_4)

// Flow control registers
#define EFLOCON 			(0x17 IN_BANK_4)
#define EPAUSL 				(0x18 IN_BANK_4)
#define EPAUSH 				(0x19 IN_BANK_4)

// PHY registers
#define PHCON1 				0x00
#define PHSTAT1 			0x01
#define PHID1 				0x02
#define PHID2 				0x03
#define PHCON2 				0x10
#define PHSTAT2 			0x11
#define PHIE 				0x12
#define PHIR 				0x13
#define PHLCON 				0x14

// EIE
#define EIE_INTIE			0x80
#define EIE_PKTIE			0x40
#define EIE_DMAIE			0x20
#define EIE_LINKIE			0x10
#define EIE_TXIE			0x08
#define EIE_WOLIE			0x04
#define EIE_TXERIE			0x02
#define EIE_RXERIE			0x01

// EIR
#define EIR_PKTIF			0x40
#define EIR_DMAIF			0x20
#define EIR_LINKIF			0x10
#define EIR_TXIF			0x08
#define EIR_WOLIF			0x04
#define EIR_TXERIF			0x02
#define EIR_RXERIF			0x01

// ESTAT
#define ESTAT_INT			0x80
#define ESTAT_LATECOL		0x10
#define ESTAT_RXBUSY		0x04
#define ESTAT_TXABRT		0x02
#define ESTAT_CLKRDY		0x01

// ECON2
#define ECON2_AUTOINC		0x80
#define ECON2_PKTDEC		0x40
#define ECON2_PWRSV			0x20
#define ECON2_VRPS			0x08

// ECON1
#define ECON1_TXRST			0x80
#define ECON1_RXRST			0x40
#define ECON1_DMAST			0x20
#define ECON1_CSUMEN		0x10
#define ECON1_TXRTS			0x08
#define ECON1_RXEN			0x04
#define ECON1_BSEL1			0x02
#define ECON1_BSEL0			0x01

// EWOLIE
#define EWOLIE_UCWOLIE		0x80
#define EWOLIE_AWOLIE		0x40
#define EWOLIE_PMWOLIE		0x10
#define EWOLIE_MPWOLIE		0x08
#define EWOLIE_HTWOLIE		0x04
#define EWOLIE_MCWOLIE		0x02
#define EWOLIE_BCWOLIE		0x01

// EWOLIR
#define EWOLIR_UCWOLIF		0x80
#define EWOLIR_AWOLIF		0x40
#define EWOLIR_PMWOLIF		0x10
#define EWOLIR_MPWOLIF		0x08
#define EWOLIR_HTWOLIF		0x04
#define EWOLIR_MCWOLIF		0x02
#define EWOLIR_BCWOLIF		0x01

// ERXFCON
#define ERXFCON_UCEN		0x80
#define ERXFCON_ANDOR		0x40
#define ERXFCON_CRCEN		0x20
#define ERXFCON_PMEN		0x10
#define ERXFCON_MPEN		0x08
#define ERXFCON_HTEN		0x04
#define ERXFCON_MCEN		0x02
#define ERXFCON_BCEN		0x01

// MACON1
#define MACON1_LOOPBK		0x10
#define MACON1_TXPAUS		0x08
#define MACON1_RXPAUS		0x04
#define MACON1_PASSALL		0x02
#define MACON1_MARXEN		0x01

// MACON2
#define MACON2_MARST		0x80
#define MACON2_RNDRST		0x40
#define MACON2_MARXRST		0x08
#define MACON2_RFUNRST		0x04
#define MACON2_MATXRST		0x02
#define MACON2_TFUNRST		0x01

// MACON3
#define MACON3_PADCFG2		0x80
#define MACON3_PADCFG1		0x40
#define MACON3_PADCFG0		0x20
#define MACON3_TXCRCEN		0x10
#define MACON3_PHDRLEN		0x08
#define MACON3_HFRMEN		0x04
#define MACON3_FRMLNEN		0x02
#define MACON3_FULDPX		0x01

// MACON4
#define MACON4_DEFER		0x40
#define MACON4_BPEN			0x20
#define MACON4_NOBKOFF		0x10
#define MACON4_LONGPRE		0x02
#define MACON4_PUREPRE		0x01

// MAPHSUP
#define MAPHSUP_RSTINTFC	0x80
#define MAPHSUP_RSTRMII		0x08

// MICON
#define MICON_RSTMII		0x80

// MICMD
#define MICMD_MIISCAN		0x02
#define MICMD_MIIRD			0x01

// EBSTCON
#define EBSTCON_PSV2		0x80
#define EBSTCON_PSV1		0x40
#define EBSTCON_PSV0		0x20
#define EBSTCON_PSEL		0x10
#define EBSTCON_TMSEL1		0x08
#define EBSTCON_TMSEL0		0x04
#define EBSTCON_TME			0x02
#define EBSTCON_BISTST		0x01

// MISTAT
#define MISTAT_NVALID		0x04
#define MISTAT_SCAN			0x02
#define MISTAT_BUSY			0x01

// ECOCON
#define ECOCON_COCON2		0x04
#define ECOCON_COCON1		0x02
#define ECOCON_COCON0		0x01

// EFLOCON
#define EFLOCON_FULDPXS		0x04
#define EFLOCON_FCEN1		0x02
#define EFLOCON_FCEN0		0x01

// PHCON1
#define PHCON1_PRST			0x8000
#define PHCON1_PLOOPBK		0x4000
#define PHCON1_PPWRSV		0x0800
#define PHCON1_PDPXMD		0x0100

// PHSTAT1
#define PHSTAT1_PFDPX		0x1000
#define PHSTAT1_PHDPX		0x0800
#define PHSTAT1_LLSTAT		0x0004
#define PHSTAT1_JBSTAT		0x0002

// PHCON2
#define PHCON2_FRCLNK		0x4000
#define PHCON2_TXDIS		0x2000
#define PHCON2_JABBER		0x0400
#define PHCON2_HDLDIS		0x0100

// PHSTAT2
#define PHSTAT2_TXSTAT		0x2000
#define PHSTAT2_RXSTAT		0x1000
#define PHSTAT2_COLSTAT		0x0800
#define PHSTAT2_LSTAT		0x0400
#define PHSTAT2_DPXSTAT		0x0200
#define PHSTAT2_PLRITY		0x0010

// PHIE
#define PHIE_PLNKIE			0x0010
#define PHIE_PGEIE			0x0002

// PHIR
#define PHIR_PLNKIF			0x0010
#define PHIR_PGIF			0x0004

// PHLCON
#define PHLCON_LACFG3		0x0800
#define PHLCON_LACFG2		0x0400
#define PHLCON_LACFG1		0x0200
#define PHLCON_LACFG0		0x0100
#define PHLCON_LACFG_TX     PHLCON_LACFG0
#define PHLCON_LACFG_RX     PHLCON_LACFG1
#define PHLCON_LACFG_LNK    PHLCON_LACFG2
#define PHLCON_LACFG_DUPLEX PHLCON_LACFG2 | PHLCON_LACFG0
#define PHLCON_LACFG_TXRX   PHLCON_LACFG2 | PHLCON_LACFG1 | PHLCON_LACFG0
#define PHLCON_LACFG_ON     PHLCON_LACFG3
#define PHLCON_LACFG_OFF    PHLCON_LACFG3 | PHLCON_LACFG0
#define PHLCON_LACFG_TXLNK  PHLCON_LACFG3 | PHLCON_LACFG2
#define PHLCON_LACFG_TXRXLNK PHLCON_LACFG3 | PHLCON_LACFG2 | PHLCON_LACFG0

#define PHLCON_LBCFG3		0x0080
#define PHLCON_LBCFG2		0x0040
#define PHLCON_LBCFG1		0x0020
#define PHLCON_LBCFG0		0x0010
#define PHLCON_LACFG_TX     PHLCON_LACFG0
#define PHLCON_LBCFG_RX     PHLCON_LBCFG1
#define PHLCON_LBCFG_LNK    PHLCON_LBCFG2
#define PHLCON_LBCFG_DUPLEX PHLCON_LBCFG2 | PHLCON_LBCFG0
#define PHLCON_LBCFG_TXRX   PHLCON_LBCFG2 | PHLCON_LBCFG1 | PHLCON_LBCFG0
#define PHLCON_LBCFG_ON     PHLCON_LBCFG3
#define PHLCON_LBCFG_OFF    PHLCON_LBCFG3 | PHLCON_LBCFG0
#define PHLCON_LBCFG_TXLNK  PHLCON_LBCFG3 | PHLCON_LBCFG2
#define PHLCON_LBCFG_TXRXLNK PHLCON_LBCFG3 | PHLCON_LBCFG2 | PHLCON_LBCFG0

#define PHLCON_LFRQ1		0x0008
#define PHLCON_LFRQ0		0x0004
#define PHLCON_STRCH		0x0002

// receive status vector
typedef struct {
	uint16_t    uiSavedRXRDPT;
	uint16_t    uiRcvBytesCount;
	union {
		uint16_t    uiStatus;
		struct {
			uint16_t    bLongEvent      :1;
			uint16_t      reserved17    :1;
			uint16_t    bCarientEvent   :1;
			uint16_t      reserved19    :1;
			uint16_t    bCrcError       :1;
			uint16_t    bLenError       :1;
			uint16_t    bOK             :1;
			uint16_t    bMulticast      :1;
			uint16_t    bBroadcast      :1;
			uint16_t    bDribble        :1;
			uint16_t    bControl        :1;
			uint16_t    bPause          :1;
			uint16_t    bUnknownOpcode  :1;
			uint16_t    bVLAN           :1;
			uint16_t    bZero           :1;
		};
	};
} RxPktHeader_t;

class CSPIDeviceENC28J60 :
		public IDeviceSPI,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
	typedef INetworkInterface::mac_addr_t mac_addr_t;
public:
//	virtual ~CSPIDeviceENC28J60();

	void Init(ISPI* pSPI, IDigitalOut* pSelectDO, IDigitalOut* pResetDO, IDigitalIn* pInteruptDI);

	void SetMacAddress(const mac_addr_t* MacAaddress = NULL);
	const mac_addr_t* GetMacAddress() const;

public: // IDeviceSPI
	virtual bool GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const;
	virtual void Select();
	virtual void Release();
	virtual void OnSPIDone();
	virtual void OnSPIError();
	virtual void OnSPIIdle();

public:
	virtual void SingleStep(CTimeType uiTime);

protected:
	virtual void OnPktReceived() = 0;
	virtual void OnLinkChanged(bool bLinkStatus) = 0;
	virtual void OnPktSent() = 0;
	virtual void OnPktError() = 0;
	virtual void OnIncomeCheck() = 0;

protected:
	ISPI*              m_pSPI;
	IDigitalOut*       m_pSelectDO;
	IDigitalOut*       m_pResetDO;
	IDigitalIn*        m_pInterruptDI;
	bool               m_bInterruptRequested;
	bool               m_bResetRequested;
	bool               m_bFrameRxRequested;
	CTimeout           m_toFrameRxTimeout;
	bool               m_bLinkStatusRequested;
	CTimeout           m_toLinkStatusTimeout;
	bool               m_bFrameTxRequested;
	CTimeout           m_toFrameTxTimeout;

	unsigned           m_uiFrameRxedSize;
	uint16_t           m__pad;
	uint8_t            m_FrameRxBuf[16 + ENC28J60_MAXFRAME];

	bool               m_bLinkStatus;
	bool               m_bReportedLinkStatus;

	unsigned             m_uiFrameTxCollectedSize;
	CChainedConstChunks  m_FrameTxHeadChunk;

	struct __attribute__ ((packed)) {
		uint8_t            bControlByte;
		mac_addr_t         BroadcastMacAddr;
		mac_addr_t         SourceMacAddr;
		uint16_t           uiFrameTxType;
	} m_TxHeaderBroadcast;
	struct __attribute__ ((packed)) {
		uint8_t            bControlByte;
		mac_addr_t         TargetMacAddr;
		mac_addr_t         SourceMacAddr;
		uint16_t           uiFrameTxType;
	} m_TxHeaderTargeted;
private:
	void Clear();

	typedef enum {ASYNCOP_BUSY, ASYNCOP_DONE} ASYNCOP_RESULT;
	ASYNCOP_RESULT ExecuteHardResetFSM();
	ASYNCOP_RESULT ExecuteSelectBankFSM();
	ASYNCOP_RESULT ExecutePHYReadFSM();
	ASYNCOP_RESULT ExecutePHYWriteFSM();
	ASYNCOP_RESULT ExecuteRead16FSM();
	ASYNCOP_RESULT ExecuteWrite16FSM();
	ASYNCOP_RESULT ExecuteTestFSM();
	ASYNCOP_RESULT ExecuteInitFSM();
	ASYNCOP_RESULT ExecuteInterruptFSM();
	ASYNCOP_RESULT ExecuteLinkStatusFSM();
	ASYNCOP_RESULT ExecuteFrameTxFSM();
	ASYNCOP_RESULT ExecuteFrameRxFSM();

	ASYNCOP_RESULT DoSetBank(uint8_t uiAddr);

	void ContinueOnSPIIdle();

	enum {
		STATE_IDLE,
		STATE_TEST,
		STATE_INIT,
		STATE_TX,
		STATE_RX,
		STATE_LINK,
		STATE_INTERRUPT,
		STATE_ERROR,
		STATE_RESET
	} m_eState;
	enum {
		HWRESET_Done,
		HWRESET_Activate,
		HWRESET_Deactivate
	} m_eHWResetFSM;
	enum {
		SELECTBANK_Idle,
		SELECTBANK_Start,
		SELECTBANK_Write1 = SELECTBANK_Start,
		SELECTBANK_Write2,
		SELECTBANK_Check,
		SELECTBANK_Done
	} m_eSelectBankFSM;
	enum {
		READPHY_Idle,
		READPHY_Start,
		READPHY_WriteAddr = READPHY_Start,
		READPHY_SetCmdBit,
		READPHY_EnterWaitBusy,
		READPHY_ReadBusy,
		READPHY_WaitBusy,
		READPHY_ClearCmdBit,
		READPHY_ReadDataLo,
		READPHY_ReadDataHi,
		READPHY_Done
	} m_ePHYReadFSM;
	enum {
		WRITEPHY_Idle,
		WRITEPHY_Start,
		WRITEPHY_WriteAddr = WRITEPHY_Start,
		WRITEPHY_WriteDataLo,
		WRITEPHY_WriteDataHi,
		WRITEPHY_EnterWaitBusy,
		WRITEPHY_ReadBusy,
		WRITEPHY_WaitBusy,
		WRITEPHY_Verify,
		WRITEPHY_Wait_Verify,
		WRITEPHY_Done
	} m_ePHYWriteFSM;
	enum {
		READ16_Idle,
		READ16_Start,
		READ16_ReadDataLo = READ16_Start,
		READ16_ReadDataHi,
		READ16_Done
	} m_eRead16FSM;
	enum {
		WRITE16_Idle,
		WRITE16_Start,
		WRITE16_WriteDataLo = WRITE16_Start,
		WRITE16_WriteDataHi,
		WRITE16_ReadDataLo,
		WRITE16_VerifyLo_ReadDataHi,
		WRITE16_VerifyHi,
		WRITE16_Done
	} m_eWrite16FSM;
	enum {
		TEST_Start,
		TEST_Write1 = TEST_Start,
		TEST_Read1,
		TEST_Write2,
		TEST_Read2,
	} m_eTestFSM;
	enum {
		INIT_Idle,
		INIT_Start,
		INIT_SoftReset = INIT_Start,
		INIT_Wait_SoftReset,
		INIT_Read_REVID,
		INIT_Validate_REVID,
		INIT_Write_ERXST,
		INIT_Write_ERXRDPT,
		INIT_Write_ERXND,
		INIT_Write_MACON2,
		INIT_Read_MACON2,
		INIT_Verify_MACON2,
		INIT_Write_MACON1,
		INIT_Read_MACON1,
		INIT_Verify_MACON1,
		INIT_Write_MACON3,
		INIT_Read_MACON3,
		INIT_Verify_MACON3,
		INIT_Write_MAMXFL,
		INIT_Write_MABBIPG,
		INIT_Write_MAIPGL,
		INIT_Write_MAIPGH,
		INIT_Write_MAADR5,
		INIT_Write_MAADR4,
		INIT_Write_MAADR3,
		INIT_Write_MAADR2,
		INIT_Write_MAADR1,
		INIT_Write_MAADR0,
		INIT_Write_PHCON1,
		INIT_Write_PHCON2,
		INIT_Write_PHLCON,
		INIT_Write_EIE,
		INIT_Read_EIE,
		INIT_Verify_EIE,
		INIT_Write_PHIE,
		INIT_Write_EnableRx,
		INIT_Done,
		INIT_HardReset,
	} m_eInitFSM;
	enum {
		LINKSTAT_Idle,
		LINKSTAT_Start,
		LINKSTAT_Read_PHSTAT = LINKSTAT_Start,
		LINKSTAT_Wait_PHSTAT,
	} m_eLinkStatusFSM;
	enum {
		INTERRUPT_Idle,
		INTERRUPT_Start,
		INTERRUPT_Read_EIR = INTERRUPT_Start,
		INTERRUPT_Wait_EIR,
		INTERRUPT_Read_PHIR,
		INTERRUPT_Wait_PHIR,
	} m_eInterruptFSM;
	enum {
		FRAMETX_Idle,
		FRAMETX_Start,
		FRAMETX_Read_ECON1 = FRAMETX_Start,
		FRAMETX_Wait_ECON1_TXRTS,
		FRAMETX_Write_EWRPT,
		FRAMETX_Write_DataChain,
		FRAMETX_Write_ETXST,
		FRAMETX_Write_ETXND,
		FRAMETX_Set_ECON1_TXRTS,
		FRAMETX_Read_EIR,
		FRAMETX_Wait_EIR,
		FRAMETX_Set_ECON1_TXRST,
		FRAMETX_Clear_ECON1_TXRST,
		FRAMETX_Done
	} m_eFrameTxFSM;
	enum {
		FRAMERX_Idle,
		FRAMERX_Start,
		FRAMERX_Read_EPKTCNT = FRAMERX_Start,
		FRAMERX_Check_EPKTCNT,
		FRAMERX_Write_ERDPT,
		FRAMERX_Read_PktHeader,
		FRAMERX_Read_Data,
		FRAMERX_Write_ERXRDPT,
		FRAMERX_Set_PKTDEC,
		FRAMERX_Wait_Done,
	} m_eFrameRxFSM;

	bool     m_bBusy;
	bool     m_bError;

	uint8_t        m_SpiTxCmd[3];
	uint8_t        m_SpiRxTempBuf[3];

	CChainedIoChunks    m_aSpiRxChunks[2];
	CChainedConstChunks m_aSpiTxChunks[2];

	unsigned m_nTries;
	uint8_t  m_uiRevision;

	uint8_t  m_uiBankSelected;
	uint8_t  m_uiBankToSelect;
	uint8_t  m_nBankSelectTries;

	uint8_t  m_uiPhyAddrToRead;
	uint16_t m_uiPhyReadData;
	uint8_t  m_uiPhyAddrToWrite;
	uint16_t m_uiPhyDataToWrite;
	uint16_t m_uiPhyVerifyMask;
	unsigned m_nPHYTries;

	uint8_t  m_uiAddrToRead16;
	uint16_t m_uiDataRead16;
	uint8_t  m_uiAddrToWrite16;
	uint16_t m_uiDataToWrite16;
	uint16_t m_uiData16VerifyMask;
	unsigned m_nWrite16Tries;

	RxPktHeader_t  m_RxPktHeader;
};

class CNetworkInterfaceENC28J60 :
		public INetworkInterface,
		public CSPIDeviceENC28J60
{
public:
	typedef INetworkInterface::mac_addr_t mac_addr_t;
public:
	// ~CNetworkInterfaceENC28J60();
public: // INetworkInterface
	virtual void BindNetworkInterface(CallbackOnPktReceived cbOnPktReceived = CallbackOnPktReceived(), CallbackOnLinkChanged cbOnLinkChanged = CallbackOnLinkChanged());
	virtual void SetPktReceivedCallback(CallbackOnPktReceived cbOnPktReceived = CallbackOnPktReceived());
	virtual void SetLinkChangedCallback(CallbackOnLinkChanged cbOnLinkChanged = CallbackOnLinkChanged());
	virtual void SetIncomeCheckCallback(CallbackOnIncomeCheck cbOnIncomeCheck = CallbackOnIncomeCheck());
	virtual void SetMacAddress(const mac_addr_t* MacAaddress);
	virtual const mac_addr_t* GetMacAddress() const;
	virtual unsigned GetMTU() const;
	virtual void ScheduleRefresh();
	virtual void Reset();
	virtual bool TxPkt(const mac_addr_t* TargetMACadr, uint16_t uiFrameType, CChainedConstChunks* pHeadChunk, CallbackOnTxDone cbOnTxDoneCallback = CallbackOnTxDone());
	virtual unsigned          GetRxedFrameSize() const;
	virtual uint16_t          GetRxedFrameType() const;
	virtual const void*       GetRxedFrameData() const;
	virtual const mac_addr_t* GetRxedFrameSourceMacAddr() const;
	virtual const mac_addr_t* GetRxedFrameTargetMacAdr() const;
	virtual void ReleaseRxedFrame();

protected:
	virtual void OnPktReceived();
	virtual void OnLinkChanged(bool bLinkStatus);
	virtual void OnPktSent();
	virtual void OnPktError();
	virtual void OnIncomeCheck();

private:
	CallbackOnPktReceived m_cbOnPktReceivedCallback;
	CallbackOnLinkChanged m_cbOnLinkChangedCallback;
	CallbackOnIncomeCheck m_cbOnIncomeCheckCallback;
	CallbackOnTxDone      m_cbOnTxDoneCallback;
};


void CSPIDeviceENC28J60::Init(ISPI* pSPI, IDigitalOut* pSelectDO, IDigitalOut* pResetDO, IDigitalIn* pInteruptDI)
{
	ASSERTE(pSPI);
	CParentProcess::Init(&g_TickProcessScheduller);
	m_pSPI = pSPI;
	m_pSelectDO = pSelectDO;
	m_pResetDO = pResetDO;
	m_pInterruptDI = pInteruptDI;
	m_TxHeaderBroadcast.bControlByte = 0;
	m_TxHeaderBroadcast.BroadcastMacAddr.b1 = 0xFF;
	m_TxHeaderBroadcast.BroadcastMacAddr.b2 = 0xFF;
	m_TxHeaderBroadcast.BroadcastMacAddr.b3 = 0xFF;
	m_TxHeaderBroadcast.BroadcastMacAddr.b4 = 0xFF;
	m_TxHeaderBroadcast.BroadcastMacAddr.b5 = 0xFF;
	m_TxHeaderBroadcast.BroadcastMacAddr.b6 = 0xFF;
	m_TxHeaderTargeted.bControlByte = 0;
	SetMacAddress(NULL);
	m_toFrameRxTimeout.Init( MILLI_TO_TICKS(RX_PULL_PERIOD_MS) );
	m_toLinkStatusTimeout.Init( MILLI_TO_TICKS(LINK_CHECK_PERIOD_MS) );
	Clear();
	m_eInitFSM = INIT_HardReset;
	ContinueNow();
}

void CSPIDeviceENC28J60::SetMacAddress(const mac_addr_t* MacAaddress /*= NULL*/)
{
	if (MacAaddress) {
		m_TxHeaderBroadcast.SourceMacAddr = *MacAaddress;
	} else {
		uint32_t uiChipID = (CHW_MCU::GetUniqueId().u[0]
					^ CHW_MCU::GetUniqueId().u[1]
					^ CHW_MCU::GetUniqueId().u[2]
	    );
		m_TxHeaderBroadcast.SourceMacAddr.b1 = 0;
		m_TxHeaderBroadcast.SourceMacAddr.b2 = 2;
		m_TxHeaderBroadcast.SourceMacAddr.b3 = uiChipID;
		m_TxHeaderBroadcast.SourceMacAddr.b4 = uiChipID>>8;
		m_TxHeaderBroadcast.SourceMacAddr.b5 = uiChipID>>16;
		m_TxHeaderBroadcast.SourceMacAddr.b6 = uiChipID>>24;
	}
	m_TxHeaderTargeted.SourceMacAddr = m_TxHeaderBroadcast.SourceMacAddr;
}
const CSPIDeviceENC28J60::mac_addr_t* CSPIDeviceENC28J60::GetMacAddress() const
{
	return &m_TxHeaderBroadcast.SourceMacAddr;
}

void CSPIDeviceENC28J60::Clear()
{
	m_eState = STATE_INIT;
//m_eState = STATE_TEST; // TODO remove test
	m_eTestFSM = TEST_Start;
	m_eHWResetFSM = HWRESET_Activate;
	m_eSelectBankFSM = SELECTBANK_Idle;
	m_ePHYReadFSM = READPHY_Idle;
	m_ePHYWriteFSM = WRITEPHY_Idle;
	m_eRead16FSM = READ16_Idle;
	m_eWrite16FSM = WRITE16_Idle;
	m_eInitFSM = INIT_Start;
	m_eFrameTxFSM = FRAMETX_Idle;
	m_eFrameRxFSM = FRAMERX_Idle;

	m_bBusy = false;
	m_nPHYTries = 0;
	m_bError = false;

	m_uiBankSelected = 0;
	m_uiBankToSelect = 0;

	m_uiPhyAddrToRead = 0;
	m_uiPhyAddrToWrite = 0;
	m_uiPhyDataToWrite = 0;

	m_uiAddrToRead16 = 0;
	m_uiDataRead16 = 0;
	m_uiAddrToWrite16 = 0;
	m_uiDataToWrite16 = 0;
	m_uiData16VerifyMask = 0;

	m_RxPktHeader.uiSavedRXRDPT = 0;

	m_bInterruptRequested = true;
	m_bResetRequested = false;
	m_bFrameRxRequested = true;
	m_toFrameRxTimeout.SetElapsed();
	m_bLinkStatusRequested = true;
	m_toLinkStatusTimeout.SetElapsed();
	m_bFrameTxRequested = false;
	m_toFrameTxTimeout.SetElapsed();
	m_uiFrameRxedSize = 0;
}

void CSPIDeviceENC28J60::SingleStep(CTimeType uiTime)
{
	bool bAgain;

	if(m_bBusy) return;

	do {
		bAgain = false;

		if (m_bError) {
			m_bError = false;
			PARANOID_ERRORS("SPI ERROR");
			ENC28J60_DEBUG_INFO("Error");
			m_eState = STATE_ERROR;
		} else if (m_bResetRequested) {
			PARANOID_ERRORS("SPI RESET");
			ENC28J60_DEBUG_INFO("RESET required");
			m_eState = STATE_RESET;
		}

		switch (m_eState) {
		case STATE_IDLE:
			if (m_bFrameTxRequested) {
				ASSERTE(m_eFrameTxFSM == FRAMETX_Idle);
				m_eFrameTxFSM = FRAMETX_Start;
				m_eState = STATE_TX;
				bAgain = true;
			} else if (m_bFrameRxRequested && !m_uiFrameRxedSize) {
				m_bFrameRxRequested = false;
				ASSERTE(m_eFrameRxFSM == FRAMERX_Idle);
				m_eFrameRxFSM = FRAMERX_Start;
				m_eState = STATE_RX;
				bAgain = true;
			} else if (m_bLinkStatusRequested) {
				m_bLinkStatusRequested = false;
				ASSERTE(m_eLinkStatusFSM == LINKSTAT_Idle);
				m_eLinkStatusFSM = LINKSTAT_Start;
				m_eState = STATE_LINK;
				bAgain = true;
			} else if (m_toFrameRxTimeout.GetIsElapsed()) {
				m_bFrameRxRequested = true;
				m_toFrameRxTimeout.Start();
				bAgain = true;
			} else if (m_toLinkStatusTimeout.GetIsElapsed()) {
				m_bLinkStatusRequested = true;
				bAgain = true;
			} else if (m_bInterruptRequested || !m_pInterruptDI->GetValue()) {
				m_bInterruptRequested = false;
				ASSERTE(m_eInterruptFSM == INTERRUPT_Idle);
				m_eInterruptFSM = INTERRUPT_Start;
				m_eState = STATE_INTERRUPT;
				bAgain = true;
			} else {
				ContinueDelay(m_toFrameRxTimeout.GetTimeLeft() + 1);
			}
			break;
		case STATE_TEST:
			if (ExecuteTestFSM() == ASYNCOP_BUSY) return;
			ASSERT("Test should not stop");
			break;
		case STATE_INIT:
			if (ExecuteInitFSM() == ASYNCOP_BUSY) return;
			m_eState = STATE_IDLE;
			bAgain = true;
			break;
		case STATE_RX:
			if (ExecuteFrameRxFSM() == ASYNCOP_BUSY) return;
			if (m_uiFrameRxedSize > ETHERNET_HEADER_LEN) {
				OnPktReceived();
			} else {
				OnIncomeCheck();
				m_eState = STATE_IDLE;
			}
			bAgain = true;
			break;
		case STATE_TX:
			if (ExecuteFrameTxFSM() == ASYNCOP_BUSY) return;
			ENC28J60_DEBUG_INFO("TX FRAME DONE");
			OnPktSent();
			m_bFrameTxRequested = false;
			m_eState = STATE_IDLE;
			bAgain = true;
			break;
		case STATE_LINK:
			if (ExecuteLinkStatusFSM() == ASYNCOP_BUSY) return;
			if (m_bReportedLinkStatus != m_bLinkStatus) {
				OnLinkChanged(m_bLinkStatus);
				m_bReportedLinkStatus = m_bLinkStatus;
			}
			m_eState = STATE_IDLE;
			m_toLinkStatusTimeout.Start();
			bAgain = true;
			break;
		case STATE_INTERRUPT:
			if (ExecuteInterruptFSM() == ASYNCOP_BUSY) return;
			m_eState = STATE_IDLE;
			bAgain = true;
			break;
		case STATE_ERROR:
			PARANOID_ERRORS("ERROR State");
			if (m_bFrameTxRequested) {
				OnPktError();
				m_bFrameTxRequested = false;
			}
			// no break, keep going
		case STATE_RESET:
			m_bResetRequested = false;
			m_eInitFSM = INIT_HardReset;
			m_eState = STATE_INIT;
			if (ExecuteInitFSM() == ASYNCOP_BUSY) return;
			bAgain = true;
			break;
		}
	} while (bAgain);
};

// IDeviceSPI
/*virtual*/
bool CSPIDeviceENC28J60::GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::GetSPIParams(uiMaximumBitrate, eMode, eBitsOrder, nBitsCount));

	bool bResult = false;
	if (uiMaximumBitrate != SPI_SPEED) {
		uiMaximumBitrate = SPI_SPEED;
		bResult = true;
	}
	if (eMode != ISPI::MODE0 && eMode != ISPI::MODE3) {
		eMode = ISPI::MODE0;
		bResult = true;
	}
	if (eBitsOrder != ISPI::MSB_1st) {
		eBitsOrder = ISPI::MSB_1st;
		bResult = true;
	}
	if (nBitsCount != 8) {
		nBitsCount = 8;
		bResult = true;
	}
	return bResult;
}
void CSPIDeviceENC28J60::Select()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Select());
	if (m_pSelectDO) m_pSelectDO->Reset();
	/* ENC28J60_DEBUG_INFO("Selected"); */
}
void CSPIDeviceENC28J60::Release()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Release());
	/* ENC28J60_DEBUG_INFO("Released"); */
	if (m_pSelectDO) m_pSelectDO->Set();
}
void CSPIDeviceENC28J60::OnSPIDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIDone());
	m_bBusy = false;
	ContinueNow();
}
void CSPIDeviceENC28J60::OnSPIError()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIError());
	m_bError = true;
	m_bBusy = false;
	ContinueNow();
}
void CSPIDeviceENC28J60::OnSPIIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIIdle());
	m_bError = true;
	m_bBusy = false;
	ContinueNow();
}

#define START_SPI_SESSION(eState, NextState) { \
		m_bBusy = true; \
		ASSERTE(m_pSPI); \
		if (m_pSPI->Start_SPIx_Session(this, &(m_aSpiTxChunks[0]), &(m_aSpiRxChunks[0]))) { \
			/* ENC28J60_DEBUG_INFO("SPI op started ->" #NextState);*/ \
			eState = NextState; \
		} else { \
			m_bBusy = false; \
			ENC28J60_DEBUG_INFO("SPI op postponed"); \
			ContinueOnSPIIdle(); \
		} \
}

#define StartSPI_RWCMD(uiTxCmdSize, uiRxCmdSize, bSkipDummy, eState, NextState) { \
		ASSERTE(!m_bBusy); \
		ASSERTE((uiTxCmdSize) + (uiRxCmdSize) <= ARRAY_SIZE(m_SpiTxCmd)); \
		ASSERTE((uiTxCmdSize) + (uiRxCmdSize) <= ARRAY_SIZE(m_SpiRxTempBuf)); \
		m_aSpiTxChunks[0].pData = &(m_SpiTxCmd[0]); \
		if (bSkipDummy) { \
			m_aSpiRxChunks[0].pData    = &(m_SpiRxTempBuf[ARRAY_SIZE(m_SpiRxTempBuf) - (uiTxCmdSize) - (uiRxCmdSize) - 1]); \
			m_aSpiTxChunks[0].uiLength = m_aSpiRxChunks[0].uiLength = (uiTxCmdSize) + (uiRxCmdSize) + 1; \
		} else { \
			m_aSpiRxChunks[0].pData    = &(m_SpiRxTempBuf[ARRAY_SIZE(m_SpiRxTempBuf) - (uiTxCmdSize) - (uiRxCmdSize)]); \
			m_aSpiTxChunks[0].uiLength = m_aSpiRxChunks[0].uiLength = (uiTxCmdSize) + (uiRxCmdSize); \
		} \
		m_aSpiTxChunks[0].Break(); \
		m_aSpiRxChunks[0].Break(); \
		START_SPI_SESSION(eState, NextState); \
}
#define RWCMD_READ_VALUE ( m_SpiRxTempBuf[ARRAY_SIZE(m_SpiRxTempBuf) - 1] )

static const uint8_t g_ENC28J60_SPI_RBM = ENC28J60_SPI_RBM;
static const uint8_t g_ENC28J60_SPI_WBM = ENC28J60_SPI_WBM;

#define StartReadChainedChunks(pHeadChunk, eState, NextState) { \
		ASSERTE(!m_bBusy); \
		m_aSpiTxChunks[0].Assign( &g_ENC28J60_SPI_RBM, sizeof(g_ENC28J60_SPI_RBM) ); \
		m_aSpiTxChunks[0].Break(); \
		m_aSpiRxChunks[0].Clean(); \
		m_aSpiRxChunks[0].Append(pHeadChunk); \
		START_SPI_SESSION(eState, NextState); \
}

#define StartReadBuffer(pBufferRx, uiRxLen, eState, NextState) { \
		ASSERTE(!m_bBusy); \
		ASSERTE(pBufferRx); \
		ASSERTE(uiRxLen); \
		m_aSpiRxChunks[1].Assign( (pBufferRx), (uiRxLen) ); \
		StartReadChainedChunks(&(m_aSpiRxChunks[1]), eState, NextState); \
}

#define StartWriteChainedChunks(pHeadChunk, eState, NextState) { \
		ASSERTE(!m_bBusy); \
		m_aSpiTxChunks[0].Assign( &g_ENC28J60_SPI_WBM, sizeof(g_ENC28J60_SPI_WBM) ); \
		m_aSpiTxChunks[0].Append(pHeadChunk); \
		m_aSpiRxChunks[0].Clean(); \
		m_aSpiRxChunks[0].Break(); \
		START_SPI_SESSION(eState, NextState); \
}

#define StartWriteBuffer(pTxBuffer, uiTxLen, eState, NextState) { \
		ASSERTE(!m_bBusy); \
		ASSERTE(pTxBuffer); \
		ASSERTE(uiTxLen); \
		m_aSpiTxChunks[1].Assign( (pTxBuffer), (uiTxLen) ); \
		StartWriteChainedChunks(&(m_aSpiTxChunks[1]), eState, NextState); \
}


#define StartCmdRead(uiOpCode, uiRegAddr, eState, NextState) { \
		ASSERTE(ENC28J60_IS_OPCODE(uiOpCode)); \
		m_SpiTxCmd[0] = (uiOpCode) | ((uiRegAddr) & ENC28J60_ADDR_MASK); \
		bool bSkipDummy = ((uiRegAddr) & 0x80); \
		/* if (bSkipDummy) { ENC28J60_DEBUG_INFO("Read (skip) from", m_SpiTxCmd[0]); } else { ENC28J60_DEBUG_INFO("Read from", m_SpiTxCmd[0]); } */ \
		StartSPI_RWCMD(1, 1, bSkipDummy, eState, NextState); \
}
#define GetCmdReadValue() ( /* ENC28J60_DEBUG_INFO("Read val", RWCMD_READ_VALUE ),*/ RWCMD_READ_VALUE )

#define StartCmdWrite(uiOpCode, uiRegAddr, uiRegValue, eState, NextState) { \
		ASSERTE(ENC28J60_IS_OPCODE(uiOpCode)); \
		m_SpiTxCmd[0] = (uiOpCode) | ((uiRegAddr) & ENC28J60_ADDR_MASK); \
		m_SpiTxCmd[1] = (uiRegValue); \
		/* ENC28J60_DEBUG_INFO("Write to", m_SpiTxCmd[0]); ENC28J60_DEBUG_INFO("Write val", m_SpiTxCmd[1]); */ \
		StartSPI_RWCMD(2, 0, false, eState, NextState); \
}

#define StartSoftReset(eState, NextState) { \
		m_SpiTxCmd[0] = ENC28J60_SPI_SC; \
		StartSPI_RWCMD(1, 0, false, eState, NextState); \
		m_uiRevision = 0; \
		m_uiBankSelected = 0; \
		m_uiBankToSelect = 0; \
		m_uiPhyAddrToRead = 0; \
		m_uiPhyAddrToWrite = 0; \
}

#define StartHardReset(eState, NextState) { \
		if (ProcessFSM_HardReset()) { \
			eState = NextState; \
			ContinueNow(); \
		} \
}

#define StartRCR(uiAddr, eState, NextState) { \
		if (DoSetBank(uiAddr) != ASYNCOP_BUSY) { \
			StartCmdRead(ENC28J60_SPI_RCR, uiAddr, eState, NextState); \
		} \
}
#define GetRCRValue() ( GetCmdReadValue() )

#define StartRCR16(uiAddr, eState, NextState) { \
		if (m_eRead16FSM == READ16_Idle) { \
			ENC28J60_DEBUG_INFO("READ16: Read", uiAddr); \
			m_uiAddrToRead16 = uiAddr; \
			m_eRead16FSM = READ16_Start; \
		} else { \
			ASSERTE(m_uiAddrToRead16 == uiAddr); \
		} \
		if (ExecuteRead16FSM() != ASYNCOP_BUSY) { \
			eState = NextState; \
			ContinueNow(); \
		} \
}
#define GetRCR16Value() ( m_uiDataRead16 )

#define StartWCR(uiAddr, uiValue, eState, NextState) { \
		if (DoSetBank(uiAddr) != ASYNCOP_BUSY) { \
			StartCmdWrite(ENC28J60_SPI_WCR, uiAddr, uiValue, eState, NextState); \
		} \
}
#define StartWCR16(uiAddr, uiValue, uiVerifyMask, eState, NextState) { \
		if (m_eWrite16FSM == WRITE16_Idle) { \
			/*ENC28J60_DEBUG_INFO("WRITE16: Start", (((uiAddr) << 16) | (uiValue)));*/ \
			m_uiAddrToWrite16 = (uiAddr); \
			m_uiDataToWrite16 = (uiValue); \
			m_uiData16VerifyMask = uiVerifyMask; \
			m_eWrite16FSM = WRITE16_Start; \
			m_nWrite16Tries = WRITE16_RETRIES; \
		} else { \
			ASSERTE(m_uiAddrToWrite16 == (uiAddr)); \
			ASSERTE(m_uiDataToWrite16 == (uiValue)); \
		} \
		if (ExecuteWrite16FSM() != ASYNCOP_BUSY) { \
			eState = NextState; \
			ContinueNow(); \
		} \
}

#define StartSelectBank(uiBank, DoneCode) { \
		if (m_eSelectBankFSM == SELECTBANK_Idle) { \
			m_uiBankToSelect = uiBank; \
			m_eSelectBankFSM = SELECTBANK_Start; \
			m_nBankSelectTries = BANK_SELECT_RETRIES; \
		} else { \
			ASSERTE(m_uiBankToSelect == uiBank); \
		} \
		if (ExecuteSelectBankFSM() != ASYNCOP_BUSY) { \
			DoneCode; \
		} \
}
#define CheckSelectBank(uiBank, DoneCode) { \
		if (m_eSelectBankFSM == SELECTBANK_Idle) { \
			m_uiBankToSelect = uiBank; \
			m_eSelectBankFSM = SELECTBANK_Check; \
			m_nBankSelectTries = BANK_SELECT_RETRIES; \
		} else { \
			ASSERTE(m_uiBankToSelect == uiBank); \
		} \
		if (ExecuteSelectBankFSM() != ASYNCOP_BUSY) { \
			DoneCode; \
		} \
}
#define StartBFC(uiAddr, uiMask, eState, NextState) { \
		if (DoSetBank(uiAddr) != ASYNCOP_BUSY) {; \
			StartCmdWrite(ENC28J60_SPI_BFC, uiAddr, uiMask, eState, NextState); \
		} \
}

#define StartBFS(uiAddr, uiMask, eState, NextState) { \
		if (DoSetBank(uiAddr) != ASYNCOP_BUSY) {; \
			StartCmdWrite(ENC28J60_SPI_BFS, uiAddr, uiMask, eState, NextState); \
		} \
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteHardResetFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eHWResetFSM) {
	case HWRESET_Activate:
		DEBUG_INFO("LAN: HARD RESET");
		if (m_pResetDO) m_pResetDO->Reset();
		m_eHWResetFSM = HWRESET_Deactivate;
		ContinueDelay(HARD_RESET_DURATION);
		break;
	case HWRESET_Deactivate:
		if (m_pResetDO) m_pResetDO->Set();
		m_eHWResetFSM = HWRESET_Done;
		ContinueDelay(HARD_RESET_DELAY);
		break;
	case HWRESET_Done:
		m_eHWResetFSM = HWRESET_Activate; // reset again
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteSelectBankFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eSelectBankFSM) {
	case SELECTBANK_Idle:
		return ASYNCOP_DONE;
	case SELECTBANK_Write1:
//		ENC28J60_DEBUG_INFO("Bank to select", m_uiBankToSelect);
		StartCmdWrite(ENC28J60_SPI_BFC, ECON1, 0x03, m_eSelectBankFSM, SELECTBANK_Write2);
		break;
	case SELECTBANK_Write2:
		StartCmdWrite(ENC28J60_SPI_BFS, ECON1, m_uiBankToSelect, m_eSelectBankFSM, SELECTBANK_Check);
		break;
	case SELECTBANK_Check:
#ifndef VERIFY_SELECTED_BANK
//		ENC28J60_DEBUG_INFO("Bank selected:", m_uiBankSelected);
		m_uiBankSelected = m_uiBankToSelect;
		m_eSelectBankFSM = SELECTBANK_Idle;
		return ASYNCOP_DONE;
#else
		StartCmdRead(ENC28J60_SPI_RCR, ECON1, m_eSelectBankFSM, SELECTBANK_Done);
		break;
#endif
	case SELECTBANK_Done:
		m_uiBankSelected = GetCmdReadValue() & 0x03;
		if (m_uiBankSelected == m_uiBankToSelect) {
			ENC28J60_DEBUG_INFO("Bank selected:", m_uiBankSelected);
			m_eSelectBankFSM = SELECTBANK_Idle;
			return ASYNCOP_DONE;
		} else if (--m_nBankSelectTries > 0) {
			PARANOID_ERRORS("Bank select retries");
			ENC28J60_DEBUG_INFO("Bank reselect:", m_uiBankSelected);
			m_eSelectBankFSM = SELECTBANK_Write1;
			ContinueNow();
		} else {
			PEDANTIC_ERRORS("Bank select failed");
			m_eState = STATE_ERROR;
			ContinueNow();
		}
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecutePHYReadFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_ePHYReadFSM) {
	case READPHY_Idle:
		return ASYNCOP_DONE;
	case READPHY_WriteAddr:
		StartWCR(MIREGADR, m_uiPhyAddrToRead, m_ePHYReadFSM, READPHY_SetCmdBit);
		break;
	case READPHY_SetCmdBit:
		m_nPHYTries = PHY_WAIT_RETRIES;
		StartBFS(MICMD, MICMD_MIIRD, m_ePHYReadFSM, READPHY_EnterWaitBusy);
		break;
	case READPHY_EnterWaitBusy:
		ENC28J60_DEBUG_INFO("PHY: read waiting. Addr:", m_uiPhyAddrToRead);
		m_ePHYReadFSM = READPHY_ReadBusy;
		// no break; keep going
	case READPHY_ReadBusy:
		StartRCR(MISTAT, m_ePHYReadFSM, READPHY_WaitBusy);
		break;
	case READPHY_WaitBusy:
		if (GetRCRValue() & MISTAT_BUSY) {
			m_ePHYReadFSM = READPHY_ReadBusy;
			if (m_nPHYTries) {
				--m_nPHYTries;
				ContinueDelay(PHY_WAIT_DELAY); // todo tune the time for phy read
			} else {
				PEDANTIC_ERRORS("READPHY Wait TIMEOUT");
				m_eState = STATE_ERROR;
				ContinueNow();
			}
			break;
		} else {
			ENC28J60_DEBUG_INFO("PHY: read waiting done");
			m_ePHYReadFSM = READPHY_ClearCmdBit;
		}
		// no break, keep going;
	case READPHY_ClearCmdBit:
		StartBFC(MICMD, MICMD_MIIRD, m_ePHYReadFSM, READPHY_ReadDataLo);
		break;
	case READPHY_ReadDataLo:
		m_uiPhyReadData = 0;
		StartRCR(MIRDL, m_ePHYReadFSM, READPHY_ReadDataHi);
		break;
	case READPHY_ReadDataHi:
		m_uiPhyReadData = GetRCRValue();
		StartRCR(MIRDL + 1, m_ePHYReadFSM, READPHY_Done);
		break;
	case READPHY_Done:
		m_uiPhyReadData |= (GetRCRValue() << 8);
		ENC28J60_DEBUG_INFO("PHY: Read data:", m_uiPhyReadData);
		m_ePHYReadFSM = READPHY_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecutePHYWriteFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_ePHYWriteFSM) {
	case WRITEPHY_Idle:
		return ASYNCOP_DONE;
	case WRITEPHY_WriteAddr:
		StartWCR(MIREGADR, m_uiPhyAddrToWrite, m_ePHYWriteFSM, WRITEPHY_WriteDataLo);
		break;
	case WRITEPHY_WriteDataLo:
		StartWCR(MIWR, m_uiPhyDataToWrite & 0xFF, m_ePHYWriteFSM, WRITEPHY_WriteDataHi);
		break;
	case WRITEPHY_WriteDataHi:
		m_nPHYTries = PHY_WAIT_RETRIES;
		StartWCR(MIWR + 1, m_uiPhyDataToWrite >> 8, m_ePHYWriteFSM, WRITEPHY_EnterWaitBusy);
		break;
	case WRITEPHY_EnterWaitBusy:
		ENC28J60_DEBUG_INFO("PHY: write waiting");
		m_ePHYWriteFSM = WRITEPHY_ReadBusy;
		// no break, keep going
	case WRITEPHY_ReadBusy:
		StartRCR(MISTAT, m_ePHYWriteFSM, WRITEPHY_WaitBusy);
		break;
	case WRITEPHY_WaitBusy:
		if (GetRCRValue() & MISTAT_BUSY) {
			m_ePHYWriteFSM = WRITEPHY_ReadBusy;
			if (m_nPHYTries) {
				--m_nPHYTries;
				ContinueDelay(PHY_WAIT_DELAY); // todo tune the time for phy write
			} else {
				PEDANTIC_ERRORS("WRITE PHY Wait TIMEOUT");
				m_eState = STATE_ERROR;
				ContinueNow();
			}
		} else {
			ENC28J60_DEBUG_INFO("PHY: write done");
			m_ePHYWriteFSM = WRITEPHY_Done;
			ContinueNow();
		}
		break;
	case WRITEPHY_Verify:
#ifdef VERIFY_WRITEPHY
		m_uiPhyAddrToRead = m_uiPhyAddrToWrite;
		m_ePHYReadFSM = READPHY_Start;
		m_ePHYWriteFSM = WRITEPHY_Wait_Verify;
		if (ExecutePHYReadFSM() != ASYNCOP_BUSY) {
			ContinueNow();
		}
		break;
#else
		m_ePHYWriteFSM = WRITEPHY_Idle;
		return ASYNCOP_DONE;
#endif
	case WRITEPHY_Wait_Verify:
		if (ExecutePHYReadFSM() == ASYNCOP_BUSY) return ASYNCOP_BUSY;
		if (m_uiPhyReadData != m_uiPhyDataToWrite) {
			PEDANTIC_ERRORS("PHY WRITE failed");
			m_ePHYWriteFSM = WRITEPHY_Idle;
			m_eState = STATE_ERROR;
			ContinueNow();
			break;
		}
		// no break, keep going
	case WRITEPHY_Done:
		m_ePHYWriteFSM = WRITEPHY_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteRead16FSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eRead16FSM) {
	case READ16_Idle:
		return ASYNCOP_DONE;
	case READ16_ReadDataLo:
		StartRCR(m_uiAddrToRead16, m_eRead16FSM, READ16_ReadDataHi);
		break;
	case READ16_ReadDataHi:
		m_uiDataRead16 = GetRCRValue();
//		StartRCR(m_uiAddrToRead16 + 1, m_eRead16FSM, READ16_Done); //
		StartCmdRead(ENC28J60_SPI_RCR, m_uiAddrToRead16 + 1, m_eRead16FSM, READ16_Done);
		break;
	case READ16_Done:
		ENC28J60_DEBUG_INFO("READ16: done");
		m_uiDataRead16 |= (GetCmdReadValue() << 8);
		m_eRead16FSM = READ16_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteWrite16FSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eWrite16FSM) {
	case WRITE16_Idle:
		return ASYNCOP_DONE;
	case WRITE16_WriteDataLo:
		StartWCR(m_uiAddrToWrite16, m_uiDataToWrite16 & 0xFF, m_eWrite16FSM, WRITE16_WriteDataHi);
		break;
	case WRITE16_WriteDataHi:
//		ENC28J60_DEBUG_INFO("WRITE16: Write Hi");
		StartCmdWrite(ENC28J60_SPI_WCR, m_uiAddrToWrite16 + 1, m_uiDataToWrite16 >> 8, m_eWrite16FSM, m_uiData16VerifyMask?WRITE16_ReadDataLo:WRITE16_Done);
		break;
	case WRITE16_ReadDataLo:
#ifdef VERIFY_WRITE16
//		ENC28J60_DEBUG_INFO("WRITE16: Read Lo");
		StartCmdRead(ENC28J60_SPI_RCR, m_uiAddrToWrite16, m_eWrite16FSM, WRITE16_VerifyLo_ReadDataHi);
		break;
#else // ! VERIFY_WRITE16
		m_eWrite16FSM = WRITE16_Idle;
		return ASYNCOP_DONE;
#endif
	case WRITE16_VerifyLo_ReadDataHi:
//		ENC28J60_DEBUG_INFO("WRITE16: Verify Lo", GetCmdReadValue());
		if ((GetCmdReadValue() & m_uiData16VerifyMask) == (m_uiDataToWrite16 & m_uiData16VerifyMask & 0xFF)) {
//			ENC28J60_DEBUG_INFO("WRITE16: Read Hi");
			StartCmdRead(ENC28J60_SPI_RCR, m_uiAddrToWrite16 + 1, m_eWrite16FSM, WRITE16_VerifyHi);
		} else if (--m_nWrite16Tries > 0) {
			PARANOID_ERRORS("WRITE16 Lo failed");
			ENC28J60_DEBUG_INFO("WRITE16: Lo retry", (GetCmdReadValue() << 8) | (m_uiDataToWrite16 & 0xFF));
			m_eWrite16FSM = WRITE16_WriteDataLo;
			ContinueNow();
		} else {
			ENC28J60_DEBUG_INFO("WRITE16: Lo failed", (GetCmdReadValue() << 8) | (m_uiDataToWrite16 & 0xFF));
			PEDANTIC_ERRORS("WRITE16 Lo failed");
			m_eState = STATE_ERROR;
			ContinueNow();
		}
		break;
	case WRITE16_VerifyHi:
//		ENC28J60_DEBUG_INFO("WRITE16: Verify Hi", GetCmdReadValue());
		if ((GetCmdReadValue() & (m_uiData16VerifyMask >> 8)) == (((m_uiDataToWrite16 & m_uiData16VerifyMask) >> 8) & 0xFF)) {
			m_eWrite16FSM = WRITE16_Idle;
			return ASYNCOP_DONE;
		} else if (--m_nWrite16Tries > 0) {
			PARANOID_ERRORS("WRITE16 Hi failed");
			ENC28J60_DEBUG_INFO("WRITE16: Hi retry", (GetCmdReadValue() << 8) | ((m_uiDataToWrite16 >> 8) & 0xFF));
			m_eWrite16FSM = WRITE16_WriteDataHi;
			ContinueNow();
		} else {
			ENC28J60_DEBUG_INFO("WRITE16: Hi failed", (GetCmdReadValue() << 8) | ((m_uiDataToWrite16 >> 8) & 0xFF));
			PEDANTIC_ERRORS("WRITE16 Hi failed");
			m_eState = STATE_ERROR;
			ContinueNow();
		}
		break;
	case WRITE16_Done:
		m_eWrite16FSM = WRITE16_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}


CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteTestFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eTestFSM) {
	case TEST_Write1:
		StartWCR(MACON2, 0x55, m_eTestFSM, TEST_Read1);
		break;
	case TEST_Read1:
		StartCmdRead(ENC28J60_SPI_RCR, MACON2, m_eTestFSM, TEST_Write2);
		break;
	case TEST_Write2:
		StartWCR(MACON2, 0xAA, m_eTestFSM, TEST_Read2);
		break;
	case TEST_Read2:
		StartRCR(MACON2, m_eTestFSM, TEST_Start);
		break;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteInitFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eInitFSM) {
	case INIT_Idle:
		return ASYNCOP_DONE;

	// Soft reset
	case INIT_SoftReset:
		ENC28J60_DEBUG_INFO("Soft RESET");
		StartSoftReset(m_eInitFSM, INIT_Wait_SoftReset);
		break;
	case INIT_Wait_SoftReset:
		Clear();
		m_eInitFSM = INIT_Read_REVID;
		m_nTries = READ_RETRIES;
		ContinueDelay(SOFT_RESET_DELAY);
		break;

	// Configure
	case INIT_Read_REVID:
		StartRCR(EREVID, m_eInitFSM, INIT_Validate_REVID);
		break;

	// Setup Rx/Tx buffer
	case INIT_Validate_REVID:
		m_uiRevision = GetRCRValue();
		if (m_uiRevision) {
			ENC28J60_DEBUG_INFO("Revision:", m_uiRevision);
			m_eInitFSM = INIT_Write_ERXST;
		} else if (--m_nTries > 0) {
			PARANOID_ERRORS("INVALID Revision");
			ENC28J60_DEBUG_INFO("Revision retry:", m_uiRevision);
			m_eInitFSM = INIT_Read_REVID;
		} else {
			PEDANTIC_ERRORS("INVALID Revision");
			m_eState = STATE_ERROR;
		}
		ContinueNow();
		break;

	// Setup Rx/Tx buffer
	case INIT_Write_ERXST:
		StartWCR16(ERXST, ENC28J60_RXSTART, 0x1FFF, m_eInitFSM, INIT_Write_ERXRDPT);
		break;
	case INIT_Write_ERXRDPT:
		m_RxPktHeader.uiSavedRXRDPT = 0;
		StartWCR16(ERXRDPT, ENC28J60_RXSTART, 0x1FFF, m_eInitFSM, INIT_Write_ERXND);
		break;
	case INIT_Write_ERXND:
		StartWCR16(ERXND, ENC28J60_RXEND, 0x1FFF, m_eInitFSM, INIT_Write_MACON2);
		break;

	// Setup MAC
	case INIT_Write_MACON2: // MAC Reset
		StartWCR(MACON2, 0, m_eInitFSM, INIT_Read_MACON2);
		break;
	case INIT_Read_MACON2:
		StartRCR(MACON2, m_eInitFSM, INIT_Verify_MACON2);
		break;
	case INIT_Verify_MACON2: // Enable flow control
		if ((GetRCRValue() & 0xCF) != (0xCF & 0)) {
			PEDANTIC_ERRORS("MACON2 failed");
			m_eState = STATE_ERROR;
			ContinueNow();
			return ASYNCOP_BUSY;
		}
		m_eInitFSM = INIT_Write_MACON1;
		// no break, keep going
	case INIT_Write_MACON1:
		StartWCR(MACON1, MACON1_TXPAUS | MACON1_RXPAUS | MACON1_MARXEN, m_eInitFSM, INIT_Read_MACON1);
		break;
	case INIT_Read_MACON1:
		StartRCR(MACON1, m_eInitFSM, INIT_Verify_MACON1);
		break;
	case INIT_Verify_MACON1:  // Enable crc & frame len chk
		if ((GetRCRValue() & 0x1F) != (0x1F & (MACON1_TXPAUS | MACON1_RXPAUS | MACON1_MARXEN))) {
			PEDANTIC_ERRORS("MACON1 failed");
			m_eState = STATE_ERROR;
			ContinueNow();
			return ASYNCOP_BUSY;
		}
		m_eInitFSM = INIT_Write_MACON3;
		// no break, keep going
	case INIT_Write_MACON3:
		StartWCR(MACON3, MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN | MACON3_FULDPX, m_eInitFSM, INIT_Read_MACON3);
		break;
	case INIT_Read_MACON3:
		StartRCR(MACON3, m_eInitFSM, INIT_Verify_MACON3);
		break;
	case INIT_Verify_MACON3:
		if ((GetRCRValue() & 0xFF) != (0xFF & (MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN | MACON3_FULDPX))) {
			ENC28J60_DEBUG_INFO("MACON3 error:", (GetRCRValue() << 16) | (MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN | MACON3_FULDPX) );
			PEDANTIC_ERRORS("MACON3 failed");
			m_eState = STATE_ERROR;
			ContinueNow();
			return ASYNCOP_BUSY;
		}
		m_eInitFSM = INIT_Write_MAMXFL;
		// no break, keep going
	case INIT_Write_MAMXFL:
		StartWCR16(MAMXFL, ENC28J60_MAXFRAME + 16, 0xFFFF, m_eInitFSM, INIT_Write_MABBIPG);
		break;
	case INIT_Write_MABBIPG:
		StartWCR(MABBIPG, 0x15, m_eInitFSM, INIT_Write_MAIPGL);
		break;
	case INIT_Write_MAIPGL:
		StartWCR(MAIPGL, 0x12, m_eInitFSM, INIT_Write_MAIPGH);
		break;
	case INIT_Write_MAIPGH:
		StartWCR(MAIPGH, 0x0C, m_eInitFSM, INIT_Write_MAADR5);
		break;
	case INIT_Write_MAADR5: // Set MAC address
		StartWCR(MAADR5, m_TxHeaderBroadcast.SourceMacAddr.b1, m_eInitFSM, INIT_Write_MAADR4);
		break;
	case INIT_Write_MAADR4:
		StartWCR(MAADR4, m_TxHeaderBroadcast.SourceMacAddr.b2, m_eInitFSM, INIT_Write_MAADR3);
		break;
	case INIT_Write_MAADR3:
		StartWCR(MAADR3, m_TxHeaderBroadcast.SourceMacAddr.b3, m_eInitFSM, INIT_Write_MAADR2);
		break;
	case INIT_Write_MAADR2:
		StartWCR(MAADR2, m_TxHeaderBroadcast.SourceMacAddr.b4, m_eInitFSM, INIT_Write_MAADR1);
		break;
	case INIT_Write_MAADR1:
		StartWCR(MAADR1, m_TxHeaderBroadcast.SourceMacAddr.b5, m_eInitFSM, INIT_Write_MAADR0);
		break;
	case INIT_Write_MAADR0:
		StartWCR(MAADR0, m_TxHeaderBroadcast.SourceMacAddr.b6, m_eInitFSM, INIT_Write_PHCON1);
		break;

	// Setup PHY
	case INIT_Write_PHCON1:
		// Force full-duplex mode
		m_uiPhyAddrToWrite = PHCON1;
		m_uiPhyDataToWrite = PHCON1_INIT_VALUE;
		m_uiPhyVerifyMask = 0xCD80;
		m_ePHYWriteFSM = WRITEPHY_Start;
		m_eInitFSM = INIT_Write_PHCON2;
		if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) {
			ContinueNow();
		}
		break;
	case INIT_Write_PHCON2:
		if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) {
			// Disable loopback
			m_uiPhyAddrToWrite = PHCON2;
			m_uiPhyDataToWrite = PHCON2_INIT_VALUE;
			m_uiPhyVerifyMask = 0x7FFF;
			m_ePHYWriteFSM = WRITEPHY_Start;
			m_eInitFSM = INIT_Write_PHLCON;
			if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) {
				ContinueNow();
			}
		}
		break;
	case INIT_Write_PHLCON:
		if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) {
			// Configure LED ctrl
			m_uiPhyAddrToWrite = PHLCON;
			m_uiPhyDataToWrite = PHLCON_INIT_VALUE;
			m_uiPhyVerifyMask = 0xFFFF;
			m_ePHYWriteFSM = WRITEPHY_Start;
			m_eInitFSM = INIT_Write_EIE;
			if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) {
				ContinueNow();
			}
		}
		break;

	// INTERRUPT
	case INIT_Write_EIE:
		if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) { // finish INIT_Write_PHLCON
			StartWCR(EIE, EIE_INIT_VALUE, m_eInitFSM, INIT_Read_EIE);
		}
		break;
	case INIT_Read_EIE:
		StartRCR(EIE, m_eInitFSM, INIT_Verify_EIE);
		break;
	case INIT_Verify_EIE:
		if ((GetRCRValue() & 0xFF) != (0xFF & EIE_INIT_VALUE)) {
			PEDANTIC_ERRORS("EIE failed");
			m_eState = STATE_ERROR;
			ContinueNow();
			return ASYNCOP_BUSY;
		}
		m_eInitFSM = INIT_Write_PHIE;
		// no break, keep going
	case INIT_Write_PHIE:
		m_uiPhyAddrToWrite = PHIE;
		m_uiPhyDataToWrite = PHIE_INIT_VALUE;
		m_uiPhyVerifyMask = 0x0012;
		m_ePHYWriteFSM = WRITEPHY_Start;
		m_eInitFSM = INIT_Write_EnableRx;
		if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) {
			ContinueNow();
		}
		break;

	case INIT_Write_EnableRx:
		if (ExecutePHYWriteFSM() != ASYNCOP_BUSY) { // finish INIT_Write_PHIE
			StartBFS(ECON1, ECON1_RXEN, m_eInitFSM, INIT_Done);
		}
		break;

	case INIT_Done:
		m_eInitFSM = INIT_Idle;
		return ASYNCOP_DONE;

	// Reset
	case INIT_HardReset:
		if (ExecuteHardResetFSM() == ASYNCOP_BUSY) return ASYNCOP_BUSY;
		Clear();
		ContinueNow();
		break;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteFrameTxFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eFrameTxFSM) {
	case FRAMETX_Idle:
		return ASYNCOP_DONE;

	// Configure
	case FRAMETX_Read_ECON1:
		StartRCR(ECON1, m_eFrameTxFSM, FRAMETX_Wait_ECON1_TXRTS);
		break;
	case FRAMETX_Wait_ECON1_TXRTS:
		if ((GetRCRValue() & ECON1_TXRTS)) {
			// not ready. check error flag
			m_eFrameTxFSM = FRAMETX_Read_EIR;
			ContinueNow();
			break;
		}
		// ready, Tx
		m_eFrameTxFSM = FRAMETX_Write_EWRPT;
		// no break, keep going
	case FRAMETX_Write_EWRPT:
		StartWCR16(EWRPT, ENC28J60_TXSTART, 0x1FFF, m_eFrameTxFSM, FRAMETX_Write_DataChain);
		break;
	case FRAMETX_Write_DataChain:
		StartWriteChainedChunks(&m_FrameTxHeadChunk, m_eFrameTxFSM, FRAMETX_Write_ETXST);
		break;
	case FRAMETX_Write_ETXST:
		StartWCR16(ETXST, ENC28J60_TXSTART, 0x1FFF, m_eFrameTxFSM, FRAMETX_Write_ETXND);
		break;
	case FRAMETX_Write_ETXND:
		StartWCR16(ETXND, ENC28J60_TXSTART + m_uiFrameTxCollectedSize - 1, 0x1FFF, m_eFrameTxFSM, FRAMETX_Set_ECON1_TXRTS);
		break;
	case FRAMETX_Set_ECON1_TXRTS:
		StartBFS(ECON1, ECON1_TXRTS, m_eFrameTxFSM, FRAMETX_Done);
		break;

	case FRAMETX_Read_EIR: // Check Tx Error
		StartRCR(EIR, m_eFrameTxFSM, FRAMETX_Wait_EIR);
		break;
	case FRAMETX_Wait_EIR:
		if (!(GetRCRValue() & EIR_TXERIF)) {
			// not ready without error. wait till previous tx complete
			m_eFrameTxFSM = FRAMETX_Read_ECON1; // TXRTS loop
			ContinueDelay(TX_BUSY_DELAY);
		}
		// not ready with error flag set. reset Tx
		m_eFrameTxFSM = FRAMETX_Set_ECON1_TXRST;
		// no break, keep going
	case FRAMETX_Set_ECON1_TXRST:
		StartBFS(ECON1, ECON1_TXRST, m_eFrameTxFSM, FRAMETX_Clear_ECON1_TXRST);
		break;
	case FRAMETX_Clear_ECON1_TXRST: // not ready with error flag set. finish Tx reset
		StartBFC(ECON1, ECON1_TXRST, m_eFrameTxFSM, FRAMETX_Read_ECON1); // TXRTS loop
		break;

	case FRAMETX_Done:
		m_eFrameTxFSM = FRAMETX_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteFrameRxFSM()
{
	ASSERTE(!m_bBusy);
	ASSERTE(!m_bError);

	switch (m_eFrameRxFSM) {
	case FRAMERX_Idle:
		return ASYNCOP_DONE;

	case FRAMERX_Read_EPKTCNT:
		ASSERTE(m_uiFrameRxedSize == 0);
		StartRCR(EPKTCNT, m_eFrameRxFSM, FRAMERX_Check_EPKTCNT);
		break;
	case FRAMERX_Check_EPKTCNT:
		if (GetRCRValue() == 0) {
//			ENC28J60_DEBUG_INFO("No packets Rxed:", GetRCRValue());
			// no packets present. skip Rx
			m_eFrameRxFSM = FRAMERX_Idle;
			return ASYNCOP_DONE;
			ContinueNow();
			break;
		} else if (GetRCRValue() > 100) {
			PARANOID_ERRORS("too many rxed pkts");
			DEBUG_INFO("LAN: too many rxed pkts:", GetRCRValue());
			m_eState = STATE_ERROR;
			ContinueNow();
			break;
		}
		ENC28J60_DEBUG_INFO("Sone packets Rxed. N:", GetRCRValue());
		m_eFrameRxFSM = FRAMERX_Write_ERDPT;
		// no break, keep going
	case FRAMERX_Write_ERDPT:
		StartWCR16(ERDPT, m_RxPktHeader.uiSavedRXRDPT, 0x1FFF, m_eFrameRxFSM, FRAMERX_Read_PktHeader);
		break;
	case FRAMERX_Read_PktHeader:
		StartReadBuffer((volatile uint8_t*)&m_RxPktHeader, sizeof(m_RxPktHeader), m_eFrameRxFSM, FRAMERX_Read_Data);
		break;
	case FRAMERX_Read_Data:
		if (m_RxPktHeader.bOK && m_RxPktHeader.uiRcvBytesCount > ETHERNET_HEADER_LEN + ETHERNET_CRC_LEN) { // received OK
			ENC28J60_DEBUG_INFO("Rx data. size:", m_RxPktHeader.uiRcvBytesCount - ETHERNET_CRC_LEN);
			m_uiFrameRxedSize = MIN2((unsigned)(m_RxPktHeader.uiRcvBytesCount - ETHERNET_CRC_LEN), sizeof(m_FrameRxBuf));
			StartReadBuffer(&(m_FrameRxBuf[0]), m_uiFrameRxedSize, m_eFrameRxFSM, FRAMERX_Write_ERXRDPT);
		} else {
			m_eFrameRxFSM = FRAMERX_Write_ERXRDPT;
			ContinueNow();
		}
		break;
	case FRAMERX_Write_ERXRDPT:
		// chip bug workaround - value to write in ERXRDPT MUST BE ODD. so decrement by 1
		StartWCR16(ERXRDPT, (m_RxPktHeader.uiSavedRXRDPT - 1) & ENC28J60_BUFEND, 0x1FFF, m_eFrameRxFSM, FRAMERX_Set_PKTDEC);
		break;

	case FRAMERX_Set_PKTDEC:
		StartBFS(ECON2, ECON2_PKTDEC, m_eFrameRxFSM, FRAMERX_Wait_Done);
		break;

	case FRAMERX_Wait_Done:
		ENC28J60_DEBUG_INFO("Pkt Rx Done. size:", m_uiFrameRxedSize);
		m_eFrameRxFSM = FRAMERX_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteLinkStatusFSM()
{
	switch (m_eLinkStatusFSM) {

	case LINKSTAT_Idle:
		return ASYNCOP_DONE;

	case LINKSTAT_Read_PHSTAT:
		m_uiPhyAddrToRead = PHSTAT2;
		m_ePHYReadFSM = READPHY_Start;
		m_eLinkStatusFSM = LINKSTAT_Wait_PHSTAT;
		if (ExecutePHYReadFSM() != ASYNCOP_BUSY) {
			ContinueNow();
		}
		break;
	case LINKSTAT_Wait_PHSTAT:
		if (ExecutePHYReadFSM() == ASYNCOP_BUSY) return ASYNCOP_BUSY;
		ENC28J60_DEBUG_INFO("Link StatusL PHSTAT2:", m_uiPhyReadData);
		m_bLinkStatus = (m_uiPhyReadData & PHSTAT2_LSTAT);

		m_eLinkStatusFSM = LINKSTAT_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::ExecuteInterruptFSM()
{
	switch (m_eInterruptFSM) {

	case INTERRUPT_Idle:
		return ASYNCOP_DONE;

	case INTERRUPT_Read_EIR:
		StartRCR(EIR, m_eInterruptFSM, INTERRUPT_Wait_EIR);
		break;
	case INTERRUPT_Wait_EIR:
		ENC28J60_DEBUG_INFO("Interrupt: EIR:", GetRCRValue());
		if (!(GetRCRValue() & EIR_LINKIF)) {
			// PKTIF - Receive buffer contains one or more unprocessed packets; cleared when PKTDEC is set
			// ERRATA - PKTIF does not reliably/accurately report the status of pending packets
			// Work around - If polling to see if a packet is pending, check the value in EPKTCNT
			m_bFrameRxRequested = true;
			m_eInterruptFSM = INTERRUPT_Idle;
			return ASYNCOP_DONE;
		} else {
			// PHY reports that the link status has changed; read PHIR register to clear
			m_bLinkStatusRequested = true;
			m_eInterruptFSM = INTERRUPT_Read_PHIR;
		}
		// no break, keep going
	case INTERRUPT_Read_PHIR:
		// clear LINK interrupt request
		m_uiPhyAddrToRead = PHIR;
		m_ePHYReadFSM = READPHY_Start;
		m_eInterruptFSM = INTERRUPT_Wait_PHIR;
		if (ExecutePHYReadFSM() != ASYNCOP_BUSY) {
			ContinueNow();
		}
		break;
	case INTERRUPT_Wait_PHIR:
		if (ExecutePHYReadFSM() == ASYNCOP_BUSY) return ASYNCOP_BUSY;
		ENC28J60_DEBUG_INFO("Interrupt: PHIR:", m_uiPhyReadData);
		if (!(m_uiPhyReadData & PHIR_PGIF)) {
			DEBUG_INFO("ENC28j60: Unknown Interrupt reason (EIR_LINKIF==1 but PHIR_PGIF==0). PHIR:", m_uiPhyReadData);
			m_bError = true;
			ContinueNow();
			return ASYNCOP_BUSY;
		}
		// One or more enabled PHY interrupts have occurred since PHIR was last read
		if (!(m_uiPhyReadData & PHIR_PLNKIF)) {
			PARANOID_ERRORS("Unknown PHY interrupt source");
			m_bError = true;
			ContinueNow();
			return ASYNCOP_BUSY;
		}
		// PHY link status has changed since PHIR was last read
		m_eInterruptFSM = INTERRUPT_Idle;
		return ASYNCOP_DONE;
	}
	return ASYNCOP_BUSY;
}

CSPIDeviceENC28J60::ASYNCOP_RESULT CSPIDeviceENC28J60::DoSetBank(uint8_t uiAddr)
{
	if ( (uiAddr & ENC28J60_ADDR_MASK) >= ENC28J60_COMMON_CR ) {
		/* common register, bank does not matter */
		return ASYNCOP_DONE;
	}

	uint8_t uiBankToSelect = (uiAddr >> 5) & 0x03; /*BSEL1|BSEL0=0x03*/

#ifndef VERIFY_SELECTED_BANK
	if (uiBankToSelect == m_uiBankSelected) {
		/* bank already selected */
		return ASYNCOP_DONE;
	}
#endif // VERIFY_SELECTED_BANK

	if (m_eSelectBankFSM == SELECTBANK_Idle) {
		m_uiBankToSelect = uiBankToSelect;
		m_eSelectBankFSM = (uiBankToSelect == m_uiBankSelected)?SELECTBANK_Check:SELECTBANK_Start;
		m_nBankSelectTries = BANK_SELECT_RETRIES;
	} else {
		ASSERTE(m_uiBankToSelect == uiBankToSelect);
	}
	if (ExecuteSelectBankFSM() != ASYNCOP_BUSY) {
		ASSERTE(uiBankToSelect == m_uiBankSelected);
		return ASYNCOP_DONE;
	}

	return ASYNCOP_BUSY;
}

void CSPIDeviceENC28J60::ContinueOnSPIIdle()
{
	ENC28J60_DEBUG_INFO("Scheduling callback");
	ASSERTE(m_pSPI);
	m_pSPI->RequestSPIIdleNotify(this);
}





void CNetworkInterfaceENC28J60::BindNetworkInterface(CallbackOnPktReceived cbOnPktReceived /*= CallbackOnPktReceived()*/, CallbackOnLinkChanged cbOnLinkChanged /*= CallbackOnLinkChanged()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::BindNetworkInterface(cbOnPktReceived));

	SetPktReceivedCallback(cbOnPktReceived);
	SetLinkChangedCallback(cbOnLinkChanged);
}
void CNetworkInterfaceENC28J60::SetPktReceivedCallback(CallbackOnPktReceived cbOnPktReceived /*= CallbackOnPktReceived()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::SetPktReceivedCallback(cbOnPktReceived));

	m_cbOnPktReceivedCallback = cbOnPktReceived;
}
void CNetworkInterfaceENC28J60::SetLinkChangedCallback(CallbackOnLinkChanged cbOnLinkChanged /*= CallbackOnLinkChanged()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::SetLinkChangedCallback(cbOnLinkChanged));

	m_cbOnLinkChangedCallback = cbOnLinkChanged;
}
void CNetworkInterfaceENC28J60::SetIncomeCheckCallback(CallbackOnIncomeCheck cbOnIncomeCheck /*= CallbackOnIncomeCheck()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::SetIncomeCheckCallback(cbOnIncomeCheck));

	m_cbOnIncomeCheckCallback = cbOnIncomeCheck;
}
void CNetworkInterfaceENC28J60::SetMacAddress(const mac_addr_t* MacAaddress)
{
	CSPIDeviceENC28J60::SetMacAddress(MacAaddress);
}
const INetworkInterface::mac_addr_t* CNetworkInterfaceENC28J60::GetMacAddress() const
{
	return CSPIDeviceENC28J60::GetMacAddress();
}
unsigned CNetworkInterfaceENC28J60::GetMTU() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetMTU());
	return ENC28J60_MAXFRAME;
}
void CNetworkInterfaceENC28J60::ScheduleRefresh()
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::ScheduleRefresh());
	m_bInterruptRequested = true;
}

void CNetworkInterfaceENC28J60::Reset()
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::Reset());
	m_bResetRequested = true;
}

bool CNetworkInterfaceENC28J60::TxPkt(const mac_addr_t* TargetMACadr, uint16_t uiFrameType, CChainedConstChunks* pHeadChunk, CallbackOnTxDone cbOnTxDoneCallback /*= NullCallback*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::TxPkt(TargetMACadr, uiFrameType, pHeadChunk, cbOnTxDoneCallback));
	bool bResult = true;
	ASSERTE(pHeadChunk);
	ENTER_CRITICAL_SECTION;
	{
		if (!m_bFrameTxRequested) {
			m_bFrameTxRequested = true;
		} else {
			bResult = false;
		}
	}
	LEAVE_CRITICAL_SECTION;
	if (bResult) {
		if (TargetMACadr) {
			m_TxHeaderTargeted.TargetMacAddr = *TargetMACadr;
			m_TxHeaderTargeted.uiFrameTxType = uiFrameType;
			m_FrameTxHeadChunk.Assign((const uint8_t*)&m_TxHeaderTargeted, sizeof(m_TxHeaderTargeted));
		} else {
			m_TxHeaderBroadcast.uiFrameTxType = uiFrameType;
			m_FrameTxHeadChunk.Assign((const uint8_t*)&m_TxHeaderBroadcast, sizeof(m_TxHeaderBroadcast));
		}
		m_FrameTxHeadChunk.Append(pHeadChunk);
		m_uiFrameTxCollectedSize = 1 + ETHERNET_HEADER_LEN + CollectChainTotalLength(pHeadChunk);
		ASSERTE(CollectChainTotalLength(&m_FrameTxHeadChunk) == m_uiFrameTxCollectedSize);
		ASSERTE(m_uiFrameTxCollectedSize <= ENC28J60_MAXFRAME + ETHERNET_HEADER_LEN);
		m_cbOnTxDoneCallback = cbOnTxDoneCallback;
		ENC28J60_DEBUG_INFO("TX FRAME START. Size:", m_uiFrameTxCollectedSize);
		ContinueNow();
	}
	return bResult;
}
unsigned CNetworkInterfaceENC28J60::GetRxedFrameSize() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameSize());
	return m_uiFrameRxedSize;
}
uint16_t CNetworkInterfaceENC28J60::GetRxedFrameType() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameType());
	ASSERTE(m_uiFrameRxedSize >= ETHERNET_HEADER_LEN);
	const uint8_t* puiPktType = &(m_FrameRxBuf[12]);
	uint16_t uiPktType = *(const uint16_t*)puiPktType;
	return uiPktType;
}
const INetworkInterface::mac_addr_t* CNetworkInterfaceENC28J60::GetRxedFrameSourceMacAddr() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameSourceMacAddr());
	ASSERTE(m_uiFrameRxedSize >= ETHERNET_HEADER_LEN);
	return (mac_addr_t*)&(m_FrameRxBuf[6]);
}
const INetworkInterface::mac_addr_t* CNetworkInterfaceENC28J60::GetRxedFrameTargetMacAdr() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameTargetMacAdr());
	ASSERTE(m_uiFrameRxedSize >= ETHERNET_HEADER_LEN);
	return (mac_addr_t*)&(m_FrameRxBuf[0]);
}
const void* CNetworkInterfaceENC28J60::GetRxedFrameData() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameData());
	ASSERTE(m_uiFrameRxedSize >= ETHERNET_HEADER_LEN);
	return &(m_FrameRxBuf[ETHERNET_HEADER_LEN]);
}

void CNetworkInterfaceENC28J60::ReleaseRxedFrame()
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::ReleaseRxedFrame());

	ASSERTE(m_uiFrameRxedSize);
	m_uiFrameRxedSize = 0;
	m_bFrameRxRequested = true;
	ContinueNow();
}

void CNetworkInterfaceENC28J60::OnPktReceived()
{
	IMPLEMENTS_INTERFACE_METHOD(CSPIDeviceENC28J60::OnPktReceived());

	if (m_cbOnPktReceivedCallback) m_cbOnPktReceivedCallback(m_uiFrameRxedSize);
}

void CNetworkInterfaceENC28J60::OnLinkChanged(bool bLinkStatus)
{
	IMPLEMENTS_INTERFACE_METHOD(CSPIDeviceENC28J60::OnLinkChanged(bLinkStatus));

	if (m_cbOnLinkChangedCallback) m_cbOnLinkChangedCallback(m_bLinkStatus);
}

void CNetworkInterfaceENC28J60::OnPktSent()
{
	IMPLEMENTS_INTERFACE_METHOD(CSPIDeviceENC28J60::OnPktSent());

	if (m_cbOnTxDoneCallback) m_cbOnTxDoneCallback(true);
	m_cbOnTxDoneCallback = CallbackOnTxDone();
}

void CNetworkInterfaceENC28J60::OnPktError()
{
	IMPLEMENTS_INTERFACE_METHOD(CSPIDeviceENC28J60::OnPktError());

	if (m_cbOnTxDoneCallback) m_cbOnTxDoneCallback(false);
	m_cbOnTxDoneCallback = CallbackOnTxDone();
}

void CNetworkInterfaceENC28J60::OnIncomeCheck()
{
	IMPLEMENTS_INTERFACE_METHOD(CSPIDeviceENC28J60::OnIncomeCheck());

	if (m_cbOnIncomeCheckCallback) m_cbOnIncomeCheckCallback();
}


CNetworkInterfaceENC28J60 g_DeviceENC28J60;

INetworkInterface* AcquireNetworkInterfaceENC28J60(ISPI* pSPI, IDigitalOut* pSelectDO, IDigitalOut* pResetDO, IDigitalIn* pInteruptDI)
{
	g_DeviceENC28J60.Init(pSPI, pSelectDO, pResetDO, pInteruptDI);
	return &g_DeviceENC28J60;
};

