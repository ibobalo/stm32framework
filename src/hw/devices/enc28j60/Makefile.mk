USE_SPI += $(if $(filter YES,$(USE_ENC28J60)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_ENC28J60)), $(addprefix src/hw/devices/enc28j60/, \
	dev_enc28j60.cpp \
))
