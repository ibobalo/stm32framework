USE_I2C += $(if $(filter YES,$(USE_DS2482)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_DS2482)), $(addprefix src/hw/devices/ds2482/, \
	dev_ds2482.cpp \
))
