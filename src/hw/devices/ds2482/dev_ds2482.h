#ifndef DEV_DS2482_H_INCLUDED
#define DEV_DS2482_H_INCLUDED

#include <stdint.h>
#include "interfaces/1wire.h"

//extern
class II2C;

class IDeviceDS2482 :
		public IDevice1WMaster
{
public:
	enum EADxxx {AD_000, AD_001, AD_010, AD_011, AD_100, AD_101, AD_110, AD_111};
	using IDevice1WMaster::TDoneNotification;
	using IDevice1WMaster::TReadNotification;

public: // IDeviceDS2482
	virtual bool IsConnected() const = 0;
	virtual bool IsBusy() const = 0;
	virtual uint8_t ChannelSelected() const = 0;
	virtual bool Reset(TDoneNotification cbDone) = 0;
	virtual bool SelectChannel(uint8_t nChannel, TDoneNotification cbDone) = 0;
};

IDeviceDS2482* AcquireDeviceDS2482(II2C* pI2C, IDeviceDS2482::EADxxx eAD = IDeviceDS2482::AD_000);

#endif /* DEV_DS2482_H_INCLUDED */
