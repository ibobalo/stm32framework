#include "stdafx.h"
#include "dev_ds2482.h"
#include "hw/mcu/hw_i2c.h"
#include "tick_process.h"
#include "hw/debug.h"
#include "util/atomic.h"
#include "util/bitops.h"
#include "tick_timeout.h"

#define DS2482_ADDRESS 0x018
#define DS2482_CMD_DRST 0xF0
#define DS2482_CMD_WCFG 0xD2
#define DS2482_CMD_CHSL 0xC3
#define DS2482_CMD_SRP  0xE1
#define DS2482_CMD_1WRS 0xB4
#define DS2482_CMD_1WWB 0xA5
#define DS2482_CMD_1WRB 0x96
#define DS2482_CMD_1WSB 0x87
#define DS2482_CMD_1WT  0x78

#define DS2482_REG_STATUS 0xF0
#  define DS2482_REG_STATUS_1WB 0x01
#  define DS2482_REG_STATUS_PPD 0x02
#  define DS2482_REG_STATUS_SD  0x04
#  define DS2482_REG_STATUS_LL  0x08
#  define DS2482_REG_STATUS_RST 0x10
#  define DS2482_REG_STATUS_SBR 0x20
#  define DS2482_REG_STATUS_TSB 0x40
#  define DS2482_REG_STATUS_DIR 0x80
#define DS2482_REG_READDATA 0xE1
#define DS2482_REG_CHANNELSELECTION 0xD2
#define DS2482_REG_CONFIGURATION 0xC3
#  define DS2482_REG_CFG   0x01
#  define DS2482_REG_CFG_APU   0x01
#  define DS2482_REG_CFG_SPU   0x04
#  define DS2482_REG_CFG_1WS   0x08
#  define DS2482_REG_CFG_NAPU  0x10
#  define DS2482_REG_CFG_NSPU  0x40
#  define DS2482_REG_CFG_N1WS  0x80

#define INITIAL_DELAY             10
#define REPEAT_WAIT_DELAY         1
#define REPEAT_AFTER_ERROR_DELAY  3
#define REPEAT_AFTER_MISSED_DELAY 1000

//#define DS2482_DEBUG_INFO(...) DEBUG_INFO("2482:" __VA_ARGS__)
#define DS2482_DEBUG_INFO(...) {}

class CI2CDeviceDS2482:
		public IDeviceDS2482,
		public IDeviceI2C,
		public CTickProcess
{
public:
	using IDeviceDS2482::TDoneNotification;
	using IDeviceDS2482::TReadNotification;
	typedef CTickProcess CParentProcess;
	typedef enum {
		RegStatus   = DS2482_REG_STATUS,
		RegReadData = DS2482_REG_READDATA,
		RegChannel  = DS2482_REG_CHANNELSELECTION,
		RegConfig   = DS2482_REG_CONFIGURATION,
	} ERegToRead;
public:
	void Init(II2C* pI2C, EADxxx eAD = AD_000);

public: // IDeviceDS2482::IDevice1WMaster
	virtual bool IsReady() const;
	virtual bool BusResetDetect(TDoneNotification cbDone);
	virtual bool TxBit(bool bit, TDoneNotification cbDone);
	virtual bool TxBitPU(bool bit, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone);
	virtual bool TxByte(uint8_t uiByte, TDoneNotification cbDone);
	virtual bool TxBytePU(uint8_t uiByte, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone);
	virtual bool TxBytes(const void* pData, unsigned nLength, TDoneNotification cbDone);
	virtual bool TxBytesPU(const void* pData, unsigned nLength, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone);
	virtual bool RxBit(TReadNotification cbReadDone);
	virtual bool RxByte(TReadNotification cbReadDone);
	virtual bool RxBytes(void* pBuffer, unsigned nLength, TDoneNotification cbDone);
	virtual bool Triplet(bool bWriteBitIf00, TReadNotification cbReadDone);

public: // IDeviceDS2482
	virtual bool IsConnected() const;
	virtual bool IsBusy() const;
	virtual uint8_t ChannelSelected() const;
	virtual bool Reset(TDoneNotification cbDone);
	virtual bool SelectChannel(uint8_t nChannel, TDoneNotification cbDone);

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

private:
	void NotifyDone();
	void NotifyReadDone(uint8_t uiData);
	void NotifyError();
	bool SessionStart();

	II2C*                 m_pI2C;
	uint8_t               m_uiAddress;
	enum {
		OpError,
		OpInitReset,
		OpInitCFG,
		OpReadReg,
		OpSelectChannel,
		OpBusResetDetect,
		OpTxBitCfg,
		OpTxBit,
		OpTxByteCfg,
		OpTxByte,
		OpRxBit,
		OpRxByte,
		OpTriplet,
		OpStrongPUDelay,
		OpWaitResetDone,
		OpWaitBusyDone,
		OpWaitBusyAndReadBit,
		OpWaitBusyAndReadByte,
		OpWaitBusyAndReadTriplet,
		OpNone,
	}         m_eCurrentOp;
	bool m_bReinitRequired;
	bool m_bOpResult;

	struct {
		uint8_t    uiCmd;
		uint8_t    uiParam;
	}           m_tCommandPkt;
	uint8_t     m_uiReadData;
	ERegToRead  m_eRegToRead;

	CChainedConstChunks m_cccCmdNoParamChunks;
	CChainedConstChunks m_cccCmdParamChunks;
	CChainedIoChunks    m_cicReadValueChunks;

	TDoneNotification m_cbDone;
	TReadNotification m_cbReadDone;
	CTimeout  m_toPUDuration;
	CTimeout  m_toWaitTimeout;
	const uint8_t*  m_pData;
	uint8_t*  m_pBuffer;
	unsigned  m_nLength;
	uint8_t   m_nChannelSelected;
	uint8_t   m_uiCfg;
	uint8_t   m_uiByte;
	uint8_t   m_nChannelRequested;
	static const uint8_t m_auiChannelSelectCodesToWrite[8];
	static const uint8_t m_auiChannelSelectCodesReadBack[8];
};

const uint8_t CI2CDeviceDS2482::m_auiChannelSelectCodesToWrite[8] = {0xF0, 0xE1, 0xD2, 0xC3, 0xB4, 0xA5, 0x96, 0x87};
const uint8_t CI2CDeviceDS2482::m_auiChannelSelectCodesReadBack[8] = {0xB8, 0xB1, 0xAA, 0xA3, 0x9C, 0x95, 0x8E, 0x87};

void CI2CDeviceDS2482::Init(II2C* pI2C, EADxxx eAD)
{
	CParentProcess::Init(&g_TickProcessScheduller);

	m_pI2C = pI2C;
	m_uiAddress = DS2482_ADDRESS | uint8_t(eAD);
	m_eCurrentOp = OpError;
	m_bReinitRequired = true;
	m_cccCmdNoParamChunks.Assign((const uint8_t*)&m_tCommandPkt, 1);
	m_cccCmdNoParamChunks.Break();
	m_cccCmdParamChunks.Assign((uint8_t*)&m_tCommandPkt, 2);
	m_cccCmdParamChunks.Break();
	m_cicReadValueChunks.Assign(&m_uiReadData, 1);
	m_cicReadValueChunks.Break();

	m_cbDone = NullCallback();
	m_cbReadDone = NullCallback();
	m_toPUDuration.Init(0);
	m_toWaitTimeout.Init(100);
	m_nChannelSelected = 0;
	m_uiCfg = DS2482_REG_CFG_NAPU | DS2482_REG_CFG_NSPU | DS2482_REG_CFG_N1WS | 0x20;
	m_nChannelRequested = 0;

	ContinueDelay(INITIAL_DELAY);            INDIRECT_CALL(SingleStep(0));
}

/*virtual*/
bool CI2CDeviceDS2482::IsReady() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::IsReady());
	return m_eCurrentOp == OpNone;
}

/*virtual*/
bool CI2CDeviceDS2482::BusResetDetect(TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::BusResetDetect(cbDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpBusResetDetect))
		return false;
	DS2482_DEBUG_INFO("CMD: BusResetDetect");
	m_cbDone = cbDone;
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::TxBit(bool bit, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::TxBit(bit, cbDone));
	return TxBitPU(bit, 0, cbDone);
}

/*virtual*/
bool CI2CDeviceDS2482::TxBitPU(bool bit, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::TxBitPU(bit, tdStrongPUDuration, cbDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpTxBit))
		return false;
	DS2482_DEBUG_INFO("CMD: TxBit:", bit?1:0);
	Cancel();
	m_uiByte = bit ? 0x80 : 0x00;
	m_cbDone = cbDone;
	m_toWaitTimeout.Start();
	m_toPUDuration.Init(tdStrongPUDuration);
	if (m_toPUDuration.GetIsDefined() && !CheckBitsAny(m_uiCfg, DS2482_REG_CFG_SPU)) {
		DS2482_DEBUG_INFO("CMD: TxBit PU:", tdStrongPUDuration);
		SetBits01(m_uiCfg, DS2482_REG_CFG_NSPU, DS2482_REG_CFG_SPU);
		m_eCurrentOp = OpTxBitCfg;
	}
	m_pI2C->Queue_I2C_Session(this);         INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
	return true;
}

/*virtual*/
bool CI2CDeviceDS2482::TxByte(uint8_t uiByte, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::TxByte(uiByte, cbDone));
	return TxBytePU(uiByte, 0, cbDone);
}
/*virtual*/
bool CI2CDeviceDS2482::TxBytePU(uint8_t uiByte, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::TxBytePU(uiByte, tdStrongPUDuration, cbDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpTxByte))
		return false;
	DS2482_DEBUG_INFO("CMD: TxByte:", uiByte);
	Cancel();
	m_nLength = 0;
	m_uiByte = uiByte;
	m_cbDone = cbDone;
	m_toWaitTimeout.Start();
	m_toPUDuration.Init(tdStrongPUDuration);
	if (m_toPUDuration.GetIsDefined() && !CheckBitsAny(m_uiCfg, DS2482_REG_CFG_SPU)) {
		DS2482_DEBUG_INFO("CMD: TxByte, PU:", tdStrongPUDuration);
//		SetBits01(m_uiCfg, DS2482_REG_CFG_NSPU, DS2482_REG_CFG_SPU);
		m_eCurrentOp = OpTxByteCfg;
	}
	m_pI2C->Queue_I2C_Session(this);         INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
	return true;
}

/*virtual*/
bool CI2CDeviceDS2482::TxBytes(const void* pData, unsigned nLength, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::TxBytes(pData, nLength, cbDone));
	return TxBytesPU(pData, nLength, 0, cbDone);
}

bool CI2CDeviceDS2482::SessionStart()
{
	Cancel();
	m_toWaitTimeout.Start();
	m_pI2C->Queue_I2C_Session(this);          INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
	return true;
}

/*virtual*/
bool CI2CDeviceDS2482::TxBytesPU(const void* pData, unsigned nLength, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::TxBytesPU(pData, nLength, tdStrongPUDuration, cbDone));
	ASSERTE(pData && nLength > 0);
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpTxByte))
		return false;
	DS2482_DEBUG_INFO("CMD: TxBytes:", nLength);
	m_toPUDuration.Init(tdStrongPUDuration);
	if (nLength == 1 && m_toPUDuration.GetIsDefined() && !CheckBitsAny(m_uiCfg, DS2482_REG_CFG_SPU)) {
		DS2482_DEBUG_INFO("CMD: TxBytes, PU:", tdStrongPUDuration);
		SetBits01(m_uiCfg, DS2482_REG_CFG_NSPU, DS2482_REG_CFG_SPU);
		m_eCurrentOp = OpTxByteCfg;
	}
	m_uiByte = *(const uint8_t*)pData;
	m_pData = (const uint8_t*)pData + 1;
	m_nLength = nLength - 1;
	m_cbDone = cbDone;
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::RxBit(TReadNotification cbReadDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::RxBit(cbReadDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpRxBit))
		return false;
	DS2482_DEBUG_INFO("CMD: RxBit");
	m_cbReadDone = cbReadDone;
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::RxByte(TReadNotification cbReadDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::RxByte(cbReadDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpRxByte))
		return false;
	DS2482_DEBUG_INFO("CMD: RxByte");
	m_nLength = 0;
	m_cbReadDone = cbReadDone;
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::RxBytes(void* pBuffer, unsigned nLength, TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::RxBytes(pBuffer, nLength, cbDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpRxByte))
		return false;
	DS2482_DEBUG_INFO("CMD: RxBytes:", nLength);
	m_pBuffer = (uint8_t*)pBuffer;
	m_nLength = nLength;
	m_cbDone = cbDone;
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::Triplet(bool bWriteBitIf00, TReadNotification cbReadDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WMaster::Triplet(bWriteBitIf00, cbReadDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpTriplet))
		return false;
	DS2482_DEBUG_INFO("CMD: Triplet", bWriteBitIf00?1:0);
	m_uiByte = bWriteBitIf00 ? 0x80 : 0x00;
	m_cbReadDone = cbReadDone;
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::IsConnected() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::IsConnected());
	return m_eCurrentOp != OpError && m_eCurrentOp != OpInitReset && m_eCurrentOp != OpInitCFG;
}

/*virtual*/
bool CI2CDeviceDS2482::IsBusy() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::IsBusy());
	return m_eCurrentOp != OpNone && m_eCurrentOp != OpError;
}

/*virtual*/
uint8_t CI2CDeviceDS2482::ChannelSelected() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::ChannelSelected());
	return m_nChannelSelected;
}

/*virtual*/
bool CI2CDeviceDS2482::Reset(IDeviceDS2482::TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::Reset(cbDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpInitReset))
		if (!AtomicCmpSet(&m_eCurrentOp, OpError, OpInitReset))
			return false;
	DS2482_DEBUG_INFO("RESET");
	return SessionStart();
}

/*virtual*/
bool CI2CDeviceDS2482::SelectChannel(uint8_t nChannel, IDeviceDS2482::TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS2482::SelectChannel(nChannel, cbDone));
	ASSERTE(nChannel < ARRAY_SIZE(m_auiChannelSelectCodesToWrite));
	if (nChannel == m_nChannelSelected && nChannel == m_nChannelRequested) {
		if (cbDone) cbDone(true);
		return true;
	}
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpSelectChannel))
		return false;
	DS2482_DEBUG_INFO("CHANNEL:", nChannel);
	m_nChannelRequested = nChannel;
	m_cbDone = cbDone;
	return SessionStart();
}

/*virtual*/
unsigned CI2CDeviceDS2482::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_uiAddress;
}

/*virtual*/
bool CI2CDeviceDS2482::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));

#define CMD_NOPARAM(cmd) { \
		m_tCommandPkt.uiCmd = cmd; \
		*ppRetChunksToSend = &m_cccCmdNoParamChunks; \
		*ppRetChunksToRecv = NULL; \
		DS2482_DEBUG_INFO("CMD NO PARAMS:", cmd); \
}
#define CMD_NOPARAM_READ(cmd) { \
		m_tCommandPkt.uiCmd = cmd; \
		*ppRetChunksToSend = &m_cccCmdNoParamChunks; \
		*ppRetChunksToRecv = &m_cicReadValueChunks; \
		DS2482_DEBUG_INFO("CMD NO PARAMS, READ:", cmd); \
}
#define CMD_PARAM(cmd, param) { \
		m_tCommandPkt.uiCmd = cmd; \
		m_tCommandPkt.uiParam = param; \
		*ppRetChunksToSend = &m_cccCmdParamChunks; \
		*ppRetChunksToRecv = NULL; \
		DS2482_DEBUG_INFO("CMD PARAM:", cmd); \
}
#define CMD_PARAM_READ(cmd, param) { \
		m_tCommandPkt.uiCmd = cmd; \
		m_tCommandPkt.uiParam = param; \
		*ppRetChunksToSend = &m_cccCmdParamChunks; \
		*ppRetChunksToRecv = &m_cicReadValueChunks; \
		DS2482_DEBUG_INFO("CMD PARAM, READ:", cmd); \
}
#define READ_AGAIN() { \
		*ppRetChunksToSend = NULL; \
		*ppRetChunksToRecv = &m_cicReadValueChunks; \
		DS2482_DEBUG_INFO("READ AGAIN"); \
}
	switch (m_eCurrentOp) {
	case OpError:
	case OpNone:
		ASSERT("invalid op");
		return false;
		break;
	case OpInitReset:
		ASSERTE(m_bReinitRequired);
		m_uiCfg = DS2482_REG_CFG_NAPU | DS2482_REG_CFG_NSPU | DS2482_REG_CFG_N1WS | 0x20;
		CMD_NOPARAM_READ(DS2482_CMD_DRST);
		break;
	case OpInitCFG:
		ASSERTE(m_bReinitRequired);
		ASSERTE(((m_uiCfg ^ (m_uiCfg >> 4)) & 0x0F) == 0x0F); // bits 4-7 are complementary to bits 0-3
		CMD_PARAM_READ(DS2482_CMD_WCFG, m_uiCfg);
		break;
	case OpReadReg:
		CMD_PARAM_READ(DS2482_CMD_SRP, (uint8_t)m_eRegToRead);
		break;
	case OpSelectChannel:
		ASSERTE(m_nChannelRequested < ARRAY_SIZE(m_auiChannelSelectCodesToWrite));
		CMD_PARAM_READ(DS2482_CMD_CHSL, m_auiChannelSelectCodesToWrite[m_nChannelRequested]);
		break;
	case OpBusResetDetect:
		CMD_NOPARAM(DS2482_CMD_1WRS);
		break;
	case OpTxBitCfg:
		ASSERTE(((m_uiCfg ^ (m_uiCfg >> 4)) & 0x0F) == 0x0F); // bits 4-7 are complementary to bits 0-3
		CMD_PARAM(DS2482_CMD_WCFG, m_uiCfg);
		break;
	case OpTxBit:
		CMD_PARAM_READ(DS2482_CMD_1WSB, m_uiByte);
		break;
	case OpTxByteCfg:
		ASSERTE(((m_uiCfg ^ (m_uiCfg >> 4)) & 0x0F) == 0x0F); // bits 4-7 are complementary to bits 0-3
		CMD_PARAM(DS2482_CMD_WCFG, m_uiCfg);
		break;
	case OpTxByte:
		CMD_PARAM(DS2482_CMD_1WWB, m_uiByte);
		break;
	case OpRxBit:
		CMD_PARAM_READ(DS2482_CMD_1WSB, 0x80);
		break;
	case OpRxByte:
		CMD_NOPARAM(DS2482_CMD_1WRB);
		break;
	case OpTriplet:
		CMD_PARAM(DS2482_CMD_1WT, m_uiByte);
		break;
	case OpStrongPUDelay:
		ASSERT("invalid state");
		break;
	case OpWaitResetDone:
	case OpWaitBusyDone:
	case OpWaitBusyAndReadBit:
	case OpWaitBusyAndReadByte:
	case OpWaitBusyAndReadTriplet:
		READ_AGAIN();
		break;
	}
	return true;
}

/*virtual*/
void CI2CDeviceDS2482::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());
	DS2482_DEBUG_INFO("I2C done:", m_eCurrentOp);
	switch (m_eCurrentOp) {
		case OpNone:
		case OpError: {
			ASSERT("invalid op");
		} break;
		case OpInitReset: {
			// m_uiReadData is Status Register
			if ((m_uiReadData & (DS2482_REG_STATUS_RST | DS2482_REG_STATUS_1WB | DS2482_REG_STATUS_PPD | DS2482_REG_STATUS_SD | DS2482_REG_STATUS_SBR | DS2482_REG_STATUS_TSB | DS2482_REG_STATUS_DIR)) == DS2482_REG_STATUS_RST) {
				DEBUG_INFO("DS2482 detected. Addr:", m_uiAddress);
				SetBits01(m_uiCfg, DS2482_REG_CFG_NAPU, DS2482_REG_CFG_APU);
				m_eCurrentOp = OpInitCFG;
				m_pI2C->Queue_I2C_Session(this);         INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
			} else {
				DEBUG_INFO("DS2482 reset fault. Addr:", m_uiAddress);
				m_eCurrentOp = OpError;
				ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
				return;
			}
		} break;
		case OpInitCFG: {
			// m_uiReadData is Config Register
			if ((m_uiReadData & 0x0F) == (m_uiCfg & 0x0F)) {
				m_bReinitRequired = false;
				m_eCurrentOp = OpNone;
				DS2482_DEBUG_INFO("config done. Addr:", m_uiAddress);
				NotifyDone();
			} else {
				DEBUG_INFO("DS2482 config fault. Addr:", m_uiAddress);
				m_eCurrentOp = OpInitReset;
				m_pI2C->Queue_I2C_Session(this);         INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
			}
		} break;
		case OpReadReg: {
			// m_uiReadData is m_eRegToRead register
			DS2482_DEBUG_INFO("REG:", m_eRegToRead);
			DS2482_DEBUG_INFO("VAL:", m_uiReadData);
			if (m_nLength) {
				*m_pBuffer++ = m_uiReadData;
				--m_nLength;
			}
			if (m_nLength) {
				m_toWaitTimeout.Start();
				m_eCurrentOp = OpRxByte;
				m_pI2C->Queue_I2C_Session(this);         INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
			} else {
				m_eCurrentOp = OpNone;
				NotifyReadDone(m_uiReadData);
			}
		} break;
		case OpSelectChannel: {
			// m_uiReadData is Channel Selection Register
			m_eCurrentOp = OpNone;
			bool bSuccess = m_uiReadData == m_auiChannelSelectCodesReadBack[m_nChannelRequested];
			if (bSuccess) {
				m_nChannelSelected = m_nChannelRequested;
				NotifyDone();
			} else {
				NotifyError();
			}
		} break;
		case OpBusResetDetect: {
			// m_uiReadData is Status Register
			m_eCurrentOp = OpWaitResetDone;
			ContinueDelay(2);                            INDIRECT_CALL(SingleStep(0));
		} break;
		case OpTxBitCfg: {
			// m_uiReadData is Status Register
			m_eCurrentOp = OpTxBit;
			m_pI2C->Queue_I2C_Session(this);             INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
		} break;
		case OpTxBit: {
			// m_uiReadData is Status Register
			m_eCurrentOp = OpWaitBusyDone;
			ContinueDelay(1);                            INDIRECT_CALL(SingleStep(0));
		} break;
		case OpTxByteCfg: {
			// m_uiReadData is Status Register
			m_eCurrentOp = OpTxByte;
			m_pI2C->Queue_I2C_Session(this);             INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
		} break;
		case OpTxByte: {
			// m_uiReadData is Status Register
			m_eCurrentOp = OpWaitBusyDone;
			ContinueDelay(6);                            INDIRECT_CALL(SingleStep(0));
		} break;
		case OpRxBit: {
			// m_uiReadData is Status Register
			m_eCurrentOp = OpWaitBusyAndReadBit;
			ContinueDelay(1);                            INDIRECT_CALL(SingleStep(0));
		} break;
		case OpRxByte: {
			// m_uiReadData is Status Register
			// NOTE: To read the data byte received from the 1-Wire IO channel, issue
			// the Set Read Pointer command and select the Read Data Register. Then
			// access the DS2482 in read mode.
			m_eCurrentOp = OpWaitBusyAndReadByte;
			ContinueDelay(6);                            INDIRECT_CALL(SingleStep(0));
		} break;
		case OpTriplet: {
			m_eCurrentOp = OpWaitBusyAndReadTriplet;
			ContinueDelay(2);                            INDIRECT_CALL(SingleStep(0));
		} break;
		case OpWaitResetDone: {
			// m_uiReadData is Status Register
			if (!(m_uiReadData & DS2482_REG_STATUS_1WB)) {
				m_eCurrentOp = OpNone;
				bool bSuccess = ((m_uiReadData & (DS2482_REG_STATUS_RST | DS2482_REG_STATUS_SD | DS2482_REG_STATUS_PPD)) == DS2482_REG_STATUS_PPD);
				if (bSuccess) {
					NotifyDone();
				} else {
					NotifyError();
				}
			} else {
				ContinueDelay(1);                        INDIRECT_CALL(SingleStep(0));
			}
		} break;
		case OpWaitBusyDone: {
			// m_uiReadData is Status Register
			if (!(m_uiReadData & DS2482_REG_STATUS_1WB)) {
				bool bSuccess = !(m_uiReadData & (DS2482_REG_STATUS_RST | DS2482_REG_STATUS_SD));
				if (!bSuccess) {
					DS2482_DEBUG_INFO("Wait failed:", (int)m_uiReadData);
					m_eCurrentOp = OpNone;
					m_toPUDuration.Init(0);
					NotifyError();
				} else if (m_nLength) {
					--m_nLength;
					m_uiByte = *m_pData++;
					if (m_nLength > 0 || !m_toPUDuration.GetIsDefined() || CheckBitsAny(m_uiCfg, DS2482_REG_CFG_SPU)) {
						DS2482_DEBUG_INFO("Wait busy: Done, next byte, bytes left:", m_nLength);
						m_eCurrentOp = OpTxByte;
					} else {
						// set SPU before last byte TX
						DS2482_DEBUG_INFO("CMD: TxByte, PU:", m_toPUDuration.GetTimeout());
						SetBits01(m_uiCfg, DS2482_REG_CFG_NSPU, DS2482_REG_CFG_SPU);
						m_eCurrentOp = OpTxByteCfg;
					}
					m_pI2C->Queue_I2C_Session(this);            INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
				} else if (m_toPUDuration.GetIsDefined()) {
					DS2482_DEBUG_INFO("Wait busy: Done, delay for PU:", m_toPUDuration.GetTimeout());
					m_eCurrentOp = OpStrongPUDelay;
					m_toPUDuration.Start();
					ContinueAt(m_toPUDuration.GetFinishTime()); INDIRECT_CALL(SingleStep(0));
				} else {
					DS2482_DEBUG_INFO("Wait busy: Done");
					m_eCurrentOp = OpNone;
					NotifyDone();
				}
			} else {
				ContinueDelay(1);                        INDIRECT_CALL(SingleStep(0));
			}
		} break;
		case OpWaitBusyAndReadBit: {
			// m_uiReadData is Status Register
			if (!(m_uiReadData & DS2482_REG_STATUS_1WB)) {
				m_eCurrentOp = OpNone;
				uint8_t uiByte = (m_uiReadData & DS2482_REG_STATUS_SBR) ? 0x01 : 0x00;
				NotifyReadDone(uiByte);
			} else {
				ContinueDelay(1);                        INDIRECT_CALL(SingleStep(0));
			}
		} break;
		case OpWaitBusyAndReadByte: {
			// m_uiReadData is Status Register
			if (!(m_uiReadData & DS2482_REG_STATUS_1WB)) {
				m_eCurrentOp = OpReadReg;
				m_eRegToRead = RegReadData;
				m_pI2C->Queue_I2C_Session(this);         INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
			} else {
				ContinueDelay(1);                        INDIRECT_CALL(SingleStep(0));
			}
		} break;
		case OpWaitBusyAndReadTriplet: {
			// m_uiReadData is Status Register
			if (!(m_uiReadData & DS2482_REG_STATUS_1WB)) {
				m_eCurrentOp = OpNone;
				uint8_t uiTripletResult = (m_uiReadData & (DS2482_REG_STATUS_SBR | DS2482_REG_STATUS_TSB | DS2482_REG_STATUS_DIR)) >> 5;
				NotifyReadDone(uiTripletResult);
			} else {
				ContinueDelay(1);                        INDIRECT_CALL(SingleStep(0));
			}
		} break;
		case OpStrongPUDelay: {
			ASSERT("invalid state");
		} break;
	}
}

/*virtual*/
void CI2CDeviceDS2482::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	DS2482_DEBUG_INFO("I2C error:", m_eCurrentOp);

	DEBUG_INFO("DS2482 missed. Addr:", m_uiAddress);
	m_bReinitRequired = true;
	m_eCurrentOp = OpError;
	m_toPUDuration.Init(0);
	ContinueDelay(REPEAT_AFTER_MISSED_DELAY);  INDIRECT_CALL(SingleStep(0));

	NotifyError();
}

/*virtual*/
void CI2CDeviceDS2482::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
	DS2482_DEBUG_INFO("I2C idle:", m_eCurrentOp);
	ContinueNow();
}

/*virtual*/
void CI2CDeviceDS2482::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CI2CDeviceDS2482");

	DS2482_DEBUG_INFO("step:", m_eCurrentOp);

	if (m_bReinitRequired) {
		if (!Reset(NullCallback())) {
			ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
		}
	} else switch (m_eCurrentOp) {
//		case OpRxBit:
//		case OpRxByte:
//			m_pI2C->Queue_I2C_Session(this);             INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
//			break;
		case OpWaitResetDone:
		case OpWaitBusyDone:
		case OpWaitBusyAndReadBit:
		case OpWaitBusyAndReadByte:
		case OpWaitBusyAndReadTriplet:
			m_pI2C->Queue_I2C_Session(this);             INDIRECT_CALL(GetI2CPacket(NULL,NULL), OnI2CDone());
			break;
		case OpStrongPUDelay:
			ASSERTE(m_toPUDuration.GetIsDefined());
			if (m_toPUDuration.GetIsElapsed()) {
				m_toPUDuration.Init(0);
				m_eCurrentOp = OpNone;
				NotifyDone();
			} else {
				DEBUG_INFO("Keep waiting PU delay:", m_toPUDuration.GetFinishTime());
				ContinueAt(m_toPUDuration.GetFinishTime()); INDIRECT_CALL(SingleStep(0));
			}
			break;
		default:
			ASSERT("Invalid state");
	}

	PROCESS_DEBUG_INFO("<<< Process: CI2CDeviceDS2482");
}

void CI2CDeviceDS2482::NotifyDone()
{
	IDeviceDS2482::TDoneNotification cbDone = m_cbDone;
	m_cbDone = NullCallback();
	if (cbDone) cbDone( true );
}

void CI2CDeviceDS2482::NotifyReadDone(uint8_t uiData)
{
	IDeviceDS2482::TReadNotification cbReadDone = m_cbReadDone;
	m_cbReadDone = NullCallback();
	if (cbReadDone) cbReadDone(true, uiData);
	else NotifyDone(); // for ReadBytes
}

void CI2CDeviceDS2482::NotifyError()
{
	IDeviceDS2482::TDoneNotification cbDone = m_cbDone;
	m_cbDone = NullCallback();
	if (cbDone) cbDone( false );
	IDeviceDS2482::TReadNotification cbReadDone = m_cbReadDone;
	m_cbReadDone = NullCallback();
	if (cbReadDone) cbReadDone(false, 0);
}

CI2CDeviceDS2482 g_DS2482;

IDeviceDS2482* AcquireDeviceDS2482(II2C* pI2C, IDeviceDS2482::EADxxx eAD)
{
	g_DS2482.Init(pI2C, eAD);
	return &g_DS2482;
}
