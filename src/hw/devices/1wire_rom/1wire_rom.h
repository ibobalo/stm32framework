#ifndef _1WIRE_ROM_H_INCLUDED
#define _1WIRE_ROM_H_INCLUDED

#include <stdint.h>
#include "interfaces/1wire.h"

class CDevice1WROM :
		virtual public IDevice1WROM
{
public:
	void Init(IDevice1WMaster* p1W);
public: // IDevice1WROM
	virtual bool SelectID(uint64_t ID);
	virtual void GetSelectedID(uint64_t& retID) const;
	virtual bool GetDetectedID(uint64_t& retID) const;
	virtual bool Detect(TDoneNotification cbDone);
	virtual bool Select(TDoneNotification cbDone);
	virtual bool FindFirst(TDoneNotification cbDone);
	virtual bool FindNext(TDoneNotification cbDone);
protected:
	IDevice1WMaster*      m_p1W;
	uint64_t              m_uiSelectedID;
	union {
		uint8_t           m_auiIDRxBuff[8];
		uint64_t          m_uiDetectedID;
	};
private:
	void OnDetectResetDone(bool bSuccess);
	void OnDetectReadRomDone(bool bSuccess);
	void OnDetectDone(bool bSuccess);
	void OnSelectResetDone(bool bSuccess);
	void OnSelectMatchDone(bool bSuccess);
	void OnSelectDone(bool bSuccess);
	bool ProcessFind(TDoneNotification cbDone);
	bool ProcessFindNextTriplet();
	void OnFindResetDone(bool bSuccess);
	void OnFindSearchDone(bool bSuccess);
	void OnFindTripletDone(bool bSuccess, uint8_t uiStatus);
	void ProcessRomDone();
	void ProcessRomError();
	typedef enum {
		SKIP_ROM         = 0xCC,
		MATCH_ROM        = 0x55,
		READ_ROM         = 0x33,
		SEARCH_ROM       = 0xF0,
	} ROM_CMD;
	enum {
		OpSelect,
		OpDetectReset,
		OpDetectReadRom,
		OpDetectRead,
		OpSelectReset,
		OpSelectSkipRom,
		OpSelectMatchRom,
		OpFindReset,
		OpFindSearchRom,
		OpFindBits,
		OpNone,
	}                     m_eRomOp;
	TDoneNotification     m_cbRomDone;

	int m_nLastDiscrepancyBitIndex;
	int LastFamilyDiscrepancy;
	bool m_bFindLastDevice;
	uint8_t crc8;
	int id_bit_number;
	int last_zero, rom_byte_number;
	uint8_t rom_byte_mask;
};

#endif /* _1WIRE_ROM_H_INCLUDED */
