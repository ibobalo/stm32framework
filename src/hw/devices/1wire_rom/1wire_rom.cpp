#include "stdafx.h"
#include "../1wire_rom/1wire_rom.h"
#include "hw/debug.h"
#include "util/atomic.h"
#include "util/endians.h"
#include "util/crc.h"
#include "util/bitops.h"

//#define ROM_DEBUG_INFO(...) DEBUG_INFO("1WROM:" __VA_ARGS__)
#define ROM_DEBUG_INFO(...) {}

void CDevice1WROM::Init(IDevice1WMaster* p1W)
{
	m_eRomOp = OpNone;
	m_p1W = p1W;
	m_uiSelectedID = 0;
	m_uiDetectedID = 0;
}

/*virtual*/
bool CDevice1WROM::SelectID(uint64_t ID)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::SelectID(ID));
	if (!AtomicCmpSet(&m_eRomOp, OpNone, OpSelect)) {
		ROM_DEBUG_INFO("CMD: SelectID busy:", m_eRomOp);
		return false;
	}
	ROM_DEBUG_INFO("CMD: Select LO:", ID);
	ROM_DEBUG_INFO("CMD: Select HI:", ID >> 32);
	m_uiSelectedID = htolell(ID);
	m_eRomOp = OpNone;
	return true;
}

/*virtual*/
void CDevice1WROM::GetSelectedID(uint64_t& retID) const
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::GetSelectedID(retID));
	retID = letohll(m_uiSelectedID);
}

/*virtual*/
bool CDevice1WROM::GetDetectedID(uint64_t& retID) const
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::GetDetectedID(retID));
	retID = letohll(m_uiDetectedID);
	return retID != 0;
}

/*virtual*/
bool CDevice1WROM::Detect(TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::Detect(cbDone));
	if (!m_p1W->IsReady()) {
		ROM_DEBUG_INFO("CMD: FindFirst 1W not ready");
		return false;
	}
	if (!AtomicCmpSet(&m_eRomOp, OpNone, OpDetectReset)) {
		ROM_DEBUG_INFO("CMD: Detect busy:", m_eRomOp);
		return false;
	}
	ROM_DEBUG_INFO("CMD: Detect");
	ASSERTE(!m_cbRomDone);
	m_cbRomDone = cbDone;
	if (!m_p1W->BusResetDetect(BIND_MEM_CB(&CDevice1WROM::OnDetectResetDone, this))) {
		ROM_DEBUG_INFO("DetectReset 1W busy");
		ASSERT("busy");
		m_eRomOp = OpNone;
		m_cbRomDone = NullCallback();
		return false;
	}
	return true;
}

/*virtual*/
bool CDevice1WROM::Select(TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::Select(cbDone));
	if (!m_p1W->IsReady()) {
		ROM_DEBUG_INFO("CMD: Select 1W not ready");
		return false;
	}
	if (!AtomicCmpSet(&m_eRomOp, OpNone, OpSelectReset)) {
		ROM_DEBUG_INFO("CMD: Select busy:", m_eRomOp);
		return false;
	}
	ROM_DEBUG_INFO("CMD: Select");
	ASSERTE(!m_cbRomDone);
	m_cbRomDone = cbDone;
	if (!m_p1W->BusResetDetect(BIND_MEM_CB(&CDevice1WROM::OnSelectResetDone, this))) {
		ROM_DEBUG_INFO("DetectReset 1W busy");
		ASSERT("busy");
		m_eRomOp = OpNone;
		m_cbRomDone = NullCallback();
		return false;
	}
	return true;
}

/*virtual*/
bool CDevice1WROM::FindFirst(TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::FindFirst(cbDone));
	if (!m_p1W->IsReady()) {
		ROM_DEBUG_INFO("CMD: FindFirst 1W not ready");
		return false;
	}
	if (!AtomicCmpSet(&m_eRomOp, OpNone, OpFindReset)) {
		ROM_DEBUG_INFO("CMD: FindFirst busy:", m_eRomOp);
		return false;
	}
	ROM_DEBUG_INFO("CMD: FindFirst");
	m_nLastDiscrepancyBitIndex = 0;
	m_bFindLastDevice = false;
	LastFamilyDiscrepancy = 0;
	return ProcessFind(cbDone);
}

/*virtual*/
bool CDevice1WROM::FindNext(TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevice1WROM::FindNext(cbDone));
	if (!m_p1W->IsReady()) {
		ROM_DEBUG_INFO("CMD: FindFirst 1W not ready");
		return false;
	}
	if (!AtomicCmpSet(&m_eRomOp, OpNone, OpFindReset)) {
		ROM_DEBUG_INFO("CMD: FindNext busy");
		return false;
	}
	ROM_DEBUG_INFO("CMD: FindNext");
	return ProcessFind(cbDone);
}

void CDevice1WROM::OnDetectResetDone(bool bSuccess)
{
	ROM_DEBUG_INFO("DetectReset done:", bSuccess?1:0);
	if (bSuccess) {
		m_eRomOp = OpDetectReadRom;
		if (!m_p1W->TxByte(ROM_CMD::READ_ROM, BIND_MEM_CB(&CDevice1WROM::OnDetectReadRomDone, this))) {
			ROM_DEBUG_INFO("DetectReadRom 1W busy");
			ASSERT("busy");
			m_eRomOp = OpNone;
			ProcessRomError();
		}
	} else {
		m_eRomOp = OpNone;
		ProcessRomError();
	}
}

void CDevice1WROM::OnDetectReadRomDone(bool bSuccess)
{
	ROM_DEBUG_INFO("DetectReadRom done:", bSuccess?1:0);
	if (bSuccess) {
		m_eRomOp = OpDetectRead;
		if (!m_p1W->RxBytes(m_auiIDRxBuff, sizeof(m_auiIDRxBuff), BIND_MEM_CB(&CDevice1WROM::OnDetectDone, this))) {
			ROM_DEBUG_INFO("DetectRead 1W busy");
			ASSERT("busy");
			m_eRomOp = OpNone;
			ProcessRomError();
		}
	} else {
		m_eRomOp = OpNone;
		ProcessRomError();
	}
}

void CDevice1WROM::OnDetectDone(bool bSuccess)
{
	ROM_DEBUG_INFO("DetectRead done");
	m_eRomOp = OpNone;
	if (bSuccess && CRC8_DALLAS::Calculate(m_auiIDRxBuff, 7) == m_auiIDRxBuff[7]) {
		ROM_DEBUG_INFO("Detect LO:", m_uiDetectedID);
		ROM_DEBUG_INFO("Detect HI:", m_uiDetectedID >> 32);
		ProcessRomDone();
	} else {
		ProcessRomError();
	}
}

void CDevice1WROM::OnSelectResetDone(bool bSuccess)
{
	ROM_DEBUG_INFO("SelectReset done:", bSuccess?1:0);
	if (bSuccess) {
		if (!(m_uiSelectedID
				? ((m_eRomOp = OpSelectMatchRom), m_p1W->TxByte(ROM_CMD::MATCH_ROM, BIND_MEM_CB(&CDevice1WROM::OnSelectMatchDone, this)))
				: ((m_eRomOp = OpSelectSkipRom),  m_p1W->TxByte(ROM_CMD::SKIP_ROM,  BIND_MEM_CB(&CDevice1WROM::OnSelectDone, this)))
		)) {
			ROM_DEBUG_INFO("Match/Skip Rom 1W busy");
			ASSERT("busy");
			m_eRomOp = OpNone;
			ProcessRomError();
		}
	} else {
		m_eRomOp = OpNone;
		ProcessRomError();
	}
}

void CDevice1WROM::OnSelectMatchDone(bool bSuccess)
{
	ROM_DEBUG_INFO("SelectMatch done");
	if (bSuccess) {
		if (!m_p1W->TxBytes(&m_uiSelectedID, sizeof(m_uiSelectedID), BIND_MEM_CB(&CDevice1WROM::OnSelectDone, this))) {
			ROM_DEBUG_INFO("Match ID 1W busy");
			ASSERT("busy");
			m_eRomOp = OpNone;
			ProcessRomError();
		}
	} else {
		m_eRomOp = OpNone;
		ProcessRomError();
	}
}

void CDevice1WROM::OnSelectDone(bool bSuccess)
{
	ROM_DEBUG_INFO("Select done");
	m_eRomOp = OpNone;
	if (bSuccess) {
		ProcessRomDone();
	} else {
		ProcessRomError();
	}
}

bool CDevice1WROM::ProcessFind(TDoneNotification cbDone)
{
	id_bit_number = 1;
	last_zero = 0;
	rom_byte_number = 0;
	rom_byte_mask = 1;
	crc8 = CRC8_DALLAS::Init();
	ASSERTE(!m_cbRomDone);
	m_cbRomDone = cbDone;
	if (m_bFindLastDevice) {
		ROM_DEBUG_INFO("Find 1W last device");
		ProcessRomError();
		return true;
	}
	if (!m_p1W->BusResetDetect(BIND_MEM_CB(&CDevice1WROM::OnFindResetDone, this))) {
		ROM_DEBUG_INFO("FindReset 1W busy");
		ASSERT("busy");
		m_eRomOp = OpNone;
		m_cbRomDone = NullCallback();
		return false;
	}
	return true;
}

bool CDevice1WROM::ProcessFindNextTriplet()
{
	// if this discrepancy if before the Last Discrepancy
	// on a previous next then pick the same as last time
	bool search_direction;
	if (id_bit_number == m_nLastDiscrepancyBitIndex) search_direction = 1;
	else if (id_bit_number > m_nLastDiscrepancyBitIndex) search_direction = 0;
	else search_direction = CheckBitsAny(m_auiIDRxBuff[rom_byte_number], rom_byte_mask);
	ROM_DEBUG_INFO("Find: Triplet, dir:", search_direction?1:0);
	if (!m_p1W->Triplet(search_direction, BIND_MEM_CB(&CDevice1WROM::OnFindTripletDone, this))) {
		ROM_DEBUG_INFO("FindBits 1W busy, bit:", id_bit_number);
		ASSERT("busy");
		m_eRomOp = OpNone;
		ProcessRomError();
		return false;
	}
	return true;
}

void CDevice1WROM::OnFindResetDone(bool bSuccess)
{
	ROM_DEBUG_INFO("FindReset done:", bSuccess?1:0);
	if (bSuccess) {
		m_eRomOp = OpFindSearchRom;
		if (!m_p1W->TxByte(ROM_CMD::SEARCH_ROM, BIND_MEM_CB(&CDevice1WROM::OnFindSearchDone, this))) {
			ROM_DEBUG_INFO("FindSearchRom 1W busy");
			ASSERT("busy");
			m_eRomOp = OpNone;
			ProcessRomError();
		}
	} else {
		m_eRomOp = OpNone;
		ProcessRomError();
	}
}

void CDevice1WROM::OnFindSearchDone(bool bSuccess)
{
	ROM_DEBUG_INFO("FindSearch done:", bSuccess?1:0);
	if (bSuccess) {
		m_eRomOp = OpFindBits;
		ProcessFindNextTriplet();
	} else {
		m_eRomOp = OpNone;
		ProcessRomError();
	}
}

void CDevice1WROM::OnFindTripletDone(bool bSuccess, uint8_t uiTripletResult)
{
	ROM_DEBUG_INFO("FindTriplet done:", bSuccess?1:0);
	if (bSuccess) {
		// check bit results in status byte
		bool bIdBit = (uiTripletResult & IDevice1WMaster::TRIPLET_FIRST_BIT);
		bool bCmpBit = (uiTripletResult & IDevice1WMaster::TRIPLET_SECOND_BIT);
		bool bChosenBit = (uiTripletResult & IDevice1WMaster::TRIPLET_CHOSEN_BIT);

		// check for no devices on 1-Wire
		if (bIdBit && bCmpBit) {
			ROM_DEBUG_INFO("Find: No devices detected on 1-Wire bus");
			ProcessRomError();
		} else {
			if (!bIdBit && !bCmpBit) {
				ROM_DEBUG_INFO("Find: conflict at bit:", id_bit_number);
				ROM_DEBUG_INFO("Find: Chosen:", bChosenBit?1:0);
				if (!bChosenBit) {
					ROM_DEBUG_INFO("Find: last zero updated:", id_bit_number);
					last_zero = id_bit_number;

					// check for Last discrepancy in family
					if (last_zero < 9)
						LastFamilyDiscrepancy = last_zero;
				}
			} else {
//				ROM_DEBUG_INFO("Find: no conflict at bit:", id_bit_number);
			}

			// set or clear the bit in the ROM byte rom_byte_number
			// with mask rom_byte_mask
			if (bChosenBit == 1) {
				m_auiIDRxBuff[rom_byte_number] |= rom_byte_mask;
			} else {
				m_auiIDRxBuff[rom_byte_number] &= (uint8_t)~rom_byte_mask;
			}

			// increment the byte counter id_bit_number
			// and shift the mask rom_byte_mask
			id_bit_number++;
			rom_byte_mask <<= 1;

			// if the mask is 0 then go to new SerialNum byte rom_byte_number
			// and reset mask
			if (rom_byte_mask == 0) {
				ROM_DEBUG_INFO("Find: ID byte completed:", m_auiIDRxBuff[rom_byte_number]);
				crc8 = CRC8_DALLAS::Accumulate<false>(crc8, m_auiIDRxBuff[rom_byte_number]);  // accumulate the CRC
				rom_byte_number++;
				rom_byte_mask = 1;
			}
		}
		if (rom_byte_number < 8) {
			// next bit
			ProcessFindNextTriplet();
		} else if (!(id_bit_number < 65 || CRC8_DALLAS::Finalize(crc8) != 0)) {
			// search successful so set LastDiscrepancy,LastDeviceFlag
			m_nLastDiscrepancyBitIndex = last_zero;

			// check for last device
			if (m_nLastDiscrepancyBitIndex == 0) {
				m_bFindLastDevice = true;
			}
			ProcessRomDone();
		} else {
			ProcessRomError();
		}
	} else {
		ProcessRomError();
	}
}

void CDevice1WROM::ProcessRomDone()
{
	ROM_DEBUG_INFO("DONE");
	m_eRomOp = OpNone;
	TDoneNotification cbDone = m_cbRomDone;
	m_cbRomDone = NullCallback();
	if (cbDone) cbDone(true);
}

void CDevice1WROM::ProcessRomError()
{
	ROM_DEBUG_INFO("ERROR:");
	m_eRomOp = OpNone;
	m_nLastDiscrepancyBitIndex = 0;
	m_bFindLastDevice = false;
	LastFamilyDiscrepancy = 0;
	TDoneNotification cbDone = m_cbRomDone;
	m_cbRomDone = NullCallback();
	if (cbDone) cbDone(false);
}
