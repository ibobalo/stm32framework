#ifndef DEVICES_H_INCLUDED
#define DEVICES_H_INCLUDED

#ifdef USE_RFM69
#include "../devices/rfm69/dev_rfm69.h"
#endif // USE_RFM69
#ifdef USE_PCA9532
#include "../devices/pca9532/dev_pca9532.h"
#endif // USE_PCA9532
#ifdef USE_SSD1306
#include "../devices/ssd1306/dev_ssd1306.h"
#endif // USE_SSD1306
#ifdef USE_DS2482
#include "../devices/ds2482/dev_ds2482.h"
#endif // USE_DS2482
#ifdef USE_ENC28J60
#include "../devices/enc28j60/dev_enc28j60.h"
#endif // USE_ENC28J60
#ifdef USE_HMC5883
#include "../devices/hmc5883/dev_hmc5883.h"
#endif // USE_HMC5883
#ifdef USE_MPU6050
#include "../devices/mpu6050/dev_mpu6050.h"
#endif // USE_MPU6050
#ifdef USE_AS5040
#include "../devices/as5040/dev_as5040.h"
#endif // USE_AS5040
#ifdef USE_24AA
#include "../devices/eeprom/dev_24aa.h"
#endif // USE_24AA

#endif // DEVICES_H_INCLUDED
