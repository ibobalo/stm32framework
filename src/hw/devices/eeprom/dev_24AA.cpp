#include "stdafx.h"
#include "dev_24AA.h"
#include "hw/debug.h"
#include "hw/mcu/hw_i2c.h"
#include "hw/tick_timeout.h"
#include "util/macros.h"
#include "util/atomic.h"
#include "util/endians.h"
#include "tick_process.h"

#define INITIAL_DELAY             10
#define REFRESH_PERIOD            1000
#define REPEAT_AFTER_ERROR_DELAY  3
#define REPEAT_AFTER_MISSED_DELAY 1000

// I2C Address is binary [1010XXXw] X=AD2-0 w=1-read/0-write
#define I2C_24AA_ADDRESS         0x50 // I2C Bus Address (7bit)


#define EEPROM_DEBUG_INFO(...) DEBUG_INFO("24AA:" __VA_ARGS__)
//#define EEPROM_DEBUG_INFO(...) {}

class CI2CDeviceEEPROM24AA :
		public IDeviceEEPROMI2C,
		public IDeviceI2C,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;

public:
//	virtual ~CI2CDeviceEEPROM24AA();

public: // IDeviceEEPROMI2C
	virtual void Init(II2C* pI2C, uint8_t uiAD);

public: // IDeviceStorage
	virtual bool StartRead(unsigned uiAddress, const CChainedIoChunks* pChunksToRead, CReadDoneCallback cb);
	virtual bool StartWrite(unsigned uiAddress, const CChainedConstChunks* pChunksToWrite, CWriteDoneCallback cb);

public: // IDeviceEEPROM
	virtual unsigned GetCapacity() const;
	virtual unsigned GetPageSize() const;

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime);

private:
	enum {
		EEPROMPageSize = 8,
		EEPROMCapacity = 1024,
	};
	typedef enum {
		StateCheck,
		StateIdle,
		StateRead,
		StateWrite,
		StateWriteWait,
	} EState;
	EState              m_eState;
	II2C*               m_pI2C;
	uint8_t             m_uiI2CAddress;
	unsigned            m_uiAddressToRead;
	CChainedIoChunks    m_cicChunksToRead;
	unsigned            m_uiLengthToRead;
	CReadDoneCallback   m_cbReadDoneCallback;
	unsigned            m_uiAddressToWrite;
	unsigned            m_uiLengthToWrite;
	CChainedConstChunks m_cccChunksToWrite;
	CReadDoneCallback   m_cbWriteDoneCallback;
	unsigned            m_nErrors;

private:
	struct {
		uint8_t uiReadAddress;
	} m_ReadPkt;
	CChainedConstChunks m_cccReadPktChunk;
	struct {
		uint8_t uiWriteAddress;
		uint8_t auiWriteData[EEPROMPageSize];
	} m_WritePkt;
	CChainedConstChunks m_cccWritePktChunk;
};


void CI2CDeviceEEPROM24AA::Init(II2C* pI2C, uint8_t uiAD)
{
	CParentProcess::Init(&g_TickProcessScheduller);

	m_eState = StateCheck;
	m_pI2C = pI2C;
	m_uiI2CAddress = I2C_24AA_ADDRESS | (uiAD & 0x07);
	m_cccReadPktChunk.Assign((const uint8_t*)&m_ReadPkt, sizeof(m_ReadPkt));
	m_cccReadPktChunk.Break();
	m_cccWritePktChunk.Assign((const uint8_t*)&m_WritePkt, 0);
	m_cccWritePktChunk.Break();
	m_nErrors = 0;

	m_pI2C->Queue_I2C_Session(this);
}

/*virtual*/
bool CI2CDeviceEEPROM24AA::StartRead(unsigned uiAddress, const CChainedIoChunks* pChunksToRead, CReadDoneCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceStorage::StartRead(uiAddress, pChunksToRead, cb));
	if (!AtomicCmpSet(&m_eState, StateIdle, StateRead)) return false;
	EEPROM_DEBUG_INFO("Start read A.L:", (uiAddress << 16) | CollectChainTotalLength(pChunksToRead));
	m_uiAddressToRead = uiAddress;
	m_cicChunksToRead = *pChunksToRead;
	m_cbReadDoneCallback = cb;
	m_pI2C->Queue_I2C_Session(this);
	return true;
}

/*virtual*/
bool CI2CDeviceEEPROM24AA::StartWrite(unsigned uiAddress, const CChainedConstChunks* pChunksToWrite, CWriteDoneCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceStorage::StartWrite(uiAddress, pChunksToWrite, cb));
	if (!AtomicCmpSet(&m_eState, StateIdle, StateWrite)) return false;
	EEPROM_DEBUG_INFO("Start write A.L:", (uiAddress << 16) | CollectChainTotalLength(pChunksToWrite));
	m_uiAddressToWrite = uiAddress;
	m_cccChunksToWrite = *pChunksToWrite;
	m_cbWriteDoneCallback = cb;
	m_pI2C->Queue_I2C_Session(this);
	return true;
}

/*virtual*/
unsigned CI2CDeviceEEPROM24AA::GetCapacity() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceEEPROM::GetCapacity());
	return EEPROMCapacity;
}

/*virtual*/
unsigned CI2CDeviceEEPROM24AA::GetPageSize() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceEEPROM::GetPageSize());
	return EEPROMPageSize;
}

/*virtual*/
unsigned CI2CDeviceEEPROM24AA::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	unsigned uiAddress = (
			m_eState == StateRead ? m_uiAddressToRead : (
			m_eState == StateWrite ? m_uiAddressToWrite : (
			0)));
	uint8_t uiPageAddress = uiAddress >> 8;
	return m_uiI2CAddress | (uiPageAddress & 0x07);
}

/*virtual*/
bool CI2CDeviceEEPROM24AA::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));

	switch(m_eState) {
	case StateCheck:
		*ppRetChunksToSend = NULL;
		*ppRetChunksToRecv = NULL;
		EEPROM_DEBUG_INFO("Check pkt");
		break;
	case StateIdle:
		ASSERT("Invalid State");
		break;
	case StateRead:
		m_ReadPkt.uiReadAddress = m_uiAddressToRead & 0xFF;
		*ppRetChunksToSend = &m_cccReadPktChunk;
		*ppRetChunksToRecv = &m_cicChunksToRead;
		EEPROM_DEBUG_INFO("Read pkt, A:", m_ReadPkt.uiReadAddress);
		break;
	case StateWrite:
		m_WritePkt.uiWriteAddress = m_uiAddressToWrite & 0xFF;
		m_cccWritePktChunk.uiLength = 1 + CopyChainData(&m_cccChunksToWrite, (uint8_t*)m_WritePkt.auiWriteData, EEPROMPageSize - (m_uiAddressToWrite % EEPROMPageSize));
		*ppRetChunksToSend = &m_cccWritePktChunk;
		*ppRetChunksToRecv = NULL;
		EEPROM_DEBUG_INFO("Write pkt, l:", m_cccWritePktChunk.uiLength);
		break;
	case StateWriteWait:
		*ppRetChunksToSend = NULL;
		*ppRetChunksToRecv = NULL;
		EEPROM_DEBUG_INFO("Write wait");
		break;
	}

	return true;
}

/*virtual*/
void CI2CDeviceEEPROM24AA::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());

	switch(m_eState) {
	case StateCheck:
		EEPROM_DEBUG_INFO("Done check");
		m_nErrors = 0;
		if (AtomicCmpSet(&m_eState, StateCheck, StateIdle)) {
		} else {
			ASSERT("state change fault");
		}
		break;
	case StateIdle:
		ASSERT("Invalid State");
		break;
	case StateRead:
		EEPROM_DEBUG_INFO("Done read :", m_ReadPkt.uiReadAddress);
		if (AtomicCmpSet(&m_eState, StateRead, StateIdle)) {
		} else {
			ASSERT("state change fault");
		}
		if (m_cbReadDoneCallback) m_cbReadDoneCallback(true);
		break;
	case StateWrite:
		EEPROM_DEBUG_INFO("Done write:", m_cccWritePktChunk.uiLength - 1);
		if (AtomicCmpSet(&m_eState, StateWrite, StateWriteWait)) {
		} else {
			ASSERT("state change fault");
		}

		{
			unsigned l = m_cccWritePktChunk.uiLength - 1;
			m_uiAddressToWrite += l;
			if (DiscardChainData(&m_cccChunksToWrite, l) != l) {
				ASSERT("Discard fault");
			}
		}

		EEPROM_DEBUG_INFO("Wait req");
		ContinueDelay(6);
		break;
	case StateWriteWait:
		if (m_cccChunksToWrite.uiLength) {
			EEPROM_DEBUG_INFO("Continue write:", m_cccChunksToWrite.uiLength);
			if (AtomicCmpSet(&m_eState, StateWriteWait, StateWrite)) {
			} else {
				ASSERT("state change fault");
			}
			m_pI2C->Queue_I2C_Session(this);
		} else {
			EEPROM_DEBUG_INFO("Done wait");
			if (AtomicCmpSet(&m_eState, StateWriteWait, StateIdle)) {
			} else {
				ASSERT("state change fault");
			}
			if (m_cbWriteDoneCallback) m_cbWriteDoneCallback(true);
		}
		break;
	}
}

/*virtual*/
void CI2CDeviceEEPROM24AA::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	switch(m_eState) {
	case StateCheck:
		EEPROM_DEBUG_INFO("Error check:", e);
		if (m_nErrors < 100) {
			++m_nErrors;
			ContinueDelay(10);
		} else if (m_nErrors < 1000) {
			++m_nErrors;
			ContinueDelay(1000);
		}
		break;
	case StateIdle:
		ASSERT("Invalid State");
		break;
	case StateRead:
		EEPROM_DEBUG_INFO("Error read:", e);
		if (AtomicCmpSet(&m_eState, StateRead, StateCheck)) {
		} else {
			ASSERT("state change fault");
		}
		if (m_cbReadDoneCallback) m_cbReadDoneCallback(false);
		ContinueNow();
		break;
	case StateWrite:
		EEPROM_DEBUG_INFO("Error write:", e);
		if (AtomicCmpSet(&m_eState, StateWrite, StateCheck)) {
		} else {
			ASSERT("state change fault");
		}
		if (m_cbWriteDoneCallback) m_cbWriteDoneCallback(false);
		ContinueNow();
		break;
	case StateWriteWait:
		if (e == I2C_ERROR_ACK) {
			EEPROM_DEBUG_INFO("Still write wait");
			m_pI2C->RequestI2CIdleNotify(this);
		} else {
			EEPROM_DEBUG_INFO("Error write wait:", e);
			if (AtomicCmpSet(&m_eState, StateWriteWait, StateCheck)) {
			} else {
				ASSERT("state change fault");
			}
			ContinueDelay(10);
		}
	}
}

/*virtual*/
void CI2CDeviceEEPROM24AA::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());

	switch (m_eState) {
	case StateCheck:
	case StateIdle:
	case StateRead:
	case StateWrite:
		ASSERT("Invalid State");
		break;
	case StateWriteWait:
		EEPROM_DEBUG_INFO("Retry write wait");
		m_pI2C->Queue_I2C_Session(this);
		break;
	}
}

/*virtual*/
void CI2CDeviceEEPROM24AA::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	switch (m_eState)
	{
	case StateCheck:
		EEPROM_DEBUG_INFO("Retry check");
		m_pI2C->Queue_I2C_Session(this);
		break;
	case StateIdle:
	case StateRead:
	case StateWrite:
		ASSERT("Invalid State");
		break;
	case StateWriteWait:
		EEPROM_DEBUG_INFO("Try write wait");
		m_pI2C->Queue_I2C_Session(this);
		break;
	}
}

CI2CDeviceEEPROM24AA       g_EEPROM24AA;

IDeviceEEPROMI2C* AcquireEEPROM24AAInterface(II2C* pI2C, uint8_t uiAD)
{
	g_EEPROM24AA.Init(pI2C, uiAD);
	return &g_EEPROM24AA;
}
