#ifndef EEPROM_H_INCLUDED
#define EEPROM_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include "util/callback.h"
#include <stdint.h>

class IDeviceStorage
{
public:
	typedef Callback<void (bool)> CReadDoneCallback;
	typedef Callback<void (bool)> CWriteDoneCallback;
public:
//	virtual ~IDeviceStorage();

public: // IDeviceStorage
	virtual bool StartRead(unsigned uiAddress, const CChainedIoChunks* pChunksToRead, CReadDoneCallback cb) = 0;
	virtual bool StartWrite(unsigned uiAddress, const CChainedConstChunks* pChunksToWrite, CWriteDoneCallback cb) = 0;
};

class IDeviceEEPROM :
		public IDeviceStorage
{
public: // IDeviceEEPROM
	virtual unsigned GetCapacity() const = 0;
	virtual unsigned GetPageSize() const = 0;

};

#endif //EEPROM_H_INCLUDED
