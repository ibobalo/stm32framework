#ifndef DEV_24AA_H_INCLUDED
#define DEV_24AA_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include <stdint.h>
#include "eeprom.h"

class IDeviceEEPROMI2C :
	public IDeviceEEPROM
{
public:
//	virtual ~IDeviceEEPROMI2C();
};

IDeviceEEPROMI2C* AcquireEEPROM24AAInterface(II2C* pI2C, uint8_t uiAD);

#endif //DEV_24AA_H_INCLUDED
