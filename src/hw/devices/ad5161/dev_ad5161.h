#ifndef DEV_AD5161_H_INCLUDED
#define DEV_AD5161_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include <stdint.h>

class IDeviceAD5161
{
public:
//	virtual ~IDeviceAD5161();

	virtual void Init(II2C* pI2C, bool bAD0 = false) = 0;

public: // IDeviceAD5161
	virtual void SetDAC(uint8_t uiDACValue) = 0;
	virtual void Reset(bool bReset) = 0;
	virtual void Shutdown(bool bReset) = 0;
};

IDeviceAD5161* GetAD5161Interface();

#endif //DEV_AD5161_H_INCLUDED
