#include "stdafx.h"
#include "dev_ad5161.h"
#include "hw/debug.h"
#include "hw/mcu/hw_i2c.h"
#include "hw/tick_timeout.h"
#include "util/macros.h"
#include "util/endians.h"
#include "tick_process.h"

#define INITIAL_DELAY             10
#define REFRESH_PERIOD            1000
#define REPEAT_AFTER_ERROR_DELAY  3
#define REPEAT_AFTER_MISSED_DELAY 1000

// I2C Address is binary [110100Xw] X=AD0 w=1-read/0-write
#define AD5161_ADDRESS         0x2C // I2C Bus Address (7bit)
#define AD5161_INSTR_RESET     0x40
#define AD5161_INSTR_SHUTDOWN  0x20



#define AD5161_DEBUG_INFO(...) DEBUG_INFO("AD5161:" __VA_ARGS__)
//#define AD5161_DEBUG_INFO(...) {}

class CI2CDeviceAD5161 :
		public IDeviceAD5161,
		public IDeviceI2C,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
public:
//	virtual ~CI2CDeviceAD5161();

public: // IDeviceAD5161
	virtual void Init(II2C* pI2C, bool bAD0 = false);

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

public: // IDeviceAD5161
	virtual void SetDAC(uint8_t uiDACValue);
	virtual void Reset(bool bReset);
	virtual void Shutdown(bool bReset);

private:
	II2C*             m_pI2C;
	uint8_t           m_uiAddress;
	bool              m_bCommOk;
	uint8_t           m_uiDACValue;
	bool              m_bReset;
	bool              m_bShutdown;


	struct {
		volatile uint8_t  m_uiData;
	} m_tReadPkt;
	struct {
		uint8_t uiInstruction;
		uint8_t uiData;
		void Assign(uint8_t bReset, bool bShutdown, uint8_t uiDacValue) {uiInstruction = 0 | (bReset?AD5161_INSTR_RESET:0) | (bShutdown?AD5161_INSTR_SHUTDOWN:0); uiData = uiDacValue;};
	} m_tWritePkt;

	bool m_bBusy;
	bool m_bWriteRequired;
	bool m_bReadRequired;

	CChainedIoChunks m_cicReadChunks;
	CChainedConstChunks m_cccWritePktChunks;
};


void CI2CDeviceAD5161::Init(II2C* pI2C, bool bAD0/* = false*/)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceAD5161::Init(pI2C, bAD0));

	CParentProcess::Init(&g_TickProcessScheduller);
	m_uiDACValue = 0;
	m_bReset = false;
	m_bShutdown = false;

	m_pI2C = pI2C;
	m_uiAddress = AD5161_ADDRESS | (bAD0 ? 0x01 : 0x00);
	m_bCommOk = false;
	m_cicReadChunks.Assign((volatile uint8_t*)&m_tReadPkt, sizeof(m_tReadPkt));
	m_cicReadChunks.Break();
	m_cccWritePktChunks.Assign((const uint8_t*)&m_tWritePkt, sizeof(m_tWritePkt));
	m_cccWritePktChunks.Break();


	m_bWriteRequired = true;
	m_bReadRequired = true;
	m_bBusy = true;
	m_pI2C->Queue_I2C_Session(this);
}

void CI2CDeviceAD5161::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	if (!m_bBusy && (m_bWriteRequired || m_bReadRequired)) {
		AD5161_DEBUG_INFO("Session started r0w:", (m_bReadRequired?4:0) + (m_bWriteRequired?1:0));
		m_bBusy = true;
		m_pI2C->Queue_I2C_Session(this);
	}
}

unsigned CI2CDeviceAD5161::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_uiAddress;
}

bool CI2CDeviceAD5161::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));

	ASSERTE(m_bBusy);

	if (m_bWriteRequired) {
		m_bWriteRequired = false;
		m_tWritePkt.Assign(m_bReset, m_bShutdown, m_uiDACValue);
		*ppRetChunksToSend = &m_cccWritePktChunks;
		AD5161_DEBUG_INFO("Pkt to write:", (m_tWritePkt.uiInstruction <<  8) | m_tWritePkt.uiData);
		m_bReadRequired = !m_bShutdown;
	} else if (m_bReadRequired) {
		m_bReadRequired = false;
		*ppRetChunksToRecv = &m_cicReadChunks;
		AD5161_DEBUG_INFO("Request read");
	} else {
		ASSERT("empty session");
		AD5161_DEBUG_INFO("Session end");
		return false;
	}

	return true;
}

/*virtual*/
void CI2CDeviceAD5161::SetDAC(uint8_t uiDACValue)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceAD5161::SetDAC(uiDACValue));
	if (m_uiDACValue != uiDACValue) {
		m_uiDACValue = uiDACValue;
		AD5161_DEBUG_INFO("DAC:", uiDACValue);
		m_bWriteRequired = true;
		ContinueNow();
	}
}

/*virtual*/
void CI2CDeviceAD5161::Reset(bool bReset)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceAD5161::Reset(bReset));
	if (m_bReset != bReset) {
		m_bReset = bReset;
		AD5161_DEBUG_INFO("Reset:", bReset ? 0 : 1);
		m_bWriteRequired = true;
		ContinueNow();
	}
}

/*virtual*/
void CI2CDeviceAD5161::Shutdown(bool bShutdown)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceAD5161::Shutdown(bShutdown));
	if (m_bShutdown != bShutdown) {
		m_bShutdown = bShutdown;
		AD5161_DEBUG_INFO("Shutdown:", bShutdown ? 0 : 1);
		m_bWriteRequired = true;
		ContinueNow();
	}
}

void CI2CDeviceAD5161::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());

	ASSERTE(m_bBusy);

	m_bBusy = false;
	m_bCommOk = true;
	if (m_bReadRequired || m_bWriteRequired) {
		AD5161_DEBUG_INFO("Pkt done, do next r0w:", (m_bReadRequired?4:0) + (m_bWriteRequired?1:0));
		ContinueNow();
	} else if (m_tReadPkt.m_uiData != m_uiDACValue && !m_bShutdown) {
		AD5161_DEBUG_INFO("Data not match:", m_tReadPkt.m_uiData);
		m_bWriteRequired = true;
		ContinueNow();
	} else {
		AD5161_DEBUG_INFO("Pkt done, refresh delay");
		m_bWriteRequired = true; // force refresh
		ContinueDelay(REFRESH_PERIOD);
	}
}

void CI2CDeviceAD5161::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	ASSERTE(m_bBusy);

	m_bCommOk = false;
	m_bBusy = false;
	m_bWriteRequired = true;
	m_bReadRequired = true;

	if (e == I2C_ERROR_ACK) {
		DEBUG_INFO("AD5161: Missed. Addr:", m_uiAddress);
		m_bCommOk = false;
		ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
	} else {
		DEBUG_INFO("AD5161: error");
		ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
	}
}

void CI2CDeviceAD5161::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
}

CI2CDeviceAD5161       g_AD5161;

IDeviceAD5161* GetAD5161Interface() {return &g_AD5161;};
