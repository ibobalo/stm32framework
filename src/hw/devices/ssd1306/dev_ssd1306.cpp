#include "stdafx.h"
#include "dev_ssd1306.h"
#include "hw/debug.h"
#include "hw/mcu/hw_i2c.h"
#include "hw/tick_timeout.h"
#include "util/macros.h"
#include "util/atomic.h"
#include "tick_process.h"
#include <string.h>

#define I2C_ADDR                  0x3C
#define I2C_ADDR_ALT              0x3D
#define INITIAL_DELAY             10
#define REPEAT_AFTER_ERROR_DELAY  500
#define IDLE_DELAY                1000
#define SSD1306_WIDTH             128
#define SSD1306_HEIGHT            64
#define SSD1306_SIZE              (SSD1306_WIDTH * SSD1306_HEIGHT / 8)
#define SSD1306_PAGE_HEIGHT       8
#define SSD1306_PAGES             (SSD1306_HEIGHT / SSD1306_PAGE_HEIGHT)
#define SSD1306_PAGE_SIZE         SSD1306_WIDTH
#define SSD1306_ZONES             8
#define SSD1306_ZONE_SIZE         (SSD1306_SIZE / SSD1306_ZONES)
#define SSD1306_UPDATEALL         0xFF
#define SSD1306_LINE_HEIGHT       8
#define SSD1306_LINES             (SSD1306_HEIGHT / SSD1306_LINE_HEIGHT)
#define SSD1306_LINE_SIZE         SSD1306_WIDTH
#define SSD1306_COLUMNS           (SSD1306_WIDTH / SSD1306_CHAR_WIDTH)
#ifndef SSD1306_ORIENTATION_FFC_DOWN
# define SSD1306_ORIENTATION_FFC_UP
#endif

//#define SSD1306_DEBUG_INFO(...) DEBUG_INFO("SSD1306:" __VA_ARGS__)
#define SSD1306_DEBUG_INFO(...) {}

#define CMD_CO_BIT(x) IF_ELSE(x)(0x80)(0x00)
#define CMD_DC_BIT(x) IF_ELSE(x)(0x40)(0x00)
//#define cmd(code,...) (CMD_CO_BIT(0) | CMD_DC_BIT(0)), code WHEN(NOT(CHECK_EMPTY(__VA_ARGS__)))(, (CMD_CO_BIT(0) | CMD_DC_BIT(0)), __VA_ARGS__)
#define cmd(code,...) (CMD_CO_BIT(0) | CMD_DC_BIT(0)), code WHEN(NOT(CHECK_EMPTY(__VA_ARGS__)))(, (CMD_CO_BIT(0) | CMD_DC_BIT(0)), __VA_ARGS__)
static const uint8_t g_auiSSD1306InitCommands[] = {
//		CMD_CO_BIT(1) | CMD_DC_BIT(0),
		cmd( 0xAE ),       //display off
		cmd( 0x20, 0x00 ), //Set Memory Addressing Mode: 00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
		cmd( 0xB0 ),           //Set Page Start Address for Page Addressing Mode,0-7
#ifdef SSD1306_ORIENTATION_FFC_UP
		cmd( 0xA0 ),       //--set segment re-map 127 to 0
		cmd( 0xC8 ),       //Set COM Output Scan Direction
#else
		cmd( 0xA1 ),       //--set segment re-map 0 to 127
		cmd( 0xC0 ),       //Set COM Output Scan Direction
#endif
		cmd( 0x00 ),	   //---set low column address
		cmd( 0x10 ),       //---set high column address
		cmd( 0x40 ),       //--set start line address
		cmd( 0x81, 0xFF ), //--set contrast control register
		cmd( 0xA6 ),       //--set normal display
		cmd( 0xA8, 0x3F ), //--set multiplex ratio(1 to 64)
		cmd( 0xA4 ),       //0xa4,Output follows RAM content;0xa5,Output ignores RAM content
		cmd( 0xD3, 0x00 ), //--set display offset -not offset
		cmd( 0xD5, 0xF0 ), //--set display clock divide ratio[3:0] / oscillator frequency[7:4]
		cmd( 0xD9, 0x22 ), //--set pre-charge period
		cmd( 0xDA, 0x12 ), //--set [4] alternative com pins hardware configuration / [5] disable com L/R remap
		cmd( 0xDB, 0x20 ), //--set vcomh = 0x20, //0x20,0.77xVcc
		cmd( 0x8D, 0x14 ), //--Enable Charge Pump
		cmd( 0xAF ),       //--turn on SSD1306 panel
};

#define SSD1306_SPACE_WIDTH  (SSD1306_CHAR_WIDTH + 1)
#define SSD1306_CHAR_WIDTH   5
#define SSD1306_CHAR_HEIGHT  8
#define SSD1306_CHAR_FIRST   '!'
static const uint8_t g_auiSSD1306CharMap[] = {
		0x00, 0x00, 0xfa, 0x00, 0x00, /* ! */
		0x00, 0xe0, 0x00, 0xe0, 0x00, /* " */
		0x28, 0xfe, 0x28, 0xfe, 0x28, /* # */
		0x24, 0x54, 0xfe, 0x54, 0x48, /* $ */
		0xc4, 0xc8, 0x10, 0x26, 0x46, /* % */
		0x6c, 0x92, 0x6a, 0x04, 0x0a, /* & */
		0x00, 0x10, 0xe0, 0xc0, 0x00, /* ; */
		0x00, 0x38, 0x44, 0x82, 0x00, /* ( */
		0x00, 0x82, 0x44, 0x38, 0x00, /* ) */
		0x54, 0x38, 0xfe, 0x38, 0x54, /* * */
		0x10, 0x10, 0x7c, 0x10, 0x10, /* + */
		0x00, 0x01, 0x0e, 0x0c, 0x00, /* , */
		0x10, 0x10, 0x10, 0x10, 0x10, /* - */
		0x00, 0x00, 0x06, 0x06, 0x00, /* . */
		0x04, 0x08, 0x10, 0x20, 0x40, /* / */
		0x7c, 0x8a, 0x92, 0xa2, 0x7c, /* 0 */
		0x00, 0x42, 0xfe, 0x02, 0x00, /* 1 */
		0x4e, 0x92, 0x92, 0x92, 0x62, /* 2 */
		0x84, 0x82, 0x92, 0xb2, 0xcc, /* 3 */
		0x18, 0x28, 0x48, 0xfe, 0x08, /* 4 */
		0xe4, 0xa2, 0xa2, 0xa2, 0x9c, /* 5 */
		0x3c, 0x52, 0x92, 0x92, 0x8c, /* 6 */
		0x82, 0x84, 0x88, 0x90, 0xe0, /* 7 */
		0x6c, 0x92, 0x92, 0x92, 0x6c, /* 8 */
		0x62, 0x92, 0x92, 0x94, 0x78, /* 9 */
		0x00, 0x00, 0x28, 0x00, 0x00, /* : */
		0x00, 0x02, 0x2c, 0x00, 0x00, /* : */
		0x00, 0x10, 0x28, 0x44, 0x82, /* < */
		0x28, 0x28, 0x28, 0x28, 0x28, /* = */
		0x00, 0x82, 0x44, 0x28, 0x10, /* > */
		0x40, 0x80, 0x9a, 0x90, 0x60, /* ? */
		0x7c, 0x82, 0xba, 0x9a, 0x72, /* @ */
		0x3e, 0x48, 0x88, 0x48, 0x3e, /* A */
		0xfe, 0x92, 0x92, 0x92, 0x6c, /* B */
		0x7c, 0x82, 0x82, 0x82, 0x44, /* C */
		0xfe, 0x82, 0x82, 0x82, 0x7c, /* D */
		0xfe, 0x92, 0x92, 0x92, 0x82, /* E */
		0xfe, 0x90, 0x90, 0x90, 0x80, /* F */
		0x7c, 0x82, 0x82, 0x8a, 0xce, /* G */
		0xfe, 0x10, 0x10, 0x10, 0xfe, /* H */
		0x00, 0x82, 0xfe, 0x82, 0x00, /* I */
		0x04, 0x02, 0x82, 0xfc, 0x80, /* J */
		0xfe, 0x10, 0x28, 0x44, 0x82, /* K */
		0xfe, 0x02, 0x02, 0x02, 0x02, /* L */
		0xfe, 0x40, 0x38, 0x40, 0xfe, /* M */
		0xfe, 0x20, 0x10, 0x08, 0xfe, /* N */
		0x7c, 0x82, 0x82, 0x82, 0x7c, /* O */
		0xfe, 0x90, 0x90, 0x90, 0x60, /* P */
		0x7c, 0x82, 0x8a, 0x84, 0x7a, /* Q */
		0xfe, 0x90, 0x98, 0x94, 0x62, /* R */
		0x64, 0x92, 0x92, 0x92, 0x4c, /* S */
		0xc0, 0x80, 0xfe, 0x80, 0xc0, /* T */
		0xfc, 0x02, 0x02, 0x02, 0xfc, /* U */
		0xf8, 0x04, 0x02, 0x04, 0xf8, /* V */
		0xfc, 0x02, 0x1c, 0x02, 0xfc, /* W */
		0xc6, 0x28, 0x10, 0x28, 0xc6, /* X */
		0xc0, 0x20, 0x1e, 0x20, 0xc0, /* Y */
		0x86, 0x9a, 0x92, 0xb2, 0xc2, /* Z */
		0x00, 0xfe, 0x82, 0x82, 0x82, /* [ */
		0x40, 0x20, 0x10, 0x08, 0x04, /* \ */
		0x00, 0x82, 0x82, 0x82, 0xfe, /* ] */
		0x20, 0x40, 0x80, 0x40, 0x20, /* ^ */
		0x02, 0x02, 0x02, 0x02, 0x02, /* _ */
		0x00, 0xc0, 0xe0, 0x10, 0x00, /* ` */
		0x04, 0x2a, 0x2a, 0x1e, 0x02, /* a */
		0xfe, 0x14, 0x22, 0x22, 0x1c, /* b */
		0x1c, 0x22, 0x22, 0x22, 0x14, /* c */
		0x1c, 0x22, 0x22, 0x14, 0xfe, /* d */
		0x1c, 0x2a, 0x2a, 0x2a, 0x18, /* e */
		0x00, 0x10, 0x7e, 0x90, 0x40, /* f */
		0x18, 0x25, 0x25, 0x39, 0x1e, /* g */
		0xfe, 0x10, 0x20, 0x20, 0x1e, /* h */
		0x00, 0x22, 0xbe, 0x02, 0x00, /* i */
		0x04, 0x02, 0x02, 0xbc, 0x00, /* j */
		0xfe, 0x08, 0x14, 0x22, 0x00, /* k */
		0x00, 0x82, 0xfe, 0x02, 0x00, /* l */
		0x3e, 0x20, 0x1e, 0x20, 0x1e, /* m */
		0x3e, 0x10, 0x20, 0x20, 0x1e, /* n */
		0x1c, 0x22, 0x22, 0x22, 0x1c, /* o */
		0x3f, 0x18, 0x24, 0x24, 0x18, /* p */
		0x18, 0x24, 0x24, 0x18, 0x3f, /* q */
		0x3e, 0x10, 0x20, 0x20, 0x10, /* r */
		0x12, 0x2a, 0x2a, 0x2a, 0x24, /* s */
		0x20, 0x20, 0xfc, 0x22, 0x24, /* t */
		0x3c, 0x02, 0x02, 0x04, 0x3e, /* u */
		0x38, 0x04, 0x02, 0x04, 0x38, /* v */
		0x3c, 0x02, 0x0c, 0x02, 0x3c, /* w */
		0x22, 0x14, 0x08, 0x14, 0x22, /* x */
		0x32, 0x09, 0x09, 0x09, 0x3e, /* y */
		0x22, 0x26, 0x2a, 0x32, 0x22, /* z */
		0x00, 0x10, 0x6c, 0x82, 0x00, /* { */
		0x00, 0x00, 0xee, 0x00, 0x00, /* | */
		0x00, 0x82, 0x6c, 0x10, 0x00, /* } */
		0x40, 0x80, 0x40, 0x20, 0x40, /* ~ */
};

class CI2CDeviceSSD1306Base :
		virtual public IDisplayDeviceMonochrome,
		public IDeviceI2C,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
public:
//	virtual ~CI2CDeviceSSD1306();

public:
	void Init(II2C* pI2CInterface, unsigned uiAddress);

public: // IDisplayDeviceMonochrome
	virtual void Clear(bool tColor);
	virtual void Update();

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** pRetChunksToSend,
			const CChainedIoChunks**    pRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

protected:
	void MarkAllZonesForUpdate() {m_uiUpdatedZoneFlags = SSD1306_UPDATEALL;}
	void MarkZoneForUpdate(unsigned nZone) {ASSERTE(nZone < SSD1306_ZONES); AtomicOr(&m_uiUpdatedZoneFlags, (unsigned)(1 << nZone)); }
	void MarkZonesForUpdate(unsigned nFirstZone, unsigned nLastZone) {ASSERTE(nFirstZone <= nLastZone); for (unsigned n = nFirstZone; n <= nLastZone; ++n) MarkZoneForUpdate(n);}
	bool GetIsAnyZoneMarkedForUpdate() {return m_uiUpdatedZoneFlags;}
	bool GetIsZoneMarkedForUpdate(unsigned nZone) {ASSERTE(nZone < SSD1306_ZONES); return m_uiUpdatedZoneFlags & (1 << nZone);}
	void UnmarkZoneUpdated(unsigned nZone) {ASSERTE(nZone < SSD1306_ZONES); AtomicAnd(&m_uiUpdatedZoneFlags, (unsigned)~(1 << nZone));}
	unsigned GetScrollLine() const {return m_uiScrollLine;}
	void DoScrollUp();

protected:
	uint8_t  m_aFrameBuffer[SSD1306_SIZE];

private:
	II2C*     m_pI2CInterface;
	unsigned  m_uiAddress;
	enum {
		OpIsCheck,
		OpIsPreInit,
		OpIsInit,
		OpIsScrollV,
		OpIsWriteAddr,
		OpIsWriteData,
		OpIsNone,
	}         m_eCurrentOp;
	unsigned  m_nInitStep;

private:
	struct {
		uint8_t reg;
		struct {
			uint8_t line  :6;     // 40~7F - Set display RAM display start line register from 0-63 using X[5:0]
			uint8_t cmd40 :2;
		} data1;
	} m_SlaveScrollVCommand;
	struct {
		uint8_t reg1;
		struct {
			uint8_t page  :3;     // B0~B7 - Set GDDRAM Page Start Address (PAGE0~PAGE7) for Page Addressing Mode using X[2:0]
			uint8_t cmdB0 :5;
		} data1;
		uint8_t reg2;
		struct {
			uint8_t nibble_lo :4; // 00~0F - Set the lower nibble of the column start address register for Page Addressing Mode using X[3:0]
			uint8_t cmd00     :4;
		} data2;
		uint8_t reg3;
		struct {
			uint8_t nibble_hi :4; // 10~1F - Set the higher nibble of the column start address register for Page Addressing Mode using X[3:0]
			uint8_t cmd10     :4;
		} data3;
	} m_SlaveSetAddressCommand;
	struct {
		uint8_t reg_data;
	} m_SlaveDataHeader;
	CChainedConstChunks m_cccTxChunkCmdScroll;
	CChainedConstChunks m_cccTxChunkCmdAddr;
	CChainedConstChunks m_cccTxChunkHeader;
	CChainedConstChunks m_cccTxChunkData;
	unsigned m_uiUpdatedZoneFlags;
	unsigned m_uiScrollLine;
	bool m_bScrollChanged;

};

class CI2CDeviceSSD1306Text :
		virtual public CI2CDeviceSSD1306Base,
		virtual public ITextDisplayDeviceMonochrome
{
public:
//	virtual ~CI2CDeviceSSD1306Text();

public: // IDisplayDeviceMonochrome
	virtual void Clear(bool tColor);
public: // ITextDisplayDeviceMonochrome
	virtual const CCoordinates& GetTextSize() const;
	virtual const CCoordinates& GetTextXY() const;
	virtual void GotoXY(const CCoordinates& tPoint1);
	virtual void PutChar(char ch, bool tColor);
	virtual void PutString(const char* s, bool tColor);

private:
	void DoGotoXY(int x, int y);
	void DoPutChar(char ch, bool tColor);

private:
	int m_iCursorPos;
};

class CI2CDeviceSSD1306Draw :
		virtual public CI2CDeviceSSD1306Text,
		virtual public IDrawDisplayDeviceMonochrome
{
public:
//	virtual ~CI2CDeviceSSD1306Draw();

public: // IDrawDisplayDeviceMonochrome
	virtual const CCoordinates& GetSize() const;
	virtual void DrawPixel(const CCoordinates& tPosition, bool tColor);
	virtual void DrawLine(const CCoordinates& tPoint1, const CCoordinates& tPoint2, bool tColor);
	virtual void DrawRect(const CCoordinates& tBottomLeftPosition, const CCoordinates& tSize, bool tColor);

private:
	void DoDrawVLine(int x, int y1, int y2, bool tColor);
	void DoDrawHLine(int y, int x1, int x2, bool tColor);
	void DoDrawLine(int x1, int x2, int y1, int y2, bool tColor);
	void DoDrawR(int x1, int x2, int y1, int y2, bool tColor);
};

class CI2CDeviceSSD1306 :
		virtual public CI2CDeviceSSD1306Text,
		virtual public CI2CDeviceSSD1306Draw
{
};


void CI2CDeviceSSD1306Base::Init(II2C* pI2CInterface, unsigned uiAddress)
{
	CParentProcess::Init(&g_TickProcessScheduller);

	m_pI2CInterface = pI2CInterface;
	m_uiAddress = uiAddress;
	m_eCurrentOp = OpIsPreInit;
	m_nInitStep = 0;
	m_SlaveScrollVCommand.reg = CMD_CO_BIT(0) | CMD_DC_BIT(0);
	m_SlaveScrollVCommand.data1.cmd40 = 0x01;
	m_SlaveSetAddressCommand.reg1 = CMD_CO_BIT(0) | CMD_DC_BIT(0);
	m_SlaveSetAddressCommand.data1.cmdB0 = 0x16;
	m_SlaveSetAddressCommand.reg2 = CMD_CO_BIT(0) | CMD_DC_BIT(0);
	m_SlaveSetAddressCommand.data2.cmd00 = 0x00;
	m_SlaveSetAddressCommand.reg3 = CMD_CO_BIT(0) | CMD_DC_BIT(0);
	m_SlaveSetAddressCommand.data3.cmd10 = 0x01;
	m_SlaveDataHeader.reg_data = CMD_CO_BIT(0) | CMD_DC_BIT(1);
	Clear(false);
	m_cccTxChunkCmdScroll.Assign((uint8_t*)&m_SlaveScrollVCommand, sizeof(m_SlaveScrollVCommand));
	m_cccTxChunkCmdScroll.Break();
	m_cccTxChunkCmdAddr.Assign((uint8_t*)&m_SlaveSetAddressCommand, sizeof(m_SlaveSetAddressCommand));
	m_cccTxChunkCmdAddr.Break();
	m_cccTxChunkHeader.Assign((uint8_t*)&m_SlaveDataHeader, sizeof(m_SlaveDataHeader));
	m_cccTxChunkHeader.Append(&m_cccTxChunkData);
	m_cccTxChunkData.Assign((uint8_t*)&m_aFrameBuffer[0], ARRAY_SIZE(m_aFrameBuffer));
	m_cccTxChunkData.Break();

//	DrawLine({6,0},{126,0}, true);
//	DrawLine({127,1},{127,62}, true);
//	DrawLine({1,63},{126,63}, true);
//	DrawLine({0,10},{0,62}, true);

//	DrawRect({32,16}, {64, 32}, true);
//	DrawRect({48,24}, {32, 16}, false);
//
//	PutString("Hello, World!\n", true);
//	PutString("v0.1\n", false);
//
//	for (char ch = ' '; ch <= '~'; ++ch)
//		PutChar(ch, true);
//	PutChar('\n', true);
//	for (char ch = ' '; ch <= '~'; ++ch)
//		PutChar(ch, false);

//	for (int n = 0; n < 15; ++n) DoDrawVLine(20 + n * 6, 24 - n, 24 + n * 2, true);

//	CCoordinates c = {63,31};
//	DrawLine(c, {2,31}, true);
//	DrawLine(c, {125,31}, true);
//	for (int dy = 6; dy < 31; dy += 6) {
//		DrawLine(c, {2,31 + dy}, true);
//		DrawLine(c, {125,31 + dy}, true);
//		DrawLine(c, {2,31 - dy}, true);
//		DrawLine(c, {125,31 - dy}, true);
//	}
//	DrawLine(c, {63, 2}, true);
//	DrawLine(c, {63, 61}, true);
//	for (int dx = 6; dx < 63; dx += 6) {
//		DrawLine(c, {63 + dx, 1}, true);
//		DrawLine(c, {63 + dx, 61}, true);
//		DrawLine(c, {63 - dx, 1}, true);
//		DrawLine(c, {63 - dx, 61}, true);
//	}


	ContinueDelay(INITIAL_DELAY);

	SSD1306_DEBUG_INFO("First Init");
}

/*********************************************************
 *
 *    DRAW to framebuffer
 *
 ********************************************************/

/*virtual*/
const CCoordinates& CI2CDeviceSSD1306Draw::GetSize() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDrawDisplayDeviceMonochrome::GetSize());

	static const CCoordinates g_SSD1306Size = {SSD1306_WIDTH, SSD1306_HEIGHT};
	return g_SSD1306Size;
}

/*virtual*/
void CI2CDeviceSSD1306Draw::DrawPixel(const CCoordinates& tPosition, bool tColor)
{
	IMPLEMENTS_INTERFACE_METHOD(IDrawDisplayDeviceMonochrome::DrawPixel(tPosition, tColor));

	if (tPosition.x >= 0 && tPosition.x < SSD1306_WIDTH && tPosition.y >= 0 && tPosition.y < SSD1306_HEIGHT) {
		unsigned nPage = tPosition.y / SSD1306_PAGE_HEIGHT;
		unsigned nRow = tPosition.y % SSD1306_PAGE_HEIGHT;
		unsigned uiFrameIndex = tPosition.x + nPage * SSD1306_WIDTH;
		uint8_t uiMask = 1 << nRow;
		if (tColor) {
			m_aFrameBuffer[uiFrameIndex] |= uiMask;
		} else {
			m_aFrameBuffer[uiFrameIndex] &= ~uiMask;
		}
		unsigned nZone = uiFrameIndex / SSD1306_ZONE_SIZE;
		MarkZoneForUpdate(nZone);
	}
}

/*virtual*/
void CI2CDeviceSSD1306Draw::DrawLine(const CCoordinates& tPoint1, const CCoordinates& tPoint2, bool tColor)
{
	IMPLEMENTS_INTERFACE_METHOD(IDrawDisplayDeviceMonochrome::DrawLine(tPoint1, tPoint2, tColor));

	DoDrawLine(tPoint1.x, tPoint1.y, tPoint2.x, tPoint2.y, tColor);
	ContinueNow();
	SSD1306_DEBUG_INFO("line updated masks:", m_uiUpdatedZoneFlags);
}

/*virtual*/
void CI2CDeviceSSD1306Draw::DrawRect(const CCoordinates& tBottomLeftPosition, const CCoordinates& tSize, bool tColor)
{
	IMPLEMENTS_INTERFACE_METHOD(IDrawDisplayDeviceMonochrome::DrawRect(tBottomLeftPosition, tSize, tColor));

	DoDrawR(tBottomLeftPosition.x, tBottomLeftPosition.x + tSize.x - 1, tBottomLeftPosition.y, tBottomLeftPosition.y + tSize.y - 1, tColor);
	ContinueNow();
	SSD1306_DEBUG_INFO("rect updated masks:", m_uiUpdatedZoneFlags);
}

/*********************************************************
 *
 *    PRINT to framebuffer
 *
 ********************************************************/

/*virtual*/
const CCoordinates& CI2CDeviceSSD1306Text::GetTextSize() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITextDisplayDeviceMonochrome::GetTextSize());

	static const CCoordinates g_SSD1306Size = {SSD1306_WIDTH / SSD1306_CHAR_WIDTH, SSD1306_HEIGHT / SSD1306_CHAR_HEIGHT};
	return g_SSD1306Size;
}

/*virtual*/
const CCoordinates& CI2CDeviceSSD1306Text::GetTextXY() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITextDisplayDeviceMonochrome::GetTextXY());

	static CCoordinates result = {0, 0};
	result.x = (m_iCursorPos % SSD1306_WIDTH) / (SSD1306_CHAR_WIDTH + 1);
	result.y = (m_iCursorPos / SSD1306_WIDTH);
	return result;
}

/*virtual*/
void CI2CDeviceSSD1306Text::GotoXY(const CCoordinates& tPosition)
{
	IMPLEMENTS_INTERFACE_METHOD(ITextDisplayDeviceMonochrome::GotoXY(tPosition));
	DoGotoXY(tPosition.x, tPosition.y);
}

/*virtual*/
void CI2CDeviceSSD1306Text::PutChar(char ch, bool bColor)
{
	IMPLEMENTS_INTERFACE_METHOD(ITextDisplayDeviceMonochrome::PutChar(ch, bColor));

	DoPutChar(ch, bColor);
	ContinueNow();
	SSD1306_DEBUG_INFO("char updated masks:", m_uiUpdatedZoneFlags);
}

/*virtual*/
void CI2CDeviceSSD1306Text::PutString(const char* s, bool bColor)
{
	IMPLEMENTS_INTERFACE_METHOD(ITextDisplayDeviceMonochrome::PutString(s, bColor));

	const char* pch = s;
	while(*pch) {
		DoPutChar(*pch, bColor);
		++pch;
	}
	ContinueNow();
	SSD1306_DEBUG_INFO("string updated masks:", m_uiUpdatedZoneFlags);
}

/*virtual*/
void CI2CDeviceSSD1306Base::Update()
{
	IMPLEMENTS_INTERFACE_METHOD(IDisplayDeviceMonochrome::Update());

	ContinueNow();
}

/*virtual*/
void CI2CDeviceSSD1306Base::Clear(bool tColor)
{
	IMPLEMENTS_INTERFACE_METHOD(IDisplayDeviceMonochrome::Clear(tColor));

	memset(m_aFrameBuffer, tColor?0xFF:0x00, sizeof(m_aFrameBuffer));
	MarkAllZonesForUpdate();
	m_uiScrollLine = 0;
	m_bScrollChanged = true;
	ContinueNow();
}

/*virtual*/
void CI2CDeviceSSD1306Text::Clear(bool tColor)
{
	m_iCursorPos = (SSD1306_LINES - 1) * SSD1306_LINE_SIZE;
	CI2CDeviceSSD1306Base::Clear(tColor);
}

/*********************************************************
 *
 *    DRAW to framebuffer
 *
 ********************************************************/

void CI2CDeviceSSD1306Draw::DoDrawVLine(int x, int y1, int y2, bool tColor)
{
	if (x < 0 || x >= SSD1306_WIDTH) return;
	if (y1 > y2) SWAP(y1, y2);
	if (y1 >= SSD1306_HEIGHT || y2 < 0) return;
	y1 = MAX2(y1, 0);
	y2 = MIN2(y2, SSD1306_HEIGHT - 1);

	unsigned nPage = y1 / SSD1306_PAGE_HEIGHT;
	unsigned nPage2 = y2 / SSD1306_PAGE_HEIGHT;
	unsigned nRow1 = y1 % SSD1306_PAGE_HEIGHT;
	unsigned nRow2 = y2 % SSD1306_PAGE_HEIGHT;
	uint8_t uiMask1 = (0xFF << nRow1);
	uint8_t uiMask2 = (0xFF >> (7 - nRow2));
	unsigned uiFrameIndex = x + nPage * SSD1306_WIDTH;

	if (nPage == nPage2) {
		uint8_t uiMask = uiMask1 & uiMask2;
		ASSERTE(uiFrameIndex < ARRAY_SIZE(m_aFrameBuffer));
		if (tColor) {
			m_aFrameBuffer[uiFrameIndex] |= uiMask;
		} else {
			m_aFrameBuffer[uiFrameIndex] &= ~uiMask;
		}
		unsigned nZone = uiFrameIndex / SSD1306_ZONE_SIZE;
		MarkZoneForUpdate(nZone);
	} else {
		ASSERTE(uiFrameIndex < ARRAY_SIZE(m_aFrameBuffer));
		if (tColor) {
			m_aFrameBuffer[uiFrameIndex] |= uiMask1;
		} else {
			m_aFrameBuffer[uiFrameIndex] &= ~uiMask1;
		}
		MarkZoneForUpdate(uiFrameIndex / SSD1306_ZONE_SIZE);
		uiFrameIndex += SSD1306_WIDTH;
		++nPage;

		uint8_t uiFill = tColor ? 0xFF : 0x00;
		while (nPage < nPage2) {
			ASSERTE(uiFrameIndex < ARRAY_SIZE(m_aFrameBuffer));
			m_aFrameBuffer[uiFrameIndex] = uiFill;
			MarkZoneForUpdate(uiFrameIndex / SSD1306_ZONE_SIZE);
			uiFrameIndex += SSD1306_WIDTH;
			++nPage;
		}

		ASSERTE(uiFrameIndex < ARRAY_SIZE(m_aFrameBuffer));
		if (tColor) {
			m_aFrameBuffer[uiFrameIndex] |= uiMask2;
		} else {
			m_aFrameBuffer[uiFrameIndex] &= ~uiMask2;
		}
		MarkZoneForUpdate(uiFrameIndex / SSD1306_ZONE_SIZE);
	}
}

void CI2CDeviceSSD1306Draw::DoDrawHLine(int y, int x1, int x2, bool tColor)
{
	if (y < 0 || y >= SSD1306_HEIGHT) return;
	if (x1 > x2) SWAP(x1, x2);
	if (x1 >= SSD1306_WIDTH || x2 < 0) return;
	x1 = MAX2(x1, 0);
	x2 = MIN2(x2, SSD1306_WIDTH - 1);

	unsigned nPage = y / SSD1306_PAGE_HEIGHT;
	unsigned nRow = y % SSD1306_PAGE_HEIGHT;
	unsigned uiFrameIndex = x1 + nPage * SSD1306_WIDTH;
	unsigned uiFrameIndexEnd = uiFrameIndex + x2 - x1;
	uint8_t uiMask = (0x01 << nRow);

	if (tColor) {
		while (uiFrameIndex <= uiFrameIndexEnd) {
			m_aFrameBuffer[uiFrameIndex] |= uiMask;
			++uiFrameIndex;
		}
	} else {
		while (uiFrameIndex <= uiFrameIndexEnd) {
			m_aFrameBuffer[uiFrameIndex] &= ~uiMask;
			++uiFrameIndex;
		}
	}

	unsigned nFirstZone = uiFrameIndex / SSD1306_ZONE_SIZE;
	unsigned nLastZone = uiFrameIndexEnd / SSD1306_ZONE_SIZE;
	MarkZonesForUpdate(nFirstZone, nLastZone);
}

#define I2Q(i) ((i) * 1024)
#define Q2I(q) ((q) / 1024)
void CI2CDeviceSSD1306Draw::DoDrawLine(int x1, int y1, int x2, int y2, bool tColor)
{
	int dx = x2 - x1;
	if (dx == 0) {
		DoDrawVLine(x1, y1, y2, tColor);
	} else {
		int dy = y2 - y1;
		if (dy == 0) {
			DoDrawHLine(y1, x1, x2, tColor);
		} else {
			int nX = ABS(dx);
			int nY = ABS(dy);
			if (nX <= nY) {
				// mostly vertical
				int qY = I2Q(y1);
				int qSubLength = I2Q(dy) / (nX + 1);
				dx = SIGN(dx);
				int qRound = (dy > 0) ? (I2Q(1) / 2) : -(I2Q(1) / 2);
				for (int x = x1; x != x2; x += dx) {
					int qY2 = qY + qSubLength;
					DoDrawVLine(x, Q2I(qY + qRound), Q2I(qY2 - qRound), tColor);
					qY = qY2;
				}
				DoDrawVLine(x2, Q2I(qY + qRound), y2, tColor);
			} else {
				// mostly horizontal
				int qX = I2Q(x1);
				int qSubLength = I2Q(dx) / (nY + 1);
				dy = SIGN(dy);
				int qRound = (dx > 0) ? (I2Q(1) / 2) : -(I2Q(1) / 2);
				for (int y = y1; y != y2; y += dy) {
					int qX2 = qX + qSubLength;
					DoDrawHLine(y, Q2I(qX + qRound), Q2I(qX2 - qRound), tColor);
					qX = qX2;
				}
				DoDrawHLine(y2, Q2I(qX + qRound), x2, tColor);
			}
		}
	}
}

void CI2CDeviceSSD1306Draw::DoDrawR(int x1, int x2, int y1, int y2, bool tColor)
{
	if (x1 > x2) SWAP(x1, x2);
	if (x1 >= SSD1306_WIDTH || x2 < 0) return;
	x1 = MAX2(x1, 0);
	x2 = MIN2(x2, SSD1306_WIDTH - 1);
	if (y1 > y2) SWAP(y1, y2);
	if (y1 >= SSD1306_HEIGHT || y2 < 0) return;
	y1 = MAX2(y1, 0);
	y2 = MIN2(y2, SSD1306_HEIGHT - 1);

	unsigned nPage = y1 / SSD1306_PAGE_HEIGHT;
	unsigned nPage2 = y2 / SSD1306_PAGE_HEIGHT;
	unsigned nRow1 = y1 % SSD1306_PAGE_HEIGHT;
	unsigned nRow2 = y2 % SSD1306_PAGE_HEIGHT;
	uint8_t uiMask1 = (0xFF << nRow1);
	uint8_t uiMask2 = (0xFF >> (7 - nRow2));
	ASSERTE(uiMask1);
	ASSERTE(uiMask2);
	unsigned uiFrameIndex1 = x1 + nPage * SSD1306_WIDTH;
	unsigned uiFrameIndex2 = x2 + nPage * SSD1306_WIDTH;
	ASSERTE(uiFrameIndex1 < ARRAY_SIZE(m_aFrameBuffer));
	ASSERTE(uiFrameIndex2 < ARRAY_SIZE(m_aFrameBuffer));

	if (nPage == nPage2) {
		uint8_t uiMask = uiMask1 & uiMask2;
		ASSERTE(uiMask);
		if (tColor) {
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] |= uiMask;
			}
		} else {
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] &= ~uiMask;
			}
		}
		unsigned nFirstZone = uiFrameIndex1 / SSD1306_ZONE_SIZE;
		unsigned nLastZone = uiFrameIndex2 / SSD1306_ZONE_SIZE;
		MarkZonesForUpdate(nFirstZone, nLastZone);
	} else {
		if (tColor) {
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] |= uiMask1;
			}
		} else {
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] &= ~uiMask1;
			}
		}
		MarkZonesForUpdate(
				uiFrameIndex1 / SSD1306_ZONE_SIZE,
				uiFrameIndex2 / SSD1306_ZONE_SIZE
		);
		uiFrameIndex1 += SSD1306_WIDTH;
		uiFrameIndex2 += SSD1306_WIDTH;
		++nPage;

		uint8_t uiFill = tColor ? 0xFF : 0x00;
		while (nPage < nPage2) {
			ASSERTE(uiFrameIndex1 < ARRAY_SIZE(m_aFrameBuffer));
			ASSERTE(uiFrameIndex2 < ARRAY_SIZE(m_aFrameBuffer));
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] = uiFill;
			}
			MarkZonesForUpdate(
					uiFrameIndex1 / SSD1306_ZONE_SIZE,
					uiFrameIndex2 / SSD1306_ZONE_SIZE
			);
			uiFrameIndex1 += SSD1306_WIDTH;
			uiFrameIndex2 += SSD1306_WIDTH;
			++nPage;
		}

		ASSERTE(uiFrameIndex1 < ARRAY_SIZE(m_aFrameBuffer));
		ASSERTE(uiFrameIndex2 < ARRAY_SIZE(m_aFrameBuffer));
		if (tColor) {
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] |= uiMask2;
			}
		} else {
			for (unsigned i = uiFrameIndex1; i <= uiFrameIndex2; ++i) {
				m_aFrameBuffer[i] &= ~uiMask2;
			}
		}
		MarkZonesForUpdate(
				uiFrameIndex1 / SSD1306_ZONE_SIZE,
				uiFrameIndex2 / SSD1306_ZONE_SIZE
		);
	}
}

/*********************************************************
 *
 *    PRINT to framebuffer
 *
 ********************************************************/

void CI2CDeviceSSD1306Text::DoGotoXY(int x, int y)
{
	m_iCursorPos = (LIMIT(x, 0, SSD1306_COLUMNS - 1) * (SSD1306_CHAR_WIDTH + 1)) + (LIMIT(y, 0, SSD1306_LINES - 1) * SSD1306_WIDTH);
}

void CI2CDeviceSSD1306Text::DoPutChar(char ch, bool bColor)
{

	unsigned iCursorLine = m_iCursorPos / SSD1306_WIDTH;
	unsigned iCursorColumn = m_iCursorPos % SSD1306_WIDTH;
	unsigned uiScrollLine = GetScrollLine();
	unsigned uiFrameIndex = (m_iCursorPos + uiScrollLine * SSD1306_LINE_SIZE) % ARRAY_SIZE(m_aFrameBuffer);
	uint8_t* pFrame = &m_aFrameBuffer[uiFrameIndex];
	bool bOnTheEdge = (iCursorColumn >= SSD1306_WIDTH - SSD1306_CHAR_WIDTH - 1);
	bool bOverEdge = (iCursorColumn >= SSD1306_WIDTH - SSD1306_CHAR_WIDTH);
	if (bOverEdge || ch == '\n') {
		while (iCursorColumn < SSD1306_WIDTH) { // clear line tail
			*pFrame = bColor ? 0x00 : 0xFF;
			++pFrame;
			++iCursorColumn;
		}
		unsigned uiFirstZone = uiFrameIndex / SSD1306_ZONE_SIZE;
		unsigned uiLastZone = (pFrame - &m_aFrameBuffer[0] - 1) / SSD1306_ZONE_SIZE;
		MarkZonesForUpdate(uiFirstZone, uiLastZone);
		iCursorColumn = 0;
		if (iCursorLine) {
			--iCursorLine;
		} else {
			DoScrollUp();
		}
		m_iCursorPos = iCursorLine * SSD1306_WIDTH;
		if (ch == '\n') return;
		uiFrameIndex = (m_iCursorPos + uiScrollLine * SSD1306_LINE_SIZE) % ARRAY_SIZE(m_aFrameBuffer);
		pFrame = &m_aFrameBuffer[uiFrameIndex];
		bOnTheEdge = false;
	}

	if (ch >= SSD1306_CHAR_FIRST && (unsigned)(ch - SSD1306_CHAR_FIRST) < ARRAY_SIZE(g_auiSSD1306CharMap) / SSD1306_CHAR_WIDTH ) {
		const uint8_t* pCharMap = &g_auiSSD1306CharMap[(ch - SSD1306_CHAR_FIRST) * SSD1306_CHAR_WIDTH];
		const uint8_t* pCharMapEnd = pCharMap + SSD1306_CHAR_WIDTH;
		while (pCharMap < pCharMapEnd) {
			uint8_t b = *pCharMap;
//			if (b) {
				*pFrame = bColor ? b : ~b;
				++pFrame;
//			}
			++pCharMap;
		}
		if (!bOnTheEdge) {
			*pFrame = bColor ? 0x00 : 0xFF;
			++pFrame;
		}
	} else {
		for (unsigned n = 0; n < SSD1306_SPACE_WIDTH; ++n) {
			*pFrame = bColor ? 0x00 : 0xFF;
			++pFrame;
		}
	}
	unsigned uiFirstZone = uiFrameIndex / SSD1306_ZONE_SIZE;
	unsigned uiLastZone = (pFrame - &m_aFrameBuffer[0] - 1) / SSD1306_ZONE_SIZE;
	MarkZonesForUpdate(uiFirstZone, uiLastZone);

	m_iCursorPos = pFrame - &m_aFrameBuffer[0] - uiScrollLine * SSD1306_LINE_SIZE;
}

void CI2CDeviceSSD1306Base::DoScrollUp()
{
	m_uiScrollLine = (m_uiScrollLine - 1 ) % SSD1306_LINES;
	m_bScrollChanged = true;
	unsigned uiFrameIndex = (m_uiScrollLine * SSD1306_LINE_SIZE) % ARRAY_SIZE(m_aFrameBuffer);
	uint8_t* pFrame = &m_aFrameBuffer[uiFrameIndex];
	for (unsigned n = 0; n < SSD1306_LINE_SIZE; ++n) {
		*pFrame = 0x00;
		++pFrame;
	}
	unsigned uiFirstZone = uiFrameIndex / SSD1306_ZONE_SIZE;
	unsigned uiLastZone = (pFrame - &m_aFrameBuffer[0] - 1) / SSD1306_ZONE_SIZE;
	MarkZonesForUpdate(uiFirstZone, uiLastZone);
}

/*virtual*/
void CI2CDeviceSSD1306Base::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	PROCESS_DEBUG_INFO(">>> Process: CI2CDeviceSSD1306Text");

	if (m_eCurrentOp == OpIsNone && m_bScrollChanged) {
		m_eCurrentOp = OpIsScrollV;
		m_pI2CInterface->Queue_I2C_Session(this);
		ContinueDelay(IDLE_DELAY);
	} else if (m_eCurrentOp == OpIsNone && GetIsAnyZoneMarkedForUpdate()) {
		m_eCurrentOp = OpIsWriteAddr;
		m_pI2CInterface->Queue_I2C_Session(this);
		ContinueDelay(IDLE_DELAY);
	} else if (m_eCurrentOp == OpIsPreInit) {
		SSD1306_DEBUG_INFO("Init");
		ASSERTE(m_nInitStep == 0);
		m_eCurrentOp = OpIsInit;
		m_pI2CInterface->Queue_I2C_Session(this);
		ContinueDelay(IDLE_DELAY);
	} else if (m_eCurrentOp == OpIsNone && m_nInitStep == 0) {
		m_eCurrentOp = OpIsPreInit;
		ContinueDelay(INITIAL_DELAY);
	} else if (m_eCurrentOp == OpIsNone) {
		m_eCurrentOp = OpIsCheck;
		m_pI2CInterface->Queue_I2C_Session(this);
		ContinueDelay(IDLE_DELAY);
	}

	PROCESS_DEBUG_INFO("<<< Process: CI2CDeviceSSD1306Text");
}

/*********************************************************
 *
 *    I2C communication
 *
 ********************************************************/


unsigned CI2CDeviceSSD1306Base::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_uiAddress;
}

bool CI2CDeviceSSD1306Base::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));
	switch (m_eCurrentOp) {
	case OpIsNone:
	case OpIsPreInit:
		ASSERT("invalid op");
		return false;
		break;
	case OpIsCheck:
		*ppRetChunksToSend = NULL;
		break;
	case OpIsInit:
		m_cccTxChunkData.Assign(&g_auiSSD1306InitCommands[m_nInitStep], 2);
		*ppRetChunksToSend = &m_cccTxChunkData;
		break;
	case OpIsScrollV:
		m_bScrollChanged = false;
		m_SlaveScrollVCommand.data1.line = m_uiScrollLine * SSD1306_LINE_HEIGHT;
		*ppRetChunksToSend = &m_cccTxChunkCmdScroll;
		break;
	case OpIsWriteAddr: {
		SSD1306_DEBUG_INFO("updated zones flags:", m_uiUpdatedZoneFlags);
		ASSERTE(m_uiUpdatedZoneFlags);
		unsigned nFirstZoneToWrite = 0;
		unsigned nZonesToWrite = 0;
		for (unsigned nZone = 0; nZone < SSD1306_ZONES; ++nZone) {
			bool bZoneMarkedForUpdate = GetIsZoneMarkedForUpdate(nZone);
			if (!nZonesToWrite) {
				if (bZoneMarkedForUpdate) {
					nFirstZoneToWrite = nZone;
					nZonesToWrite = 1;
					UnmarkZoneUpdated(nZone);
					if (!GetIsAnyZoneMarkedForUpdate()) break;
				}
			} else if (bZoneMarkedForUpdate) {
				++nZonesToWrite;
				UnmarkZoneUpdated(nZone);
				if (!GetIsAnyZoneMarkedForUpdate()) break;
			} else {
				break;
			}
		}
		ASSERTE(nZonesToWrite);
		SSD1306_DEBUG_INFO("updating zones count:", nZonesToWrite);
		SSD1306_DEBUG_INFO("updating zones first:", nFirstZoneToWrite);
		m_SlaveSetAddressCommand.data1.page = nFirstZoneToWrite;
		m_SlaveSetAddressCommand.data2.nibble_lo = 0;
		m_SlaveSetAddressCommand.data3.nibble_hi = 0;
		m_cccTxChunkData.Assign((uint8_t*)&m_aFrameBuffer + nFirstZoneToWrite * SSD1306_ZONE_SIZE, nZonesToWrite * SSD1306_ZONE_SIZE);
		*ppRetChunksToSend = &m_cccTxChunkCmdAddr;
		break; }
	case OpIsWriteData: {
		*ppRetChunksToSend = &m_cccTxChunkHeader;
		break; }
	}
	*ppRetChunksToRecv = NULL;
	return true;
}

void CI2CDeviceSSD1306Base::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());
	SSD1306_DEBUG_INFO("I2C done. State:", m_eCurrentOp);
	switch (m_eCurrentOp) {
	case OpIsNone:
	case OpIsPreInit:
		ASSERT("invalid op");
		break;
	case OpIsCheck:
		m_eCurrentOp = OpIsNone;
		break;
	case OpIsInit:
		m_nInitStep += 2;
		if (m_nInitStep < ARRAY_SIZE(g_auiSSD1306InitCommands)) {
			m_pI2CInterface->Queue_I2C_Session(this);
		} else {
			m_eCurrentOp = OpIsNone;
			MarkAllZonesForUpdate();
			m_bScrollChanged = true;
			ContinueDelay(INITIAL_DELAY);
			SSD1306_DEBUG_INFO("INIT done");
		}
		break;
	case OpIsScrollV:
		m_eCurrentOp = OpIsNone;
		if (GetIsAnyZoneMarkedForUpdate()) {
			ContinueNow();
		}
		break;
	case OpIsWriteAddr:
		m_eCurrentOp = OpIsWriteData;
		m_pI2CInterface->Queue_I2C_Session(this);
		break;
	case OpIsWriteData:
		if (GetIsAnyZoneMarkedForUpdate()) {
			m_eCurrentOp = OpIsWriteAddr;
			m_pI2CInterface->Queue_I2C_Session(this);
		} else if (m_nInitStep == 0) {
			m_eCurrentOp = OpIsPreInit;
			ContinueDelay(INITIAL_DELAY);
		} else {
			m_eCurrentOp = OpIsNone;
			ContinueDelay(IDLE_DELAY);
		}
		break;
	}
}

void CI2CDeviceSSD1306Base::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	DEBUG_INFO("SSD1306 error:", e);
	SSD1306_DEBUG_INFO("I2C error. State:", m_eCurrentOp);

	switch (m_eCurrentOp) {
	case OpIsPreInit:
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsCheck:
	case OpIsInit:
	case OpIsScrollV:
	case OpIsWriteAddr:
	case OpIsWriteData:
		break;
	}
	MarkAllZonesForUpdate();
	m_bScrollChanged = true;
	m_nInitStep = 0;
	m_eCurrentOp = OpIsPreInit;

	ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
}

void CI2CDeviceSSD1306Base::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
	ContinueNow();
}


ITextDisplayDeviceMonochrome* AcquireSSD1306TextDisplayInterface(II2C* pI2CInterface, bool bAltAddress) {
	static CI2CDeviceSSD1306Text   g_SSD1306TextDisplay;
	g_SSD1306TextDisplay.Init(pI2CInterface, bAltAddress?I2C_ADDR_ALT:I2C_ADDR); return &g_SSD1306TextDisplay;
}
IDrawDisplayDeviceMonochrome* AcquireSSD1306DrawDisplayInterface(II2C* pI2CInterface, bool bAltAddress) {
	static CI2CDeviceSSD1306Draw   g_SSD1306DrawDisplay;
	g_SSD1306DrawDisplay.Init(pI2CInterface, bAltAddress?I2C_ADDR_ALT:I2C_ADDR); return &g_SSD1306DrawDisplay;
}
