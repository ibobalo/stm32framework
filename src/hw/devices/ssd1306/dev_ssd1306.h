#ifndef DEV_SSD1306_H_INCLUDED
#define DEV_SSD1306_H_INCLUDED

#include "interfaces/display.h"

//extern
class II2C;

ITextDisplayDeviceMonochrome* AcquireSSD1306TextDisplayInterface(II2C* pI2CInterface, bool bAltAddress = false);
IDrawDisplayDeviceMonochrome* AcquireSSD1306DrawDisplayInterface(II2C* pI2CInterface, bool bAltAddress = false);

#endif //DEV_SSD1306_H_INCLUDED
