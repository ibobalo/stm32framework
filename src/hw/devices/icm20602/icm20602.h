#ifndef ICM20602_H_INCLUDED
#define ICM20602_H_INCLUDED

#define ICM20602_REG_SMPLRT_DIV                    0x19    /* sample rate.  Fsample= 1Khz/(SMPLRT_DIV+1) */
#  define ICM20602_SMPLRT_IGNORE                     0x00
#  define ICM20602_SMPLRT_1000HZ                     0x00  /* only effective when ICM20602_GYRO_CONFIG_FCHOICE_B_NONE, and (0 < DLPF_CFG < 7) */
#  define ICM20602_SMPLRT_500HZ                      0x01
#  define ICM20602_SMPLRT_250HZ                      0x03
#  define ICM20602_SMPLRT_200HZ                      0x04
#  define ICM20602_SMPLRT_100HZ                      0x09
#  define ICM20602_SMPLRT_50HZ                       0x13
#define ICM20602_REG_CONFIG                        0x1A
#  define ICM20602_CONFIG_DLPF_CFG_SHIFT             0
#  define ICM20602_CONFIG_DLPF_CFG_MASK              0x07
#  define ICM20602_CONFIG_DLPF_CFG_IGNORE            0x00 // not used when ICM20602_GYRO_CONFIG_FCHOICE_B
#  define ICM20602_CONFIG_DLPF_CFG_250Hz             0x00
#  define ICM20602_CONFIG_DLPF_CFG_176Hz             0x01
#  define ICM20602_CONFIG_DLPF_CFG_92Hz              0x02
#  define ICM20602_CONFIG_DLPF_CFG_41Hz              0x03
#  define ICM20602_CONFIG_DLPF_CFG_20Hz              0x04
#  define ICM20602_CONFIG_DLPF_CFG_10Hz              0x05
#  define ICM20602_CONFIG_DLPF_CFG_5Hz               0x06
#  define ICM20602_CONFIG_DLPF_CFG_3kHz              0x07
#  define ICM20602_CONFIG_EXT_SYNC_NONE              0x00
#  define ICM20602_CONFIG_EXT_SYNC_TEMP              0x08
#  define ICM20602_CONFIG_EXT_SYNC_GX                0x10
#  define ICM20602_CONFIG_EXT_SYNC_GY                0x18
#  define ICM20602_CONFIG_EXT_SYNC_GZ                0x20
#  define ICM20602_CONFIG_EXT_SYNC_AX                0x28
#  define ICM20602_CONFIG_EXT_SYNC_AY                0x30
#  define ICM20602_CONFIG_EXT_SYNC_AZ                0x38
#  define ICM20602_CONFIG_FIFO_MODE_REPLACE          0x00 // when the FIFO is full, additional writes will be written to the FIFO, replacing the oldest data
#  define ICM20602_CONFIG_FIFO_MODE_STOP             0x40 // when the FIFO is full, additional writes will not be written to FIFO
#  define ICM20602_CONFIG_BIT7                       0x80
#define ICM20602_REG_GYRO_CONFIG                   0x1B
#  define ICM20602_GYRO_CONFIG_FCHOICE_B_NONE        0x00 // Bypass DLPF
#  define ICM20602_GYRO_CONFIG_FCHOICE_B_32kHz_8     0x01
#  define ICM20602_GYRO_CONFIG_FCHOICE_B_32kHz_3     0x02
#  define ICM20602_GYRO_CONFIG_FS_SEL_SHIFT          3    // Gyro Full Scale Select
#  define ICM20602_GYRO_CONFIG_FS_SEL_MASK           0x18
#  define ICM20602_GYRO_CONFIG_FS_SEL_250dps         0x00
#  define ICM20602_GYRO_CONFIG_FS_SEL_500dps         0x08
#  define ICM20602_GYRO_CONFIG_FS_SEL_1000dps        0x10
#  define ICM20602_GYRO_CONFIG_FS_SEL_2500dps        0x18
#  define ICM20602_GYRO_CONFIG_ZG_ST                 0x20 // Z Gyro self-test
#  define ICM20602_GYRO_CONFIG_YG_ST                 0x40 // Y Gyro self-test
#  define ICM20602_GYRO_CONFIG_XG_ST                 0x80 // X Gyro self-test
#define ICM20602_REG_ACCEL_CONFIG                  0x1C
#  define ICM20602_ACCEL_CONFIG_FS_SEL_SHIFT         3    // Accel Full Scale Select
#  define ICM20602_ACCEL_CONFIG_FS_SEL_MASK          0x18
#  define ICM20602_ACCEL_CONFIG_FS_SEL_2g            0x00
#  define ICM20602_ACCEL_CONFIG_FS_SEL_4g            0x08
#  define ICM20602_ACCEL_CONFIG_FS_SEL_8g            0x10
#  define ICM20602_ACCEL_CONFIG_FS_SEL_16g           0x18
#  define ICM20602_ACCEL_CONFIG_ZG_ST                0x20 // Z Accel self-test
#  define ICM20602_ACCEL_CONFIG_YG_ST                0x40 // Y Accel self-test
#  define ICM20602_ACCEL_CONFIG_XG_ST                0x80 // X Accel self-test
#define ICM20602_REG_ACCEL_CONFIG2                 0x1D
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_SHIFT      0    // Accelerometer low pass filter setting
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_MASK       0x03
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_218Hz      0x00
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_99Hz       0x02
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_44Hz       0x03
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_21Hz       0x04
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_10Hz       0x05
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_5Hz        0x06
#  define ICM20602_ACCEL_CONFIG2_DLPF_CFG_420Hz      0x07
#  define ICM20602_ACCEL_CONFIG2_ACCEL_FCHOICE_B     0x04 // Bypass DLPF (3dB @ 1kHz)
#  define ICM20602_ACCEL_CONFIG2_DEC2_CFG_SHIFT      4    // Averaging filter settings for Low Power Accelerometer mode
#  define ICM20602_ACCEL_CONFIG2_DEC2_CFG_MASK       0x30
#  define ICM20602_ACCEL_CONFIG2_DEC2_CFG_4          0x00
#  define ICM20602_ACCEL_CONFIG2_DEC2_CFG_8          0x10
#  define ICM20602_ACCEL_CONFIG2_DEC2_CFG_16         0x20
#  define ICM20602_ACCEL_CONFIG2_DEC2_CFG_32         0x30
#define ICM20602_REG_FIFO_EN                       0x23
#  define ICM20602_BITS_GYRO_FIFO_EN                 0x10
#  define ICM20602_BITS_ACCEL_FIFO_EN                0x08
#define ICM20602_REG_FSYNC_INT                     0x36
#  define ICM20602_FSYNC_INT                         0x80 // This bit automatically sets to 1 when a FSYNC interrupt has been generated. The bit clears to 0 after the register has been read.
#define ICM20602_REG_INT_PIN_CFG                   0x37
#  define ICM20602_INT_PIN_FSYNC_INT_MODE_EN         0x04 // FSYNC pin will trigger an interrupt when it transitions to the level specified by FSYNC_INT_LEVEL
#  define ICM20602_INT_PIN_FSYNC_INT_LEVEL_LOW       0x08 // The logic level for the FSYNC pin as an interrupt is active low.
#  define ICM20602_INT_PIN_FSYNC_INT_LEVEL_HIGH      0x00 // The logic level for the FSYNC pin as an interrupt is active high.
#  define ICM20602_INT_PIN_INT_RD_CLEAR              0x10 // Interrupt status is cleared if any read operation is performed
#  define ICM20602_INT_PIN_INT_MAN_CLEAR             0x00 // Interrupt status is cleared only by reading INT_STATUS register
#  define ICM20602_INT_PIN_LATCH_INT_EN              0x20 // INT/DRDY pin level held until interrupt status is cleared
#  define ICM20602_INT_PIN_LATCH_INT_50uS            0x00 // INT/DRDY pin indicates interrupt pulse�s width is 50us
#  define ICM20602_INT_PIN_INT_OPEN                  0x40 // INT/DRDY pin is configured as open drain
#  define ICM20602_INT_PIN_INT_PP                    0x00 // INT/DRDY pin is configured as push-pull
#  define ICM20602_INT_PIN_INT_LEVEL_LOW             0x80 // The logic level for INT/DRDY pin is active low
#  define ICM20602_INT_PIN_INT_LEVEL_HIGH            0x00 // The logic level for INT/DRDY pin is active high
#define ICM20602_REG_INT_ENABLE                    0x38
#  define ICM20602_INT_ENABLE_DATA_RDY_INT_EN        0x01 // Data Ready interrupt enable
#  define ICM20602_INT_ENABLE_GDRIVE_INT_EN          0x04 // Gyroscope Drive System Ready interrupt enable
#  define ICM20602_INT_ENABLE_FSYNC_INT_EN           0x08 // FSYNC pin interrupt enable
#  define ICM20602_INT_ENABLE_FIFO_OFLOW_INT         0x10 // FIFO buffer overflow interrupt enable
#  define ICM20602_INT_ENABLE_WOM_Z_INT              0x20 // Z-axis accelerometer WoM interrupt enable
#  define ICM20602_INT_ENABLE_WOM_Y_INT              0x40 // Y-axis accelerometer WoM interrupt enable
#  define ICM20602_INT_ENABLE_WOM_X_INT              0x80 // X-axis accelerometer WoM interrupt enable
#define ICM20602_REG_FIFO_WM_INT_STATUS            0x39
#  define ICM20602_FIFO_WM_INT                       0x40 // FIFO Watermark interrupt status. Cleared on Read.
#define ICM20602_REG_INT_STATUS                    0x3A
#  define ICM20602_INT_STATUS_DATA_RDY_INT           0x01 // 1 when a Data Ready interrupt is generated. Cleared on Read.
#  define ICM20602_INT_STATUS_GDRIVE_INT             0x04 // Gyroscope Drive System Ready interrupt
#  define ICM20602_INT_STATUS_FIFO_OFLOW_INT         0x10 // 1 when a FIFO buffer overflow has been generated. Cleared on Read.
#  define ICM20602_INT_STATUS_WOM_Z_INT              0x20 // Z-axis accelerometer WoM interrupt status. Cleared on Read.
#  define ICM20602_INT_STATUS_WOM_Y_INT              0x40 // Y-axis accelerometer WoM interrupt status. Cleared on Read.
#  define ICM20602_INT_STATUS_WOM_X_INT              0x80 // X-axis accelerometer WoM interrupt status. Cleared on Read.
#define ICM20602_REG_ACCEL_XOUT_H                  0x3B
#define ICM20602_REG_ACCEL_XOUT_L                  0x3C
#define ICM20602_REG_ACCEL_YOUT_H                  0x3D
#define ICM20602_REG_ACCEL_YOUT_L                  0x3E
#define ICM20602_REG_ACCEL_ZOUT_H                  0x3F
#define ICM20602_REG_ACCEL_ZOUT_L                  0x40
#define ICM20602_REG_TEMP_OUT_H                    0x41
#define ICM20602_REG_TEMP_OUT_L                    0x42
#define ICM20602_REG_GYRO_XOUT_H                   0x43
#define ICM20602_REG_GYRO_XOUT_L                   0x44
#define ICM20602_REG_GYRO_YOUT_H                   0x45
#define ICM20602_REG_GYRO_YOUT_L                   0x46
#define ICM20602_REG_GYRO_ZOUT_H                   0x47
#define ICM20602_REG_GYRO_ZOUT_L                   0x48
#define ICM20602_REG_FIFO_WM_TH1                   0x60
#define ICM20602_REG_FIFO_WM_TH2                   0x61
#define ICM20602_REG_USER_CTRL                     0x6A
#  define ICM20602_USER_SIG_COND_RST                 0x01 // Reset all gyro/accel/temp digital signal path and clear sensor registers
#  define ICM20602_USER_CTRL_FIFO_RST                0x04 // Reset FIFO module
#  define ICM20602_USER_CTRL_FIFO_EN                 0x40 // Enable FIFO operation mode
#define ICM20602_REG_PWR_MGMT_1                    0x6B
#  define ICM20602_PWR_MGMT_1_CLK_INTERNAL           0x00 // Internal 20Mhz oscillator
#  define ICM20602_PWR_MGMT_1_CLK_AUTO               0x01 // Auto selects the best available clock source � PLL if ready, else use the Internal oscillator
#  define ICM20602_PWR_MGMT_1_CLK_STOP               0x07 // Stops the clock and keeps the timing generator in reset
#  define ICM20602_PWR_MGMT_1_TEMP_DIS               0x08 // Disable temperature sensor
#  define ICM20602_PWR_MGMT_1_GYRO_STANDBY           0x10 // Gyro drive and pll circuitry are enabled, but the sense paths are disabled
#  define ICM20602_PWR_MGMT_1_CYCLE                  0x20 // put sensor into cycle mode.  cycles between sleep mode and waking up to take a single sample of data from active sensors at a rate determined by LP_WAKE_CTRL
#  define ICM20602_PWR_MGMT_1_SLEEP                  0x40 // put sensor into low power sleep mode
#  define ICM20602_PWR_MGMT_1_DEVICE_RESET           0x80 // reset entire device
#define ICM20602_REG_PWR_MGMT_2                    0x6C   // allows the user to configure the frequency of wake-ups in Accelerometer Only Low Power Mode
#  define ICM20602_PWR_MGMT_2_ON_ALL                 0x00 // all is on
#  define ICM20602_PWR_MGMT_2_STBY_ZG                0x01 // Z gyro is disabled
#  define ICM20602_PWR_MGMT_2_STBY_YG                0x02 // Y gyro is disabled
#  define ICM20602_PWR_MGMT_2_STBY_XG                0x03 // X gyro is disabled
#  define ICM20602_PWR_MGMT_2_STBY_ZA                0x08 // Z accelerometer is disabled
#  define ICM20602_PWR_MGMT_2_STBY_YA                0x10 // Y accelerometer is disabled
#  define ICM20602_PWR_MGMT_2_STBY_XA                0x20 // X accelerometer is disabled
#define ICM20602_REG_I2C_IF                        0x70
#  define ICM20602_BITS_I2C_IF_DIS                   0x40
#define ICM20602_REG_FIFO_COUNTH                   0x72
#define ICM20602_REG_FIFO_COUNTL                   0x73
#define ICM20602_REG_FIFO_R_W                      0x74
#define ICM20602_REG_WHO_AM_I                      0x75
#  define ICM20602_WHO_AM_I_20602                    0x12

#define ICM20602_REG_XG_OFFS_TC                    0x00
#define ICM20602_REG_YG_OFFS_TC                    0x01
#define ICM20602_REG_ZG_OFFS_TC                    0x02
#define ICM20602_REG_X_FINE_GAIN                   0x03
#define ICM20602_REG_Y_FINE_GAIN                   0x04
#define ICM20602_REG_Z_FINE_GAIN                   0x05
#define ICM20602_REG_XA_OFFS_H                     0x06    // X axis accelerometer offset (high byte)
#define ICM20602_REG_XA_OFFS_L                     0x07    // X axis accelerometer offset (low byte)
#define ICM20602_REG_YA_OFFS_H                     0x08    // Y axis accelerometer offset (high byte)
#define ICM20602_REG_YA_OFFS_L                     0x09    // Y axis accelerometer offset (low byte)
#define ICM20602_REG_ZA_OFFS_H                     0x0A    // Z axis accelerometer offset (high byte)
#define ICM20602_REG_ZA_OFFS_L                     0x0B    // Z axis accelerometer offset (low byte)
#define ICM20602_REG_PRODUCT_ID                    0x0C    // Product ID Register
#define ICM20602_REG_XG_OFFS_USRH                  0x13    // X axis gyro offset (high byte)
#define ICM20602_REG_XG_OFFS_USRL                  0x14    // X axis gyro offset (low byte)
#define ICM20602_REG_YG_OFFS_USRH                  0x15    // Y axis gyro offset (high byte)
#define ICM20602_REG_YG_OFFS_USRL                  0x16    // Y axis gyro offset (low byte)
#define ICM20602_REG_ZG_OFFS_USRH                  0x17    // Z axis gyro offset (high byte)
#define ICM20602_REG_ZG_OFFS_USRL                  0x18    // Z axis gyro offset (low byte)
#define ICM20602_REG_MOT_THR                       0x1F    // detection threshold for Motion interrupt generation.  Motion is detected when the absolute value of any of the accelerometer measurements exceeds this
#define ICM20602_REG_MOT_DUR                       0x20    // duration counter threshold for Motion interrupt generation. The duration counter ticks at 1 kHz, therefore MOT_DUR has a unit of 1 LSB = 1 ms
#define ICM20602_REG_ZRMOT_THR                     0x21    // detection threshold for Zero Motion interrupt generation.
#define ICM20602_REG_ZRMOT_DUR                     0x22    // duration counter threshold for Zero Motion interrupt generation. The duration counter ticks at 16 Hz, therefore ZRMOT_DUR has a unit of 1 LSB = 64 ms.
# define ICM20602_BIT_TEMP_FIFO_EN                     0x80
# define ICM20602_BIT_XG_FIFO_EN                       0x40
# define ICM20602_BIT_YG_FIFO_EN                       0x20
# define ICM20602_BIT_ZG_FIFO_EN                       0x10
# define ICM20602_BIT_ACCEL_FIFO_EN                    0x08
# define ICM20602_BIT_SLV2_FIFO_EN                     0x04
# define ICM20602_BIT_SLV1_FIFO_EN                     0x02
# define ICM20602_BIT_SLV0_FIFI_EN0                    0x01
#define ICM20602_REG_I2C_MST_CTRL                  0x24
# define ICM20602_BIT_I2C_MST_P_NSR                    0x10
# define ICM20602_BIT_I2C_MST_CLK_400KHZ               0x0D
#define ICM20602_REG_I2C_SLV0_ADDR                 0x25
#define ICM20602_REG_I2C_SLV1_ADDR                 0x28
#define ICM20602_REG_I2C_SLV2_ADDR                 0x2B
#define ICM20602_REG_I2C_SLV3_ADDR                 0x2E
#define ICM20602_REG_I2C_SLV4_CTRL                 0x34
// bit definitions for ICM20602_REG_INT_ENABLE
# define ICM20602_BIT_RAW_RDY_EN                       0x01
# define ICM20602_BIT_DMP_INT_EN                       0x02    // enabling this bit (DMP_INT_EN) also enables RAW_RDY_EN it seems
# define ICM20602_BIT_UNKNOWN_INT_EN                   0x04
# define ICM20602_BIT_I2C_MST_INT_EN                   0x08
# define ICM20602_BIT_FIFO_OFLOW_EN                    0x10
# define ICM20602_BIT_ZMOT_EN                          0x20
# define ICM20602_BIT_MOT_EN                           0x40
# define ICM20602_BIT_FF_EN                            0x80
#define ICM20602_REG_INT_STATUS                    0x3A
// bit definitions for ICM20602_REG_INT_STATUS (same bit pattern as above because this register shows what interrupt actually fired)
# define ICM20602_BIT_RAW_RDY_INT                      0x01
# define ICM20602_BIT_DMP_INT                          0x02
# define ICM20602_BIT_UNKNOWN_INT                      0x04
# define ICM20602_BIT_I2C_MST_INT                      0x08
# define ICM20602_BIT_FIFO_OFLOW_INT                   0x10
# define ICM20602_BIT_ZMOT_INT                         0x20
# define ICM20602_BIT_MOT_INT                          0x40
# define ICM20602_BIT_FF_INT                           0x80
#define ICM20602_REG_EXT_SENS_DATA_00              0x49
#define ICM20602_REG_I2C_SLV0_DO                   0x63
#define ICM20602_REG_I2C_MST_DELAY_CTRL            0x67
# define ICM20602_BIT_I2C_SLV0_DLY_EN                  0x01
# define ICM20602_BIT_I2C_SLV1_DLY_EN                  0x02
# define ICM20602_BIT_I2C_SLV2_DLY_EN                  0x04
# define ICM20602_BIT_I2C_SLV3_DLY_EN                  0x08
#define ICM20602_REG_USER_CTRL                     0x6A
// bit definitions for ICM20602_REG_USER_CTRL
# define ICM20602_BIT_USER_CTRL_SIG_COND_RESET         0x01 // resets signal paths and results registers for all sensors (gyros, accel, temp)
# define ICM20602_BIT_USER_CTRL_FIFO_RESET             0x04 // Reset (i.e. clear) FIFO buffer
# define ICM20602_BIT_USER_CTRL_FIFO_EN                0x40 // Enable FIFO operations
#define ICM20602_REG_BANK_SEL                      0x6D    // DMP bank selection register (used to indirectly access DMP registers)
#define ICM20602_REG_MEM_START_ADDR                0x6E    // DMP memory start address (used to indirectly write to dmp memory)
#define ICM20602_REG_MEM_R_W                       0x6F // DMP related register
#define ICM20602_REG_DMP_CFG_1                     0x70 // DMP related register
#define ICM20602_REG_DMP_CFG_2                     0x71 // DMP related register

/* this is an undocumented register which
   if set incorrectly results in getting a 2.7m/s/s offset
   on the Y axis of the accelerometer
*/
#define ICM20602_REG_ICM_UNDOC1       0x11
#define ICM20602_REG_ICM_UNDOC1_VALUE 0xc9

// WHOAMI values

#define ICM20602_BIT_READ_FLAG                           0x80
#define ICM20602_BIT_I2C_IF_DIS                          0x80

#endif //ICM20602_H_INCLUDED
