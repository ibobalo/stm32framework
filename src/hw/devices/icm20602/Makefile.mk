USE_HW_IF += $(if $(filter YES,$(USE_ICM20602)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_ICM20602)), $(addprefix src/hw/devices/icm20602/, \
	dev_icm20602.cpp \
))
