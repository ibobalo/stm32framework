#include "stdafx.h"
#include "dev_icm20602.h"
#include "hw/debug.h"
#include "hw/mcu/hw_spi.h"
#include "tick_process.h"
#include "util/macros.h"
#include "util/endians.h"
#include "util/atomic.h"
#include "tick_timeout.h"

#include "icm20602.h"

#define INITIAL_DELAY             10
#define REPEAT_AFTER_ERROR_DELAY  3
#define REPEAT_AFTER_MISSED_DELAY 1000

#define ICM20602_ADDRESS        0x68 // Address default (AD0 - low)
#define ICM20602_ADDRESS_ALT    0x69 // Address alternate (AD0 - high)
#define GYRO_SAMPLE_FREQ                  32000U
#define ACCEL_SAMPLE_FREQ                 4000U
#define TEMP_SAMPLE_FREQ                  8000U
#define GYRO_TEMP_DATA_SIZE               8U /* TEMP_H, TEMP_L, GYRO_X_H,... GYRO_Z_L */
#define GYRO_SAMPLES_PER_ACCEL_SAMPLE     (GYRO_SAMPLE_FREQ / ACCEL_SAMPLE_FREQ) /* 8 */
#define GYRO_SAMPLES_PER_TEMP_SAMPLE      (GYRO_SAMPLE_FREQ / TEMP_SAMPLE_FREQ) /* 4 */
#define FIFO_DATA_SIZE_PER_ACCEL_SAMPLE   (GYRO_TEMP_DATA_SIZE * GYRO_SAMPLES_PER_ACCEL_SAMPLE)
#define FIFO_WATERMARK_THRESHOLD          FIFO_DATA_SIZE_PER_ACCEL_SAMPLE
#define FIFO_CAPACITY           1008

typedef struct {
	uint8_t reg;       uint8_t value;     bool skipverify;
} INIT_SCRIPT;
static
const INIT_SCRIPT ICM20602_INIT_REGS[] = {
	{ICM20602_REG_I2C_IF,           ICM20602_BITS_I2C_IF_DIS},  // Disable I2C communication, use SPI only
	{ICM20602_REG_USER_CTRL,        0, true},
	{ICM20602_REG_PWR_MGMT_1,       ICM20602_PWR_MGMT_1_CLK_AUTO},
	{ICM20602_REG_PWR_MGMT_2,       ICM20602_PWR_MGMT_2_ON_ALL},
	{ICM20602_REG_GYRO_CONFIG,      ICM20602_GYRO_CONFIG_FCHOICE_B_32kHz_8 |
//	{ICM20602_REG_GYRO_CONFIG,      ICM20602_GYRO_CONFIG_FCHOICE_B_NONE |
			                        ICM20602_GYRO_CONFIG_FS_SEL_500dps},
	{ICM20602_REG_ACCEL_CONFIG,     ICM20602_ACCEL_CONFIG_FS_SEL_4g},
	{ICM20602_REG_ACCEL_CONFIG2,    ICM20602_ACCEL_CONFIG2_ACCEL_FCHOICE_B},
	{ICM20602_REG_CONFIG,           ICM20602_CONFIG_EXT_SYNC_NONE |
	                                ICM20602_CONFIG_DLPF_CFG_IGNORE |
	                                ICM20602_CONFIG_FIFO_MODE_REPLACE},
	{ICM20602_REG_SMPLRT_DIV,       ICM20602_SMPLRT_IGNORE},
	{ICM20602_REG_FIFO_EN,          ICM20602_BITS_GYRO_FIFO_EN},
	{ICM20602_REG_FIFO_WM_TH1,      (FIFO_WATERMARK_THRESHOLD >> 8) & 0x03},
	{ICM20602_REG_FIFO_WM_TH2,      FIFO_WATERMARK_THRESHOLD & 0xFF},
	{ICM20602_REG_INT_PIN_CFG,      ICM20602_INT_PIN_INT_LEVEL_HIGH |
			                        ICM20602_INT_PIN_INT_PP |
			                        ICM20602_INT_PIN_LATCH_INT_EN |
			                        ICM20602_INT_PIN_INT_RD_CLEAR},
};
const INIT_SCRIPT ICM20602_RESET_REGS[] = {
	{ICM20602_REG_USER_CTRL,        0},
	{ICM20602_REG_USER_CTRL,        ICM20602_USER_CTRL_FIFO_RST, true},
	{ICM20602_REG_USER_CTRL,        ICM20602_USER_CTRL_FIFO_EN},
	{ICM20602_REG_INT_ENABLE,       ICM20602_INT_ENABLE_DATA_RDY_INT_EN},
};

#define ICM20602_DEBUG_INFO(...) DEBUG_INFO("ACC:" __VA_ARGS__)
//#define ICM20602_DEBUG_INFO(...) {}

class CDeviceICM20602 :
		public IDeviceICM20602,
		public IDeviceSPI,
		public IDeviceI2C,
		public IDigitalInNotifier,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
	typedef CTickProcess::CTimeDelta CTimeDelta;
public:
//	virtual ~CSPIDeviceICM20602();

	void Init(
			ISPI* pSPI,
			IDigitalOut* pSelect,
			II2C* pI2C,
			bool bAlternateAddress,
			IDigitalInProvider* pDrdyInt,
			ticktime_delta_t tdRefreshPeriod
	);

public: // IDeviceICM20602
	virtual const angular_rates_3D_t& GetAngularRates() const;
	virtual bool SetAngularRatesCallback(AngularRatesCallback cb);
	virtual const linear_rates_3D_t& GetLinearRates() const;
	virtual bool SetLinearRatesCallback(LinearRatesCallback cb);
	virtual unsigned GetTemperature() const;
	virtual bool SetTemperatureCallback(TemperatureCallback cb);
	virtual unsigned GetOverflows() const;

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceSPI
	virtual bool GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const;
	virtual void Select();
	virtual void Release();
	virtual void OnSPIDone();
	virtual void OnSPIError();
	virtual void OnSPIIdle();

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const; /* address is 7 bit */
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(EI2CErrorType e);
	virtual void OnI2CIdle();

public: // IDigitalInNotifier
	virtual void OnRise();
	virtual void OnFall();

private:
	typedef enum {
		OpIsInit_Check,
		OpIsInit_INIT_REGS_N,
		OpIsInit_INIT_VERIFY_N,
		OpIsInit_RESET_REGS_N,
		OpIsReadAccelTempGyro,
		OpIsReadAccelTemp,
		OpIsReadAccel,
		OpIsReadTemp,
		OpIsReadGyro,
		OpIsReadFIFOCount,
		OpIsReadFIFOCountData1,
		OpIsReadFIFOData1,
		OpIsReadFIFOCountData2,
		OpIsReadFIFOData2,
		OpIsNone,
	} ECurrentOp;
	ECurrentOp GetNextState() const;
	void ContinueLater();
	void QueueSession();
	bool GetPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	void OnPktDone();
	void OnPktError(bool bCritical);
	void ProcessFifoData(const uint16_t* pDataStart, unsigned nDataLength);

private:
	ISPI *                m_pSPI;
	IDigitalOut*          m_pSelect;
	II2C *                m_pI2C;
	unsigned              m_uiAddress;
	IDigitalInProvider*   m_pDrdyInt;
	unsigned              m_nInitStep;
	bool                  m_bCommOk;
	angular_rates_3D_t    m_Gyro;
	AngularRatesCallback  m_cbGyroOutCallback;
	unsigned              m_uiTempOut;

	linear_rates_3D_t     m_AccelOut;
	LinearRatesCallback   m_cbAccelOutCallback;
	TemperatureCallback   m_cbTempOutCallback;

	ECurrentOp m_eCurrentOp;
	unsigned  m_nInitRegIndex;
	bool      m_bCheckRequired;
	bool      m_bReinitRequired;
	bool      m_bResetRequired;
	bool      m_bInputReadRequired;
	bool      m_bFifoCountReadRequired;
	unsigned  m_uiLastInputReadTime;

	CTimeout  m_toRefreshTimeout;

	uint8_t  m_uiRegAddressToRead;
	uint8_t  m_auiRegReadBuffer[16];
	uint8_t  m_auiFifoCountReadBuffer1[2];
	uint8_t  m_auiFifoReadBuffer1[FIFO_CAPACITY];
	unsigned  m_nProcessFifoDataReady1;
	uint8_t  m_auiFifoCountReadBuffer2[2];
	uint8_t  m_auiFifoReadBuffer2[FIFO_CAPACITY];
	unsigned  m_nProcessFifoDataReady2;
	unsigned m_nFifoOverflows;
	unsigned m_nFifoKnownCount;
	unsigned m_nFifoReadSize;
	struct {
		uint8_t uiReg;
		uint8_t uiValue;
		void Assign(uint8_t _uiReg, uint8_t _uiValue) {uiReg = _uiReg; uiValue = _uiValue;};
	} m_tWriteRegPkt;
	CChainedConstChunks m_cccRegAddressToReadChunks;
	CChainedIoChunks    m_cicRegAddressIgnoredChunks;
	CChainedIoChunks    m_cicRegValueChunks;
	CChainedIoChunks    m_cicFifoAddressIgnoredChunks;
	CChainedIoChunks    m_cicFifoChunks;
	CChainedConstChunks m_cccWriteRegPktChunks;
};



void CDeviceICM20602::Init(
		ISPI* pSPI,
		IDigitalOut* pSelect,
		II2C* pI2C,
		bool bAlternateAddress,
		IDigitalInProvider* pDrdyInt,
		ticktime_delta_t tdRefreshPeriod
) {
	CParentProcess::Init(&g_TickProcessScheduller);

	ASSERTE((pSPI && pSelect && !pI2C) || (pI2C && !pSPI && !pSelect));
	m_pSPI = pSPI;
	m_pSelect = pSelect;
	m_pI2C = pI2C;
	m_uiAddress = bAlternateAddress ? ICM20602_ADDRESS_ALT : ICM20602_ADDRESS;
	m_pDrdyInt = pDrdyInt;
	m_bCommOk = false;
	m_eCurrentOp = OpIsNone;
	m_nInitRegIndex = m_pSPI ? 0 : 1;
	m_bCheckRequired = true;
	m_bReinitRequired = true;
	m_bResetRequired = true;
	m_bInputReadRequired = false;
	m_bFifoCountReadRequired = false;
	m_uiLastInputReadTime = 0;
	m_nProcessFifoDataReady1 = false;
	m_nFifoOverflows = 0;
	m_nFifoKnownCount = 0;
	m_nFifoReadSize = 0;
	m_toRefreshTimeout.Init(tdRefreshPeriod);
	m_cbGyroOutCallback = NullCallback();
	m_cbAccelOutCallback = NullCallback();
	m_cbTempOutCallback = NullCallback();
	m_cccRegAddressToReadChunks.Assign((uint8_t*)&m_uiRegAddressToRead, sizeof(m_uiRegAddressToRead));
	m_cccRegAddressToReadChunks.Break();
	m_cicRegAddressIgnoredChunks.Assign(NULL, 1);
	m_cicRegAddressIgnoredChunks.Append(&m_cicRegValueChunks);
	m_cicRegValueChunks.Assign(m_auiRegReadBuffer, 1);
	m_cicRegValueChunks.Break();
	m_cicFifoAddressIgnoredChunks.Assign(NULL, 1);
	m_cicFifoAddressIgnoredChunks.Append(&m_cicFifoChunks);
	m_cicFifoChunks.Assign(m_auiFifoReadBuffer1, 1);
	m_cicFifoChunks.Break();
	m_cccWriteRegPktChunks.Assign((uint8_t*)&m_tWriteRegPkt, sizeof(m_tWriteRegPkt));
	m_cccWriteRegPktChunks.Break();

	if (m_toRefreshTimeout.GetIsDefined()) {
		m_toRefreshTimeout.Start();
		ContinueDelay(m_toRefreshTimeout.GetTimeLeft() + 1);
	}

	if (m_pDrdyInt) {
		m_pDrdyInt->SetNotifier(this);  INDIRECT_CALL(OnRise());
	}

	ContinueNow();
}

/*virtual*/
const CDeviceICM20602::angular_rates_3D_t& CDeviceICM20602::GetAngularRates() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::GetAngularRates());
	return m_Gyro;
}
/*virtual*/
bool CDeviceICM20602::SetAngularRatesCallback(AngularRatesCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::SetAngularRatesCallback(cb));
	if (!m_cbGyroOutCallback) {
		m_cbGyroOutCallback = cb;
		return true;
	}
	return false;
}
/*virtual*/
const CDeviceICM20602::linear_rates_3D_t& CDeviceICM20602::GetLinearRates() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::GetLinearRates());
	return m_AccelOut;
}
/*virtual*/
bool CDeviceICM20602::SetLinearRatesCallback(LinearRatesCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::SetLinearRatesCallback(cb));
	if (!m_cbAccelOutCallback) {
		m_cbAccelOutCallback = cb;
		return true;
	}
	return false;
}
/*virtual*/
unsigned CDeviceICM20602::GetTemperature() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::GetTemperature());

	return m_uiTempOut;
}
/*virtual*/
bool CDeviceICM20602::SetTemperatureCallback(TemperatureCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::SetTemperatureCallback(cb));
	if (!m_cbTempOutCallback) {
		m_cbTempOutCallback = cb;
		return true;
	}
	return false;
}

/*virtual*/
unsigned CDeviceICM20602::GetOverflows() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceICM20602::GetOverflows());
	return m_nFifoOverflows;
}

/*virtual*/
void CDeviceICM20602::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	PROCESS_DEBUG_INFO(">>> Process: CDeviceICM20602");

	ICM20602_DEBUG_INFO("step. op:", m_eCurrentOp);

	if (m_toRefreshTimeout.GetIsDefined()) {
		if (m_toRefreshTimeout.GetIsElapsed()) {
			m_toRefreshTimeout.Start();
			m_bInputReadRequired = true;
		}
	}

	if (m_bCheckRequired) {
		if (AtomicCmpSet(&m_eCurrentOp, OpIsNone, OpIsInit_Check)) {
			ICM20602_DEBUG_INFO("STATE: Check start");
			m_bCheckRequired = false;
			QueueSession();
		}
	} else if (m_bReinitRequired) {
		if (AtomicCmpSet(&m_eCurrentOp, OpIsNone, OpIsInit_INIT_REGS_N)) {
			ICM20602_DEBUG_INFO("STATE: Init start");
			m_bReinitRequired = false;
			m_nInitRegIndex = m_pSPI ? 0 : 1;
			QueueSession();
		}
	} else if (m_bResetRequired) {
		if (AtomicCmpSet(&m_eCurrentOp, OpIsNone, OpIsInit_RESET_REGS_N)) {
			ICM20602_DEBUG_INFO("STATE: Reset start");
			m_bResetRequired = false;
			m_nInitRegIndex = 0;
			QueueSession();
		}
	} else if (m_bInputReadRequired) {
		if (AtomicCmpSet(&m_eCurrentOp, OpIsNone, OpIsReadAccelTempGyro)) {
			ICM20602_DEBUG_INFO("STATE: Read required");
			m_bInputReadRequired = false;
			QueueSession();
		}
	} else {
		ECurrentOp eNextState = GetNextState();
		if (eNextState != OpIsNone && AtomicCmpSet(&m_eCurrentOp, OpIsNone, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: Next read state:", eNextState);
			QueueSession();
		}
	}

	if (m_nProcessFifoDataReady1) {
		const uint16_t* pData16 = (const uint16_t*)m_auiFifoReadBuffer1;
		ICM20602_DEBUG_INFO("FIFO 1 data processing:", m_nProcessFifoDataReady1);
		ProcessFifoData(pData16, m_nProcessFifoDataReady1);
		ICM20602_DEBUG_INFO("FIFO 1 data processing done.");
		m_nProcessFifoDataReady1 = 0;
	} else if (m_nProcessFifoDataReady2) {
		const uint16_t* pData16 = (const uint16_t*)m_auiFifoReadBuffer2;
		ICM20602_DEBUG_INFO("FIFO 2 data processing:", m_nProcessFifoDataReady2);
		ProcessFifoData(pData16, m_nProcessFifoDataReady2);
		ICM20602_DEBUG_INFO("FIFO 2 data processing done.");
		m_nProcessFifoDataReady2 = 0;
	}

	ContinueLater();

	PROCESS_DEBUG_INFO("<<< Process: CDeviceICM20602");
}

/*virtual*/
bool CDeviceICM20602::GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::GetSPIParams(uiMaximumBitrate, eMode, eBitsOrder, nBitsCount));

	bool bResult = false;
	if (uiMaximumBitrate != 10000000) {
		uiMaximumBitrate = 10000000;
		bResult = true;
	}
	if (eMode != ISPI::MODE0 && eMode != ISPI::MODE3) {
		eMode = ISPI::MODE0;
		bResult = true;
	}
	if (eBitsOrder != ISPI::MSB_1st) {
		eBitsOrder = ISPI::MSB_1st;
		bResult = true;
	}
	if (nBitsCount != 8) {
		nBitsCount = 8;
		bResult = true;
	}
	return bResult;
}

/*virtual*/
void CDeviceICM20602::Select()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Select());
	m_pSelect->Reset();
//	ICM20602_DEBUG_INFO("SPI Selected");
}

/*virtual*/
void CDeviceICM20602::Release()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Release());
	m_pSelect->Set();
//	ICM20602_DEBUG_INFO("SPI Released");
}

/*virtual*/
void CDeviceICM20602::OnSPIDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIDone());

	OnPktDone();
}

/*virtual*/
void CDeviceICM20602::OnSPIError()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIError());

	OnPktError(m_eCurrentOp == OpIsInit_Check);
}

/*virtual*/
void CDeviceICM20602::OnSPIIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIIdle());

	ICM20602_DEBUG_INFO("SPI IDLE got");
	ContinueNow();
}

/*virtual*/
unsigned CDeviceICM20602::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_uiAddress;
}

/*virtual*/
bool CDeviceICM20602::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	return GetPacket(ppRetChunksToSend, ppRetChunksToRecv);
}

void CDeviceICM20602::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());

	OnPktDone();
}

void CDeviceICM20602::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	OnPktError(e == I2C_ERROR_ACK || m_eCurrentOp == OpIsInit_Check);
}

void CDeviceICM20602::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
//	ContinueNow();
	// TODO
}

/*virtual*/
void CDeviceICM20602::OnRise()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInNotifier::OnRise());
	if (m_bCommOk) {
		if (AtomicCmpSet(&m_eCurrentOp, OpIsNone, OpIsReadFIFOData1)) {
			if (AtomicCmpSet(&m_nFifoKnownCount, 0U, FIFO_DATA_SIZE_PER_ACCEL_SAMPLE)) {
				ICM20602_DEBUG_INFO("STATE: Drdy rise, read data now:", FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
			} else if (AtomicCmpSet(&m_eCurrentOp, OpIsReadFIFOData1, OpIsReadFIFOCount)) {
				ICM20602_DEBUG_INFO("STATE: Drdy rise, read count now");
			} else {ASSERT("state error");}
			QueueSession();
		} else if (AtomicCmpSet(&m_nFifoKnownCount, 0U, FIFO_DATA_SIZE_PER_ACCEL_SAMPLE)) {
			ICM20602_DEBUG_INFO("Drdy rise, read data later");
			ContinueLater();
		} else {
			m_bFifoCountReadRequired = true;
			ICM20602_DEBUG_INFO("Drdy rise, read count later");
			ContinueLater();
		}
	}
}

/*virtual*/
void CDeviceICM20602::OnFall()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInNotifier::OnFall());
	ICM20602_DEBUG_INFO("Drdy fall");
	// do nothing
}

CDeviceICM20602::ECurrentOp CDeviceICM20602::GetNextState() const
{
	ECurrentOp eNextState = (
		(m_bCheckRequired || m_bReinitRequired || m_bResetRequired || (!m_bFifoCountReadRequired && m_nFifoKnownCount < FIFO_DATA_SIZE_PER_ACCEL_SAMPLE))
			? OpIsNone
			: (m_bFifoCountReadRequired
				? OpIsReadFIFOCount
				: (!m_nProcessFifoDataReady1
						? OpIsReadFIFOData1
						: (!m_nProcessFifoDataReady2
							? OpIsReadFIFOData2
							: OpIsNone
					)
				)
			)
	);
	return eNextState;
}
void CDeviceICM20602::ContinueLater()
{
	if (m_eCurrentOp == OpIsNone && (
		(m_bCommOk && (m_bFifoCountReadRequired || m_nFifoKnownCount > FIFO_DATA_SIZE_PER_ACCEL_SAMPLE || m_bInputReadRequired))
		|| m_bReinitRequired
		|| m_bResetRequired
		|| m_nProcessFifoDataReady1
		|| m_nProcessFifoDataReady2
	)) {
		ContinueNow();
//	} else if (m_nExtraFifoCount) {
//		ContinueDelay(10);
	} else if (m_toRefreshTimeout.GetIsDefined()) {
		ContinueAt(m_toRefreshTimeout.GetFinishTime() + 1);
		ICM20602_DEBUG_INFO("Nothing to do. Wake:", m_toRefreshTimeout.GetTimeLeft() + 1);
	} else {
		ICM20602_DEBUG_INFO("Nothing to do");
	}
}

void CDeviceICM20602::QueueSession()
{
	const CChainedConstChunks* pChunksToSend;
	const CChainedIoChunks*    pChunksToRecv;
	if (m_pI2C) {
		m_pI2C->Queue_I2C_Session(this);
	} else if (GetPacket(&pChunksToSend, &pChunksToRecv)) {
		ASSERTE(m_pSPI);
		if (m_pSPI->Start_SPIx_Session(this, pChunksToSend, pChunksToRecv)) {
			ICM20602_DEBUG_INFO("SPI start, op:", m_eCurrentOp);
		} else {
			ICM20602_DEBUG_INFO("SPI queue");
			m_pSPI->RequestSPIIdleNotify(this);
		}
	} else {
		ICM20602_DEBUG_INFO("Nothing to queue");
	}
}

bool CDeviceICM20602::GetPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));

#define REG_READ(reg, size) { \
		m_uiRegAddressToRead = reg | (m_pSPI ? 0x80 : 0); \
		*ppRetChunksToSend = &m_cccRegAddressToReadChunks; \
		ASSERTE(size <= ARRAY_SIZE(m_auiRegReadBuffer)); \
		m_cicRegValueChunks.uiLength = size; \
		*ppRetChunksToRecv = m_pSPI ? &m_cicRegAddressIgnoredChunks : &m_cicRegValueChunks; \
}
#define CNTFIFO_READ(buf_cnt, buf, size) { \
		m_uiRegAddressToRead = ICM20602_REG_FIFO_COUNTH | (m_pSPI ? 0x80 : 0); \
		*ppRetChunksToSend = &m_cccRegAddressToReadChunks; \
		ASSERTE(2 <= ARRAY_SIZE(buf_cnt)); \
		ASSERTE(size <= ARRAY_SIZE(buf)); \
		STATIC_ASSERTE((const char*)buf_cnt + 2 == (const char*)buf, buffers_consecutive); \
		m_cicFifoChunks.Assign(buf_cnt, 2 + size); \
		*ppRetChunksToRecv = m_pSPI ? &m_cicFifoAddressIgnoredChunks : &m_cicFifoChunks; \
}
#define FIFO_READ(buf, size) { \
		m_uiRegAddressToRead = ICM20602_REG_FIFO_R_W | (m_pSPI ? 0x80 : 0); \
		*ppRetChunksToSend = &m_cccRegAddressToReadChunks; \
		ASSERTE(size <= ARRAY_SIZE(buf)); \
		m_cicFifoChunks.Assign(buf, size); \
		*ppRetChunksToRecv = m_pSPI ? &m_cicFifoAddressIgnoredChunks : &m_cicFifoChunks; \
}
#define REG_WRITE(reg, val) { \
		m_tWriteRegPkt.Assign(reg, val); \
		*ppRetChunksToSend = &m_cccWriteRegPktChunks; \
		*ppRetChunksToRecv = NULL; \
}
	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		return false;
		break;
	case OpIsInit_Check:
		ICM20602_DEBUG_INFO("Checking");
		REG_READ(ICM20602_REG_WHO_AM_I, 1);
		break;
	case OpIsInit_INIT_REGS_N:
		ASSERTE(m_nInitRegIndex < ARRAY_SIZE(ICM20602_INIT_REGS));
		ICM20602_DEBUG_INFO("Configuring:", m_nInitRegIndex);
		REG_WRITE(ICM20602_INIT_REGS[m_nInitRegIndex].reg, ICM20602_INIT_REGS[m_nInitRegIndex].value);
		break;
	case OpIsInit_INIT_VERIFY_N:
		ASSERTE(m_nInitRegIndex < ARRAY_SIZE(ICM20602_INIT_REGS));
		ICM20602_DEBUG_INFO("Verifying:", m_nInitRegIndex);
		REG_READ(ICM20602_INIT_REGS[m_nInitRegIndex].reg, 1);
		break;
	case OpIsInit_RESET_REGS_N:
		ASSERTE(m_nInitRegIndex < ARRAY_SIZE(ICM20602_RESET_REGS));
		ICM20602_DEBUG_INFO("Resetting:", m_nInitRegIndex);
		REG_WRITE(ICM20602_RESET_REGS[m_nInitRegIndex].reg, ICM20602_RESET_REGS[m_nInitRegIndex].value);
		break;
	case OpIsReadAccelTempGyro:
		ICM20602_DEBUG_INFO("Reading acc+temp+gyro. Len:", 14);
		REG_READ(ICM20602_REG_ACCEL_XOUT_H, 14);
		break;
	case OpIsReadAccelTemp:
		ICM20602_DEBUG_INFO("Reading acc+temp. Len:", 8);
		REG_READ(ICM20602_REG_ACCEL_XOUT_H, 8);
		break;
	case OpIsReadAccel:
		ICM20602_DEBUG_INFO("Reading acc. Len:", 6);
		REG_READ(ICM20602_REG_ACCEL_XOUT_H, 6);
		break;
	case OpIsReadTemp:
		ICM20602_DEBUG_INFO("Reading temp. Len:", 2);
		REG_READ(ICM20602_REG_TEMP_OUT_H, 2);
		break;
	case OpIsReadGyro:
		ICM20602_DEBUG_INFO("Reading gyro. Len:", 6);
		REG_READ(ICM20602_REG_GYRO_XOUT_H, 6);
		break;
	case OpIsReadFIFOCount:
		ICM20602_DEBUG_INFO("Reading FIFO count. Len:", 2);
		m_nFifoKnownCount = 0;
		m_bFifoCountReadRequired = false;
		REG_READ(ICM20602_REG_FIFO_COUNTH, 2);
		break;
	case OpIsReadFIFOCountData1:
		ASSERTE(!m_nProcessFifoDataReady1);
		ASSERTE(m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
		m_nFifoReadSize = (m_nFifoKnownCount / FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) * FIFO_DATA_SIZE_PER_ACCEL_SAMPLE;
		AtomicSub(&m_nFifoKnownCount, m_nFifoReadSize);
		m_bFifoCountReadRequired = false;
		ICM20602_DEBUG_INFO("Reading FIFO 1 count+data. Len:", 2 + m_nFifoReadSize);
		CNTFIFO_READ(m_auiFifoCountReadBuffer1, m_auiFifoReadBuffer1, m_nFifoReadSize);
		break;
	case OpIsReadFIFOData1:
		ASSERTE(!m_nProcessFifoDataReady1);
		ASSERTE(m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
		m_nFifoReadSize = (m_nFifoKnownCount / FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) * FIFO_DATA_SIZE_PER_ACCEL_SAMPLE;
		AtomicSub(&m_nFifoKnownCount, m_nFifoReadSize);
		ICM20602_DEBUG_INFO("Reading FIFO 1 data. Len:", m_nFifoReadSize);
		FIFO_READ(m_auiFifoReadBuffer1, m_nFifoReadSize);
		break;
	case OpIsReadFIFOCountData2:
		ASSERTE(m_nFifoKnownCount);
		ASSERTE(!m_nProcessFifoDataReady2);
		ASSERTE(m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
		m_nFifoReadSize = (m_nFifoKnownCount / FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) * FIFO_DATA_SIZE_PER_ACCEL_SAMPLE;
		AtomicSub(&m_nFifoKnownCount, m_nFifoReadSize);
		m_bFifoCountReadRequired = false;
		ICM20602_DEBUG_INFO("Reading FIFO 2 count+data. Len:", 2 + m_nFifoReadSize);
		CNTFIFO_READ(m_auiFifoCountReadBuffer2, m_auiFifoReadBuffer2, m_nFifoReadSize);
		break;
	case OpIsReadFIFOData2:
		ASSERTE(!m_nProcessFifoDataReady2);
		ASSERTE(m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
		m_nFifoReadSize = (m_nFifoKnownCount / FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) *  FIFO_DATA_SIZE_PER_ACCEL_SAMPLE;
		AtomicSub(&m_nFifoKnownCount, m_nFifoReadSize);
		ICM20602_DEBUG_INFO("Reading FIFO 2 data. Len:", m_nFifoReadSize);
		FIFO_READ(m_auiFifoReadBuffer2, m_nFifoReadSize);
		break;
	}
	return true;
}

void CDeviceICM20602::OnPktDone()
{
	ICM20602_DEBUG_INFO("Pkt Done");
	ECurrentOp eNextState = GetNextState();
	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit_Check:
		if (m_auiRegReadBuffer[0] == ICM20602_WHO_AM_I_20602 && AtomicCmpSet(&m_eCurrentOp, OpIsInit_Check, OpIsNone)) {
			DEBUG_INFO("ICM20602 detected. Addr:", m_uiAddress);
		} else {
			DEBUG_INFO("ICM20602 missed. Addr:", m_uiAddress);
			m_eCurrentOp = OpIsNone;
			m_bReinitRequired = true;
			m_bResetRequired = true;
			m_bCheckRequired = true;
			m_bCommOk = false;
			ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
			return;
		}
		break;
	case OpIsInit_INIT_REGS_N:
		ASSERTE(!m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsInit_INIT_REGS_N, OpIsInit_INIT_VERIFY_N)) {
			ICM20602_DEBUG_INFO("STATE: init reg done:", m_nInitRegIndex);
		} else {ASSERT("state error");}
		if (ICM20602_INIT_REGS[m_nInitRegIndex].skipverify) {
			m_auiRegReadBuffer[0] = ICM20602_INIT_REGS[m_nInitRegIndex].value;
			// no break, keep processing OpIsInit_INIT_VERIFY_N state
		} else {
			QueueSession();
			break;
		}
		// no break
	case OpIsInit_INIT_VERIFY_N:
		ASSERTE(!m_bCommOk);
		if (m_auiRegReadBuffer[0] == ICM20602_INIT_REGS[m_nInitRegIndex].value) {
			ICM20602_DEBUG_INFO("init reg verified:", m_nInitRegIndex);
			++m_nInitRegIndex;
		} else {
			ICM20602_DEBUG_INFO("init reg failed:", m_nInitRegIndex);
			ICM20602_DEBUG_INFO("init reg read:", m_auiRegReadBuffer[0]);
			ICM20602_DEBUG_INFO("init reg prog:", ICM20602_INIT_REGS[m_nInitRegIndex].value);
		}
		if (m_nInitRegIndex < ARRAY_SIZE(ICM20602_INIT_REGS)) {
			eNextState = OpIsInit_INIT_REGS_N;
		} else {
			ICM20602_DEBUG_INFO("init done:", m_nInitRegIndex);
			eNextState = OpIsInit_RESET_REGS_N;
			m_nInitRegIndex = 0;
			m_bResetRequired = false;
		}
		if (AtomicCmpSet(&m_eCurrentOp, OpIsInit_INIT_VERIFY_N, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: verified, next:", eNextState);
			if (eNextState != OpIsNone) {
				QueueSession();
			}
		} else {ASSERT("state error");}
		break;
	case OpIsInit_RESET_REGS_N:
		ASSERTE(!m_bCommOk);
		if (++m_nInitRegIndex < ARRAY_SIZE(ICM20602_RESET_REGS)) {
			QueueSession();
		} else {
			m_bCommOk = true;
			if (m_toRefreshTimeout.GetIsDefined()) {
				m_toRefreshTimeout.Start();
				m_bInputReadRequired = true;
			}
			eNextState = GetNextState();
			if (AtomicCmpSet(&m_eCurrentOp, OpIsInit_RESET_REGS_N, eNextState)) {
				ICM20602_DEBUG_INFO("STATE: reset done:", m_nInitRegIndex);
				if (eNextState != OpIsNone) {
					QueueSession();
				}
			} else {ASSERT("state error");}
		}
		break;
	case OpIsReadAccelTempGyro:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadAccelTempGyro, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: done accel+temp+gyro read, next:", eNextState);
			if (eNextState != OpIsNone) {
				QueueSession();
			}
			const uint16_t* pData16 = (const uint16_t*)m_auiRegReadBuffer;
			m_AccelOut.iX  = ntohs(*pData16++);
			m_AccelOut.iY  = ntohs(*pData16++);
			m_AccelOut.iZ  = ntohs(*pData16++);
			m_uiTempOut    = ntohs(*pData16++);
			m_Gyro.iX      = ntohs(*pData16++);
			m_Gyro.iY      = ntohs(*pData16++);
			m_Gyro.iZ      = ntohs(*pData16++);
			ICM20602_DEBUG_INFO("Read all. X:", m_AccelOut.iX);
			if (m_cbGyroOutCallback) m_cbGyroOutCallback(m_Gyro);
			if (m_cbAccelOutCallback)  m_cbAccelOutCallback(m_AccelOut);
			if (m_cbTempOutCallback)  m_cbTempOutCallback(m_uiTempOut);
		} else {ASSERT("state error");}
		break;
	case OpIsReadAccelTemp:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadAccelTemp, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: done accel+temp read, next:", eNextState);
			if (eNextState != OpIsNone) {
				QueueSession();
			}
			const uint16_t* pData16 = (const uint16_t*)m_auiRegReadBuffer;
			m_AccelOut.iX = ntohs(*pData16++);
			m_AccelOut.iY = ntohs(*pData16++);
			m_AccelOut.iZ = ntohs(*pData16++);
			m_uiTempOut   = ntohs(*pData16++);
			ICM20602_DEBUG_INFO("Read accel temp. X:", m_AccelOut.iX);
			if (m_cbGyroOutCallback) m_cbGyroOutCallback(m_Gyro);
			if (m_cbAccelOutCallback)  m_cbAccelOutCallback(m_AccelOut);
			if (m_cbTempOutCallback)  m_cbTempOutCallback(m_uiTempOut);
		} else {ASSERT("state error");}
		break;
	case OpIsReadAccel:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadAccel, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: done accel read, next:", eNextState);
			if (eNextState != OpIsNone) {
				QueueSession();
			}
			const uint16_t* pData16 = (const uint16_t*)m_auiRegReadBuffer;
			m_AccelOut.iX = ntohs(*pData16++);
			m_AccelOut.iY = ntohs(*pData16++);
			m_AccelOut.iZ = ntohs(*pData16++);
			if (m_cbAccelOutCallback)  m_cbAccelOutCallback(m_AccelOut);
			ICM20602_DEBUG_INFO("Read acc. X:", m_AccelOut.iX);
		} else {ASSERT("state error");}
		break;
	case OpIsReadTemp:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadTemp, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: done temp read, next:", eNextState);
			if (eNextState != OpIsNone) {
				QueueSession();
			}
			const uint16_t* pData16 = (const uint16_t*)m_auiRegReadBuffer;
			m_uiTempOut = ntohs(*pData16++);
			if (m_cbTempOutCallback)  m_cbTempOutCallback(m_uiTempOut);
			ICM20602_DEBUG_INFO("Read temp. X:", m_uiTempOut);
		} else {ASSERT("state error");}
		break;
	case OpIsReadGyro:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadGyro, eNextState)) {
			ICM20602_DEBUG_INFO("STATE: done gyro read, next:", eNextState);
			if (eNextState != OpIsNone) {
				QueueSession();
			}
			const uint16_t* pData16 = (const uint16_t*)m_auiRegReadBuffer;
			m_Gyro.iX = ntohs(*pData16++);
			m_Gyro.iY = ntohs(*pData16++);
			m_Gyro.iZ = ntohs(*pData16++);
			if (m_cbGyroOutCallback) m_cbGyroOutCallback(m_Gyro);
			ICM20602_DEBUG_INFO("Read gyro. X:", m_Gyro.iX);
		} else {ASSERT("state error");}
		break;
	case OpIsReadFIFOCount:
		ASSERTE(m_bCommOk);
		if (!AtomicCmpSet(&m_nFifoKnownCount, 0U, (unsigned)ntohs(*(const uint16_t*)m_auiRegReadBuffer))) {
			ICM20602_DEBUG_INFO("Read FIFO count conflict, read again");
			QueueSession();
		} else {
			if (m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) {
				ICM20602_DEBUG_INFO("Read FIFO count:", m_nFifoKnownCount);
				ASSERTE(m_nFifoKnownCount <= FIFO_CAPACITY * 90 / 100);
				if (m_nFifoKnownCount >= FIFO_CAPACITY) {
					ASSERT("FIFO overflow");
					++m_nFifoOverflows;
					m_nFifoKnownCount = 0;
					m_bFifoCountReadRequired = true;
					m_bResetRequired = true;
				}
			} else {
				ICM20602_DEBUG_INFO("Read FIFO empty");
			}
			eNextState = GetNextState();
			if (AtomicCmpSet(&m_eCurrentOp, OpIsReadFIFOCount, eNextState)) {
				ICM20602_DEBUG_INFO("STATE: done fifo count read, next:", eNextState);
				if (eNextState != OpIsNone) {
					QueueSession();
				}
			} else {ASSERT("state error");}
		}
		break;
	case OpIsReadFIFOCountData1:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadFIFOCountData1, OpIsReadAccel)) {
			ICM20602_DEBUG_INFO("STATE: done fifo count+data1 read");
			unsigned nFIFOCount  = ntohs(*(const uint16_t*)m_auiFifoCountReadBuffer1);
			m_nProcessFifoDataReady1 = m_nFifoReadSize;
			QueueSession();
			ASSERTE(nFIFOCount >= m_nFifoReadSize);
			unsigned nFifoDataLeft = nFIFOCount - m_nFifoReadSize;
			ICM20602_DEBUG_INFO("Read FIFO 1 count+data:", nFIFOCount);
			ASSERTE(m_nFifoReadSize >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
			ASSERTE(m_nFifoReadSize % FIFO_DATA_SIZE_PER_ACCEL_SAMPLE == 0);
			if (nFifoDataLeft < FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) {
				// no data left in FIFO
			} else if (nFIFOCount < FIFO_CAPACITY) {
				m_nFifoKnownCount = nFifoDataLeft;
				ICM20602_DEBUG_INFO("Extra FIFO to read:", m_nFifoKnownCount);
				ASSERTE(m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
				ASSERTE(m_nFifoKnownCount <= FIFO_CAPACITY * 90 / 100);
			} else {
				ASSERT("FIFO overflow");
				++m_nFifoOverflows;
				m_nFifoKnownCount = 0;
				m_bFifoCountReadRequired = true;
				m_bResetRequired = true;
			}
//			ICM20602_DEBUG_INFO("FIFO data and count done, more:", m_nFifoKnownCount);
		} else {ASSERT("state error");}
		break;
	case OpIsReadFIFOData1:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadFIFOData1, OpIsReadAccel)) {
			ICM20602_DEBUG_INFO("STATE: done fifo data1 read");
			QueueSession();
			ICM20602_DEBUG_INFO("Read FIFO 1 data, size:", m_nFifoReadSize);
			m_nProcessFifoDataReady1 = m_nFifoReadSize;
		} else {ASSERT("state error");}
		break;
	case OpIsReadFIFOCountData2:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadFIFOCountData2, OpIsReadAccel)) {
			ICM20602_DEBUG_INFO("STATE: done fifo count+data2 read");
			unsigned nFIFOCount = ntohs(*(const uint16_t*)m_auiFifoCountReadBuffer2);
			m_nProcessFifoDataReady2 = m_nFifoReadSize;
			QueueSession();
			ASSERTE(nFIFOCount >= m_nFifoReadSize);
			unsigned nFifoDataLeft = nFIFOCount - m_nFifoReadSize;
			ICM20602_DEBUG_INFO("Read FIFO 2 count+data:", nFIFOCount);
			ASSERTE(m_nFifoReadSize >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
			ASSERTE(m_nFifoReadSize % FIFO_DATA_SIZE_PER_ACCEL_SAMPLE == 0);
			if (nFifoDataLeft < FIFO_DATA_SIZE_PER_ACCEL_SAMPLE) {
				// no data left in FIFO
			} else if (nFIFOCount < FIFO_CAPACITY) {
				m_nFifoKnownCount = nFifoDataLeft;
				ICM20602_DEBUG_INFO("Extra FIFO to read:", m_nFifoKnownCount);
				ASSERTE(m_nFifoKnownCount >= FIFO_DATA_SIZE_PER_ACCEL_SAMPLE);
				ASSERTE(m_nFifoKnownCount <= FIFO_CAPACITY * 90 / 100);
			} else {
				ASSERT("FIFO overflow");
				++m_nFifoOverflows;
				m_nFifoKnownCount = 0;
				m_bFifoCountReadRequired = true;
				m_bResetRequired = true;
			}
//			ICM20602_DEBUG_INFO("FIFO data and count done, more:", m_nFifoKnownCount);
		} else {ASSERT("state error");}
		break;
	case OpIsReadFIFOData2:
		ASSERTE(m_bCommOk);
		if (AtomicCmpSet(&m_eCurrentOp, OpIsReadFIFOData2, OpIsReadAccel)) {
			ICM20602_DEBUG_INFO("STATE: done fifo data2 read");
			QueueSession();
			ICM20602_DEBUG_INFO("Read FIFO 2 data, size:", m_nFifoReadSize);
			m_nProcessFifoDataReady2 = m_nFifoReadSize;
		} else {ASSERT("state error");}
		break;
	}
	ContinueLater();
}

void CDeviceICM20602::OnPktError(bool bCritical)
{

	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit_Check:
	case OpIsInit_INIT_REGS_N:
	case OpIsInit_INIT_VERIFY_N:
	case OpIsInit_RESET_REGS_N:
		m_bCommOk = false;
		m_bCheckRequired = true;
		m_bReinitRequired = true;
		m_bResetRequired = true;
		break;
	case OpIsReadAccelTempGyro:
	case OpIsReadAccelTemp:
	case OpIsReadGyro:
	case OpIsReadAccel:
	case OpIsReadTemp:
		m_bInputReadRequired = true;
		break;
	case OpIsReadFIFOCount:
	case OpIsReadFIFOCountData1:
	case OpIsReadFIFOData1:
	case OpIsReadFIFOCountData2:
	case OpIsReadFIFOData2:
		break;
	}

	if (bCritical) {
		DEBUG_INFO("ICM20602 missed. Addr:", m_uiAddress);
		m_bCommOk = false;
		m_bCheckRequired = true;
		m_bReinitRequired = true;
		m_bResetRequired = true;
		ContinueDelay(REPEAT_AFTER_MISSED_DELAY);
	} else {
		DEBUG_INFO("ICM20602 error");
		m_bCheckRequired = true;
		ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
	}

	m_eCurrentOp = OpIsNone;
	ICM20602_DEBUG_INFO("STATE: none by pkt error");
}

void CDeviceICM20602::ProcessFifoData(const uint16_t* pDataStart, unsigned nDataLength)
{
	const uint16_t* pData = (const uint16_t*)pDataStart;
	const uint16_t* pDataEnd = (const uint16_t*)((const uint8_t*)pDataStart + m_nFifoReadSize);
	unsigned n = 0;
	ASSERTE(pData < pDataEnd);
	while (pData < pDataEnd) {
		m_uiTempOut = ntohs(*pData++);
		m_Gyro.iX   = ntohs(*pData++);
		m_Gyro.iY   = ntohs(*pData++);
		m_Gyro.iZ   = ntohs(*pData++);
		ASSERTE(m_uiTempOut < 2000);
		ASSERTE(m_Gyro.iX > -1000 && m_Gyro.iX < 1000);
		ASSERTE(m_Gyro.iY > -1000 && m_Gyro.iY < 1000);
		ASSERTE(m_Gyro.iZ > -1000 && m_Gyro.iZ < 1000);
		if (m_cbGyroOutCallback) m_cbGyroOutCallback(m_Gyro);
		if (m_cbTempOutCallback && n % GYRO_SAMPLES_PER_TEMP_SAMPLE == 0) m_cbTempOutCallback(m_uiTempOut);
		++n;
	}
	ASSERTE(pData == pDataEnd);
	ASSERTE(n * 8 == m_nFifoReadSize);
//	if (m_pDrdyInt && m_pDrdyInt->GetValue()) m_bFifoReadRequired = true;
}


CDeviceICM20602       g_ICM20602;

IDeviceICM20602* AcquireICM20602Interface(
		ISPI* pSPI,
		IDigitalOut* pSelect,
		IDigitalInProvider* pDrdyInt,
		ticktime_delta_t tdRefreshPeriod
) {
	g_ICM20602.Init(pSPI, pSelect, NULL, false, pDrdyInt, tdRefreshPeriod);
	return &g_ICM20602;
}

IDeviceICM20602* AcquireICM20602Interface(
		II2C * pI2C,
		bool bAlternateAddress,
		IDigitalInProvider* pDrdyInt,
		ticktime_delta_t tdRefreshPeriod
) {
	g_ICM20602.Init(NULL, NULL, pI2C, bAlternateAddress, pDrdyInt, tdRefreshPeriod);
	return &g_ICM20602;
}
