#ifndef DEV_ICM20602_H_INCLUDED
#define DEV_ICM20602_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include <stdint.h>

class IDeviceICM20602
{
public:
	typedef CTickTimeSource::time_delta_t time_delta_t;
	typedef struct angular_rates_3D { int16_t iX; int16_t iY; int16_t iZ; } angular_rates_3D_t;
	typedef Callback<void (const angular_rates_3D_t&)> AngularRatesCallback;
	typedef enum {ANGULAR_MAXRATE_250, ANGULAR_MAXRATE_500, ANGULAR_MAXRATE_1000, ANGULAR_MAXRATE_2000} angular_maxrate_t;
	typedef struct linear_rates_3D { int16_t iX; int16_t iY; int16_t iZ; } linear_rates_3D_t;
	typedef Callback<void (const linear_rates_3D_t&)> LinearRatesCallback;
	typedef enum {LINEAR_MAXRATE_2G, LINEAR_MAXRATE_4G, LINEAR_MAXRATE_8G, LINEAR_MAXRATE_16G} linear_maxrate_t;
	typedef Callback<void (int)> TemperatureCallback;
public:
//	virtual ~IDeviceICM20602();

public: // angular rates (gyroscope)
	virtual const angular_rates_3D_t& GetAngularRates() const = 0;
	virtual bool SetAngularRatesCallback(AngularRatesCallback cb) = 0;
public: // linear rates (accelerometer)
	virtual const linear_rates_3D_t& GetLinearRates() const = 0;
	virtual bool SetLinearRatesCallback(LinearRatesCallback cb) = 0;
public: // temperature
	virtual unsigned GetTemperature() const = 0;
	virtual bool SetTemperatureCallback(TemperatureCallback cb) = 0;
public: // diag
	virtual unsigned GetOverflows() const = 0;
};

IDeviceICM20602* AcquireICM20602Interface(
		ISPI* pSPI,
		IDigitalOut* pSelect,
		IDigitalInProvider* pDrdyInt = NULL,
		ticktime_delta_t tdRefreshPeriod = 0
);
IDeviceICM20602* AcquireICM20602Interface(
		II2C * pI2C,
		bool bAlternateAddress = false,
		IDigitalInProvider* pDrdyInt = NULL,
		ticktime_delta_t tdRefreshPeriod = 0
);

#endif //DEV_ICM20602_H_INCLUDED
