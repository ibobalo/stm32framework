#ifndef DEV_AS5045_H_INCLUDED
#define DEV_AS5045_H_INCLUDED

#include "dev_as504x.h"

typedef TDeviceAS504x<12> CDeviceAS5045;

CDeviceAS5045* AcquireAS5045(ISPI* pSPI, IDigitalOut* pSelect, uint16_t uiAngleOffset, CTickProcess::CTimeDelta tdRefreshPeriod);

void ReleaseAS5045();

#endif //DEV_AS5045_H_INCLUDED
