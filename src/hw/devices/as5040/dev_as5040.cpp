#include "dev_as5040.h"

#include "dev_as504x.cxx"

template class TDeviceAS504x<10>;

CDeviceAS5040       g_AS5040;

CDeviceAS5040* AcquireAS5040(ISPI* pSPI, IDigitalOut* pSelect, uint16_t uiAngleOffset, CTickProcess::CTimeDelta tdRefreshPeriod)
{
	g_AS5040.Init(pSPI, pSelect, uiAngleOffset, tdRefreshPeriod);
	return &g_AS5040;
}

void ReleaseAS5040()
{
	g_AS5040.Deinit();
}
