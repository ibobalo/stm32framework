#ifndef DEV_AS5040_H_INCLUDED
#define DEV_AS5040_H_INCLUDED

#include "dev_as504x.h"

typedef TDeviceAS504x<10> CDeviceAS5040;

CDeviceAS5040* AcquireAS5040(ISPI* pSPI, IDigitalOut* pSelect, uint16_t uiAngleOffset, CTickProcess::CTimeDelta tdRefreshPeriod);

void ReleaseAS5040();

#endif //DEV_AS5040_H_INCLUDED
