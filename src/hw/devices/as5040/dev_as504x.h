#ifndef DEV_AS504X_H_INCLUDED
#define DEV_AS504X_H_INCLUDED

#include <stdint.h>
#include "util/callback.h"
#include "tick_process.h"
#include "tick_timeout.h"

template<unsigned nResolutionBits>
class TDeviceAS504x :
		public IDeviceSPI,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
	typedef CTickProcess::CTimeDelta CTimeDelta;
	typedef enum {NotReady, DataValid, MagnetLost, CommError, DataTimeout} EStatus;
	typedef Callback<void (uint16_t)> AngleCallback;
	typedef Callback<void (EStatus)> StatusCallback;

	enum {
		AngleMax = 0xFFFF,
		AngleBits = nResolutionBits,
		AngleStep = (1 << (16 - nResolutionBits)) - 1,
		AngleMask = 0xFFFF ^ AngleStep,
		StatusBits = 5,
	};

public:
//	virtual ~IDeviceAS5040();

	void Init(ISPI* pSPI, IDigitalOut* pSelect, uint16_t uiAngleOffset, CTickProcess::CTimeDelta tdRefreshPeriod);
	void Deinit();

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceSPI
	virtual bool GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const;
	virtual void Select();
	virtual void Release();
	virtual void OnSPIDone();
	virtual void OnSPIError();
	virtual void OnSPIIdle();

public:
	bool Zero(uint16_t uiNewAngle);
	void StartSensorRead();
	EStatus GetStatus() const {return m_eStatus;}
	uint16_t GetAngle() const {return m_uiAngle;}
	uint16_t GetAngleOffset() const {return m_uiAngleOffset;}
	void SetStatusCallback(StatusCallback cb) {m_cbStatusCallback = cb;}
	void SetAngleCallback(AngleCallback cb) {m_cbAngleCallback = cb;}
private:
	void SetStatus(EStatus eStatus);
	void ProcessSensorData(uint16_t uiAngle, uint8_t uiStatusBits);

	EStatus m_eStatus;
	unsigned m_nCommErrorCounts;
	unsigned m_nMagnetErrorCounts;
	uint16_t m_uiAngle;
	uint8_t m_uiStatusBits;
	StatusCallback m_cbStatusCallback;
	AngleCallback m_cbAngleCallback;

	ISPI* m_pSPI;
	IDigitalOut* m_pSelect;
	uint16_t  m_uiAngleOffset;
	CTimeout  m_toDataTimeout;
	CTimeout  m_toRefreshTimeout;
	volatile uint8_t m_auiReadBuffer[((AngleBits + StatusBits + 1) + 7) / 8];
	CChainedIoChunks m_cicReadValueChunks;
	bool m_bReadRequired;
};

#endif //DEV_AS504X_H_INCLUDED
