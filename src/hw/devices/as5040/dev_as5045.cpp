#include "dev_as5045.h"

#include "dev_as504x.cxx"

template class TDeviceAS504x<12>;

CDeviceAS5045       g_AS5045;

CDeviceAS5045* AcquireAS5045(ISPI* pSPI, IDigitalOut* pSelect, uint16_t uiAngleOffset, CTickProcess::CTimeDelta tdRefreshPeriod)
{
	g_AS5045.Init(pSPI, pSelect, uiAngleOffset, tdRefreshPeriod);
	return &g_AS5045;
}

void ReleaseAS5045()
{
	g_AS5045.Deinit();
}
