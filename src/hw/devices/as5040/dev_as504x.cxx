#include "hw/devices/as5040/dev_as504x.h"
#include "stdafx.h"
#include "hw/debug.h"
#include "hw/mcu/hw_spi.h"
#include "tick_process.h"
#include "util/macros.h"
#include "util/endians.h"
#include "util/atomic.h"
#include "util/bitops.h"

#define AS504X_STATUS_BIT_OCF     0x80
#define AS504X_STATUS_BIT_COF     0x40
#define AS504X_STATUS_BIT_LIN     0x20
#define AS504X_STATUS_BIT_MagINC  0x10
#define AS504X_STATUS_BIT_MagDEC  0x08
#define AS504X_STATUS_BIT_ParEven 0x04

//#define AS504X_DEBUG_INFO(...) DEBUG_INFO("AS504X:" __VA_ARGS__)
#define AS504X_DEBUG_INFO(...) {}

template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::Init(ISPI* pSPI, IDigitalOut* pSelect, uint16_t uiAngleOffset, CTickProcess::CTimeDelta tdRefreshPeriod)
{
	CParentProcess::Init(&g_TickProcessScheduller);

	ASSERTE(pSPI && pSelect);
	m_pSPI = pSPI;
	m_pSelect = pSelect;
	m_uiAngleOffset = uiAngleOffset;

	m_eStatus = EStatus::NotReady;
	m_nCommErrorCounts = 0;
	m_nMagnetErrorCounts = 0;
	m_uiAngle = 0;
	m_cbStatusCallback = NullCallback();
	m_cbAngleCallback = NullCallback();
	m_toDataTimeout.Init(1000);
	m_toRefreshTimeout.Init(tdRefreshPeriod);
	m_cicReadValueChunks.Assign(m_auiReadBuffer, sizeof(m_auiReadBuffer));
	m_cicReadValueChunks.Break();

	if (m_toRefreshTimeout.GetIsDefined()) {
		m_bReadRequired = true;
		m_toRefreshTimeout.Start();
		ContinueDelay(m_toRefreshTimeout.GetTimeLeft());
	} else {
		m_bReadRequired = false;
	}
}

template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::Deinit()
{
	ASSERTE(m_pSPI && m_pSelect);
	m_pSPI = NULL;
	m_pSelect = NULL;
	SetStatus(EStatus::NotReady);
	m_cbStatusCallback = NullCallback();
	m_cbAngleCallback = NullCallback();
	m_bReadRequired = false;
	Cancel();
}

/*virtual*/
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	PROCESS_DEBUG_INFO(">>> Process: CDeviceAS504x");

	if (m_eStatus == EStatus::DataValid && m_toDataTimeout.GetIsDefined()) {
		if (m_toDataTimeout.GetIsElapsed()) {
			SetStatus(EStatus::DataTimeout);
			AS504X_DEBUG_INFO("Data timeout");
		} else {
			ContinueAt(m_toDataTimeout.GetFinishTime());
		}
	}

	if (m_toRefreshTimeout.GetIsDefined()) {
		if (m_toRefreshTimeout.GetIsElapsed()) {
			m_toRefreshTimeout.Start();
			m_bReadRequired = true;
		} else {
			ContinueAt(m_toRefreshTimeout.GetFinishTime());
		}
	}
	if (m_bReadRequired) {
		m_bReadRequired = false;
		Cancel();
		AS504X_DEBUG_INFO("SPI start");
		if (!m_pSPI->Start_SPIx_Session(this, NULL, &m_cicReadValueChunks)) {
			m_bReadRequired = true;
			if (m_eStatus == EStatus::DataValid && m_toDataTimeout.GetIsDefined()) {
				ContinueAt(m_toDataTimeout.GetFinishTime());
			}
			AS504X_DEBUG_INFO("SPI queue");
			m_pSPI->RequestSPIIdleNotify(this); // continue in OnSPIIdle
		}
	}

	PROCESS_DEBUG_INFO("<<< Process: CDeviceAS504x");
}

/*virtual*/
template<unsigned nResolutionBits>
bool TDeviceAS504x<nResolutionBits>::GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::GetSPIParams(uiMaximumBitrate, eMode, eBitsOrder, nBitsCount));

	bool bResult = false;
	if (uiMaximumBitrate != 500000) {
		uiMaximumBitrate = 500000;
		bResult = true;
	}
	if (eMode != ISPI::MODE_0_1) {
		eMode = ISPI::MODE_0_1;
		bResult = true;
	}
	if (eBitsOrder != ISPI::MSB_1st) {
		eBitsOrder = ISPI::MSB_1st;
		bResult = true;
	}
	if (nBitsCount != 8) {
		nBitsCount = 8;
		bResult = true;
	}
	return bResult;
}

/*virtual*/
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::Select()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Select());
	m_pSelect->Reset();
	AS504X_DEBUG_INFO("SPI Selected");
}

/*virtual*/
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::Release()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::Release());
	m_pSelect->Set();
	AS504X_DEBUG_INFO("SPI Released");
}

/*virtual*/
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::OnSPIDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIDone());

	uint16_t uiAngle = ((m_auiReadBuffer[0] << 8) | m_auiReadBuffer[1]) & AngleMask; // bits 0-11
	uint8_t uiStatusBits = m_auiReadBuffer[1] << (AngleBits - 8);
	if (ARRAY_SIZE(m_auiReadBuffer) > 2) {
		uiStatusBits |= m_auiReadBuffer[2] >> (16 - AngleBits);
	}
	uiStatusBits &= ((1 << (StatusBits + 1)) - 1) << (8 - StatusBits - 1);

	ProcessSensorData(uiAngle, uiStatusBits);

	if (m_toRefreshTimeout.GetIsDefined()) {
		ContinueAt(m_toRefreshTimeout.GetFinishTime());
	} else if (m_eStatus == EStatus::DataValid && m_toDataTimeout.GetIsDefined()) {
		ContinueAt(m_toDataTimeout.GetFinishTime());
	}
	AS504X_DEBUG_INFO("Done");
}

/*virtual*/
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::OnSPIError()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIError());

	if (m_nCommErrorCounts > 5) {
		SetStatus(EStatus::CommError);
	} else {
		++m_nCommErrorCounts;
	}

	m_bReadRequired = true;
	ContinueNow();
	AS504X_DEBUG_INFO("ERROR");
}

/*virtual*/
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::OnSPIIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceSPI::OnSPIIdle());

	AS504X_DEBUG_INFO("SPI IDLE got");
	ContinueNow();
}

template<unsigned nResolutionBits>
bool TDeviceAS504x<nResolutionBits>::Zero(uint16_t uiNewAngle)
{
	if (m_eStatus != DataValid) return false;

	uint16_t uiAngleOrg = m_uiAngle - m_uiAngleOffset;
	// from equation: uiNewAngle = uiAngleOrg + m_uiAngleOffset;
	m_uiAngleOffset = uiNewAngle - uiAngleOrg;

	return true;
}
template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::StartSensorRead()
{
	m_bReadRequired = true;
	ContinueNow();
}

template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::SetStatus(EStatus eStatus)
{
	if (m_eStatus != eStatus) {
		m_eStatus = eStatus;
		AS504X_DEBUG_INFO("Status changed:", eStatus);
		if (m_cbStatusCallback) m_cbStatusCallback(eStatus);
	}
}

template<unsigned nResolutionBits>
void TDeviceAS504x<nResolutionBits>::ProcessSensorData(uint16_t uiAngle, uint8_t uiStatusBits)
{
	if (CheckBitsParityOdd(uiAngle) == CheckBitsParityOdd(uiStatusBits)) {
		if (
				   (uiStatusBits & (AS504X_STATUS_BIT_OCF | AS504X_STATUS_BIT_COF | AS504X_STATUS_BIT_LIN)) == AS504X_STATUS_BIT_OCF
				&& (uiStatusBits & (AS504X_STATUS_BIT_MagINC | AS504X_STATUS_BIT_MagDEC)) != (AS504X_STATUS_BIT_MagINC | AS504X_STATUS_BIT_MagDEC)
		) {
			AS504X_DEBUG_INFO("Read angle:", uiAngle);
			m_uiAngle = uiAngle + m_uiAngleOffset;
			m_uiStatusBits = uiStatusBits;
			m_toDataTimeout.Start();
			m_nMagnetErrorCounts = 0;
			if (m_cbAngleCallback) m_cbAngleCallback(m_uiAngle);
			SetStatus(EStatus::DataValid);
		} else if (m_nMagnetErrorCounts < 5) {
			++m_nMagnetErrorCounts;
		} else {
			AS504X_DEBUG_INFO("Angle invalid, status:", uiStatusBits >> 1);
			SetStatus(EStatus::MagnetLost);
		}

		m_nCommErrorCounts = 0;
	} else {
		if (m_nCommErrorCounts < 5) {
			++m_nCommErrorCounts;
		} else {
			SetStatus(EStatus::CommError);
		}
		AS504X_DEBUG_INFO("Checksum bit invalid, data:", (m_auiReadBuffer[0] << 16) | (m_auiReadBuffer[1] << 8) | (ARRAY_SIZE(m_auiReadBuffer) > 2 ? m_auiReadBuffer[2] >> 6 : 0));
	}
}
