USE_AS504X += $(if $(filter YES,$(USE_AS5040)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_AS5040)), $(addprefix src/hw/devices/as5040/, \
	dev_as5040.cpp \
))

USE_AS504X += $(if $(filter YES,$(USE_AS5045)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_AS5045)), $(addprefix src/hw/devices/as5040/, \
	dev_as5045.cpp \
))

USE_SPI += $(if $(filter YES,$(USE_AS504X)), \
	YES \
)
USE_HW_IF += $(if $(filter YES,$(USE_AS504X)), \
	YES \
)
