USE_I2C += $(if $(filter YES,$(USE_PCA9532)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_PCA9532)), $(addprefix src/hw/devices/pca9532/, \
	dev_pca9532.cpp \
))
