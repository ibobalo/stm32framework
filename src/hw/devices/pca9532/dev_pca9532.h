#ifndef DEV_PCA9532_H_INCLUDED
#define DEV_PCA9532_H_INCLUDED

#include "util/callback.h"
#include "hw/tick_timesource.h"
#include "interfaces/i2c.h"
#include "interfaces/gpio.h"
#include <stdint.h>

class IDevicePCA9532
{
public:
	typedef CTickTimeSource::time_delta_t time_delta_t;
	typedef Callback<void (uint16_t)> KeysChangedCallback;
public:
//	virtual ~IDevicePCA9532();

	virtual void Init(II2C* pI2CMaster, unsigned uiAddress, time_delta_t tdKeyRefreshPeriod, time_delta_t tdAllRefreshPeriod) = 0;

public: // LEDs interface
	virtual void ClearLed(unsigned nIndex) = 0;
	virtual void SetLed(unsigned nIndex) = 0;
	virtual void ToggleLed(unsigned nIndex) = 0;
	virtual void BlinkLed(unsigned nIndex) = 0;
	virtual void FastBlinkLed(unsigned nIndex) = 0;

public: // Inputs interface
//	virtual void RefreshAll() = 0;
//	virtual void RefreshInputs() = 0;
//	virtual bool GetInputState(unsigned nIndex) = 0;
//	virtual uint16_t GetInputAllStates() = 0;
//	virtual void SetKeysChangedCallback(KeysChangedCallback cb) = 0;
public: // IDevicePCA9532
	virtual const IDigitalIn* AcquireInputStateInterface(unsigned nIndex) = 0;
	virtual void ReleaseInputStateInterface(unsigned nIndex) = 0;
	virtual IDigitalOut* AcquireLedOutputInterface(unsigned nIndex) = 0;
	virtual void ReleaseLedOutputInterface(unsigned nIndex) = 0;
};

IDevicePCA9532* GetPCA9532LedsBtnsInterface();
IDevicePCA9532* GetPCA9532AUXInterface();

#endif //DEV_PCA9532_H_INCLUDED
