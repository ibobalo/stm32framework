#include "stdafx.h"
#include "dev_pca9532.h"
#include "hw/debug.h"
#include "hw/mcu/hw_i2c.h"
#include "hw/tick_timeout.h"
#include "util/macros.h"
#include "tick_process.h"

#define INITIAL_DELAY             10
#define REPEAT_AFTER_ERROR_DELAY  5

//#define PCA9532_DEBUG_INFO(...) DEBUG_INFO("PCA9532:" __VA_ARGS__)
#define PCA9532_DEBUG_INFO(...) {}


class CI2CDevicePCA9532 :
		public IDevicePCA9532,
		public IDeviceI2C,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
	typedef IDevicePCA9532::time_delta_t time_delta_t;
public:
//	virtual ~CI2CDevicePCA9532();

public: // IDevicePCA9532
	virtual void Init(II2C* pI2CMaster, unsigned uiAddress, time_delta_t tdKeyRefreshPeriod, time_delta_t tdAllRefreshPeriod);

public: // IDevicePCA9532
	virtual const IDigitalIn* AcquireInputStateInterface(unsigned nIndex);
	virtual void ReleaseInputStateInterface(unsigned nIndex);
	virtual IDigitalOut* AcquireLedOutputInterface(unsigned nIndex);
	virtual void ReleaseLedOutputInterface(unsigned nIndex);

public: // IDevicePCA9532 - LEDs interface
	virtual void ClearLed(unsigned nIndex);
	virtual void SetLed(unsigned nIndex);
	virtual void ToggleLed(unsigned nIndex);
	virtual void BlinkLed(unsigned nIndex);
	virtual void FastBlinkLed(unsigned nIndex);

public: // IDevicePCA9532 - Inputsq interface
//	virtual void RefreshAll();
//	virtual void RefreshInputs();
	virtual bool GetInputState(unsigned nIndex) const;
//	virtual uint16_t GetInputAllStates();
//	virtual void SetKeysChangedCallback(KeysChangedCallback cb);

public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	);
	virtual void OnI2CDone();
	virtual void OnI2CError(ErrorType e);
	virtual void OnI2CIdle();

private:
	II2C*  m_pI2CMaster;
	class CDigitalInOutProxy :
			virtual public IDigitalIn,
			virtual public IDigitalOut
	{
	public:
		void Init(CI2CDevicePCA9532* pHost, unsigned nIndex) {m_pHost = pHost; m_nIndex = nIndex;}
	public: // IDigitalIn::IValueSource<bool>
	public: // IDigitalOut::IValueSource<bool>
		virtual bool GetValue() const;
	public: // IDigitalOut::IValueReceiver<bool>
		virtual void SetValue(bool bValue);
	public: // IDigitalOut
		virtual void Set();
		virtual void Reset();
		virtual void Toggle();
	private:
		CI2CDevicePCA9532* m_pHost;
		unsigned           m_nIndex;
	};

private:
	typedef union {
		struct {
//			uint8_t rw         :1;
			uint8_t a          :3;
			uint8_t fixed_part :4;
		};
		uint8_t raw;

		void Init(unsigned uiAddress) {a = uiAddress; fixed_part = 0xC;};
	} slave_addr_t;

	typedef union {
		typedef enum { REG_INPUT0, REG_INPUT1, REG_PSC0, REG_PWM0, REG_PSC1, REG_PWM1, REG_LS0, REG_LS1, REG_LS2, REG_LS3 } EREGADDR;
		struct {

			uint8_t reg_addr :4;
			uint8_t incr     :1;
			uint8_t reserved :3;
		};
		uint8_t raw;

		void Init(EREGADDR _reg_addr, int _incr) {reg_addr = _reg_addr; incr = _incr; reserved = 0;};
	} ctrl_reg_t;

	typedef union _input_reg {
		/* INPUT0 - Input register 0
			The INPUT0 register reflects the state of the device pins (inputs 0 to 7). Writes to this
			register will be acknowledged but will have no effect */
		struct  {
			uint8_t inputs[2]; // led:1 [16];
		};
		uint16_t raw;

		void Init() {raw = 0xFFFF;};
	} input_reg_t;
	typedef struct _pwm_config_t {
		uint8_t     psc_reg; /* PCS0 - Frequency Prescaler 0
			PSC0 is used to program the period of the PWM output.
			The period of BLINK0 = (PSC0 + 1) / 152. */
		uint8_t     pwm_reg; /* PWM0 - Pulse Width Modulation 0
			The PWM0 register determines the duty cycle of BLINK0. The outputs are LOW (LED on)
			when the count is less than the value in PWM0 and HIGH (LED off) when it is greater. If
			PWM0 is programmed with 00h, then the PWM0 output is always HIGH (LED off).
			The duty cycle of BLINK0 = PWM0 / 256. */

		void Init(int _psc, int _pwm) {psc_reg = _psc; pwm_reg = _pwm;};
	} pwm_config_t;
	typedef struct _LS_regs { /* LS0 to LS3 - LED selector registers
		The LSn LED selector registers determine the source of the LED data.
			00 = output is set high-impedance (LED off; default)
			01 = output is set LOW (LED on)
			10 = output blinks at PWM0 rate
			11 = output blinks at PWM1 rate */
		typedef enum { LS_INPUT, LS_ON, LS_PWM0, LS_PWM1 } ELEDSELECTOR;

		//ELEDSELECTOR  led:2 [16];
		uint8_t  uiLedSelectors[4];

		bool SetLedState(unsigned nIndex, ELEDSELECTOR eState) {
			if (nIndex < 16) {
				unsigned  i = (nIndex & 0x0F) >> 2;
				uint8_t   j = (nIndex & 0x03) << 1;
				uint8_t&  x = uiLedSelectors[i];
				uint8_t   y = x;
				y &= ~(0x03 << j);
				y |= (eState << j);
				if (y != x) {
					x = y;
					return true;
				}
			}
			return false;
		};
		void ToggleLedState(unsigned nIndex) {
			if (nIndex < 16) {
				unsigned  i = (nIndex & 0x0F) >> 2;
				uint8_t   j = (nIndex & 0x03) << 1;
				uint8_t&  x = uiLedSelectors[i];
				uint8_t m = (0x03 << j);
				if ( (x & m) != 0) {
					x &= ~m;
					x |= (LS_ON << j);
				} else {
					x &= ~m;
				}
			}
		};
		void Init(ELEDSELECTOR eState) { for (unsigned i=0; i < 16; ++i) {SetLedState(i, eState);}};
	} LS_regs_t;

private:
	void SetLedState(unsigned nIndex, LS_regs_t::ELEDSELECTOR eState);
	void ToggleLedState(unsigned nIndex);

private:
	slave_addr_t m_tAddress;
	// slave_addr_t  m_slave_addr;
	struct _read_pkt {
		ctrl_reg_t    m_ctrl_reg;
		struct {
			input_reg_t   m_input_reg;
		} payload;

		void Init() { m_ctrl_reg.Init(ctrl_reg_t::REG_INPUT0, 1); payload.m_input_reg.Init();};
	} m_tReadPkt;
	CChainedConstChunks m_cccReadPktSentChunk;
	CChainedIoChunks    m_cicReadPktRecvChunk;
	struct _init_pkt {
		ctrl_reg_t    m_ctrl_reg;
		struct {
			pwm_config_t  m_pwm_config0;
			pwm_config_t  m_pwm_config1;
		} payload;

		void Init() { m_ctrl_reg.Init(ctrl_reg_t::REG_PSC0, 1);
				payload.m_pwm_config0.Init(151, 128);  // blink 1 Hz 50% PWM
				payload.m_pwm_config1.Init(15,  128);  // blink 10 Hz 25% PWM
		};
	} m_tInitPkt;
	CChainedConstChunks m_cccInitPktChunk;
	struct _write_pkt {
		ctrl_reg_t    m_ctrl_reg;
		struct {
			LS_regs_t     m_LS_regs;
		} payload;

		void Init() { m_ctrl_reg.Init(ctrl_reg_t::REG_LS0, 1);
				payload.m_LS_regs.Init(LS_regs_t::LS_INPUT);
		};
	} m_tWritePkt;
	CChainedConstChunks m_cccWritePktChunk;
	enum {
		OpIsInit,
		OpIsWrite,
		OpIsRead,
		OpIsNone,
	}         m_eCurrentOp;
	bool      m_bInitPktChanged;
	bool      m_bWritePktChanged;
	bool      m_bInputReadRequired;
	bool      m_bCommOk;
	unsigned  m_uiLastInputReadTime;
	input_reg_t m_tActualInputs;
	CTimeout  m_toRefreshKeysTimeout;
	CTimeout  m_toRefreshAllTimeout;
	CTimeout  m_toCommunicationTimeout;
	KeysChangedCallback m_cbKeysChangedCallback;

	CDigitalInOutProxy m_aDigitalInOutProxies[16];
};


void CI2CDevicePCA9532::Init(II2C* pI2CMaster, unsigned uiAddress, CTimeDelta tdKeyRefreshPeriod, CTimeDelta tdAllRefreshPeriod)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::Init(m_pI2CMaster, uiAddress, tdKeyRefreshPeriod, tdAllRefreshPeriod));

	CParentProcess::Init(&g_TickProcessScheduller);

	m_pI2CMaster = pI2CMaster;
	m_tAddress.Init(uiAddress);
	m_tReadPkt.Init();
	m_cccReadPktSentChunk.Assign((const uint8_t*)&m_tReadPkt.m_ctrl_reg.raw, sizeof(m_tReadPkt.m_ctrl_reg.raw));
	m_cicReadPktRecvChunk.Assign((uint8_t*)&m_tReadPkt.payload, sizeof(m_tReadPkt.payload));
	m_tInitPkt.Init();
	m_cccInitPktChunk.Assign((const uint8_t*)&m_tInitPkt, sizeof(m_tInitPkt));
	m_tWritePkt.Init();
	m_cccWritePktChunk.Assign((const uint8_t*)&m_tWritePkt, sizeof(m_tWritePkt));
	m_tActualInputs.Init();
	m_eCurrentOp = OpIsInit;
	m_bInitPktChanged = true;
	m_bWritePktChanged = true;
	m_bInputReadRequired = true;
	m_bCommOk = false;
	m_uiLastInputReadTime = 0;
	m_toRefreshKeysTimeout.Init(tdKeyRefreshPeriod);
	m_toRefreshAllTimeout.Init(tdAllRefreshPeriod);
	m_toCommunicationTimeout.Init(tdKeyRefreshPeriod * 10);
	m_cbKeysChangedCallback = NullCallback();
	for (unsigned n = 0; n < ARRAY_SIZE(m_aDigitalInOutProxies); ++n) {
		m_aDigitalInOutProxies[n].Init(this, n);
	}

	if (m_toRefreshKeysTimeout.GetIsDefined()) {
		m_toRefreshKeysTimeout.Start();
		ContinueDelay(m_toRefreshKeysTimeout.GetTimeLeft() + 1);
	} else if (m_toRefreshAllTimeout.GetIsDefined()) {
		m_toRefreshAllTimeout.Start();
		ContinueDelay(m_toRefreshAllTimeout.GetTimeLeft() + 1);
	}

	PCA9532_DEBUG_INFO("First Init");
	m_pI2CMaster->Queue_I2C_Session(this);
}

/*virtual*/
const IDigitalIn* CI2CDevicePCA9532::AcquireInputStateInterface(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::AcquireInputStateInterface(nIndex));
	ASSERTE(nIndex < ARRAY_SIZE(m_aDigitalInOutProxies));
	return &m_aDigitalInOutProxies[nIndex];
}

/*virtual*/
void CI2CDevicePCA9532::ReleaseInputStateInterface(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::ReleaseInputStateInterface(nIndex));
	ASSERTE(nIndex < ARRAY_SIZE(m_aDigitalInOutProxies));
}

/*virtual*/
IDigitalOut* CI2CDevicePCA9532::AcquireLedOutputInterface(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::AcquireLedOutputInterface(nIndex));
	ASSERTE(nIndex < ARRAY_SIZE(m_aDigitalInOutProxies));
	return &m_aDigitalInOutProxies[nIndex];
}

/*virtual*/
void CI2CDevicePCA9532::ReleaseLedOutputInterface(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::ReleaseLedOutputInterface(nIndex));
	ASSERTE(nIndex < ARRAY_SIZE(m_aDigitalInOutProxies));
}

/*virtual*/
void CI2CDevicePCA9532::ClearLed(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::ClearLed(nIndex));
	SetLedState(nIndex, LS_regs_t::LS_INPUT);
}
/*virtual*/
void CI2CDevicePCA9532::SetLed(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::SetLed(nIndex));
	SetLedState(nIndex, LS_regs_t::LS_ON);
}
/*virtual*/
void CI2CDevicePCA9532::ToggleLed(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::SetLed(nIndex));
	ToggleLedState(nIndex);
}
/*virtual*/
void CI2CDevicePCA9532::BlinkLed(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::BlinkLed(nIndex));
	SetLedState(nIndex, LS_regs_t::LS_PWM0);
}
/*virtual*/
void CI2CDevicePCA9532::FastBlinkLed(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::FastBlinkLed(nIndex));
	SetLedState(nIndex, LS_regs_t::LS_PWM1);
}
//void CI2CDevicePCA9532::RefreshAll()
//{
//	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::RefreshAll());
//
//	m_bInputReadRequired = m_bInitPktChanged = m_bWritePktChanged = true;
//	ContinueNow();
//}
//void CI2CDevicePCA9532::RefreshInputs()
//{
//	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::RefreshInputs());
//
//	m_bInputReadRequired = true;
//	ContinueNow();
//}
bool CI2CDevicePCA9532::GetInputState(unsigned nIndex) const
{
//	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::GetInputState(nIndex));
	return m_tActualInputs.inputs[(nIndex & 0x08) >> 3] & (1 << (nIndex & 0x07));
}

//uint16_t CI2CDevicePCA9532::GetInputAllStates() {
//	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::GetInputAllStates());
//	return m_tActualInputs.inputs[0] | (m_tActualInputs.inputs[1] << 8);
//}
//
//void CI2CDevicePCA9532::SetKeysChangedCallback(KeysChangedCallback cb)
//{
//	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::SetKeysChangedCallback(cb));
//	m_cbKeysChangedCallback = cb;
//}

void CI2CDevicePCA9532::SingleStep(CTimeType ttTime )
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));

	PROCESS_DEBUG_INFO(">>> Process: CI2CDevicePCA9532");

	if (m_bCommOk) {
		if (m_toCommunicationTimeout.GetIsElapsed()) {
			m_bCommOk = false;
			DEBUG_INFO("PCA9532: timeout, comm lost. Addr:", m_tAddress.raw);
			m_pI2CMaster->Cancel_I2C_Session(this);
			m_eCurrentOp = OpIsNone;
			m_bInitPktChanged = true;
			m_bWritePktChanged = true;
			m_bInputReadRequired = true;
			if (m_tActualInputs.raw) {
				m_tActualInputs.raw = 0xFFFF;
				if (m_cbKeysChangedCallback) m_cbKeysChangedCallback(0);
			}
		} else if (m_toRefreshKeysTimeout.GetIsElapsed()) {
			m_bInputReadRequired = true;
		} else if (m_toRefreshAllTimeout.GetIsElapsed()) {
			m_bInitPktChanged = true;
			m_bWritePktChanged = true;
			m_bInputReadRequired = true;
			m_toRefreshAllTimeout.Start();
		}
	}

	if (m_eCurrentOp == OpIsNone) {
		if (m_bInitPktChanged) {
			PCA9532_DEBUG_INFO("Init");
			m_eCurrentOp = OpIsInit;
			m_pI2CMaster->Queue_I2C_Session(this);
		} else if (m_bWritePktChanged) {
			PCA9532_DEBUG_INFO("Write");
			m_eCurrentOp = OpIsWrite;
			m_pI2CMaster->Queue_I2C_Session(this);
		} else if (m_bInputReadRequired) {
			PCA9532_DEBUG_INFO("Read");
			m_eCurrentOp = OpIsRead;
			m_pI2CMaster->Queue_I2C_Session(this);
		} else if (m_bCommOk && m_toRefreshKeysTimeout.GetIsDefined()) {
			ContinueDelay(m_toRefreshKeysTimeout.GetTimeLeft() + 1);
		} else if (m_toRefreshAllTimeout.GetIsDefined()) {
			ContinueDelay(m_toRefreshAllTimeout.GetTimeLeft() + 1);
		} else {
			ContinueDelay(10000);
		}
	}

	PROCESS_DEBUG_INFO("<<< Process: CI2CDevicePCA9532");
}

unsigned CI2CDevicePCA9532::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CAddress());

	return m_tAddress.raw;
}

bool CI2CDevicePCA9532::GetI2CPacket(
		const CChainedConstChunks** ppRetChunksToSend,
		const CChainedIoChunks**    ppRetChunksToRecv
) {
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::GetI2CPacket(ppRetChunksToSend, ppRetChunksToRecv));
	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		return false;
		break;
	case OpIsInit:
		ASSERTE(m_bInitPktChanged);
		m_bInitPktChanged = false;
		*ppRetChunksToSend = &m_cccInitPktChunk;
		*ppRetChunksToRecv = NULL;
		break;
	case OpIsWrite:
		ASSERTE(m_bWritePktChanged);
		m_bWritePktChanged = false;
		*ppRetChunksToSend = &m_cccWritePktChunk;
		*ppRetChunksToRecv = NULL;
		break;
	case OpIsRead:
		ASSERTE(m_bInputReadRequired);
		m_bInputReadRequired = false;
		*ppRetChunksToSend = &m_cccReadPktSentChunk;
		*ppRetChunksToRecv = &m_cicReadPktRecvChunk;
		m_toRefreshKeysTimeout.Start();
		break;
	}
	return true;
}

void CI2CDevicePCA9532::OnI2CDone()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CDone());
	PCA9532_DEBUG_INFO("I2C done. State:", m_eCurrentOp);
	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit:
		m_eCurrentOp = OpIsNone;
		break;
	case OpIsWrite:
		m_eCurrentOp = OpIsNone;
		break;
	case OpIsRead:
		m_eCurrentOp = OpIsNone;
		if (!m_bCommOk) {
			m_bCommOk = true;
			DEBUG_INFO("PCA9532 comm restores. Addr:", m_tAddress.raw);
		}
		if (m_tActualInputs.raw != m_tReadPkt.payload.m_input_reg.raw) {
			m_tActualInputs.raw = m_tReadPkt.payload.m_input_reg.raw;
			if (m_cbKeysChangedCallback) m_cbKeysChangedCallback(m_tReadPkt.payload.m_input_reg.raw);
		}
		m_toCommunicationTimeout.Start();
	}
	if (m_bInputReadRequired || m_bInitPktChanged || m_bWritePktChanged) {
		ContinueNow();
	} else if (m_toRefreshKeysTimeout.GetIsDefined()) {
		ContinueDelay(m_toRefreshKeysTimeout.GetTimeLeft() + 1);
	} else if (m_toRefreshAllTimeout.GetIsDefined()) {
		ContinueDelay(m_toRefreshAllTimeout.GetTimeLeft() + 1);
	}
}

void CI2CDevicePCA9532::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CError(e));

	DEBUG_INFO("PCA9532 error:", e);
	PCA9532_DEBUG_INFO("I2C error. State:", m_eCurrentOp);

	switch (m_eCurrentOp) {
	case OpIsNone:
		ASSERT("invalid op");
		break;
	case OpIsInit:
		m_bInitPktChanged = true;
		break;
	case OpIsWrite:
		m_bWritePktChanged = true;
		break;
	case OpIsRead:
		m_bInputReadRequired = true;
		break;
	}
	m_eCurrentOp = OpIsNone;

	ContinueDelay(REPEAT_AFTER_ERROR_DELAY);
}

void CI2CDevicePCA9532::OnI2CIdle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceI2C::OnI2CIdle());
	ContinueNow();
}

void CI2CDevicePCA9532::SetLedState(unsigned nIndex, LS_regs_t::ELEDSELECTOR eState)
{
	if (m_tWritePkt.payload.m_LS_regs.SetLedState(nIndex, eState)) {
		m_bWritePktChanged = true;
		ContinueNow();
	}
}
void CI2CDevicePCA9532::ToggleLedState(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(IDevicePCA9532::SetLed(nIndex));
	m_tWritePkt.payload.m_LS_regs.ToggleLedState(nIndex);
	m_bWritePktChanged = true;
	ContinueNow();
}

/*virtual*/
bool CI2CDevicePCA9532::CDigitalInOutProxy::GetValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalIn::IValueSource<bool>::GetValue());
	IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IValueSource<bool>::GetValue());

	return m_pHost->GetInputState(m_nIndex);
}

/*virtual*/
void CI2CDevicePCA9532::CDigitalInOutProxy::SetValue(bool bValue)
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IValueReceiver<bool>::SetValue(bValue));

	m_pHost->SetLedState(m_nIndex, bValue ? LS_regs_t::ELEDSELECTOR::LS_ON : LS_regs_t::ELEDSELECTOR::LS_INPUT);
}

/*virtual*/
void CI2CDevicePCA9532::CDigitalInOutProxy::Set()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IDigitalOut::Set());

	m_pHost->SetLed(m_nIndex);
}

/*virtual*/
void CI2CDevicePCA9532::CDigitalInOutProxy::Reset()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IDigitalOut::Reset());

	m_pHost->ClearLed(m_nIndex);
}

/*virtual*/
void CI2CDevicePCA9532::CDigitalInOutProxy::Toggle()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IDigitalOut::Toggle());

	m_pHost->SetLedState(m_nIndex, m_pHost->GetInputState(m_nIndex) ? LS_regs_t::ELEDSELECTOR::LS_ON : LS_regs_t::ELEDSELECTOR::LS_INPUT);
}


CI2CDevicePCA9532       PCA9532LedsBtns;
CI2CDevicePCA9532       PCA9532AUX;

IDevicePCA9532* GetPCA9532LedsBtnsInterface() {return &PCA9532LedsBtns;}
IDevicePCA9532* GetPCA9532AUXInterface() {return &PCA9532AUX;}
