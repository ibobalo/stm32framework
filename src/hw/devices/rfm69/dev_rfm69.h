#ifndef DEV_RFM69_H_INCLUDED
#define DEV_RFM69_H_INCLUDED

#include "hw/mcu/hw_spi.h"
#include "interfaces/notify.h"
#include "tick_process.h"
#include <stdint.h>

class CSPIDeviceRFM69 :
		public IDeviceSPI,
		public CTickProcess
{
public:
	typedef CTickProcess CParent;
public:
//	virtual ~CSPIDeviceRFM69();

	void Init(ISPI* pSPI, IDigitalOut* pSelectDO, IDigitalOut* pResetDO, IDigitalIn* pInteruptDI);
	virtual void SingleStep(CTimeType uiTime);

public: // IDeviceSPI
	virtual bool GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const;
	virtual void Select();
	virtual void Release();
	virtual void OnSPIDone();
	virtual void OnSPIError();
	virtual void OnSPIIdle();

private:
	void ProcessFSM_Init();

	void ContinueOnSPIIdle();

	typedef enum {STATE_IDLE, STATE_INIT, STATE_TX, STATE_RX} FSM_STATE;
	typedef enum {
		INIT_Sync1_Write, INIT_Sync1_Read, INIT_Sync1_Check,
		INIT_Sync2_Write, INIT_Sync2_Read, INIT_Sync2_Check,
		INIT_Config_Write,
		INIT_Error
	} INIT_FSM_STATE;
	ISPI*              m_pSPI;
	IDigitalOut*       m_pSelectDO;
	IDigitalOut*       m_pResetDO;
	IDigitalIn*        m_pInterruptDI;
	FSM_STATE   m_eState;
	INIT_FSM_STATE m_eInit;
	unsigned m_nTries;

	bool     m_bBusy;
	uint8_t  m_TxBuf[16];
	uint8_t  m_uiTxSize;

	uint8_t  m_RxBuf[16];
	uint8_t  m_uiRxSize;
	bool     m_bError;

	CChainedConstChunks m_pTxChunks[2];
	CChainedIoChunks    m_pRxChunks[2];

private:
	typedef enum {
		RF69_MODE_SLEEP      = 0, // XTAL OFF
		RF69_MODE_STANDBY    = 1, // XTAL ON
		RF69_MODE_SYNTH	     = 2, // PLL ON
		RF69_MODE_RX         = 3, // RX MODE
		RF69_MODE_TX		 = 4, // TX MODE
	} RF69_MODE;
	RF69_MODE  m_eRF69Mode;
	bool       m_bPromiscuousMode;
	unsigned   m_uiPowerLevel;
};

CSPIDeviceRFM69* AcquireRFM69(ISPI* pSPI, IDigitalOut* pSelectDO, IDigitalOut* pResetDO = NULL, IDigitalIn* pInteruptDI = NULL);

#endif // DEV_RFM69_H_INCLUDED
