#ifndef DEV_DS18B20_H_INCLUDED
#define DEV_DS18B20_H_INCLUDED

#include <stdint.h>
#include "interfaces/1wire.h"

class IDeviceDS18B20:
		virtual public IDevice1WROM
{
public:
	using IDevice1WROM::TDoneNotification;
	typedef Callback<void (bool, int16_t)> TReadNotification;

public: // IDeviceDS18B20
	virtual bool StartConvertion(TDoneNotification cbDone) = 0;
	virtual bool Read(TReadNotification cbRead) = 0;
	virtual bool StartAndRead(TReadNotification cbRead) = 0;
};

IDeviceDS18B20* AcquireDeviceDS18B20(IDevice1WMaster* p1W);

#endif /* DEV_DS18B20_H_INCLUDED */
