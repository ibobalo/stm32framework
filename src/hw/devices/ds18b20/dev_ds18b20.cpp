#include "stdafx.h"
#include "dev_ds18b20.h"
#include "devices/1wire_rom/1wire_rom.h"
#include "tick_process.h"
#include "hw/debug.h"
#include "util/atomic.h"
#include "util/endians.h"
#include "util/crc.h"


//#define DS18B20_DEBUG_INFO(...) DEBUG_INFO("18B20:" __VA_ARGS__)
#define DS18B20_DEBUG_INFO(...) {}

class CDeviceDS18B20:
		public CDevice1WROM,
		public IDeviceDS18B20
{
public:
	using IDeviceDS18B20::TDoneNotification;
	using IDeviceDS18B20::TReadNotification;
public:
	void Init(IDevice1WMaster* p1W);

public: // IDeviceDS18B20
	virtual bool StartConvertion(TDoneNotification cbDone);
	virtual bool Read(TReadNotification cbRead);
	virtual bool StartAndRead(TReadNotification cbRead);

private:
	void OnStartSelectDone(bool bSuccess);
	void OnStartConvertDone(bool bSuccess);
	void OnReadSelectDone(bool bSuccess);
	void OnReadScratchpadDone(bool bSuccess);
	void OnReadTemperatureDone(bool bSuccess);
	void NotifyDone();
	void NotifyReadDone(int16_t q4Temperature);
	void NotifyError();
private:
	enum {
		OpStartSelect,
		OpStartConvert,
		OpReadSelect,
		OpReadScratchpad,
		OpReadTemperature,

		OpNone,
	}         m_eCurrentOp;
	typedef enum {
		CONVERT_T        = 0x44,
		COPY_SCRATCHPAD  = 0x48,
		READ_SCRATCHPAD  = 0xBE,
		WRITE_SCRATCHPAD = 0x4E,
		TH_REGISTER      = 0x4B,
		TL_REGISTER      = 0x46,
	} DS18B20_CMD;

	TDoneNotification m_cbDone;
	TReadNotification m_cbReadDone;
	uint8_t m_auiReadBuffer[16];
	int16_t m_q4Temperature;
};

void CDeviceDS18B20::Init(IDevice1WMaster* p1W)
{
	CDevice1WROM::Init(p1W);
	m_eCurrentOp = OpNone;
}

/*virtual*/
bool CDeviceDS18B20::StartConvertion(TDoneNotification cbDone)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS18B20::StartConvertion(cbDone));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpStartSelect)) {
		DS18B20_DEBUG_INFO("CMD: Start busy");
		return false;
	}
	DS18B20_DEBUG_INFO("CMD: Start");
	ASSERTE(!m_cbDone && !m_cbReadDone);
	m_cbDone = cbDone;
	if (!Select(BIND_MEM_CB(&CDeviceDS18B20::OnStartSelectDone, this))) {
		DS18B20_DEBUG_INFO("StartReset 1W busy");
		m_eCurrentOp = OpNone;
		m_cbDone = NullCallback();
		return false;
	}
	return true;
}

/*virtual*/
bool CDeviceDS18B20::Read(TReadNotification cbRead)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS18B20::Read(cbRead));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpReadSelect)) {
		DS18B20_DEBUG_INFO("CMD: Read busy");
		return false;
	}
	DS18B20_DEBUG_INFO("CMD: Read");
	ASSERTE(!m_cbDone && !m_cbReadDone);
	m_cbReadDone = cbRead;
	if (!Select(BIND_MEM_CB(&CDeviceDS18B20::OnReadSelectDone, this))) {
		DS18B20_DEBUG_INFO("ReadReset 1W busy");
		m_eCurrentOp = OpNone;
		m_cbReadDone = NullCallback();
		return false;
	}
	return true;
}

/*virtual*/
bool CDeviceDS18B20::StartAndRead(TReadNotification cbRead)
{
	IMPLEMENTS_INTERFACE_METHOD(IDeviceDS18B20::StartAndRead(cbRead));
	if (!AtomicCmpSet(&m_eCurrentOp, OpNone, OpStartSelect)) {
		DS18B20_DEBUG_INFO("CMD: StartAndReadRead busy");
		return false;
	}
	DS18B20_DEBUG_INFO("CMD: StartAndRead");
	ASSERTE(!m_cbDone && !m_cbReadDone);
	m_cbReadDone = cbRead;
	if (!Select(BIND_MEM_CB(&CDeviceDS18B20::OnStartSelectDone, this))) {
		DS18B20_DEBUG_INFO("StartSelect 1W busy");
		m_eCurrentOp = OpNone;
		m_cbReadDone = NullCallback();
		return false;
	}
	return true;
}

void CDeviceDS18B20::OnStartSelectDone(bool bSuccess)
{
	DS18B20_DEBUG_INFO("StartSelect done:", bSuccess?1:0);
	if (bSuccess) {
		m_eCurrentOp = OpStartConvert;
		if (!m_p1W->TxBytePU(DS18B20_CMD::CONVERT_T, 750, BIND_MEM_CB(&CDeviceDS18B20::OnStartConvertDone, this))) {
			DS18B20_DEBUG_INFO("StartConvert 1W busy");
			m_eCurrentOp = OpNone;
			NotifyError();
		}
	} else {
		m_eCurrentOp = OpNone;
		NotifyError();
	}
}

void CDeviceDS18B20::OnStartConvertDone(bool bSuccess)
{
	DS18B20_DEBUG_INFO("StartConvert done:", bSuccess?1:0);
	if (bSuccess)
		if (m_cbReadDone) {
			m_eCurrentOp = OpReadSelect;
			if (!Select(BIND_MEM_CB(&CDeviceDS18B20::OnReadSelectDone, this))) {
				DS18B20_DEBUG_INFO("ReadSelect 1W busy");
				m_eCurrentOp = OpNone;
				NotifyError();
			}
		} else {
			m_eCurrentOp = OpNone;
			NotifyDone();
	} else {
		m_eCurrentOp = OpNone;
		NotifyError();
	}
}

void CDeviceDS18B20::OnReadSelectDone(bool bSuccess)
{
	DS18B20_DEBUG_INFO("ReadSkip done:", bSuccess?1:0);
	if (bSuccess) {
		m_eCurrentOp = OpReadScratchpad;
		if (!m_p1W->TxByte(DS18B20_CMD::READ_SCRATCHPAD, BIND_MEM_CB(&CDeviceDS18B20::OnReadScratchpadDone, this))) {
			DS18B20_DEBUG_INFO("ReadScratch 1W busy");
			m_eCurrentOp = OpNone;
			NotifyError();
		}
	} else {
		m_eCurrentOp = OpNone;
		NotifyError();
	}
}

void CDeviceDS18B20::OnReadScratchpadDone(bool bSuccess)
{
	DS18B20_DEBUG_INFO("ReadScratch done:", bSuccess?1:0);
	if (bSuccess) {
		m_eCurrentOp = OpReadTemperature;
		if (!m_p1W->RxBytes(m_auiReadBuffer, 9, BIND_MEM_CB(&CDeviceDS18B20::OnReadTemperatureDone, this))) {
			DS18B20_DEBUG_INFO("ReadTemperature 1W busy");
			m_eCurrentOp = OpNone;
			NotifyError();
		}
	} else {
		m_eCurrentOp = OpNone;
		NotifyError();
	}
}

void CDeviceDS18B20::OnReadTemperatureDone(bool bSuccess)
{
	DS18B20_DEBUG_INFO("ReadTemperature done");
	m_eCurrentOp = OpNone;
	if (bSuccess && CRC8_DALLAS::Calculate(m_auiReadBuffer, 8) == m_auiReadBuffer[8]) {
		m_q4Temperature = letohs(*(uint16_t*)m_auiReadBuffer);
		if ((m_uiSelectedID & 0xFF) == 0x10) {
			m_q4Temperature *= 8;
		}
		NotifyReadDone(m_q4Temperature);
	} else {
		if (bSuccess) {
			DEBUG_INFO("crc calculated:", CRC8_DALLAS::Calculate(m_auiReadBuffer, 8));
			DEBUG_INFO("crc received  :", m_auiReadBuffer[8]);
		}
		NotifyError();
	}
}

void CDeviceDS18B20::NotifyDone()
{
	DS18B20_DEBUG_INFO("DONE");
	TDoneNotification cbDone = m_cbDone;
	m_cbDone = NullCallback();
	if (cbDone) cbDone(true);
}

void CDeviceDS18B20::NotifyReadDone(int16_t q4Temperature)
{
	DS18B20_DEBUG_INFO("READDONE:", q4Temperature);
	TReadNotification cbReadDone = m_cbReadDone;
	m_cbReadDone = NullCallback();
	if (cbReadDone) cbReadDone(true, q4Temperature);
}

void CDeviceDS18B20::NotifyError()
{
	DS18B20_DEBUG_INFO("ERROR:");
	TDoneNotification cbDone = m_cbDone;
	m_cbDone = NullCallback();
	TReadNotification cbReadDone = m_cbReadDone;
	m_cbReadDone = NullCallback();
	if (cbDone) cbDone(false);
	if (cbReadDone) cbReadDone(false, 0);
}

CDeviceDS18B20 g_DS18B20;

IDeviceDS18B20* AcquireDeviceDS18B20(IDevice1WMaster* p1W)
{
	g_DS18B20.Init(p1W);
	return &g_DS18B20;
}
