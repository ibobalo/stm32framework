#ifndef HW_MACROS_H_INCLUDED
#define HW_MACROS_H_INCLUDED

#include "util/macros.h"
#include "hw/debug.h"
#include "hw/mcu/mcu.h"
#include "stm32f_irqs.h"

#define DEFINE_ISR_EMPTY(handler_name) extern "C" void handler_name(void) __attribute__((weak, nothrow, alias("Empty_Handler")));
#define IMPLEMENT_ISR(handler_name) extern "C" void CC(handler_name,_fn)(void); extern "C" void handler_name##Handler(void) { \
			DEBUG_IRQ_BEGIN(#handler_name); CC(handler_name,_fn)(); DEBUG_IRQ_END(#handler_name); } \
		extern "C" void CC(handler_name,_fn)(void)
#define REDIRECT_ISR(handler_name, fn) extern "C" void handler_name(void) { \
		DEBUG_IRQ_BEGIN(#handler_name); fn(); DEBUG_IRQ_END(#handler_name); } \
		DEBUG_IRQ_REDIRECTED(handler_name);
#define REDIRECT_ISR_OBJ(handler_name, pObj, fnMethod) extern "C" void handler_name(void) { \
		DEBUG_IRQ_BEGIN(#handler_name); (pObj)->fnMethod(); DEBUG_IRQ_END(#handler_name); } \
		DEBUG_IRQ_REDIRECTED(handler_name);
#define REDIRECT_ISR_STATIC(handler_name, cls, fnMethod) extern "C" void handler_name(void) { \
		DEBUG_IRQ_BEGIN(#handler_name); cls::fnMethod(); DEBUG_IRQ_END(#handler_name); } \
		DEBUG_IRQ_REDIRECTED(handler_name);
#define REDIRECT_ISR_CALLBACK(handler_name, CLEAR_CODE, cb) extern "C" void handler_name(void) { \
		DEBUG_IRQ_BEGIN(#handler_name); CLEAR_CODE; if (cb) (cb)(); DEBUG_IRQ_END(#handler_name); } \
		DEBUG_IRQ_REDIRECTED(handler_name);
#define REDIRECT_ISRID(ID, fn) REDIRECT_ISR(CC(ID,Handler), fn) \
		DEBUG_IRQn_REDIRECTED(CC(ID,n));
#define REDIRECT_ISRID_STATIC(ID, cls, fnMethod) REDIRECT_ISR_STATIC(CC(ID,Handler), cls, fnMethod) \
		DEBUG_IRQn_REDIRECTED(CC(ID,n));
#define REDIRECT_ISRID_CALLBACK(ID, CLEAR_CODE, cb) REDIRECT_ISR_STATIC(CC(ID,Handler), CLEAR_CODE, cb) \
		DEBUG_IRQn_REDIRECTED(CC(ID,n));

#define ISR_SHARED_SUB_VARS(host_name, sub_name) \
		static bool CCCCC(ISR_SHARED_,host_name,_,sub_name,_NVIC_Inited) = false;
#define ISR_SHARED_HOST_VARS(host_name, ...) \
		MAP_ARG(ISR_SHARED_SUB_VARS, host_name, __VA_ARGS__)
#define ISR_SHARED_SUB_CALL(host_name, sub_name) \
		if (CCCCC(ISR_SHARED_,host_name,_,sub_name,_NVIC_Inited)) CC(sub_name,SubHandler)();
#define ISR_SHARED_HOST_CALLS(host_name, ...) \
		MAP_ARG(ISR_SHARED_SUB_CALL, host_name, __VA_ARGS__)
#define IMPLEMENT_ISR_SHARED_HOST(host_name) \
		ISR_SHARED_HOST_VARS(host_name, CC(ISR_SHARED_SUBS_,host_name)) \
		IMPLEMENT_ISR(host_name) { \
			ISR_SHARED_HOST_CALLS(host_name, CC(ISR_SHARED_SUBS_,host_name)) \
		}
#define	ISR_SHARED_IS_SUB_NVIC_INITED(host_name, sub_name) \
		|| CCCCC(ISR_SHARED_,host_name,_,sub_name,_NVIC_Inited)
#define	ISR_SHARED_IS_ANY_NVIC_INITED(host_name, ...) (false \
		MAP_ARG(ISR_SHARED_IS_SUB_NVIC_INITED, host_name, CC(ISR_SHARED_SUBS_,host_name)) \
)
#define	ISR_SHARED_NVIC_INIT(host_name, sub_name, PRIO, SUB_PRIO) { \
		bool bHostInited = ISR_SHARED_IS_ANY_NVIC_INITED(host_name, CC(ISR_SHARED_SUBS_,host_name)); \
		if (!bHostInited) NVIC_INIT(CC(host_name,n), PRIO, SUB_PRIO); \
		CCCCC(ISR_SHARED_,host_name,_,sub_name,_NVIC_Inited) = true; \
}
/*
#define INIT_EXTI(PORT, PIN, LINE, TRIGGER, GroupPrio, SubPrio) { \
		SYSCFG_EXTILineConfig(CC(EXTI_PortSourceGPIO,PORT), CC(EXTI_PinSource,PIN)); \
		EXTI_InitTypeDef EXTI_InitStructure; \
		EXTI_StructInit(&EXTI_InitStructure); \
		EXTI_InitStructure.EXTI_Line = CC(EXTI_Line,PIN); \
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt; \
		EXTI_InitStructure.EXTI_Trigger = CC(EXTI_Trigger_,TRIGGER); \
		EXTI_InitStructure.EXTI_LineCmd = ENABLE; \
		EXTI_Init(&EXTI_InitStructure); \
	    NVIC_INIT(CCCC(EX,TI,LINE,_IRQn), GroupPrio, SubPrio); \
}
*/

#define INIT_GPIO_IN(PORT, PIN)                     INIT_GPIO_PORT(PORT, PIN, 0, IN, PP, NOPULL, 2)
#define INIT_GPIO_IN_PU(PORT, PIN)                  INIT_GPIO_PORT(PORT, PIN, 0, IN, PP, UP, 2)
#define INIT_GPIO_IN_PU_FAST(PORT, PIN)             INIT_GPIO_PORT(PORT, PIN, 0, IN, PP, UP, 50)
#define INIT_GPIO_IN_ADC(ADC_CHANNEL, PORT, PIN)    INIT_GPIO_PORT(PORT, PIN, 0, AIN, PP, NOPULL, 2)
#define INIT_GPIO_OUT(PORT, PIN, STATE)             INIT_GPIO_PORT(PORT, PIN, STATE, OUT, PP, NOPULL, 2)
#define INIT_GPIO_OUT_FAST(PORT, PIN, STATE)        INIT_GPIO_PORT(PORT, PIN, STATE, OUT, PP, NOPULL, 50)
#define INIT_GPIO_OUT_OD(PORT, PIN, STATE)          INIT_GPIO_PORT(PORT, PIN, STATE, OUT, OD, NOPULL, 2)
#define INIT_GPIO_OUT_OD_PU(PORT, PIN, STATE)       INIT_GPIO_PORT(PORT, PIN, STATE, OUT, OD, UP, 2)
#define INIT_GPIO_OUT_OD_PU_FAST(PORT, PIN, STATE)  INIT_GPIO_PORT(PORT, PIN, STATE, OUT, OD, UP, 50)

#define INIT_GPIO_AF(PORT, PIN, AF_AF) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		GPIO_PinAFConfig(CC(GPIO,PORT), CC(GPIO_PinSource,PIN), CC(GPIO_,AF_AF)); \
		INIT_GPIO_PORT_PROPS(PORT, PIN, AF, PP, NOPULL, 2); \
}
#define INIT_GPIO_AF_FAST(PORT, PIN, AF_AF) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		GPIO_PinAFConfig(CC(GPIO,PORT), CC(GPIO_PinSource,PIN), CC(GPIO_,AF_AF)); \
		INIT_GPIO_PORT_PROPS(PORT, PIN, AF, PP, NOPULL, 50); \
}
#define INIT_GPIO_AF_OD(PORT, PIN, AF_AF) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		GPIO_PinAFConfig(CC(GPIO,PORT), CC(GPIO_PinSource,PIN), CC(GPIO_,AF_AF)); \
		INIT_GPIO_PORT_PROPS(PORT, PIN, AF, OD, NOPULL, 2); \
}
#define INIT_GPIO_AF_OD_FAST(PORT, PIN, AF_AF) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		GPIO_PinAFConfig(CC(GPIO,PORT), CC(GPIO_PinSource,PIN), CC(GPIO_,AF_AF)); \
		INIT_GPIO_PORT_PROPS(PORT, PIN, AF, OD, NOPULL, 50); \
}
#define INIT_GPIO_AF_PU_FAST(PORT, PIN, AF_AF) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		GPIO_PinAFConfig(CC(GPIO,PORT), CC(GPIO_PinSource,PIN), CC(GPIO_,AF_AF)); \
		INIT_GPIO_PORT_PROPS(PORT, PIN, AF, PP, UP, 50); \

#define INIT_GPIO_AF_OD_PU_FAST(PORT, PIN, AF_AF) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		GPIO_PinAFConfig(CC(GPIO,PORT), CC(GPIO_PinSource,PIN), CC(GPIO_,AF_AF)); \
		INIT_GPIO_PORT_PROPS(PORT, PIN, AF, OD, UP, 50); \
}
#define DEINIT_GPIO(PORT, PIN) { \
		GPIO_InitTypeDef GPIO_InitStructure; \
		GPIO_StructInit(&GPIO_InitStructure); \
		GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,PIN); \
		GPIO_Init(CC(GPIO,PORT), &GPIO_InitStructure); \
}
#define INIT_GPIO_PORT(PORT, PIN, STATE, MODE, OTYPE, PULL, SPEED) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		if (STATE) {GPIO_SetBits(CC(GPIO,PORT), CC(GPIO_Pin_,PIN));} \
		else {GPIO_ResetBits(CC(GPIO,PORT), CC(GPIO_Pin_,PIN));} \
		INIT_GPIO_PORT_PROPS(PORT, PIN, MODE, OTYPE, PULL, SPEED); \
}
#define INIT_GPIO_PORT_PROPS(PORT, PIN, MODE, OTYPE, PULL, SPEED) { \
		GPIO_InitTypeDef GPIO_InitStructure; \
		GPIO_StructInit(&GPIO_InitStructure); \
		GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,PIN); \
		GPIO_InitStructure.GPIO_Mode = CC(GPIO_Mode_,MODE); \
		GPIO_InitStructure.GPIO_OType = CC(GPIO_OType_,OTYPE); \
		GPIO_InitStructure.GPIO_Speed = CCC(GPIO_Speed_,SPEED,MHz); \
		GPIO_InitStructure.GPIO_PuPd = CC(GPIO_PuPd_,PULL); \
		GPIO_Init(CC(GPIO,PORT), &GPIO_InitStructure); \
}
#define READ_GPIO_PORT(PORT,PIN) ( GPIO_ReadInputDataBit(CC(GPIO,PORT), CC(GPIO_Pin_,PIN)) )

#define INIT_TIMER_PWM(APB, TIMER, PRESCALER, PERIOD, MODE) { \
		CCC(RCC_,APB,PeriphClockCmd)(CCCC(RCC_,APB,Periph_TIM,TIMER), ENABLE); \
		TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure; \
		TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure); \
		TIM_TimeBaseInitStructure.TIM_Prescaler = PRESCALER; \
		TIM_TimeBaseInitStructure.TIM_CounterMode = CC(TIM_CounterMode_,MODE); \
		TIM_TimeBaseInitStructure.TIM_Period = PERIOD; \
		TIM_TimeBaseInitStructure.TIM_ClockDivision = 0; \
		TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0; \
		TIM_TimeBaseInit(CC(TIM,TIMER), &TIM_TimeBaseInitStructure); \
		TIM_ARRPreloadConfig(CC(TIM,TIMER), ENABLE); \
}

#define INIT_GPIO_OUT_PWM_EX(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE, PIN_EXTRA) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,PORT), ENABLE)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,PORT), ENABLE)); \
		TIM_OCInitTypeDef TIM_OCInitStructure; \
		TIM_OCStructInit(&TIM_OCInitStructure); \
		TIM_OCInitStructure.TIM_Pulse = VALUE; \
		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; \
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; \
		TIM_OCInitStructure.TIM_OCPolarity = (INVERTED)?TIM_OCPolarity_Low:TIM_OCPolarity_High; \
		if (TIMER == 1 || TIMER == 8) { \
			TIM_OCInitStructure.TIM_OCIdleState = (INVERTED)?TIM_OCIdleState_Reset:TIM_OCIdleState_Set; \
			TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable; \
			TIM_OCInitStructure.TIM_OCNPolarity = (INVERTED)?TIM_OCNPolarity_Low:TIM_OCNPolarity_High; \
			TIM_OCInitStructure.TIM_OCNIdleState = (INVERTED)?TIM_OCNIdleState_Set:TIM_OCNIdleState_Reset; \
		} \
		CCC(TIM_OC,TIMER_CHANNEL,Init)(CC(TIM,TIMER), &TIM_OCInitStructure); \
		CC(INIT_GPIO_OUT_AF,PIN_EXTRA)(PORT, PIN, CC(AF_TIM,TIMER)); \
		CCC(TIM_OC,TIMER_CHANNEL,PreloadConfig)(CC(TIM,TIMER), TIM_OCPreload_Enable); \
}

#define INIT_GPIO_OUT_PWM(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE) \
		INIT_GPIO_OUT_PWM_EX(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE, /* */)
#define INIT_GPIO_OUT_PWM_OD(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE) \
		INIT_GPIO_OUT_PWM_EX(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE, _OD)
#define INIT_GPIO_OUT_PWM_FAST(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE) \
		INIT_GPIO_OUT_PWM_EX(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE, _FAST)
#define INIT_GPIO_OUT_PWM_OD_FAST(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE) \
		INIT_GPIO_OUT_PWM_EX(PORT, PIN, TIMER, TIMER_CHANNEL, INVERTED, VALUE, _OD_FAST)

#define INIT_GPIO_OUT_ADV_PWM(PORT, PIN, N_PORT, N_PIN, TIMER, TIMER_CHANNEL, INVRTED, VALUE) { \
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIO##PORT, ENABLE); \
		TIM_OCInitTypeDef TIM_OCInitStructure; \
		TIM_OCStructInit(&TIM_OCInitStructure); \
		TIM_OCInitStructure.TIM_Pulse = VALUE; \
		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; \
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; \
		TIM_OCInitStructure.TIM_OCPolarity = (INVRTED)?TIM_OCPolarity_Low:TIM_OCPolarity_High; \
		CCC(TIM_OC,TIMER_CHANNEL,Init)(CC(TIM,TIMER), &TIM_OCInitStructure); \
		INIT_GPIO_OUT_AF_FAST(PORT, PIN, CC(AF_TIM,TIMER)); \
		INIT_GPIO_OUT_AF_FAST(N_PORT, N_PIN, CC(AF_TIM,TIMER)); \
		/*TIM_BDTRInitTypeDef TIM_BDTRInitStructure;*/ \
		/*TIM_BDTRStructInit(&TIM_BDTRInitStructure);*/ \
		/*TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;*/ \
		/*TIM_BDTRConfig(TIMER, &TIM_BDTRInitStructure);*/ \
		CCC(TIM_OC,TIMER_CHANNEL,PreloadConfig)(CC(TIM,TIMER), TIM_OCPreload_Enable); \

#define INIT_INTERRUPT_PWM(TIMER, GroupPrio, SubPrio) { \
		TIM_ARRPreloadConfig(CC(TIM,TIMER), ENABLE); \
		TIM_ITConfig(CC(TIM,TIMER), TIM_IT_Update, ENABLE); \
		/*TIM_ITConfig(CC(TIM,TIMER), TIM_IT_Trigger, ENABLE);*/ \
		NVIC_INIT(CCC(TIM,TIMER,_IRQn), GroupPrio, SubPrio); \
}

#define START_TIMER_PWM(TIMER) { \
		TIM_Cmd(CC(TIM,TIMER), ENABLE); \
		if (TIMER == 1 || TIMER == 8) TIM_CtrlPWMOutputs(CC(TIM,TIMER), ENABLE); \
}
#define GPIO_OUT_SET_PWM(TIMER, TIMER_CHANNEL, VALUE) { \
		CC(TIM_SetCompare,TIMER_CHANNEL)(CC(TIM,TIMER), VALUE); \
}

#define NVIC_INIT(IRQn, GroupPrio, SubPrio) { \
		if (IRQn >= 0) NVIC_ClearPendingIRQ(IRQn); \
		NVIC_InitTypeDef NVIC_InitStructure; \
		NVIC_InitStructure.NVIC_IRQChannel = IRQn; \
		IF_STM32F0( \
			NVIC_InitStructure.NVIC_IRQChannelPriority = GroupPrio; \
			UNUSED(SubPrio); \
		) \
		IF_STM32F4( \
				NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = GroupPrio; \
				NVIC_InitStructure.NVIC_IRQChannelSubPriority = SubPrio; \
		) \
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; \
		NVIC_Init(&NVIC_InitStructure); \
}

#endif /* HW_MACROS_H_INCLUDED */
