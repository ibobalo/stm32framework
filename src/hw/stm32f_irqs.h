#include "stm32fxxxx.h"
#include "util/macros.h"

#define ISR_TABLE_ITERATOR_INT(OP,...) \
	/*InitialStack                   0x00 Initial value for stack pointer register  ) */ \
	/*Reset_Handler                  0x04 Reset                                ) */ \
	OP(NMI_##__VA_ARGS__                  /* 0x08 Non maskable interrupt            */   ) \
	OP(HardFault_##__VA_ARGS__            /* 0x0C All class of fault                */   ) \
IF_STM32F0( \
	OP(    reserved_10##__VA_ARGS__       /* 0x10                              */   ) \
	OP(    reserved_14##__VA_ARGS__       /* 0x14                              */   ) \
	OP(    reserved_18##__VA_ARGS__       /* 0x18                              */   ) \
) \
IF_STM32F4( \
	OP(MemManage_##__VA_ARGS__     /* 0x10 Memory management                 */   ) \
	OP(BusFault_##__VA_ARGS__      /* 0x14 Pre-fetch and memory access faults */  ) \
	OP(UsageFault_##__VA_ARGS__    /* 0x18 Undefined instruction, illegal state */  ) \
) \
	OP(    reserved_1C##__VA_ARGS__       /* 0x1C                              */   ) \
	OP(    reserved_20##__VA_ARGS__       /* 0x20                              */   ) \
	OP(    reserved_24##__VA_ARGS__       /* 0x24                              */   ) \
	OP(    reserved_28##__VA_ARGS__       /* 0x28                              */   ) \
	OP(SVC_##__VA_ARGS__                   /* 0x2C System Service call via SWI instruction */ ) \
IF_STM32F0( \
	OP(    reserved_30##__VA_ARGS__       /* 0x30                              */   ) \
) \
IF_STM32F4( \
	OP(DebugMon_##__VA_ARGS__             /* 0x30 Debug Monitor                */   ) \
) \
	OP(    reserved_34##__VA_ARGS__       /* 0x34                              */   ) \
	OP(PendSV_##__VA_ARGS__               /* 0x38 Pendable request for system service */ ) \
	OP(SysTick_##__VA_ARGS__              /* 0x3C System tick timer                 */   )

#ifdef INTERRUPTS_DISABLE

#define	ISR_TABLE_ITERATOR_EXT(OP,...)
#define	ISR_SHARED_ITERATOR(OP,...)

#else // ! INTERRUPTS_DISABLE

#define	ISR_TABLE_ITERATOR_EXT(OP,...) \
	/* External Interrupts */ \
	OP(WWDG_IRQ##__VA_ARGS__                /* 0x40 Window Watchdog                                */) \
IF_STM32F030( \
	OP(    reserved_44##__VA_ARGS__         /* 0x44 Reserved                                       */) \
	OP(RTC_IRQ##__VA_ARGS__                 /* 0x48 RTC through EXTI Line                          */) \
	OP(FLASH_IRQ##__VA_ARGS__               /* 0x4C FLASH                                          */) \
	OP(RCC_IRQ##__VA_ARGS__                 /* 0x50 RCC                                            */) \
	OP(EXTI0_1_IRQ##__VA_ARGS__             /* 0x54 EXTI Line 0 and 1                              */) \
	OP(EXTI2_3_IRQ##__VA_ARGS__             /* 0x58 EXTI Line 2 and 3                              */) \
	OP(EXTI4_15_IRQ##__VA_ARGS__            /* 0x5C EXTI Line 4 to 15                              */) \
	OP(    reserved_60##__VA_ARGS__         /* 0x60 Reserved                                       */) \
	OP(DMA1_Channel1_IRQ##__VA_ARGS__       /* 0x64 DMA1 Channel 1                                 */) \
	OP(DMA1_Channel2_3_IRQ##__VA_ARGS__     /* 0x68 DMA1 Channel 2 and Channel 3                   */) \
	OP(DMA1_Channel4_5_IRQ##__VA_ARGS__     /* 0x6C DMA1 Channel 4 and Channel 5                   */) \
	OP(ADC1_IRQ##__VA_ARGS__                /* 0x70 ADC1                                           */) \
	OP(TIM1_BRK_UP_TRG_COM_IRQ##__VA_ARGS__ /* 0x74 TIM1 Break, Update, Trigger and Commutation    */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__             /* 0x78 TIM1 Capture Compare                           */) \
	OP(    reserved_7c##__VA_ARGS__         /* 0x7C Reserved                                       */) \
	OP(TIM3_IRQ##__VA_ARGS__                /* 0x80 TIM3                                           */) \
	OP(    reserved_84##__VA_ARGS__         /* 0x84 Reserved                                       */) \
	OP(    reserved_88##__VA_ARGS__         /* 0x88 Reserved                                       */) \
	OP(TIM14_IRQ##__VA_ARGS__               /* 0x8C TIM14                                          */) \
	OP(TIM15_IRQ##__VA_ARGS__               /* 0x90 TIM15                                          */) \
	OP(TIM16_IRQ##__VA_ARGS__               /* 0x94 TIM16                                          */) \
	OP(TIM17_IRQ##__VA_ARGS__               /* 0x98 TIM17                                          */) \
	OP(I2C1_IRQ##__VA_ARGS__                /* 0x9C I2C1                                           */) \
	OP(I2C2_IRQ##__VA_ARGS__                /* 0xA0 I2C2                                           */) \
	OP(SPI1_IRQ##__VA_ARGS__                /* 0xA4 SPI1                                           */) \
	OP(SPI2_IRQ##__VA_ARGS__                /* 0xA8 SPI2                                           */) \
	OP(USART1_IRQ##__VA_ARGS__              /* 0xAC USART1                                         */) \
	OP(USART2_IRQ##__VA_ARGS__              /* 0xB0 USART2                                         */) \
) \
IF_STM32F031( \
	OP(PVD_IRQ##__VA_ARGS__                 /* 0x44 PVD through EXTI Line detect                   */) \
	OP(RTC_IRQ##__VA_ARGS__                 /* 0x48 RTC through EXTI Line                          */) \
	OP(FLASH_IRQ##__VA_ARGS__               /* 0x4C FLASH                                          */) \
	OP(RCC_IRQ##__VA_ARGS__                 /* 0x50 RCC                                            */) \
	OP(EXTI0_1_IRQ##__VA_ARGS__             /* 0x54 EXTI Line 0 and 1                              */) \
	OP(EXTI2_3_IRQ##__VA_ARGS__             /* 0x58 EXTI Line 2 and 3                              */) \
	OP(EXTI4_15_IRQ##__VA_ARGS__            /* 0x5C EXTI Line 4 to 15                              */) \
	OP(    reserved_60##__VA_ARGS__         /* 0x60 Reserved                                       */) \
	OP(DMA1_Channel1_IRQ##__VA_ARGS__       /* 0x64 DMA1 Channel 1                                 */) \
	OP(DMA1_Channel2_3_IRQ##__VA_ARGS__     /* 0x68 DMA1 Channel 2 and Channel 3                   */) \
	OP(DMA1_Channel4_5_IRQ##__VA_ARGS__     /* 0x6C DMA1 Channel 4 and Channel 5                   */) \
	OP(ADC1_IRQ##__VA_ARGS__                /* 0x70 ADC1                                           */) \
	OP(TIM1_BRK_UP_TRG_COM_IRQ##__VA_ARGS__ /* 0x74 TIM1 Break, Update, Trigger and Commutation    */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__             /* 0x78 TIM1 Capture Compare                           */) \
	OP(TIM2_IRQ##__VA_ARGS__                /* 0x7C TIM2                                           */) \
	OP(TIM3_IRQ##__VA_ARGS__                /* 0x80 TIM3                                           */) \
	OP(    reserved_84##__VA_ARGS__         /* 0x84 Reserved                                       */) \
	OP(    reserved_88##__VA_ARGS__         /* 0x88 Reserved                                       */) \
	OP(TIM14_IRQ##__VA_ARGS__               /* 0x8C TIM14                                          */) \
	OP(    reserved_90##__VA_ARGS__         /* 0x90 Reserved                                       */) \
	OP(TIM16_IRQ##__VA_ARGS__               /* 0x94 TIM16                                          */) \
	OP(TIM17_IRQ##__VA_ARGS__               /* 0x98 TIM17                                          */) \
	OP(I2C1_IRQ##__VA_ARGS__                /* 0x9C I2C1                                           */) \
	OP(    reserved_A0##__VA_ARGS__         /* 0xA0 Reserved                                       */) \
	OP(SPI1_IRQ##__VA_ARGS__                /* 0xA4 SPI1                                           */) \
	OP(    reserved_A8##__VA_ARGS__         /* 0xA8 Reserved                                       */) \
	OP(USART1_IRQ##__VA_ARGS__              /* 0xAC USART1                                         */) \
) \
IF_STM32F042( \
	OP(PVD_VDDIO2_IRQ##__VA_ARGS__          /* 0x44 PVD and VDDIO2 supply comparator through EXTI Line detect */) \
	OP(RTC_IRQ##__VA_ARGS__                 /* 0x48 RTC through EXTI Line                          */) \
	OP(FLASH_IRQ##__VA_ARGS__               /* 0x4C FLASH                                          */) \
	OP(RCC_CRS_IRQ##__VA_ARGS__             /* 0x50 RCC and CRS                                    */) \
	OP(EXTI0_1_IRQ##__VA_ARGS__             /* 0x54 EXTI Line 0 and 1                              */) \
	OP(EXTI2_3_IRQ##__VA_ARGS__             /* 0x58 EXTI Line 2 and 3                              */) \
	OP(EXTI4_15_IRQ##__VA_ARGS__            /* 0x5C EXTI Line 4 to 15                              */) \
	OP(TSC_IRQ##__VA_ARGS__                 /* 0x60 TSC                                            */) \
	OP(DMA1_Channel1_IRQ##__VA_ARGS__       /* 0x64 DMA1 Channel 1                                 */) \
	OP(DMA1_Channel2_3_IRQ##__VA_ARGS__     /* 0x68 DMA1 Channel 2 and Channel 3                   */) \
	OP(DMA1_Channel4_5_IRQ##__VA_ARGS__     /* 0x6C DMA1 Channel 4, Channel 5                      */) \
	OP(ADC1_IRQ##__VA_ARGS__                /* 0x70 ADC1                                           */) \
	OP(TIM1_BRK_UP_TRG_COM_IRQ##__VA_ARGS__ /* 0x74 TIM1 Break, Update, Trigger and Commutation    */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__             /* 0x78 TIM1 Capture Compare                           */) \
	OP(TIM2_IRQ##__VA_ARGS__                /* 0x7C TIM2                                           */) \
	OP(TIM3_IRQ##__VA_ARGS__                /* 0x80 TIM3                                           */) \
	OP(    reserved_84##__VA_ARGS__         /* 0x84 Reserved                                       */) \
	OP(    reserved_88##__VA_ARGS__         /* 0x88 Reserved                                       */) \
	OP(TIM14_IRQ##__VA_ARGS__               /* 0x8C TIM14                                          */) \
	OP(    reserved_90##__VA_ARGS__         /* 0x90 Reserved                                       */) \
	OP(TIM16_IRQ##__VA_ARGS__               /* 0x94 TIM16                                          */) \
	OP(TIM17_IRQ##__VA_ARGS__               /* 0x98 TIM17                                          */) \
	OP(I2C1_IRQ##__VA_ARGS__                /* 0x9C I2C1                                           */) \
	OP(    reserved_a0##__VA_ARGS__         /* 0xA0 Reserved                                       */) \
	OP(SPI1_IRQ##__VA_ARGS__                /* 0xA4 SPI1                                           */) \
	OP(SPI2_IRQ##__VA_ARGS__                /* 0xA8 SPI2                                           */) \
	OP(USART1_IRQ##__VA_ARGS__              /* 0xAC USART1                                         */) \
	OP(USART2_IRQ##__VA_ARGS__              /* 0xB0 USART2                                         */) \
	OP(    reserved_b4##__VA_ARGS__         /* 0xB4 Reserved                                       */) \
	OP(CEC_CAN_IRQ##__VA_ARGS__             /* 0xB8 CEC and CAN                                    */) \
	OP(USB_IRQ##__VA_ARGS__                 /* 0xBC USB Low Priority global                        */) \
) \
IF_STM32F051( \
	OP(PVD_IRQ##__VA_ARGS__                 /* 0x44 PVD through EXTI Line detect                   */) \
	OP(RTC_IRQ##__VA_ARGS__                 /* 0x48 RTC through EXTI Line                          */) \
	OP(FLASH_IRQ##__VA_ARGS__               /* 0x4C FLASH                                          */) \
	OP(RCC_IRQ##__VA_ARGS__                 /* 0x50 RCC                                            */) \
	OP(EXTI0_1_IRQ##__VA_ARGS__             /* 0x54 EXTI Line 0 and 1                              */) \
	OP(EXTI2_3_IRQ##__VA_ARGS__             /* 0x58 EXTI Line 2 and 3                              */) \
	OP(EXTI4_15_IRQ##__VA_ARGS__            /* 0x5C EXTI Line 4 to 15                              */) \
	OP(TS_IRQ##__VA_ARGS__                  /* 0x60 Touch sense controller                         */) \
	OP(DMA1_Channel1_IRQ##__VA_ARGS__       /* 0x64 DMA1 Channel 1                                 */) \
	OP(DMA1_Channel2_3_IRQ##__VA_ARGS__     /* 0x68 DMA1 Channel 2 and Channel 3                   */) \
	OP(DMA1_Channel4_5_IRQ##__VA_ARGS__     /* 0x6C DMA1 Channel 4 and Channel 5                   */) \
	OP(ADC1_COMP_IRQ##__VA_ARGS__           /* 0x70 ADC1, COMP1 and COMP2                          */) \
	OP(TIM1_BRK_UP_TRG_COM_IRQ##__VA_ARGS__ /* 0x74 TIM1 Break, Update, Trigger and Commutation    */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__             /* 0x78 TIM1 Capture Compare                           */) \
	OP(TIM2_IRQ##__VA_ARGS__                /* 0x7C TIM2                                           */) \
	OP(TIM3_IRQ##__VA_ARGS__                /* 0x80 TIM3                                           */) \
	OP(TIM6_DAC_IRQ##__VA_ARGS__            /* 0x84 TIM6 and DAC                                   */) \
	OP(    reserved_88##__VA_ARGS__         /* 0x88 Reserved                                       */) \
	OP(TIM14_IRQ##__VA_ARGS__               /* 0x8C TIM14                                          */) \
	OP(TIM15_IRQ##__VA_ARGS__               /* 0x90 TIM15                                          */) \
	OP(TIM16_IRQ##__VA_ARGS__               /* 0x94 TIM16                                          */) \
	OP(TIM17_IRQ##__VA_ARGS__               /* 0x98 TIM17                                          */) \
	OP(I2C1_IRQ##__VA_ARGS__                /* 0x9C I2C1                                           */) \
	OP(I2C2_IRQ##__VA_ARGS__                /* 0xA0 I2C2                                           */) \
	OP(SPI1_IRQ##__VA_ARGS__                /* 0xA4 SPI1                                           */) \
	OP(SPI2_IRQ##__VA_ARGS__                /* 0xA8 SPI2                                           */) \
	OP(USART1_IRQ##__VA_ARGS__              /* 0xAC USART1                                         */) \
	OP(USART2_IRQ##__VA_ARGS__              /* 0xB0 USART2                                         */) \
	OP(    reserved_b4##__VA_ARGS__         /* 0xB4 Reserved                                       */) \
	OP(CEC_IRQ##__VA_ARGS__                 /* 0xB8 CEC                                            */) \
) \
IF_STM32F072( \
	OP(PVD_VDDIO2_IRQ##__VA_ARGS__          /* 0x44 PVD and VDDIO2 supply comparator through EXTI Line detect  */) \
	OP(RTC_IRQ##__VA_ARGS__                 /* 0x48 RTC through EXTI Line                                */) \
	OP(FLASH_IRQ##__VA_ARGS__               /* 0x4C FLASH                                                */) \
	OP(RCC_CRS_IRQ##__VA_ARGS__             /* 0x50 RCC and CRS                                          */) \
	OP(EXTI0_1_IRQ##__VA_ARGS__             /* 0x54 EXTI Line 0 and 1                                    */) \
	OP(EXTI2_3_IRQ##__VA_ARGS__             /* 0x58 EXTI Line 2 and 3                                    */) \
	OP(EXTI4_15_IRQ##__VA_ARGS__            /* 0x5C EXTI Line 4 to 15                                    */) \
	OP(TSC_IRQ##__VA_ARGS__                 /* 0x60 TSC                                                  */) \
	OP(DMA1_Channel1_IRQ##__VA_ARGS__       /* 0x64 DMA1 Channel 1                                       */) \
	OP(DMA1_Channel2_3_IRQ##__VA_ARGS__     /* 0x68 DMA1 Channel 2 and Channel 3                         */) \
	OP(DMA1_Channel4_5_6_7_IRQ##__VA_ARGS__ /* 0x6C DMA1 Channel 4, Channel 5, Channel 6 and Channel 7   */) \
	OP(ADC1_COMP_IRQ##__VA_ARGS__           /* 0x70 ADC1, COMP1 and COMP2                                */) \
	OP(TIM1_BRK_UP_TRG_COM_IRQ##__VA_ARGS__ /* 0x74 TIM1 Break, Update, Trigger and Commutation          */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__             /* 0x78 TIM1 Capture Compare                                 */) \
	OP(TIM2_IRQ##__VA_ARGS__                /* 0x7C TIM2                                                 */) \
	OP(TIM3_IRQ##__VA_ARGS__                /* 0x80 TIM3                                                 */) \
	OP(TIM6_DAC_IRQ##__VA_ARGS__            /* 0x84 TIM6 and DAC                                         */) \
	OP(TIM7_IRQ##__VA_ARGS__                /* 0x88 TIM7                                                 */) \
	OP(TIM14_IRQ##__VA_ARGS__               /* 0x8C TIM14                                                */) \
	OP(TIM15_IRQ##__VA_ARGS__               /* 0x90 TIM15                                                */) \
	OP(TIM16_IRQ##__VA_ARGS__               /* 0x94 TIM16                                                */) \
	OP(TIM17_IRQ##__VA_ARGS__               /* 0x98 TIM17                                                */) \
	OP(I2C1_IRQ##__VA_ARGS__                /* 0x9C I2C1                                                 */) \
	OP(I2C2_IRQ##__VA_ARGS__                /* 0xA0 I2C2                                                 */) \
	OP(SPI1_IRQ##__VA_ARGS__                /* 0xA4 SPI1                                                 */) \
	OP(SPI2_IRQ##__VA_ARGS__                /* 0xA8 SPI2                                                 */) \
	OP(USART1_IRQ##__VA_ARGS__              /* 0xAC USART1                                               */) \
	OP(USART2_IRQ##__VA_ARGS__              /* 0xB0 USART2                                               */) \
	OP(USART3_4_IRQ##__VA_ARGS__            /* 0xB4 USART3 and USART4                                    */) \
	OP(CEC_CAN_IRQ##__VA_ARGS__             /* 0xB8 CEC and CAN                                          */) \
	OP(USB_IRQ##__VA_ARGS__                 /* 0xBC USB Low Priority global                              */) \
) \
IF_STM32F091( \
	OP(PVD_VDDIO2_IRQ##__VA_ARGS__            /* 0x44 PVD and VDDIO2 supply comparator through EXTI Line detect */) \
	OP(RTC_IRQ##__VA_ARGS__                   /* 0x48 RTC through EXTI Line                                */) \
	OP(FLASH_IRQ##__VA_ARGS__                 /* 0x4C FLASH                                                */) \
	OP(RCC_CRS_IRQ##__VA_ARGS__               /* 0x50 RCC and CRS                                          */) \
	OP(EXTI0_1_IRQ##__VA_ARGS__               /* 0x54 EXTI Line 0 and 1                                    */) \
	OP(EXTI2_3_IRQ##__VA_ARGS__               /* 0x58 EXTI Line 2 and 3                                    */) \
	OP(EXTI4_15_IRQ##__VA_ARGS__              /* 0x5C EXTI Line 4 to 15                                    */) \
	OP(TSC_IRQ##__VA_ARGS__                   /* 0x60 TSC                                                  */) \
	OP(DMA1_Ch1_IRQ##__VA_ARGS__              /* 0x64 DMA1 Channel 1                                       */) \
	OP(DMA1_Ch2_3_DMA2_Ch1_2_IRQ##__VA_ARGS__ /* 0x68 DMA1 Channel 2 and Channel 3                         */) \
	OP(DMA1_Ch4_7_DMA2_Ch3_5_IRQ##__VA_ARGS__ /* 0x6C DMA1 Channel 4, Channel 5, Channel 6 and Channel 7   */) \
	OP(ADC1_COMP_IRQ##__VA_ARGS__             /* 0x70 ADC1, COMP1 and COMP2                                */) \
	OP(TIM1_BRK_UP_TRG_COM_IRQ##__VA_ARGS__   /* 0x74 TIM1 Break, Update, Trigger and Commutation          */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__               /* 0x78 TIM1 Capture Compare                                 */) \
	OP(TIM2_IRQ##__VA_ARGS__                  /* 0x7C TIM2                                                 */) \
	OP(TIM3_IRQ##__VA_ARGS__                  /* 0x80 TIM3                                                 */) \
	OP(TIM6_DAC_IRQ##__VA_ARGS__              /* 0x84 TIM6 and DAC                                         */) \
	OP(TIM7_IRQ##__VA_ARGS__                  /* 0x88 TIM7                                                 */) \
	OP(TIM14_IRQ##__VA_ARGS__                 /* 0x8C TIM14                                                */) \
	OP(TIM15_IRQ##__VA_ARGS__                 /* 0x90 TIM15                                                */) \
	OP(TIM16_IRQ##__VA_ARGS__                 /* 0x94 TIM16                                                */) \
	OP(TIM17_IRQ##__VA_ARGS__                 /* 0x98 TIM17                                                */) \
	OP(I2C1_IRQ##__VA_ARGS__                  /* 0x9C I2C1                                                 */) \
	OP(I2C2_IRQ##__VA_ARGS__                  /* 0xA0 I2C2                                                 */) \
	OP(SPI1_IRQ##__VA_ARGS__                  /* 0xA4 SPI1                                                 */) \
	OP(SPI2_IRQ##__VA_ARGS__                  /* 0xA8 SPI2                                                 */) \
	OP(USART1_IRQ##__VA_ARGS__                /* 0xAC USART1                                               */) \
	OP(USART2_IRQ##__VA_ARGS__                /* 0xB0 USART2                                               */) \
	OP(USART3_8_IRQ##__VA_ARGS__              /* 0xB4 USART3 and USART4                                    */) \
	OP(CEC_CAN_IRQ##__VA_ARGS__               /* 0xB8 CEC and CAN                                          */) \
) \
IF_STM32F4( \
	OP(PVD_IRQ##__VA_ARGS__                /* 0x44 PVD through EXTI Line detection             */) \
	OP(TAMP_STAMP_IRQ##__VA_ARGS__         /* 0x48 Tamper and TimeStamps through the EXTI line */) \
	OP(RTC_WKUP_IRQ##__VA_ARGS__           /* 0x4C RTC Wakeup through the EXTI line            */) \
	OP(FLASH_IRQ##__VA_ARGS__              /* 0x50 FLASH                                       */) \
	OP(RCC_IRQ##__VA_ARGS__                /* 0x54 RCC                                         */) \
	OP(EXTI0_IRQ##__VA_ARGS__              /* 0x58 EXTI Line0                                  */) \
	OP(EXTI1_IRQ##__VA_ARGS__              /* 0x5C EXTI Line1                                  */) \
	OP(EXTI2_IRQ##__VA_ARGS__              /* 0x60 EXTI Line2                                  */) \
	OP(EXTI3_IRQ##__VA_ARGS__              /* 0x64 EXTI Line3                                  */) \
	OP(EXTI4_IRQ##__VA_ARGS__              /* 0x68 EXTI Line4                                  */) \
	OP(DMA1_Stream0_IRQ##__VA_ARGS__       /* 0x6C DMA1 Stream 0                               */) \
	OP(DMA1_Stream1_IRQ##__VA_ARGS__       /* 0x70 DMA1 Stream 1                               */) \
	OP(DMA1_Stream2_IRQ##__VA_ARGS__       /* 0x74 DMA1 Stream 2                               */) \
	OP(DMA1_Stream3_IRQ##__VA_ARGS__       /* 0x78 DMA1 Stream 3                               */) \
	OP(DMA1_Stream4_IRQ##__VA_ARGS__       /* 0x7C DMA1 Stream 4                               */) \
	OP(DMA1_Stream5_IRQ##__VA_ARGS__       /* 0x80 DMA1 Stream 5                               */) \
	OP(DMA1_Stream6_IRQ##__VA_ARGS__       /* 0x84 DMA1 Stream 6                               */) \
	OP(ADC_IRQ##__VA_ARGS__                /* 0x88 ADC1, ADC2 and ADC3s                        */) \
	OP(CAN1_TX_IRQ##__VA_ARGS__            /* 0x8C CAN1 TX                                     */) \
	OP(CAN1_RX0_IRQ##__VA_ARGS__           /* 0x90 CAN1 RX0                                    */) \
	OP(CAN1_RX1_IRQ##__VA_ARGS__           /* 0x94 CAN1 RX1                                    */) \
	OP(CAN1_SCE_IRQ##__VA_ARGS__           /* 0x98 CAN1 SCE                                    */) \
	OP(EXTI9_5_IRQ##__VA_ARGS__            /* 0x9C External Line[9:5]s                         */) \
	OP(TIM1_BRK_TIM9_IRQ##__VA_ARGS__      /* 0xA0 TIM1 Break and TIM9                         */) \
	OP(TIM1_UP_TIM10_IRQ##__VA_ARGS__      /* 0xA4 TIM1 Update and TIM10                       */) \
	OP(TIM1_TRG_COM_TIM11_IRQ##__VA_ARGS__ /* 0xA8 TIM1 Trigger and Commutation and TIM11      */) \
	OP(TIM1_CC_IRQ##__VA_ARGS__            /* 0xAC TIM1 Capture Compare                        */) \
	OP(TIM2_IRQ##__VA_ARGS__               /* 0xB0 TIM2                                        */) \
	OP(TIM3_IRQ##__VA_ARGS__               /* 0xB4 TIM3                                        */) \
	OP(TIM4_IRQ##__VA_ARGS__               /* 0xB8 TIM4                                        */) \
	OP(I2C1_EV_IRQ##__VA_ARGS__            /* 0xBC I2C1 Event                                  */) \
	OP(I2C1_ER_IRQ##__VA_ARGS__            /* 0xC0 I2C1 Error                                  */) \
	OP(I2C2_EV_IRQ##__VA_ARGS__            /* 0xC4 I2C2 Event                                  */) \
	OP(I2C2_ER_IRQ##__VA_ARGS__            /* 0xC8 I2C2 Error                                  */) \
	OP(SPI1_IRQ##__VA_ARGS__               /* 0xCC SPI1                                        */) \
	OP(SPI2_IRQ##__VA_ARGS__               /* 0xD0 SPI2                                        */) \
	OP(USART1_IRQ##__VA_ARGS__             /* 0xD4 USART1                                      */) \
	OP(USART2_IRQ##__VA_ARGS__             /* 0xD8 USART2                                      */) \
	OP(USART3_IRQ##__VA_ARGS__             /* 0xDC USART3                                      */) \
	OP(EXTI15_10_IRQ##__VA_ARGS__          /* 0xE0 External Line[15:10]s                       */) \
	OP(RTC_Alarm_IRQ##__VA_ARGS__          /* 0xE4 RTC Alarm (A and B) through EXTI Line       */) \
	OP(OTG_FS_WKUP_IRQ##__VA_ARGS__        /* 0xE8 USB OTG FS Wakeup through EXTI line         */) \
	OP(TIM8_BRK_TIM12_IRQ##__VA_ARGS__     /* 0xEC TIM8 Break and TIM12                        */) \
	OP(TIM8_UP_TIM13_IRQ##__VA_ARGS__      /* 0xF0 TIM8 Update and TIM13                       */) \
	OP(TIM8_TRG_COM_TIM14_IRQ##__VA_ARGS__ /* 0xF4 TIM8 Trigger and Commutation and TIM14      */) \
	OP(TIM8_CC_IRQ##__VA_ARGS__            /* 0xF8 TIM8 Capture Compare                        */) \
	OP(DMA1_Stream7_IRQ##__VA_ARGS__       /* 0xFC DMA1 Stream7                                */) \
	OP(FSMC_IRQ##__VA_ARGS__               /* 0x100 FSMC                                       */) \
	OP(SDIO_IRQ##__VA_ARGS__               /* 0x104 SDIO                                       */) \
	OP(TIM5_IRQ##__VA_ARGS__               /* 0x108 TIM5                                       */) \
	OP(SPI3_IRQ##__VA_ARGS__               /* 0x10C SPI3                                       */) \
	OP(UART4_IRQ##__VA_ARGS__              /* 0x110 UART4                                      */) \
	OP(UART5_IRQ##__VA_ARGS__              /* 0x114 UART5                                      */) \
	OP(TIM6_DAC_IRQ##__VA_ARGS__           /* 0x118 TIM6 and DAC1&2 underrun errors            */) \
	OP(TIM7_IRQ##__VA_ARGS__               /* 0x11C TIM7                                       */) \
	OP(DMA2_Stream0_IRQ##__VA_ARGS__       /* 0x120 DMA2 Stream 0                              */) \
	OP(DMA2_Stream1_IRQ##__VA_ARGS__       /* 0x124 DMA2 Stream 1                              */) \
	OP(DMA2_Stream2_IRQ##__VA_ARGS__       /* 0x128 DMA2 Stream 2                              */) \
	OP(DMA2_Stream3_IRQ##__VA_ARGS__       /* 0x12C DMA2 Stream 3                              */) \
	OP(DMA2_Stream4_IRQ##__VA_ARGS__       /* 0x130 DMA2 Stream 4                              */) \
	OP(ETH_IRQ##__VA_ARGS__                /* 0x134 Ethernet                                   */) \
	OP(ETH_WKUP_IRQ##__VA_ARGS__           /* 0x138 Ethernet Wakeup through EXTI line          */) \
	OP(CAN2_TX_IRQ##__VA_ARGS__            /* 0x13C CAN2 TX                                    */) \
	OP(CAN2_RX0_IRQ##__VA_ARGS__           /* 0x140 CAN2 RX0                                   */) \
	OP(CAN2_RX1_IRQ##__VA_ARGS__           /* 0x144 CAN2 RX1                                   */) \
	OP(CAN2_SCE_IRQ##__VA_ARGS__           /* 0x148 CAN2 SCE                                   */) \
	OP(OTG_FS_IRQ##__VA_ARGS__             /* 0x14C USB OTG FS                                 */) \
	OP(DMA2_Stream5_IRQ##__VA_ARGS__       /* 0x150 DMA2 Stream 5                              */) \
	OP(DMA2_Stream6_IRQ##__VA_ARGS__       /* 0x154 DMA2 Stream 6                              */) \
	OP(DMA2_Stream7_IRQ##__VA_ARGS__       /* 0x158 DMA2 Stream 7                              */) \
	OP(USART6_IRQ##__VA_ARGS__             /* 0x15C USART6                                     */) \
	OP(I2C3_EV_IRQ##__VA_ARGS__            /* 0x160 I2C3 event                                 */) \
	OP(I2C3_ER_IRQ##__VA_ARGS__            /* 0x164 I2C3 error                                 */) \
	OP(OTG_HS_EP1_OUT_IRQ##__VA_ARGS__     /* 0x168 USB OTG HS End Point 1 Out                 */) \
	OP(OTG_HS_EP1_IN_IRQ##__VA_ARGS__      /* 0x16C USB OTG HS End Point 1 In                  */) \
	OP(OTG_HS_WKUP_IRQ##__VA_ARGS__        /* 0x170 USB OTG HS Wakeup through EXTI             */) \
	OP(OTG_HS_IRQ##__VA_ARGS__             /* 0x174 USB OTG HS                                 */) \
	OP(DCMI_IRQ##__VA_ARGS__               /* 0x178 DCMI                                       */) \
	OP(CRYP_IRQ##__VA_ARGS__               /* 0x17C CRYP crypto                                */) \
	OP(HASH_RNG_IRQ##__VA_ARGS__           /* 0x180 Hash and Rng                               */) \
	OP(FPU_IRQ##__VA_ARGS__                /* 0x184 FPU                                        */) \
) \

#ifdef STM32F0
#define	ISR_SHARED_SUBS_EXTI0_1_IRQ \
		EXTI0_IRQ, EXTI1_IRQ
#define	ISR_SHARED_SUBS_EXTI2_3_IRQ \
		EXTI2_IRQ, EXTI3_IRQ
#define	ISR_SHARED_SUBS_EXTI4_15_IRQ \
		EXTI4_IRQ, EXTI5_IRQ, EXTI6_IRQ, EXTI7_IRQ, \
		EXTI8_IRQ, EXTI9_IRQ, EXTI10_IRQ, EXTI11_IRQ, \
		EXTI12_IRQ, EXTI13_IRQ, EXTI14_IRQ, EXTI15_IRQ
#define	ISR_SHARED_SUBS_DMA1_Channel2_3_IRQ \
		DMA1_Channel2_IRQ, DMA1_Channel3_IRQ
#endif
#if defined(STM32F030) || defined(STM32F031) || defined(STM32F042) || defined(STM32F051)
#define	ISR_SHARED_SUBS_DMA1_Channel4_5_IRQ \
		DMA1_Channel4_IRQ, DMA1_Channel5_IRQ
#endif
#if defined(STM32F072)
#define	ISR_SHARED_SUBS_DMA1_Channel4_5_6_7_IRQ \
		DMA1_Channel4_IRQ, DMA1_Channel5_IRQ, DMA1_Channel6_IRQ, DMA1_Channel7_IRQ
#define	ISR_SHARED_SUBS_USART3_4_IRQ \
		USART3_IRQ, USART4_IRQ
#endif
#if defined(STM32F091)
#defined ISR_SHARED_SUBS_DMA1_Ch2_3_DMA2_Ch1_2_IRQ \
		DMA1_Ch2_IRQ, DMA1_Ch3_IRQ, DMA2_Ch1_IRQ, DMA2_Ch2_IRQ
#define	ISR_SHARED_SUBS_USART3_8_IRQ \
		USART3_IRQ, USART4_IRQ, USART5_IRQ, USART6_IRQ, USART7_IRQ, USART8_IRQ
#endif

#define	ISR_SHARED_ITERATOR(OP,...) \
	/* SHARED Interrupts */ \
IF_STM32F0( \
		OP(EXTI0_1_IRQ##__VA_ARGS__) \
		OP(EXTI2_3_IRQ##__VA_ARGS__) \
		OP(EXTI4_15_IRQ##__VA_ARGS__) \
		OP(DMA1_Channel2_3_IRQ##__VA_ARGS__) \
) \
IF_STM32F030( \
		OP(DMA1_Channel4_5_IRQ##__VA_ARGS__) \
) \
IF_STM32F031( \
		OP(DMA1_Channel4_5_IRQ##__VA_ARGS__) \
) \
IF_STM32F042( \
		OP(DMA1_Channel4_5_IRQ##__VA_ARGS__) \
) \
IF_STM32F051( \
		OP(DMA1_Channel4_5_IRQ##__VA_ARGS__) \
) \
IF_STM32F072( \
		OP(DMA1_Channel4_5_6_7_IRQ##__VA_ARGS__) \
		OP(USART3_4_IRQ##__VA_ARGS__) \
) \
IF_STM32F091( \
		OP(DMA1_Ch2_3_DMA2_Ch1_2_IRQ##__VA_ARGS__) \
		OP(USART3_8_IRQ##__VA_ARGS__) \
) \
IF_STM32F4( \
		OP(EXTI9_5_IRQ##__VA_ARGS__) \
		OP(EXTI15_10_IRQ##__VA_ARGS__) \
) \

#endif //INTERRUPTS_DISABLE

#define	ISR_TABLE_ITERATOR_ALL(OP,...) \
	ISR_TABLE_ITERATOR_INT(OP, ##__VA_ARGS__) \
	ISR_TABLE_ITERATOR_EXT(OP, ##__VA_ARGS__)


// declare all interrupt handlers:
// void USART1_IRQHandler(void);

#define ISR_ENTRY_DECLARE(name)     extern "C" void name##Handler(void);
ISR_TABLE_ITERATOR_ALL(ISR_ENTRY_DECLARE);

// declare all shared interrupt sub handlers:
// for host USART3_4_IRQHandler:
// void USART3_IRQSubHandler(void); void USART4_IRQSubHandler(void);

#define ISR_SHARED_SUB_DECLARE(sub_name)     extern "C" void CC(sub_name,SubHandler)(void);
#define ISR_SHARED_HOST_DECLARE(host_name) MAP(ISR_SHARED_SUB_DECLARE, CC(ISR_SHARED_SUBS_,host_name))
ISR_SHARED_ITERATOR(ISR_SHARED_HOST_DECLARE)
