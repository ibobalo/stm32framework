#ifneq (,$(filter-out STM32F401%B STM32F401%C STM32F405%E STM32F405%G STM32F407%E STM32F407%G STM32F446%C STM32F446%E, $(MPU)))
#$(error invalid target mpu $(MPU). Allowed are STM32F401xB STM32F401xC STM32F405xE STM32F405xG STM32F407xE STM32F407xG)  
#endif

UID_ADDR_FOR_DEVID = $(strip \
	$(if $(filter $1, 0x0440 0x0442 0x0444 0x0445 0x0448 ), 0x1FFFF7AC ) \
	$(if $(filter $1, 0x0413 0x0419 0x0421 0x0423 0x0431 0x0433 0x0434 0x0441 0x0458 0x0463), 0x1FFF7A10) \
)

ifneq (,$(MPU))

ifneq ($(call strlen,$(MPU)), 11)
$(error invalid target mpu format $(MPU). Allowed are 11 symbol length format STM32Fsmmpf, where <s> - 1 digit serie 0 or 4, <mm> - 2 digit model <p> - 1 letter pin count, <f> - memory size)
endif

MPU_SERIE = $(call substr,$(MPU),7,7)
MPU_MODEL = $(call substr,$(MPU),7,9)
MPU_PINCODE = $(call substr,$(MPU),10,10)
MPU_MEMCODE = $(call substr,$(MPU),11,11)

UID_ADDR = $(call UID_ADDR_FOR_DEVID, $(MPU_DEVID))  
#$(strip \
#	$(if $(filter 042, $(MPU_MODEL)),  0x1FFFF7AC) \
#	$(if $(filter 072, $(MPU_MODEL)),  0x1FFFF7AC) \
#	$(if $(filter 4%,  $(MPU_MODEL)),  0x1FFF7A10) \
#)
FLASHSIZE_ADDR = $(strip \
	$(if $(filter 042, $(MPU_MODEL)),  0x1FFFF7CC) \
	$(if $(filter 072, $(MPU_MODEL)),  0x1FFFF7CC) \
	$(if $(filter 4%,  $(MPU_MODEL)),  0x1FFF7A22) \
)
PACKAGEID_ADDR = $(strip \
	$(if $(filter 446, $(MPU_MODEL)),  0x1FFF7BF0) \
)
OPTIONBYTE_ADDR = $(strip \
	$(if $(filter 0%, $(MPU_MODEL)),   0x1FFFF801) \
	$(if $(filter 405, $(MPU_MODEL)),  0x1FFFC001) \
)
DEFINES += \
	$(if $(UID_ADDR),       UID_ADDR=$(UID_ADDR),) \
	$(if $(FLASHSIZE_ADDR), FLASHSIZE_ADDR=$(FLASHSIZE_ADDR),) \
	$(if $(PACKAGEID_ADDR), PACKAGEID_ADDR=$(PACKAGEID_ADDR),)

FLASH_PAGE = $(strip \
	$(if $(filter 0, $(MPU_SERIE)), \
		$(if $(filter 4 6 8, $(MPU_MEMCODE)),  1) \
		$(if $(filter B C,   $(MPU_MEMCODE)),  2) \
	) \
	$(if $(filter 4, $(MPU_SERIE)),  128) \
)
FLASH_SIZE = $(strip \
	$(if $(filter 4, $(MPU_MEMCODE)), 16) \
	$(if $(filter 6, $(MPU_MEMCODE)), 32) \
	$(if $(filter 8, $(MPU_MEMCODE)), 64) \
	$(if $(filter B, $(MPU_MEMCODE)), 128) \
	$(if $(filter C, $(MPU_MEMCODE)), 256) \
	$(if $(filter E, $(MPU_MEMCODE)), 512) \
	$(if $(filter G, $(MPU_MEMCODE)), 1024) \
)
RAM_SIZE = $(strip \
	$(if $(filter 03%, $(MPU_MODEL)), \
		$(if $(filter 4 6, $(MPU_MEMCODE)), 4) \
		$(if $(filter 8,   $(MPU_MEMCODE)), 8) \
		$(if $(filter C,   $(MPU_MEMCODE)), 32) \
	) \
	$(if $(filter 04%, $(MPU_MODEL)), 6) \
	$(if $(filter 05%, $(MPU_MODEL)), 8) \
	$(if $(filter 07%, $(MPU_MODEL)), \
		$(if $(filter 6,   $(MPU_MEMCODE)), 6) \
		$(if $(filter 8 B, $(MPU_MEMCODE)), 16) \
	) \
	$(if $(filter 09%, $(MPU_MODEL)), 32) \
	$(if $(filter 401, $(MPU_MODEL)), \
		$(if $(filter B C, $(MPU_MEMCODE)), 64) \
		$(if $(filter D E, $(MPU_MEMCODE)), 96) \
	) \
	$(if $(filter 405 407 446 41%, $(MPU_MODEL)), 128) \
	$(if $(filter 42%, $(MPU_MODEL)),   192) \
)
MPU_DEVID = $(strip \
	$(if $(filter 03%,     $(MPU_MODEL)), \
		$(if $(filter 4 6,  $(MPU_MEMCODE)), 0x0444) \
		$(if $(filter 8,    $(MPU_MEMCODE)), 0x0440) \
		$(if $(filter C,    $(MPU_MEMCODE)), 0x0442) \
	) \
	$(if $(filter 04%,     $(MPU_MODEL)), 0x0445) \
	$(if $(filter 05%,     $(MPU_MODEL)), 0x0440) \
	$(if $(filter 070,     $(MPU_MODEL)), \
		$(if $(filter 6,    $(MPU_MEMCODE)), 0x0445) \
		$(if $(filter B,    $(MPU_MEMCODE)), 0x0448) \
	) \
	$(if $(filter 071 072, $(MPU_MODEL)), 0x0448) \
	$(if $(filter 09%,     $(MPU_MODEL)), 0x0442) \
	$(if $(filter 401,     $(MPU_MODEL)), \
		$(if $(filter B C,  $(MPU_MEMCODE)), 0x423) \
		$(if $(filter D E,  $(MPU_MEMCODE)), 0x433) \
	) \
	$(if $(filter 405 407 417 417, $(MPU_MODEL)), 0x0413) \
	$(if $(filter 410, $(MPU_MODEL)),     0x0458) \
	$(if $(filter 411, $(MPU_MODEL)),     0x0431) \
	$(if $(filter 412, $(MPU_MODEL)),     0x0441) \
	$(if $(filter 413 423, $(MPU_MODEL)), 0x0463) \
	$(if $(filter 42% 43%, $(MPU_MODEL)), 0x0419) \
	$(if $(filter 446, $(MPU_MODEL)),     0x0421) \
	$(if $(filter 469 479, $(MPU_MODEL)), 0x0434) \
)

MPU_PINS_COUNT = $(strip \
	$(if $(filter F, $(MPU_PINCODE)), 20) \
	$(if $(filter G, $(MPU_PINCODE)), 28) \
	$(if $(filter K, $(MPU_PINCODE)), 32) \
	$(if $(filter T, $(MPU_PINCODE)), 36) \
	$(if $(filter C, $(MPU_PINCODE)), 48) \
	$(if $(filter R, $(MPU_PINCODE)), 64) \
	$(if $(filter O, $(MPU_PINCODE)), 90) \
	$(if $(filter V, $(MPU_PINCODE)), 100) \
	$(if $(filter Z, $(MPU_PINCODE)), 144) \
	$(if $(filter I, $(MPU_PINCODE)), 176) \
)

DEFINES += \
	FLASH_PAGE=$(FLASH_PAGE) \
	FLASH_SIZE=$(FLASH_SIZE) \
	RAM_SIZE=$(RAM_SIZE) \
	MPU_PINS_COUNT=$(MPU_PINS_COUNT)

LD_MAP = $(strip \
	$(if $(filter STM32F0%, $(MPU)),   src/hw/stm32f0.ld,) \
	$(if $(filter STM32F4%, $(MPU)),   src/hw/stm32f4.ld,) \
)
.SECONDARY: $(LD_MAP)

INCLUDE_PATH += src/hw

SRC += $(addprefix src/hw/, \
		debug.cpp \
)


SRC += $(addprefix src/hw/, \
	mcu/mcu.cpp \
	mcu/hw_clk.cpp \
)

endif # MPU

include src/hw/devices/Makefile.mk
