#ifndef HW_SDIO_H_INCLUDED
#define HW_SDIO_H_INCLUDED

#include "interfaces/notify.h"
#include <stdint.h>

class CHW_SDIO
{
public:
	static void Init();


public: // Interrupts
	static void OnSDIO();
	static void OnDMA();

private:
	bool SdPowerOn();
	bool SdPowerOff();
	bool GetCmdError();
private:
	static bool               m_bSDIOPresent;
};

#endif /* HW_SDIO_H_INCLUDED */
