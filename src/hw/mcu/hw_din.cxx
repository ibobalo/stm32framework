// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define DINx_PORT          A,5
// /* optional defines: */
// #define DINx_TRIGGER       Falling
// #define DINx_IT_PRIO       1,0
// #include "hw_din.cxx"
// IDigitalIn* getDInInterfaceA5() {CHW_DIN_A5::Init(false, false); return &g_HW_DIN_A5;};

#if !defined(DINx_PORT)
#error params must be defined : DINx_PORT
//next defines for editor hints
#define DINx_PORT          A,5
#define DINx_TRIGGER       Falling
#define DINx_IT_PRIO       1,0
#endif

#include "stdafx.h"
#include "hw_din.h"
#include "../hw_macros.h"
#include "../debug.h"

#define CHW_DINx_EXTI CHW_DIN_EXTI<CC(GPIOPort,FIRST(DINx_PORT)),SECOND(DINx_PORT)>

#ifdef DINx_TRIGGER
#warning DINx_TRIGGER is deprecated. use DINx_IT_PRIO and params of SetNotifier() instead

#ifndef DINx_IT_PRIO
#define DINx_IT_PRIO 0, 0
#endif
#endif

#ifdef DINx_IT_PRIO

/**************************************************************************************
 * ISR (interrupts handling
 *************************************************************************************/
#if defined(STM32F0) && (SECOND(DINx_PORT)>=0) && (SECOND(DINx_PORT)<=1)
#  define EXTIx_ID EXTI0_1
#elif defined(STM32F0) && (SECOND(DINx_PORT)>=2) && (SECOND(DINx_PORT)<=3)
#  define EXTIx_ID EXTI2_3
#elif defined(STM32F0) && (SECOND(DINx_PORT)>=4) && (SECOND(DINx_PORT)<=15)
#  define EXTIx_ID EXTI4_15
#elif defined(STM32F4) && (SECOND(DINx_PORT)==0)
#  define EXTIx_ID EXTI0
#elif defined(STM32F4) && (SECOND(DINx_PORT)==1)
#  define EXTIx_ID EXTI1
#elif defined(STM32F4) && (SECOND(DINx_PORT)==2)
#  define EXTIx_ID EXTI2
#elif defined(STM32F4) && (SECOND(DINx_PORT)==3)
#  define EXTIx_ID EXTI3
#elif defined(STM32F4) && (SECOND(DINx_PORT)==4)
#  define EXTIx_ID EXTI4
#elif defined(STM32F4) && (SECOND(DINx_PORT)>=5) && (SECOND(DINx_PORT)<=9)
#  define EXTIx_ID EXTI9_5
#elif defined(STM32F4) && (SECOND(DINx_PORT)>=10) && (SECOND(DINx_PORT)<=15)
#  define EXTIx_ID EXTI15_10
#else
#  error undefined EXTI
#endif

// avoid , macro eval
#define CHW_EXTIx CCC(CHW_EXTI_,FIRST(DINx_PORT),SECOND(DINx_PORT))
typedef CHW_DINx_EXTI CHW_EXTIx;

REDIRECT_ISRID(CC(EXTIx_ID,_IRQ), CHW_EXTIx::OnEXTI);

#endif // DINx_IT_PRIO

#undef EXTIx_ID
#undef CHW_DINx_EX
#undef DINx_IT_PRIO
#undef DINx_TRIGGER
#undef DINx_PORT
