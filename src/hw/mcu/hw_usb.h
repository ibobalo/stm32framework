#ifndef HW_USB_H_INCLUDED
#define HW_USB_H_INCLUDED

#include "hw_uart.h"
#include "util/callback.h"
#include <stdint.h>

#define USB_VCP_COUNT 1

// friend fns
#ifdef __cplusplus
extern "C" {
#endif
void USBD_USR_DeviceConfigured(void);
void USBD_USR_DeviceSuspended(void);
void USBD_USR_DeviceResumed(void);
void USBD_USR_DeviceConnected(void);
void USBD_USR_DeviceDisconnected(void);
uint16_t VCP_DataRxHandler(uint8_t*, uint32_t);
uint16_t VCP_Ctrl(uint32_t Cmd, uint8_t* Buf, uint32_t Len);
#ifdef __cplusplus
} // extern "C" {
#endif


class CHW_USB
{
public:
	static void Init();
	static ISerial* AcquireUSBSerialInterface(unsigned uiIndex);

protected: // Interrupts and callback
	friend void USBD_USR_DeviceConfigured(void);
	static void OnUSBDeviceConfigured();

	friend void USBD_USR_DeviceSuspended(void);
	static void OnUSBDeviceSuspended();

	friend void USBD_USR_DeviceResumed(void);
	static void OnUSBDeviceResumed();

	friend void USBD_USR_DeviceConnected(void);
	static void OnUSBDeviceConnected();

	friend void USBD_USR_DeviceDisconnected(void);
	static void OnUSBDeviceDisconnected();

	friend uint16_t VCP_DataRxHandler(uint8_t*, uint32_t);
	static void OnUSBDataReceived(const void* pData, unsigned uiLength);

	friend uint16_t VCP_Ctrl(uint32_t Cmd, uint8_t* Buf, uint32_t Len);
	static void OnUsbVcpParamSet(unsigned uiBitrate, ISerial::StopBits eStopBits, ISerial::ParityBits eParityBits);
private:
};

#endif /* HW_USB_H_INCLUDED */
