// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define UxART       USART
// #define UxARTx_N        2
// #define UxARTx_PORT_TX  D,5
// #define UxARTx_PORT_RX  D,6,AF_1
// #define UxARTx_PORT_RTS D,7 /* optional 485 */
// #define UxARTx_IT_PRIO  2,0
// //next defines is optional for DMA access
// #define UxARTx_DMA_TX           1,6,4
// #define UxARTx_DMA_RX           1,5,4
// #include "hw_uart.cxx"
// ISerial* getUSARTInterface1() {return &g_HW_USART2;};

#if !defined(UxART) || !defined(UxARTx_N) || \
	!defined(UxARTx_PORT_TX) || \
	!defined(UxARTx_PORT_RX) || \
	!defined(UxARTx_IT_PRIO)
#error params must be defined : UxART, UxARTx_N, UxARTx_PORT_TX, UxARTx_PORT_RX, UxARTx_IT_PRIO
// next defines for editor hints
#define UxART       USART
#define UxARTx_N        2
#define UxARTx_PORT_TX  D,5
#define UxARTx_PORT_RX  D,6,AF_1
#define UxARTx_PORT_RTS D,7
#define UxARTx_IT_PRIO  2,0
#define UxARTx_DMA_TX  1,6,4
#define UxARTx_DMA_RX  1,5,4
#endif

#if defined(UxARTx_IT_GROUP_PRIO) || \
	defined(UxARTx_IT_SUBPRIO)
# error UxARTx_IT_GROUP_PRIO, UxARTx_IT_SUBPRIO is obsolete.
# pragma message("Hint: use")
# pragma message("#define UxARTx_IT_PRIO   PRIO,SUBPRIO")
#endif

#if defined(UxARTx_DMA_N) || \
	defined(UxARTx_DMA_TX_STREAM_N) || \
	defined(UxARTx_DMA_TX_CHANNEL_N) || \
	defined(UxARTx_DMA_RX_STREAM_N) || \
	defined(UxARTx_DMA_RX_CHANNEL_N)
# error UxARTx_DMA_N, UxARTx_DMA_TX_STREAM_N, UxARTx_DMA_TX_CHANNEL_N, UxARTx_DMA_RX_STREAM_N, UxARTx_DMA_RX_CHANNEL_N is obsolete.
# pragma message("Hint: use")
# pragma message("#define UxARTx_DMA_TX   DMA,STREAM,CHANNEL")
# pragma message("#define UxARTx_DMA_RX   DMA,STREAM,CHANNEL")
#endif

#include "hw_uart.h"
#include "hw/hw_macros.h"
#include "util/core.h"
#include "util/utils.h"
#include "hw/debug.h"
#include <stdint.h>
#include <stddef.h>

//#define USART_DEBUG_INFO(...) DEBUG_INFO("USART" STR(UxARTx_N) ":" __VA_ARGS__)
//#define USARTTX_DEBUG_INFO(...) DEBUG_INFO("USART" STR(UxARTx_N) ":TX:" __VA_ARGS__)
//#define USARTRX_DEBUG_INFO(...) DEBUG_INFO("USART" STR(UxARTx_N) ":RX:" __VA_ARGS__)
#define USART_DEBUG_INFO(...) {}
#define USARTTX_DEBUG_INFO(...) {}
#define USARTRX_DEBUG_INFO(...) {}

#ifdef DEBUG
#define CHECK_UNEXPECTED_EVENTS
#endif

# define UxARTx_IRQ  CCC(UxART,UxARTx_N,_IRQ)
#if defined(STM32F0) && ((UxARTx_N == 3) || (UxARTx_N == 4))
#ifndef ISR_SHARED_HOST_USART3_4_IRQ_IMPLEMENTED
#define ISR_SHARED_HOST_USART3_4_IRQ_IMPLEMENTED
IMPLEMENT_ISR_SHARED_HOST(USART3_4_IRQ)
#endif // ISR_SHARED_HOST_USART3_4_IRQ_IMPLEMENTED
# define UxARTx_IRQ_HOST USART3_4_IRQ
# define UxARTx_IRQHandler CC(UxARTx_IRQ,SubHandler)
#else
# define UxARTx_IRQn CC(UxARTx_IRQ,n)
# define UxARTx_IRQHandler CC(UxARTx_IRQ,Handler)
#endif
#define USARTx CC(UxART,UxARTx_N)
#define g_HW_USARTx CCC(g_HW_,UxART,UxARTx_N)

#ifndef USART_STATICS_DEFINED
#define USART_STATICS_DEFINED
static constexpr unsigned g_auiUSART_BaudRates[] = {
		9600, 115200, 19200, 57600, 38400, 28800 //, 230400, 460800, 921600, 1843200, 3686400
};
static const uint16_t stopBitsDecode[] = {IF_STM32F4(USART_StopBits_0_5,) USART_StopBits_1, USART_StopBits_2, USART_StopBits_1_5};
static const uint16_t parityBitDecode[] = {USART_Parity_No, USART_Parity_Even, USART_Parity_Odd};

template<unsigned USART_N> bool        CHW_USARTx<USART_N>::m_bEnabled;
template<unsigned USART_N> TSerialConnectionParams  CHW_USARTx<USART_N>::m_tConnectionParams;

template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_uiTxByteTimeoutTicks;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_uiRxByteTimeoutTicks;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_uiAddress;
template<unsigned USART_N> CConstChunk CHW_USARTx<USART_N>::m_TxChunk;
//template<unsigned USART_N> bool        CHW_USARTx<USART_N>::m_bTxDma;
template<unsigned USART_N> bool        CHW_USARTx<USART_N>::m_bTxDmaInited;
template<unsigned USART_N> CIoChunk    CHW_USARTx<USART_N>::m_RxChunk;
template<unsigned USART_N> bool        CHW_USARTx<USART_N>::m_bRxDma;
template<unsigned USART_N> bool        CHW_USARTx<USART_N>::m_bRxDmaInited;
template<unsigned USART_N> bool        CHW_USARTx<USART_N>::m_bAutoBaudrate;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_nAutoBaudrateIndex;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_nRxErrors;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_nRxErrorsTreshold;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_nTxTimeoutCountdown;
template<unsigned USART_N> unsigned    CHW_USARTx<USART_N>::m_nRxTimeoutCountdown;
template<unsigned USART_N> ISerialRxNotifier* CHW_USARTx<USART_N>::m_pRxNotifier;
template<unsigned USART_N> ISerialTxNotifier* CHW_USARTx<USART_N>::m_pTxNotifier;
template<unsigned USART_N> ISerialConnectionParamsNotifier* CHW_USARTx<USART_N>::m_pParamsNotifier;
template<unsigned USART_N> typename CHW_USARTx<USART_N>::RS485FsmState  CHW_USARTx<USART_N>::m_eFSMState;
#endif // USART_STATICS_DEFINED

static CHW_USARTx<UxARTx_N> g_HW_USARTx;

template< > void CHW_USARTx<UxARTx_N>::Init();
template< > void CHW_USARTx<UxARTx_N>::Deinit();
template< > void CHW_USARTx<UxARTx_N>::SetSerialRxNotifier(ISerialRxNotifier* pNotifier);
template< > void CHW_USARTx<UxARTx_N>::SetSerialTxNotifier(ISerialTxNotifier* pNotifier);
template< > void CHW_USARTx<UxARTx_N>::SetConnectionParams(const TSerialConnectionParams* pParams);
template< > unsigned CHW_USARTx<UxARTx_N>::GetBaudrate();
template< > void CHW_USARTx<UxARTx_N>::SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier);
template< > void CHW_USARTx<UxARTx_N>::DoBaudrate(bool bReenable);
template< > bool CHW_USARTx<UxARTx_N>::StartDataSend(const CConstChunk& TxChunk);
template< > void CHW_USARTx<UxARTx_N>::StartDataRecv(const CIoChunk& RxChunk);
template< > void CHW_USARTx<UxARTx_N>::Control(bool bEnable);
template< > void CHW_USARTx<UxARTx_N>::ControlRx(bool bEnable);
template< > void CHW_USARTx<UxARTx_N>::ControlTx(bool bEnable);
template< > void CHW_USARTx<UxARTx_N>::SetRxByteTimeout(unsigned uiByteTimeoutTicks);
template< > void CHW_USARTx<UxARTx_N>::SetTxByteTimeout(unsigned uiByteTimeoutTicks);
template< > void CHW_USARTx<UxARTx_N>::OnSysTick();
template< > void CHW_USARTx<UxARTx_N>::OnUSART();
template< > void CHW_USARTx<UxARTx_N>::Init_USARTx();
template< > void CHW_USARTx<UxARTx_N>::Init_USARTx_IRQ();
template< > void CHW_USARTx<UxARTx_N>::Init_USARTx_DMA();
template< > void CHW_USARTx<UxARTx_N>::Cancel_USARTx();
template< > void CHW_USARTx<UxARTx_N>::DoRXByte(uint8_t uiByteReceived);
template< > void CHW_USARTx<UxARTx_N>::DoRXPart(unsigned uiBytesLeft);
template< > void CHW_USARTx<UxARTx_N>::DoRXDone();
template< > void CHW_USARTx<UxARTx_N>::DoRXIdle(unsigned uiBytesLeft);
template< > void CHW_USARTx<UxARTx_N>::DoRXError(unsigned uiBytesLeft);
template< > void CHW_USARTx<UxARTx_N>::DoRXTimeout(unsigned uiBytesLeft);
template< > void CHW_USARTx<UxARTx_N>::DoTXByte();
template< > void CHW_USARTx<UxARTx_N>::DoTXDone();
template< > void CHW_USARTx<UxARTx_N>::DoTXComplete();
template< > void CHW_USARTx<UxARTx_N>::DoTXError(unsigned uiBytesLeft);
template< > void CHW_USARTx<UxARTx_N>::DoTXTimeout(unsigned uiBytesLeft);
template< > void CHW_USARTx<UxARTx_N>::DoStartRx();
template< > void CHW_USARTx<UxARTx_N>::DoStartTx();
template< > void CHW_USARTx<UxARTx_N>::DoContinueRx();
template< > void CHW_USARTx<UxARTx_N>::DoContinueTx();
template< > void CHW_USARTx<UxARTx_N>::DoPrepareTXDMA();
template< > void CHW_USARTx<UxARTx_N>::OnDMATX();
template< > void CHW_USARTx<UxARTx_N>::DoPrepareRXDMA();
template< > void CHW_USARTx<UxARTx_N>::OnDMARX();
template< > void CHW_USARTx<UxARTx_N>::Deinit_USARTx();
template< > void CHW_USARTx<UxARTx_N>::SwitchToTx();
template< > void CHW_USARTx<UxARTx_N>::SwitchToRx();


template< >
void CHW_USARTx<UxARTx_N>::Init()
{
	m_bEnabled = false;
	m_uiTxByteTimeoutTicks = 0;
	m_uiRxByteTimeoutTicks = 0;
	m_uiAddress = 0;
	m_TxChunk.Clean();
	m_RxChunk.Clean();
	m_bAutoBaudrate = false;
	m_nAutoBaudrateIndex = 1;
	m_nRxErrors = 0;
	m_nRxErrorsTreshold = 10;
	m_nRxTimeoutCountdown = 0;

	Init_USARTx();
	ControlRx(false);
	ControlTx(false);

	TSerialConnectionParams cpar = {9600, TSerialConnectionParams::stop1, TSerialConnectionParams::parityNo};
	SetConnectionParams(&cpar);

	CHW_MCU::RegisterSysTickCallback(&g_HW_USARTx);

#ifdef UxARTx_PORT_RTS
	INIT_GPIO_OUT(FIRST(UxARTx_PORT_RTS), SECOND(UxARTx_PORT_RTS), 0);
	SwitchToRx();
#endif
}

template< >
void CHW_USARTx<UxARTx_N>::Deinit()
{
	CHW_MCU::UnregisterSysTickCallback(&g_HW_USARTx);
	Deinit_USARTx();

#ifdef UxARTx_PORT_RTS
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(UxARTx_PORT_RTS)); /* Deinitialize RS485 RTS Pin */
	GPIO_Init(CC(GPIO,FIRST(UxARTx_PORT_RTS)), &GPIO_InitStructure);
#endif
}

template< >
void CHW_USARTx<UxARTx_N>::SetSerialRxNotifier(ISerialRxNotifier* pNotifier)
{
	ASSERTE(pNotifier);
	m_pRxNotifier = pNotifier;
}

template< >
void CHW_USARTx<UxARTx_N>::SetSerialTxNotifier(ISerialTxNotifier* pNotifier)
{
	ASSERTE(pNotifier);
	m_pTxNotifier = pNotifier;
}

template< >
void CHW_USARTx<UxARTx_N>::SetConnectionParams(const TSerialConnectionParams* pParams)
{
	if (pParams->uiBaudRate != 0) {
		m_bAutoBaudrate = false;
		m_nAutoBaudrateIndex = 0;
		m_tConnectionParams.uiBaudRate = pParams->uiBaudRate;
	} else {
		m_bAutoBaudrate = true;
		m_nRxErrors = 0;
	}
	m_tConnectionParams.eStopBits = pParams->eStopBits;
	m_tConnectionParams.eParityBits = pParams->eParityBits;

	DoBaudrate(false);
}

template< >
unsigned CHW_USARTx<UxARTx_N>::GetBaudrate()
{
	return m_tConnectionParams.uiBaudRate;
}

template< >
void CHW_USARTx<UxARTx_N>::SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier)
{
	ASSERTE(pNotifier);
	m_pParamsNotifier = pNotifier;
}

template< >
void CHW_USARTx<UxARTx_N>::DoBaudrate(bool bReenable)
{
	ASSERTE((unsigned)m_tConnectionParams.eStopBits < ARRAY_SIZE(stopBitsDecode));
	ASSERTE((unsigned)m_tConnectionParams.eParityBits < ARRAY_SIZE(parityBitDecode));

	/* Disable USARTx Device */
	USART_Cmd(USARTx, DISABLE);
	USART_DEBUG_INFO("Disabled for change params");

	if (m_bAutoBaudrate) {
		++m_nAutoBaudrateIndex;
		m_nAutoBaudrateIndex %= ARRAY_SIZE(g_auiUSART_BaudRates);
		m_tConnectionParams.uiBaudRate = g_auiUSART_BaudRates[m_nAutoBaudrateIndex];
	}
	/* Initialize USARTx device with parameters */
	USART_InitTypeDef USART_InitStructure;
	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate = m_tConnectionParams.uiBaudRate;
	USART_InitStructure.USART_WordLength = (m_tConnectionParams.eParityBits == TSerialConnectionParams::ParityBits::parityNo) ? USART_WordLength_8b : USART_WordLength_9b;
	USART_InitStructure.USART_StopBits = stopBitsDecode[m_tConnectionParams.eStopBits];
	USART_InitStructure.USART_Parity = parityBitDecode[m_tConnectionParams.eParityBits];
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USARTx->CR1 & (USART_Mode_Rx | USART_Mode_Tx); // do not change
	if (!USART_InitStructure.USART_Mode) USART_InitStructure.USART_Mode = USART_Mode_Rx;
	USART_Init(USARTx, &USART_InitStructure);

	m_nRxErrors = 0;
	USART_DEBUG_INFO("USARTx Baudrate. =nErrs:", m_nRxErrors);

	if (m_pParamsNotifier) m_pParamsNotifier->NotifyConnectionParams(&m_tConnectionParams);

	if (bReenable) {
		USART_Cmd(USARTx, ENABLE);
		USART_DEBUG_INFO("Enabled after change params");
	}
}

template< >
bool CHW_USARTx<UxARTx_N>::StartDataSend(const CConstChunk& TxChunk)
{
	ASSERTE(TxChunk.pData);
	ASSERTE(TxChunk.uiLength);

	SwitchToTx();

	if (m_TxChunk.uiLength != 0) {
		USARTTX_DEBUG_INFO("busy... len:", TxChunk.uiLength);
		return false;
	}
	USARTTX_DEBUG_INFO("starting... len:", TxChunk.uiLength);

	m_TxChunk = TxChunk;

	DoStartTx();
	return true;
}

template< >
void CHW_USARTx<UxARTx_N>::StartDataRecv(const CIoChunk& RxChunk)
{
	ASSERTE(USARTx);

	ASSERTE(m_RxChunk.uiLength == 0);
	if (m_uiRxByteTimeoutTicks) {
		m_nRxTimeoutCountdown = m_uiRxByteTimeoutTicks + 1;
	}
	m_RxChunk = RxChunk;

	DoStartRx();
}

template< >
void CHW_USARTx<UxARTx_N>::Control(bool bEnable)
{
	if (bEnable) {
		/* Enable USARTx Device */
		USART_Cmd(USARTx, ENABLE);
		USART_DEBUG_INFO("Enabbled by control");
	} else {
		USART_Cmd(USARTx, DISABLE);
		USART_DEBUG_INFO("Disabled by control");
	}
	if (!m_bEnabled && bEnable) SwitchToRx();
	m_bEnabled = bEnable;
}

template< >
void CHW_USARTx<UxARTx_N>::ControlRx(bool bEnable)
{
	if (bEnable) {
		USARTRX_DEBUG_INFO("Enabled");
		USART_GetFlagStatus(USARTx, USART_FLAG_RXNE); USART_ReceiveData(USARTx); // for clear flags
		USART_ITConfig(USARTx, USART_IT_IDLE, ENABLE);
		USART_ITConfig(USARTx, USART_IT_ERR, ENABLE);
//#ifdef UxARTx_DMA_RX
//		DMA_ITConfig(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), DMA_IT_TC | DMA_IT_TE, ENABLE);
//#endif // UxARTx_DMA_RX
		USARTx->CR1 |= USART_Mode_Rx;
	} else {
#ifdef UxARTx_DMA_RX
			USARTRX_DEBUG_INFO("DMA 0 left:", DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX))));
#endif // UxARTx_DMA_RX
		USARTRX_DEBUG_INFO("Disabled by control");
		USART_ITConfig(USARTx, USART_IT_RXNE, DISABLE);
		USART_ITConfig(USARTx, USART_IT_IDLE, DISABLE);
		USART_ITConfig(USARTx, USART_IT_ERR, DISABLE);
		USARTx->CR1 &= ~USART_Mode_Rx;
		m_RxChunk.Clean();
#ifdef UxARTx_DMA_RX
			USARTRX_DEBUG_INFO("DMA 1 left:", DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX))));
		USART_DMACmd(USARTx, USART_DMAReq_Rx, DISABLE);
			USARTRX_DEBUG_INFO("DMA 2 left:", DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX))));
		DMA_Cmd(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), DISABLE);
			USARTRX_DEBUG_INFO("DMA 3 left:", DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX))));
//		DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_RX)) | CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_RX)));
//		NVIC_ClearPendingIRQ(CCCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX),_IRQn));
//		DMA_ITConfig(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), DMA_IT_TC | DMA_IT_TE, DISABLE);
#endif // UxARTx_DMA_RX
	}
}

template< >
void CHW_USARTx<UxARTx_N>::ControlTx(bool bEnable)
{
	ASSERTE(USARTx);

	if (bEnable) {
		USARTTX_DEBUG_INFO("Enabled");
		USARTx->CR1 |= USART_Mode_Tx;
	} else {
		USARTTX_DEBUG_INFO("Disabled");
		USARTx->CR1 &= ~USART_Mode_Tx;
	}
}

template< >
void CHW_USARTx<UxARTx_N>::SetRxByteTimeout(unsigned uiByteTimeoutTicks)
{
	m_uiRxByteTimeoutTicks = uiByteTimeoutTicks;
	if (m_RxChunk.uiLength && m_uiRxByteTimeoutTicks) {
		// apply immediate
		m_nRxTimeoutCountdown = m_uiRxByteTimeoutTicks + 1;
	}
}

template< >
void CHW_USARTx<UxARTx_N>::SetTxByteTimeout(unsigned uiByteTimeoutTicks)
{
	m_uiTxByteTimeoutTicks = uiByteTimeoutTicks;
	if (m_TxChunk.uiLength && m_uiTxByteTimeoutTicks) {
		// apply immediate
		m_nTxTimeoutCountdown = m_uiTxByteTimeoutTicks + 1;
	}
}


template< >
void CHW_USARTx<UxARTx_N>::OnSysTick()
{
	IMPLEMENTS_INTERFACE_METHOD(ISysTickNotifee::OnSysTick());
	if (m_TxChunk.uiLength && m_nTxTimeoutCountdown) {
		--m_nTxTimeoutCountdown;
		if (!m_nTxTimeoutCountdown) {
			USARTTX_DEBUG_INFO("TX timeout");
			unsigned uiBytesLeft = m_TxChunk.uiLength;
			DoTXTimeout(uiBytesLeft);
		}
	}
	if (m_RxChunk.uiLength && m_nRxTimeoutCountdown) {
		--m_nRxTimeoutCountdown;
		if (!m_nRxTimeoutCountdown) {
			unsigned uiBytesLeft = m_RxChunk.uiLength;
			DoRXTimeout(uiBytesLeft);
		}
	}
}

template< >
void CHW_USARTx<UxARTx_N>::OnUSART()
{
	const uint32_t uiStatus = IF_STM32F0(USARTx->ISR) IF_STM32F4(USARTx->SR);
	uint16_t uiByteReceived = USART_ReceiveData(USARTx);
	const uint16_t uiCtrl = USARTx->CR1;
	const uint16_t uiCtrl2 = USARTx->CR2;
	const uint16_t uiCtrl3 = USARTx->CR3;
	USART_DEBUG_INFO("IRQ. SR:",  uiStatus);
	if (uiStatus & USART_FLAG_PE) USART_DEBUG_INFO("IRQ. SR: PE");
	if (uiCtrl   & USART_CR1_PEIE) USART_DEBUG_INFO("IRQ. PEIE");
	if (uiStatus & USART_FLAG_FE) USART_DEBUG_INFO("IRQ. SR: FE");
	if (uiStatus & USART_FLAG_NE) USART_DEBUG_INFO("IRQ. SR: NE");
	if (uiStatus & USART_FLAG_ORE) USART_DEBUG_INFO("IRQ. SR: ORE");
	if (uiCtrl3  & USART_CR3_EIE) USART_DEBUG_INFO("IRQ. IDLEIE");
	if (uiStatus & USART_FLAG_IDLE) USART_DEBUG_INFO("IRQ. SR: IDLE");
	if (uiCtrl   & USART_CR1_IDLEIE) USART_DEBUG_INFO("IRQ. IDLEIE");
	if (uiStatus & USART_FLAG_RXNE) USART_DEBUG_INFO("IRQ. SR: RXNE");
	if (uiCtrl   & USART_CR1_RXNEIE) USART_DEBUG_INFO("IRQ. RXNEIE");
	if (uiStatus & USART_FLAG_TC) USART_DEBUG_INFO("IRQ. SR: TC");
	if (uiCtrl   & USART_CR1_TCIE) USART_DEBUG_INFO("IRQ. TCIE");
	if (uiStatus & USART_FLAG_TXE) USART_DEBUG_INFO("IRQ. SR: TXE");
	if (uiCtrl   & USART_CR1_TXEIE) USART_DEBUG_INFO("IRQ. TXEIE");
	if (uiStatus & USART_FLAG_LBD) USART_DEBUG_INFO("IRQ. SR: LBD");
	if (uiCtrl2  & USART_CR2_LBDIE) USART_DEBUG_INFO("IRQ. LBDIE");
	if (uiStatus & USART_FLAG_CTS) USART_DEBUG_INFO("IRQ. SR: CTS");
	if (uiCtrl3  & USART_CR3_CTSIE) USART_DEBUG_INFO("IRQ. CTSIE");
	IS_EVENT_HANDLER;

	do {
		if (m_bRxDma) break;
		if (uiStatus & (USART_FLAG_NE | USART_FLAG_FE | USART_FLAG_PE)) {
			// Noise error, Framing error, Parity Error
			DEBUG_INFO("USART" STR(UxARTx_N) " error, reason:", uiStatus & (USART_FLAG_NE | USART_FLAG_FE | USART_FLAG_PE));
			unsigned uiBytesLeft = m_RxChunk.uiLength;
			DoRXError(uiBytesLeft);
			MARK_EVENT_IS_HANDLED;
			__DSB(); // Data Synchronization Barrier. Ensure USART_IT_RXNE status cleared and isr not trigger again
			break;
		}
#ifndef UxARTx_DMA_RX
		if (uiStatus & USART_FLAG_RXNE) {
			// Read Data register not empty interrupt
			// interrupt status cleared by read DR
			USARTRX_DEBUG_INFO("IRQ: RXNE. nErrs:", m_nRxErrors);
			DoRXByte(uiByteReceived);
			IF_STM32F0(USART_ClearFlag(USARTx, USART_FLAG_IDLE));
			IF_STM32F4(); // interrupt status cleared by read SR followed by a read DR
			USART_ITConfig(USARTx, USART_IT_IDLE, ENABLE);
			MARK_EVENT_IS_HANDLED;
			__DSB(); // Data Synchronization Barrier. Ensure USART_IT_RXNE status cleared and isr not trigger again
		}
#else
		if (uiByteReceived) {} // unused warning
#endif // UxARTx_DMA_RX
		if (uiStatus & USART_FLAG_ORE) {
			// OverRun Error interrupt if the EIE bit is set
			IF_STM32F0(USART_ClearFlag(USARTx, USART_FLAG_ORE)); // is cleared by a software, writing 1 to the ORECF
			IF_STM32F4(); // interrupt status cleared by read SR followed by a read DR
			DEBUG_INFO("USART" STR(UxARTx_N) " error: OVERFLOW");
#ifdef UxARTx_DMA_RX
			unsigned uiBytesLeft = DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)));
#else
			unsigned uiBytesLeft = m_RxChunk.uiLength;
#endif
			DoRXError(uiBytesLeft);
			MARK_EVENT_IS_HANDLED;
			__DSB(); // Data Synchronization Barrier. Ensure USART_IT_ER,USART_IT_RX status cleared and isr not trigger again
			break;
		}
		if (uiStatus & USART_FLAG_IDLE && uiCtrl & USART_CR1_IDLEIE) {
			// Idle line interrupt
			// interrupt status cleared by read SR followed by a read DR
			USARTRX_DEBUG_INFO("IRQ: IDLE");
			USART_ITConfig(USARTx, USART_IT_IDLE, DISABLE); // react only to first IDLE
			unsigned uiBytesLeft = m_RxChunk.uiLength;
			DoRXIdle(uiBytesLeft);
			MARK_EVENT_IS_HANDLED;
			__DSB(); // Data Synchronization Barrier. Ensure USART_IT_IDLE status cleared and isr not trigger again
		}
	} while (false);

#ifndef UxARTx_DMA_TX
	do {
		USART_DEBUG_INFO("IRQ. CR1:", uiCtrl);
//		USART_DEBUG_INFO("IRQ. CR2:", USARTx->CR2);
//		USART_DEBUG_INFO("IRQ. CR3:", USARTx->CR3);
//		if (m_bTxDma) break;
		if (uiStatus & USART_FLAG_TXE) {
			if (uiCtrl & USART_CR1_TXEIE) {
				// Transmission data Register Empty
				// interrupt status cleared by write to DR
				USARTTX_DEBUG_INFO("IRQ: TXE");
				if (!m_TxChunk.uiLength) {
					DoTXDone();
				}
				if (m_TxChunk.uiLength) {
					DoTXByte();
					MARK_EVENT_IS_HANDLED;
					__DSB(); // Data Synchronization Barrier. Ensure USART_IT_TXE status cleared and isr not trigger again
					break;
				}
				// no data left, nothing to write to DR, no way to clear TXE interrupt status,
				// so disable further TXE interrupts and wait for TC
				USART_ITConfig(USARTx, USART_IT_TXE, DISABLE);
				USARTTX_DEBUG_INFO("USART_IT_TXE disabled");
				USART_ITConfig(USARTx, USART_IT_TC, ENABLE);
				USARTTX_DEBUG_INFO("USART_IT_TC enabled");
				MARK_EVENT_IS_HANDLED;
				__DSB(); // Data Synchronization Barrier. Ensure USART_IT_TXE status cleared and isr not trigger again
			}
			if ((uiStatus & USART_FLAG_TC) && (uiCtrl & USART_CR1_TCIE)) {
				// Transmission Complete
				// interrupt status cleared by write to DR or clear TC
				USARTTX_DEBUG_INFO("IRQ: TC");
				if (!m_TxChunk.uiLength) {
					// all done, nothing to write to DR, so disable further TC interrupts
					USART_ITConfig(USARTx, USART_IT_TC, DISABLE);
					USARTTX_DEBUG_INFO("USART_IT_TC disabled");
					DoTXComplete();
					if (m_TxChunk.uiLength) {
						USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
						USARTTX_DEBUG_INFO("USART_IT_TXE reenabled");
						DoTXByte(); // also clears TC flag
					} else {
						// clear TC flag
						USART_ClearFlag(USARTx, USART_FLAG_TC);
					}
					MARK_EVENT_IS_HANDLED;
					__DSB(); // Data Synchronization Barrier. Ensure USART_IT_TC interrupt disabled and isr not trigger again
					break;
				}

				// hm, TXE interrupt was missed
//				ASSERTE(USART_GetFlagStatus(USARTx, USART_FLAG_TXE));
				DoTXByte();
				MARK_EVENT_IS_HANDLED;
				__DSB(); // Data Synchronization Barrier. Ensure USART_IT_TC status cleared and isr not trigger again
			}
		}
	} while (false);
#endif // UxARTx_DMA_TX
#ifndef UxARTx_IRQ_HOST
	ASSERT_EVENT_IS_HANDLED("USART IRQ. SR:", IF_STM32F0(USARTx->ISR) IF_STM32F4(USARTx->SR));
#endif
	USART_DEBUG_INFO("IRQ done. SR:", IF_STM32F0(USARTx->ISR) IF_STM32F4(USARTx->SR));
}

template< >
void CHW_USARTx<UxARTx_N>::Init_USARTx()
{
	/* Initialize USARTx Clock */
#ifdef STM32F0
# if (UxARTx_N >= 2 && UxARTx_N <= 5)
	RCC_APB1PeriphResetCmd(CC(RCC_APB1Periph_USART,UxARTx_N), ENABLE);
	RCC_APB1PeriphResetCmd(CC(RCC_APB1Periph_USART,UxARTx_N), DISABLE);
	RCC_APB1PeriphClockCmd(CC(RCC_APB1Periph_USART,UxARTx_N), ENABLE);
# else
	RCC_APB2PeriphResetCmd(CC(RCC_APB2Periph_USART,UxARTx_N), ENABLE);
	RCC_APB2PeriphResetCmd(CC(RCC_APB2Periph_USART,UxARTx_N), DISABLE);
	RCC_APB2PeriphClockCmd(CC(RCC_APB2Periph_USART,UxARTx_N), ENABLE);
# endif
#endif
#ifdef STM32F4
# if ((UxARTx_N >= 2 && UxARTx_N <= 5) || UxARTx_N >= 7)
	RCC_APB1PeriphResetCmd(CCC(RCC_APB1Periph_,UxART,UxARTx_N), ENABLE);
	RCC_APB1PeriphResetCmd(CCC(RCC_APB1Periph_,UxART,UxARTx_N), DISABLE);
	RCC_APB1PeriphClockCmd(CCC(RCC_APB1Periph_,UxART,UxARTx_N), ENABLE);
# else
	RCC_APB2PeriphResetCmd(CCC(RCC_APB2Periph_,UxART,UxARTx_N), ENABLE);
	RCC_APB2PeriphResetCmd(CCC(RCC_APB2Periph_,UxART,UxARTx_N), DISABLE);
	RCC_APB2PeriphClockCmd(CCC(RCC_APB2Periph_,UxART,UxARTx_N), ENABLE);
# endif
#endif

	/* Initialize USARTx GPIO */
	INIT_GPIO_AF_FAST(FIRST(UxARTx_PORT_TX), SECOND(UxARTx_PORT_TX), THIRD(UxARTx_PORT_TX, CCC(AF_,UxART,UxARTx_N)));
	INIT_GPIO_AF_FAST(FIRST(UxARTx_PORT_RX), SECOND(UxARTx_PORT_RX), THIRD(UxARTx_PORT_RX, CCC(AF_,UxART,UxARTx_N)));

	Init_USARTx_IRQ();
#if defined(UxARTx_DMA_TX) || defined(UxARTx_DMA_TX)
	Init_USARTx_DMA();
#endif
}

template< >
void CHW_USARTx<UxARTx_N>::Init_USARTx_IRQ()
{
	/* Enable USART Interrupts */
	USART_ITConfig(USARTx, USART_IT_TXE, DISABLE);
	USARTTX_DEBUG_INFO("USART_IT_TXE disabled");
//	USART_ITConfig(USARTx, USART_IT_TC, DISABLE);
//	USARTTX_DEBUG_INFO("USART_IT_TC disabled");
	USART_ClearFlag(USARTx, USART_FLAG_TC);
	USART_ITConfig(USARTx, USART_IT_IDLE, DISABLE);
	USART_ITConfig(USARTx, USART_IT_RXNE, DISABLE);
	USART_ITConfig(USARTx, USART_IT_ERR, DISABLE);
#ifdef UxARTx_IRQn
	/* Enable the IRQ channel */
	NVIC_INIT(UxARTx_IRQn, FIRST(UxARTx_IT_PRIO), SECOND(UxARTx_IT_PRIO,0));
#else
#ifndef UxARTx_IRQ_HOST
#error UxARTx_IRQ_HOST undefined
#endif
	ISR_SHARED_NVIC_INIT(UxARTx_IRQ_HOST, UxARTx_IRQ, FIRST(UxARTx_IT_PRIO), SECOND(UxARTx_IT_PRIO,0));
#endif
}

#if defined(UxARTx_DMA_TX) || defined(UxARTx_DMA_TX)
template< >
void CHW_USARTx<UxARTx_N>::Init_USARTx_DMA()
{
	/* Initialize USARTx DMA Channels */
	/* Enable USARTx DMA clock */
# ifdef STM32F0
#  if defined(UxARTx_DMA_TX)
	RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_DMA,FIRST(UxARTx_DMA_TX)), ENABLE);
#  else
	RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_DMA,FIRST(UxARTx_DMA_RX)), ENABLE);
#  endif
# endif
# ifdef STM32F4
#  if defined(UxARTx_DMA_TX)
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(UxARTx_DMA_TX)), ENABLE);
#  else
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(UxARTx_DMA_RX)), ENABLE);
#  endif
# endif
	/* Initialize USARTx DMA Tx Stream */
	m_bTxDmaInited = false; // do it later - when TX op starts
//	m_bTxDma = false;
	/* Initialize USARTx DMA Rx Stream */
	m_bRxDmaInited = false; // do it later - when RX op starts
	m_bRxDma = false;

# ifdef UxARTx_DMA_TX
	NVIC_ClearPendingIRQ(CCCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX),_IRQn));
	/* Configure NVIC for DMA TX channel interrupt */
	NVIC_INIT(CCCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX),_IRQn), FIRST(UxARTx_IT_PRIO), SECOND(UxARTx_IT_PRIO,0));
# endif // UxARTx_DMA_TX
# ifdef UxARTx_DMA_RX
	NVIC_ClearPendingIRQ(CCCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX),_IRQn));
	/* Configure NVIC for DMA RX channel interrupt */
	NVIC_INIT(CCCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX),_IRQn), FIRST(UxARTx_IT_PRIO), SECOND(UxARTx_IT_PRIO,0));
# endif // UxARTx_DMA_RX
}
#endif

template< >
void CHW_USARTx<UxARTx_N>::Cancel_USARTx()
{
	USART_DEBUG_INFO("Cancel USART");

	/* Disable USART Interrupts - it would be enabled on comm starts */
	USART_ITConfig(USARTx, USART_IT_TXE, DISABLE);
	USARTTX_DEBUG_INFO("USART_IT_TXE disabled");
//	USART_ITConfig(USARTx, USART_IT_TC, DISABLE);
//	USART_ClearFlag(USARTx, USART_FLAG_TC);
//	USARTTX_DEBUG_INFO("USART_IT_TC disabled");
	USART_ITConfig(USARTx, USART_IT_IDLE, DISABLE);
	USART_ITConfig(USARTx, USART_IT_RXNE, DISABLE);
	USART_ITConfig(USARTx, USART_IT_ERR, DISABLE);
#ifdef UxARTx_DMA_TX
	DMA_ITConfig(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_TX)) | CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_TX)));
	NVIC_ClearPendingIRQ(CCCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX),_IRQn));
	m_bTxDmaInited = false;
//	m_bTxDma = false;
#endif
#ifdef UxARTx_DMA_RX
	DMA_ITConfig(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_RX)) | CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_RX)));
	NVIC_ClearPendingIRQ(CCCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX),_IRQn));
	m_bRxDmaInited = false;
	m_bRxDma = false;
#endif
#ifdef UxARTx_IRQn
	NVIC_ClearPendingIRQ(UxARTx_IRQn);
#endif
	USART_ReceiveData(USARTx);

	SwitchToRx();
}

template< >
void CHW_USARTx<UxARTx_N>::DoRXByte(uint8_t uiByteReceived)
{
	USARTRX_DEBUG_INFO("byte:", uiByteReceived);
	USARTRX_DEBUG_INFO("left:", m_RxChunk.uiLength);
	m_nRxErrors /= 2;
	USARTRX_DEBUG_INFO("-nErrs:", m_nRxErrors);
	if (m_RxChunk.uiLength) {
		ASSERTE(m_RxChunk.pData);
		*(uint8_t*)m_RxChunk.pData = uiByteReceived;
		--m_RxChunk.uiLength;
		m_RxChunk.pData = (uint8_t*)m_RxChunk.pData + 1;

		if (m_RxChunk.uiLength == 0) {
			DoRXDone();
		} else {
			DoRXPart(m_RxChunk.uiLength);
		}
	} else {
		CConstChunk ccData = {&uiByteReceived, 1};
		if (m_pRxNotifier) m_pRxNotifier->NotifyRxOverflow(ccData, &m_RxChunk);
	}
	if (m_uiRxByteTimeoutTicks) {
		m_nRxTimeoutCountdown = m_uiRxByteTimeoutTicks + 1;
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoRXPart(unsigned uiBytesLeft)
{
	USARTRX_DEBUG_INFO("part");
	bool bNext = (m_pRxNotifier ? m_pRxNotifier->NotifyRxPart(uiBytesLeft, &m_RxChunk) : true);
	if (bNext && m_RxChunk.uiLength) {
		DoContinueRx();
	} else {
		ControlRx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoRXDone()
{
	USARTRX_DEBUG_INFO("done");
	bool bNext = (m_pRxNotifier ? m_pRxNotifier->NotifyRxDone(&m_RxChunk) : false);
	if (bNext && m_RxChunk.uiLength) {
		DoContinueRx();
	} else {
		ControlRx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoRXIdle(unsigned uiBytesLeft)
{
	USARTRX_DEBUG_INFO("idle");
	bool bNext = (m_pRxNotifier ? m_pRxNotifier->NotifyRxIdle(uiBytesLeft, &m_RxChunk) : true);
	if (bNext && m_RxChunk.uiLength) {
		DoContinueRx();
	} else {
		ControlRx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoRXError(unsigned uiBytesLeft)
{
	USARTRX_DEBUG_INFO("+nErrs:", m_nRxErrors);
	++m_nRxErrors;
	if (m_nRxErrors > m_nRxErrorsTreshold && m_bAutoBaudrate) {
		USARTRX_DEBUG_INFO("errors over threshold:", m_nRxErrors);
		DoBaudrate(true);
	}
	m_RxChunk.Clean();
	bool bNext = (m_pRxNotifier ? m_pRxNotifier->NotifyRxError(uiBytesLeft, &m_RxChunk) : false);
	if (bNext && m_RxChunk.uiLength) {
		DoContinueRx();
	} else {
		ControlRx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoRXTimeout(unsigned uiBytesLeft)
{
	USARTRX_DEBUG_INFO("timeout. left:", uiBytesLeft);
	bool bNext = (m_pRxNotifier ? m_pRxNotifier->NotifyRxTimeout(uiBytesLeft, &m_RxChunk) : false);
	if (bNext && m_RxChunk.uiLength) {
		DoContinueRx();
	} else {
		ControlRx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoTXByte()
{
	ASSERTE(m_TxChunk.uiLength);
	ASSERTE(m_TxChunk.pData);

	uint8_t uiByteToSend = *(const uint8_t*)m_TxChunk.pData;
	m_TxChunk.pData = (uint8_t*)m_TxChunk.pData + 1;
	--m_TxChunk.uiLength;

	USARTTX_DEBUG_INFO("Txing byte:", uiByteToSend);
	USARTTX_DEBUG_INFO("Txing left:", m_TxChunk.uiLength);
	USART_SendData(USARTx, uiByteToSend);

	if (m_uiTxByteTimeoutTicks) {
		m_nTxTimeoutCountdown = m_uiTxByteTimeoutTicks + 1;
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoTXDone()
{
	USARTTX_DEBUG_INFO("TX done");
	m_TxChunk.Clean();
	bool bNext = (m_pTxNotifier ? m_pTxNotifier->NotifyTxDone(&m_TxChunk) : false);
	if (bNext || m_TxChunk.uiLength) {
		DoContinueTx();
//	} else {
//		ControlTx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoTXComplete()
{
	USARTTX_DEBUG_INFO("TX complete");
	m_TxChunk.Clean();
	SwitchToRx();
	bool bNext = (m_pTxNotifier ? m_pTxNotifier->NotifyTxComplete(&m_TxChunk) : false);
	if (bNext || m_TxChunk.uiLength) {
		DoContinueTx();
//	} else {
//		ControlTx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoTXError(unsigned uiBytesLeft)
{
	USARTTX_DEBUG_INFO("TX error");
	m_TxChunk.Clean();
	SwitchToRx();
	bool bNext = (m_pTxNotifier ? m_pTxNotifier->NotifyTxError(uiBytesLeft, &m_TxChunk) : false);
	if (bNext || m_TxChunk.uiLength) {
		DoContinueTx();
	} else {
		ControlTx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoTXTimeout(unsigned uiBytesLeft)
{
	USARTTX_DEBUG_INFO("TX timeout");
	m_TxChunk.Clean();
	SwitchToRx();
	bool bNext = (m_pTxNotifier ? m_pTxNotifier->NotifyTxTimeout(uiBytesLeft, &m_TxChunk) : false);
	if (bNext || m_TxChunk.uiLength) {
		USARTTX_DEBUG_INFO("continue");
	} else {
		ControlTx(false);
	}
}

template< >
void CHW_USARTx<UxARTx_N>::DoStartRx()
{
#ifdef UxARTx_DMA_RX
	// DMA
	DoPrepareRXDMA();
	USART_DMACmd(USARTx, USART_DMAReq_Rx, ENABLE);
	ControlRx(true);
#else
	// IRQ
	ControlRx(true);
	USARTRX_DEBUG_INFO("USART_IT_RXNE enable");
	USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
#endif
	USARTRX_DEBUG_INFO("Rx Start. len:", m_RxChunk.uiLength);
	USART_Cmd(USARTx, ENABLE);
	USARTRX_DEBUG_INFO("Enabled by Rx start");
}

template< >
void CHW_USARTx<UxARTx_N>::DoStartTx()
{
#ifdef UxARTx_DMA_TX
//	m_bTxDma = true;
	// DMA
	DoPrepareTXDMA();
	USART_Cmd(USARTx, ENABLE);
	USART_DMACmd(USARTx, USART_DMAReq_Tx, ENABLE);
	USART_ClearFlag(USARTx, USART_FLAG_TC);
	/* Enable DMA TX operation */
	DMA_Cmd(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), ENABLE);
	// continue in OnDMATX()
	ControlTx(true);
#else
	// IRQ
	ControlTx(true);
	DoTXByte();
	USARTTX_DEBUG_INFO("USART_IT_TXE enable");
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
//	USARTTX_DEBUG_INFO("USART_IT_TC enable");
//	USART_ITConfig(USARTx, USART_IT_TC, ENABLE);
	USART_Cmd(USARTx, ENABLE);
#endif

	USARTTX_DEBUG_INFO("Enabled by Tx start");
}

template< >
void CHW_USARTx<UxARTx_N>::DoContinueRx()
{
	USARTRX_DEBUG_INFO("continue. len:", m_RxChunk.uiLength);
#ifdef UxARTx_DMA_RX
	DoPrepareRXDMA();
#endif
}

template< >
void CHW_USARTx<UxARTx_N>::DoContinueTx()
{
	USARTTX_DEBUG_INFO("continue. len:", m_TxChunk.uiLength);
#ifdef UxARTx_DMA_TX
	DoPrepareTXDMA();
	USART_ClearFlag(USARTx, USART_FLAG_TC);
#endif
}

#ifdef UxARTx_DMA_TX
template< >
void CHW_USARTx<UxARTx_N>::DoPrepareTXDMA()
{
	ASSERTE(m_TxChunk.pData);
	ASSERTE(m_TxChunk.uiLength);
	if (m_bTxDmaInited) {
		DMA_MemoryTargetConfig(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), (uint32_t)m_TxChunk.pData, DMA_Memory_0);
		DMA_SetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), m_TxChunk.uiLength);
	} else {
		USARTTX_DEBUG_INFO("DMA init");
		DMA_InitTypeDef DMAInitStructure;
		DMA_StructInit(&DMAInitStructure);
		DMAInitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
		DMAInitStructure.DMA_Memory0BaseAddr = (uint32_t)m_TxChunk.pData;
		DMAInitStructure.DMA_BufferSize = m_TxChunk.uiLength;
		DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(USARTx->DR);
		DMAInitStructure.DMA_Channel = CC(DMA_Channel_,THIRD(UxARTx_DMA_TX));
		DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMAInitStructure.DMA_Mode = DMA_Mode_Normal;
		DMAInitStructure.DMA_Priority = DMA_Priority_Medium;
		DMAInitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
		DMAInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
		DMAInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
		DMAInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
		/* Select UxARTx DR Address register as DMA PeripheralBaseAddress */
		DMA_Init(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), &DMAInitStructure);
		/* Enable DMA TX Channel TC, TE  */
		DMA_ITConfig(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), DMA_IT_TC | DMA_IT_TE, ENABLE);
		m_bTxDmaInited = true;
	}
}

template< >
void CHW_USARTx<UxARTx_N>::OnDMATX()
{
	USARTTX_DEBUG_INFO("DMA IRQ");
	IS_EVENT_HANDLER;

	if (DMA_GetITStatus(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_TX)))) {
		DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_TX)));
		USARTTX_DEBUG_INFO("DMA complete");
		DoTXDone();
		if (m_TxChunk.uiLength) {
			DoContinueTx();
			/* Enable DMA TX operation */
			DMA_Cmd(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), ENABLE);
		} else {
			USART_DMACmd(USARTx, USART_DMAReq_Tx, DISABLE);
		}
		MARK_EVENT_IS_HANDLED;
	}
	if (DMA_GetITStatus(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_TX)))) {
		DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)), CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_TX)));
		USARTTX_DEBUG_INFO("DMA error");
		unsigned uiBytesLeft = DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)));
		DoTXError(uiBytesLeft);
		if (m_TxChunk.uiLength) {
			DoContinueTx();
		} else {
			USART_DMACmd(USARTx, USART_DMAReq_Tx, DISABLE);
		}
		ASSERTE("USART DMA Tx error");
		MARK_EVENT_IS_HANDLED;
	}
	ASSERT_EVENT_IS_HANDLED("USART DMA RX IRQ");
	USARTTX_DEBUG_INFO("DMA IRQ done");
}
#endif // UxARTx_DMA_TX

#ifdef UxARTx_DMA_RX
template< >
void CHW_USARTx<UxARTx_N>::DoPrepareRXDMA()
{
	ASSERTE(m_RxChunk.pData);
	ASSERTE(m_RxChunk.uiLength);
	DMA_Cmd(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), DISABLE);
	if (m_bRxDmaInited) {
		USARTRX_DEBUG_INFO("DMA set, len:", m_RxChunk.uiLength);
		DMA_MemoryTargetConfig(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), (uint32_t)m_RxChunk.pData, DMA_Memory_0);
		DMA_SetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), m_RxChunk.uiLength);
	} else {
		USARTRX_DEBUG_INFO("DMA init, len:", m_RxChunk.uiLength);
		DMA_InitTypeDef DMAInitStructure;
		DMA_StructInit(&DMAInitStructure);
		DMAInitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
		DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(USARTx->DR);
		DMAInitStructure.DMA_Memory0BaseAddr = (uint32_t)m_RxChunk.pData;
		DMAInitStructure.DMA_BufferSize = m_RxChunk.uiLength;
		DMAInitStructure.DMA_Channel = CC(DMA_Channel_,THIRD(UxARTx_DMA_RX));
		DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMAInitStructure.DMA_Mode = DMA_Mode_Normal;
		DMAInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		DMAInitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
		DMAInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
		DMAInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
		DMAInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
		/* Select UxARTx DR Address register as DMA PeripheralBaseAddress */
		DMA_Init(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), &DMAInitStructure);
		/* Enable DMA RX Channel TC, TE  */
		DMA_ITConfig(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), DMA_IT_TC | DMA_IT_TE, ENABLE);
		m_bRxDmaInited = true;
	}
	/* Enable DMA RX operation */
	DMA_Cmd(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), ENABLE);
}

template< >
void CHW_USARTx<UxARTx_N>::OnDMARX()
{
	USARTRX_DEBUG_INFO("DMA IRQ");
	IS_EVENT_HANDLER;

	if (DMA_GetITStatus(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_RX)))) {
		DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), CC(DMA_IT_TCIF,SECOND(UxARTx_DMA_RX)));
//		unsigned uiBytesLeft = DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)));
//		USARTRX_DEBUG_INFO("DMA complete, left:", uiBytesLeft);
//		if (!uiBytesLeft) {
			DoRXDone();
//		} else {
//			DoRXPart(uiBytesLeft);
//		}
		MARK_EVENT_IS_HANDLED;
	}
	if (DMA_GetITStatus(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_RX)))) {
		DMA_ClearITPendingBit(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)), CC(DMA_IT_TEIF,SECOND(UxARTx_DMA_RX)));
		USARTRX_DEBUG_INFO("DMA error");
		unsigned uiBytesLeft = DMA_GetCurrDataCounter(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)));
		DoRXError(uiBytesLeft);
		MARK_EVENT_IS_HANDLED;
		ASSERTE("USART DMA Rx error");
	}
	ASSERT_EVENT_IS_HANDLED("USART DMA RX IRQ");
	USARTRX_DEBUG_INFO("DMA IRQ done");
}
#endif // UxARTx_DMA_RX

template< >
void CHW_USARTx<UxARTx_N>::Deinit_USARTx()
{
	Cancel_USARTx();

	/* Disable USARTx Device */
	USART_Cmd(USARTx, DISABLE);
	USART_DEBUG_INFO("Disabled by deinit");
	USART_DeInit(USARTx);

	/* Deinitialize USARTx GPIO */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(UxARTx_PORT_TX));  /* Deinitialize USARTx TX Pin */
	GPIO_Init(CC(GPIO,FIRST(UxARTx_PORT_TX)), &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(UxARTx_PORT_RX));  /* Deinitialize USARTx RX Pin */
	GPIO_Init(CC(GPIO,FIRST(UxARTx_PORT_RX)), &GPIO_InitStructure);

	/* Deinitialize DMA Channels */
#ifdef UxARTx_DMA_TX
    DMA_DeInit(CCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX)));
#endif // UxARTx_DMA_TX
#ifdef UxARTx_DMA_RX
    DMA_DeInit(CCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX)));
#endif // UxARTx_DMA_RX

#ifdef STM32F0
# if (UxARTx_N >= 2 && UxARTx_N <= 5)
	RCC_APB1PeriphClockCmd(CC(RCC_APB1Periph_USART,UxARTx_N), DISABLE);
# else
	RCC_APB2PeriphClockCmd(CC(RCC_APB2Periph_USART,UxARTx_N), DISABLE);
# endif
#endif
#ifdef STM32F4
# if ((UxARTx_N >= 2 && UxARTx_N <= 5) || UxARTx_N >= 7)
	RCC_APB1PeriphClockCmd(CCC(RCC_APB1Periph_,UxART,UxARTx_N), DISABLE);
# else
	RCC_APB2PeriphClockCmd(CCC(RCC_APB2Periph_,UxART,UxARTx_N), DISABLE);
# endif
#endif
}

#ifdef UxARTx_PORT_RTS
template< >
void CHW_USARTx<UxARTx_N>::SwitchToTx()
{
	ASSERTE(m_eFSMState == FSMSTATE_RX);

	ControlRx(false);

	GPIO_SetBits(CC(GPIO,FIRST(UxARTx_PORT_RTS)), CC(GPIO_Pin_,SECOND(UxARTx_PORT_RTS)));

	m_eFSMState = FSMSTATE_TX;
}

template< >
void CHW_USARTx<UxARTx_N>::SwitchToRx()
{
	GPIO_ResetBits(CC(GPIO,FIRST(UxARTx_PORT_RTS)), CC(GPIO_Pin_,SECOND(UxARTx_PORT_RTS)));

	if (m_bEnabled) {
		ControlRx(true);
	}

	m_eFSMState = FSMSTATE_RX;
}
#else
template< >
void CHW_USARTx<UxARTx_N>::SwitchToTx() {/* do nothing*/}
template< >
void CHW_USARTx<UxARTx_N>::SwitchToRx() {/* do nothing*/}

#endif //UxARTx_PORT_RTS


REDIRECT_ISR_STATIC(UxARTx_IRQHandler, CHW_USARTx<UxARTx_N>, OnUSART);
#ifdef UxARTx_DMA_TX
REDIRECT_ISR_OBJ(CCCCC(DMA,FIRST(UxARTx_DMA_TX),_Stream,SECOND(UxARTx_DMA_TX),_IRQHandler), &g_HW_USARTx, OnDMATX);
#endif
#ifdef UxARTx_DMA_RX
REDIRECT_ISR_OBJ(CCCCC(DMA,FIRST(UxARTx_DMA_RX),_Stream,SECOND(UxARTx_DMA_RX),_IRQHandler), &g_HW_USARTx, OnDMARX);
#endif

#undef UxART
#undef UxARTx_N
#undef UxARTx_PORT_TX
#undef UxARTx_PORT_RX
#undef UxARTx_PORT_RTS
#undef UxARTx_IT_PRIO
#undef UxARTx_IRQ
#undef UxARTx_IRQn
#undef UxARTx_IRQHandler
#undef UxARTx_IRQ_HOST
#undef UxARTx_DMA_TX
#undef UxARTx_DMA_RX
#undef g_HW_USARTx
