#ifndef HW_AIN_H_INCLUDED
#define HW_AIN_H_INCLUDED

#include "interfaces/aio.h"
#include "util/macros.h"
#include "hw_ports.h"
#include <type_traits>

#define ADC_CHANNELS_COUNT          IF_STM32F0(19)IF_STM32F4(19)

#define ADC_CHANNEL_N_TEMPSENS 16
#define ADC_CHANNEL_N_VREFINT  17
#define ADC_CHANNEL_N_VBAT     18

#define ADC_TS_TEMP1    30
#define ADC_TS_CAL1     IF_STM32F0((*(uint16_t*)(uint32_t)0x1FFFF7B8)) IF_STM32F4((*(uint16_t*)(uint32_t)0x1FFF7A2C))
#define ADC_TS_TEMP2    110
#define ADC_TS_CAL2     IF_STM32F0((*(uint16_t*)(uint32_t)0x1FFFF7C2)) IF_STM32F4((*(uint16_t*)(uint32_t)0x1FFF7A2E))
//#define ADC_VREFINT_CAL IF_STM32F0((*(uint16_t*)(uint32_t)0x1FFFF7BA)) IF_STM32F4(1500)
#define ADC_VREFINT_CAL IF_STM32F0((*(uint16_t*)(uint32_t)0x1FFFF7BA)) IF_STM32F4((*(uint16_t*)(uint32_t)0x1FFF7A2A))

//TODO namespace CMCU {

template <class CHW_AINx>
class TAnalogIn_InterfaceWrapper :
		virtual public IAnalogIn
{
public:
	static unsigned GetChIndex() {return CHW_AINx::ChIndex;}
public: // IAnalogInProvider::IValueSource<unsigned>
	virtual value_type GetValue() const {IMPLEMENTS_INTERFACE_METHOD(IValueSource<value_type>::GetValue()); return CHW_AINx::GetValue();}
public: // IAnalogIn
	virtual value_type GetMaxValue() const {IMPLEMENTS_INTERFACE_METHOD(IAnalogIn::GetMaxValue()); return CHW_AINx::GetMaxValue();}
};

template <class CHW_AINx>
class TAnalogInProvider_InterfaceWrapper :
		virtual public TAnalogIn_InterfaceWrapper<CHW_AINx>,
		virtual public IAnalogInProvider
{
public: // IAnalogInProvider
	virtual void SetValueTargetInterface(IValueReceiver<typename IAnalogInProvider::value_type>* pValueTarget) {
			IMPLEMENTS_INTERFACE_METHOD(IAnalogInProvider::SetValueTargetInterface(pValueTarget));
			CHW_AINx::SetValueTargetInterface(pValueTarget);}
};

template <unsigned ADCCH_N, bool bHasPin = AINPort<ADCCH_N>::has_pin::value>
class CHW_AIN_Pin
{
protected:
	static void InitPin() {}
};
template <unsigned ADCCH_N>
class CHW_AIN_Pin<ADCCH_N, true>
{
protected:
	static void InitPin() {
		using port_type = typename AINPort<ADCCH_N>::port_type;
		using pin_type = typename AINPort<ADCCH_N>::pin_type;
		port_type::RCC_Cmd(ENABLE);
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_StructInit(&GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = pin_type::GPIO_Pin_x;
		IF_STM32F0(GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;)
		IF_STM32F4(GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;)
		GPIO_Init((GPIO_TypeDef*)port_type::GPIOx, &GPIO_InitStructure);
	}
};

template <unsigned ADCCH_N>
class CHW_AIN :
		public CHW_AIN_Pin<ADCCH_N, AINPort<ADCCH_N>::has_pin::value>
{
	public:
		enum {ChIndex = ADCCH_N};
		typedef CHW_AIN<ADCCH_N> type;
		typedef uint16_t value_type;
		typedef IValueReceiver<value_type> receiver_type;
		template <unsigned ADC_N> friend class CHW_ADC;
		static constexpr unsigned uiDefaultSampleTime = (
				IF_STM32F0(ADC_SampleTime_239_5Cycles)
				IF_STM32F4(ADC_SampleTime_28Cycles)
		);
	public:
		static inline type* Acquire(unsigned uiSampleTime = uiDefaultSampleTime) {
			type::Init(uiSampleTime);
			static type instance;
			return &instance;
		}
		static inline IAnalogIn* AcquireInterface(unsigned uiSampleTime = uiDefaultSampleTime) {
			Acquire(uiSampleTime);
			static TAnalogIn_InterfaceWrapper<type> wrapperInstance;
			return &wrapperInstance;
		}
		static inline IAnalogInProvider* AcquireProviderInterface(unsigned uiSampleTime = uiDefaultSampleTime) {
			Acquire(uiSampleTime);
			static TAnalogInProvider_InterfaceWrapper<type> wrapperInstance;
			return &wrapperInstance;
		}
		static inline void Release() { Deinit(); }
	public:
		static inline bool IsUsed() {return false;}
		static void Init(unsigned uiSampleTime) {m_uiSampleTime = uiSampleTime; CHW_AIN_Pin<ADCCH_N, AINPort<ADCCH_N>::has_pin::value>::InitPin();}
		static inline void SetScanRank(unsigned nScanRank) {UNUSED(nScanRank);}
		static inline void SetAdcDataPtr(volatile value_type* pData, unsigned nDataStep, unsigned nDataCount) {UNUSED(pData); UNUSED(nDataStep); UNUSED(nDataCount);}
		static void Connect() {}
		static void Disconnect() {}
		static void Deinit() {}
		static unsigned GetSampleTime() {return m_uiSampleTime;}
;
		static value_type GetMaxValue();
	public: /* IValueSource*/
		static value_type GetValue();
	public: /* IValueProvider */
		static void SetValueTargetInterface(IValueReceiver<value_type>* pValueTarget);
	protected:
		static void NotifySingleValue() {}
		static void NotifyMultipleValues(unsigned nCount) {UNUSED(nCount);}
	private:
		static unsigned m_uiSampleTime;
		static unsigned m_nScanRank;
		static volatile value_type* m_pData;
		static unsigned m_nDataStep;
		static unsigned m_nDataCount;
		static receiver_type* m_pValueTarget;
//	public:
//		static type instance;
};
/*static*/ template<unsigned ADCCH_N> unsigned CHW_AIN<ADCCH_N>::m_uiSampleTime = CHW_AIN<ADCCH_N>::uiDefaultSampleTime;
/*static*/ template<unsigned ADCCH_N> unsigned CHW_AIN<ADCCH_N>::m_nScanRank = 0;
/*static*/ template<unsigned ADCCH_N> volatile typename CHW_AIN<ADCCH_N>::value_type* CHW_AIN<ADCCH_N>::m_pData = NULL;
/*static*/ template<unsigned ADCCH_N> unsigned CHW_AIN<ADCCH_N>::m_nDataStep = 0;
/*static*/ template<unsigned ADCCH_N> unsigned CHW_AIN<ADCCH_N>::m_nDataCount = 0;
/*static*/ template<unsigned ADCCH_N> typename CHW_AIN<ADCCH_N>::receiver_type* CHW_AIN<ADCCH_N>::m_pValueTarget;


#define ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(AIN_N, ID,...) /* ##__VA_ARGS__ used for prevent early ID expansion */ \
	using CAnalogIn_##ID##__VA_ARGS__ = CHW_AIN<AIN_N>; \
	static inline CAnalogIn_##ID##__VA_ARGS__* AcquireAnalogIn_##ID##__VA_ARGS__(unsigned uiSampleTime = CAnalogIn_##ID##__VA_ARGS__::uiDefaultSampleTime) { \
		return CAnalogIn_##ID##__VA_ARGS__::Acquire(uiSampleTime); } \
	static inline void ReleaseAnalogIn_##ID##__VA_ARGS__() { CAnalogIn_##ID##__VA_ARGS__::Release(); } \
	static inline IAnalogIn* AcquireAnalogInInterface_##ID##__VA_ARGS__(unsigned uiSampleTime = CAnalogIn_##ID##__VA_ARGS__::uiDefaultSampleTime) { \
		return CAnalogIn_##ID##__VA_ARGS__::AcquireInterface(uiSampleTime); } \
	static inline IAnalogInProvider* AcquireAnalogInProviderInterface_##ID##__VA_ARGS__(unsigned uiSampleTime = CAnalogIn_##ID##__VA_ARGS__::uiDefaultSampleTime) { \
		return CAnalogIn_##ID##__VA_ARGS__::AcquireProviderInterface(uiSampleTime); } \
	static inline void ReleaseAnalogInInterface_##ID##__VA_ARGS__() { CAnalogIn_##ID##__VA_ARGS__::Release();}

#define ALIAS_ANALOG_IN(ID,ALIAS) \
	using CAnalogIn_##ALIAS = CAnalogIn_##ID;

#define ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(AIN_N,CHWBRD,ID,...) /* DEPRECATED, use CAnalogIn_X::Acquire(...) */

#endif /* HW_AIN_H_INCLUDED */
