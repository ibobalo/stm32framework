// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define SPIx_N                     2
// #define SPIx_SCK_GPIO_PORT         B,13
// #define SPIx_MISO_GPIO_PORT        B,14
// #define SPIx_MOSI_GPIO_PORT        B,15
// //next defines is optional for DMA access
// #define SPIx_DMA_TX                1,4,0
// #define SPIx_DMA_RX                1,3,0
// #include "hw_spi.cxx"
// ISPI* getSPIInterface() {return &g_HW_SPI2;};

#if !defined(SPIx_N) || \
	!defined(SPIx_SCK_GPIO_PORT) || \
	(!defined(SPIx_MISO_GPIO_PORT) && !defined(SPIx_MOSI_GPIO_PORT))
#error params must be defined : SPIx_N, SPIx_SCK_GPIO_PORT, SPIx_MISO_GPIO_PORT, SPIx_MOSI_GPIO_PORT
// next defines for editor hints
# define SPIx_N                     2
# define SPIx_SCK_GPIO_PORT         B,13
# define SPIx_MISO_GPIO_PORT        B,14,AF_1
# define SPIx_MOSI_GPIO_PORT        B,15
# define SPIx_DMA_TX                1,4,0
# define SPIx_DMA_RX                1,3,0
#endif

#if defined(SPIx_DMA_N) || \
	defined(SPIx_DMA_TX_STREAM_N) || \
	defined(SPIx_DMA_TX_CHANNEL_N) || \
	defined(SPIx_DMA_RX_STREAM_N) || \
	defined(SPIx_DMA_RX_CHANNEL_N)
# error SPIx_DMA_N, SPIx_DMA_TX_STREAM_N, SPIx_DMA_TX_CHANNEL_N, SPIx_DMA_RX_STREAM_N, SPIx_DMA_RX_CHANNEL_N is obsolete.
# pragma message("Hint: use")
#if defined(STM32F0)
# pragma message("#define SPIx_DMA_TX   DMA,CHANNEL")
# pragma message("#define SPIx_DMA_RX   DMA,CHANNEL")
#elif defined(STM32F4)
# pragma message("#define SPIx_DMA_TX   DMA,STREAM,CHANNEL")
# pragma message("#define SPIx_DMA_RX   DMA,STREAM,CHANNEL")
#endif
#endif

#if defined(STM32F0)
#if defined(SPIx_DMA_TX) && CHECK_EMPTY(SECOND(SPIx_DMA_TX))
# error invalid SPIx_DMA_TX defined
# pragma message("Hint: valid format is comma separated 2 numbers for DMA controller, channel")
# pragma message("#define SPIx_DMA_TX   1,2")
#endif
#if defined(SPIx_DMA_RX) && CHECK_EMPTY(SECOND(SPIx_DMA_RX))
# error invalid SPIx_DMA_RX defined
# pragma message("Hint: valid format is comma separated 2 numbers for DMA controller, channel")
# pragma message("#define SPIx_DMA_RX   1,2")
#endif
#elif defined(STM32F4)
#if defined(SPIx_DMA_TX) && CHECK_EMPTY(THIRD(SPIx_DMA_TX))
# error invalid SPIx_DMA_TX defined
# pragma message("Hint: valid format is comma separated 3 numbers for DMA controller, stream, channel")
# pragma message("#define SPIx_DMA_TX   1,2,3")
#endif
#if defined(SPIx_DMA_RX) && CHECK_EMPTY(THIRD(SPIx_DMA_RX))
# error invalid SPIx_DMA_RX defined
# pragma message("Hint: valid format is comma separated 3 numbers for DMA controller, stream, channel")
# pragma message("#define SPIx_DMA_RX   1,2,3")
#endif
#endif

#include "hw_spi.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include <stdint.h>
#include <stddef.h>
#include "stm32fxxxx.h"
#include "util/queue.h"
#include "util/endians.h"

#if 0 \
	|| ((SPIx_N == 1) && defined(RCC_APB1Periph_SPI1)) \
	|| ((SPIx_N == 2) && defined(RCC_APB1Periph_SPI2)) \
	|| ((SPIx_N == 3) && defined(RCC_APB1Periph_SPI3)) \
	|| ((SPIx_N == 4) && defined(RCC_APB1Periph_SPI4)) \
	|| ((SPIx_N == 5) && defined(RCC_APB1Periph_SPI5)) \
	|| ((SPIx_N == 6) && defined(RCC_APB1Periph_SPI6))
# define SPIx_APB_NUMBER 1
#elif 0 \
	|| ((SPIx_N == 1) && defined(RCC_APB2Periph_SPI1)) \
	|| ((SPIx_N == 2) && defined(RCC_APB2Periph_SPI2)) \
	|| ((SPIx_N == 3) && defined(RCC_APB2Periph_SPI3)) \
	|| ((SPIx_N == 4) && defined(RCC_APB2Periph_SPI4)) \
	|| ((SPIx_N == 5) && defined(RCC_APB2Periph_SPI5)) \
	|| ((SPIx_N == 6) && defined(RCC_APB2Periph_SPI6))
# define SPIx_APB_NUMBER 2
#endif

#define SPIx                       CC(SPI,SPIx_N)
#ifdef SPIx_DMA_TX
//# define SPIx_DMA_Tx_TE_FLAG        CC(DMA_FLAG_TEIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_HT_FLAG        CC(DMA_FLAG_HTIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_TC_FLAG        CC(DMA_FLAG_TCIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_FE_BIT         CC(DMA_IT_FEIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_DME_BIT        CC(DMA_IT_DMEIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_TE_BIT         CC(DMA_IT_TEIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_HT_BIT         CC(DMA_IT_HTIF,SECOND(SPIx_DMA_TX))
//# define SPIx_DMA_Tx_TC_BIT         CC(DMA_IT_TCIF,SECOND(SPIx_DMA_TX))
#if defined(STM32F0)
# define DMATX_Stream                  CCCC(DMA,FIRST(SPIx_DMA_TX),_Channel,SECOND(SPIx_DMA_TX))
# if (SECOND(SPIx_DMA_TX) == 2 || SECOND(SPIx_DMA_TX) == 3)
# define DMATX_IRQn                    CCCC(DMA,FIRST(SPIx_DMA_TX),_Channel2_3,_IRQn)
# define DMATX_IRQHandler              CCCC(DMA,FIRST(SPIx_DMA_TX),_Channel2_3,_IRQHandler)
# define DMATX_IRQID                   23
# elif (SECOND(SPIx_DMA_TX) == 4 || SECOND(SPIx_DMA_TX) == 5)
# define DMATX_IRQn                    CCCC(DMA,FIRST(SPIx_DMA_TX),_Channel4_5,_IRQn)
# define DMATX_IRQHandler              CCCC(DMA,FIRST(SPIx_DMA_TX),_Channel4_5,_IRQHandler)
# define DMATX_IRQID                   45
# else
# define DMATX_IRQn                    CCCCC(DMA,FIRST(SPIx_DMA_TX),_Channel,SECOND(SPIx_DMA_TX),_IRQn)
# define DMATX_IRQHandler              CCCCC(DMA,FIRST(SPIx_DMA_TX),_Channel,SECOND(SPIx_DMA_TX),_IRQHandler)
# define DMATX_IRQID                   SECOND(SPIx_DMA_TX)
# endif
# define DMATX_FLAG(FLAG)              CCCCC(DMA,FIRST(SPIx_DMA_TX),_FLAG_,FLAG,SECOND(SPIx_DMA_TX))
# define DMATX_ITBIT(BIT)              CCCCC(DMA,FIRST(SPIx_DMA_TX),_IT_,BIT,SECOND(SPIx_DMA_TX))
# define DMATX_Cmd(STATE)              DMA_Cmd(DMATX_Stream, STATE)
# define DMATX_GetCmdStatus()          (DMATX_Stream->CCR & DMA_CCR_EN ? ENABLE : DISABLE)
# define DMATX_GetFlagStatus(FLAG)     DMA_GetFlagStatus(DMATX_FLAG(FLAG))
# define DMATX_ClearFlag(...)          DMA_ClearFlag(MAP_SEP(DMATX_FLAG, |, __VA_ARGS__))
# define DMATX_ITConfig(BITS, STATE)   DMA_ITConfig(DMATX_Stream, BITS, STATE)
# define DMATX_GetITStatus(BIT)        DMA_GetITStatus(DMATX_ITBIT(BIT))
# define DMATX_MemoryTargetConfig(A,N) {DMATX_Stream->CMAR = (uint32_t)(A);}
# define DMATX_ClearITPendingBit(...)  DMA_ClearITPendingBit(MAP_SEP(DMATX_ITBIT, |, __VA_ARGS__));
#elif defined(STM32F4)
# define DMATX_Stream                  CCCC(DMA,FIRST(SPIx_DMA_TX),_Stream,SECOND(SPIx_DMA_TX))
# define DMATX_IRQn                    CCCCC(DMA,FIRST(SPIx_DMA_TX),_Stream,SECOND(SPIx_DMA_TX),_IRQn)
# define DMATX_IRQHandler              CCCCC(DMA,FIRST(SPIx_DMA_TX),_Stream,SECOND(SPIx_DMA_TX),_IRQHandler)
# define DMATX_IRQID                   SECOND(SPIx_DMA_TX)
# define DMATX_FLAG(BIT)               CCCC(DMA_FLAG_,BIT,IF,SECOND(SPIx_DMA_TX))
# define DMATX_ITBIT(BIT)              CCCC(DMA_IT_,BIT,IF,SECOND(SPIx_DMA_TX))
# define DMATX_Cmd(STATE)              DMA_Cmd(DMATX_Stream, STATE)
# define DMATX_GetCmdStatus()          DMA_GetCmdStatus(DMATX_Stream)
# define DMATX_GetFlagStatus(FLAG)     DMA_GetFlagStatus(DMATX_Stream, DMATX_FLAG(FLAG))
# define DMATX_ClearFlag(...)          DMA_ClearFlag(DMATX_Stream, MAP_SEP(DMATX_FLAG, |, __VA_ARGS__))
# define DMATX_ITConfig(BITS, STATE)   DMA_ITConfig(DMATX_Stream, BITS, STATE)
# define DMATX_GetITStatus(BIT)        DMA_GetITStatus(DMATX_Stream, DMATX_ITBIT(BIT))
# define DMATX_MemoryTargetConfig(A,N) DMA_MemoryTargetConfig(DMATX_Stream, A, CC(DMA_Memory_,N));
# define DMATX_ClearITPendingBit(...)  DMA_ClearITPendingBit(DMATX_Stream, MAP_SEP(DMATX_ITBIT, |, __VA_ARGS__));
#endif
# define DMATX_SetCurrDataCounter(N)   DMA_SetCurrDataCounter(DMATX_Stream, N);
#endif // SPIx_DMA_TX
#ifdef SPIx_DMA_RX
#if defined(STM32F0)
# define DMARX_Stream                  CCCC(DMA,FIRST(SPIx_DMA_RX),_Channel,SECOND(SPIx_DMA_RX))
# if (SECOND(SPIx_DMA_RX) == 2 || SECOND(SPIx_DMA_RX) == 3)
# define DMARX_IRQn                    CCCC(DMA,FIRST(SPIx_DMA_RX),_Channel2_3,_IRQn)
# define DMARX_IRQHandler              CCCC(DMA,FIRST(SPIx_DMA_RX),_Channel2_3,_IRQHandler)
# define DMARX_IRQID                   23
# elif (SECOND(SPIx_DMA_RX) == 4 || SECOND(SPIx_DMA_RX) == 5)
# define DMARX_IRQn                    CCCC(DMA,FIRST(SPIx_DMA_RX),_Channel4_5,_IRQn)
# define DMARX_IRQHandler              CCCC(DMA,FIRST(SPIx_DMA_RX),_Channel4_5,_IRQHandler)
# define DMARX_IRQID                   45
# else
# define DMARX_IRQn                    CCCCC(DMA,FIRST(SPIx_DMA_RX),_Channel,SECOND(SPIx_DMA_RX),_IRQn)
# define DMARX_IRQHandler              CCCCC(DMA,FIRST(SPIx_DMA_RX),_Channel,SECOND(SPIx_DMA_RX),_IRQHandler)
# define DMARX_IRQID                   SECOND(SPIx_DMA_RX)
# endif
# define DMARX_FLAG(FLAG)              CCCCC(DMA,FIRST(SPIx_DMA_RX),_FLAG_,FLAG,SECOND(SPIx_DMA_RX))
# define DMARX_ITBIT(BIT)              CCCCC(DMA,FIRST(SPIx_DMA_RX),_IT_,BIT,SECOND(SPIx_DMA_RX))
# define DMARX_Cmd(STATE)              DMA_Cmd(DMARX_Stream, STATE)
# define DMARX_GetCmdStatus()          (DMARX_Stream->CCR & DMA_CCR_EN ? ENABLE : DISABLE)
# define DMARX_GetFlagStatus(FLAG)     DMA_GetFlagStatus(DMARX_FLAG(FLAG))
# define DMARX_ClearFlag(...)          DMA_ClearFlag(MAP_SEP(DMARX_FLAG, |, __VA_ARGS__))
# define DMARX_ITConfig(BITS, STATE)   DMA_ITConfig(DMARX_Stream, BITS, STATE)
# define DMARX_GetITStatus(BIT)        DMA_GetITStatus(DMARX_ITBIT(BIT))
# define DMARX_MemoryTargetConfig(A,N) {DMARX_Stream->CMAR = (uint32_t)(A);}
# define DMARX_ClearITPendingBit(...)  DMA_ClearITPendingBit(MAP_SEP(DMARX_ITBIT, |, __VA_ARGS__));
#elif defined(STM32F4)
# define DMARX_Stream                  CCCC(DMA,FIRST(SPIx_DMA_RX),_Stream,SECOND(SPIx_DMA_RX))
# define DMARX_IRQn                    CCCCC(DMA,FIRST(SPIx_DMA_RX),_Stream,SECOND(SPIx_DMA_RX),_IRQn)
# define DMARX_IRQHandler              CCCCC(DMA,FIRST(SPIx_DMA_RX),_Stream,SECOND(SPIx_DMA_RX),_IRQHandler)
# define DMARX_IRQID                   SECOND(SPIx_DMA_RX)
# define DMARX_FLAG(BIT)               CCCC(DMA_FLAG_,BIT,IF,SECOND(SPIx_DMA_RX))
# define DMARX_ITBIT(BIT)              CCCC(DMA_IT_,BIT,IF,SECOND(SPIx_DMA_RX))
# define DMARX_Cmd(STATE)              DMA_Cmd(DMARX_Stream, STATE)
# define DMARX_GetCmdStatus()          DMA_GetCmdStatus(DMARX_Stream)
# define DMARX_GetFlagStatus(FLAG)     DMA_GetFlagStatus(DMARX_Stream, DMARX_FLAG(FLAG))
# define DMARX_ClearFlag(...)          DMA_ClearFlag(DMARX_Stream, MAP_SEP(DMARX_FLAG, |, __VA_ARGS__))
# define DMARX_ITConfig(BITS, STATE)   DMA_ITConfig(DMARX_Stream, BITS, STATE)
# define DMARX_GetITStatus(BIT)        DMA_GetITStatus(DMARX_Stream, DMARX_ITBIT(BIT))
# define DMARX_MemoryTargetConfig(A,N) DMA_MemoryTargetConfig(DMARX_Stream, A, CC(DMA_Memory_,N));
# define DMARX_ClearITPendingBit(...)  DMA_ClearITPendingBit(DMARX_Stream, MAP_SEP(DMARX_ITBIT, |, __VA_ARGS__));
#endif
# define DMARX_SetCurrDataCounter(N)   DMA_SetCurrDataCounter(DMARX_Stream, N);
#endif // SPIx_DMA_RX
/*----------- SPIx Interrupt Priority -------------*/
#define SPIx_IRQn                  CCC(SPI,SPIx_N,_IRQn)
#define SPIx_IRQHandler            CCC(SPI,SPIx_N,_IRQHandler)
#define SPIx_IT_GROUP_PRIO              3   /* SPI GROUP PRIORITY */
#define SPIx_IT_SUBPRIO                 2   /* SPI IRQ SUB-PRIORITY */
#define SPIx_IT_DMATx_SUBPRIO           1   /* SPI DMA Tx SUB-PRIORITY */
#define SPIx_IT_DMARx_SUBPRIO           1   /* SPI DMA Rx SUB-PRIORITY */

#define CHW_SPIx                   CC(CHW_SPI,SPIx_N)
#define g_HW_SPIx                  CC(g_HW_SPI,SPIx_N)
#define CHW_SPIx_InterfaceWrapper  CCC(CHW_SPI,SPIx_N,_InterfaceWrapper)
#define g_HW_SPIx_InterfaceWrapper CCC(g_HW_SPI,SPIx_N,_InterfaceWrapper)

#define SPI_TIMEOUT(nBytes) (1 + nBytes * 10)


class CHW_SPIx :
		public ISysTickNotifee
{
public:
	void Init(unsigned uiBitrate);
	void Deinit();

	bool Start_SPIx_Session(IDeviceSPI* pDeviceSPI, const CChainedConstChunks* pHeadTxChunk, const CChainedIoChunks* pHeadRxChunk);
	void RequestSPIIdleNotify(IDeviceSPI* pDeviceSPI);

public: // ISysTickNotifee
	virtual void OnSysTick();

public: // IRQHandlers
	static void OnSPIxInterrupt();
	static void OnSPIxDMATxRx();
	static void OnSPIxDMATx();
	static void OnSPIxDMARx();

private:
	static void ProcessSPIxFSM();
	static bool Process_SPIx_TxFSM();
	static bool Process_SPIx_RxFSM();
	static void Finish_Session();
	static void Terminate_Session();

	static void NotifyPktDone();
	static void NotifyPktError();
	static void NotifySPIIdle();

private:
	static void Init_SPIx();
	static void Init_SPIx_GPIO();
	static void Init_SPIx_Params();
	static void Init_SPIx_DMA();
	static void Init_SPIx_IRQ();
	static void Deinit_SPIx();
	static void Cancel_SPIx();
	static void Stop_SPIx_Session();

	static inline void Send_SPIx_CLKs();
	static inline bool Send_SPIx_Data();
	static inline bool Receive_SPIx_Data();
	static inline bool Check_SPIx_Flag_Busy();
	static inline bool Check_SPIx_Flag_TXE();
	static inline void Clear_SPIx_Flags_OVR_UDR();
	static inline void Clear_SPIx_Pending_CRCERR();
	static inline bool Check_SPIx_IT_TXE();
	static inline bool Check_SPIx_IT_RXNE();
	static inline bool Check_SPIx_IT_MODF();
	static inline bool Check_SPIx_IT_OVR();
	static inline bool Check_SPIx_IT_FRE();
	static inline bool Check_SPIx_IT_CRCERR();
	static inline void Enable_SPIx_IT_TXE();
	static inline void Enable_SPIx_IT_RXNE();
	static inline void Enable_SPIx_IT_ERR();
	static inline void Disable_SPIx_IT_TXE();
	static inline void Disable_SPIx_IT_RXNE();
	static inline void Disable_SPIx_IT_ERR();
	static inline void Disable_SPIx_IT_All();
	static inline void Disable_SPIx_DMA();

	static void DoTimeoutDowncount();
	static void DoPrepareTxDMA(unsigned uiLength);
	static void DoPrepareRxDMA(unsigned uiLength);

	static unsigned m_uiActualBitrate;
	static unsigned m_uiMaximumBitrate;
	static ISPI::ESPIMode m_eMode;
	static ISPI::EBitsOrder m_eBitsOrder;
	static unsigned m_nBitsCount;

	typedef enum {
		FSMSTATE_Init,
		FSMSTATE_Idle,
		FSMSTATE_TxRx,
	} FSMSTATE;
	static FSMSTATE           m_eFSMState;

	static DMA_InitTypeDef    m_DmaInitStructureTx;
	static bool               m_bTxDmaReinitRequired;
	static DMA_InitTypeDef    m_DmaInitStructureRx;
	static bool               m_bRxDmaReinitRequired;

	static IDeviceSPI*         m_pCurrentSPIDevice;
	static CChainedConstChunks m_pCurrentTxChunk;
	static CChainedIoChunks    m_pCurrentRxChunk;

	static volatile unsigned             m_nTimeoutCountdown;
	static TAtomicQueue<IDeviceSPI*, MAX_SPI_DEVICES>   m_qIdleNotifyQueue;
};


REDIRECT_ISR_STATIC(SPIx_IRQHandler,  CHW_SPIx, OnSPIxInterrupt);
#if defined(SPIx_DMA_TX) && defined(SPIx_DMA_RX) && (DMATX_IRQID == DMARX_IRQID)
	/* IRQ is shared for TX and RX */
	REDIRECT_ISR_STATIC(DMATX_IRQHandler, CHW_SPIx, OnSPIxDMATxRx);
#else
# ifdef SPIx_DMA_TX
	REDIRECT_ISR_STATIC(DMATX_IRQHandler, CHW_SPIx, OnSPIxDMATx);
# endif
# ifdef SPIx_DMA_RX
	REDIRECT_ISR_STATIC(DMARX_IRQHandler, CHW_SPIx, OnSPIxDMARx);
# endif
#endif

//#define SPI_DEBUG_INFO(...) DEBUG_INFO("SPI" STR(SPIx_N) ": " __VA_ARGS__)
#define SPI_DEBUG_INFO(...) {}

#define RESTART_TIMEOUT(timeout) { \
		m_nTimeoutCountdown = timeout; \
}
#define CHANGE_SPI_STATE(TO_STATE, timeout) { \
		RESTART_TIMEOUT(timeout); \
		m_eFSMState = TO_STATE; \
		SPI_DEBUG_INFO("FSM " #TO_STATE); \
}
#define CHECKED_CHANGE_SPI_STATE(FROM_STATE, TO_STATE, timeout) { \
		m_nTimeoutCountdown = timeout; \
		ASSERTE(m_eFSMState == FROM_STATE); \
		m_eFSMState = TO_STATE; \
		SPI_DEBUG_INFO("FSM " #TO_STATE); \
}

#define ASSERT_EVENT_IS_HANDLED_SPI(...) ASSERT_EVENT_IS_HANDLED("SPI: " __VA_ARGS__)

/*static*/
unsigned           CHW_SPIx::m_uiActualBitrate;
unsigned           CHW_SPIx::m_uiMaximumBitrate;
ISPI::ESPIMode     CHW_SPIx::m_eMode;
ISPI::EBitsOrder   CHW_SPIx::m_eBitsOrder;
unsigned           CHW_SPIx::m_nBitsCount;
CHW_SPIx::FSMSTATE CHW_SPIx::m_eFSMState;
DMA_InitTypeDef    CHW_SPIx::m_DmaInitStructureTx;
bool               CHW_SPIx::m_bTxDmaReinitRequired;
DMA_InitTypeDef    CHW_SPIx::m_DmaInitStructureRx;
bool               CHW_SPIx::m_bRxDmaReinitRequired;
IDeviceSPI*         CHW_SPIx::m_pCurrentSPIDevice;
CChainedConstChunks CHW_SPIx::m_pCurrentTxChunk;
CChainedIoChunks    CHW_SPIx::m_pCurrentRxChunk;
volatile unsigned          CHW_SPIx::m_nTimeoutCountdown;
TAtomicQueue<IDeviceSPI*, MAX_SPI_DEVICES> CHW_SPIx::m_qIdleNotifyQueue;


void CHW_SPIx::Init(unsigned uiBitrate)
{
	m_uiMaximumBitrate = uiBitrate;
	m_eMode = ISPI::MODE_0_0;
	m_eBitsOrder = ISPI::MSB_1st;
	m_nBitsCount = 8;
	m_eFSMState = FSMSTATE_Init;

	m_bTxDmaReinitRequired = true;
	m_bRxDmaReinitRequired = true;

	m_pCurrentSPIDevice = NULL;
	m_pCurrentTxChunk.Clean();
	m_pCurrentRxChunk.Clean();

	m_nTimeoutCountdown = 0;

	ProcessSPIxFSM();
}

void CHW_SPIx::Deinit()
{
	Deinit_SPIx();
}

bool CHW_SPIx::Start_SPIx_Session(IDeviceSPI* pDeviceSPI, const CChainedConstChunks* pHeadTxChunk, const CChainedIoChunks* pHeadRxChunk)
{
	ASSERTE(pDeviceSPI);
	ASSERTE(pHeadTxChunk || pHeadRxChunk);

	SPI_DEBUG_INFO("Start");

	if (m_eFSMState != FSMSTATE_Idle) {
		SPI_DEBUG_INFO("Busy state");
		return false;
	}
	if (Check_SPIx_Flag_Busy()) {
		SPI_DEBUG_INFO("Busy flag");
		return false;
	}

	ASSERTE(!m_pCurrentTxChunk.pData && !m_pCurrentTxChunk.uiLength && !m_pCurrentTxChunk.GetNextItem());
	ASSERTE(!m_pCurrentRxChunk.pData && !m_pCurrentRxChunk.uiLength && !m_pCurrentRxChunk.GetNextItem());

	if (m_pCurrentSPIDevice != pDeviceSPI) {
		m_pCurrentSPIDevice = pDeviceSPI;
		if (pDeviceSPI->GetSPIParams(m_uiMaximumBitrate, m_eMode, m_eBitsOrder, m_nBitsCount)) {
			// paramaters changed, reinit
			Init_SPIx_Params();

		}
	}
	if (pHeadTxChunk) m_pCurrentTxChunk = *pHeadTxChunk;
	if (pHeadRxChunk) m_pCurrentRxChunk = *pHeadRxChunk;

	/* Single master - set NSS */
	SPI_NSSInternalSoftwareConfig(SPIx, SPI_NSSInternalSoft_Set);

	unsigned uiLength = MAX2(m_pCurrentTxChunk.uiLength, m_pCurrentRxChunk.uiLength);
#ifndef SPIx_DMA_TX
	SPI_DataSizeConfig(SPIx, m_pCurrentTxChunk.uiLength == 1 ? SPI_DataSize_8b : SPI_DataSize_16b);
#endif
#ifdef STM32F0
	SPI_RxFIFOThresholdConfig(SPIx, m_pCurrentRxChunk.uiLength == 1 ? SPI_RxFIFOThreshold_QF : SPI_RxFIFOThreshold_HF);
#endif

	CHECKED_CHANGE_SPI_STATE(FSMSTATE_Idle, FSMSTATE_TxRx, SPI_TIMEOUT(uiLength));

	Clear_SPIx_Flags_OVR_UDR();
	Enable_SPIx_IT_ERR();

#ifdef SPIx_DMA_RX
	DoPrepareRxDMA(uiLength);
	DMARX_ITConfig(DMA_IT_TC | DMA_IT_TE, ENABLE);
	IF_STM32F0(SPI_I2S_DMACmd(SPIx, SPI_I2S_DMAReq_Rx, ENABLE));
	IF_STM32F4(SPI_DMACmd(SPIx, SPI_DMAReq_Rx, ENABLE));
	if (DMARX_GetITStatus(TC)) {SPI_DEBUG_INFO("DMA Rx TC");}
#else
	if (m_pCurrentRxChunk.uiLength) {
		if (m_pCurrentRxChunk.pData) {
			SPI_DEBUG_INFO("bytes to RX:", m_pCurrentRxChunk.uiLength);
		} else {
			SPI_DEBUG_INFO("bytes to skip:", m_pCurrentRxChunk.uiLength);
		}
		Enable_SPIx_IT_RXNE();                INDIRECT_CALL(OnSPIxInterrupt());
	}
#endif

#ifdef SPIx_DMA_TX
	DoPrepareTxDMA(uiLength);
	IF_STM32F0(SPI_I2S_DMACmd(SPIx, SPI_I2S_DMAReq_Tx, ENABLE));
	IF_STM32F4(SPI_DMACmd(SPIx, SPI_DMAReq_Tx, ENABLE));
	if (DMATX_GetITStatus(TC)) {SPI_DEBUG_INFO("DMA Tx TC");}
#else
	if (m_pCurrentTxChunk.uiLength) {
		if (m_pCurrentTxChunk.pData) {
			SPI_DEBUG_INFO("zeroes to TX:", m_pCurrentTxChunk.uiLength);
		} else {
			SPI_DEBUG_INFO("bytes to TX:", m_pCurrentTxChunk.uiLength);
		}
		Send_SPIx_Data();
		Enable_SPIx_IT_TXE();                INDIRECT_CALL(OnSPIxInterrupt())
	} else if (m_pCurrentRxChunk.uiLength) {
		Send_SPIx_CLKs();
	}
#endif

	m_pCurrentSPIDevice->Select();

	SPI_Cmd(SPIx, ENABLE);

	return true;
}

void CHW_SPIx::RequestSPIIdleNotify(IDeviceSPI* pDeviceSPI)
{
	m_qIdleNotifyQueue.Push(pDeviceSPI);
	if (m_eFSMState == FSMSTATE_Idle && Check_SPIx_Flag_Busy()) {
		NotifySPIIdle();
	}
}

/*virtual*/
void CHW_SPIx::OnSysTick()
{
	IMPLEMENTS_INTERFACE_METHOD(ISysTickNotifee::OnSysTick());

	switch (m_eFSMState) {
	case FSMSTATE_Init:
		// do nothing
		break;
	case FSMSTATE_Idle:
		if (!Check_SPIx_Flag_Busy()) {
			NotifySPIIdle();
		} else {
			DoTimeoutDowncount();
		}
		break;
	case FSMSTATE_TxRx:
/*		if (m_eTxState == TxSTATE_WAIT_NOT_BSY && Check_SPIx_Busy()) {
			SPI_DEBUG_INFO("NOT BSY catch in tick IRQ");
		}*/
		DoTimeoutDowncount();
		break;
	}
}

void CHW_SPIx::OnSPIxInterrupt()
{
	IS_EVENT_HANDLER;

#ifdef DEBUG
	SPI_DEBUG_INFO("IRQ: SPI");

	if (Check_SPIx_IT_RXNE()) {SPI_DEBUG_INFO("IT RxNE"); }
	if (Check_SPIx_IT_TXE()) {SPI_DEBUG_INFO("IT TxE"); }
	if (Check_SPIx_IT_MODF()) {SPI_DEBUG_INFO("IT MODF"); }
	if (Check_SPIx_IT_CRCERR()) {SPI_DEBUG_INFO("IT CRCERR"); }
	if (Check_SPIx_IT_OVR()) {SPI_DEBUG_INFO("IT OVR"); }
	if (Check_SPIx_IT_FRE()) {SPI_DEBUG_INFO("IT FRE"); }
	if (Check_SPIx_Flag_Busy()) {SPI_DEBUG_INFO("Flag BSY"); }
#endif

#ifndef SPIx_DMA_TX
	if (Check_SPIx_IT_TXE()) {
		bool bMoreToSend = m_pCurrentTxChunk.uiLength && Send_SPIx_Data();
		if (!bMoreToSend) {
			Disable_SPIx_IT_TXE();
		}
		MARK_EVENT_IS_HANDLED;
	}
#endif
#ifndef SPIx_DMA_RX
	while (Check_SPIx_IT_RXNE()) {
		bool bMoreToReceive = Receive_SPIx_Data();
		if (bMoreToReceive && !m_pCurrentTxChunk.uiLength && Check_SPIx_Flag_TXE()) {
			Send_SPIx_CLKs();
		}
		MARK_EVENT_IS_HANDLED;
		if (!bMoreToReceive) {
			Disable_SPIx_IT_RXNE();
		}
	}
#endif
#if !defined(SPIx_DMA_TX) || !defined(SPIx_DMA_RX)
	if (!m_pCurrentTxChunk.uiLength && !m_pCurrentRxChunk.uiLength) {
		Finish_Session();
		SPI_DEBUG_INFO("IRQ done");
	}
#endif
	if (Check_SPIx_IT_MODF()) {
		SPI_DEBUG_INFO("Error MODF");
		Terminate_Session();
		MARK_EVENT_IS_HANDLED;
	}
	if (Check_SPIx_IT_FRE()) {
		SPI_DEBUG_INFO("Error FRE");
		Terminate_Session();
		MARK_EVENT_IS_HANDLED;
	}
	if (Check_SPIx_IT_OVR()) {
		SPI_DEBUG_INFO("Error OVR");
		Terminate_Session();
		MARK_EVENT_IS_HANDLED;
		ASSERT("SPI OVERRUN");
	}

	__DSB(); // Data Synchronization Barrier. Ensure SPI_IT_* statuses cleared and isr not trigger again
	ASSERT_EVENT_IS_HANDLED_SPI("IRQ");
//	if (NVIC_GetPendingIRQ(SPIx_IRQn)) SPI_DEBUG_INFO("IRQ pending 9");
	SPI_DEBUG_INFO("IRQ: SPI done");
}

void CHW_SPIx::OnSPIxDMATxRx()
{
	OnSPIxDMATx();
	OnSPIxDMARx();
}

#ifdef SPIx_DMA_TX
void CHW_SPIx::OnSPIxDMATx()
{
	IS_EVENT_HANDLER;

# ifdef DEBUG
	SPI_DEBUG_INFO("IRQ: DMA Tx");
	if (DMATX_GetITStatus(TC)) {SPI_DEBUG_INFO("DMA Tx TC");}
	if (DMATX_GetITStatus(TE)) {SPI_DEBUG_INFO("DMA Tx TE");}
# endif

	if (DMATX_GetITStatus(TE)) {
		DMATX_ClearITPendingBit(TE);
		SPI_DEBUG_INFO("DMA Tx Transfer Error");
		ASSERT("DMA Tx ERROR");
		Terminate_Session();
		MARK_EVENT_IS_HANDLED;
	}
	ASSERT_EVENT_IS_HANDLED_SPI("DMA Tx IRQ");
	SPI_DEBUG_INFO("IRQ: DMA Tx done");
}
#endif // SPIx_DMA_TX

#ifdef SPIx_DMA_RX
void CHW_SPIx::OnSPIxDMARx()
{
	IS_EVENT_HANDLER;

# ifdef DEBUG
	SPI_DEBUG_INFO("IRQ: DMA Rx");
	if (DMARX_GetITStatus(TC)) {SPI_DEBUG_INFO("DMA Rx TC");}
	if (DMARX_GetITStatus(TE)) {SPI_DEBUG_INFO("DMA Rx TE");}
# endif

	if (DMARX_GetITStatus(TC)) {
		DMARX_ClearITPendingBit(TC);
# ifdef DEBUG
		if (m_pCurrentTxChunk.pData) {
			SPI_DEBUG_INFO("DMA Tx len:", m_pCurrentTxChunk.uiLength);
			SPI_DEBUG_INFO("DMA Tx data:",
				(m_pCurrentTxChunk.uiLength == 0) ? 0 :
				(m_pCurrentTxChunk.uiLength == 1) ? (m_pCurrentTxChunk.pData[0]) :
				(m_pCurrentTxChunk.uiLength == 2) ? ((m_pCurrentTxChunk.pData[0] << 8) | m_pCurrentTxChunk.pData[1]) :
				(m_pCurrentTxChunk.uiLength == 3) ? ((m_pCurrentTxChunk.pData[0] << 16) | (m_pCurrentTxChunk.pData[1] << 8) | m_pCurrentTxChunk.pData[2]) :
				((m_pCurrentTxChunk.pData[0] << 24) | (m_pCurrentTxChunk.pData[1] << 16) | (m_pCurrentTxChunk.pData[2] << 8) | m_pCurrentTxChunk.pData[3])
			);
		}
		if (m_pCurrentRxChunk.pData) {
			SPI_DEBUG_INFO("DMA Rx len:", m_pCurrentRxChunk.uiLength);
			SPI_DEBUG_INFO("DMA Rx data:",
				(m_pCurrentRxChunk.uiLength == 0) ? 0 :
				(m_pCurrentRxChunk.uiLength == 1) ? (m_pCurrentRxChunk.pData[0]) :
				(m_pCurrentRxChunk.uiLength == 2) ? ((m_pCurrentRxChunk.pData[0] << 8) | m_pCurrentRxChunk.pData[1]) :
				(m_pCurrentRxChunk.uiLength == 3) ? ((m_pCurrentRxChunk.pData[0] << 16) | (m_pCurrentRxChunk.pData[1] << 8) | m_pCurrentRxChunk.pData[2]) :
				((m_pCurrentRxChunk.pData[0] << 24) | (m_pCurrentRxChunk.pData[1] << 16) | (m_pCurrentRxChunk.pData[2] << 8) | m_pCurrentRxChunk.pData[3])
			);
		}
# endif
# ifdef SPIx_DMA_TX
		ASSERTE(DMATX_GetFlagStatus(TC));
# endif
		auto pNextTxChunk = m_pCurrentTxChunk.GetNextItem();
		if (!pNextTxChunk) m_pCurrentTxChunk.Clean(); else m_pCurrentTxChunk = *pNextTxChunk;
		auto pNextRxChunk = m_pCurrentRxChunk.GetNextItem();
		if (!pNextRxChunk) m_pCurrentRxChunk.Clean(); else m_pCurrentRxChunk = *pNextRxChunk;
		if (m_pCurrentTxChunk.uiLength || m_pCurrentRxChunk.uiLength) {
			// promote to next Rx vector item
			unsigned uiLength = MAX2(m_pCurrentTxChunk.uiLength, m_pCurrentRxChunk.uiLength);
			SPI_DEBUG_INFO("DMA next item", uiLength);
			RESTART_TIMEOUT(SPI_TIMEOUT(uiLength));
#ifdef SPIx_DMA_RX
			DoPrepareRxDMA(uiLength);
			ASSERTE(SPIx->CR2 & SPI_CR2_RXDMAEN);
#endif
#ifdef SPIx_DMA_TX
			DoPrepareTxDMA(uiLength);
			ASSERTE(SPIx->CR2 & SPI_CR2_TXDMAEN);
#endif
			ASSERTE(SPIx->CR1 & SPI_CR1_SPE);
		} else {
			Finish_Session();
			SPI_DEBUG_INFO("Rx DMA done");
		}
		MARK_EVENT_IS_HANDLED;
	}

	if (DMARX_GetITStatus(TE)) {
		DMARX_ClearITPendingBit(TE);
		SPI_DEBUG_INFO("DMA Rx Transfer Error");
		ASSERT("DMA Rx ERROR");
		Terminate_Session();
		MARK_EVENT_IS_HANDLED;
	}

	ASSERT_EVENT_IS_HANDLED_SPI("DMA Tx IRQ");
	SPI_DEBUG_INFO("IRQ: DMA Rx done");
}
#endif // SPIx_DMA_RX

void CHW_SPIx::ProcessSPIxFSM()
{
	switch (m_eFSMState) {
	case FSMSTATE_Init:
		Deinit_SPIx();
		Init_SPIx();
		CHECKED_CHANGE_SPI_STATE(FSMSTATE_Init, FSMSTATE_Idle, SPI_TIMEOUT(1));
		// no break, proceed immediate
	case FSMSTATE_Idle:
		if (!Check_SPIx_Flag_Busy()) {
			NotifySPIIdle();
		}
		break;
	case FSMSTATE_TxRx:
		ASSERT("should wait for IRQ processing");
		break;
	}
}

void CHW_SPIx::Init_SPIx()
{
	/* Disable SPI Interrupts - it would be enabled on comm starts */
	Disable_SPIx_IT_All();
	NVIC_ClearPendingIRQ(SPIx_IRQn);

	Init_SPIx_Params();
	Init_SPIx_GPIO();


	Init_SPIx_DMA();
	Init_SPIx_IRQ();
}

void CHW_SPIx::Init_SPIx_GPIO()
{
	/* Initialize SPIx GPIO */
	INIT_GPIO_AF_FAST(FIRST(SPIx_SCK_GPIO_PORT),  SECOND(SPIx_SCK_GPIO_PORT),  THIRD(SPIx_SCK_GPIO_PORT, CC(AF_SPI,SPIx_N)));
#if defined(SPIx_MOSI_GPIO_PORT)
	INIT_GPIO_AF_FAST(FIRST(SPIx_MOSI_GPIO_PORT), SECOND(SPIx_MOSI_GPIO_PORT), THIRD(SPIx_MOSI_GPIO_PORT, CC(AF_SPI,SPIx_N)));
#endif
#if defined(SPIx_MISO_GPIO_PORT)
	INIT_GPIO_AF_FAST(FIRST(SPIx_MISO_GPIO_PORT), SECOND(SPIx_MISO_GPIO_PORT), THIRD(SPIx_MISO_GPIO_PORT, CC(AF_SPI,SPIx_N)));
#endif

}

void CHW_SPIx::Init_SPIx_Params()
{
	/* Initialize SPIx Clock */
	CCC(RCC_APB,SPIx_APB_NUMBER,PeriphClockCmd)(CCCC(RCC_APB,SPIx_APB_NUMBER,Periph_SPI,SPIx_N), ENABLE);
	uint32_t uiBaseFrequency = CHW_Clocks::CCC(GetAPB,SPIx_APB_NUMBER,Frequency)();
	unsigned uiMinimumPrescaler = uiBaseFrequency / m_uiMaximumBitrate; // APB1(6MHz) / 1Mbit/s = 6
	uint16_t uiBaudRatePrescaler = SPI_BaudRatePrescaler_2;
	if (uiMinimumPrescaler <= 2)        {uiBaudRatePrescaler = SPI_BaudRatePrescaler_2;   m_uiActualBitrate = uiBaseFrequency / 2;}
	else if (uiMinimumPrescaler <= 4)   {uiBaudRatePrescaler = SPI_BaudRatePrescaler_4;   m_uiActualBitrate = uiBaseFrequency / 4;}
	else if (uiMinimumPrescaler <= 8)   {uiBaudRatePrescaler = SPI_BaudRatePrescaler_8;   m_uiActualBitrate = uiBaseFrequency / 8;}
	else if (uiMinimumPrescaler <= 16)  {uiBaudRatePrescaler = SPI_BaudRatePrescaler_16;  m_uiActualBitrate = uiBaseFrequency / 16;}
	else if (uiMinimumPrescaler <= 32)  {uiBaudRatePrescaler = SPI_BaudRatePrescaler_32;  m_uiActualBitrate = uiBaseFrequency / 32;}
	else if (uiMinimumPrescaler <= 64)  {uiBaudRatePrescaler = SPI_BaudRatePrescaler_64;  m_uiActualBitrate = uiBaseFrequency / 64;}
	else if (uiMinimumPrescaler <= 128) {uiBaudRatePrescaler = SPI_BaudRatePrescaler_128; m_uiActualBitrate = uiBaseFrequency / 128;}
	else                                {uiBaudRatePrescaler = SPI_BaudRatePrescaler_256; m_uiActualBitrate = uiBaseFrequency / 256;}

	/* Initialize SPIx device with parameters */
	SPI_InitTypeDef SPI_InitStructure;
	SPI_StructInit(&SPI_InitStructure);
	SPI_InitStructure.SPI_Mode                = SPI_Mode_Master;
#if defined(SPIx_MOSI_GPIO_PORT) && defined(SPIx_MISO_GPIO_PORT)
	SPI_InitStructure.SPI_Direction           = SPI_Direction_2Lines_FullDuplex;
#elif defined(SPIx_MOSI_GPIO_PORT)
	SPI_InitStructure.SPI_Direction           = SPI_Direction_1Line_Tx;
#elif defined(SPIx_MISO_GPIO_PORT)
//	SPI_InitStructure.SPI_Direction           = SPI_Direction_1Line_Rx; /* ACTUAL MOSI PIN IS USED in master mode*/
//	SPI_InitStructure.SPI_Direction           = SPI_Direction_2Lines_RxOnly;
	SPI_InitStructure.SPI_Direction           = SPI_Direction_2Lines_FullDuplex; /* use tx for better clk gen */
#endif
	SPI_InitStructure.SPI_DataSize            = (m_nBitsCount == 16 ? SPI_DataSize_16b : SPI_DataSize_8b);
	SPI_InitStructure.SPI_CPHA                = (m_eMode == ISPI::MODE_0_1 || m_eMode == ISPI::MODE_1_1) ? SPI_CPHA_2Edge : SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_CPOL                = (m_eMode == ISPI::MODE_1_0 || m_eMode == ISPI::MODE_1_1) ? SPI_CPOL_High : SPI_CPOL_Low;
	SPI_InitStructure.SPI_NSS                 = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler   = uiBaudRatePrescaler;
	SPI_InitStructure.SPI_FirstBit            = (m_eBitsOrder == ISPI::MSB_1st) ? SPI_FirstBit_MSB : SPI_FirstBit_LSB;
	SPI_Init(SPIx, &SPI_InitStructure);

	SPI_DEBUG_INFO("Bitrate:", m_uiActualBitrate);
}

void CHW_SPIx::Init_SPIx_DMA()
{
#ifdef SPIx_DMA_TX
	/* Initialize SPIx DMA Channels */
	/* Enable SPIx DMA */
# if defined(STM32F0)
	RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_DMA,FIRST(SPIx_DMA_TX)), ENABLE);
# elif defined(STM32F4)
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(SPIx_DMA_TX)), ENABLE);
# endif
	/* Initialize SPIx DMA Tx Stream */
	DMA_StructInit(&m_DmaInitStructureTx);
	m_DmaInitStructureTx.DMA_PeripheralBaseAddr = (uint32_t)&(SPIx->DR);
	m_DmaInitStructureTx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	m_DmaInitStructureTx.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
	m_DmaInitStructureTx.DMA_MemoryInc = DMA_MemoryInc_Disable;
	m_DmaInitStructureTx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	m_DmaInitStructureTx.DMA_Mode = DMA_Mode_Normal;
	m_DmaInitStructureTx.DMA_Priority = DMA_Priority_VeryHigh;
# if defined(STM32F0)
	m_DmaInitStructureTx.DMA_DIR = DMA_DIR_PeripheralDST;
# elif defined(STM32F4)
	m_DmaInitStructureTx.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	m_DmaInitStructureTx.DMA_Channel = CC(DMA_Channel_,THIRD(SPIx_DMA_TX));
	m_DmaInitStructureTx.DMA_FIFOMode = DMA_FIFOMode_Disable;
	m_DmaInitStructureTx.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	m_DmaInitStructureTx.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	m_DmaInitStructureTx.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
# endif
	m_bTxDmaReinitRequired = true; // do it later - when Tx op starts
#endif // SPIx_DMA_TX

#ifdef SPIx_DMA_RX
	/* Enable SPIx DMA */
# if defined(STM32F0)
	RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_DMA,FIRST(SPIx_DMA_RX)), ENABLE);
# elif defined(STM32F4)
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(SPIx_DMA_RX)), ENABLE);
# endif
	/* Initialize SPIx DMA Rx Stream */
	DMA_StructInit(&m_DmaInitStructureRx);
	m_DmaInitStructureRx.DMA_PeripheralBaseAddr = (uint32_t)&(SPIx->DR);
	m_DmaInitStructureRx.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	m_DmaInitStructureRx.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
	m_DmaInitStructureRx.DMA_MemoryInc = DMA_MemoryInc_Disable;
	m_DmaInitStructureRx.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	m_DmaInitStructureRx.DMA_Mode = DMA_Mode_Normal;
	m_DmaInitStructureRx.DMA_Priority = DMA_Priority_VeryHigh;
# if defined(STM32F0)
	m_DmaInitStructureRx.DMA_DIR = DMA_DIR_PeripheralSRC;
# elif defined(STM32F4)
	m_DmaInitStructureRx.DMA_DIR = DMA_DIR_PeripheralToMemory;
	m_DmaInitStructureRx.DMA_Channel = CC(DMA_Channel_,THIRD(SPIx_DMA_RX));
	m_DmaInitStructureRx.DMA_FIFOMode = DMA_FIFOMode_Disable;
	m_DmaInitStructureRx.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	m_DmaInitStructureRx.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	m_DmaInitStructureRx.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
# endif
	m_bRxDmaReinitRequired = true; // do it later - when Rx op starts
#endif // SPIx_DMA_RX
}

void CHW_SPIx::Init_SPIx_IRQ()
{
	/* Configure NVIC priority Group */
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	/* Enable the IRQ channel */
	NVIC_INIT(SPIx_IRQn, SPIx_IT_GROUP_PRIO, SPIx_IT_SUBPRIO);

#ifdef DMATX_IRQID
	/* Configure NVIC for DMA Tx channel interrupt */
	NVIC_INIT(DMATX_IRQn, SPIx_IT_GROUP_PRIO, SPIx_IT_DMATx_SUBPRIO);
#endif // DMATX_IRQID
#if defined(DMARX_IRQID) && (!defined(DMATX_IRQID) || (DMATX_IRQID != DMARX_IRQID))
	/* Configure NVIC for DMA Rx channel interrupt */
	NVIC_INIT(DMARX_IRQn, SPIx_IT_GROUP_PRIO, SPIx_IT_DMARx_SUBPRIO);
#endif // DMARX_IRQID
}

void CHW_SPIx::Cancel_SPIx()
{
	SPI_DEBUG_INFO("Cancel");

	SPI_Cmd(SPIx, DISABLE);

	Disable_SPIx_DMA();
#ifdef SPIx_DMA_TX
	DMATX_Cmd(DISABLE);
	DMATX_ITConfig(DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMATX_ClearITPendingBit(TC, TE);
	NVIC_ClearPendingIRQ(DMATX_IRQn);
#endif // SPIx_DMA_TX
#ifdef SPIx_DMA_RX
	DMARX_Cmd(DISABLE);
	DMARX_ITConfig(DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMARX_ClearITPendingBit(TC, TE);
# if defined(DMARX_IRQID) && (!defined(DMATX_IRQID) || (DMATX_IRQID != DMARX_IRQID))
	NVIC_ClearPendingIRQ(DMARX_IRQn);
# endif
#endif // SPIx_DMA_RX

	/* Disable SPI Interrupts - it would be enabled on comm starts */
	Disable_SPIx_IT_All();
	Clear_SPIx_Flags_OVR_UDR();
	Clear_SPIx_Pending_CRCERR();
	NVIC_ClearPendingIRQ(SPIx_IRQn);

	m_bTxDmaReinitRequired = true;
	m_bRxDmaReinitRequired = true;
}

void CHW_SPIx::Deinit_SPIx()
{
	Cancel_SPIx();

	/* Disable SPIx Device */
	SPI_Cmd(SPIx, DISABLE);
	IF_STM32F0( SPI_I2S_DeInit(SPIx) );
	IF_STM32F4( SPI_DeInit(SPIx) );

	/* Deinitialize SPIx GPIO */
	DEINIT_GPIO(FIRST(SPIx_SCK_GPIO_PORT), SECOND(SPIx_SCK_GPIO_PORT));
#if defined(SPIx_MOSI_GPIO_PORT)
	DEINIT_GPIO(FIRST(SPIx_MOSI_GPIO_PORT), SECOND(SPIx_MOSI_GPIO_PORT));
#endif
#if defined(SPIx_MISO_GPIO_PORT)
	DEINIT_GPIO(FIRST(SPIx_MISO_GPIO_PORT), SECOND(SPIx_MISO_GPIO_PORT));
#endif

    /* Deinitialize DMA Channels */
#ifdef SPIx_DMA_TX
    DMA_DeInit(DMATX_Stream);
#endif // SPIx_DMA_TX
#ifdef SPIx_DMA_RX
    DMA_DeInit(DMARX_Stream);
#endif // SPIx_DMA_RX

    m_bTxDmaReinitRequired = true;
    m_bRxDmaReinitRequired = true;
}

void CHW_SPIx::DoTimeoutDowncount()
{
	if (m_nTimeoutCountdown) {
		--m_nTimeoutCountdown;
	} else {
		DEBUG_INFO("SPI: Timeout");
		Terminate_Session();
	}
}


#ifdef SPIx_DMA_TX
void CHW_SPIx::DoPrepareTxDMA(unsigned uiLength)
{
	ASSERTE(uiLength);
	static const uint8_t g_uiTxStubBuf = 0;
	bool bHasRealData = (m_pCurrentTxChunk.uiLength);
	uint32_t uiMemoryBaseAddr = (bHasRealData
			?(uint32_t)m_pCurrentTxChunk.pData
			:(uint32_t)&g_uiTxStubBuf);
	bool bIgnoreMemoryInc = (uiLength == 1);
	bool bNeedMemoryInc = (bHasRealData && (uiLength > 1));
	bool bMatchMemoryInc = 0
			|| (bNeedMemoryInc && m_DmaInitStructureTx.DMA_MemoryInc == DMA_MemoryInc_Enable)
			|| (!bNeedMemoryInc && m_DmaInitStructureTx.DMA_MemoryInc == DMA_MemoryInc_Disable);

	ASSERTE(DMATX_GetCmdStatus() == DISABLE);
	if (!m_bTxDmaReinitRequired && (bIgnoreMemoryInc || bMatchMemoryInc)) {
		// fast init
		DMATX_MemoryTargetConfig(uiMemoryBaseAddr, 0);
		DMATX_SetCurrDataCounter(uiLength);
		SPI_DEBUG_INFO("Tx DMA. Length:", uiLength);
		if (bHasRealData) {
			SPI_DEBUG_INFO("Tx DMA. Data:", (*(m_pCurrentTxChunk.pData) << 24) |
					((m_pCurrentTxChunk.uiLength > 1) ? *(m_pCurrentTxChunk.pData + 1) << 16 : 0) |
					((m_pCurrentTxChunk.uiLength > 2) ? *(m_pCurrentTxChunk.pData + 2) << 8 : 0) |
					((m_pCurrentTxChunk.uiLength > 3) ? *(m_pCurrentTxChunk.pData + 2) << 0 : 0)
			);
		}
	} else {
		// full init
 		m_DmaInitStructureTx.DMA_BufferSize = uiLength;
#if defined(STM32F0)
		m_DmaInitStructureTx.DMA_MemoryBaseAddr = uiMemoryBaseAddr;
#elif defined(STM32F4)
		m_DmaInitStructureTx.DMA_Memory0BaseAddr = uiMemoryBaseAddr;
#endif
		m_DmaInitStructureTx.DMA_MemoryInc = (bNeedMemoryInc || bIgnoreMemoryInc)
			? DMA_MemoryInc_Enable
			: DMA_MemoryInc_Disable;
		DMA_Init(DMATX_Stream, &m_DmaInitStructureTx);
		m_bTxDmaReinitRequired = false;
		SPI_DEBUG_INFO("Tx DMA. Length:", uiLength);
		if (bHasRealData) {
			SPI_DEBUG_INFO("Tx DMA. Data:", (*(m_pCurrentTxChunk.pData) << 24) |
					((m_pCurrentTxChunk.uiLength > 1) ? *(m_pCurrentTxChunk.pData + 1) << 16 : 0) |
					((m_pCurrentTxChunk.uiLength > 2) ? *(m_pCurrentTxChunk.pData + 2) << 8 : 0) |
					((m_pCurrentTxChunk.uiLength > 3) ? *(m_pCurrentTxChunk.pData + 2) << 0 : 0)
			);
		}
	}

	DMATX_ITConfig(DMA_IT_TE, ENABLE);
#ifdef DEBUG
# if defined(STM32F0)
	if (DMATX_GetITStatus(GL))  {SPI_DEBUG_INFO("DMA Tx GL");}
#elif defined(STM32F4)
	if (DMATX_GetITStatus(FE))  {SPI_DEBUG_INFO("DMA Tx FE");}
	if (DMATX_GetITStatus(DME)) {SPI_DEBUG_INFO("DMA Tx DME");}
# endif
	if (DMATX_GetITStatus(TE))  {SPI_DEBUG_INFO("DMA Tx TE");}
	if (DMATX_GetITStatus(HT))  {SPI_DEBUG_INFO("DMA Tx HT");}
	if (DMATX_GetITStatus(TC))  {SPI_DEBUG_INFO("DMA Tx TC");}
#endif
#if defined(STM32F0)
	DMATX_ClearFlag(GL, TE, HT, TC);
	ASSERTE(!DMATX_GetITStatus(GL));
#elif defined(STM32F4)
	DMATX_ClearFlag(FE, DME, TE, HT, TC);
	ASSERTE(!DMATX_GetITStatus(FE));
	ASSERTE(!DMATX_GetITStatus(DME));
#endif
	ASSERTE(!DMATX_GetITStatus(TE));
	ASSERTE(!DMATX_GetITStatus(HT));
	ASSERTE(!DMATX_GetITStatus(TC));
	DMATX_Cmd(ENABLE);
	SPI_DEBUG_INFO("Tx DMA Activated:", (DMATX_GetCmdStatus()?0x01:0)
			| (DMATX_GetITStatus(TC)?0x10:0x00));
}
#endif // SPIx_DMA_TX


#ifdef SPIx_DMA_RX
void CHW_SPIx::DoPrepareRxDMA(unsigned uiLength)
{
	ASSERTE(uiLength);
	static uint8_t g_uiRxStubBuf = 0;
	bool bHasRealData = (m_pCurrentRxChunk.uiLength);
	uint32_t uiMemoryBaseAddr = (bHasRealData
			?(uint32_t)m_pCurrentRxChunk.pData
			:(uint32_t)&g_uiRxStubBuf);
	bool bIgnoreMemoryInc = (uiLength == 1);
	bool bNeedMemoryInc = (bHasRealData && (uiLength > 1));
	bool bMatchMemoryInc = 0
			|| (bNeedMemoryInc && m_DmaInitStructureRx.DMA_MemoryInc == DMA_MemoryInc_Enable)
			|| (!bNeedMemoryInc && m_DmaInitStructureRx.DMA_MemoryInc == DMA_MemoryInc_Disable);

	ASSERTE(DMARX_GetCmdStatus() == DISABLE);
	if (!m_bRxDmaReinitRequired && (bIgnoreMemoryInc || bMatchMemoryInc)) {
		// fast init
		DMARX_MemoryTargetConfig(uiMemoryBaseAddr, 0);
		DMARX_SetCurrDataCounter(uiLength);
		SPI_DEBUG_INFO("Rx DMA. Length:", uiLength);
		if (bHasRealData) {
			SPI_DEBUG_INFO("Rx DMA ptr:", (uint32_t)m_pCurrentRxChunk.pData);
		}
	} else {
		// full init
 		m_DmaInitStructureRx.DMA_BufferSize = uiLength;
#if defined(STM32F0)
		m_DmaInitStructureRx.DMA_MemoryBaseAddr = uiMemoryBaseAddr;
#elif defined(STM32F4)
		m_DmaInitStructureRx.DMA_Memory0BaseAddr = uiMemoryBaseAddr;
#endif
		m_DmaInitStructureRx.DMA_MemoryInc = (bNeedMemoryInc || bIgnoreMemoryInc)
			? DMA_MemoryInc_Enable
			: DMA_MemoryInc_Disable;
		DMA_Init(DMARX_Stream, &m_DmaInitStructureRx);
		m_bRxDmaReinitRequired = false;
		SPI_DEBUG_INFO("Rx DMA. Length:", uiLength);
		if (bHasRealData) {
			SPI_DEBUG_INFO("Rx DMA ptr:", (uint32_t)m_pCurrentRxChunk.pData);
		}
	}

#ifdef DEBUG
# if defined(STM32F0)
	if (DMARX_GetITStatus(GL)) {SPI_DEBUG_INFO("DMA Rx GL");}
# elif defined(STM32F4)
	if (DMARX_GetITStatus(FE)) {SPI_DEBUG_INFO("DMA Rx FE");}
	if (DMARX_GetITStatus(DME)) {SPI_DEBUG_INFO("DMA Rx DME");}
# endif
	if (DMARX_GetITStatus(TE)) {SPI_DEBUG_INFO("DMA Rx TE");}
	if (DMARX_GetITStatus(HT)) {SPI_DEBUG_INFO("DMA Rx HT");}
	if (DMARX_GetITStatus(TC)) {SPI_DEBUG_INFO("DMA Rx TC");}
#endif
#if defined(STM32F0)
	DMARX_ClearFlag(GL, TE, HT, TC);
	ASSERTE(!DMARX_GetITStatus(GL));
#elif defined(STM32F4)
	DMARX_ClearFlag(FE, DME, TE, HT, TC);
	ASSERTE(!DMARX_GetITStatus(FE));
	ASSERTE(!DMARX_GetITStatus(DME));
#endif
	ASSERTE(!DMARX_GetITStatus(TE));
	ASSERTE(!DMARX_GetITStatus(HT));
	ASSERTE(!DMARX_GetITStatus(TC));
	DMARX_Cmd(ENABLE);
	SPI_DEBUG_INFO("Rx DMA Activated:", (DMARX_GetCmdStatus()?0x01:0) | (DMARX_GetITStatus(TC)?0x10:0x00));
}
#endif // SPIx_DMA_RX



void CHW_SPIx::Stop_SPIx_Session()
{
	ASSERTE(m_pCurrentSPIDevice != NULL);

	Disable_SPIx_IT_All();
	NVIC_ClearPendingIRQ(SPIx_IRQn);
	SPI_Cmd(SPIx, DISABLE);

	m_pCurrentSPIDevice->Release();
}

void CHW_SPIx::Send_SPIx_CLKs()
{
	ASSERTE(m_pCurrentRxChunk.uiLength);

	if (m_pCurrentRxChunk.uiLength >= 2) {
		SPI_DataSizeConfig(SPIx, SPI_DataSize_16b); // next TX is 16 bits
		IF_STM32F0( SPI_I2S_SendData16(SPIx, 0) );
		IF_STM32F4( SPI_I2S_SendData(SPIx, 0) );
		SPI_DEBUG_INFO("TX 16: -");
	} else {
		SPI_DataSizeConfig(SPIx, SPI_DataSize_8b); // next TX is 8 bits
		IF_STM32F0( SPI_SendData8(SPIx, 0) );
		IF_STM32F4( SPI_I2S_SendData(SPIx, 0) );
		SPI_DEBUG_INFO("TX 8: -");
	}
}

bool CHW_SPIx::Send_SPIx_Data()
{
	if (m_pCurrentTxChunk.uiLength >= 2) {
		const uint16_t uiData = m_pCurrentTxChunk.pData ? b_swap2(*(const uint16_t*)(m_pCurrentTxChunk.pData)) : 0;
		m_pCurrentTxChunk.pData += 2;
		m_pCurrentTxChunk.uiLength -= 2;
		if (!m_pCurrentTxChunk.uiLength) {
			const CChainedConstChunks* pNext = m_pCurrentTxChunk.GetNextItem();
			if (pNext) {
				m_pCurrentTxChunk = *pNext;
			} else {
				m_pCurrentTxChunk.Clean();
			}
		}
		SPI_DataSizeConfig(SPIx, SPI_DataSize_16b); // next TX is 16 bits
		IF_STM32F0( SPI_I2S_SendData16(SPIx, uiData) );
		IF_STM32F4( SPI_I2S_SendData(SPIx, uiData) );
		SPI_DEBUG_INFO("TX 16:", uiData);
	} else {
		ASSERTE(m_pCurrentTxChunk.uiLength == 1);
		const uint8_t uiData = m_pCurrentTxChunk.pData ? *m_pCurrentTxChunk.pData : 0;
		const CChainedConstChunks* pNext = m_pCurrentTxChunk.GetNextItem();
		if (pNext) {
			m_pCurrentTxChunk = *pNext;
		} else {
			m_pCurrentTxChunk.Clean();
		}
		SPI_DataSizeConfig(SPIx, SPI_DataSize_8b); // next TX is 8 bits
		IF_STM32F0( SPI_SendData8(SPIx, uiData) );
		IF_STM32F4( SPI_I2S_SendData(SPIx, uiData) );
		SPI_DEBUG_INFO("TX 8:", uiData);
	}

	return m_pCurrentTxChunk.uiLength;
}

bool CHW_SPIx::Receive_SPIx_Data()
{
	if (m_pCurrentRxChunk.uiLength >= 2) {
		uint16_t uiData = IF_STM32F0( SPI_I2S_ReceiveData16(SPIx) ) IF_STM32F4( SPI_I2S_ReceiveData(SPIx) );
		if (m_pCurrentRxChunk.pData) {
			*(uint16_t*)(m_pCurrentRxChunk.pData) = b_swap2(uiData);
			SPI_DEBUG_INFO("RX 16:", uiData);
		} else {
			SPI_DEBUG_INFO("skip RX 16:", uiData);
		}
		m_pCurrentRxChunk.pData += 2;
		m_pCurrentRxChunk.uiLength -= 2;
		if (!m_pCurrentRxChunk.uiLength) {
			const CChainedIoChunks* pNext = m_pCurrentRxChunk.GetNextItem();
			if (pNext) {
				m_pCurrentRxChunk = *pNext;
				SPI_DEBUG_INFO("RX next chunk p:", (uint32_t)m_pCurrentRxChunk.pData);
				SPI_DEBUG_INFO("RX next chunk l:", m_pCurrentRxChunk.uiLength);
			} else {
				m_pCurrentRxChunk.Clean();
				SPI_DEBUG_INFO("RX last chunk finishing");
			}
		}
	} else if (m_pCurrentRxChunk.uiLength == 1) {
		uint8_t uiData = IF_STM32F0( SPI_ReceiveData8(SPIx) ) IF_STM32F4( SPI_I2S_ReceiveData(SPIx) );
		if (m_pCurrentRxChunk.pData) {
			*(uint8_t*)(m_pCurrentRxChunk.pData) = uiData;
			SPI_DEBUG_INFO("RX 8:", uiData);
		} else {
			SPI_DEBUG_INFO("skip RX 8:", uiData);
		}
		++m_pCurrentRxChunk.pData;
		--m_pCurrentRxChunk.uiLength;
		if (!m_pCurrentRxChunk.uiLength) {
			const CChainedIoChunks* pNext = m_pCurrentRxChunk.GetNextItem();
			if (pNext) {
				m_pCurrentRxChunk = *pNext;
			} else {
				m_pCurrentRxChunk.Clean();
			}
		}
	} else {
		ASSERTE(m_pCurrentRxChunk.uiLength == 0);
		uint16_t uiDataToIgnore = IF_STM32F0( SPI_I2S_ReceiveData16(SPIx) ) IF_STM32F4( SPI_I2S_ReceiveData(SPIx) );
		if (uiDataToIgnore) {} // suppress warning
	}
#ifdef STM32F0
	SPI_RxFIFOThresholdConfig(SPIx, m_pCurrentRxChunk.uiLength == 1 ? SPI_RxFIFOThreshold_QF : SPI_RxFIFOThreshold_HF);
#endif
	return m_pCurrentRxChunk.uiLength;
}

bool CHW_SPIx::Check_SPIx_Flag_Busy()
{
	return (
			IF_STM32F0( SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_BSY) )
			IF_STM32F4( SPI_GetFlagStatus(SPIx, SPI_FLAG_BSY) )
	);
}
bool CHW_SPIx::Check_SPIx_Flag_TXE()
{
	return (
			IF_STM32F0( SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) )
			IF_STM32F4( SPI_GetFlagStatus(SPIx, SPI_FLAG_TXE) )
	);
}
bool CHW_SPIx::Check_SPIx_IT_TXE()
{
	return (
			IF_STM32F0( SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_TXE) )
			IF_STM32F4( SPI_GetITStatus(SPIx, SPI_IT_TXE) )
	);
}
bool CHW_SPIx::Check_SPIx_IT_RXNE()
{
	return (
			IF_STM32F0( SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_RXNE) )
			IF_STM32F4( SPI_GetITStatus(SPIx, SPI_IT_RXNE) )
	);
}
bool CHW_SPIx::Check_SPIx_IT_MODF()
{
	return (
			IF_STM32F0( SPI_I2S_GetITStatus(SPIx, SPI_IT_MODF) )
			IF_STM32F4( SPI_GetITStatus(SPIx, SPI_IT_MODF) )
	);
}
bool CHW_SPIx::Check_SPIx_IT_OVR()
{
	return (
			IF_STM32F0( SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_OVR) )
			IF_STM32F4( SPI_GetITStatus(SPIx, SPI_IT_OVR) )
	);
}
bool CHW_SPIx::Check_SPIx_IT_FRE()
{
	return (
			IF_STM32F0( SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_FRE) )
			IF_STM32F4( SPI_GetITStatus(SPIx, SPI_I2S_IT_TIFRFE) )
	);
}

bool CHW_SPIx::Check_SPIx_IT_CRCERR()
{
	return (
			IF_STM32F0( false )
			IF_STM32F4( SPI_GetITStatus(SPIx, SPI_IT_CRCERR) )
	);
}

void CHW_SPIx::Clear_SPIx_Flags_OVR_UDR()
{
	IF_STM32F0(SPI_I2S_ReceiveData16(SPIx); SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_RXNE); )
	IF_STM32F4(SPI_ReceiveData(SPIx); SPI_GetITStatus(SPIx, SPI_IT_RXNE); )
}

void CHW_SPIx::Clear_SPIx_Pending_CRCERR()
{
	IF_STM32F4(SPI_ClearITPendingBit(SPIx, SPI_IT_CRCERR) );
}

void CHW_SPIx::Enable_SPIx_IT_TXE()
{
#ifdef STM32F0
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_TXE, ENABLE);
#endif
#ifdef STM32F4
//	SPI_I2S_ClearITPendingBit(SPIx, SPI_IT_TXE);
	SPI_I2S_ITConfig(SPIx, SPI_IT_TXE, ENABLE);
#endif
}

void CHW_SPIx::Enable_SPIx_IT_RXNE()
{
#ifdef STM32F0
	while(SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE)) SPI_I2S_ReceiveData16(SPIx); // Clear RXNE
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_RXNE, ENABLE);
#endif
#ifdef STM32F4
//	SPI_I2S_ClearITPendingBit(SPIx, SPI_IT_RXNE);
	SPI_I2S_ITConfig(SPIx, SPI_IT_RXNE, ENABLE);
#endif
}

void CHW_SPIx::Enable_SPIx_IT_ERR()
{
#ifdef STM32F0
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_ERR, ENABLE);
#endif
#ifdef STM32F4
	SPI_I2S_ClearITPendingBit(SPIx, SPI_IT_CRCERR);
	SPI_I2S_ITConfig(SPIx, SPI_IT_ERR, ENABLE);
#endif
}

void CHW_SPIx::Disable_SPIx_IT_TXE()
{
#ifdef STM32F0
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_TXE, DISABLE);
#endif
#ifdef STM32F4
	SPI_I2S_ITConfig(SPIx, SPI_IT_TXE, DISABLE);
#endif
}

void CHW_SPIx::Disable_SPIx_IT_RXNE()
{
#ifdef STM32F0
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_RXNE, DISABLE);
#endif
#ifdef STM32F4
	SPI_I2S_ITConfig(SPIx, SPI_IT_RXNE, DISABLE);
#endif
}

void CHW_SPIx::Disable_SPIx_IT_ERR()
{
#ifdef STM32F0
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_ERR, DISABLE);
#endif
#ifdef STM32F4
	SPI_I2S_ITConfig(SPIx, SPI_IT_ERR, DISABLE);
#endif
}

void CHW_SPIx::Disable_SPIx_IT_All()
{
#ifdef STM32F0
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_TXE, DISABLE);
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_RXNE, DISABLE);
	SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_ERR, DISABLE);
#endif
#ifdef STM32F4
	SPI_I2S_ITConfig(SPIx, SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR, DISABLE);
#endif
}

void CHW_SPIx::Disable_SPIx_DMA()
{
#ifdef STM32F0
	SPI_I2S_DMACmd(SPIx, SPI_I2S_DMAReq_Tx | SPI_I2S_DMAReq_Rx, DISABLE);
#endif
#ifdef STM32F4
	SPI_I2S_DMACmd(SPIx, SPI_DMAReq_Tx | SPI_DMAReq_Rx, DISABLE);
#endif
}

void CHW_SPIx::Finish_Session()
{
	SPI_DEBUG_INFO("Finish");
#ifdef SPIx_DMA_TX
	SPI_I2S_DMACmd(SPIx, SPI_I2S_DMAReq_Tx, DISABLE);
	DMATX_Cmd(DISABLE);
#endif
#ifdef SPIx_DMA_RX
	SPI_I2S_DMACmd(SPIx, SPI_I2S_DMAReq_Rx, DISABLE);
	DMARX_Cmd(DISABLE);
	NVIC_ClearPendingIRQ(DMARX_IRQn);
#endif

	Stop_SPIx_Session();
	CHECKED_CHANGE_SPI_STATE(FSMSTATE_TxRx, FSMSTATE_Idle, SPI_TIMEOUT(1));
	NotifyPktDone();
}

void CHW_SPIx::Terminate_Session()
{
	SPI_DEBUG_INFO("Terminate");

	Cancel_SPIx();

	if (m_eFSMState == FSMSTATE_TxRx) {
		Stop_SPIx_Session();
		NotifyPktError();
	}

	m_pCurrentTxChunk.Clean();
	m_pCurrentRxChunk.Clean();

	CHANGE_SPI_STATE(FSMSTATE_Init, 0);

	ProcessSPIxFSM();
}

void CHW_SPIx::NotifyPktDone()
{
	SPI_DEBUG_INFO("notify done");
	ASSERTE(m_pCurrentSPIDevice != NULL);
	m_pCurrentSPIDevice->OnSPIDone();
}

void CHW_SPIx::NotifyPktError()
{
	SPI_DEBUG_INFO("notify error");
	ASSERTE(m_pCurrentSPIDevice != NULL);
	m_pCurrentSPIDevice->OnSPIError();
}

void CHW_SPIx::NotifySPIIdle()
{
	IDeviceSPI* pDeviceSPI;
	if (m_qIdleNotifyQueue.Pop(pDeviceSPI)) {
		pDeviceSPI->OnSPIIdle();
		SPI_DEBUG_INFO("idle callback");
	}
}

CHW_SPIx g_HW_SPIx;


class CHW_SPIx_InterfaceWrapper :
		public ISPI
{
public: // ISPI
	virtual void Init(unsigned uiSpeed);
	virtual void Deinit();
	virtual bool Start_SPIx_Session(IDeviceSPI* pDeviceSPI, const CChainedConstChunks* pHeadTxChunk, const CChainedIoChunks* pHeadRxChunk);
	virtual void RequestSPIIdleNotify(IDeviceSPI* pDeviceSPI);
};

/*virtual*/
void CHW_SPIx_InterfaceWrapper::Init(unsigned uiSpeed)
{
	IMPLEMENTS_INTERFACE_METHOD(ISPI::Init(uiSpeed));
	g_HW_SPIx.Init(uiSpeed);
	CHW_Clocks::RegisterSysTickCallback(&g_HW_SPIx);           INDIRECT_CALL(g_HW_SPIx.OnSysTick());
}

/*virtual*/
void CHW_SPIx_InterfaceWrapper::Deinit()
{
	IMPLEMENTS_INTERFACE_METHOD(ISPI::Deinit());
	g_HW_SPIx.Deinit();
}

/*virtual*/
bool CHW_SPIx_InterfaceWrapper::Start_SPIx_Session(IDeviceSPI* pDeviceSPI, const CChainedConstChunks* pHeadTxChunk, const CChainedIoChunks* pHeadRxChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISPI::Start_SPIx_Session(pDeviceSPI, pHeadTxChunk, pHeadRxChunk));
	return g_HW_SPIx.Start_SPIx_Session(pDeviceSPI, pHeadTxChunk, pHeadRxChunk);

}

/*virtual*/
void CHW_SPIx_InterfaceWrapper::RequestSPIIdleNotify(IDeviceSPI* pDeviceSPI)
{
	IMPLEMENTS_INTERFACE_METHOD(ISPI::RequestSPIIdleNotify(pDeviceSPI));
	g_HW_SPIx.RequestSPIIdleNotify(pDeviceSPI);
}

CHW_SPIx_InterfaceWrapper g_HW_SPIx_InterfaceWrapper;

#undef SPIx_N
/* SPI1 use APB2 clock, but SPI2, SPI3 uses APB1 clock */
#undef SPIx_SCK_GPIO_PORT
#undef SPIx_MISO_GPIO_PORT
#undef SPIx_MOSI_GPIO_PORT
#undef SPIx_DMA_TX
#undef SPIx_DMA_RX
#undef SPIx_APB_NUMBER
#undef DMARX_IRQID
#undef DMATX_IRQID
#undef CHW_SPIx
#undef g_HW_SPIx
#undef CHW_SPIx_InterfaceWrapper
#undef g_HW_SPIx_InterfaceWrapper
