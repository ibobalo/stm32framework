// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define AINx_PORT          A,6,6
// #include "hw_adc_in.cxx"
// #define AINx_CHANNEL ADC_CHANNEL_N_TEMPSENS
// #include "hw_adc_in.cxx"

#if !defined(AINx_PORT) && !defined(AINx_CHANNEL)
#error params must be defined : AINx_PORT or AINx_CHANNEL
//next defines for editor hints
#define AINx_PORT          A,6,6
#endif

#include "stdafx.h"
#include "hw_adc_in.h"
#include "../hw_macros.h"
#include "../debug.h"

#if defined(AINx_PORT)
# define AINx_N THIRD(AINx_PORT)
#elif defined(AINx_CHANNEL)
# define AINx_N AINx_CHANNEL
#endif

/*************************************************************************************/
// forward declaration of template functions specialization

template<> void CHW_AIN<AINx_N>::Connect();
template<> void CHW_AIN<AINx_N>::Disconnect();
template<> void CHW_AIN<AINx_N>::Deinit();
template<> CHW_AIN<AINx_N>::value_type CHW_AIN<AINx_N>::GetMaxValue();
template<> CHW_AIN<AINx_N>::value_type CHW_AIN<AINx_N>::GetValue();
template<> void CHW_AIN<AINx_N>::SetValueTargetInterface(IValueReceiver<value_type>* pValueTarget);
template<> void CHW_AIN<AINx_N>::NotifySingleValue();
template<> void CHW_AIN<AINx_N>::NotifyMultipleValues(unsigned nCount);


/*************************************************************************************/
// template functions specialization

template<>
bool CHW_AIN<AINx_N>::IsUsed() {return true;}

/*static*/
template<>
void CHW_AIN<AINx_N>::SetScanRank(unsigned nScanRank)
{
	m_nScanRank = nScanRank;
}

/*static*/
template<>
void CHW_AIN<AINx_N>::SetAdcDataPtr(volatile value_type* pData, unsigned nDataStep, unsigned nDataCount)
{
	m_pData = pData;
	m_nDataStep = nDataStep;
	m_nDataCount = nDataCount;
}

/*static*/
template<>
void CHW_AIN<AINx_N>::Connect()
{
#if (AINx_N == ADC_CHANNEL_N_TEMPSENS)
	IF_STM32F0(ADC_TempSensorCmd(ENABLE))
	IF_STM32F4(ADC_TempSensorVrefintCmd(ENABLE));
#elif (AINx_N == ADC_CHANNEL_N_VREFINT)
	IF_STM32F0(ADC_VrefintCmd(ENABLE));
	IF_STM32F4(ADC_TempSensorVrefintCmd(ENABLE));
#elif (AINx_N == ADC_CHANNEL_N_VBAT)
	IF_STM32F0(ADC_VbatCmd(ENABLE));
	IF_STM32F4(ADC_VBATCmd(ENABLE));
#endif
}

/*static*/
template<>
void CHW_AIN<AINx_N>::Disconnect()
{
#if (AINx_N == ADC_CHANNEL_N_TEMPSENS)
	IF_STM32F0(ADC_TempSensorCmd(DISABLE))
	IF_STM32F4(ADC_TempSensorVrefintCmd(DISABLE));
#elif (AINx_N == ADC_CHANNEL_N_VREFINT)
	IF_STM32F0(ADC_VrefintCmd(DISABLE));
	IF_STM32F4(ADC_TempSensorVrefintCmd(DISABLE));
#elif (AINx_N == ADC_CHANNEL_N_VBAT)
	IF_STM32F0(ADC_VbatCmd(DISABLE));
	IF_STM32F4(ADC_VBATCmd(DISABLE));
#endif
}

/*static*/
template<>
void CHW_AIN<AINx_N>::Deinit()
{
#ifdef AINx_PORT
//	CHW_ADCs::DisableADCChannel(THIRD(AINx_PORT));
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(AINx_PORT));
	GPIO_Init(CC(GPIO,FIRST(AINx_PORT)), &GPIO_InitStructure);
#endif
}

/*static*/
template<>
CHW_AIN<AINx_N>::value_type CHW_AIN<AINx_N>::GetMaxValue()
{
	return 0x0FFF;
}

/*static*/
template<>
CHW_AIN<AINx_N>::value_type CHW_AIN<AINx_N>::GetValue()
{
	return *m_pData;
}

/*static*/
template<>
void CHW_AIN<AINx_N>::SetValueTargetInterface(IValueReceiver<value_type>* pValueTarget)
{
	m_pValueTarget = pValueTarget;
}

/*static*/
template<>
void CHW_AIN<AINx_N>::NotifySingleValue()
{
	if (!m_pValueTarget) return;

	value_type value = *m_pData;
	m_pValueTarget->SetValue(value);
}

/*static*/
template<>
void CHW_AIN<AINx_N>::NotifyMultipleValues(unsigned nCount)
{
	if (!m_pValueTarget) return;

	volatile value_type* pValue = m_pData;
	for (unsigned n = 0; n < nCount; ++n) {
		value_type value = *pValue;
		m_pValueTarget->SetValue(value);
		pValue += m_nDataStep;
	}
}

#undef AINx_PORT
#undef AINx_CHANNEL
#undef AINx_N
