#ifndef MCU_H_INCLUDED
#define MCU_H_INCLUDED

#include "hw_clk.h"
#include "interfaces/notify.h"
#include <stdint.h>

/*
 * critical section
 */
#define ENTER_CRITICAL_SECTION {__disable_irq(); DEBUG_CS_START(); }
#define LEAVE_CRITICAL_SECTION {DEBUG_CS_END(); __enable_irq(); }
//#define CRITICAL_SECTION for(bool critical_section_flag = (__disable_irq(),true); critical_section_flag; critical_section_flag = (__enable_irq(),false))


#if defined(DEBUG) || defined(USE_FULL_ASSERT)
#include "hw/stm32f_irqs.h"
#include "util/atomic.h"
extern volatile unsigned g_nHandlingIRQ;
extern volatile unsigned g_nIRQDisabled;
#define ISR_REDIRECTED_DECLARE(name)     extern bool name;
ISR_TABLE_ITERATOR_EXT(ISR_REDIRECTED_DECLARE, Handler_REDIRECTED);
#ifdef __cplusplus
template<unsigned N> struct IRQnHandler_REDIRECTED {static bool bREDIRECTED;};
#endif
#define ASSERT_IF_INTERRUPT_HANDLER(msg, ...) { ASSERTE(!g_nHandlingIRQ, msg, ##__VA_ARGS__); }
#define ASSERT_IF_NOT_INTERRUPT_HANDLER(msg, ...) { ASSERTE(g_nHandlingIRQ, msg, ##__VA_ARGS__); }
#define ASSERT_IF_CRITICAL_SECTION(msg, ...) { ASSERTE(!g_nIRQDisabled, msg, ##__VA_ARGS__); }
#define ASSERT_IF_NOT_CRITICAL_SECTION(msg, ...) { ASSERTE(g_nIRQDisabled, msg, ##__VA_ARGS__); }
#define ASSERT_IF_NOT_IRQ_REDIRECTED(handler_name, msg, ...) { ASSERTE(handler_name##_REDIRECTED, msg, ##__VA_ARGS__); }
#define ASSERT_IF_NOT_IRQn_REDIRECTED(N, msg, ...) { ASSERTE(IRQnHandler_REDIRECTED<N>::bREDIRECTED, msg, ##__VA_ARGS__); }
#define DEBUG_IRQ_REDIRECTED(handler_name) bool handler_name##_REDIRECTED = true;
#define DEBUG_IRQn_REDIRECTED(N) /*static*/template<> bool IRQnHandler_REDIRECTED<N>::bREDIRECTED = true;
#define DEBUG_IRQ_BEGIN(handler_name) {AtomicInc(&g_nHandlingIRQ); ASSERT_IF_CRITICAL_SECTION("IRQ inside CS"); IRQ_DEBUG_INFO(">" handler_name ":", g_nHandlingIRQ);}
#define DEBUG_IRQ_END(handler_name) {ASSERT_IF_CRITICAL_SECTION("IRQ inside CS"); IRQ_DEBUG_INFO(">" handler_name ":", g_nHandlingIRQ); AtomicDec(&g_nHandlingIRQ); }
#define DEBUG_CS_START() {/*ASSERT_IF_INTERRUPT_HANDLER("CS inside IRQ");*/ ASSERT_IF_CRITICAL_SECTION("recursive CS"); ++g_nIRQDisabled;}
#define DEBUG_CS_END() {/*ASSERT_IF_INTERRUPT_HANDLER("CS inside IRQ");*/ ASSERT_IF_NOT_CRITICAL_SECTION("unbalanced CS"); --g_nIRQDisabled;}
#else
#define DEBUG_IRQ_REDIRECTED(handler_name)
#define DEBUG_IRQn_REDIRECTED(handler_name)
#define DEBUG_IRQ_BEGIN(handler_name) { }
#define DEBUG_IRQ_END(handler_name) { }
#define DEBUG_CS_START() { }
#define DEBUG_CS_END() { }
#define ASSERT_IF_INTERRUPT_HANDLER(msg, ...) { }
#define ASSERT_IF_NOT_INTERRUPT_HANDLER(msg, ...) { }
#define ASSERT_IF_CRITICAL_SECTION(msg, ...) { }
#define ASSERT_IF_NOT_CRITICAL_SECTION(msg, ...) { }
#define ASSERT_IF_NOT_IRQ_REDIRECTED(handler_name, msg, ...) { }
#define ASSERT_IF_NOT_IRQn_REDIRECTED(N, msg, ...) { }
#endif


#define MILLI_TO_TICKS(uiMilliSeconds) (uiMilliSeconds * CHW_MCU::SysTicksPerSecond / 1000U)
#define SysTickToMs(x) (x)

#define MAXSYSTICKCALLBACKS 6


#define DECLARE_DIO(PORT,PIN) \
		DECLARE_DIN(PORT,PIN) \
		DECLARE_DOUT(PORT,PIN)


#define DECLARE_TIM(N) \
	class CHWTIM##N;

class CHW_MCU :
	public CHW_Clocks
{
public:
	static void Init();
	static void Reboot();

public: // Flash
	static bool ErasePage(unsigned uiPageNumber);
	static bool Flash(void* pTgt, const void* pValue, unsigned uiSize);
	static void FlashClearAllFlags();

public: // Signature
	typedef struct _uniqid_t{
		union { uint8_t b[12]; uint16_t w[6]; uint32_t u[3];};
		inline bool operator == (const struct _uniqid_t& r) const {for (unsigned n = 0; n < 3; ++n) {if (w[n] != r.w[n]) return 0;} return true;};
		template<class T> void digest(T& v) const {
			v = 0;
			for (unsigned n = 0; n < sizeof(b) / sizeof(T); ++n) {
				v = (v << 1) | (v >> (sizeof(T)*8 - 1)); /* ROL */
				v ^= *(const T*)&b[n * sizeof(T)];
		}};
	} uniqid_t;
#ifdef UID_ADDR
	static inline const uniqid_t& GetUniqueId() {return *((const uniqid_t*)((const void*)UID_ADDR));} /* The unique device identifier */
#endif
#ifdef FLASHSIZE_ADDR
	static inline const uint16_t& GetFlashsizeKb() {return (*(const uint16_t*)(const void*)FLASHSIZE_ADDR);} /* Flash memory size in Kbytes*/
#endif
#ifdef PACKAGEID_ADDR
	static inline const uint16_t& GetPackageId() {return (*(const uint16_t*)(const void*)PACKAGEID_ADDR);} /* Package type */
#endif

public: // Interrupts
	static void OnADC1();
	static void OnDMA2Stream0();
protected:
	static void Init_IRQs();
//	static void Init_RTC();
	static void Init_OneWire();

private:
//	static Callback<void (void)> m_cbTimer6WrapCallback;
//	static Callback<void (void)> m_cbTimer7WrapCallback;
	static unsigned           m_auiTimerPeriod[12];

//	static unsigned           m_uiTriacTimerPeriod;
//	static unsigned           m_uiTriacTimerPeriodMin;
//	static unsigned           m_uiTriacTimerPeriodMax;
//	static Callback<void (void)> m_cbTriacTimerWrapCallback;
};

#endif /* MCU_H_INCLUDED */
