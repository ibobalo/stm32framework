#include "stdafx.h"
#include "hw/hw.h"
#include "hw_clk.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"



/**************************************************************************************
 * ISR (interrupts handling - redirecting to CHW_MCU methods
 *************************************************************************************/
REDIRECT_ISR_STATIC(SysTick_Handler,     CHW_Clocks, OnSysTick);

//#define IDLE_DEBUG_INFO(...) DEBUG_INFO(__VA_ARGS__)
#define IDLE_DEBUG_INFO(...) {}

RCC_ClocksTypeDef       g_RCC_Clocks;

ISysTickNotifee* CHW_Clocks::m_apSysTickNotifees[MAXSYSTICKCALLBACKS];
volatile unsigned CHW_Clocks::m_uiSysTicksCount;
uint32_t CHW_Clocks::m_uiClkPerSysTick;
uint32_t CHW_Clocks::m_uiClkLast;
uint32_t CHW_Clocks::m_uiClkIdleSum;
uint32_t CHW_Clocks::m_uiClkIdleMin;
uint32_t CHW_Clocks::m_uiClkIdleMax;
uint32_t CHW_Clocks::m_uiClkUsedSum;
uint32_t CHW_Clocks::m_uiClkUsedMin;
uint32_t CHW_Clocks::m_uiClkUsedMax;
uint32_t CHW_Clocks::m_uiLoops;
bool CHW_Clocks::m_bHSEPresent;

void CHW_Clocks::Init()
{
	for (unsigned n = 0; n < ARRAY_SIZE(m_apSysTickNotifees); ++n) {
		m_apSysTickNotifees[n] = NULL;
	}
	m_uiSysTicksCount = 0;
	m_uiClkPerSysTick = 0;
	m_uiClkLast = 0;
	m_uiClkIdleSum = 0;
	m_uiClkIdleMin = 0xFFFFFFFF;
	m_uiClkIdleMax = 0;
	m_uiClkUsedSum = 0;
	m_uiClkUsedMin = 0xFFFFFFFF;
	m_uiClkUsedMax = 0;
	m_uiLoops = 0;
	m_bHSEPresent = false;

//	::SetSysClock(); //use default clocks, defined in system_stm32f4xx.h set from SystemInit()
	Init_Clocks(); //custom clocks
	SystemCoreClockUpdate(); // recalc SystemCoreClock (HCLK, AHB)
	RCC_GetClocksFreq(&g_RCC_Clocks);

	Init_SysTick();
	Init_IWDG();
#ifdef USE_WWDG
	Init_WWDG();
#endif // USE_WWDG

	DEBUG_INFO("SYSCLK MHz:", SystemCoreClock / 1000000);
	DEBUG_INFO("HCLK   MHz:", g_RCC_Clocks.SYSCLK_Frequency / 1000000);
#if defined(STM32F0)
	DEBUG_INFO("PCLK   MHz:", g_RCC_Clocks.PCLK_Frequency / 1000000);
#elif defined(STM32F4)
	DEBUG_INFO("PCLK1  MHz:", g_RCC_Clocks.PCLK1_Frequency / 1000000);
	DEBUG_INFO("PCLK2  MHz:", g_RCC_Clocks.PCLK2_Frequency / 1000000);
#endif

}

void CHW_Clocks::Init_Clocks()
{
#if defined(HSE_VALUE) && (HSE_VALUE > 0)
	/* default config done in SetSysClock() */
	// Init_ClocksHSE();
	m_bHSEPresent = (RCC_WaitForHSEStartUp() == SUCCESS);
#else
	m_bHSEPresent = false;
#endif

	if (!m_bHSEPresent) {
		/* HSI is not implemented in SetSysClock() for STM32F0 */
		IF_STM32F0( Init_ClocksHSI() );
	}
}

void CHW_Clocks::Init_ClocksBase()
{
	RCC_HSICmd(ENABLE);
	while (!RCC_GetFlagStatus(RCC_FLAG_HSIRDY)) {} // wait HSI starts

	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI); // internal
	RCC_HSEConfig(RCC_HSE_OFF);

	RCC_PLLCmd(DISABLE);
	while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY)) {} // wait PLL fully stopped
}
void CHW_Clocks::Init_ClocksHSI()
{
	// clock by internal RC 8MHz [HSI_VALUE]
	Init_ClocksBase();

#if defined(STM32F0)
	/* Enable Prefetch Buffer and set Flash Latency */
	FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;

	uint32_t uiPLLSource = RCC_PLLSource_HSI_Div2; // HSI
	uint32_t uiPLLM = 48000000 / (HSI_VALUE / 2); // 12 for HSI 8MHz/2 mean 48 MHz
	RCC_PLLConfig(
			uiPLLSource,
			(uiPLLM - 2)<<18                       // 8 MHz * 6 =   48 MHz PLLCLK
	);
	RCC_PLLCmd(ENABLE);
	while (!RCC_GetFlagStatus(RCC_FLAG_PLLRDY)) {} // wait PLL starts and ready
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK); // SYSCLK     = PLLCLK       =              48 MHz  -- CPU System clock   [~0.021 us]
	RCC_HCLKConfig(RCC_SYSCLK_Div1);           // HCLK,AHB   = SYSCLK       = 48MHz / 1  = 48 MHz  -- AHB clock          [~0.021 us]
	RCC_PCLKConfig(RCC_HCLK_Div8);             // PCLK,APB   = HCLK/8       = 48MHz / 8  = 6  MHz  -- Lo spd APB clock   [~0.167 us]
	while (RCC_GetSYSCLKSource() != 0x08) {}; /* Wait till PLL is used as system clock source */
#endif
}
#if defined(HSE_VALUE) && (HSE_VALUE > 0)
void CHW_Clocks::Init_ClocksHSE()
{
	Init_ClocksBase();

	// clock by external XTAL 8MHz [HSE_VALUE]
	RCC_HSEConfig(RCC_HSE_ON);
	m_bHSEPresent = (RCC_WaitForHSEStartUp() == SUCCESS);
	if (!m_bHSEPresent) {
		RCC_HSEConfig(RCC_HSE_OFF);
		return;
	}
#if defined(STM32F0)
	uint32_t uiPLLSource = RCC_PLLSource_PREDIV1; // HSE = 8 MHz external crystal freq
	uint32_t uiPLLM = 48000000 / HSE_VALUE; // 6 for HSE 8MHz mean 48 MHz
	RCC_PLLConfig(
			uiPLLSource,
			(uiPLLM - 2)<<18                       // 8 MHz * 6 =   48 MHz PLLCLK
	);
#elif defined(STM32F4)
	const uint32_t uiPLLSource = m_bHSEPresent?RCC_PLLSource_HSE:RCC_PLLSource_HSI; // HSE = 8 MHz external crystal freq
	const uint32_t uiPLLM = (m_bHSEPresent?HSE_VALUE:HSI_VALUE) / (2 *1000*1000); // 4 for HSE 8MHz,  8 MHz / 4   = 2 MHz - recommended VCO input freq
	const uint32_t uiPLLN = 192;                                                  // 2 MHz * 192 = 384 MHz PLLCLK
	const uint32_t uiPLLP = 4;                                                    // 384 MHz / 4 = 96 MHz clock for PLLCLK
	const uint32_t uiPLLQ = 8;                                                    // 384 MHz / 8 = 48 MHz clock for the USB (FIXED)
#  if defined(STM32F446)
	const uint32_t uiPLLR = 2;                                                    // I2S, SAI not used. 2 is default value
	RCC_PLLConfig(uiPLLSource, uiPLLM, uiPLLN, uiPLLP, uiPLLQ, uiPLLR);
#  else
	RCC_PLLConfig(uiPLLSource, uiPLLM, uiPLLN, uiPLLP, uiPLLQ);
#  endif // STM32F446
#else
#error unsupportred MPU
#endif
	RCC_PLLCmd(ENABLE);
	while (!RCC_GetFlagStatus(RCC_FLAG_PLLRDY)) {} // wait PLL starts and ready
//	while (RCC->CR & (1<<25)) { /* wait PLLRDY flag */ }
	do {
#if defined(STM32F0)
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK); // SYSCLK     = PLLCLK       =              48 MHz  -- CPU System clock   [~0.021 us]
		RCC_HCLKConfig(RCC_SYSCLK_Div1);           // HCLK,AHB   = SYSCLK       = 48MHz / 1  = 48 MHz  -- AHB clock          [~0.021 us]
		RCC_PCLKConfig(RCC_HCLK_Div8);             // PCLK,APB   = HCLK/8       = 48MHz / 8  = 6  MHz  -- Lo spd APB clock   [~0.167 us]
#elif defined(STM32F4)
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK); // SYSCLK     = PLLCLK       =              96 MHz  -- CPU System clock   [~0.010 us]
		RCC_HCLKConfig(RCC_SYSCLK_Div1);           // HCLK,AHB   = SYSCLK/1     = 96MHz / 1  = 96 MHz  -- AHB clock          [~0.010 us]
		RCC_PCLK1Config(RCC_HCLK_Div16);           // PCLK1,APB1 = HCLK/16      = 96MHz / 16 = 6  MHz  -- Lo spd APB clock   [~0.167 us]
		RCC_PCLK2Config(RCC_HCLK_Div2);            // PCLK2,APB2 = HCLK/2       = 96MHz / 2  = 48 MHz  -- Hi spd APB clock   [~0.021 us]
#else
#error unsupportred MPU
#endif
	} while (RCC_GetSYSCLKSource() != 0x08);
}
#endif // HSE_VALUE
void CHW_Clocks::Init_SysTick()
{
	// System timer. callback to    SysTick_Handler   -->   this->OnSystemTimeTick()
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	m_uiClkPerSysTick = GetAHBFrequency()/SysTicksPerSecond;
	SysTick_Config(m_uiClkPerSysTick);
    NVIC_SetPriority(SysTick_IRQn, 14);
    NVIC_SystemLPConfig(NVIC_LP_SEVONPEND, ENABLE); // for Idle() processing
}

/////////////////////////////////////////////////////////////////////////////////
//
//    System timer
//
/////////////////////////////////////////////////////////////////////////////////
void CHW_Clocks::RegisterSysTickCallback(ISysTickNotifee* pNotifee)
{
	unsigned n = 0;
	for (;  m_apSysTickNotifees[n]; ++n) { }
	ASSERTE(n < ARRAY_SIZE(m_apSysTickNotifees) - 1 && "Increase MAXSYSTICKCALLBACKS"); /* reserve last NULL */
	m_apSysTickNotifees[n] = pNotifee;
	INDIRECT_CALL(pNotifee->OnSysTick());
}
void CHW_Clocks::UnregisterSysTickCallback(ISysTickNotifee* pNotifee)
{
	for (unsigned n = 0;  n < ARRAY_SIZE(m_apSysTickNotifees); ++n) {
		if (m_apSysTickNotifees[n] == pNotifee) {
			m_apSysTickNotifees[n] = NULL;
		}
		if (m_apSysTickNotifees[n] == NULL && n + 1 < ARRAY_SIZE(m_apSysTickNotifees)) {
			m_apSysTickNotifees[n] = m_apSysTickNotifees[n + 1];
		}
	}
}

unsigned CHW_Clocks::GetSysTick()
{
	return m_uiSysTicksCount;
}
uint32_t CHW_Clocks::GetCLKCounter24()
{
	uint32_t uiValue = SysTick->VAL;
	return m_uiClkPerSysTick - ((uiValue & SysTick_VAL_CURRENT_Msk) >> SysTick_VAL_CURRENT_Pos);
}

uint32_t CHW_Clocks::GetCLKCounter32()
{
	uint32_t t;
	uint32_t uiCount;
	uint32_t uiValue;
	bool     bPending;
	do {
		uiCount = m_uiSysTicksCount;
		uiValue = SysTick->VAL;
		bPending = (SCB->ICSR & SCB_ICSR_PENDSTSET_Msk);
	} while (uiCount != m_uiSysTicksCount); // to be sure interrupt was not processed in while

	t = (uint32_t)(uiCount + 1) * m_uiClkPerSysTick - ((uiValue & SysTick_VAL_CURRENT_Msk) >> SysTick_VAL_CURRENT_Pos);
	if (bPending && uiValue > m_uiClkPerSysTick / 2) {
		t += m_uiClkPerSysTick;
	}

	return t;
}
uint64_t CHW_Clocks::GetCLKCounter64()
{
	uint64_t t;
	uint32_t uiCount;
	uint32_t uiValue;
	bool     bPending;
	do {
		uiCount = m_uiSysTicksCount;
		uiValue = SysTick->VAL;
		bPending = (SCB->ICSR & SCB_ICSR_PENDSTSET_Msk);
	} while (uiCount != m_uiSysTicksCount); // to be sure interrupt was not processed in while

	t = (uint64_t)(uiCount + 1) * m_uiClkPerSysTick - ((uiValue & SysTick_VAL_CURRENT_Msk) >> SysTick_VAL_CURRENT_Pos);
	if (bPending && uiValue > m_uiClkPerSysTick / 2) {
		t += m_uiClkPerSysTick;
	}

	return t;
}

uint32_t CHW_Clocks::GetCLKFrequency()
{
	return SystemCoreClock;
}

uint32_t CHW_Clocks::GetHCLKFrequency()
{
	return g_RCC_Clocks.HCLK_Frequency;
}
uint32_t CHW_Clocks::GetPCLK1Frequency()
{
	IF_STM32F0( return g_RCC_Clocks.PCLK_Frequency );
	IF_STM32F4( return g_RCC_Clocks.PCLK1_Frequency );
}
uint32_t CHW_Clocks::GetPCLK2Frequency()
{
	IF_STM32F0( return g_RCC_Clocks.PCLK_Frequency );
	IF_STM32F4( return g_RCC_Clocks.PCLK2_Frequency );
}
uint32_t CHW_Clocks::GetPCLK1TimerFrequency()
{
	if (GetPCLK1Frequency() == GetHCLKFrequency())
		return GetPCLK1Frequency();
	return GetPCLK1Frequency() * 2;
}
uint32_t CHW_Clocks::GetPCLK2TimerFrequency()
{
	if (GetPCLK2Frequency() == GetHCLKFrequency())
		return GetPCLK2Frequency();
	return GetPCLK2Frequency() * 2;
}

uint32_t CHW_Clocks::Clk24Diff(uint32_t uiClk24_Start, uint32_t uiClk24_End)
{
	return (uiClk24_End <= uiClk24_Start) ? (uiClk24_Start - uiClk24_End) : (m_uiClkPerSysTick + uiClk24_Start - uiClk24_End);
}

void CHW_Clocks::Idle()
{
	uint32_t uiClk24_1;
	uint32_t uiClk24_2;

	IDLE_DEBUG_INFO(">>IDLE");

	ENTER_CRITICAL_SECTION;
	{
		uiClk24_1 = GetCLKCounter24();
		__WFE();
		uiClk24_2 = GetCLKCounter24();
	}
	LEAVE_CRITICAL_SECTION;

	IDLE_DEBUG_INFO("<<WAKE");

	if (m_uiLoops) {
		uint32_t uiClkUsed = Clk24Diff(m_uiClkLast, uiClk24_1);
		m_uiClkUsedSum += uiClkUsed;
		if (uiClkUsed < m_uiClkUsedMin) { m_uiClkUsedMin = uiClkUsed;}
		if (uiClkUsed > m_uiClkUsedMax) { m_uiClkUsedMax = uiClkUsed;}
		m_uiClkLast = uiClk24_2;
	}
	m_uiLoops++;

	uint32_t uiClkIdle = Clk24Diff(uiClk24_1, uiClk24_2);
	m_uiClkIdleSum += uiClkIdle;
	if (uiClkIdle < m_uiClkIdleMin) { m_uiClkIdleMin = uiClkIdle;}
	if (uiClkIdle > m_uiClkIdleMax) { m_uiClkIdleMax = uiClkIdle;}



}
void CHW_Clocks::Sleep(const uint32_t uiMilliSeconds)
{
	int iTicksDelay = MILLI_TO_TICKS(uiMilliSeconds);
	unsigned uiStartTick = GetSysTick();
	int iTicksSpent;

	do {
		KickIWDG();
		__WFI();
		iTicksSpent = GetSysTick() - uiStartTick;
	} while (iTicksSpent < iTicksDelay);
}
void CHW_Clocks::DelayMicro(const uint32_t uiMicroSeconds)
{
	uint32_t uiClkStart = GetCLKCounter32();
	uint32_t uiClkEnd = uiClkStart + uiMicroSeconds * 1000000ULL / SystemCoreClock;
	do {
		if ((int32_t)(GetCLKCounter32() - uiClkEnd) > 0) {
			break;
		}
	} while (true);
}

/////////////////////////////////////////////////////////////////////////////////
//
//     IWDG
//
/////////////////////////////////////////////////////////////////////////////////
void CHW_Clocks::Init_IWDG()
{
#ifdef DEBUG
	DBGMCU_APB1PeriphConfig(DBGMCU_IWDG_STOP, ENABLE);
#endif // DEBUG

	RCC_LSICmd(ENABLE);
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET) {} // Wait till LSI is ready
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	DEBUG_INFO("IWDG: Init done");
}
void CHW_Clocks::EnableIWDG(unsigned uiMs/*=100*/)
{
	DEBUG_INFO("IWDG: Enable:", uiMs);
	// IWDG CLK = LSI/32 = 40kHz/32 ~ 1 kHz  -- IWDG clock counter [~1 ms]
	IWDG_SetPrescaler(IWDG_Prescaler_32);
	static const unsigned uiIWDGFrequency = LSI_VALUE / 32; // ~1000Hz
	unsigned uiCounter = uiMs * uiIWDGFrequency / 1000;
	IWDG_SetReload(uiCounter);
	IWDG_ReloadCounter();
	IWDG_Enable();
	DEBUG_INFO("IWDG Enabled:", uiCounter);
}
void CHW_Clocks::KickIWDG()
{
	IWDG_ReloadCounter();
//	DEBUG_INFO("IWDG: Kick");
}

#ifdef USE_WWDG
/////////////////////////////////////////////////////////////////////////////////
//
//     WWDG
//
/////////////////////////////////////////////////////////////////////////////////
unsigned CHW_Clocks::m_uiWWDGFrequency;
void CHW_Clocks::Init_WWDG()
{
#ifdef DEBUG
	DBGMCU_APB1PeriphConfig(DBGMCU_WWDG_STOP, ENABLE);
#endif // DEBUG

	// WWDGCLK    = PCLK1/4096/8 = 6MHz/4096/4~ 366 Hz  -- WWDG clock counter [~2.7 ms]
	// see Init_Clocks() for PCLK1;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
	WWDG_DeInit();
	WWDG_SetPrescaler(WWDG_Prescaler_8);
	m_uiWWDGFrequency = GetAPB1Frequency() / 4096 / 8;
	WWDG_SetWindowValue(0x7F /*127*/);
	WWDG_ClearFlag();
	KickWWDG();
	NVIC_INIT(WWDG_IRQn, 0, 0); // watchdog timer. callback to    WWDG_IRQHandler   -->   this->OnWWDG()
	WWDG_EnableIT();
	DEBUG_INFO("WWDG: Init done");
}
void CHW_Clocks::EnableWWDG()
{
	DEBUG_INFO("WWDG: Enable");
	KickWWDG();
	WWDG_ClearFlag();
	WWDG_Enable(127);
}
void CHW_Clocks::KickWWDG(unsigned uiMs)
{
	unsigned uiCounter = m_uiWWDGFrequency * uiMs / 1000 + 1;
	if (uiCounter > 0x3F) uiCounter = 0x3F;
	WWDG_SetCounter(0x40 + uiCounter);
//	DEBUG_INFO("WWDG: Kick:", uiCounter);
}
#endif // USE_WWDG

/////////////////////////////////////////////////////////////////////////////////
//
//     IRQ
//
/////////////////////////////////////////////////////////////////////////////////
void CHW_Clocks::OnSysTick()
{
	++m_uiSysTicksCount;
	__DSB(); // Data Synchronization Barrier. Ensure m_uiSysTicksCount updated
	for (unsigned n = 0; m_apSysTickNotifees[n]; ++n) {
		ASSERTE(n < ARRAY_SIZE(m_apSysTickNotifees));
		ISysTickNotifee* cb = m_apSysTickNotifees[n];
		cb->OnSysTick();
	}
}
