// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define CANx_N 1
// #define CANx_PORT_TX D,0,AF_4
// #define CANx_PORT_RX D,1,AF_4
// #define CANx_IT_GROUP_PRIO 0
// #define CANx_IT_SUBPRIO 0
// #include "hw_can.cpp"
// IHW_CAN* getCANInterface1() {return &g_HW_CAN1;};

#if !defined(CANx_N) || \
	!defined(CANx_PORT_TX) || CHECK_EMPTY(SECOND(CANx_PORT_TX)) || \
	!defined(CANx_PORT_RX) || CHECK_EMPTY(SECOND(CANx_PORT_RX)) || \
	!defined(CANx_IT_GROUP_PRIO) || !defined(CANx_IT_SUBPRIO)
#error params must be defined : CANx_N, CANx_PORT_TX, CANx_PORT_RX, CANx_IT_GROUP_PRIO, CANx_IT_SUBPRIO
// next defines for editor hints
#define CANx_N 1
#define CANx_PORT_TX D,0
#define CANx_PORT_RX D,1
#define CANx_IT_GROUP_PRIO 0
#define CANx_IT_SUBPRIO 0
#endif

#include "hw_can.h"
#include "mcu.h"
#include "util/macros.h"
#include "util/core.h"
#include "util/atomic.h"
#include "util/utils.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include <stdint.h>
#include <stddef.h>

#include "stm32fxxxx.h"

#define CANx                    IF_STM32F4(CC(CAN,CANx_N)) IF_STM32F0(CAN)
#define CHW_CANx                CC(CHW_CAN,CANx_N)

//#define CAN_DEBUG_INFO(...) DEBUG_INFO("CAN" STR(CANx_N) ": " __VA_ARGS__)
#define CAN_DEBUG_INFO(...) {}

class CHW_CANx:
		public IHW_CAN
{
public:
	virtual bool Init(unsigned uiBaudrate, bool bTxAutoRetry = true, unsigned uiSamplePercent = 70);
public:
	virtual bool QueueMessage(CanTxMsg* pTxMessage, ICANTxDoneNotifee* pDoneNotifee, CTxMsgHandle* pRetHandle = NULL);
	virtual void Abort(CTxMsgHandle msgHandle);
	virtual bool SetListener(ICANListener* pListener,
			unsigned uiId, unsigned uiMask = 0x7FF,
			EFrameTypesFiltering eFrameTypes = FTF_BOTH_DATA_AND_REMOTE);
	virtual bool SetListenerEx(ICANListener* pListener,
			unsigned uiExtId, unsigned uiExtMask = 0x1FFFFFFF,
			EFrameTypesFiltering eFrameTypes = FTF_BOTH_DATA_AND_REMOTE);
	virtual void ClearListeners(ICANListener* pListener);
public: // Interrupts
	void OnIrq();
	void OnTX();
	void OnRX0();
	void OnRX1();
//	void OnSCE();
private:
	void BroadcastAllListeners(const CanRxMsg* RxMessage) const;

	ICANTxDoneNotifee* m_apDoneNotifee[3]; // for each of 3 tx mailboxes

	ICANListener* m_apListeners[MAX_CAN_FILTERS];
	unsigned m_nRxMessagesQueueSize;

};


bool CHW_CANx::Init(unsigned uiBaudrate, bool bTxAutoRetry/* = true*/, unsigned uiSamplePercent/* = 70*/)
{
	for (unsigned n = 0; n < ARRAY_SIZE(m_apListeners); ++n) {
		m_apListeners[n] = NULL;
	}

	CAN_InitTypeDef		    CAN_InitStructure;
	GPIO_InitTypeDef  	    GPIO_InitStructure;
	NVIC_InitTypeDef	    NVIC_InitStructure;
#ifdef STM32F0
	NVIC_InitStructure.NVIC_IRQChannel = CEC_CAN_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = CANx_IT_SUBPRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
#ifdef STM32F4
	NVIC_InitStructure.NVIC_IRQChannel = IF_STM32F0(CEC_CAN_IRQn) IF_STM32F4(CCC(CAN,CANx_N,_TX_IRQn));
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = CANx_IT_GROUP_PRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = CANx_IT_SUBPRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = CCC(CAN,CANx_N,_RX0_IRQn);
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = CANx_IT_GROUP_PRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = CANx_IT_SUBPRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = CCC(CAN,CANx_N,_RX1_IRQn);
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = CANx_IT_GROUP_PRIO;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = CANx_IT_SUBPRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

//	NVIC_InitStructure.NVIC_IRQChannel = CCC(CAN,CANx_N,_SCE_IRQn);
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = pCfg->IRQChannelPriority;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = pCfg->IRQChannelSubPriority;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);

	/* CAN GPIOs configuration **************************************************/

	/* Enable GPIO clock */
	IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(CANx_PORT_TX)), ENABLE));
	IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,FIRST(CANx_PORT_TX)), ENABLE));

	/* PA11 and PA12 remap on QFN28 and TSSOP20 packages */
	IF_STM32F042(RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE));
	IF_STM32F042(SYSCFG->CFGR1 |= SYSCFG_CFGR1_PA11_PA12_RMP);

	/* Configure CAN RX and TX pins */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(CANx_PORT_RX));
	GPIO_Init(CC(GPIO,FIRST(CANx_PORT_RX)), &GPIO_InitStructure);
	GPIO_PinAFConfig(CC(GPIO,FIRST(CANx_PORT_RX)), CC(GPIO_PinSource,SECOND(CANx_PORT_RX)),
			IF_STM32F4(CC(GPIO_AF_CAN,CANx_N)) IF_STM32F0(CC(GPIO_,THIRD(CANx_PORT_RX,AF_4))));
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(CANx_PORT_TX));
	GPIO_Init(CC(GPIO,FIRST(CANx_PORT_TX)), &GPIO_InitStructure);
	GPIO_PinAFConfig(CC(GPIO,FIRST(CANx_PORT_TX)), CC(GPIO_PinSource,SECOND(CANx_PORT_TX)),
			IF_STM32F4(CC(GPIO_AF_CAN,CANx_N)) IF_STM32F0(CC(GPIO_,THIRD(CANx_PORT_TX,AF_4))));

	/* CAN configuration ********************************************************/
	/* Enable CAN clock */
	IF_STM32F0(RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN, ENABLE));
	IF_STM32F4(RCC_APB1PeriphClockCmd(CC(RCC_APB1Periph_CAN,CANx_N), ENABLE));
#if (CANx_N != 1)
	// CAN1 is master controller and is required by other CANx to work
	IF_STM32F4(RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE));
#endif

	/* CAN register init */
	CAN_DeInit(CANx);

	/* CAN cell init */
	CAN_StructInit(&CAN_InitStructure);
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = ENABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = bTxAutoRetry ? DISABLE : ENABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
	CAN_DEBUG_INFO("baudrate requested :", uiBaudrate);
	/*
	 *  Baudrate = (PCLK1,APB1_FREQ) / (CAN_Prescaler * (1 + BS1 + BS2))
	 *     * invertor old
	 *       24 MHz / (12 * (1 + 3 + 3)) ~ 285 Kb/s
	 *     * invertor new
	 *       24 MHz / (72  * (1 + 16 + 7)) ~ 13.9 Kb/s
	 *  Prescaler * (1 + BS1 + BS2) = (PCLK1,APB1_FREQ) / Baudrate
	 *     * invertor, baudate 250K
	 *       nTicks = 24000000 / 250000 = 96
	 *       minPrescaler = 96 / (1 + 16 + 8) = 96/25 = 3.84
	 *       maxPrescaler = 96 / (1 + 1 + 1) = 96/3 = 32
	 *       Prescaler = roundup(3.84) = 4
	 *       BS1 + BS2 = 96 / 4 - 1 = 23
	 *       BS1 = 23 * 0.7 = 16
	 *	     BS2 = 23 - 16 = 7
	 *     * controller, baudate 250K
	 *       6000000 / 250000 = 24
	 *       minPrescaler = 24 / (1 + 16 + 8) = 96/25 = 0.96
	 *       Prescaler = roundup(0.96) = 1
	 *       BS1 + BS2 = 24 / 1 - 1 = 23
	 *       BS1 = 23 * 0.7 = 16
	 *	     BS2 = 23 - 16 = 7
	 *
	 */
	unsigned uiFreq = CHW_MCU::GetAPB1Frequency();
	unsigned nTicks = uiFreq / uiBaudrate;
	ASSERTE(nTicks >= 0);
#define U2Q(u) ((u) * 1024U)
#define Q2U(q) ((q) / 1024U)
	unsigned qMinPrescaler = U2Q(nTicks) / 25U;
	unsigned qMaxPrescaler = U2Q(nTicks) / 3U;
	unsigned uiMinPrescaler = Q2U(qMinPrescaler + (U2Q(1U) - 1U));
	unsigned uiMaxPrescaler = Q2U(qMaxPrescaler);
	if (uiMaxPrescaler < 1) {
		CAN_InitStructure.CAN_Prescaler = 1;
		CAN_InitStructure.CAN_BS1 = CAN_BS1_1tq;
		CAN_InitStructure.CAN_BS2 = CAN_BS2_1tq;
	} else if (uiMinPrescaler >= 1024) {
		CAN_InitStructure.CAN_Prescaler = 1024;
		CAN_InitStructure.CAN_BS1 = CAN_BS1_16tq;
		CAN_InitStructure.CAN_BS2 = CAN_BS2_8tq;
	} else {
		unsigned uiPrescaler = uiMinPrescaler;
		if (uiPrescaler < 1) uiPrescaler = 1;
		unsigned nBS = nTicks / uiPrescaler - 1;
		ASSERTE(nBS >= 3);
		ASSERTE(nBS <= 25);
		unsigned nBS1 = (nBS + 1) * uiSamplePercent / 100;
		if (nBS1 > nBS - 1) nBS1 = nBS - 1;
		if (nBS1 > 16) nBS1 = 16;
		if (nBS1 < 1) nBS1 = 1;
		unsigned nBS2 = nBS - nBS1;
		if (nBS2 > 8) {nBS2 = 8; nBS1 = nBS - nBS1;}
		ASSERTE(nBS2 >= 1);
		CAN_InitStructure.CAN_Prescaler = uiPrescaler;
		CAN_InitStructure.CAN_BS1 = nBS1 - 1;
		CAN_InitStructure.CAN_BS2 = nBS2 - 1;
		CAN_DEBUG_INFO("baudrate actual :", uiFreq / (uiPrescaler * (1 + nBS1 + nBS2)));
	}

	uint8_t res = CAN_Init(CANx, &CAN_InitStructure);
	return (res == CAN_InitStatus_Success);
}


void CHW_CANx::OnIrq()
{
	OnTX();
	OnRX0();
	OnRX1();
//	OnSCE();
}
void CHW_CANx::OnTX()
{
	CAN_DEBUG_INFO("OnTx TSR:", CANx->TSR);
	if (CAN_GetITStatus(CANx, CAN_IT_TME)) {
//		CAN_ClearITPendingBit(CANx, CAN_IT_TME); clears all boxes but we need it separate
		unsigned nPending = 0;
		for (unsigned nTxMailbox = 0; nTxMailbox < ARRAY_SIZE(m_apDoneNotifee); ++nTxMailbox) {
			uint8_t status = CAN_TransmitStatus(CANx, nTxMailbox);
			if (status != CAN_TxStatus_Pending) {
				CAN_DEBUG_INFO("Tx done from irq, mbox:", nTxMailbox);
				CAN_DEBUG_INFO("Tx done from irq, status:", status);
//				clear separate ITPendingBit CAN_IT_TMEx;
				CANx->TSR = CAN_TSR_RQCP0 << (nTxMailbox * 8);
				ICANTxDoneNotifee* pDoneNotifee;
				AtomicExchange(&m_apDoneNotifee[nTxMailbox], (ICANTxDoneNotifee*)NULL, &pDoneNotifee);
				if (pDoneNotifee) {
					bool bSucceed = (status == CAN_TxStatus_Ok);
					CTxMsgHandle hMsgHandle = (nTxMailbox + 1);
					pDoneNotifee->OnCANMessageTxDone(bSucceed, hMsgHandle);
				} else {
					CAN_DEBUG_INFO("Tx done without notifee");
				}
			} else {
				++nPending;
			}
		}
		if (!nPending) {
			CAN_ITConfig(CANx, CAN_IT_TME, DISABLE);
		}
	}
	return;
}
void CHW_CANx::OnRX0()
{
	CanRxMsg RxMessage;

	while (CAN_GetITStatus(CANx, CAN_IT_FMP0)) {
		CAN_Receive(CANx, CAN_FIFO0, &RxMessage);

		unsigned n = RxMessage.FMI * 2;
		ICANListener* pListener = m_apListeners[n];
		if (!pListener || !pListener->ProcessRxedCANMessage(&RxMessage)) {
			BroadcastAllListeners(&RxMessage);
		}
	}
}
void CHW_CANx::OnRX1()
{
	CanRxMsg RxMessage;

	while (CAN_GetITStatus(CANx, CAN_IT_FMP1)) {
		CAN_Receive(CANx, CAN_FIFO1, &RxMessage);

		unsigned n = RxMessage.FMI * 2 + 1;
		ICANListener* pListener = m_apListeners[n];
		if (!pListener || !pListener->ProcessRxedCANMessage(&RxMessage)) {
			BroadcastAllListeners(&RxMessage);
		}
	}
}

//void CHW_CANx::OnSCE()
//{
//
//}

bool CHW_CANx::QueueMessage(CanTxMsg* pTxMessage, ICANTxDoneNotifee* pDoneNotifee, CTxMsgHandle* pRetHandle /*= NULL*/)
{
	IMPLEMENTS_INTERFACE_METHOD(IHW_CAN::QueueMessage(pTxMessage, pDoneNotifee, pRetHandle));
	bool bResult;

	if (pDoneNotifee) CAN_ITConfig(CANx, CAN_IT_TME, DISABLE); // prevent OnTx IRQ before m_apDoneNotifee assigned
	do {
		uint8_t nTxMailbox = CAN_Transmit(CANx, pTxMessage);
		CAN_DEBUG_INFO("Tx mbox:", nTxMailbox);
		if (nTxMailbox != CAN_TxStatus_NoMailBox) {
			ASSERTE(nTxMailbox < ARRAY_SIZE(m_apDoneNotifee));
			CTxMsgHandle hMsgHandle = (nTxMailbox + 1);
			if (pRetHandle) *pRetHandle = hMsgHandle;
			if (pDoneNotifee) {
				ICANTxDoneNotifee* pPrevNotifee;
				AtomicExchange(&m_apDoneNotifee[nTxMailbox], pDoneNotifee, &pPrevNotifee);
				ASSERTE(pPrevNotifee == NULL);
			}
			bResult = true;
		} else {
			CAN_DEBUG_INFO("Tx overflow");
			bResult = false;
		}
	} while (false);
	if (pDoneNotifee) CAN_ITConfig(CANx, CAN_IT_TME, ENABLE);

	return bResult;
}

/*virtual*/
void CHW_CANx::Abort(CTxMsgHandle msgHandle)
{
	ASSERTE(msgHandle);
	uint8_t nTxMailbox = (uint8_t)msgHandle - 1;
	ASSERTE(nTxMailbox < ARRAY_SIZE(m_apDoneNotifee));
	CAN_CancelTransmit(CANx, nTxMailbox);
}

/*virtual*/
bool CHW_CANx::SetListener(ICANListener* pListener,
		unsigned uiId, unsigned uiMask /*= 0x7FF*/,
		EFrameTypesFiltering eFrameTypes /*= FTF_BOTH_DATA_AND_REMOTE*/
) {
	IMPLEMENTS_INTERFACE_METHOD(IHW_CAN::SetListener(pListener, uiId, uiMask, eFrameTypes));
	CAN_FilterInitTypeDef	CAN_FilterInitStructure;
	bool bResult = false;
	for (unsigned n = 0; n < ARRAY_SIZE(m_apListeners); ++n) {
		if (m_apListeners[n] == NULL) {
			m_apListeners[n] = pListener;

			/* CAN filter init */
			CAN_FilterInitStructure.CAN_FilterNumber = (((CANx_N) - 1) * 14) + n;
			CAN_FilterInitStructure.CAN_FilterIdHigh = (uiId << 5);
			CAN_FilterInitStructure.CAN_FilterIdLow = CAN_Id_Standard
					| ( (eFrameTypes == FTF_ONLY_REMOTE) ? CAN_RTR_Remote : CAN_RTR_Data );
			CAN_FilterInitStructure.CAN_FilterMaskIdHigh = (uiMask << 5);
			CAN_FilterInitStructure.CAN_FilterMaskIdLow = CAN_Id_Standard
					| ( (eFrameTypes != FTF_BOTH_DATA_AND_REMOTE) ? CAN_RTR_Remote : CAN_RTR_Data );
			CAN_FilterInitStructure.CAN_FilterFIFOAssignment = (n&1) ? CAN_FIFO1 : CAN_FIFO0;
			CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
			CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
			CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
			CAN_FilterInit(&CAN_FilterInitStructure);

			CAN_DEBUG_INFO("Std filter registered,   Id:", uiId);
			CAN_DEBUG_INFO("Std filter registered, Mask:", uiMask);

			bResult = true;
			break;
		}
	}
	if (bResult) {
		/* Enable FIFO message pending Interrupt */
		CAN_ITConfig(CANx, CAN_IT_FMP0, ENABLE);
		CAN_ITConfig(CANx, CAN_IT_FMP1, ENABLE);
	}
	return bResult;
}

/*virtual*/
bool CHW_CANx::SetListenerEx(ICANListener* pListener,
		unsigned uiId, unsigned uiMask /*= 0x1FFFFFFF*/,
		EFrameTypesFiltering eFrameTypes /*= FTF_BOTH_DATA_AND_REMOTE*/
) {
	IMPLEMENTS_INTERFACE_METHOD(IHW_CAN::SetListenerEx(pListener, uiId, uiMask, eFrameTypes));
	CAN_FilterInitTypeDef	CAN_FilterInitStructure;
	bool bResult = false;
	for (unsigned n = 0; n < ARRAY_SIZE(m_apListeners); ++n) {
		if (m_apListeners[n] == NULL) {
			m_apListeners[n] = pListener;

//			uint32_t uiStdId = (uiId >> 18) & 0x7FF;
//			uint32_t uiExtId = (uiId >> 0) & 0x3FFFF;
//			uint32_t uiStdMask = (uiMask >> 18) & 0x7FF;
//			uint32_t uiExtMask = (uiMask >> 0) & 0x3FFFF;

			/* CAN filter init */
			CAN_FilterInitStructure.CAN_FilterNumber = (((CANx_N) - 1) * 14) + n;
			CAN_FilterInitStructure.CAN_FilterIdHigh = (uiId >> 13);
			CAN_FilterInitStructure.CAN_FilterIdLow = (uiId << 3)
					| CAN_Id_Extended
					| ((eFrameTypes == FTF_ONLY_REMOTE) ? CAN_RTR_Remote : CAN_RTR_Data );
			CAN_FilterInitStructure.CAN_FilterMaskIdHigh = (uiMask >> 13);
			CAN_FilterInitStructure.CAN_FilterMaskIdLow = (uiMask << 3)
					| CAN_Id_Extended
					| ( (eFrameTypes != FTF_BOTH_DATA_AND_REMOTE) ? CAN_RTR_Remote : CAN_RTR_Data );
			CAN_FilterInitStructure.CAN_FilterFIFOAssignment = (n&1) ? CAN_FIFO1 : CAN_FIFO0;
			CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
			CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
			CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
			CAN_FilterInit(&CAN_FilterInitStructure);

			CAN_DEBUG_INFO("Ext filter registered,   Id:", uiId);
			CAN_DEBUG_INFO("Ext filter registered, Mask:", uiMask);

			bResult = true;
			break;
		}
	}
	if (bResult) {
		/* Enable FIFO message pending Interrupt */
		CAN_ITConfig(CANx, CAN_IT_FMP0, ENABLE);
		CAN_ITConfig(CANx, CAN_IT_FMP1, ENABLE);
	}
	return bResult;
}

/*virtual*/
void CHW_CANx::ClearListeners(ICANListener* pListener)
{
	unsigned nAssignedToFIFO0 = 0;
	unsigned nAssignedToFIFO1 = 0;

	/* Disable FIFO message pending Interrupt */
	CAN_ITConfig(CANx, CAN_IT_FMP0, DISABLE);
	CAN_ITConfig(CANx, CAN_IT_FMP1, DISABLE);

	/* Deactivate FIFO filters */
	CAN_FilterInitTypeDef	CAN_FilterInitStructure;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = DISABLE;
	for (unsigned n = 0; n < ARRAY_SIZE(m_apListeners); ++n) {
		if (m_apListeners[n] == pListener) {
			m_apListeners[n] = NULL;
			CAN_FilterInitStructure.CAN_FilterNumber = (((CANx_N) - 1) * 14) + n;
			CAN_FilterInit(&CAN_FilterInitStructure);
		} else if (m_apListeners[n]) {
			if (n&1) ++nAssignedToFIFO1;
			else ++nAssignedToFIFO0;
		}
	}

	/* Enable FIFO message pending Interrupt */
	if (nAssignedToFIFO0) {
		CAN_ITConfig(CANx, CAN_IT_FMP0, ENABLE);
	}
	if (nAssignedToFIFO1) {
		CAN_ITConfig(CANx, CAN_IT_FMP1, ENABLE);
	}
}

void CHW_CANx::BroadcastAllListeners(const CanRxMsg* pRxMessage) const
{
	for (unsigned n = 0; n < ARRAY_SIZE(m_apListeners); ++n) {
		ICANListener* pListener = m_apListeners[n];
		if (pListener && pListener->ProcessRxedCANMessage(pRxMessage)) {
			break;
		}
	}
}

// GLOBAL INSTANCE (for irq redirecting
CHW_CANx CC(g_HW_CAN,CANx_N);

#ifdef STM32F0
	REDIRECT_ISR_OBJ(CEC_CAN_IRQHandler, &CC(g_HW_CAN,CANx_N), OnIrq);
#endif
#ifdef STM32F4
REDIRECT_ISR_OBJ(CCCC(CA,N,CANx_N,_TX_IRQHandler),   &CC(g_HW_CAN,CANx_N), OnTX);
REDIRECT_ISR_OBJ(CCCC(CA,N,CANx_N,_RX0_IRQHandler),  &CC(g_HW_CAN,CANx_N), OnRX0);
REDIRECT_ISR_OBJ(CCCC(CA,N,CANx_N,_RX1_IRQHandler),  &CC(g_HW_CAN,CANx_N), OnRX1);
//REDIRECT_ISR_OBJ(CCC(CAN,CANx_N,_SCE_IRQHandler),  &CC(g_HW_CAN,CANx_N), OnSCE);
#endif


#undef	CANx_N
#undef	CANx_PORT_TX
#undef	CANx_PORT_RX
#undef	CANx_IT_GROUP_PRIO
#undef	CANx_IT_SUBPRIO
#undef  CANx
#undef  CHW_CANx
#undef  GPIO_AF_CANx

