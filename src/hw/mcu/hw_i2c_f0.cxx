// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define I2Cx_N                     1
// #define I2Cx_SCL_GPIO_PORT         B,6,AF_1
// #define I2Cx_SDA_GPIO_PORT         B,7,AF_1
// #define I2Cx_IT_GROUP_PRIO         3
// //#define I2Cx_SLAVE               1
// //next defines is optional for DMA access
// #define I2Cx_DMA_TX                1,6,1
// #define I2Cx_DMA_RX                1,0,1
// #include "hw_i2c.cxx"
// II2C* getI2CInterface() {return &g_HW_I2C1;};

#if !defined(I2Cx_N) || \
	!defined(I2Cx_SCL_GPIO_PORT) || \
	!defined(I2Cx_SDA_GPIO_PORT) || \
	!defined(I2Cx_IT_GROUP_PRIO)
# error params must be defined : I2Cx_N, I2Cx_SCL_GPIO_PORT, I2Cx_SDA_GPIO_PORT, I2Cx_IT_GROUP_PRIO
// next defines for editor hints
# define I2Cx_N                     1
# define I2Cx_SCL_GPIO_PORT         B,6,AF_1
# define I2Cx_SDA_GPIO_PORT         B,7,AF_1
# define I2Cx_IT_GROUP_PRIO         3
# define I2Cx_DMA_TX                1,6,1
# define I2Cx_DMA_RX                1,0,1
#endif

#ifndef MAX_I2C_DEVICES
# define MAX_I2C_DEVICES 3
#endif

#if defined(I2Cx_DMA_N) || \
	defined(I2Cx_DMA_TX_STREAM_N) || \
	defined(I2Cx_DMA_TX_CHANNEL_N) || \
	defined(I2Cx_DMA_RX_STREAM_N) || \
	defined(I2Cx_DMA_RX_CHANNEL_N)
# error I2Cx_DMA_N, I2Cx_DMA_TX_STREAM_N, I2Cx_DMA_TX_CHANNEL_N, I2Cx_DMA_RX_STREAM_N, I2Cx_DMA_RX_CHANNEL_N is obsolete.
# pragma message("Hint: use")
# pragma message("#define I2Cx_DMA_TX   DMA,STREAM,CHANNEL")
# pragma message("#define I2Cx_DMA_RX   DMA,STREAM,CHANNEL")
#endif

#if defined(I2Cx_DMA_TX) && CHECK_EMPTY(THIRD(I2Cx_DMA_TX))
# error invalid I2Cx_DMA_TX defined
# pragma message("Hint: valid format is comma separated 3 numbers for DMA controller, stream, channel")
# pragma message("#define I2Cx_DMA_TX   1,2,3")
#endif
#if defined(I2Cx_DMA_RX) && CHECK_EMPTY(THIRD(I2Cx_DMA_RX))
# error invalid I2Cx_DMA_RX defined
# pragma message("Hint: valid format is comma separated 3 numbers for DMA controller, stream, channel")
# pragma message("#define I2Cx_DMA_TX   1,2,3")
#endif

#include "hw_i2c.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include "util/utils.h"
#include "util/queue.h"
#include "util/callback.h"
#include "util/bitops.h"
#include <stddef.h>
#include <stdint.h>

// configuration
#define I2C_TIMEOUT_START          10
#define I2C_TIMEOUT_TX(nBytes)     (4 * (nBytes * m_nTicksForKB / 1024 + 3))
#define I2C_TIMEOUT_RX(nBytes)     (4 * (nBytes * m_nTicksForKB / 1024 + 3))
#define I2C_TIMEOUT_STOP           20
#define I2C_TIMEOUT_BUS_RECOVER    200
#define I2C_BUS_RECOVER_VALIDATE   5
#define I2C_BUS_RECOVER_BLINK_LO   40
#define I2C_BUS_RECOVER_BLINK_HI   60
#define MinTXSizeForDmaUse         5
#define MinRXSizeForDmaUse         5
#define MAX_F0NBYTES               255

// aliases
#define I2Cx                       CC(I2C,I2Cx_N)
#define CHW_I2Cx                   CC(CHW_I2C,I2Cx_N)
#define CHW_I2Cx_Base              CC(CHW_I2Cx,_Base)
#define CHW_I2Cx_Slave             CC(CHW_I2Cx,_Slave)
#define CHW_I2Cx_Master            CC(CHW_I2Cx,_Master)
#define g_HW_I2Cx                  CC(g_HW_I2C,I2Cx_N)
#define CHW_I2Cx_InterfaceWrapper  CCC(CHW_I2C,I2Cx_N,_InterfaceWrapper)
#define g_HW_I2Cx_InterfaceWrapper CCC(g_HW_I2C,I2Cx_N,_InterfaceWrapper)
#define I2Cx_DMA_TX_Channel        CC(DMA_Channel_,THIRD(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_Stream         CCCC(DMA,FIRST(I2Cx_DMA_TX),_Channel,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_IRQn           CCCCC(DMA,FIRST(I2Cx_DMA_TX),_Channel,SECOND(I2Cx_DMA_TX),_IRQn)
#define I2Cx_DMA_TX_IRQHandler     CCCCC(DMA,FIRST(I2Cx_DMA_TX),_Channel,SECOND(I2Cx_DMA_TX),_IRQHandler)
#define I2Cx_DMA_TX_TC_FLAG        CC(DMA_FLAG_TCIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_HT_FLAG        CC(DMA_FLAG_HTIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_TE_FLAG        CC(DMA_FLAG_TEIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_TC_BIT         CCCC(DMA,FIRST(I2Cx_DMA_TX),_IT_TC,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_TE_BIT         CCCC(DMA,FIRST(I2Cx_DMA_TX),_IT_TE,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_RX_Channel        CC(DMA_Channel_,THIRD(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_Stream         CCCC(DMA,FIRST(I2Cx_DMA_RX),_Channel,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_IRQn           CCCCC(DMA,FIRST(I2Cx_DMA_RX),_Channel,SECOND(I2Cx_DMA_RX),_IRQn)
#define I2Cx_DMA_RX_IRQHandler     CCCCC(DMA,FIRST(I2Cx_DMA_RX),_Channel,SECOND(I2Cx_DMA_RX),_IRQHandler)
#define I2Cx_DMA_RX_TC_FLAG        CC(DMA_FLAG_TCIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_HT_FLAG        CC(DMA_FLAG_HTIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_TE_FLAG        CC(DMA_FLAG_TEIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_TC_BIT         CCCC(DMA,FIRST(I2Cx_DMA_RX),_IT_TC,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_TE_BIT         CCCC(DMA,FIRST(I2Cx_DMA_RX),_IT_TE,SECOND(I2Cx_DMA_RX))
/*----------- I2Cx Interrupt Priority -------------*/
#define I2Cx_IT_EVT_IRQn           IF_STM32F4(CCC(I2C,I2Cx_N,_EV_IRQn))       IF_STM32F0(CCC(I2C,I2Cx_N,_IRQn))
#define I2Cx_IT_EVT_IRQHandler     IF_STM32F4(CCC(I2C,I2Cx_N,_EV_IRQHandler)) IF_STM32F0(CCC(I2C,I2Cx_N,_IRQHandler))
#define I2Cx_IT_ERR_IRQn           CCC(I2C,I2Cx_N,_ER_IRQn)
#define I2Cx_IT_ERR_IRQHandler     CCC(I2C,I2Cx_N,_ER_IRQHandler)
#define I2Cx_IT_EVT_SUBPRIO             2   /* I2Cx EVT SUB-PRIORITY */
#define I2Cx_IT_ERR_SUBPRIO             0   /* I2Cx ERR SUB-PRIORITY */
#define I2Cx_IT_DMATX_SUBPRIO           1   /* I2Cx DMA TX SUB-PRIORITY */
#define I2Cx_IT_DMARX_SUBPRIO           1   /* I2Cx DMA RX SUB-PRIORITY */

#define  DMA1_Channel2_IRQn        DMA1_Channel2_3_IRQn
#define  DMA1_Channel3_IRQn        DMA1_Channel2_3_IRQn
#define  DMA1_Channel4_IRQn        DMA1_Channel4_5_IRQn
#define  DMA1_Channel5_IRQn        DMA1_Channel4_5_IRQn

//#define I2C_DEBUG_INFO(...) DEBUG_INFO("I2C" STR(I2Cx_N) ":" __VA_ARGS__)
#define I2C_DEBUG_INFO(...) {}

#define ASSERT_EVENT_IS_HANDLED_I2C(msg) ASSERT_EVENT_IS_HANDLED(msg ". " IF_STM32F4("SR1:", I2Cx->SR1) IF_STM32F0("ISR:", I2Cx->ISR))

class CHW_I2Cx :
		public ISysTickNotifee
{
public:
	typedef IDeviceI2C::ErrorType ErrorType;

protected:
	typedef enum {
		STATE_FAULT,
		STATE_RECOVER,
		STATE_INIT,
		STATE_IDLE,
		STATE_DATA_START,
		STATE_DATA_TX,
		STATE_DATA_RX,
		STATE_STOP,
	} EState;

public:
	static void Init(unsigned uiClockSpeed);
	static void Deinit();

	static void AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1 = NULL, ISlaveI2C* pSlaveI2CDevice2 = NULL);

	static bool Queue_I2C_Session(IDeviceI2C* pDeviceI2C);
	static void Cancel_I2C_Session(IDeviceI2C* pDeviceI2C);
	static void RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C);

protected:
	static void Init_Pins(bool bI2C);
	static bool Init_I2Cx();
	static void Init_I2Cx_DMA();
	static void Init_I2Cx_IRQ();
	static void Deinit_I2Cx();
	static void Deinit_Pins();

	static void DoStartRx(unsigned uiAddress);
	static void DoReloadRx(unsigned uiAddress);
	static void DoStartTx(unsigned uiAddress);
	static void DoReloadTx(unsigned uiAddress);
	static void DoStartCheck(unsigned uiAddress);
	static void DoTXByte();
	static void DoRXByte();
	static void DoStop();
	static void NextTXChunkIfNecessary();
	static void NextRXChunkIfNecessary();
	static void UpdateTxMode();
	static void UpdateRxMode();
	static void DoPrepareChunkTX();
	static void DoPrepareChunkRX();
#if defined(I2Cx_DMA_TX)
	static void DoPrepareTXDMA();
	static unsigned GetDMATxBytes();
#endif
#if defined(I2Cx_DMA_RX)
	static void DoPrepareRXDMA();
	static unsigned GetDMARxBytes();
#endif
	static void DoEnableEventInterrupt();
	static void DoDisableEventInterrupt();
	static void DoEnableBufferTxInterrupt();
	static void DoEnableBufferRxInterrupt();
	static void DoDisableBufferInterrupt();
	static void DoEnableStopInterrupt();
	static void DoDisableStopInterrupt();
	static void DoCancel();
	typedef enum {
		BUS_OK,
		BUSERROR_LOWSCL,
		BUSERROR_LOWSDA,
		BUSERROR_LOWBOTH,
	} EBusError;
	static EBusError DoCheckBusPins();

protected:
	static unsigned           m_uiClockSpeed;
	static unsigned           m_nTicksForKB;
#if defined(I2Cx_DMA_TX)
	static unsigned           m_uiMinTXSizeForDmaUse;
	static bool               m_bTxDmaReinitRequired;
	static bool               m_bUseTxDma;
#endif
#if defined(I2Cx_DMA_RX)
	static unsigned           m_uiMinRXSizeForDmaUse;
	static bool               m_bRxDmaReinitRequired;
	static bool               m_bUseRxDma;
#endif

	static CChainedConstChunks m_cccTxChunks;
	static CChainedIoChunks    m_cicRxChunks;
	static unsigned           m_uiTxTotalBytesLeft;
	static unsigned           m_uiRxTotalBytesLeft;

	static EState             m_eState;
	static unsigned           m_nTimeoutCountdown;

protected:
	static void Init_Master(unsigned uiClockSpeed);
	static bool PopNextMasterSession();
	static void PrepareMasterSession();
	static void DoStartMasterSession();
//	static void DoStartAgainMasterSession();
//	static void DoStopMasterSession();

	static void NotifyMasterPktDone(IDeviceI2C* pDeviceI2C);
	static void NotifyMasterPktError(ErrorType e, IDeviceI2C* pDeviceToNotify);
	static void NotifyMasterI2CIdle();

protected:
	static IDeviceI2C*        m_pCurrentI2CDevice;
	static TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> m_qSessionsQueue;
	static TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> m_qIdleNotifyQueue;

protected:
	static void ProcessI2CStopEvent(uint32_t uiEvent);
	static void ProcessI2CDataEvent(uint32_t uiEvent);
	static void ProcessI2CDataTxEvent(uint32_t uiEvent);
	static void ProcessI2CDataRestartEvent(uint32_t uiEvent);
	static void ProcessI2CDataRxEvent(uint32_t uiEvent);
	static void ProcessI2CDataReloadEvent(uint32_t uiEvent);
	static void ProcessI2CDataErrorEvent(uint32_t uiEvent);
	static void ProcessI2CDataStopEvent(uint32_t uiEvent);
	static void ProcessI2CxMasterDMATXTC();
	static void ProcessI2CxMasterDMATXTE();
	static void ProcessI2CxMasterDMARXTC();
	static void ProcessI2CxMasterDMARXTE();


public: // ISysTickNotifee
	virtual void OnSysTick();

public: // IRQHandlers
	static void OnI2CxEvent();
	static void OnI2CxDMATX();
	static void OnI2CxDMARX();

private:

private:
	static bool ProcessBusFault();
	static bool ProcessBusRecover();
	static bool ProcessBusInit();
	static bool ProcessQueuedSessions();
	static void ProcessAbortSession(ErrorType e);
};


/****************************************************************************/

unsigned             CHW_I2Cx::m_uiClockSpeed;
unsigned             CHW_I2Cx::m_nTicksForKB;
#if defined(I2Cx_DMA_TX)
unsigned             CHW_I2Cx::m_uiMinTXSizeForDmaUse;
bool                 CHW_I2Cx::m_bTxDmaReinitRequired;
bool                 CHW_I2Cx::m_bUseTxDma;
#endif
#if defined(I2Cx_DMA_RX)
unsigned             CHW_I2Cx::m_uiMinRXSizeForDmaUse;
bool                 CHW_I2Cx::m_bRxDmaReinitRequired;
bool                 CHW_I2Cx::m_bUseRxDma;
#endif
CChainedConstChunks  CHW_I2Cx::m_cccTxChunks;
CChainedIoChunks     CHW_I2Cx::m_cicRxChunks;
unsigned             CHW_I2Cx::m_uiTxTotalBytesLeft;
unsigned             CHW_I2Cx::m_uiRxTotalBytesLeft;
CHW_I2Cx::EState     CHW_I2Cx::m_eState;
unsigned             CHW_I2Cx::m_nTimeoutCountdown;

IDeviceI2C*                          CHW_I2Cx::m_pCurrentI2CDevice;
TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> CHW_I2Cx::m_qSessionsQueue;
TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> CHW_I2Cx::m_qIdleNotifyQueue;

REDIRECT_ISR_STATIC(I2Cx_IT_EVT_IRQHandler, CHW_I2Cx, OnI2CxEvent);
#if defined(I2Cx_DMA_TX)
REDIRECT_ISR_STATIC(I2Cx_DMA_TX_IRQHandler, CHW_I2Cx, OnI2CxDMATX);
#endif
#if defined(I2Cx_DMA_RX)
REDIRECT_ISR_STATIC(I2Cx_DMA_RX_IRQHandler, CHW_I2Cx, OnI2CxDMARX);
#endif

CHW_I2Cx g_HW_I2Cx;

/****************************************************************************/

/*static*/
void CHW_I2Cx::Init(unsigned uiClockSpeed)
{
	m_uiClockSpeed = uiClockSpeed;
	m_nTicksForKB =  (1024 * CHW_Clocks::SysTicksPerSecond) / (1 + m_uiClockSpeed / 10);
	m_eState = STATE_FAULT;
	m_nTimeoutCountdown = 0;

#if defined(I2Cx_DMA_TX)
	m_uiMinTXSizeForDmaUse = MinTXSizeForDmaUse;
	m_bTxDmaReinitRequired = true;
#endif
#if defined(I2Cx_DMA_RX)
	m_uiMinRXSizeForDmaUse = MinRXSizeForDmaUse;
	m_bRxDmaReinitRequired = true;
#endif
	m_cccTxChunks.Clean();
	m_uiTxTotalBytesLeft = 0;
	m_cicRxChunks.Clean();
	m_uiRxTotalBytesLeft = 0;
	m_pCurrentI2CDevice = NULL;

	Init_Pins(false);
	if (ProcessBusFault()) {
		if (ProcessBusRecover()) {
			if (ProcessBusInit()) {
				ProcessQueuedSessions();
			}
		}
	}

	CHW_Clocks::RegisterSysTickCallback(&g_HW_I2Cx);        INDIRECT_CALL(g_HW_I2Cx.OnSysTick());
}

/*static*/
void CHW_I2Cx::Deinit()
{
	CHW_Clocks::UnregisterSysTickCallback(&g_HW_I2Cx);

	Deinit_I2Cx();

	m_eState = STATE_FAULT;
	m_nTimeoutCountdown = 0;
}


/*static*/
void CHW_I2Cx::AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1, ISlaveI2C* pSlaveI2CDevice2)
{
	ASSERT("NOT SUPPORTED");
}

/*static*/
bool CHW_I2Cx::Queue_I2C_Session(IDeviceI2C* pDeviceI2C)
{
	ASSERTE(pDeviceI2C);
	bool bResult = true;
	if (AtomicCmpSet(&m_eState, STATE_IDLE, STATE_DATA_START)) {
		if (AtomicCmpSet(&m_pCurrentI2CDevice, (IDeviceI2C*)NULL, pDeviceI2C)) {
			I2C_DEBUG_INFO("Current Device set:", (int32_t)pDeviceI2C);
			m_nTimeoutCountdown = I2C_TIMEOUT_START;
			I2C_DEBUG_INFO("STATE IDLE->START, TO:", m_nTimeoutCountdown);
			DoStartMasterSession();
			I2C_DEBUG_INFO("Session started:", pDeviceI2C->GetI2CAddress());
			return true;
		} else {
			if (AtomicCmpSet(&m_eState, STATE_DATA_START, STATE_IDLE)) {
				I2C_DEBUG_INFO("STATE revert IDLE->START");
			} else {
				ASSERT("Invalid state");
			}
		}
	}
	I2C_DEBUG_INFO("session queue push:", pDeviceI2C->GetI2CAddress());
	bResult = m_qSessionsQueue.Push(pDeviceI2C);
	ASSERTE(bResult)
	return bResult;
}

/*static*/
void CHW_I2Cx::Cancel_I2C_Session(IDeviceI2C* pDeviceI2C)
{
	ASSERTE(pDeviceI2C);
	if (AtomicCmpSet(&m_pCurrentI2CDevice, pDeviceI2C, (IDeviceI2C*)NULL)) {
		I2C_DEBUG_INFO("Current Device cleared, was:", (int32_t)pDeviceI2C);
		if (m_eState != STATE_IDLE) {
			DEBUG_INFO("I2C aborted by invalid start state:", m_eState);
			ProcessAbortSession(I2C_ABORT);
		}
	} else {
		for (unsigned n = 0; n < MAX_I2C_DEVICES; ++n) {
			IDeviceI2C* pQueuedDeviceI2C;
			if (m_qSessionsQueue.Pop(pQueuedDeviceI2C)) {
				if (pQueuedDeviceI2C != pDeviceI2C) {
					I2C_DEBUG_INFO("session queue re-push:", pQueuedDeviceI2C->GetI2CAddress());
					m_qSessionsQueue.Push(pQueuedDeviceI2C);
				} else {
					DEBUG_INFO("session queue pop (abort):", pQueuedDeviceI2C->GetI2CAddress());
					NotifyMasterPktError(ErrorType::I2C_ABORT, pDeviceI2C);
					break;
				}
			}
		}
	}
}

/*static*/
void CHW_I2Cx::RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C)
{
	bool bResult = m_qIdleNotifyQueue.Push(pDeviceI2C);
	ASSERTE(bResult);
	if (bResult && m_eState == STATE_IDLE) {
		NotifyMasterI2CIdle();
	}
}

/*static*/
void CHW_I2Cx::Init_Pins(bool bI2C)
{
	ASSERTE(m_eState <= STATE_INIT);

	/* PA11 and PA12 remap on QFN28 and TSSOP20 packages */
#if (MPU_PINS_COUNT <= 28)
# if ((CC(0x0,FIRST(I2Cx_SCL_GPIO_PORT)) == 0x0A) && (SECOND(I2Cx_SCL_GPIO_PORT) == 11)) \
  || ((CC(0x0,FIRST(I2Cx_SDA_GPIO_PORT)) == 0x0A) && (SECOND(I2Cx_SCL_GPIO_PORT) == 12))
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG->CFGR1 |= SYSCFG_CFGR1_PA11_PA12_RMP;
# endif
#endif

	/* Initialize I2Cx GPIO */
	/* Enable I2Cx SCL and SDA Pin Clock */
	RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(I2Cx_SCL_GPIO_PORT)) | CC(RCC_AHBPeriph_GPIO,FIRST(I2Cx_SDA_GPIO_PORT)), ENABLE);
	if (bI2C) {
		INIT_GPIO_AF_OD_PU_FAST(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT), THIRD(I2Cx_SCL_GPIO_PORT,CC(AF_I2C,I2Cx_N)));
		INIT_GPIO_AF_OD_PU_FAST(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT), THIRD(I2Cx_SDA_GPIO_PORT,CC(AF_I2C,I2Cx_N)));
	} else {
		INIT_GPIO_IN_PU(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT));
		INIT_GPIO_IN_PU(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT));
	}
}

/*static*/
bool CHW_I2Cx::Init_I2Cx()
{
	ASSERTE(m_eState == STATE_INIT);

	/* Initialize I2Cx Clock */
	RCC_APB1PeriphResetCmd(CC(RCC_APB1Periph_I2C,I2Cx_N), ENABLE);
	RCC_APB1PeriphResetCmd(CC(RCC_APB1Periph_I2C,I2Cx_N), DISABLE);
	RCC_APB1PeriphClockCmd(CC(RCC_APB1Periph_I2C,I2Cx_N), ENABLE);

	/* Initialize I2Cx device with parameters */
	I2C_InitTypeDef I2C_InitStructure;
	I2C_StructInit(&I2C_InitStructure);
//	const unsigned tClockSpeed_ns = 1000000000 / m_uiClockSpeed;
//	const unsigned tMaxSCL_ns = m_uiClockSpeed * 3;
//	const unsigned tSCL_ns = 1000000000 / (CHW_Clocks::GetCLKFrequency() / timing_PRESC);
	union TI2CTiming {
		struct TFlds {
			uint32_t SCLL     :8;
			uint32_t SCLH     :8;
			uint32_t SDADEL   :4;
			uint32_t SCLDEL   :4;
			uint32_t _rsrv    :4;
			uint32_t PRESC    :4;
			TFlds(uint32_t aPRESC, uint32_t aSCLDEL, uint32_t aSDADEL, uint32_t aSCLH, uint32_t aSCLL):
				SCLL(aSCLH),SCLH(aSCLH),SDADEL(aSDADEL),SCLDEL(aSCLDEL),_rsrv(0),PRESC(aPRESC) {}
		};

		TFlds flds;
		uint32_t timing;

		TI2CTiming(uint32_t aPRESC, uint32_t aSCLDEL, uint32_t aSDADEL, uint32_t aSCLH, uint32_t aSCLL):
			flds(aPRESC, aSCLDEL, aSDADEL, aSCLH, aSCLL) {}
		TI2CTiming(uint32_t aTiming) : timing(aTiming) {}
	};
	switch (CHW_Clocks::GetCLKFrequency()) {
	case 48000000:
		switch (m_uiClockSpeed) {
		case 10000:   I2C_InitStructure.I2C_Timing = TI2CTiming(0xB, 0x4, 0x2, 0xC3, 0xC7).timing; break;
		case 100000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0xB, 0x4, 0x2, 0x0F, 0x13).timing; break;
		case 400000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x5, 0x3, 0x3, 0x03, 0x09).timing; break;
		case 1000000: I2C_InitStructure.I2C_Timing = TI2CTiming(0x5, 0x1, 0x0, 0x01, 0x03).timing; break;
		default:      ASSERTE(false); // TODO implement I2C timing calculation for custom I2C speed
		}
		break;
	case 16000000:
		switch (m_uiClockSpeed) {
		case 10000:   I2C_InitStructure.I2C_Timing = TI2CTiming(0x3, 0x4, 0x2, 0xC3, 0xC7).timing; break;
		case 100000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x3, 0x4, 0x2, 0x0F, 0x13).timing; break;
		case 400000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x1, 0x3, 0x2, 0x03, 0x09).timing; break;
		case 1000000: I2C_InitStructure.I2C_Timing = TI2CTiming(0x0, 0x2, 0x0, 0x02, 0x04).timing; break;
		default:      ASSERTE(false); // TODO implement I2C timing calculation for custom I2C speed
		}
		break;
	case 8000000:
		switch (m_uiClockSpeed) {
		case 10000:   I2C_InitStructure.I2C_Timing = TI2CTiming(0x1, 0x4, 0x2, 0xC3, 0xC7).timing; break;
		case 100000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x1, 0x4, 0x2, 0x0F, 0x13).timing; break;
		case 400000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x0, 0x3, 0x1, 0x03, 0x09).timing; break;
		case 1000000: I2C_InitStructure.I2C_Timing = TI2CTiming(0x0, 0x1, 0x0, 0x03, 0x06).timing; break;
		default:      ASSERTE(false); // TODO implement I2C timing calculation for custom I2C speed
		}
		break;
	default:
		ASSERTE(false); // TODO implement I2C timing calculation for custom CLK freq
	}
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);

	I2C_InitStructure.I2C_Mode                = I2C_Mode_I2C;                  /* Initialize the I2C_Mode member */
	I2C_InitStructure.I2C_Ack                 = I2C_Ack_Enable;                /* Initialize the I2C_Ack member */
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;  /* Initialize the I2C_AcknowledgedAddress member */
	I2C_Init(I2Cx, &I2C_InitStructure);
	I2C_DEBUG_INFO("I2C_Init");

	/* Disable I2C events interrupt - it would be enabled on comm starts */
	DoDisableEventInterrupt();

	return true;
}

void CHW_I2Cx::Init_I2Cx_DMA()
{
	ASSERTE(m_eState == STATE_INIT);

	/* Initialize I2Cx DMA Channels */
	/* Enable I2Cx DMA */
#if defined(I2Cx_DMA_TX)
	RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_DMA,FIRST(I2Cx_DMA_TX)), ENABLE);
	/* Initialize I2Cx DMA Tx Stream */
	m_bTxDmaReinitRequired = true; // do it later - when TX op starts
#endif
#if defined(I2Cx_DMA_RX)
# if defined(I2Cx_DMA_TX) && FIRST(I2Cx_DMA_TX) != FIRST(I2Cx_DMA_RX)
	RCC_AHBPeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(I2Cx_DMA_RX)), ENABLE);
# endif
	/* Initialize I2Cx DMA Rx Stream */
	m_bRxDmaReinitRequired = true; // do it later - when RX op starts
#endif
}

void CHW_I2Cx::Init_I2Cx_IRQ()
{
	ASSERTE(m_eState == STATE_INIT);

	/* Configure NVIC priority Group */
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	/* Enable the IRQ channel */
	NVIC_INIT(I2Cx_IT_EVT_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_EVT_SUBPRIO);
#if defined(I2Cx_DMA_TX)
	/* Configure NVIC for DMA TX channel interrupt */
	NVIC_INIT(I2Cx_DMA_TX_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_DMATX_SUBPRIO);
#endif
#if defined(I2Cx_DMA_RX)
	/* Configure NVIC for DMA RX channel interrupt */
	NVIC_INIT(I2Cx_DMA_RX_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_DMARX_SUBPRIO);
#endif
}

void CHW_I2Cx::Deinit_I2Cx()
{
	DoCancel();

	/* Reset I2Cx Device */
	I2C_SoftwareResetCmd(I2Cx);
	I2C_DEBUG_INFO("I2C_SoftwareResetCmd");

	/* Disable I2Cx Device */
	I2C_Cmd(I2Cx, DISABLE);
	I2C_DEBUG_INFO("I2C_Cmd :DISABLED");
	I2C_DeInit(I2Cx);
	I2C_DEBUG_INFO("I2C_Deinit");

	Deinit_Pins();

	/* Deinitialize DMA Channels */
#if defined(I2Cx_DMA_TX)
    DMA_DeInit(I2Cx_DMA_TX_Stream);
    m_bTxDmaReinitRequired = true;
#endif
#if defined(I2Cx_DMA_RX)
    DMA_DeInit(I2Cx_DMA_RX_Stream);
    m_bRxDmaReinitRequired = true;
#endif
}

/*static*/
void CHW_I2Cx::Deinit_Pins()
{
	/* Deinitialize I2Cx GPIO */
	DEINIT_GPIO(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT));
	DEINIT_GPIO(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT));
}


/*static*/
void CHW_I2Cx::DoEnableEventInterrupt()
{
	I2C_DEBUG_INFO("Event IRQ enabled");
	I2C_ITConfig(I2Cx, I2C_IT_NACKI | I2C_IT_STOPI | I2C_IT_TCI | I2C_IT_ERRI, ENABLE);
	I2C_DEBUG_INFO("I2C_ITConfig ENABLE");
	INDIRECT_CALL(CHW_I2Cx::OnI2CxEvent());
}

/*static*/
void CHW_I2Cx::DoDisableEventInterrupt()
{
	I2C_ITConfig(I2Cx, I2C_IT_NACKI | I2C_IT_STOPI | I2C_IT_TCI | I2C_IT_ERRI, DISABLE);
	I2C_DEBUG_INFO("I2C_ITConfig DISABLE");
	NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
	I2C_DEBUG_INFO("Event IRQ disabled");
}

/*static*/
void CHW_I2Cx::DoEnableBufferTxInterrupt()
{
	I2C_ITConfig(I2Cx, I2C_IT_TXI, ENABLE);
	I2C_ITConfig(I2Cx, I2C_IT_RXI, DISABLE);
	I2C_DEBUG_INFO("I2C_ITConfig TXI ENABLE RXI DISABLE");
	I2C_DEBUG_INFO("Buffer IRQ TX enabled");
	INDIRECT_CALL(CHW_I2Cx::OnI2CxEvent());
}

/*static*/
void CHW_I2Cx::DoEnableBufferRxInterrupt()
{
	I2C_ITConfig(I2Cx, I2C_IT_TXI, DISABLE);
	I2C_ITConfig(I2Cx, I2C_IT_RXI, ENABLE);
	I2C_DEBUG_INFO("I2C_ITConfig TXI DISABLE RXI ENABLE");
	I2C_DEBUG_INFO("Buffer IRQ RX enabled");
	INDIRECT_CALL(CHW_I2Cx::OnI2CxEvent());
}

/*static*/
void CHW_I2Cx::DoDisableBufferInterrupt()
{
	I2C_ITConfig(I2Cx, I2C_IT_TXI | I2C_IT_RXI, DISABLE);
	I2C_DEBUG_INFO("I2C_ITConfig TX RX DISABLE");
	NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
	I2C_DEBUG_INFO("Buffer IRQ disabled");
}

/*static*/
void CHW_I2Cx::DoEnableStopInterrupt()
{
	I2C_ITConfig(I2Cx, I2C_IT_STOPI, ENABLE);
	I2C_DEBUG_INFO("I2C_ITConfig STOP ENABLE");
	I2C_DEBUG_INFO("STOP IRQ enabled");
	INDIRECT_CALL(CHW_I2Cx::OnI2CxEvent());
}

/*static*/
void CHW_I2Cx::DoDisableStopInterrupt()
{
	I2C_ITConfig(I2Cx, I2C_IT_STOPI, DISABLE);
	I2C_DEBUG_INFO("I2C_ITConfig STOP DISABLE");
	NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
	I2C_DEBUG_INFO("STOP IRQ disabled");
}

/*static*/
void CHW_I2Cx::DoCancel()
{
	I2C_DEBUG_INFO("Cancel I2C");
#if defined(I2Cx_DMA_TX)
	I2C_DMACmd(I2Cx, I2C_DMAReq_Tx, DISABLE);
	I2C_DEBUG_INFO("I2C_DMACmd Tx DISABLE");
	DMA_Cmd(I2Cx_DMA_TX_Stream, DISABLE);
	DMA_ITConfig(I2Cx_DMA_TX_Stream, DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMA_ClearITPendingBit(I2Cx_DMA_TX_TC_BIT | I2Cx_DMA_TX_TE_BIT);
	NVIC_ClearPendingIRQ(I2Cx_DMA_TX_IRQn);
#endif
#if defined(I2Cx_DMA_RX)
	I2C_DMACmd(I2Cx, I2C_DMAReq_Rx, DISABLE);
	I2C_DEBUG_INFO("I2C_DMACmd Rx DISABLE");
	DMA_Cmd(I2Cx_DMA_RX_Stream, DISABLE);
	DMA_ITConfig(I2Cx_DMA_RX_Stream, DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMA_ClearITPendingBit(I2Cx_DMA_RX_TC_BIT | I2Cx_DMA_RX_TE_BIT);
	NVIC_ClearPendingIRQ(I2Cx_DMA_RX_IRQn);
#endif
	/* Disable I2C Interrupts - it would be enabled on comm starts */
	DoDisableEventInterrupt();
	DoDisableBufferInterrupt();
	I2C_ReceiveData(I2Cx);      I2C_DEBUG_INFO("I2C_ReceiveData clear BTF, RXE");
	I2C_SendData(I2Cx, 0);      I2C_DEBUG_INFO("I2C_SendData clear TXIS");
	I2Cx->ISR |= I2C_FLAG_TXE;  I2C_DEBUG_INFO("I2C flush TXDR");
	ASSERTE(I2C_GetFlagStatus(I2Cx, I2C_FLAG_TXE));
	I2C_ClearITPendingBit(I2Cx, I2C_IT_ALERT | I2C_IT_TIMEOUT | I2C_IT_PECERR | I2C_IT_OVR | I2C_IT_NACKF | I2C_IT_ARLO | I2C_IT_BERR | I2C_IT_ADDR);
	I2C_DEBUG_INFO("I2C_ClearITPendingBit all but STOP");
}

/*static*/
CHW_I2Cx::EBusError CHW_I2Cx::DoCheckBusPins()
{
	ASSERTE(m_eState <= STATE_INIT);

	/* Check bus state */
	bool bStateScl = READ_GPIO_PORT(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT));
	bool bStateSda = READ_GPIO_PORT(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT));

	return (bStateScl && bStateSda) ? BUS_OK : (bStateScl ? BUSERROR_LOWSDA : (bStateSda ? BUSERROR_LOWSCL : BUSERROR_LOWBOTH));
}

void CHW_I2Cx::DoStartRx(unsigned uiAddress)
{
	ASSERTE(m_eState == STATE_DATA_RX);
	ASSERTE(m_uiRxTotalBytesLeft) ;
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)) ;

	DoPrepareChunkRX();

	if (m_uiRxTotalBytesLeft > MAX_F0NBYTES) {
#ifndef I2Cx_DMA_RX_Channel
		ASSERT("ERRATA: Last-received byte loss in reload mode. Use I2Cx_DMA_RX");
#endif
		I2C_TransferHandling(I2Cx, uiAddress << 1, MAX_F0NBYTES, I2C_Reload_Mode, I2C_Generate_Start_Read);
		I2C_DEBUG_INFO("I2C_TransferHandling Rx last. NBYTES:", MAX_F0NBYTES);
		// continue in CHW_I2Cx::OnI2CxEvent (TCR expected)
	} else {
		I2C_TransferHandling(I2Cx, uiAddress << 1, m_uiRxTotalBytesLeft, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
		I2C_DEBUG_INFO("I2C_TransferHandling Rx last. NBYTES:", m_uiRxTotalBytesLeft);
		// continue in CHW_I2Cx::OnI2CxEvent (STOPF expected)
	}
}

void CHW_I2Cx::DoReloadRx(unsigned uiAddress)
{
	ASSERTE(m_eState == STATE_DATA_RX);
	ASSERTE(m_uiRxTotalBytesLeft) ;

	DoPrepareChunkRX();

	if (m_uiRxTotalBytesLeft > MAX_F0NBYTES) {
		I2C_TransferHandling(I2Cx, uiAddress << 1, MAX_F0NBYTES, I2C_Reload_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Rx last. NBYTES:", MAX_F0NBYTES);
		// continue in CHW_I2Cx::OnI2CxEvent (TCR expected)
	} else {
		I2C_TransferHandling(I2Cx, uiAddress << 1, m_uiRxTotalBytesLeft, I2C_AutoEnd_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Rx last. NBYTES:", m_uiRxTotalBytesLeft);
		// continue in CHW_I2Cx::OnI2CxEvent (STOPF expected)
	}
}

void CHW_I2Cx::DoStartTx(unsigned uiAddress)
{
	ASSERTE(m_uiTxTotalBytesLeft);
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)) ;

	DoPrepareChunkTX();

	if (m_uiTxTotalBytesLeft > MAX_F0NBYTES) {
		I2C_TransferHandling(I2Cx, uiAddress << 1, MAX_F0NBYTES, I2C_Reload_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Tx. NBYTES:", MAX_F0NBYTES);
		// continue in CHW_I2Cx::OnI2CxEvent (TCR expected)
	} else if (m_uiRxTotalBytesLeft) {
		I2C_TransferHandling(I2Cx, uiAddress << 1, m_uiTxTotalBytesLeft, I2C_SoftEnd_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Tx before Rx. NBYTES:", m_uiTxTotalBytesLeft);
		// continue in CHW_I2Cx::OnI2CxEvent (TC expected)
	} else {
		I2C_TransferHandling(I2Cx, uiAddress << 1, m_uiTxTotalBytesLeft, I2C_AutoEnd_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Tx last. NBYTES:", m_uiTxTotalBytesLeft);
		// continue in CHW_I2Cx::OnI2CxEvent (STOPF expected)
	}
	ASSERTE(I2C_GetFlagStatus(I2Cx, I2C_FLAG_TXE));
//	I2C_MasterRequestConfig(I2Cx, I2C_Direction_Transmitter); // already done in I2C_TransferHandling
	if (m_uiTxTotalBytesLeft) {
		DoTXByte();
	} else {
		I2C_SendData(I2Cx, 0);
		I2C_DEBUG_INFO("I2C_SendData dummy");
	}
	I2C_GenerateSTART(I2Cx, ENABLE);
	I2C_DEBUG_INFO("I2C_GenerateSTART");
}

void CHW_I2Cx::DoReloadTx(unsigned uiAddress)
{
	ASSERTE(m_uiTxTotalBytesLeft);

	DoPrepareChunkTX();

	if (m_uiTxTotalBytesLeft > MAX_F0NBYTES) {
		I2C_TransferHandling(I2Cx, uiAddress << 1, MAX_F0NBYTES, I2C_Reload_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Tx. NBYTES:", MAX_F0NBYTES);
		// continue in CHW_I2Cx::OnI2CxEvent (TCR expected)
	} else if (m_uiRxTotalBytesLeft) {
		I2C_TransferHandling(I2Cx, uiAddress << 1, m_uiTxTotalBytesLeft, I2C_SoftEnd_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Tx before Rx. NBYTES:", m_uiTxTotalBytesLeft);
		// continue in CHW_I2Cx::OnI2CxEvent (TC expected)
	} else {
		I2C_TransferHandling(I2Cx, uiAddress << 1, m_uiTxTotalBytesLeft, I2C_AutoEnd_Mode, I2C_No_StartStop);
		I2C_DEBUG_INFO("I2C_TransferHandling Tx last. NBYTES:", m_uiTxTotalBytesLeft);
		// continue in CHW_I2Cx::OnI2CxEvent (STOPF expected)
	}
}

void CHW_I2Cx::DoStartCheck(unsigned uiAddress)
{
	// no tx/rx - only check connection to the address
	ASSERTE(!m_uiTxTotalBytesLeft) ;
	ASSERTE(!m_uiRxTotalBytesLeft) ;
	I2C_TransferHandling(I2Cx, uiAddress << 1, 0, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);
	I2C_DEBUG_INFO("I2C_TransferHandling Check");
	// continue in CHW_I2Cx::OnI2CxEvent (STOPF)
}

void CHW_I2Cx::DoTXByte()
{
	ASSERTE(m_eState == STATE_DATA_TX);
	ASSERTE(m_cccTxChunks.pData);
	ASSERTE(m_cccTxChunks.uiLength);
	ASSERTE(m_uiTxTotalBytesLeft);
	uint8_t uiByteToSend = 0;
	if (m_cccTxChunks.PopData(&uiByteToSend, 1) == 1) {
		I2C_DEBUG_INFO("TX next byte. (non DMA):", uiByteToSend);
		I2C_SendData(I2Cx, uiByteToSend);
		I2C_DEBUG_INFO("I2C_SendData :", uiByteToSend);
	} else {
		ASSERT("Pop fault");
	}
	--m_uiTxTotalBytesLeft;
	I2C_DEBUG_INFO("TX bytes left:", m_uiTxTotalBytesLeft);

	NextTXChunkIfNecessary();
}

void CHW_I2Cx::DoRXByte()
{
	ASSERTE(m_eState == STATE_DATA_RX);
	ASSERTE(m_cicRxChunks.pData);
	ASSERTE(m_cicRxChunks.uiLength);
	ASSERTE(m_uiRxTotalBytesLeft);
	uint8_t uiByteReceived = I2C_ReceiveData(I2Cx);
	I2C_DEBUG_INFO("I2C_ReceiveData :", uiByteReceived);
	m_cicRxChunks.PushData(&uiByteReceived, 1);
	--m_uiRxTotalBytesLeft;

	NextRXChunkIfNecessary();
}

void CHW_I2Cx::DoStop()
{
	I2C_GenerateSTOP(I2Cx, ENABLE);
	I2C_DEBUG_INFO("I2C_GenerateSTOP: Master");
}

void CHW_I2Cx::NextTXChunkIfNecessary()
{
	if (!m_cccTxChunks.uiLength && m_uiTxTotalBytesLeft) {
		I2C_DEBUG_INFO("TX chunk done");
		const CChainedConstChunks* pNextTxChunk = m_cccTxChunks.GetNextItem();
		ASSERTE(pNextTxChunk);
		m_cccTxChunks = *pNextTxChunk;
		DoPrepareChunkTX();
	}
}

void CHW_I2Cx::NextRXChunkIfNecessary()
{
	if (!m_cicRxChunks.uiLength && m_uiRxTotalBytesLeft) {
		const CChainedIoChunks* pNextRxChunk = m_cicRxChunks.GetNextItem();
		ASSERTE(pNextRxChunk);
		m_cicRxChunks = *pNextRxChunk;
		DoPrepareChunkRX();
	}
}

void CHW_I2Cx::UpdateTxMode()
{
#if defined(I2Cx_DMA_TX)
	m_bUseTxDma = (m_cccTxChunks.uiLength >= m_uiMinTXSizeForDmaUse);
#endif
}

void CHW_I2Cx::UpdateRxMode()
{
#if defined(I2Cx_DMA_RX)
	m_bUseRxDma = (m_cicRxChunks.uiLength >= m_uiMinRXSizeForDmaUse);
#endif
}

void CHW_I2Cx::DoPrepareChunkTX()
{
	ASSERTE(m_cccTxChunks.uiLength);

	UpdateTxMode();

	I2C_DEBUG_INFO("Next TX chunk");
	I2C_DEBUG_INFO("data:", (uint32_t)m_cccTxChunks.pData);
	I2C_DEBUG_INFO("len:", m_cccTxChunks.uiLength);
#if defined(I2Cx_DMA_TX)
	I2C_DEBUG_INFO("dma?:", m_bUseTxDma);
	if (m_bUseTxDma) {
		DoDisableBufferInterrupt();
		DoPrepareTXDMA();
		I2C_DMACmd(I2Cx, I2C_DMAReq_Tx, ENABLE);
		I2C_DEBUG_INFO("I2C_DMACmd Tx ENABLE");
	} else
#endif
	{
		DoEnableBufferTxInterrupt();
	}
}

void CHW_I2Cx::DoPrepareChunkRX()
{
	ASSERTE(m_cicRxChunks.uiLength);

	UpdateRxMode();

	I2C_DEBUG_INFO("Next RX chunk");
	I2C_DEBUG_INFO("data:", (uint32_t)m_cicRxChunks.pData);
	I2C_DEBUG_INFO("len:", m_cicRxChunks.uiLength);
#if defined(I2Cx_DMA_TX)
	I2C_DEBUG_INFO("dma?:", m_bUseRxDma);
	if (m_bUseRxDma) {
		DoDisableBufferInterrupt();
		DoPrepareRXDMA();
		I2C_DMACmd(I2Cx, I2C_DMAReq_Rx, ENABLE);
		I2C_DEBUG_INFO("I2C_DMACmd Rx ENABLE");
	} else
#endif
	{
		DoEnableBufferRxInterrupt();
	}
}

#if defined(I2Cx_DMA_TX)
/*static*/
void CHW_I2Cx::DoPrepareTXDMA()
{
	ASSERTE(m_cccTxChunks.pData);
	ASSERTE(m_cccTxChunks.uiLength);
	ASSERTE(m_uiTxTotalBytesLeft);
	uint32_t uiMemoryBaseAddr = (uint32_t)(m_cccTxChunks.pData);
	uint16_t uiLength = MIN2(m_cccTxChunks.uiLength, (unsigned)MAX_F0NBYTES);
	if (m_bTxDmaReinitRequired) {
		DMA_InitTypeDef DMAInitStructure;
		DMA_StructInit(&DMAInitStructure);
		DMAInitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
		DMAInitStructure.DMA_MemoryBaseAddr = uiMemoryBaseAddr;
		DMAInitStructure.DMA_BufferSize = uiLength;
		DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(I2Cx->TXDR);
		DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMAInitStructure.DMA_Mode = DMA_Mode_Normal;
		DMAInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		/* Select I2Cx DR Address register as DMA PeripheralBaseAddress */
		DMA_Init(I2Cx_DMA_TX_Stream, &DMAInitStructure);

		/* Enable DMA TX Channel TC, TE  */
		DMA_ITConfig(I2Cx_DMA_TX_Stream, DMA_IT_TC | DMA_IT_TE, ENABLE);
	} else {
		I2Cx_DMA_TX_Stream->CMAR = uiMemoryBaseAddr;
		DMA_SetCurrDataCounter(I2Cx_DMA_TX_Stream, uiLength);
	}
	I2C_DEBUG_INFO("DMA TX start len:", uiLength);
	I2C_DEBUG_INFO("DMA TX start ptr:", uiMemoryBaseAddr);
	m_cccTxChunks.DiscardData(uiLength);
	m_uiTxTotalBytesLeft -= uiLength;
	if (m_uiTxTotalBytesLeft && !m_cccTxChunks.uiLength) {

	}
	DMA_Cmd(I2Cx_DMA_TX_Stream, ENABLE);
}

/*static*/
unsigned CHW_I2Cx::GetDMATxBytes()
{
	unsigned uiBytesLeft = DMA_GetCurrDataCounter(I2Cx_DMA_TX_Stream);
	ASSERTE(uiBytesLeft <= m_cccTxChunks.uiLength);
	return m_cccTxChunks.uiLength - uiBytesLeft;
}
#endif

#if defined(I2Cx_DMA_RX)

/*static*/
void CHW_I2Cx::DoPrepareRXDMA()
{
	ASSERTE(m_cicRxChunks.pData);
	ASSERTE(m_cicRxChunks.uiLength);
	ASSERTE(m_uiRxTotalBytesLeft);
	uint32_t uiMemoryBaseAddr = (uint32_t)(m_cicRxChunks.pData);
	uint16_t uiLength = MIN2(m_cicRxChunks.uiLength, (unsigned)MAX_F0NBYTES);
	if (m_bRxDmaReinitRequired) {
		DMA_InitTypeDef DMAInitStructure;
		DMA_StructInit(&DMAInitStructure);
		DMAInitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
		DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(I2Cx->RXDR);
		DMAInitStructure.DMA_MemoryBaseAddr = uiMemoryBaseAddr;
		DMAInitStructure.DMA_BufferSize = uiLength;
		DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMAInitStructure.DMA_Mode = DMA_Mode_Normal;
		DMAInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		/* Select I2Cx DR Address register as DMA PeripheralBaseAddress */
		DMA_Init(I2Cx_DMA_RX_Stream, &DMAInitStructure);

		/* Enable DMA RX Channel TC, TE  */
		DMA_ITConfig(I2Cx_DMA_RX_Stream, DMA_IT_TC | DMA_IT_TE, ENABLE);
	} else {
		I2Cx_DMA_RX_Stream->CMAR = uiMemoryBaseAddr;
		DMA_SetCurrDataCounter(I2Cx_DMA_RX_Stream, uiLength);
	}
	I2C_DEBUG_INFO("DMA RX start len:", uiLength);
	I2C_DEBUG_INFO("DMA RX start ptr:", uiMemoryBaseAddr);
	m_cicRxChunks.DiscardData(uiLength);
	m_uiRxTotalBytesLeft -= uiLength;
	DMA_Cmd(I2Cx_DMA_RX_Stream, ENABLE);
}

/*static*/
unsigned CHW_I2Cx::GetDMARxBytes()
{
	unsigned uiBytesLeft = DMA_GetCurrDataCounter(I2Cx_DMA_RX_Stream);
	ASSERTE(uiBytesLeft <= m_cicRxChunks.uiLength);
	return m_cicRxChunks.uiLength - uiBytesLeft;
}
#endif

void CHW_I2Cx::PrepareMasterSession()
{
	ASSERTE(m_eState == STATE_DATA_START);
	ASSERTE(m_pCurrentI2CDevice);
	ASSERTE(m_uiTxTotalBytesLeft == 0);
	ASSERTE(m_uiRxTotalBytesLeft == 0);
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));

	const CChainedConstChunks* pDeviceChunksToSend;
	const CChainedIoChunks*    pDeviceChunksToRecv;
	m_pCurrentI2CDevice->GetI2CPacket(&pDeviceChunksToSend, &pDeviceChunksToRecv);
	if (pDeviceChunksToSend) m_cccTxChunks = *pDeviceChunksToSend;
	else {m_cccTxChunks.Clean(); m_cccTxChunks.Break();}
	if (pDeviceChunksToRecv) m_cicRxChunks = *pDeviceChunksToRecv;
	else {m_cicRxChunks.Clean(); m_cicRxChunks.Break();}

#ifdef DEBUG
	if (m_cccTxChunks.uiLength) {
		const CChainedConstChunks* pNextTxChunk = &m_cccTxChunks;
		while (pNextTxChunk) {
			ASSERTE(pNextTxChunk->pData);
			ASSERTE(pNextTxChunk->uiLength);
			pNextTxChunk = pNextTxChunk->GetNextItem();
		}
	}
	if (m_cicRxChunks.uiLength) {
		const CChainedIoChunks* pNextRxChunk = &m_cicRxChunks;
		while (pNextRxChunk) {
			ASSERTE(pNextRxChunk->pData);
			ASSERTE(pNextRxChunk->uiLength);
			pNextRxChunk = pNextRxChunk->GetNextItem();
		}
	}
#endif
	m_uiRxTotalBytesLeft = CollectChainTotalLength(&m_cicRxChunks);
	m_uiTxTotalBytesLeft = CollectChainTotalLength(&m_cccTxChunks);
}

void CHW_I2Cx::DoStartMasterSession()
{
	ASSERTE(m_eState == STATE_DATA_START);
	ASSERTE(m_pCurrentI2CDevice);
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));
	I2C_DEBUG_INFO("Start. Addr:", m_pCurrentI2CDevice->GetI2CAddress());
	I2C_DEBUG_INFO("Start. ISR:", I2Cx->ISR);
	I2C_DEBUG_INFO("Start. CR1:", I2Cx->CR1);
	I2C_DEBUG_INFO("Start. CR2:", I2Cx->CR2);
	ASSERTE(I2Cx->ISR == 0x01);
	ASSERTE(I2Cx->CR1 == 0x01);
	ASSERTE((I2Cx->CR2 & I2C_CR2_STOP) == 0);
	DoEnableEventInterrupt();
	PrepareMasterSession();
	if (m_uiTxTotalBytesLeft) { // for master do TX before RX
		AtomicCmpSet(&m_eState, STATE_DATA_START, STATE_DATA_TX);
		m_nTimeoutCountdown = I2C_TIMEOUT_TX(m_uiTxTotalBytesLeft);
		I2C_DEBUG_INFO("STATE START->TX, TO:", m_nTimeoutCountdown);
		DoStartTx(m_pCurrentI2CDevice->GetI2CAddress());
	} else if (m_uiRxTotalBytesLeft) {
		AtomicCmpSet(&m_eState, STATE_DATA_START, STATE_DATA_RX);
		m_nTimeoutCountdown = I2C_TIMEOUT_RX(m_uiRxTotalBytesLeft);
		I2C_DEBUG_INFO("STATE START->TX, TO:", m_nTimeoutCountdown);
		DoStartRx(m_pCurrentI2CDevice->GetI2CAddress());
	} else { // check
		DoStartCheck(m_pCurrentI2CDevice->GetI2CAddress());
	}
	// continue processing in CHW_I2Cx::OnI2CxEvent (expect SB)
}

void CHW_I2Cx::NotifyMasterPktDone(IDeviceI2C* pDeviceToNotify)
{
	I2C_DEBUG_INFO("Done Notify");
	ASSERTE(pDeviceToNotify);
	pDeviceToNotify->OnI2CDone();
}

void CHW_I2Cx::NotifyMasterPktError(ErrorType e, IDeviceI2C* pDeviceToNotify)
{
	I2C_DEBUG_INFO("Error Notify e:",e);
	ASSERTE(pDeviceToNotify);
	pDeviceToNotify->OnI2CError(e);
}

void CHW_I2Cx::NotifyMasterI2CIdle()
{
	I2C_DEBUG_INFO("Idle Notify");
	IDeviceI2C* pDeviceI2C;
	while (m_eState == STATE_IDLE && m_qIdleNotifyQueue.Pop(pDeviceI2C)) {
		I2C_DEBUG_INFO("idle callback :", pDeviceI2C->GetI2CAddress());
		pDeviceI2C->OnI2CIdle();
	}
}

/****************************************************************************/

void CHW_I2Cx::OnSysTick()
{
	IMPLEMENTS_INTERFACE_METHOD(ISysTickNotifee::OnSysTick());

	if (m_nTimeoutCountdown) {
		--m_nTimeoutCountdown;
	}
	switch (m_eState) {
		case STATE_FAULT:
			ProcessBusFault();
		break;
		case STATE_RECOVER:
			if (ProcessBusRecover()) {
				if (ProcessBusInit()) {
					ProcessQueuedSessions();
				}
			}
			break;
		case STATE_INIT:
		case STATE_IDLE:
			// do nothing
			break;
		case STATE_DATA_START:
		case STATE_DATA_TX:
		case STATE_DATA_RX:
		case STATE_STOP:
			switch (m_nTimeoutCountdown) {
			case 1: // 1st stage: disable I2C to prevent IRQ for 2nd stage
				I2C_DEBUG_INFO("TIMEOUT -> DISABLED, State:", m_eState);
				I2C_Cmd(I2Cx, DISABLE);
				break;
			case 0:  // 2nd stage: no IRQ so it is safe to change state
				I2C_DEBUG_INFO("TIMEOUT -> FAULT, State:", m_eState);
				ProcessAbortSession(I2C_ERROR_TIMEOUT);
				break;
			}
			break;
	}
}

void CHW_I2Cx::OnI2CxEvent()
{
	uint32_t uiEvent = I2Cx->ISR;
	I2C_DEBUG_INFO("Event IRQ: event:", uiEvent);
	switch (m_eState) {
		case STATE_FAULT:
		case STATE_RECOVER:
		case STATE_INIT:
		case STATE_IDLE:
			ASSERT("Unexpected IRQ, state:", m_eState);
			break;
		case STATE_DATA_START:
		case STATE_DATA_TX:
		case STATE_DATA_RX:
			ProcessI2CDataEvent(uiEvent);
			break;
		case STATE_STOP:
			ProcessI2CStopEvent(uiEvent);
			break;
	}
	if (m_eState == STATE_IDLE) {
		ProcessQueuedSessions();
	}
	I2C_DEBUG_INFO("Event IRQ finished. ISR:", I2Cx->ISR);
}

#if defined(I2Cx_DMA_TX)
void CHW_I2Cx::OnI2CxDMATX()
{
	IS_EVENT_HANDLER;
	bool bTC = DMA_GetITStatus(I2Cx_DMA_TX_TC_BIT);
	if (bTC) {
		DMA_ClearITPendingBit(I2Cx_DMA_TX_TC_BIT);
	}
	bool bTE = DMA_GetITStatus(I2Cx_DMA_TX_TE_BIT);
	if (bTE) {
		DMA_ClearITPendingBit(I2Cx_DMA_TX_TE_BIT);
	}
	if (bTC && bTE) { I2C_DEBUG_INFO("DMATXIRQ: TC TE"); }
	else if (bTC) { I2C_DEBUG_INFO("DMATXIRQ: TC"); }
	else if (bTE) { I2C_DEBUG_INFO("DMATXIRQ: TE"); }
	else { I2C_DEBUG_INFO("DMATXIRQ: ??"); }
	switch (m_eState) {
		case STATE_FAULT:
		case STATE_RECOVER:
		case STATE_INIT:
		case STATE_IDLE:
		case STATE_DATA_START:
		case STATE_DATA_TX:
			if (bTC) ProcessI2CxMasterDMATXTC();
			if (bTE) ProcessI2CxMasterDMATXTE();
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case STATE_DATA_RX:
		case STATE_STOP:
			break;
	}
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C Tx DMA IRQ");
}

void CHW_I2Cx::ProcessI2CxMasterDMATXTC()
{
	ASSERTE(m_eState == STATE_DATA_TX);
	const CChainedConstChunks* pNextTxChunk = m_cccTxChunks.GetNextItem();
	I2C_DEBUG_INFO("Next Tx chunk:", (uint32_t)pNextTxChunk);
	if (pNextTxChunk) {
		m_cccTxChunks = *pNextTxChunk;
		I2C_DEBUG_INFO("data:", (uint32_t)m_cccTxChunks.pData);
		I2C_DEBUG_INFO("len:", m_cccTxChunks.uiLength);
		I2C_DEBUG_INFO("dma?:", m_bUseTxDma);
		ASSERTE(m_uiTxTotalBytesLeft >= m_cccTxChunks.uiLength);
		DoPrepareTXDMA();
		m_uiTxTotalBytesLeft -= m_cccTxChunks.uiLength;
//		I2C_DMALastTransferCmd(I2Cx, (m_cccTxChunks.GetNextItem() || m_cicRxChunks.uiLength)?DISABLE:ENABLE);
		m_nTimeoutCountdown = I2C_TIMEOUT_TX(m_cccTxChunks.uiLength);
	} else {
		ASSERTE(m_uiTxTotalBytesLeft == 0);
		m_uiTxTotalBytesLeft = 0;
		ASSERTE(m_uiTxTotalBytesLeft == 0);
		I2C_DMACmd(I2Cx, I2C_DMAReq_Tx, DISABLE);
//		AtomicCmpSet(&m_eState, STATE_DATA_TX, STATE_DATA_RX);
	}
}

void CHW_I2Cx::ProcessI2CxMasterDMATXTE()
{
	ASSERTE(m_eState == STATE_DATA_TX);
	ProcessAbortSession(ErrorType::I2C_ERROR_DMA);
}
#endif

#if defined(I2Cx_DMA_RX)
void CHW_I2Cx::OnI2CxDMARX()
{
	IS_EVENT_HANDLER;
	I2C_DEBUG_INFO("I2C DMA Rx");
	bool bTC = DMA_GetITStatus(I2Cx_DMA_RX_TC_BIT);
	if (bTC) {
		DMA_ClearITPendingBit(I2Cx_DMA_RX_TC_BIT);
	}
	bool bTE = DMA_GetITStatus(I2Cx_DMA_RX_TE_BIT);
	if (bTE) {
		DMA_ClearITPendingBit(I2Cx_DMA_RX_TE_BIT);
	}
	switch (m_eState) {
		case STATE_FAULT:
		case STATE_RECOVER:
		case STATE_INIT:
		case STATE_IDLE:
		case STATE_DATA_START:
		case STATE_DATA_TX:
			break;
		case STATE_DATA_RX:
			if (bTC) ProcessI2CxMasterDMARXTC();
			if (bTE) ProcessI2CxMasterDMARXTE();
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case STATE_STOP:
			break;
	}
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C Rx DMA IRQ");
}

void CHW_I2Cx::ProcessI2CxMasterDMARXTC()
{
	ASSERTE(m_eState == STATE_DATA_RX);
	const CChainedIoChunks* pNextRxChunk = m_cicRxChunks.GetNextItem();
	I2C_DEBUG_INFO("Next Rx chunk:", (uint32_t)pNextRxChunk);
	if (pNextRxChunk) {
		m_cicRxChunks = *pNextRxChunk;
		I2C_DEBUG_INFO("data:", (uint32_t)m_cicRxChunks.pData);
		I2C_DEBUG_INFO("len:", m_cicRxChunks.uiLength);
		I2C_DEBUG_INFO("dma?:", m_bUseTxDma);
		ASSERTE(m_uiRxTotalBytesLeft >= m_cicRxChunks.uiLength);
		DoPrepareRXDMA();
		m_uiRxTotalBytesLeft -= m_cicRxChunks.uiLength;
		m_nTimeoutCountdown = I2C_TIMEOUT_RX(m_cicRxChunks.uiLength);
	} else {
		ASSERTE(m_uiRxTotalBytesLeft == 0);
		I2C_DMACmd(I2Cx, I2C_DMAReq_Rx, DISABLE);
		I2C_DEBUG_INFO("DMA RX disabled");
		DoStop();
		// continue on OnI2CxEvent (STOPF)
	}
	//DoStopSession();
}

void CHW_I2Cx::ProcessI2CxMasterDMARXTE()
{
	// todo rx error processing
	ASSERTE("I2C DMA Rx error");
	ProcessAbortSession(ErrorType::I2C_ABORT);
}
#endif

void CHW_I2Cx::ProcessI2CStopEvent(uint32_t uiEvent)
{
	ASSERTE(m_eState == STATE_STOP);
	ASSERTE(!m_pCurrentI2CDevice);

	I2C_DEBUG_INFO("STOP event:", uiEvent);

	if (uiEvent & I2C_FLAG_STOPF) {
		DoDisableStopInterrupt();
		I2C_ClearFlag(I2Cx, I2C_FLAG_STOPF);
		I2C_DEBUG_INFO("I2C_ClearFlag STOP");
		AtomicCmpSet(&m_eState, STATE_STOP, STATE_IDLE);
		I2C_DEBUG_INFO("STATE STOP->IDLE");
		ASSERTE(I2C_GetFlagStatus(I2Cx, I2C_FLAG_TXE));
		IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_STOPF | I2C_FLAG_TXE | I2C_FLAG_RXNE));
	}

	__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_SB status cleared and isr not trigger again

	IF_DEBUG(
		if (uiEvent) {
			ASSERT("invalid stop event flags not handled:", uiEvent);
		}
	)
}

void CHW_I2Cx::ProcessI2CDataEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("DATA event:", uiEvent);
	if (uiEvent & I2C_FLAG_RXNE) {
		ProcessI2CDataRxEvent(uiEvent);
		IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_RXNE | I2C_FLAG_TXE));
	}
	if (uiEvent & (I2C_FLAG_BERR | I2C_FLAG_ARLO | I2C_FLAG_NACKF)) {
		ProcessI2CDataErrorEvent(uiEvent);
		IF_DEBUG(SetBits0(uiEvent, (I2C_FLAG_BERR | I2C_FLAG_ARLO | I2C_FLAG_NACKF | I2C_FLAG_BUSY | I2C_FLAG_TXE | I2C_FLAG_STOPF | I2C_FLAG_TXIS)));
	} else {
		if (uiEvent & I2C_FLAG_STOPF) {
			ProcessI2CDataStopEvent(uiEvent);
			IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_STOPF | I2C_FLAG_TXE));
		}
		if (uiEvent & I2C_FLAG_TC) {
			ProcessI2CDataRestartEvent(uiEvent);
			IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_TC | I2C_FLAG_TXE));
		}
		if (uiEvent & I2C_FLAG_TCR) {
			ProcessI2CDataReloadEvent(uiEvent);
			IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_TCR | I2C_FLAG_TXE));
		}
		if (uiEvent & I2C_FLAG_TXIS) {
			ASSERTE(m_eState == STATE_DATA_TX);
			ProcessI2CDataTxEvent(uiEvent);
			IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_TXIS | I2C_FLAG_TXE));
		}
		if (uiEvent & I2C_FLAG_BUSY) {
			ASSERTE(m_eState == STATE_DATA_TX || m_eState == STATE_DATA_RX, "BUSY unexpected");
			IF_DEBUG(SetBits0(uiEvent, I2C_FLAG_BUSY));
		}
	}
	__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_SB status cleared and isr not trigger again

	IF_DEBUG(
		if (uiEvent) {
			ASSERT("invalid idle event not handled:", (int)uiEvent);
			ProcessAbortSession(ErrorType::I2C_ABORT);
		}
	)
}

void CHW_I2Cx::ProcessI2CDataTxEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("Data Tx event:", uiEvent);

	ASSERTE(m_eState == STATE_DATA_TX);

	DoTXByte();
}

void CHW_I2Cx::ProcessI2CDataRestartEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("Data Reload event:", uiEvent);

	ASSERTE(m_eState == STATE_DATA_TX);

	AtomicCmpSet(&m_eState, STATE_DATA_TX, STATE_DATA_RX);
	m_nTimeoutCountdown = 2 * (m_uiRxTotalBytesLeft * m_nTicksForKB / 1024 + 3);
	I2C_DEBUG_INFO("STATE TX->RX, TO:", m_nTimeoutCountdown);

	DoStartRx(m_pCurrentI2CDevice->GetI2CAddress());
}

void CHW_I2Cx::ProcessI2CDataRxEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("Data Rx event:", uiEvent);

	ASSERTE(m_eState == STATE_DATA_RX);

	DoRXByte();
}

void CHW_I2Cx::ProcessI2CDataReloadEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("Data reload event:", uiEvent);

	if (m_eState == STATE_DATA_TX) {
		DoReloadTx(m_pCurrentI2CDevice->GetI2CAddress());
	} else
	if (m_eState == STATE_DATA_RX) {
		DoReloadRx(m_pCurrentI2CDevice->GetI2CAddress());
	} else {
		ASSERT("Invalid state for reload");
	}
}

void CHW_I2Cx::ProcessI2CDataStopEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("Data stop event:", uiEvent);
	ASSERTE(m_eState == STATE_DATA_START || m_eState == STATE_DATA_TX || m_eState == STATE_DATA_RX);
	if (!m_uiTxTotalBytesLeft && !m_uiRxTotalBytesLeft) {
#if defined(I2Cx_DMA_TX)
		I2C_DMACmd(I2Cx, I2C_DMAReq_Tx, DISABLE);
#endif
#if defined (I2Cx_DMA_RX)
		I2C_DMACmd(I2Cx, I2C_DMAReq_Rx, DISABLE);
#endif
		DoDisableBufferInterrupt();
		DoDisableEventInterrupt();
		I2C_ClearFlag(I2Cx, I2C_FLAG_STOPF);
		I2C_DEBUG_INFO("I2C_ClearFlag STOP");
		if (AtomicCmpSet(&m_eState, STATE_DATA_START, STATE_IDLE)) {
			I2C_DEBUG_INFO("STATE START->IDLE");
		} else if (AtomicCmpSet(&m_eState, STATE_DATA_TX, STATE_IDLE)) {
			I2C_DEBUG_INFO("STATE TX->IDLE");
		} else if (AtomicCmpSet(&m_eState, STATE_DATA_RX, STATE_IDLE)) {
			I2C_DEBUG_INFO("STATE RX->IDLE");
		} else {
			ASSERT("Invalid state");
		}
		IDeviceI2C* pDeviceI2C = NULL;
		AtomicExchange(&m_pCurrentI2CDevice, &pDeviceI2C);
		I2C_DEBUG_INFO("Current Device cleared, was:", (int32_t)pDeviceI2C);
		NotifyMasterPktDone(pDeviceI2C);
	} else {
		DEBUG_INFO("I2C session aborted by STOP, state:", m_eState);
		ASSERT("Session aborted");
		ProcessAbortSession(I2C_ABORT);
//		ASSERT("Unexpected stop (TX left)");
		ASSERTE(I2C_GetFlagStatus(I2Cx, I2C_FLAG_TXE));
	}
}

void CHW_I2Cx::ProcessI2CDataErrorEvent(uint32_t uiEvent)
{
	I2C_DEBUG_INFO("DATA error event:", uiEvent);
	ErrorType e = (
		(uiEvent & I2C_FLAG_BERR) ? I2C_ERROR_BUS : (
		(uiEvent & I2C_FLAG_ARLO) ? I2C_ERROR_ARB : (
			I2C_ERROR_ACK)
	));
	I2C_ClearFlag(I2Cx, (I2C_FLAG_BERR | I2C_FLAG_ARLO | I2C_FLAG_NACKF));
	I2C_DEBUG_INFO("I2C_ClearFlag BERR, ARLO, NACK");
	ProcessAbortSession(e);
}

bool CHW_I2Cx::ProcessBusFault()
{
	ASSERTE(m_eState == STATE_FAULT);

	bool bResult = false;

	EBusError eError = DoCheckBusPins();
	switch (eError) {
		case BUS_OK:
			Init_Pins(false);
			AtomicCmpSet(&m_eState, STATE_FAULT, STATE_RECOVER);
			m_nTimeoutCountdown = I2C_BUS_RECOVER_VALIDATE;
			I2C_DEBUG_INFO("STATE FAULT->RECOVER, TO:", m_nTimeoutCountdown);
			bResult = true;
			break;
		case BUSERROR_LOWSCL:
			// SCL LOW. slave stuck. blink SDA to try unstuck
			if (!m_nTimeoutCountdown) {
				m_nTimeoutCountdown = I2C_BUS_RECOVER_BLINK_HI + I2C_BUS_RECOVER_BLINK_LO;
				I2C_DEBUG_INFO("Blink SDA for recover SCL low");
				INIT_GPIO_OUT_OD_PU(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT), 0);
			} else if (m_nTimeoutCountdown <= I2C_BUS_RECOVER_BLINK_HI) {
				// Release pins after recovering blink
				Init_Pins(false);
			}
			break;
		case BUSERROR_LOWSDA:
			// SDA LOW. slave stuck. blink SCL to try unstuck
			if (!m_nTimeoutCountdown) {
				m_nTimeoutCountdown = I2C_BUS_RECOVER_BLINK_HI + I2C_BUS_RECOVER_BLINK_LO;
				I2C_DEBUG_INFO("Blink SCL for recover SDA low");
				INIT_GPIO_OUT_OD_PU(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT), 0);
			} else if (m_nTimeoutCountdown <= I2C_BUS_RECOVER_BLINK_HI) {
				// Release pins after recovering blink
				Init_Pins(false);
			}
			break;
		case BUSERROR_LOWBOTH:
			if (m_nTimeoutCountdown > I2C_BUS_RECOVER_BLINK_HI) {
			} else {
				// Release SCL, SDA after recovering blink
				Init_Pins(false);
				// nothing we can do
			}
			break;
	}

	return bResult;
}

bool CHW_I2Cx::ProcessBusRecover()
{
	ASSERTE(m_eState == STATE_RECOVER);

	bool bResult = false;

	EBusError eError = DoCheckBusPins();
	switch (eError) {
		case BUS_OK:
			if (!m_nTimeoutCountdown) {
				AtomicCmpSet(&m_eState, STATE_RECOVER, STATE_INIT);
				I2C_DEBUG_INFO("STATE RECOVER->INIT");
				bResult = true;
			}
			break;
		case BUSERROR_LOWSCL:
		case BUSERROR_LOWSDA:
		case BUSERROR_LOWBOTH:
			AtomicCmpSet(&m_eState, STATE_RECOVER, STATE_FAULT);
			m_nTimeoutCountdown = 0;
			I2C_DEBUG_INFO("STATE RECOVER->FAULT");
			I2C_DEBUG_INFO("Recover fault");
			break;
	}
	return bResult;
}

bool CHW_I2Cx::ProcessBusInit()
{
	ASSERTE(m_eState == STATE_INIT);

	bool bResult = false;

	Deinit_I2Cx();
	if (DoCheckBusPins() == BUS_OK) {
		Init_I2Cx();
#if defined(I2Cx_DMA_TX) || defined(I2Cx_DMA_RX)
		Init_I2Cx_DMA();
#endif
		Init_I2Cx_IRQ();
		Init_Pins(true);
		/* Enable I2Cx Device */
		I2C_Cmd(I2Cx, ENABLE);
		I2C_DEBUG_INFO("I2C_Cmd ENABLE");

		AtomicCmpSet(&m_eState, STATE_INIT, STATE_IDLE);
		I2C_DEBUG_INFO("STATE INIT->IDLE");
		bResult = true;
	} else {
		AtomicCmpSet(&m_eState, STATE_INIT, STATE_FAULT);
		m_nTimeoutCountdown = 0;
		I2C_DEBUG_INFO("STATE INIT->FAULT");
		// continue in CHW_I2Cx::OnSysTick interrupt handler
	}

	return bResult;
}

bool CHW_I2Cx::ProcessQueuedSessions()
{
	bool bResult;
	if (m_eState != STATE_IDLE) {
		bResult = false;
	}

	IDeviceI2C* pI2CDevice;
	if (m_qSessionsQueue.Pop(pI2CDevice)) {
		ASSERTE(pI2CDevice);
		I2C_DEBUG_INFO("Session queue pop:", pI2CDevice->GetI2CAddress());
		if (AtomicCmpSet(&m_pCurrentI2CDevice, (IDeviceI2C*)NULL, pI2CDevice)) {
			if (AtomicCmpSet(&m_eState, STATE_IDLE, STATE_DATA_START)) {
				I2C_DEBUG_INFO("Current Device set:", (int32_t)pI2CDevice);
				m_nTimeoutCountdown = I2C_TIMEOUT_START;
				I2C_DEBUG_INFO("STATE IDLE->START, TO:", m_nTimeoutCountdown);
				DoStartMasterSession();
				return true;
			} else {
				AtomicCmpSet(&m_pCurrentI2CDevice, pI2CDevice, (IDeviceI2C*)NULL);
				I2C_DEBUG_INFO("Current Device cleared, was:", (int32_t)pI2CDevice);
				ASSERT("Unexpected state change");
			}
		} else {
			ASSERT("invalid current device");
		}
		// push device back to queue
		m_qSessionsQueue.Push(pI2CDevice);
		return false;
	} else {
		I2C_DEBUG_INFO("session queue empty");
		NotifyMasterI2CIdle();
		bResult = false;
	}
	return bResult;
}

void CHW_I2Cx::ProcessAbortSession(ErrorType e)
{
	I2C_DEBUG_INFO("Session aborted. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);

	DoCancel();

	if (e == I2C_ERROR_BUS || e == I2C_ERROR_ARB || e == I2C_ERROR_TIMEOUT) {
		m_nTimeoutCountdown = 0;
		m_eState = STATE_FAULT;
		I2C_DEBUG_INFO("STATE ->FAULT");
	} else if (I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)) {
		I2C_ClearFlag(I2Cx, I2C_FLAG_STOPF);
		I2C_ClearFlag(I2Cx, I2C_FLAG_STOPF);
		if (AtomicCmpSet(&m_eState, STATE_DATA_START, STATE_IDLE)) {
			I2C_DEBUG_INFO("STATE START->IDLE");
		} else if (AtomicCmpSet(&m_eState, STATE_DATA_TX, STATE_IDLE)) {
			I2C_DEBUG_INFO("STATE TX->IDLE");
		} else if (AtomicCmpSet(&m_eState, STATE_DATA_RX, STATE_IDLE)) {
			I2C_DEBUG_INFO("STATE RX->IDLE");
		} else {
			ASSERT("Invalid state:", e);
		}
		I2C_DEBUG_INFO("Stop already detected");
	} else {
		DoEnableStopInterrupt();
		DoStop();
		if (AtomicCmpSet(&m_eState, STATE_DATA_START, STATE_STOP)) {
			m_nTimeoutCountdown = I2C_TIMEOUT_STOP;
			I2C_DEBUG_INFO("STATE START->STOP, TO:", m_nTimeoutCountdown);
		} else if (AtomicCmpSet(&m_eState, STATE_DATA_TX, STATE_STOP)) {
			m_nTimeoutCountdown = I2C_TIMEOUT_STOP;
			I2C_DEBUG_INFO("STATE TX->STOP, TO:", m_nTimeoutCountdown);
		} else if (AtomicCmpSet(&m_eState, STATE_DATA_RX, STATE_STOP)) {
			m_nTimeoutCountdown = I2C_TIMEOUT_STOP;
			I2C_DEBUG_INFO("STATE RX->STOP, TO:", m_nTimeoutCountdown);
		} else {
			ASSERT("Invalid state:", e);
		}
	}

	IDeviceI2C* pDeviceI2C = NULL;
	AtomicExchange(&m_pCurrentI2CDevice, &pDeviceI2C);
	I2C_DEBUG_INFO("Current Device cleared, was:", (int32_t)pDeviceI2C);
	if (pDeviceI2C) {
		m_cccTxChunks.Clean(); m_cccTxChunks.Break();
		m_cicRxChunks.Clean(); m_cicRxChunks.Break();
		m_uiRxTotalBytesLeft = 0;
		m_uiTxTotalBytesLeft = 0;

		NotifyMasterPktError(e, pDeviceI2C);
	} else {
		ASSERTE(m_cccTxChunks.uiLength == 0 && m_cccTxChunks.GetNextItem() == NULL);
		ASSERTE(m_cicRxChunks.uiLength == 0 && m_cicRxChunks.GetNextItem() == NULL);
	}
}

