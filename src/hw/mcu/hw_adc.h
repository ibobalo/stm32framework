#ifndef HW_ADC_H_INCLUDED
#define HW_ADC_H_INCLUDED

#include "interfaces/notify.h"
#include "interfaces/aio.h"
#include <stdint.h>
#include "hw_adc_in.h"
#include "util/macros.h"

#ifndef AVDD_mV
#define AVDD_mV 3300
#endif

#define ADC_COUNT                   IF_STM32F0(1) IF_STM32F4(3)
#ifndef ADC_CHANNELS_USED
# define ADC_CHANNELS_USED ADC_CHANNELS_COUNT
#endif

//TODO namespace CMCU {

template<class CHWADCx>
class THWADCx_InterfaceWrapper :
	public virtual IADC
{
public: //IADC
	virtual void AttachAIN(unsigned nChIndex) {
		IMPLEMENTS_INTERFACE_METHOD(IADC::AttachAIN(nChIndex));
		switch(nChIndex) {
#define CASE_ATTACH(N) case N: CHWADCx::template AttachAIN<N>(); break;
			REPEAT(ADC_CHANNELS_COUNT, CASE_ATTACH)
#undef CASE_ATTACH
		}
	}
	virtual void DetachAIN(unsigned nChIndex) {
		IMPLEMENTS_INTERFACE_METHOD(IADC::DetachAIN(nChIndex));
		switch(nChIndex) {
#define CASE_DETACH(N) case N: CHWADCx::template DetachAIN<N>(); break;
			REPEAT(ADC_CHANNELS_COUNT, CASE_DETACH)
#undef CASE_DETACH
		}
	}

	virtual void SetSampleNotifier(IADCSampleNotifier* pNotifier) {IMPLEMENTS_INTERFACE_METHOD(IADC::SetSampleNotifier(pNotifier)); CHWADCx::SetSampleNotifier(pNotifier);}
	virtual void SetSequenceNotifier(IADCSequenceNotifier* pNotifier) {IMPLEMENTS_INTERFACE_METHOD(IADC::SetSequenceNotifier(pNotifier)); CHWADCx::SetSequenceNotifier(pNotifier);}
	virtual void SetBufferNotifier(IADCBufferNotifier* pNotifier) {IMPLEMENTS_INTERFACE_METHOD(IADC::SetBufferNotifier(pNotifier)); CHWADCx::SetBufferNotifier(pNotifier);}
	virtual void SetErrorNotifier(IADCErrorNotifier* pNotifier) {IMPLEMENTS_INTERFACE_METHOD(IADC::SetErrorNotifier(pNotifier)); CHWADCx::SetErrorNotifier(pNotifier);}

	virtual void SetSamplesBuffer(volatile uint16_t* pBuffer, unsigned uiMaxSamplesCount) {IMPLEMENTS_INTERFACE_METHOD(IADC::SetSamplesBuffer(pBuffer, uiMaxSamplesCount)); CHWADCx::SetSamplesBuffer(pBuffer, uiMaxSamplesCount);}

	virtual void InitAINs() {IMPLEMENTS_INTERFACE_METHOD(IADC::InitAINs()); CHWADCx::InitAINs();}
	virtual void Start() {IMPLEMENTS_INTERFACE_METHOD(IADC::Start()); CHWADCx::Start();}
	virtual void Stop() {IMPLEMENTS_INTERFACE_METHOD(IADC::Stop()); CHWADCx::Stop();}
	virtual void DeinitAINs() {IMPLEMENTS_INTERFACE_METHOD(IADC::DeinitAINs()); CHWADCx::DeinitAINs();}

	virtual unsigned GetSamplesCount() const {IMPLEMENTS_INTERFACE_METHOD(IADC::GetSamplesCount()); return CHWADCx::GetSamplesCount();}
	virtual volatile const uint16_t* GetSamples() const {IMPLEMENTS_INTERFACE_METHOD(IADC::GetSamples()); return CHWADCx::GetSamples();}
	virtual uint16_t GetSample(unsigned nScanIndex) const {IMPLEMENTS_INTERFACE_METHOD(IADC::GetSample(nScanIndex)); return CHWADCx::GetSample(nScanIndex);}
};

template <unsigned N>
class CHW_ADC
{
public:
	static constexpr unsigned AdcIndex = N;
	typedef CHW_ADC<N> type;
	typedef uint16_t value_type;
public:
	static inline type* Acquire(bool bContinuous = true) {
		type::Init(bContinuous);
		static type instance;
		return &instance;
	}
	static inline IADC* AcquireInterface(bool bContinuous = true) {
		Acquire(bContinuous);
		static THWADCx_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static inline void Release() {
		type::Deinit();
	}
public:
	static void Init(bool bContinuous = true);
	static void Deinit();
	template<unsigned ChIndex> static void AttachAIN() {
		STATIC_ASSERTE(ChIndex >= 0 && ChIndex < ADC_CHANNELS_COUNT, AdcChannelIndexOutOfBounds);
		ASSERTE(!m_abScanEnabled[ChIndex]);
#if !defined(ADCx_DMA) && !defined(ADCx_IT_PRIO)
		ASSERTE(!m_nScanInputsCount, "define ADCx_DMA and/or ADCx_IT_PRIO for multiple AINs")
#endif
		m_abScanEnabled[ChIndex] = true;
		m_nScanInputsCount ++;
	}
	template<unsigned ChIndex> static void DetachAIN() {
		ASSERTE(!m_bChannelsReady); /* DeinitAIN before ReleaseAIN */
		ASSERTE(!m_bStarted); /*Stop before ReleaseAIN*/
		STATIC_ASSERTE(ChIndex >= 0 && ChIndex < ADC_CHANNELS_COUNT, AdcChannelIndexOutOfBounds);
		ASSERTE(m_abScanEnabled[ChIndex]);
		ASSERTE(m_nScanInputsCount > 0);
		m_abScanEnabled[ChIndex] = false;
		m_nScanInputsCount --;
	}
public:
	static void SetSampleNotifier(IADCSampleNotifier* pNotifier);
	static void SetSequenceNotifier(IADCSequenceNotifier* pNotifier);
	static void SetBufferNotifier(IADCBufferNotifier* pNotifier);
	static void SetErrorNotifier(IADCErrorNotifier* pNotifier);
	static void InitAINs();
	static void Start();
	static void Stop();
	static void DeinitAINs();
	static void SetSamplesBuffer(volatile value_type* pBuffer, unsigned uiMaxSamplesCount);
	static unsigned GetSamplesCount();
	static volatile const value_type* GetSamples();
	static value_type GetSample(unsigned nChannelIndex);
	static value_type GetRecentSample();
public: /* ISR handlers */
	static void OnADC(); /* single channel conversion */
	static void OnDMA(); /* all samples collected */
private:
	static void DoReadSample(uint16_t uiValue);
	static void NotifyError();
	static void NotifySample();
	static void NotifySequence();
	static void NotifyBuffer();
	static void NotifyChannels();
	static void NotifyChannelsMultiple();
	template<uint8_t ChIndex, uint32_t ADC_Channel_x> static void Init_Channel(uint8_t nScanRank);
	template<uint8_t ChIndex, uint32_t ADC_Channel_x> static void Deinit_Channel();
	static void Init_ADC();
	static void Init_Channels();
	static void Flush_ADC();
	static void Start_ADC();
	static void Stop_ADC();
	static void Deinit_Channels();
	static void Deinit_ADC();
	static void Init_DMA();
	static void Start_DMA();
	static void Stop_DMA();
	static void Deinit_DMA();
	static bool IsStarted() {return m_bStarted;}
protected:
	static unsigned  m_nScanInputsCount;
	static volatile value_type*  m_pauiADCSamplesBuff;
	static bool m_bStarted;
private:
	static bool m_bContinuous;
	static bool m_bChannelsReady;
	static IADCSampleNotifier* m_pSampleNotifier;
	static IADCSequenceNotifier* m_pSequenceNotifier;
	static IADCBufferNotifier* m_pBufferNotifier;
	static IADCErrorNotifier* m_pErrorNotifier;
	static unsigned  m_nADCSamplesBuffSize;
	static unsigned  m_nADCSamplesToRead;
	static unsigned  m_nDataRead;
	static unsigned  m_nChannelSamplesCount;
	static volatile value_type m_auiInternalDataBuffer[ADC_CHANNELS_USED];
	static bool m_abScanEnabled[ADC_CHANNELS_COUNT];
};
/*static*/ template<unsigned N> unsigned CHW_ADC<N>::m_nScanInputsCount = 0;
/*static*/ template<unsigned N> volatile typename CHW_ADC<N>::value_type*  CHW_ADC<N>::m_pauiADCSamplesBuff = NULL;
/*static*/ template<unsigned N> bool CHW_ADC<N>::m_bChannelsReady = false;
/*static*/ template<unsigned N> bool CHW_ADC<N>::m_bStarted = false;
/*static*/ template<unsigned N> bool CHW_ADC<N>::m_bContinuous = false;
/*static*/ template<unsigned N> IADCSampleNotifier* CHW_ADC<N>::m_pSampleNotifier = 0;
/*static*/ template<unsigned N> IADCSequenceNotifier* CHW_ADC<N>::m_pSequenceNotifier = 0;
/*static*/ template<unsigned N> IADCBufferNotifier* CHW_ADC<N>::m_pBufferNotifier = 0;
/*static*/ template<unsigned N> IADCErrorNotifier* CHW_ADC<N>::m_pErrorNotifier = 0;
/*static*/ template<unsigned N> unsigned  CHW_ADC<N>::m_nADCSamplesBuffSize = 0;
/*static*/ template<unsigned N> unsigned  CHW_ADC<N>::m_nADCSamplesToRead = 0;
/*static*/ template<unsigned N> unsigned  CHW_ADC<N>::m_nDataRead = 0;
/*static*/ template<unsigned N> unsigned  CHW_ADC<N>::m_nChannelSamplesCount = 0;
/*static*/ template<unsigned N> volatile typename CHW_ADC<N>::value_type CHW_ADC<N>::m_auiInternalDataBuffer[ADC_CHANNELS_USED] = {0};
/*static*/ template<unsigned N> bool      CHW_ADC<N>::m_abScanEnabled[ADC_CHANNELS_COUNT] = {false};
///*static*/ template<unsigned N> uint8_t   CHW_ADC<N>::m_anInputIndexToScanRank[ADC_CHANNELS_COUNT] = {0};

//#define DECLARE_ADC(N) typedef CHW_ADC<N> CHW_ADC##N;
//REPEAT1(ADC_COUNT, DECLARE_ADC)


#define ACQUIRE_RELEASE_ADC_DECLARATION(ADC_N, ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	using CADC_##ID##__VA_ARGS__ = CHW_ADC<ADC_N>; \
	static inline CADC_##ID##__VA_ARGS__* AcquireADC_##ID##__VA_ARGS__(bool bContinuous = true) { \
		return CADC_##ID##__VA_ARGS__::Acquire(bContinuous);} \
	static inline void ReleaseADC_##ID##__VA_ARGS__() {CADC_##ID##__VA_ARGS__::Release();} \
	static inline IADC* AcquireADCInterface_##ID##__VA_ARGS__(bool bContinuous = true) { \
		return CADC_##ID##__VA_ARGS__::AcquireInterface(bContinuous);} \
	static inline void ReleaseADCInterface_##ID##__VA_ARGS__() {CADC_##ID##__VA_ARGS__::Release();}

#define ALIAS_ADC(ID,ALIAS) \
	using CADC_##ALIAS = CADC_##ID;

#define ACQUIRE_RELEASE_ADC_DEFINITION(ADC_N,CHWBRD,ID,...) /* DEPRECATED, use CADC_X::Acquire(...) */

#endif /* HW_ADC_H_INCLUDED */
