// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define EXTIx_PORT          A,5
// //next defines is optional
// #define EXTIx_TRIGGER       Rising_Falling
// #include "hw_exti.cxx"
// IDigitalInEXTI* getEXTIInterfaceA14() {return &g_HW_EXTI_A5;};

#if !defined(EXTIx_PORT)
#error params must be defined : EXTIx_PORT
// next defines for editor hints
#define EXTIx_PORT          A,5
#define EXTIx_TRIGGER       Rising_Falling
#endif

#include "stdafx.h"
#include "hw_exti.h"
#include "../hw_macros.h"
#include "../debug.h"

#define DINx_PORT    EXTIx_PORT
#include "hw_din.cxx"

/**************************************************************************************
 * ISR (interrupts handling - redirecting to CHW_MCU methods
 *************************************************************************************/
#if defined(STM32F0) && (SECOND(EXTIx_PORT)>=0) && (SECOND(EXTIx_PORT)<=1)
#  define EXTIx_ID EXTI0_1
#elif defined(STM32F0) && (SECOND(EXTIx_PORT)>=2) && (SECOND(EXTIx_PORT)<=3)
#  define EXTIx_ID EXTI2_3
#elif defined(STM32F0) && (SECOND(EXTIx_PORT)>=4) && (SECOND(EXTIx_PORT)<=15)
#  define EXTIx_ID EXTI4_15
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)==0)
#  define EXTIx_ID EXTI0
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)==1)
#  define EXTIx_ID EXTI1
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)==2)
#  define EXTIx_ID EXTI2
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)==3)
#  define EXTIx_ID EXTI3
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)==4)
#  define EXTIx_ID EXTI4
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)>=5) && (SECOND(EXTIx_PORT)<=9)
#  define EXTIx_ID EXTI9_5
#elif defined(STM32F4) && (SECOND(EXTIx_PORT)>=10) && (SECOND(EXTIx_PORT)<=15)
#  define EXTIx_ID EXTI15_10
#else
#  error undefined EXTI
#endif

#define CParentDINx  CCC(CHW_DIN_,FIRST(EXTIx_PORT),SECOND(EXTIx_PORT))
#define CHW_EXTIx    CC(CHW_EXTI_,EXTIx_ID)
#define g_HW_EXTIx   CCC(g_HW_EXTI_,FIRST(EXTIx_PORT),SECOND(EXTIx_PORT))

class CHW_EXTIx;

class CHW_EXTIx :
		public IDigitalInProvider,
		public CParentDINx
{
public:
	static void Init(bool bPullUp = false, bool bPullDown = false);
	static void Deinit();
public: // IDigitalInEXTI
	virtual void SetNotifier(IDigitalInNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0);
public: // IDigitalInEXTI::IDigitalIn::IValueSource<bool>
	virtual bool GetValue() const;
public: // IDigitalInEXTI::IValueProvider<bool>
	virtual void SetValueTargetInterface(IValueReceiver<bool>* pValueTarget);

//	static bool                  m_bPrevState;
	static IDigitalInNotifier*   m_pNotifier;
	static IValueReceiver<bool>* m_pValueTarget;

public: // Interrupts
	static void OnEXTI();
};

/*************************************************************************************/
// static
//bool CHW_EXTIx::m_bPrevState;
/*static*/
IDigitalInNotifier* CHW_EXTIx::m_pNotifier = NULL;
IValueReceiver<bool>* CHW_EXTIx::m_pValueTarget = NULL;

static CHW_EXTIx g_HW_EXTIx;

/*************************************************************************************/

/*static*/
void CHW_EXTIx::Init(bool bPullUp, bool bPullDown)
{
	CParentDINx::Init(bPullUp, bPullDown);

//	m_bPrevState = CParentDINx::GetValue();
	m_pNotifier = NULL;
	m_pValueTarget = NULL;

	SYSCFG_EXTILineConfig(CC(EXTI_PortSourceGPIO,FIRST(EXTIx_PORT)), CC(EXTI_PinSource,SECOND(EXTIx_PORT)));
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_StructInit(&EXTI_InitStructure);
	EXTI_InitStructure.EXTI_Line = CC(EXTI_Line,SECOND(EXTIx_PORT));
#ifdef EXTIx_TRIGGER
	EXTI_InitStructure.EXTI_Trigger = CC(EXTI_Trigger_,EXTIx_TRIGGER);
#endif
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
}

/*static*/
void CHW_EXTIx::Deinit()
{
	CParentDINx::Deinit();

	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_StructInit(&EXTI_InitStructure);
	EXTI_InitStructure.EXTI_Line = CC(EXTI_Line,SECOND(EXTIx_PORT));
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);
	NVIC_DisableIRQ(CC(EXTIx_ID,_IRQn));
}

/*virtual*/
void CHW_EXTIx::SetNotifier(IDigitalInNotifier* pNotifier, unsigned uiGroupPriority, unsigned uiSubPriority)
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInProvider::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority))

    m_pNotifier = pNotifier;

    NVIC_INIT(CC(EXTIx_ID,_IRQn), uiGroupPriority, uiSubPriority);
}

bool CHW_EXTIx::GetValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInProvider::GetValue());

	return CParentDINx::GetValue();
}

void CHW_EXTIx::SetValueTargetInterface(IValueReceiver<bool>* pValueTarget)
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInProvider::IValueProvider<bool>::SetValueTargetInterface(pValueTarget));

	m_pValueTarget = pValueTarget;

	NVIC_EnableIRQ(CC(EXTIx_ID,_IRQn));
}

/////////////////////////////////////////////////////////////////////////////////
//
//     IRQ
//
/////////////////////////////////////////////////////////////////////////////////


void CHW_EXTIx::OnEXTI()
{
	if (EXTI_GetITStatus(CC(EXTI_Line,SECOND(EXTIx_PORT)))) {
		EXTI_ClearITPendingBit(CC(EXTI_Line,SECOND(EXTIx_PORT)));
		if (m_pNotifier) {
#define IF_TRIGGER_Rising \
			m_pNotifier->OnRise()
#define IF_TRIGGER_Falling \
			m_pNotifier->OnFall()
#define IF_TRIGGER_Rising_Falling \
			bool bState = g_HW_EXTIx.GetValue(); \
			if (bState) m_pNotifier->OnRise(); \
			else m_pNotifier->OnFall()
			EVAL(CC(IF_TRIGGER_,EXTIx_TRIGGER));
		}
		if (m_pValueTarget) {
			bool bState = g_HW_EXTIx.GetValue(); \
			m_pValueTarget->SetValue(bState);
		}
		__DSB(); /* Data Synchronization Barrier. Ensure EXTI_LineX status cleared and isr not trigger again */ \
	}
}

REDIRECT_ISR_STATIC(CC(EXTIx_ID,_IRQHandler), CHW_EXTIx, OnEXTI);

#undef DIOx_PORT
//#undef EXTIx_PORT
//#undef EXTIx_TRIGGER
//#undef CParentDINx
//#undef CHW_EXTIx
//#undef g_HW_EXTIx
