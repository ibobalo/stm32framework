// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: hw_tim.cxx
// #define TIMx_CHx_N      1
// # define TIMx_CHx_PORT   D,5
// # define TIMx_CHxN_PORT  D,6,AF_1
// #include "hw_tim_ch.cxx"
// CHW_TIM2_CH1* getTimer2Channel1() {return &g_HW_TIM2_Ch1;};
#include "util/macros.h"
#if !defined(TIMx_CHx_N) || !defined(TIMx_N)
#error params must be defined : TIMx_CHx_N, TIMx_N
// next defines for editor hints
#define TIMx_N           1
#define TIMx_MAXCOUNTER  0xFFFF
#define TIMx_CHx_N       1
#define TIMx_CHx_PORT    A,1
#define TIMx_CHxN_PORT   A,2,AF_1
#endif
#if (TIMx_CHx_N > TIMx_CH_COUNT)
#error channel CHx not available for timer TIMx
#endif
#if defined(TIMx_CHxN_PORT) && (TIMx_CHxN_N > TIMx_CHN_COUNT)
#error complementary channel CHxN not available for timer TIMx
#endif

#include "stdafx.h"
#include "hw_tim.h"
#include "hw/hw_macros.h"
#include "util/core.h"
#include "util/utils.h"
#include "hw/debug.h"
#include <stdint.h>
#include <stddef.h>

//#define TIM_CH_DEBUG_INFO(...) DEBUG_INFO("TIM" STR(TIMx_N) "." STR(TIMx_CHx_N)":" __VA_ARGS__)
#define TIM_CH_DEBUG_INFO(...) {}

template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::Init(unsigned uiValue, bool bInvert, bool bOpenDrain, bool bFast);
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::InitAdv(unsigned uiValue, bool bInvert, bool bOpenDrain, bool bFast, bool bSetIdleStates, bool bIdleState);
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::Deinit();
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority, unsigned uiSubPriority);
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::Enable(bool bEnable /*= true*/);
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::SetOutputInvertion(bool bInvert);
template<> unsigned CHW_TIMCH<TIMx_N, TIMx_CHx_N>::GetValue();
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::SetValue(unsigned uiValue);
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::ControlInterrupt();
template<> void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::OnTIM_CC();


/*static*/
template<>
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::Init(unsigned uiValue, bool bInvert, bool bOpenDrain, bool bFast)
#if TIMx_CHN_COUNT
{
	InitAdv(uiValue, bInvert, bOpenDrain, bFast, false, false);
}
template<>
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::InitAdv(unsigned uiValue, bool bInvert, bool bOpenDrain, bool bFast, bool bSetIdleStates, bool bIdleState)
#endif
{
	m_pNotifier = NULL;

	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_Pulse = uiValue;
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = bInvert?TIM_OCPolarity_Low:TIM_OCPolarity_High;
#if TIMx_CHN_COUNT
	{
		TIM_OCInitStructure.TIM_OCIdleState = bSetIdleStates?
				(bIdleState?TIM_OCIdleState_Set:TIM_OCIdleState_Reset):
				(bInvert?TIM_OCIdleState_Reset:TIM_OCIdleState_Set);
#if defined(TIMx_CHxN_PORT)
		TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
		TIM_OCInitStructure.TIM_OCNPolarity = bInvert?TIM_OCNPolarity_Low:TIM_OCNPolarity_High;
		TIM_OCInitStructure.TIM_OCNIdleState = bSetIdleStates?
				(bIdleState?TIM_OCIdleState_Set:TIM_OCIdleState_Reset):
				(bInvert?TIM_OCNIdleState_Set:TIM_OCNIdleState_Reset);
#else
		TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
#endif // TIMx_CHxN_PORT
		if (bSetIdleStates) CHostTimer::UseCustomIdleStates();
	}
#endif // TIMx_CHN_COUNT
	CCC(TIM_OC,TIMx_CHx_N,Init)(CC(TIM,TIMx_N), &TIM_OCInitStructure);

#if defined(TIMx_CHx_PORT)
	{
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(TIMx_CHx_PORT)), ENABLE));
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,FIRST(TIMx_CHx_PORT)), ENABLE));
		GPIO_PinAFConfig(CC(GPIO,FIRST(TIMx_CHx_PORT)), CC(GPIO_PinSource,SECOND(TIMx_CHx_PORT)),
				CC(GPIO_,THIRD(TIMx_CHx_PORT,EVAL(CC(AF_TIM,TIMx_N)))));
		GPIO_ResetBits(CC(GPIO,FIRST(TIMx_CHx_PORT)), CC(GPIO_Pin_,SECOND(TIMx_CHx_PORT)));
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_StructInit(&GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(TIMx_CHx_PORT));
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = bOpenDrain?GPIO_OType_OD:GPIO_OType_PP;
		GPIO_InitStructure.GPIO_Speed = bFast?GPIO_Speed_50MHz:GPIO_Speed_2MHz;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(CC(GPIO,FIRST(TIMx_CHx_PORT)), &GPIO_InitStructure);
	}
#endif // TIMx_CHx_PORT
#if defined(TIMx_CHxN_PORT)
	{
		IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(TIMx_CHxN_PORT)), ENABLE));
		IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,FIRST(TIMx_CHxN_PORT)), ENABLE));
		GPIO_PinAFConfig(CC(GPIO,FIRST(TIMx_CHxN_PORT)), CC(GPIO_PinSource,SECOND(TIMx_CHxN_PORT)),
				CC(GPIO_,THIRD(TIMx_CHxN_PORT,EVAL(CC(AF_TIM,TIMx_N)))));
		GPIO_ResetBits(CC(GPIO,FIRST(TIMx_CHxN_PORT)), CC(GPIO_Pin_,SECOND(TIMx_CHxN_PORT)));
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_StructInit(&GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(TIMx_CHxN_PORT));
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = bOpenDrain?GPIO_OType_OD:GPIO_OType_PP;
		GPIO_InitStructure.GPIO_Speed = bFast?GPIO_Speed_50MHz:GPIO_Speed_2MHz;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(CC(GPIO,FIRST(TIMx_CHxN_PORT)), &GPIO_InitStructure);
	}
#endif // TIMx_CHxN_PORT
	UNUSED(bOpenDrain); UNUSED(bFast);
#ifdef TIMx_IT_PRIO
	CCC(TIM_OC,TIMx_CHx_N,PreloadConfig)(CC(TIM,TIMx_N), TIM_OCPreload_Enable);
	ControlInterrupt();
#endif
}

/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::Deinit()
{	/* Deinitialize TIMx GPIO */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	TIM_ITConfig(CC(TIM,TIMx_N), CC(TIM_IT_CC,TIMx_CHx_N), DISABLE);
#if defined(TIMx_CHx_PORT)
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(TIMx_CHx_PORT));  /* Deinitialize TIMx Ch Pin */
	GPIO_Init(CC(GPIO,FIRST(TIMx_CHx_PORT)), &GPIO_InitStructure);
#endif // TIMx_CHx_PORT
#if defined(TIMx_CHxN_PORT)
	GPIO_InitStructure.GPIO_Pin = CC(GPIO_Pin_,SECOND(TIMx_CHxN_PORT));  /* Deinitialize TIMx Ch Pin */
	GPIO_Init(CC(GPIO,FIRST(TIMx_CHxN_PORT)), &GPIO_InitStructure);
#endif // TIMx_CHxN_PORT
}

#ifdef TIMx_IT_PRIO
/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority, unsigned uiSubPriority)
{
	m_pNotifier = pNotifier;

	/* Initialize TIMx IRQ */
# if defined(TIMx_CC_IRQ)
	NVIC_INIT(CC(TIMx_CC_IRQ,n), uiGroupPriority, uiSubPriority);
	ASSERT_IF_NOT_IRQn_REDIRECTED(CC(TIMx_CC_IRQ,n), "define TIMxCHy_IT_PRIO in brd_" STR(BOARD) ".cpp to allow timer notifications");
# elif defined(TIMx_IRQ)
	NVIC_INIT(CC(TIMx_IRQ,n), uiGroupPriority, uiSubPriority);
	ASSERT_IF_NOT_IRQn_REDIRECTED(CC(TIMx_IRQ,n), "define TIMx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow timer notifications");
# else
	ASSERT("IRQ for TIM CC is shared");
# endif

	ControlInterrupt();
}
#endif

/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::Enable(bool bEnable /*= true*/)
{
#if defined(TIMx_CHx_PORT)
	TIM_CCxCmd(CC(TIM,TIMx_N), CC(TIM_Channel_,TIMx_CHx_N), bEnable ? TIM_CCx_Enable : TIM_CCx_Disable);
#endif // TIMx_CHx_PORT
#if defined(TIMx_CHxN_PORT)
	TIM_CCxNCmd(CC(TIM,TIMx_N), CC(TIM_Channel_,TIMx_CHx_N), bEnable ? TIM_CCxN_Enable : TIM_CCxN_Disable);
#endif // TIMx_CHxN_PORT
#if !defined(TIMx_CHx_PORT) && !defined(TIMx_CHxN_PORT)
	UNUSED(bEnable);
#endif
}

/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::SetOutputInvertion(bool bInvert)
{
	TIM_CH_DEBUG_INFO("Set Invertion:", bInvert ? 1 : 0);
	CCC(TIM_OC,TIMx_CHx_N,PolarityConfig)(CC(TIM,TIMx_N), bInvert ? TIM_OCPolarity_Low : TIM_OCPolarity_High);
#if defined(TIMx_CHxN_PORT)
	CCC(TIM_OC,TIMx_CHx_N,NPolarityConfig)(CC(TIM,TIMx_N), bInvert ? TIM_OCNPolarity_Low : TIM_OCNPolarity_High);
#endif
}

/*static*/
template< >
unsigned CHW_TIMCH<TIMx_N, TIMx_CHx_N>::GetValue()
{
	return CC(TIM_GetCapture,TIMx_CHx_N)(CC(TIM,TIMx_N));
}

/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::SetValue(unsigned uiValue)
{
	TIM_CH_DEBUG_INFO("Set Value:", uiValue);
	CC(TIM_SetCompare,TIMx_CHx_N)(CC(TIM,TIMx_N), uiValue);
//	ControlInterrupt();
}

#ifdef TIMx_IT_PRIO
/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::ControlInterrupt()
{	/* Initialize TIMxCC IT */
	if (m_pNotifier) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), CC(TIM_IT_CC,TIMx_CHx_N));
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_CCx status cleared and isr not trigger again
		TIM_ITConfig(CC(TIM,TIMx_N), CC(TIM_IT_CC,TIMx_CHx_N), ENABLE);
	} else {
		TIM_ITConfig(CC(TIM,TIMx_N), CC(TIM_IT_CC,TIMx_CHx_N), DISABLE);
	}
}

/*static*/
template< >
void CHW_TIMCH<TIMx_N, TIMx_CHx_N>::OnTIM_CC()
{
	if (m_pNotifier) {
		m_pNotifier->NotifyTimerChannel(TIMx_CHx_N);
	}
}
#endif

/****************************************************************************/

#undef TIMx_CHx_N
#undef TIMx_CHx_PORT
#undef TIMx_CHxN_PORT
