#ifndef HW_I2C_H_INCLUDED
#define HW_I2C_H_INCLUDED

#include "interfaces/i2c.h"

/*extern*/
class CHW_I2C1;
class CHW_I2C2;
class CHW_I2C3;

#define ACQUIRE_RELEASE_I2C_DECLARATION(I2CN,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	typedef CHW_I2C##I2CN CI2C_##ID##__VA_ARGS__; \
	static CI2C_##ID##__VA_ARGS__* AcquireI2C_##ID##__VA_ARGS__(unsigned uiClockSpeed); \
	static void ReleaseI2C_##ID##__VA_ARGS__(); \
	static II2C* AcquireI2CInterface_##ID##__VA_ARGS__(unsigned uiClockSpeed); \
	static void ReleaseI2CInterface_##ID##__VA_ARGS__();

#define ACQUIRE_RELEASE_I2C_DEFINITION(I2CN,CHWBRD,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	CHWBRD::CI2C_##ID##__VA_ARGS__* CHWBRD::AcquireI2C_##ID##__VA_ARGS__(unsigned uiClockSpeed) { \
		g_HW_I2C##I2CN.Init(uiClockSpeed); \
		return &g_HW_I2C##I2CN;} \
	void CHWBRD::ReleaseI2C_##ID##__VA_ARGS__() {g_HW_I2C##I2CN.Deinit();} \
	II2C* CHWBRD::AcquireI2CInterface_##ID##__VA_ARGS__(unsigned uiClockSpeed) { \
		g_HW_I2C##I2CN##_InterfaceWrapper.Init(uiClockSpeed); \
		return &g_HW_I2C##I2CN##_InterfaceWrapper;} \
	void CHWBRD::ReleaseI2CInterface_##ID##__VA_ARGS__() {g_HW_I2C##I2CN##_InterfaceWrapper.Deinit();}

#endif /* HW_I2C_H_INCLUDED */
