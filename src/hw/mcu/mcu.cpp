#include "stdafx.h"
#include "../hw.h"
#include "mcu.h"
#include "../hw_macros.h"
#include "../debug.h"


//#define IDLE_DEBUG_INFO(...) DEBUG_INFO(__VA_ARGS__)
#define IDLE_DEBUG_INFO(...) {}

volatile unsigned g_nHandlingIRQ;
volatile unsigned g_nIRQDisabled;

extern "C" void Safe_Deinit();
//extern "C" void NMI_Handler() {Default_Handler();}
//extern "C" void HardFault_Handler() {Default_Handler();}
//extern "C" void MemManage_Handler() {Default_Handler();}
//extern "C" void BusFault_Handler() {Default_Handler();}
//extern "C" void UsageFault_Handler() {Default_Handler();}
#ifdef USE_WWDG
extern "C" void WWDG_IRQHandler() {WWDG_ClearFlag(); Default_Handler();}
#endif

/*************************************************************************************/



/*************************************************************************************/

unsigned CHW_MCU::m_auiTimerPeriod[12];

void CHW_MCU::Init()
{
#ifdef DEBUG
	if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST))   { DEBUG_INFO("!! Reset by IWDG detected !!"); }
	if (RCC_GetFlagStatus(RCC_FLAG_WWDGRST))   { DEBUG_INFO("!! Reset by WWDG detected !!"); }
	if (RCC_GetFlagStatus(RCC_FLAG_PORRST))    { DEBUG_INFO("!! Reset by POR detected !!"); }
	if (RCC_GetFlagStatus(RCC_FLAG_LPWRRST))   { DEBUG_INFO("!! Reset by low power detected !!"); }
IF_STM32F0(
	if (RCC_GetFlagStatus(RCC_FLAG_V18PWRRSTF)){ DEBUG_INFO("!! Reset by 1.8V power detected !!"); }
)
IF_STM32F4(
	if (RCC_GetFlagStatus(RCC_FLAG_BORRST))    { DEBUG_INFO("!! Reset by BOR detected !!"); }
)
	RCC_ClearFlag(); // Clear RCC_CSR register (recent reset reason)
#endif

	Init_IRQs();

	CHW_Clocks::Init();
}

void CHW_MCU::Reboot()
{
	Safe_Deinit();
	NVIC_SystemReset();
}

void CHW_MCU::Init_IRQs()
{
	IF_STM32F4(NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2)); // 4 Groups, 4 sub-priorities
}

//void CHW_MCU::Init_RTC()
//{
//	//RTC
//	PWR_BackupAccessCmd(ENABLE);
//	RCC_BackupResetCmd(ENABLE);
//	RCC_BackupResetCmd(DISABLE);
//	RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div8);
//	RCC_RTCCLKCmd(ENABLE);
//	//todo RTC use
//	PWR_BackupAccessCmd(DISABLE);
//}


/////////////////////////////////////////////////////////////////////////////////
//
//    Flash
//
/////////////////////////////////////////////////////////////////////////////////
bool CHW_MCU::ErasePage(unsigned uiPageNumber)
{
	FLASH_Unlock();
	IF_STM32F0( FLASH_Status status = FLASH_ErasePage(uiPageNumber);)
	IF_STM32F4( FLASH_Status status = FLASH_EraseSector(uiPageNumber << 3, VoltageRange_3);)
	DEBUG_INFO("Flash record error:", status);
	FLASH_Lock();
	return 	(status == FLASH_COMPLETE);
}

bool CHW_MCU::Flash(void* pTgt, const void* pValue, unsigned uiSize)
{
	IF_STM32F0(ASSERTE((uiSize & 0x01) == 0);) // F0 unable to write single bytes
	FLASH_Status status = FLASH_COMPLETE;
	uint8_t* pTarget = (uint8_t*)pTgt;
	uint8_t* pSource = (uint8_t*)pValue;
	unsigned nBytesLeft = uiSize;
	FLASH_Unlock();
//  EXTERNAL VPP REQUIRED
//	IF_STM32F4(if (IS_MEMORY_ALIGNED(pTarget, 8) && IS_MEMORY_ALIGNED(pSource, 8) && status == FLASH_COMPLETE) while (nBytesLeft >= 8) {
//		status = FLASH_ProgramDoubleWord((uint32_t)pTarget, *(const uint64_t*)pSource);
//		if (status == FLASH_COMPLETE) {
//			nBytesLeft -= 8;
//			pTarget += 8;
//			pSource += 8;
//		} else {
//			break;
//		}
//	})
	if (IS_MEMORY_ALIGNED(pTarget, 4) && IS_MEMORY_ALIGNED(pSource, 4) && status == FLASH_COMPLETE) while (nBytesLeft >= 4) {
		status = FLASH_ProgramWord((uint32_t)pTarget, *(const uint32_t*)pSource);
		if (status == FLASH_COMPLETE) {
			nBytesLeft -= 4;
			pTarget += 4;
			pSource += 4;
		} else {
			break;
		}
	}
	if (IS_MEMORY_ALIGNED(pTarget, 2) && IS_MEMORY_ALIGNED(pSource, 2) && status == FLASH_COMPLETE) while (nBytesLeft >= 2) {
		status = FLASH_ProgramHalfWord((uint32_t)pTarget, *(const uint16_t*)pSource);
		if (status == FLASH_COMPLETE) {
			nBytesLeft -= 2;
			pTarget += 2;
			pSource += 2;
		} else {
			break;
		}
	}
	IF_STM32F4( if (status == FLASH_COMPLETE) while (nBytesLeft > 0) {
		status = FLASH_ProgramByte((uint32_t)pTarget, *(const uint8_t*)pSource);
		if (status == FLASH_COMPLETE) {
				nBytesLeft -= 1;
				pTarget += 1;
				pSource += 1;
			} else {
				break;
			}
		}
	)
	if (status != FLASH_COMPLETE) {
		FlashClearAllFlags();
		DEBUG_INFO("Flash record error:", status);
	}
	FLASH_Lock();
	return 	(status == FLASH_COMPLETE) && !nBytesLeft;
}
void CHW_MCU::FlashClearAllFlags()
{
	IF_STM32F0( FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR | FLASH_FLAG_EOP ) );
	IF_STM32F4( FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR | FLASH_FLAG_RDERR) );
}
