#include "stdafx.h"
#include "hw_usb.h"
#include "hw/hw.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"


#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

#include "usbd_conf.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_req.h"

USB_OTG_CORE_HANDLE           USB_OTG_dev;

#define USBD_CONST_UNICODE_STRING_DEFINITION(NAME, STRING) \
struct USBD_UniStr_##NAME##_t { \
	uint8_t  uiLength; \
	uint8_t  uiType; \
	wchar_t  wchars[sizeof(L##STRING)]; \
}; \
static const __ALIGN_BEGIN USBD_UniStr_##NAME##_t USBD_UniStr_##NAME = { \
		2 + sizeof(L##STRING) - 2,  /* header + string - finishing \0 */ \
		USB_DESC_TYPE_STRING, \
		L##STRING \
}; \
uint8_t *  USBD_USR_##NAME##StrDescriptor( uint8_t speed , uint16_t *length) \
{ \
	*length = USBD_UniStr_##NAME.uiLength; \
	return (uint8_t*)&USBD_UniStr_##NAME; \
}

#define USBD_VID                    0x0483
#define USBD_PID                    0x5740
USBD_CONST_UNICODE_STRING_DEFINITION(LangID, "\x0409");
USBD_CONST_UNICODE_STRING_DEFINITION(Manufacturer, "Generic DIY");
USBD_CONST_UNICODE_STRING_DEFINITION(Product, "STM32 Virtual ComPort in FS Mode");
USBD_CONST_UNICODE_STRING_DEFINITION(Serial, "00000000050C");
USBD_CONST_UNICODE_STRING_DEFINITION(Config, "VCP Config");
USBD_CONST_UNICODE_STRING_DEFINITION(Interface, "VCP Interface");

uint8_t *  USBD_USR_DeviceDescriptor( uint8_t speed , uint16_t *length)
{
	*length = sizeof(USBD_DeviceDesc);
	return USBD_DeviceDesc;
}

USBD_DEVICE USR_desc = {
	USBD_USR_DeviceDescriptor,
	USBD_USR_LangIDStrDescriptor,
	USBD_USR_ManufacturerStrDescriptor,
	USBD_USR_ProductStrDescriptor,
	USBD_USR_SerialStrDescriptor,
	USBD_USR_ConfigStrDescriptor,
	USBD_USR_InterfaceStrDescriptor,
};
/* USB Standard Device Descriptor */
__ALIGN_BEGIN uint8_t USBD_DeviceDesc[USB_SIZ_DEVICE_DESC] __ALIGN_END = {
	0x12,                       /*bLength */
	USB_DEVICE_DESCRIPTOR_TYPE, /*bDescriptorType*/
	0x00,                       /*bcdUSB */
	0x02,
	0x02,                       /*bDeviceClass*/
	0x01,                       /*bDeviceSubClass*/
	0x00,                       /*bDeviceProtocol*/
	USB_OTG_MAX_EP0_SIZE ,      /*bMaxPacketSize*/
	LOBYTE(USBD_VID),           /*idVendor*/
	HIBYTE(USBD_VID),           /*idVendor*/
	LOBYTE(USBD_PID),           /*idVendor*/
	HIBYTE(USBD_PID),           /*idVendor*/
	0x00,                       /*bcdDevice rel. 2.00*/
	0x02,
	USBD_IDX_MFC_STR,           /*Index of manufacturer  string*/
	USBD_IDX_PRODUCT_STR,       /*Index of product string*/
	USBD_IDX_SERIAL_STR,        /*Index of serial number string*/
	USBD_CFG_MAX_NUM            /*bNumConfigurations*/
} ; /* USB_DeviceDescriptor */
__ALIGN_BEGIN uint8_t USBD_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC] __ALIGN_END = {
	USB_LEN_DEV_QUALIFIER_DESC,
	USB_DESC_TYPE_DEVICE_QUALIFIER,
	0x00,
	0x02,
	0x00,
	0x00,
	0x00,
	0x40,
	0x01,
	0x00,
};

void USBD_USR_Init(void)
{
}
void USBD_USR_DeviceReset(uint8_t speed)
{
	switch (speed) {
	case USB_OTG_SPEED_HIGH:
		break;

	case USB_OTG_SPEED_FULL:
		break;
	default:
		break;
	}
}
void USBD_USR_DeviceConfigured(void)
{
	CHW_USB::OnUSBDeviceConfigured();
}
void USBD_USR_DeviceSuspended(void)
{
	CHW_USB::OnUSBDeviceSuspended();
}
void USBD_USR_DeviceResumed(void)
{
	CHW_USB::OnUSBDeviceResumed();
}
void USBD_USR_DeviceConnected(void)
{
	CHW_USB::OnUSBDeviceConnected();
}
void USBD_USR_DeviceDisconnected(void)
{
	CHW_USB::OnUSBDeviceDisconnected();
}
USBD_Usr_cb_TypeDef USR_cb =
{
	USBD_USR_Init,
	USBD_USR_DeviceReset,
	USBD_USR_DeviceConfigured,
	USBD_USR_DeviceSuspended,
	USBD_USR_DeviceResumed,

	USBD_USR_DeviceConnected,
	USBD_USR_DeviceDisconnected,
};
//
// This part is responsible to offer board support package and is configurable by user.
//
// USB_OTG_BSP_uDelay - This function provides delay time in micro sec
void USB_OTG_BSP_uDelay (const uint32_t usec)
{
	CHW_MCU::DelayMicro(usec);
}
// USB_OTG_BSP_mDelay - This function provides delay time in milli sec
void USB_OTG_BSP_mDelay (const uint32_t msec)
{
	CHW_MCU::Sleep(msec);
}
// USB_OTG_BSP_Init - Initializes BSP configurations
void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev)
{
#if defined(STM32F2) || defined(STM32F4)
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA , ENABLE);

   /* Configure SOF ID DM DP Pins */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8  |
                                GPIO_Pin_11 |
                                GPIO_Pin_12;

  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_PinAFConfig(GPIOA,GPIO_PinSource8,GPIO_AF_OTG1_FS) ;
  GPIO_PinAFConfig(GPIOA,GPIO_PinSource11,GPIO_AF_OTG1_FS) ;
  GPIO_PinAFConfig(GPIOA,GPIO_PinSource12,GPIO_AF_OTG1_FS) ;

  /* Configure  VBUS Pin */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Configure ID pin */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_OTG1_FS) ;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_OTG_FS, ENABLE) ;
#elif defined(STM32F1)
  RCC_OTGFSCLKConfig(RCC_OTGFSCLKSource_PLLVCO_Div3);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_OTG_FS, ENABLE) ;
#else
#  error "Unknown / unsupported CPU. One of STM32F2, STM32F4 or STM32F1 must be defined"
#endif
}
// USB_OTG_BSP_EnableInterrupt - Enable USB Global interrupt
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev)
{
	NVIC_INIT(OTG_FS_IRQn, 1, 3);
}

#ifdef __cplusplus
} // extern "C"
#endif

#define VCPx 0
#include "hw_usb_vcp.cxx" /* g_USBVCP0 */

void CHW_USB::Init()
{
	USBD_Init(
			&USB_OTG_dev,
			USB_OTG_FS_CORE_ID,
			&USR_desc,
			&USBD_CDC_cb,
			&USR_cb
	);

//	for (unsigned n = 0; n < ARRAY_SIZE(g_apUSBVCPs); ++n) {
//		g_apUSBVCPs[n]->Init(n);
//	}
}

ISerial* CHW_USB::AcquireUSBSerialInterface(unsigned uiIndex)
{
	ISerial* pResult = NULL;

	switch (uiIndex) {
	case 0:
		g_USBVCP0.Init();
		pResult = &g_USBVCP0;
	}
	return pResult;
}
void CHW_USB::OnUSBDeviceConfigured()
{
	DEBUG_INFO("USB Configured");
}
void CHW_USB::OnUSBDeviceSuspended()
{
	DEBUG_INFO("USB Suspended");
}
void CHW_USB::OnUSBDeviceResumed()
{
	DEBUG_INFO("USB Resumed");
}
void CHW_USB::OnUSBDeviceConnected()
{
	DEBUG_INFO("USB Connect");
}
void CHW_USB::OnUSBDeviceDisconnected()
{
	DEBUG_INFO("USB Disconnect");
}

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

extern USB_OTG_CORE_HANDLE           USB_OTG_dev;
extern uint32_t USBD_OTG_ISR_Handler (USB_OTG_CORE_HANDLE *pdev);
void OTG_FS_WKUP_IRQHandler(void)
{
	if (USB_OTG_dev.cfg.low_power) {
		*(uint32_t *) (0xE000ED10) &= 0xFFFFFFF9;
		SystemInit();
		USB_OTG_UngateClock(&USB_OTG_dev);
	}
	EXTI_ClearITPendingBit(EXTI_Line18);
}
static unsigned nOTG_FS_IRQHandler_Counter = 0;
void OTG_FS_IRQHandler(void)
{
//	uint32_t uiCount1 = CHW_MCU::GetSysTick();
//	uint32_t uiValue1 = SysTick->VAL;
	nOTG_FS_IRQHandler_Counter++;
	USBD_OTG_ISR_Handler (&USB_OTG_dev);
//	if (uiCount1 && uiValue1) {} // suppress warning
}

#ifdef PRINTF_TO_USB
/*
 * stdlib
 */
#include <stdlib.h>
#include <sys/unistd.h>
#include <errno.h>
//extern int errno;
int _read(int file, char *ptr, int len)
{
    int num = 0;
    switch (file)
    {
    case STDIN_FILENO:
    default:
        errno = EBADF;
        return -1;
    }
    return num;
}
int _write(int file, char *ptr, int len)
{
    switch (file)
    {
    case STDOUT_FILENO: /* stdout */
    case STDERR_FILENO: /* stderr */
    	CHW_USB::UsbDataSent(ptr, len);
        break;
    default:
        errno = EBADF;
        return -1;
    }
    return len;
}
#endif // PRINTF_TO_USB

#ifdef __cplusplus
} // extern "C"
#endif
