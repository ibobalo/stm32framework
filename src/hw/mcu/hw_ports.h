#pragma once

#include <stdint.h>
#include "hw/stm32fxxxx.h"
#include "util/macros.h"
#include <type_traits>

#define DEFINE_GPIO_PORTx(PORT) \
	class CC(GPIOPort,PORT) { public: \
	static inline void RCC_Cmd(FunctionalState NewState) { \
		IF_STM32F0(RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIO##PORT, NewState)); \
		IF_STM32F4(RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIO##PORT, NewState)); \
	} \
	static constexpr auto GPIOx = GPIO##PORT##_BASE; \
	static constexpr auto EXTI_PortSourceGPIOx = EXTI_PortSourceGPIO##PORT; \
	};

template <unsigned PINn> class GPIOPin {};

#define DEFINE_GPIO_PIN(PINn) \
	template<> class GPIOPin<PINn> { public: \
	static constexpr auto GPIO_Pin_x = GPIO_Pin_##PINn; \
	static constexpr auto EXTI_PinSourcex = EXTI_PinSource##PINn; \
	static constexpr auto EXTI_Linex = EXTI_Line##PINn; \
	static constexpr auto EXTI_IRQn = \
		IF_STM32F0( \
			(PINn >= 0 && PINn <= 1) ? EXTI0_1_IRQn : ( \
			(PINn >= 2 && PINn <= 3) ? EXTI2_3_IRQn : \
			EXTI4_15_IRQn) \
		) \
		IF_STM32F4( \
			(PINn == 0) ? EXTI0_IRQn : ( \
			(PINn == 1) ? EXTI1_IRQn : ( \
			(PINn == 2) ? EXTI2_IRQn : ( \
			(PINn == 3) ? EXTI3_IRQn : ( \
			(PINn == 4) ? EXTI4_IRQn : ( \
			(PINn >= 5 && PINn <= 9) ? EXTI9_5_IRQn : \
			EXTI15_10_IRQn))))) \
		); \
	};

MAP(DEFINE_GPIO_PORTx, A, B, C, D, E, F)
#ifdef GPIOG
DEFINE_GPIO_PORTx(G)
#endif
#ifdef GPIOH
DEFINE_GPIO_PORTx(H)
#endif
#ifdef GPIOI
DEFINE_GPIO_PORTx(I)
#endif
#ifdef GPIOJ
DEFINE_GPIO_PORTx(J)
#endif
#ifdef GPIOK
DEFINE_GPIO_PORTx(K)
#endif


REPEAT(16, DEFINE_GPIO_PIN)

template <unsigned ADCCH_N> struct AINPort {using has_pin = std::false_type;};
template <> struct AINPort<0> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<0>;};
template <> struct AINPort<1> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<1>;};
template <> struct AINPort<2> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<2>;};
template <> struct AINPort<3> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<3>;};
template <> struct AINPort<4> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<4>;};
template <> struct AINPort<5> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<5>;};
template <> struct AINPort<6> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<6>;};
template <> struct AINPort<7> {using has_pin = std::true_type; using port_type = GPIOPortA; using pin_type = GPIOPin<7>;};
template <> struct AINPort<8> {using has_pin = std::true_type; using port_type = GPIOPortB; using pin_type = GPIOPin<0>;};
template <> struct AINPort<9> {using has_pin = std::true_type; using port_type = GPIOPortB; using pin_type = GPIOPin<1>;};
template <> struct AINPort<10> {using has_pin = std::true_type; using port_type = GPIOPortC; using pin_type = GPIOPin<0>;};
template <> struct AINPort<11> {using has_pin = std::true_type; using port_type = GPIOPortC; using pin_type = GPIOPin<1>;};
template <> struct AINPort<12> {using has_pin = std::true_type; using port_type = GPIOPortC; using pin_type = GPIOPin<2>;};
template <> struct AINPort<13> {using has_pin = std::true_type; using port_type = GPIOPortC; using pin_type = GPIOPin<3>;};
template <> struct AINPort<14> {using has_pin = std::true_type; using port_type = GPIOPortC; using pin_type = GPIOPin<4>;};
template <> struct AINPort<15> {using has_pin = std::true_type; using port_type = GPIOPortC; using pin_type = GPIOPin<5>;};
