#ifndef HW_CAN_H_INCLUDED
#define HW_CAN_H_INCLUDED

#include <stdint.h>
#include "util/queue.h"

/* 14 filters per CAN for STM32f4xx */
#define MAX_CAN_FILTERS 14

class IHW_CAN;
class ICANListener;
class ICANTxDoneNotifee;
class ICANNotifierCallbacks;

class IHW_CAN
{
public:
	enum EFrameTypesFiltering {FTF_ONLY_DATA, FTF_ONLY_REMOTE, FTF_BOTH_DATA_AND_REMOTE};
	typedef int CTxMsgHandle;
public:
	virtual bool Init(unsigned uiBaudrate, bool bTxAutoRetry = true, unsigned uiSamplePercent = 70) = 0;
public: // IHW_CAN
	virtual bool QueueMessage(CanTxMsg* pTxMessage, ICANTxDoneNotifee* pDoneNotifee = NULL, CTxMsgHandle* pRetHandle = NULL) = 0;
	virtual void Abort(CTxMsgHandle msgHandle) = 0;
	virtual bool SetListener(ICANListener* pListener,
			unsigned uiId, unsigned uiMask = 0x7FF,
			EFrameTypesFiltering eFrameTypes = FTF_BOTH_DATA_AND_REMOTE) = 0;
	virtual bool SetListenerEx(ICANListener* pListener,
			unsigned uiId = 0, unsigned uitMask = 0x1FFFFFFF,
			EFrameTypesFiltering eFrameTypes = FTF_BOTH_DATA_AND_REMOTE) = 0;
	virtual void ClearListeners(ICANListener* pListener) = 0;
};

//class CCANNotifierCallbacks
//{
//public: // CCATNotifierCallbacks
//	virtual void NotifyTxDone() = 0;
//	virtual void NotifyTxTimeout(unsigned uiBytesLeft) = 0;
//	virtual void NotifyTxError(unsigned uiBytesLeft) = 0;
//public:
//	void Init(IHW_CAN* pCAN) {m_pCAN = pCAN;};
//private:
//	IHW_CAN* m_pCAN;
//};

class ICANListener
{
public: // ICANListener
	virtual bool ProcessRxedCANMessage(const CanRxMsg*) = 0; // returns true if message is consumed
};
class ICANTxDoneNotifee
{
public: // ICANTxDoneNotifee
	virtual void OnCANMessageTxDone(bool bSucceed, IHW_CAN::CTxMsgHandle msgHandle) = 0;
};

#endif /* HW_CAN_H_INCLUDED */
