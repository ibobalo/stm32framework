#ifndef HW_SPI_H_INCLUDED
#define HW_SPI_H_INCLUDED

#include "interfaces/spi.h"

/*extern*/
class CHW_SPI1;
class CHW_SPI2;
class CHW_SPI3;
class CHW_SPI4;
class CHW_SPI5;

#define MAX_SPI_DEVICES 4

#define ACQUIRE_RELEASE_SPI_DECLARATION(SPIN,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	typedef CHW_SPI##SPIN CSPI_##ID##__VA_ARGS__; \
	static CSPI_##ID##__VA_ARGS__* AcquireSPI_##ID##__VA_ARGS__(unsigned uiSpeed); \
	static void ReleaseSPI_##ID##__VA_ARGS__(); \
	static ISPI* AcquireSPIInterface_##ID##__VA_ARGS__(unsigned uiSpeed); \
	static void ReleaseSPIInterface_##ID##__VA_ARGS__();

#define ACQUIRE_RELEASE_SPI_DEFINITION(SPIN,CHWBRD,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	CHWBRD::CSPI_##ID##__VA_ARGS__* CHWBRD::AcquireSPI_##ID##__VA_ARGS__(unsigned uiSpeed) { \
		g_HW_SPI##SPIN.Init(uiSpeed); \
		return &g_HW_SPI##SPIN;} \
	void CHWBRD::ReleaseSPI_##ID##__VA_ARGS__() {g_HW_SPI##SPIN.Deinit();} \
	ISPI* CHWBRD::AcquireSPIInterface_##ID##__VA_ARGS__(unsigned uiSpeed) { \
		g_HW_SPI##SPIN##_InterfaceWrapper.Init(uiSpeed); \
		return &g_HW_SPI##SPIN##_InterfaceWrapper;} \
	void CHWBRD::ReleaseSPIInterface_##ID##__VA_ARGS__() {g_HW_SPI##SPIN##_InterfaceWrapper.Deinit();}

#endif /* HW_SPI_H_INCLUDED */
