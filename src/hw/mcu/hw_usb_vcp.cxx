#include "util/macros.h"
#include <string.h>

#ifndef VCPx
#define VCPx 0
#endif

#define CHW_USB_VCPx CC(CHW_USB_VCP,VCPx)
#define VCPx_Ctrl CCC(VCP,VCPx,_Ctrl)
#define VCPx_DataTx CCC(VCP,VCPx,_DataTx)
#define VCPx_DataRxHandler CCC(VCP,VCPx,_DataRxHandler)
#define VCPx_fops CCC(VCP,VCPx,_fops)
#define g_USBVCPx CC(g_USBVCP,VCPx)

typedef struct
{
	uint32_t bitrate;
	uint8_t  format;      /* stop bits-1*/
	uint8_t  paritytype;
	uint8_t  datatype;
} LINE_CODING;

class CHW_USB_VCPx :
		public ISerial,
		protected ISysTickNotifee
{
public:
//	virtual ~CHW_USB_VCPx();
	static void Init();
	static void Deinit();

public: // ISerial
	virtual void SetSerialRxNotifier(ISerialRxNotifier* pNotifier);
	virtual void SetSerialTxNotifier(ISerialTxNotifier* pNotifier);
	virtual void SetConnectionParams(const TSerialConnectionParams* params);
	virtual bool StartDataSend(const CConstChunk& TxChunk);
	virtual void StartDataRecv(const CIoChunk& RxChunk);
	virtual void Control(bool bEnable);
	virtual void ControlRx(bool bEnable);
	virtual void ControlTx(bool bEnable);
	virtual void SetRxByteTimeout(unsigned uiByteTimeoutTicks);
	virtual void SetTxByteTimeout(unsigned uiByteTimeoutTicks);
	virtual unsigned GetBaudrate() const;
	virtual void SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier);

protected: // ISysTickNotifee
	virtual void OnSysTick();

public:
	static void OnUSBDataReceived(const uint8_t* pData, unsigned uiLength);
	static void OnUSBDataTxDone();
	static void OnUsbVcpParamSet(unsigned uiBitrate, ISerial::StopBits eStopBits, ISerial::ParityBits eParityBits);
	static uint16_t OnUSBCtrl(uint32_t Cmd, uint8_t* Buf, uint32_t Len);

private:
	static void DoTX();
	static void DoTXTimeout(unsigned uiBytesLeft);
	static void DoRXTimeout(unsigned uiBytesLeft);

	static ISerialRxNotifier*   m_pRxNotifier;
	static ISerialTxNotifier*   m_pTxNotifier;
	static CIoChunk             m_RxChunk;
	static CConstChunk          m_TxChunk;
	static ISerialConnectionParamsNotifier* m_pBaudrateNotifier;
	static LINE_CODING          m_Linecoding;

	static bool               m_bTxEnabled;
	static bool               m_bRxEnabled;
	static unsigned           m_uiTxByteTimeoutTicks;
	static unsigned           m_uiRxByteTimeoutTicks;
	static unsigned           m_nTxTimeoutCountdown;
	static unsigned           m_nRxTimeoutCountdown;

};

ISerialRxNotifier* CHW_USB_VCPx::m_pRxNotifier;
ISerialTxNotifier* CHW_USB_VCPx::m_pTxNotifier;
CIoChunk           CHW_USB_VCPx::m_RxChunk;
CConstChunk        CHW_USB_VCPx::m_TxChunk;
ISerialConnectionParamsNotifier* CHW_USB_VCPx::m_pBaudrateNotifier;
LINE_CODING        CHW_USB_VCPx::m_Linecoding;
bool               CHW_USB_VCPx::m_bTxEnabled;
bool               CHW_USB_VCPx::m_bRxEnabled;
unsigned           CHW_USB_VCPx::m_uiTxByteTimeoutTicks;
unsigned           CHW_USB_VCPx::m_uiRxByteTimeoutTicks;
unsigned           CHW_USB_VCPx::m_nTxTimeoutCountdown;
unsigned           CHW_USB_VCPx::m_nRxTimeoutCountdown;

CHW_USB_VCPx g_USBVCPx;

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

extern uint8_t  APP_Rx_Buffer []; /* Write CDC received data in this buffer.
                                     These data will be sent over USB IN endpoint
                                     in the CDC core functions. */
extern uint32_t APP_Rx_ptr_in;    /* Increment this pointer or roll it back to
                                     start address when writing received data
                                     in the buffer APP_Rx_Buffer. */
extern uint32_t APP_Rx_ptr_out ;  /* equal to APP_Rx_ptr_in when buffer empty */

static uint16_t VCP_Init(void)
{
	return USBD_OK;
}
static uint16_t VCP_DeInit(void)
{
	return USBD_OK;
}
uint16_t VCPx_Ctrl (uint32_t Cmd, uint8_t* Buf, uint32_t Len)
{
	return g_USBVCPx.OnUSBCtrl(Cmd, Buf, Len);
}
// VCP_DataRx - Data received over USB OUT endpoint are sent over CDC interface through this function.
uint16_t VCPx_DataRxHandler (uint8_t* pData, uint32_t uiLength)
{
	g_USBVCPx.OnUSBDataReceived(pData, uiLength);
	return USBD_OK;
}
uint16_t VCPx_DataTx(void)
{
	g_USBVCPx.OnUSBDataTxDone();
	return USBD_OK;
}
CDC_IF_Prop_TypeDef VCP_fops =
{
	VCP_Init,
	VCP_DeInit,
	VCPx_Ctrl,
	VCPx_DataTx,
	VCPx_DataRxHandler
};

#ifdef __cplusplus
} // extern "C"
#endif /* __cplusplus */


//ISerialNotifier* CHW_USB::m_pSerialNotifier;

/*static*/
void CHW_USB_VCPx::Init()
{
	m_pRxNotifier = NULL;
	m_pTxNotifier = NULL;
	m_RxChunk.Assign(NULL, 0);
	m_TxChunk.Assign(NULL, 0);
	m_pBaudrateNotifier = NULL;
	m_Linecoding.bitrate = 115200;
	m_Linecoding.format = 0x00;
	m_Linecoding.paritytype = 0x00;
	m_Linecoding.datatype = 0x08;
	m_bTxEnabled = false;
	m_bRxEnabled = false;
	m_uiTxByteTimeoutTicks = 0;
	m_uiRxByteTimeoutTicks = 0;
	m_nTxTimeoutCountdown = 0;
	m_nRxTimeoutCountdown = 0;

	CHW_MCU::RegisterSysTickCallback(&g_USBVCPx);
}

/*static*/
void CHW_USB_VCPx::Deinit()
{
	CHW_MCU::UnregisterSysTickCallback(&g_USBVCPx);
}

/*virtual*/
void CHW_USB_VCPx::SetSerialRxNotifier(ISerialRxNotifier* pNotifier)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::SetSerialRxNotifier(pNotifier));
	m_pRxNotifier = pNotifier;
}

/*virtual*/
void CHW_USB_VCPx::SetSerialTxNotifier(ISerialTxNotifier* pNotifier)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::SetSerialTxNotifier(pNotifier));
	m_pTxNotifier = pNotifier;
}

/*virtual*/
void CHW_USB_VCPx::SetConnectionParams(const TSerialConnectionParams* params)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::SetConnectionParams(params));
	// do nothing
}

/*static*/
void CHW_USB_VCPx::DoTX()
{
	uint32_t APP_Rx_ptr_limit = (APP_Rx_ptr_out ? APP_Rx_ptr_out : APP_RX_DATA_SIZE) - 1;
	unsigned n = m_TxChunk.PopData(&APP_Rx_Buffer[APP_Rx_ptr_in], APP_Rx_ptr_limit - APP_Rx_ptr_in);
	if (n) {
		APP_Rx_ptr_in += n;
		if (APP_Rx_ptr_in >= APP_RX_DATA_SIZE) {
			APP_Rx_ptr_in = 0;
		}
	}

	if (m_uiTxByteTimeoutTicks) {
		m_nTxTimeoutCountdown = m_uiTxByteTimeoutTicks + 1;
	}
}

/*static*/
void CHW_USB_VCPx::DoTXTimeout(unsigned uiBytesLeft)
{
	DEBUG_INFO("timeout. left:", uiBytesLeft);
	bool bNext = (m_pTxNotifier ? m_pTxNotifier->NotifyTxTimeout(uiBytesLeft, &m_TxChunk) : false);
	if (bNext && m_RxChunk.uiLength) {
		//DoContinueTx();
	} else {
		g_USBVCPx.ControlTx(false);
	}
}

/*static*/
void CHW_USB_VCPx::DoRXTimeout(unsigned uiBytesLeft)
{
	DEBUG_INFO("timeout. left:", uiBytesLeft);
	bool bNext = (m_pRxNotifier ? m_pRxNotifier->NotifyRxTimeout(uiBytesLeft, &m_RxChunk) : false);
	if (bNext && m_RxChunk.uiLength) {
		//DoContinueRx();
	} else {
		g_USBVCPx.ControlRx(false);
	}
}

/*virtual*/
bool CHW_USB_VCPx::StartDataSend(const CConstChunk& TxChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::StartDataSend(TxChunk));
	ASSERTE(!m_TxChunk.uiLength);
	m_TxChunk = TxChunk;
	DoTX();
	ControlTx(true);
	return true;
}

/*virtual*/
void CHW_USB_VCPx::StartDataRecv(const CIoChunk& RxChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::StartDataRecv(RxChunk));
	ENTER_CRITICAL_SECTION;
	m_RxChunk = RxChunk;
	LEAVE_CRITICAL_SECTION;
	if (m_uiRxByteTimeoutTicks) {
		m_nRxTimeoutCountdown = m_uiRxByteTimeoutTicks + 1;
	}
	ControlRx(true);
}

/*virtual*/
void CHW_USB_VCPx::Control(bool bEnable)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::Control(bEnable));
}

/*virtual*/
void CHW_USB_VCPx::ControlRx(bool bEnable)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::ControlRx(bEnable));
	m_bRxEnabled = bEnable;
	if (!bEnable) m_RxChunk.Clean();
}

/*virtual*/
void CHW_USB_VCPx::ControlTx(bool bEnable)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::ControlTx(bEnable));
	m_bTxEnabled = bEnable;
	if (!bEnable) m_TxChunk.Clean();
}

/*virtual*/
void CHW_USB_VCPx::SetRxByteTimeout(unsigned uiByteTimeoutTicks)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::SetRxByteTimeout(uiByteTimeoutTicks));
	m_uiRxByteTimeoutTicks = uiByteTimeoutTicks;
	if (m_RxChunk.uiLength && m_uiRxByteTimeoutTicks) {
		// apply immediate
		m_nRxTimeoutCountdown = m_uiRxByteTimeoutTicks + 1;
	}
}

/*virtual*/
void CHW_USB_VCPx::SetTxByteTimeout(unsigned uiByteTimeoutTicks)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::SetTxByteTimeout(uiByteTimeoutTicks));
	m_uiTxByteTimeoutTicks = uiByteTimeoutTicks;
	if (m_TxChunk.uiLength && m_uiTxByteTimeoutTicks) {
		// apply immediate
		m_nTxTimeoutCountdown = m_uiTxByteTimeoutTicks + 1;
	}
}

/*virtual*/
unsigned CHW_USB_VCPx::GetBaudrate() const
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::GetBaudrate());
	return 0;
}

/*virtual*/
void CHW_USB_VCPx::SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerial::SetBaudrateNotifier(pNotifier));
	m_pBaudrateNotifier = pNotifier;
}

/*virtual*/
void CHW_USB_VCPx::OnSysTick()
{
	IMPLEMENTS_INTERFACE_METHOD(ISysTickNotifee::OnSysTick());
	unsigned uiTxBytesLeft = m_TxChunk.uiLength;
	if (m_nTxTimeoutCountdown && uiTxBytesLeft) {
		--m_nTxTimeoutCountdown;
		if (!m_nTxTimeoutCountdown) {
			DEBUG_INFO("TX timeout");
			DoTXTimeout(uiTxBytesLeft);
		}
	}
	unsigned uiRxBytesLeft = m_RxChunk.uiLength;
	if (m_nRxTimeoutCountdown && uiRxBytesLeft) {
		--m_nRxTimeoutCountdown;
		if (!m_nRxTimeoutCountdown) {
			DoRXTimeout(uiRxBytesLeft);
		}
	}
}

/*static*/
void CHW_USB_VCPx::OnUSBDataReceived(const uint8_t* pData, unsigned uiLength)
{
	if (!m_pRxNotifier) return;
	unsigned uiProcessedLength = 0;
	while (uiProcessedLength < uiLength) {
		const uint8_t* p = pData + uiProcessedLength;
		ENTER_CRITICAL_SECTION;
		if (!m_bRxEnabled) break;
		if (m_RxChunk.pData) {
			ASSERTE(m_RxChunk.uiLength);
			unsigned n = m_RxChunk.PushData(p, uiLength);
			uiProcessedLength += n;
			bool bGotNewChunk;
			if (m_RxChunk.uiLength) {
				bGotNewChunk = m_pRxNotifier->NotifyRxPart(m_RxChunk.uiLength, &m_RxChunk);
				if (bGotNewChunk) {
					ASSERTE(m_RxChunk.pData);
					ASSERTE(m_RxChunk.uiLength);
				} else {
					ASSERTE(!m_RxChunk.uiLength);
				}
			} else {
				bGotNewChunk = m_pRxNotifier->NotifyRxDone(&m_RxChunk);
				if (bGotNewChunk) {
					ASSERTE(m_RxChunk.pData);
					ASSERTE(m_RxChunk.uiLength);
				} else {
					m_RxChunk.pData = NULL;
					ASSERTE(!m_RxChunk.uiLength);
				}
			}
		} else {
			CConstChunk ccData = {pData, uiLength};
			if (m_pRxNotifier->NotifyRxOverflow(ccData, &m_RxChunk)) {
				// got new RxChunk
				ASSERTE(m_RxChunk.pData);
				ASSERTE(m_RxChunk.uiLength);
				uiProcessedLength += uiLength;
			} else {
				ASSERTE(!m_RxChunk.pData);
				ASSERTE(!m_RxChunk.uiLength);
				m_RxChunk.pData = NULL;
				uiProcessedLength += uiLength;
			}
		}
		LEAVE_CRITICAL_SECTION;
	}
	if (m_uiRxByteTimeoutTicks && m_RxChunk.uiLength) {
		m_nRxTimeoutCountdown = m_uiRxByteTimeoutTicks + 1;
	}
}

/*static*/
void CHW_USB_VCPx::OnUSBDataTxDone()
{
	if (!m_pTxNotifier) return;
	if (m_pTxNotifier->NotifyTxDone(&m_TxChunk)) {
		ASSERTE(!m_TxChunk.pData);
		ASSERTE(!m_TxChunk.uiLength);
		DoTX();
	}
	if (m_pTxNotifier->NotifyTxComplete(&m_TxChunk)) {
		ASSERTE(!m_TxChunk.pData);
		ASSERTE(!m_TxChunk.uiLength);
		DoTX();
	}
}

/*static*/
void CHW_USB_VCPx::OnUsbVcpParamSet(unsigned uiBitrate, ISerial::StopBits eStopBits, ISerial::ParityBits eParityBits)
{
	TSerialConnectionParams params = {uiBitrate, eStopBits, eParityBits};
	if (m_pBaudrateNotifier) m_pBaudrateNotifier->NotifyConnectionParams(&params);
}

/*static*/
uint16_t CHW_USB_VCPx::OnUSBCtrl(uint32_t Cmd, uint8_t* Buf, uint32_t Len)
{
	switch (Cmd) {
		case SET_LINE_CODING:
			m_Linecoding.bitrate = (uint32_t) (Buf[0] | (Buf[1] << 8) | (Buf[2] << 16)
					| (Buf[3] << 24));
			m_Linecoding.format = Buf[4];
			m_Linecoding.paritytype = Buf[5];
			m_Linecoding.datatype = Buf[6];
			const static TSerialConnectionParams::StopBits aStopBitsDecoding[] = {
					TSerialConnectionParams::stop1,
					TSerialConnectionParams::stop2,
					TSerialConnectionParams::stop0_5,
					TSerialConnectionParams::stop1_5
			};
			const static TSerialConnectionParams::ParityBits aParityBitsDecoding[] = {
					TSerialConnectionParams::parityNo,
					TSerialConnectionParams::parityEven,
					TSerialConnectionParams::parityOdd
			};
			OnUsbVcpParamSet(
					m_Linecoding.bitrate,
					(m_Linecoding.format     < ARRAY_SIZE(aStopBitsDecoding  ))?aStopBitsDecoding  [m_Linecoding.format]    :TSerialConnectionParams::stop1,
					(m_Linecoding.paritytype < ARRAY_SIZE(aParityBitsDecoding))?aParityBitsDecoding[m_Linecoding.paritytype]:TSerialConnectionParams::parityNo
			);
			break;

		case GET_LINE_CODING:
			Buf[0] = (uint8_t) (m_Linecoding.bitrate);
			Buf[1] = (uint8_t) (m_Linecoding.bitrate >> 8);
			Buf[2] = (uint8_t) (m_Linecoding.bitrate >> 16);
			Buf[3] = (uint8_t) (m_Linecoding.bitrate >> 24);
			Buf[4] = m_Linecoding.format;
			Buf[5] = m_Linecoding.paritytype;
			Buf[6] = m_Linecoding.datatype;
			break;

		case SEND_ENCAPSULATED_COMMAND:
		case GET_ENCAPSULATED_RESPONSE:
		case SET_COMM_FEATURE:
		case GET_COMM_FEATURE:
		case CLEAR_COMM_FEATURE:
		case SET_CONTROL_LINE_STATE:
		case SEND_BREAK:
			/* Not  needed for this driver */
			break;
		default:
			break;
	}

	return USBD_OK;
}

#undef CHW_USB_VCPx
#undef VCPx_Ctrl
#undef VCPx_DataTx
#undef VCPx_DataRxHandler
#undef VCPx_fops
#undef g_USBVCPx

