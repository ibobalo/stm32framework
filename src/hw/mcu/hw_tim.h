#ifndef HW_TIM_H_INCLUDED
#define HW_TIM_H_INCLUDED

#include "interfaces/timer.h"
#include "util/macros.h"
#include "hw_tim_ch.h"

/* 1 for 1 and 8, 0 for others */
#define IS_TIM_ADVANCED(TIM_N) IF(TIM_N)(IF(MACRO_DEC(TIM_N))(IF(MACRO_SUB(TIM_N,7))(IF(MACRO_SUB(TIM_N,8))(0,1),0), 1),0)
#define IF_TIM_IS_ADVANCED(TIM_N) IF(IS_TIM_ADVANCED(TIM_N))

template<class CHWTIMx>
class THWTIMxBase_InterfaceWrapper :
		public virtual ITimer
{
public:
	template <unsigned nChannel>
	static void ReleaseTimerChannelInterface();
public: // ITimer
	virtual void SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0) {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority));
		CHWTIMx::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority);
	}
	virtual void SetPeriodClks(uint64_t ullPeriodClks) {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::SetPeriodClks(ullPeriodClks));
		CHWTIMx::SetPeriodClks(ullPeriodClks);
	}
	virtual void SetPrescaler(unsigned uiPrescaler){
		IMPLEMENTS_INTERFACE_METHOD(ITimer::SetPrescaler(uiPrescaler));
		CHWTIMx::SetPrescaler(uiPrescaler);
	}
	virtual void SetPeriod(unsigned uiPeriod) {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::SetPeriod(uiPeriod));
		CHWTIMx::SetPeriod(uiPeriod);
	}
	virtual void SetCounter(unsigned uiCounter) {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::SetCounter(uiCounter));
		CHWTIMx::SetCounter(uiCounter);
	}
	virtual unsigned GetCounter() const {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::GetCounter());
		return CHWTIMx::GetCounter();
	}
	virtual unsigned GetCounterMax() const {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::GetCounterMax());
		return CHWTIMx::GetCounterMax();
	}
	virtual unsigned GetPeriod() const {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::GetPeriod());
		return CHWTIMx::GetPeriod();
	}
	virtual unsigned ClksToCounts(unsigned uiClks) const {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::ClksToCounts(uiClks));
		return CHWTIMx::ClksToCounts(uiClks);
	}
	virtual void Start() {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::Start());
		CHWTIMx::Start();
	}
	virtual void Stop() {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::Stop());
		CHWTIMx::Stop();
	}
	virtual void Idle() {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::Idle());
		CHWTIMx::Idle();
	}
	virtual void Work() {
		IMPLEMENTS_INTERFACE_METHOD(ITimer::Work());
		CHWTIMx::Work();
	}
};

template<class CHWTIMx>
class THWTIMx_InterfaceWrapper :
		public virtual THWTIMxBase_InterfaceWrapper<CHWTIMx>
{
public:
	template <int nChannel>
	static ITimerChannel* AcquireTimerChannelInterface(unsigned uiValue, bool bInverted, bool bOpenDrain, bool bFast);
};

template<class CHWTIMx>
class THWTIMxAdv_InterfaceWrapper :
		public virtual THWTIMxBase_InterfaceWrapper<CHWTIMx>,
		public virtual ITimerAdv
{
public:
	template<int nChannel>
	static ITimerChannelAdv* AcquireTimerChannelInterface(unsigned uiValue, bool bInverted, bool bOpenDrain, bool bFast, bool bSetIdleState = false, bool bIdleState = false);
public: // ITimerAdv
	virtual void SetPulsesCount(unsigned nPulsesCount) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerAdv::SetPulsesCount(nPulsesCount));
		CHWTIMx::SetPulsesCount(nPulsesCount);
	}
	virtual void SetDeadtimeClks(unsigned uiDeadtimeClks) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerAdv::SetDeadtimeClks(uiDeadtimeClks));
		CHWTIMx::SetDeadtimeClks(uiDeadtimeClks);
	}
	virtual void UseCustomIdleStates(bool bResetOnUpdate = false) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerAdv::UseCustomIdleStates(bResetOnUpdate));
		CHWTIMx::UseCustomIdleStates(bResetOnUpdate);
	}
};

template<unsigned TIM_N>
class CHW_TIM
{
public:
	typedef CHW_TIM<TIM_N> type;
	static inline type* Acquire() {
		type::Init();
		static type instance;
		return &instance;
	}
	static inline ITimer* AcquireInterface() {
		Acquire();
		static THWTIMx_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static inline ITimerAdv* AcquireInterfaceAdv() {
		Acquire();
		static THWTIMxAdv_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static inline void Release() {
		type::Deinit();
	}
public:
	static constexpr bool IsAdvanced() {return false;}
	static constexpr bool HasRepetitionCounter() {return false;}
	static constexpr bool HasComplemntary() {return false;}
	static constexpr unsigned ChannelsCount();
	static constexpr unsigned ComplementaryChannelsCount();
public:
	static void Init();
	static void Deinit();
public:
	static void SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0);
	static void SetPeriodClks(uint64_t ulPeriodClks);
	static void SetPrescaler(unsigned uiPrescaler);
	static void SetPeriod(unsigned uiPeriod);
	static void SetCounter(unsigned uiCounter);
	static unsigned GetCounter();
	static unsigned GetCounterMax();
	static unsigned GetPrescaler();
	static unsigned GetPeriodClks() {return CountsToClks(GetPeriod());}
	static unsigned GetPeriod() {return m_tBaseInitStructure.TIM_Period;}
	static unsigned ClksToCounts(unsigned uiClks) {return uiClks / m_uiPrescalerToClkTotal;}
	static unsigned CountsToClks(unsigned uiCounts) {return uiCounts * m_uiPrescalerToClkTotal;}
	static void Update();
	static void Start();
	static void Stop();
	static void Idle();
	static void Work();
public: /* for advanced timers only */
	static void SetPulsesCount(unsigned nPulsesCount);
	static void SetDeadtimeClks(unsigned uiDeadtimeClks);
	static void UseCustomIdleStates(bool bResetOnUpdate = false);
public: /* Interrupts */
	static void OnTIM();
	static void OnTIM_CC();
protected:
	static unsigned GetSourceFrequency();
private:
	static void BaseInit();
	static void AdvInit();
private:
	static TIM_TimeBaseInitTypeDef m_tBaseInitStructure;
	static unsigned           m_uiPrescalerToClkTotal;
	static unsigned           m_uiSourceDivider;
	static unsigned           m_uiDeadtimeClks;
	static bool               m_bHasIdleState;
	static bool               m_bResetIdleOnUpdate;
	static ITimerNotifier*    m_pNotifier;
};
/*static*/
template<unsigned TIM_N> TIM_TimeBaseInitTypeDef CHW_TIM<TIM_N>::m_tBaseInitStructure;
template<unsigned TIM_N> unsigned           CHW_TIM<TIM_N>::m_uiPrescalerToClkTotal;
template<unsigned TIM_N> unsigned           CHW_TIM<TIM_N>::m_uiSourceDivider;
template<unsigned TIM_N> unsigned           CHW_TIM<TIM_N>::m_uiDeadtimeClks;
template<unsigned TIM_N> bool               CHW_TIM<TIM_N>::m_bHasIdleState;
template<unsigned TIM_N> bool               CHW_TIM<TIM_N>::m_bResetIdleOnUpdate;
template<unsigned TIM_N> ITimerNotifier*    CHW_TIM<TIM_N>::m_pNotifier;


#define ACQUIRE_RELEASE_TIMER_DECLARATION(TIM_N) \
		using CTimer_##TIM_N = CHW_TIM<TIM_N> ; \
		static inline CTimer_##TIM_N* AcquireTimer_##TIM_N() { \
			return CTimer_##TIM_N::Acquire(); } \
		static inline void ReleaseTimer_##TIM_N() { \
			CTimer_##TIM_N::Release(); } \
	IF_TIM_IS_ADVANCED(TIM_N) ( \
		static inline ITimerAdv* AcquireAdvTimerInterface_##TIM_N() { \
			return CTimer_##TIM_N::AcquireInterfaceAdv(); } \
	) \
		static inline ITimer* AcquireTimerInterface_##TIM_N() { \
			return CTimer_##TIM_N::AcquireInterface(); } \
		static inline void ReleaseTimerInterface_##TIM_N() { \
			ReleaseTimer_##TIM_N(); }

#define ACQUIRE_RELEASE_TIMER_DEFINITION(CHWBRD,TIM_N) /* DEPRECATED, use CTimer_X::Acquire(...) */

#define ALIAS_TIMER(TIM_N, ALIAS) \
		using CTimer_##ALIAS = CTimer_##TIM_N;

#endif /* HW_TIM_H_INCLUDED */
