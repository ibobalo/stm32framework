#ifndef HW_CLK_H_INCLUDED
#define HW_CLK_H_INCLUDED

#include "interfaces/notify.h"
#include <stdint.h>

#define MILLI_TO_TICKS(uiMilliSeconds) (uiMilliSeconds * CHW_MCU::SysTicksPerSecond / 1000U)
#define SysTickToMs(x) (x)

#define MAXSYSTICKCALLBACKS 6

class ISysTickNotifee {
public: // ISysTickNotifee
	virtual void OnSysTick() = 0;
};

class CHW_Clocks
{
public:
	static void Init();

public: // SysTick timer
	static void RegisterSysTickCallback(ISysTickNotifee* pNotifee);
	static void UnregisterSysTickCallback(ISysTickNotifee* pNotifee);
	static bool IsHSEPresent() {return m_bHSEPresent;}
	static unsigned GetSysTick();
	static uint32_t GetCLKCounter24();
	static uint32_t GetCLKCounter32();
	static uint64_t GetCLKCounter64();
	static uint32_t GetCLKFrequency();
	static uint32_t GetHCLKFrequency();
	static uint32_t GetPCLK1Frequency();
	static uint32_t GetPCLK2Frequency();
	static uint32_t GetPCLK1TimerFrequency();
	static uint32_t GetPCLK2TimerFrequency();
	static uint32_t GetSysTickFrequency() {return SysTicksPerSecond;}
	static uint32_t GetAHBFrequency() {return GetHCLKFrequency();}
	static uint32_t GetAPB1Frequency() {return GetPCLK1Frequency();}
	static uint32_t GetAPB2Frequency() {return GetPCLK2Frequency();}
	static uint32_t GetAPB1TimerFrequency() {return GetPCLK1TimerFrequency();}
	static uint32_t GetAPB2TimerFrequency() {return GetPCLK2TimerFrequency();}
	static uint32_t GetTimBaseFrequency();
	static const uint32_t SysTicksPerSecond = 1000;
//	uint32_t ClkDiff(uint32_t uiClk24_Start, uint32_t uiClk24_End);
	static uint32_t GetClkIdleSum() {return m_uiClkIdleSum;}
	static uint32_t GetClkIdleMin() {return m_uiClkIdleMin;}
	static uint32_t GetClkIdleMax() {return m_uiClkIdleMax;}
	static uint32_t GetClkUsedSum() {return m_uiClkUsedSum;}
	static uint32_t GetClkUsedMin() {return m_uiClkUsedMin;}
	static uint32_t GetClkUsedMax() {return m_uiClkUsedMax;}
	static uint32_t GetLoops() {return m_uiLoops;}
	static void Sleep(const uint32_t uiMilliSeconds);
	static void DelayMicro(const uint32_t uiMicroSeconds);
	static void Idle();

public:
	static void EnableIWDG(unsigned uiMs=100);
	static void KickIWDG();
#ifdef USE_WWDG
	static void EnableWWDG();
	static void KickWWDG(unsigned uiMs=100);
#endif // USE_WWDG

public: // Interrupts
	static void OnSysTick();

private:
	static void Init_Clocks();
	static void Init_ClocksBase();
	static void Init_ClocksHSI();
	static void Init_ClocksHSE();
	static void Init_SysTick();
	static void Init_IWDG();
	static uint32_t Clk24Diff(uint32_t uiClk24_1, uint32_t uiClk24_2);

private:
#ifdef USE_WWDG
	static void Init_WWDG();
	static unsigned           m_uiWWDGFrequency;
#endif // USE_WWDG
	static ISysTickNotifee*   m_apSysTickNotifees[MAXSYSTICKCALLBACKS];
	static volatile unsigned  m_uiSysTicksCount;
	static uint32_t           m_uiClkPerSysTick;
	static uint32_t           m_uiClkLast;
	static uint32_t           m_uiClkIdleSum;
	static uint32_t           m_uiClkIdleMin;
	static uint32_t           m_uiClkIdleMax;
	static uint32_t           m_uiClkUsedSum;
	static uint32_t           m_uiClkUsedMin;
	static uint32_t           m_uiClkUsedMax;
	static uint32_t           m_uiLoops;
	static bool               m_bHSEPresent;
};

#endif /* HW_CLK_H_INCLUDED */
