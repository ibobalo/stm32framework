// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define TIMx_N          2
// //next defines is optional for PWM PINs or interrupts
// #define TIMx_TRIG_OUT_ENABLE /* use EN flag for TIMx.TRGO output (Syncro start) */
// #define TIMx_TRIG_OUT_UPDATE /* use update (counter overflow) event for TIMx.TRGO output (Timers chain) */
// #define TIMx_TRIG_OUT_RESET /* use update (counter overflow) event for TIMx.TRGO output (Timers chain) */
// #define TIMx_ENABLE_TRIG  T1 /* use TIM1.TRGO to enable the timer (Syncro start) */
// #define TIMx_IN_TRIG      T3 /* use TIM3.TRGO for input clock (Timers chain) */
// #define TIMx_IT_PRIO           1,1   /* priority, sub-priority */
// #define TIMx_Ch1_PORT          D,5
// #define TIMx_Ch2_PORT          D,6
// #define TIMx_Ch3_PORT          D,7
// #define TIMx_Ch4_PORT          D,8
// #include "hw_tim.cxx"
// CHW_TIM2* getTimer2() {return &g_HW_TIM2;};

#if !defined(TIMx_N)
#  error params must be defined : TIMx_N
// next defines for editor hints
#  define TIMx_N                 2
#  define TIMx_IT_PRIO           1,1
#  define TIMx_Ch1_PORT          D,5,AF_1
#  define TIMx_Ch2_PORT          D,6
#  define TIMx_Ch3_PORT          D,7
#  define TIMx_Ch4_PORT          D,8
#endif

#if defined(TIMx_TRIG_OUT_ENABLE) && defined(TIMx_TRIG_OUT_UPDATE)
#  error conflicting params TIMx_TRIG_OUT_ENABLE and TIMx_TRIG_OUT_UPDATE
#endif
#if defined(TIMx_TRIG_OUT_ENABLE) && defined(TIMx_TRIG_OUT_RESET)
#  error conflicting params TIMx_TRIG_OUT_ENABLE and TIMx_TRIG_OUT_RESET
#endif
#if defined(TIMx_TRIG_OUT_UPDATE) && defined(TIMx_TRIG_OUT_RESET)
#  error conflicting params TIMx_TRIG_OUT_UPDATE and TIMx_TRIG_OUT_RESET
#endif
#if defined(TIMx_ENABLE_TRIG) && defined(TIMx_IN_TRIG)
#  error conflicting params TIMx_ENABLE_TRIG and TIMx_IN_TRIG
#endif

#include "stdafx.h"
#include "hw_tim.h"
#include "hw/hw_macros.h"
#include "util/core.h"
#include "util/utils.h"
#include "hw/debug.h"
#include "interfaces/timer.h" // notifiers
#include <stdint.h>
#include <stddef.h>

//#define TIM_DEBUG_INFO(...) DEBUG_INFO("TIM" STR(TIMx_N) ":" __VA_ARGS__)
#define TIM_DEBUG_INFO(...) {}

#if \
	((TIMx_N==1 ) && defined( RCC_APB1Periph_TIM1  )) || \
	((TIMx_N==2 ) && defined( RCC_APB1Periph_TIM2  )) || \
	((TIMx_N==3 ) && defined( RCC_APB1Periph_TIM3  )) || \
	((TIMx_N==4 ) && defined( RCC_APB1Periph_TIM4  )) || \
	((TIMx_N==5 ) && defined( RCC_APB1Periph_TIM5  )) || \
	((TIMx_N==6 ) && defined( RCC_APB1Periph_TIM6  )) || \
	((TIMx_N==7 ) && defined( RCC_APB1Periph_TIM7  )) || \
	((TIMx_N==8 ) && defined( RCC_APB1Periph_TIM8  )) || \
	((TIMx_N==9 ) && defined( RCC_APB1Periph_TIM9  )) || \
	((TIMx_N==10) && defined( RCC_APB1Periph_TIM10 )) || \
	((TIMx_N==11) && defined( RCC_APB1Periph_TIM11 )) || \
	((TIMx_N==12) && defined( RCC_APB1Periph_TIM12 )) || \
	((TIMx_N==13) && defined( RCC_APB1Periph_TIM13 )) || \
	((TIMx_N==14) && defined( RCC_APB1Periph_TIM14 )) || \
	((TIMx_N==15) && defined( RCC_APB1Periph_TIM15 )) || \
	((TIMx_N==16) && defined( RCC_APB1Periph_TIM16 )) || \
	((TIMx_N==17) && defined( RCC_APB1Periph_TIM17 )) || \
	((TIMx_N==18) && defined( RCC_APB1Periph_TIM18 )) || \
	((TIMx_N==19) && defined( RCC_APB1Periph_TIM19 ))
#  define APBx_N 1
#elif \
	((TIMx_N==1 ) && defined( RCC_APB2Periph_TIM1  )) || \
	((TIMx_N==2 ) && defined( RCC_APB2Periph_TIM2  )) || \
	((TIMx_N==3 ) && defined( RCC_APB2Periph_TIM3  )) || \
	((TIMx_N==4 ) && defined( RCC_APB2Periph_TIM4  )) || \
	((TIMx_N==5 ) && defined( RCC_APB2Periph_TIM5  )) || \
	((TIMx_N==6 ) && defined( RCC_APB2Periph_TIM6  )) || \
	((TIMx_N==7 ) && defined( RCC_APB2Periph_TIM7  )) || \
	((TIMx_N==8 ) && defined( RCC_APB2Periph_TIM8  )) || \
	((TIMx_N==9 ) && defined( RCC_APB2Periph_TIM9  )) || \
	((TIMx_N==10) && defined( RCC_APB2Periph_TIM10 )) || \
	((TIMx_N==11) && defined( RCC_APB2Periph_TIM11 )) || \
	((TIMx_N==12) && defined( RCC_APB2Periph_TIM12 )) || \
	((TIMx_N==13) && defined( RCC_APB2Periph_TIM13 )) || \
	((TIMx_N==14) && defined( RCC_APB2Periph_TIM14 )) || \
	((TIMx_N==15) && defined( RCC_APB2Periph_TIM15 )) || \
	((TIMx_N==16) && defined( RCC_APB2Periph_TIM16 )) || \
	((TIMx_N==17) && defined( RCC_APB2Periph_TIM17 )) || \
	((TIMx_N==18) && defined( RCC_APB2Periph_TIM18 )) || \
	((TIMx_N==19) && defined( RCC_APB2Periph_TIM19 ))
#  define APBx_N 2
#else
#  error unknown APB number for TIMx_N
#endif

#if defined(STM32F0) && (TIMx_N==1)
#  define TIMx_IRQ  TIM1_BRK_UP_TRG_COM_IRQ
#  define TIMx_CC_IRQ  TIM1_CC_IRQ
#elif defined(STM32F0) && (TIMx_N==8)
#  define TIMx_IRQ  TIM8_BRK_UP_TRG_COM_IRQ
#elif defined(STM32F4) && (TIMx_N==1)
#  define TIMx_IRQ  TIM1_UP_TIM10_IRQ
#  define TIMx_CC_IRQ  TIM1_CC_IRQ
#elif defined(STM32F4) && (TIMx_N==8)
#  define TIMx_IRQ  TIM8_UP_TIM13_IRQ
#  if !defined(STM32F446xx)
#    define TIMx_CC_IRQ  TIM8_CC_IRQ
#  endif
#elif defined(STM32F4) && (TIMx_N==9)
#  define TIMx_IRQ  TIM1_BRK_TIM9_IRQ
#elif defined(STM32F4) && (TIMx_N==10)
#  define TIMx_IRQ  TIM1_UP_TIM10_IRQ
#elif defined(STM32F4) && (TIMx_N==11)
#  define TIMx_IRQ  TIM1_TRG_COM_TIM11_IRQ
#elif defined(STM32F4) && (TIMx_N==12)
#  define TIMx_IRQ  TIM8_BRK_TIM12_IRQ
#elif defined(STM32F4) && (TIMx_N==13)
#  define TIMx_IRQ  TIM8_UP_TIM13_IRQ
#elif defined(STM32F4) && (TIMx_N==14)
#  define TIMx_IRQ  TIM8_TRG_COM_TIM14_IRQ
#elif (TIMx_N==6)
#  define TIMx_IRQ  TIM6_DAC_IRQ
#else
#  define TIMx_IRQ  CCC(TIM,TIMx_N,_IRQ)
#endif

#if (defined(STM32F0) || defined(STM32F4)) && ((TIMx_N==2) || (TIMx_N==5))
#  define TIMx_MAXCOUNTER	0xFFFFFFFF
#else
#  define TIMx_MAXCOUNTER	0xFFFF
#endif

#if defined(STM32F0)
#  define TIM1_TS_T15   TIM_TS_ITR0
#  define TIM1_TS_T3    TIM_TS_ITR2
#  define TIM1_TS_T17   TIM_TS_ITR3
#  define TIM2_TS_T1    TIM_TS_ITR0
#  define TIM2_TS_T15   TIM_TS_ITR1
#  define TIM2_TS_T3    TIM_TS_ITR2
#  define TIM2_TS_T14   TIM_TS_ITR3
#  define TIM3_TS_T1    TIM_TS_ITR0
#  define TIM3_TS_T15   TIM_TS_ITR2
#  define TIM3_TS_T14   TIM_TS_ITR3
#  if !defined(STM32F0x0)
#    define TIM1_TS_T2    TIM_TS_ITR1
#    define TIM3_TS_T2    TIM_TS_ITR1
#    define TIM15_TS_T2    TIM_TS_ITR0
#    define TIM15_TS_T3    TIM_TS_ITR1
#    define TIM15_TS_T16    TIM_TS_ITR2
#    define TIM15_TS_T17    TIM_TS_ITR3
#  endif
#elif defined(STM32F4)
#  define TIM1_TS_T5    TIM_TS_ITR0
#  define TIM1_TS_T2    TIM_TS_ITR1
#  define TIM1_TS_T3    TIM_TS_ITR2
#  define TIM1_TS_T4    TIM_TS_ITR3
#  define TIM2_TS_T1    TIM_TS_ITR0
#  define TIM2_TS_T8    TIM_TS_ITR1
#  define TIM2_TS_T3    TIM_TS_ITR2
#  define TIM2_TS_T4    TIM_TS_ITR3
#  define TIM3_TS_T1    TIM_TS_ITR0
#  define TIM3_TS_T2    TIM_TS_ITR1
#  define TIM3_TS_T5    TIM_TS_ITR2
#  define TIM3_TS_T4    TIM_TS_ITR3
#  define TIM4_TS_T1    TIM_TS_ITR0
#  define TIM4_TS_T2    TIM_TS_ITR1
#  define TIM4_TS_T3    TIM_TS_ITR2
#  define TIM4_TS_T8    TIM_TS_ITR3
#  define TIM5_TS_T2    TIM_TS_ITR0
#  define TIM5_TS_T3    TIM_TS_ITR1
#  define TIM5_TS_T4    TIM_TS_ITR2
#  define TIM5_TS_T8    TIM_TS_ITR3
#  define TIM8_TS_T1    TIM_TS_ITR0
#  define TIM8_TS_T2    TIM_TS_ITR1
#  define TIM8_TS_T4    TIM_TS_ITR2
#  define TIM8_TS_T5    TIM_TS_ITR3
#  define TIM9_TS_T2    TIM_TS_ITR0
#  define TIM9_TS_T3    TIM_TS_ITR1
#  define TIM9_TS_T10   TIM_TS_ITR2
#  define TIM9_TS_T11   TIM_TS_ITR3
#  define TIM12_TS_T4   TIM_TS_ITR0
#  define TIM12_TS_T5   TIM_TS_ITR1
#  define TIM12_TS_T13  TIM_TS_ITR2
#  define TIM12_TS_T14  TIM_TS_ITR3
#endif

#define TIMx_IS_ADVANCED (TIMx_N == 1 || TIMx_N == 8)
#define TIMx_HAS_REPETITION (TIMx_N == 1 || TIMx_N == 8 || TIMx_N == 15 || TIMx_N == 16 || TIMx_N == 17)

#if TIMx_N <= 5 || TIMx_N == 8
#  define TIMx_CH_COUNT 4
#elif TIMx_N == 6 || TIMx_N == 7
#  define TIMx_CH_COUNT 0
#elif TIMx_N == 9 || TIMx_N == 12 || TIMx_N == 15
#  define TIMx_CH_COUNT 2
#elif TIMx_N == 10 || TIMx_N == 11 || TIMx_N == 13 || TIMx_N == 14 || TIMx_N == 16 || TIMx_N == 17
#  define TIMx_CH_COUNT 1
#else
#  error unknown timer channels count for TIM TIMx_N
#endif

#if TIMx_N == 1 || TIMx_N == 8
#  define TIMx_CHN_COUNT 4
#elif TIMx_N >= 15 && TIMx_N <= 17
#  define TIMx_CHN_COUNT 1
#else
#  define TIMx_CHN_COUNT 0
#endif

template<> constexpr unsigned CHW_TIM<TIMx_N>::ChannelsCount() {return TIMx_CH_COUNT;}
template<> constexpr unsigned CHW_TIM<TIMx_N>::ComplementaryChannelsCount() {return TIMx_CHN_COUNT;}
template<> void CHW_TIM<TIMx_N>::Init();
template<> void CHW_TIM<TIMx_N>::Deinit();
template<> void CHW_TIM<TIMx_N>::SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority, unsigned uiSubPriority);
template<> void CHW_TIM<TIMx_N>::SetPeriodClks(uint64_t ulPeriodClks);
template<> void CHW_TIM<TIMx_N>::SetPrescaler(unsigned uiPrescaler);
template<> void CHW_TIM<TIMx_N>::SetPeriod(unsigned uiPeriod);
template<> void CHW_TIM<TIMx_N>::SetCounter(unsigned uiCounter);
template<> unsigned CHW_TIM<TIMx_N>::GetCounter();
template<> unsigned CHW_TIM<TIMx_N>::GetCounterMax();
template<> void CHW_TIM<TIMx_N>::Update();
template<> void CHW_TIM<TIMx_N>::Start();
template<> void CHW_TIM<TIMx_N>::Stop();
template<> void CHW_TIM<TIMx_N>::Idle();
template<> void CHW_TIM<TIMx_N>::Work();
template<> void CHW_TIM<TIMx_N>::OnTIM();
template<> void CHW_TIM<TIMx_N>::OnTIM_CC();
template<> unsigned CHW_TIM<TIMx_N>::GetSourceFrequency();
template<> unsigned CHW_TIM<TIMx_N>::GetPrescaler();
template<> void CHW_TIM<TIMx_N>::BaseInit();
#if TIMx_HAS_REPETITION
template<> constexpr bool CHW_TIM<TIMx_N>::HasRepetitionCounter() {return true;}
template<> void CHW_TIM<TIMx_N>::SetPulsesCount(unsigned nPulsesCount);
#endif
#if TIMx_CHN_COUNT
template<> void CHW_TIM<TIMx_N>::UseCustomIdleStates(bool bResetOnUpdate /*= false*/);
template<> void CHW_TIM<TIMx_N>::AdvInit();
#endif
#if TIMx_IS_ADVANCED
template<> constexpr bool CHW_TIM<TIMx_N>::IsAdvanced() {return true;}
template<> void CHW_TIM<TIMx_N>::SetDeadtimeClks(unsigned uiDeadtimeClks);
#endif

#if TIMx_CH_COUNT >= 1
#  define TIMx_CHx_N      1
#  ifdef TIMx_Ch1_PORT
#    define TIMx_CHx_PORT   TIMx_Ch1_PORT
#  endif
#  ifdef TIMx_Ch1N_PORT
#    define TIMx_CHxN_PORT   TIMx_Ch1N_PORT
#  endif
#  include "hw_tim_ch.cxx"
#endif

#if TIMx_CH_COUNT >= 2
#  define TIMx_CHx_N      2
#  ifdef TIMx_Ch2_PORT
#    define TIMx_CHx_PORT   TIMx_Ch2_PORT
#  endif
#  ifdef TIMx_Ch2N_PORT
#    define TIMx_CHxN_PORT   TIMx_Ch2N_PORT
#  endif
#  include "hw_tim_ch.cxx"
#endif

#if TIMx_CH_COUNT >= 3
#  define TIMx_CHx_N      3
#  ifdef TIMx_Ch3_PORT
#    define TIMx_CHx_PORT   TIMx_Ch3_PORT
#  endif
#  ifdef TIMx_Ch3N_PORT
#    define TIMx_CHxN_PORT   TIMx_Ch3N_PORT
#  endif
#  include "hw_tim_ch.cxx"
#endif

#if TIMx_CH_COUNT >= 4
#  define TIMx_CHx_N      4
#  ifdef TIMx_Ch4_PORT
#    define TIMx_CHx_PORT   TIMx_Ch4_PORT
#  endif
#  ifdef TIMx_Ch4N_PORT
#    define TIMx_CHxN_PORT   TIMx_Ch4N_PORT
#  endif
#  include "hw_tim_ch.cxx"
#endif


/*static*/
template<>
void CHW_TIM<TIMx_N>::Init()
{
	TIM_TimeBaseStructInit(&m_tBaseInitStructure);
	m_tBaseInitStructure.TIM_Period = 1;
	m_uiSourceDivider = CHW_MCU::GetCLKFrequency() / GetSourceFrequency();
	m_uiPrescalerToClkTotal = m_uiSourceDivider * (m_tBaseInitStructure.TIM_Prescaler + 1);
#if TIMx_CHN_COUNT
	m_uiDeadtimeClks = 0;
	m_bHasIdleState = false;
	m_bResetIdleOnUpdate = false;
#endif

	/* Initialize TIMx Clock */
	CCC(RCC_APB,APBx_N,PeriphClockCmd)(CCCC(RCC_APB,APBx_N,Periph_TIM,TIMx_N), ENABLE);
	TIM_Cmd(CC(TIM,TIMx_N), DISABLE);
	TIM_ARRPreloadConfig(CC(TIM,TIMx_N), ENABLE);
	TIM_TimeBaseInit(CC(TIM,TIMx_N), &m_tBaseInitStructure);

#if defined(TIMx_TRIG_OUT_ENABLE)
	TIM_SelectOutputTrigger(CC(TIM,TIMx_N), TIM_TRGOSource_Enable);
#elif defined(TIMx_TRIG_OUT_UPDATE)
	TIM_SelectOutputTrigger(CC(TIM,TIMx_N), TIM_TRGOSource_Update);
#elif defined(TIMx_TRIG_OUT_RESET)
	TIM_SelectOutputTrigger(CC(TIM,TIMx_N), TIM_TRGOSource_Reset);
#endif

	if (IF_STM32F0(IS_TIM_LIST6_PERIPH(CC(TIM,TIMx_N))) IF_STM32F4(IS_TIM_LIST2_PERIPH(CC(TIM,TIMx_N)))) {
		TIM_InternalClockConfig(CC(TIM,TIMx_N));
#if defined(TIMx_ENABLE_TRIG) || defined(TIMx_IN_TRIG)
		TIM_SelectMasterSlaveMode(CC(TIM,TIMx_N), TIM_MasterSlaveMode_Enable);
#  if defined(TIMx_ENABLE_TRIG)
		/* Initialize TIMx syncro start */
		TIM_SelectInputTrigger(CC(TIM,TIMx_N), CCCC(TIM,TIMx_N,_TS_,TIMx_ENABLE_TRIG));
		TIM_SelectSlaveMode(CC(TIM,TIMx_N), TIM_SlaveMode_Gated);
#  elif defined(TIMx_IN_TRIG)
		/* Initialize TIMx clock input */
		TIM_SelectInputTrigger(CC(TIM,TIMx_N), CCCC(TIM,TIMx_N,_TS_,TIMx_IN_TRIG));
		TIM_SelectSlaveMode(CC(TIM,TIMx_N), TIM_SlaveMode_External1);
#  endif
		TIM_DEBUG_INFO("Slave mode:", CC(TIM,TIMx_N)->SMCR);
#endif
	}

	m_pNotifier = NULL;
	TIM_ITConfig(CC(TIM,TIMx_N), TIM_IT_Update, DISABLE);
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::Deinit()
{
	TIM_ITConfig(CC(TIM,TIMx_N), TIM_IT_Update | TIM_IT_CC1 | TIM_IT_CC2 | TIM_IT_CC3 | TIM_IT_CC4, DISABLE);

#if defined(TIMx_IRQ)
	/* Disable TIMx IRQ */
	NVIC_DisableIRQ(CC(TIMx_IRQ,n));
#endif // TIMx_IRQ

#if defined(TIMx_CC_IRQ)
	/* Disable TIMx IRQ */
	NVIC_DisableIRQ(CC(TIMx_CC_IRQ,n));
#endif // TIMx_CC_IRQ

	/* Disable TIMx Device */
	TIM_Cmd(CC(TIM,TIMx_N), DISABLE);
	TIM_DEBUG_INFO("Disabled by deinit");
	TIM_DeInit(CC(TIM,TIMx_N));

	CCC(RCC_APB,APBx_N,PeriphClockCmd)(CCCC(RCC_APB,APBx_N,Periph_TIM,TIMx_N), DISABLE);
}

#ifdef TIMx_IT_PRIO
/*static*/
template<>
void CHW_TIM<TIMx_N>::SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority, unsigned uiSubPriority)
{
	m_pNotifier = pNotifier;

	if (pNotifier) {
		/* block notify call from update config */
		TIM_UpdateRequestConfig(CC(TIM,TIMx_N), TIM_UpdateSource_Regular);
		/* Initialize TIMx IRQ */
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_Update);
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_Update status cleared before enable interrupt
		TIM_ITConfig(CC(TIM,TIMx_N), TIM_IT_Update, DISABLE);

		NVIC_INIT(CC(TIMx_IRQ,n), uiGroupPriority ? uiGroupPriority : FIRST(TIMx_IT_PRIO), uiSubPriority ? uiSubPriority : SECOND(TIMx_IT_PRIO));

		ASSERT_IF_NOT_IRQn_REDIRECTED(CC(TIMx_IRQ,n), "define TIMx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow timer notifications");
	} else {
		TIM_ITConfig(CC(TIM,TIMx_N), TIM_IT_Update, DISABLE);
		NVIC_DisableIRQ(CC(TIMx_IRQ,n));
	}
}
#endif // TIMx_IRQ

/*static*/
template<>
void CHW_TIM<TIMx_N>::SetPeriodClks(uint64_t ulPeriodClks)
{
	ASSERTE(ulPeriodClks > 0);
	unsigned uiPrescaler = ulPeriodClks / TIMx_MAXCOUNTER / m_uiSourceDivider + 1;
	SetPrescaler(uiPrescaler);
	unsigned uiPeriod = ClksToCounts(ulPeriodClks);
	SetPeriod(uiPeriod);
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::SetPrescaler(unsigned uiPrescaler) {
	ASSERTE(uiPrescaler >= 1);
	if (m_tBaseInitStructure.TIM_Prescaler != uiPrescaler - 1) {
		m_tBaseInitStructure.TIM_Prescaler = uiPrescaler - 1;
		m_uiPrescalerToClkTotal = m_uiSourceDivider * uiPrescaler;
		TIM_PrescalerConfig(CC(TIM,TIMx_N), uiPrescaler - 1, TIM_PSCReloadMode_Update);
		TIM_DEBUG_INFO("set prescaler:", uiPrescaler);
		TIM_DEBUG_INFO("set prescaler total:", m_uiPrescalerToClkTotal);
	}
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::SetPeriod(unsigned uiPeriod)
{
	ASSERTE(uiPeriod > 0);
	ASSERTE(uiPeriod <= TIMx_MAXCOUNTER);
	if (m_tBaseInitStructure.TIM_Period != uiPeriod) {
		m_tBaseInitStructure.TIM_Period = uiPeriod;
		TIM_SetAutoreload(CC(TIM,TIMx_N), uiPeriod - 1);
		TIM_DEBUG_INFO("set period:", uiPeriod);
	}
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::SetCounter(unsigned uiCounter)
{
	TIM_SetCounter(CC(TIM,TIMx_N), uiCounter);
	if (!uiCounter) {
		TIM_GenerateEvent(CC(TIM,TIMx_N), TIM_EventSource_Update);
	}
	TIM_DEBUG_INFO("set counter:", uiCounter);
}

/*static*/
template<>
unsigned CHW_TIM<TIMx_N>::GetCounter()
{
	return TIM_GetCounter(CC(TIM,TIMx_N));
}

/*static*/
template<>
unsigned CHW_TIM<TIMx_N>::GetCounterMax()
{
	return TIMx_MAXCOUNTER;
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::Update()
{
	TIM_GenerateEvent(CC(TIM,TIMx_N), TIM_EventSource_Update);

	TIM_DEBUG_INFO("Update");
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::Start()
{
#ifdef TIMx_IT_PRIO
	if (m_pNotifier) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_Update);
		TIM_ITConfig(CC(TIM,TIMx_N), TIM_IT_Update, ENABLE);
	}
#endif // TIMx_IT_PRIO
	TIM_Cmd(CC(TIM,TIMx_N), ENABLE);
#if TIMx_CHN_COUNT
	TIM_CtrlPWMOutputs(CC(TIM,TIMx_N), ENABLE);
#endif

	TIM_DEBUG_INFO("START");
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::Stop()
{
	TIM_Cmd(CC(TIM,TIMx_N), DISABLE);
	TIM_ITConfig(CC(TIM,TIMx_N), TIM_IT_Update, DISABLE);
	TIM_DEBUG_INFO("STOP");
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::Idle()
{
	TIM_CtrlPWMOutputs(CC(TIM,TIMx_N), DISABLE);
	TIM_DEBUG_INFO("IDLE");
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::Work()
{
	TIM_CtrlPWMOutputs(CC(TIM,TIMx_N), ENABLE);
	TIM_DEBUG_INFO("WORK");
}

#ifdef TIMx_IT_PRIO
/*static*/
template<>
void CHW_TIM<TIMx_N>::OnTIM()
{
	TIM_DEBUG_INFO("IRQ SR:",CC(TIM,TIMx_N)->SR);

	if (TIM_GetITStatus(CC(TIM,TIMx_N), TIM_IT_Update)) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_Update);
		if (m_pNotifier) { m_pNotifier->NotifyTimer(); }
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_Update status cleared and isr not trigger again
	}

	//	TIM_DEBUG_INFO("IRQ done CNT:", CC(TIM,TIMx_N)->CNT);

# if !defined(TIMx_CC_IRQ)
	OnTIM_CC();
# endif
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::OnTIM_CC()
{
	TIM_DEBUG_INFO("IRQ CC SR:",CC(TIM,TIMx_N)->SR);

# if TIMx_CH_COUNT >= 1
	if (TIM_GetITStatus(CC(TIM,TIMx_N), TIM_IT_CC1)) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_CC1);
		CHW_TIMCH<TIMx_N, 1>::OnTIM_CC();
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_CCx status cleared and isr not trigger again
	}
# endif
# if TIMx_CH_COUNT >= 2
	if (TIM_GetITStatus(CC(TIM,TIMx_N), TIM_IT_CC2)) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_CC2);
		CHW_TIMCH<TIMx_N, 2>::OnTIM_CC();
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_CCx status cleared and isr not trigger again
	}
# endif
# if TIMx_CH_COUNT >= 3
	if (TIM_GetITStatus(CC(TIM,TIMx_N), TIM_IT_CC3)) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_CC3);
		CHW_TIMCH<TIMx_N, 3>::OnTIM_CC();
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_CCx status cleared and isr not trigger again
	}
# endif
# if TIMx_CH_COUNT >= 4
	if (TIM_GetITStatus(CC(TIM,TIMx_N), TIM_IT_CC4)) {
		TIM_ClearITPendingBit(CC(TIM,TIMx_N), TIM_IT_CC4);
		CHW_TIMCH<TIMx_N, 4>::OnTIM_CC();
		__DSB(); // Data Synchronization Barrier. Ensure TIM_IT_CCx status cleared and isr not trigger again
	}
# endif

//	TIM_DEBUG_INFO("IRQ CC done CNT:", CC(TIM,TIMx_N)->CNT);
}

#endif // TIMx_IT_PRIO

/*static*/
template<>
unsigned CHW_TIM<TIMx_N>::GetSourceFrequency()
{
	return CHW_Clocks::CCC(GetAPB,APBx_N,TimerFrequency)();
}

/*static*/
template<>
unsigned CHW_TIM<TIMx_N>::GetPrescaler()
{
	return TIM_GetPrescaler(CC(TIM,TIMx_N)) + 1;
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::BaseInit()
{
	TIM_TimeBaseInit(CC(TIM,TIMx_N), &m_tBaseInitStructure);
}

#if TIMx_HAS_REPETITION
/*static*/
template<>
void CHW_TIM<TIMx_N>::SetPulsesCount(unsigned nPulsesCount)
{
	if (nPulsesCount) {
		if (m_tBaseInitStructure.TIM_RepetitionCounter != nPulsesCount - 1) {
			m_tBaseInitStructure.TIM_RepetitionCounter = nPulsesCount - 1;
			TIM_SelectOnePulseMode(CC(TIM,TIMx_N), TIM_OPMode_Single);
			BaseInit();
			TIM_DEBUG_INFO("set repetition:", nPulsesCount);
		}
	} else {
		TIM_SelectOnePulseMode(CC(TIM,TIMx_N), TIM_OPMode_Repetitive);
	}
}
#endif

#if TIMx_CHN_COUNT
template<> constexpr bool CHW_TIM<TIMx_N>::HasComplemntary() {return true;}

/*static*/
template<>
void CHW_TIM<TIMx_N>::SetDeadtimeClks(unsigned uiDeadtimeClks)
{
	if (uiDeadtimeClks != m_uiDeadtimeClks) {
		m_uiDeadtimeClks = uiDeadtimeClks;
		AdvInit();
		TIM_DEBUG_INFO("set deatime clks:", uiDeadtimeClks);
	}
}
#endif

#if TIMx_CHN_COUNT
/*static*/
template<>
void CHW_TIM<TIMx_N>::UseCustomIdleStates(bool bResetOnUpdate /*= false*/)
{
	if (!m_bHasIdleState) {
		m_bHasIdleState = true;
		m_bResetIdleOnUpdate = bResetOnUpdate;
		AdvInit();
		TIM_DEBUG_INFO("set has idle state");
	}
}

/*static*/
template<>
void CHW_TIM<TIMx_N>::AdvInit()
{
	unsigned uiDeadtime = 0;

	unsigned uiDeadtimeSrc = m_uiDeadtimeClks / m_uiSourceDivider;
	if (uiDeadtimeSrc <= 0x7F) { uiDeadtime = uiDeadtimeSrc; }
	else if (uiDeadtimeSrc / 2 <= 64 + 0x3F) {uiDeadtime = 0x80 + uiDeadtimeSrc / 2 - 64;}
	else if (uiDeadtimeSrc / 8 <= 32 + 0x1F) {uiDeadtime = 0xC0 + uiDeadtimeSrc / 8 - 32;}
	else if (uiDeadtimeSrc / 16 <= 32 + 0x1F) {uiDeadtime = 0xE0 + uiDeadtimeSrc / 16 - 32;}
	else {uiDeadtime = 0xFF;}

	ASSERTE(uiDeadtime <= 0xFF);

	TIM_BDTRInitTypeDef TIM_BDTRInitStruct;
	TIM_BDTRStructInit(&TIM_BDTRInitStruct);
	TIM_BDTRInitStruct.TIM_DeadTime = uiDeadtime;
	TIM_BDTRInitStruct.TIM_OSSIState = m_bHasIdleState?TIM_OSSIState_Enable:TIM_OSSIState_Disable;
	TIM_BDTRInitStruct.TIM_AutomaticOutput = m_bResetIdleOnUpdate ? TIM_AutomaticOutput_Enable : TIM_AutomaticOutput_Disable;
	TIM_BDTRConfig(CC(TIM,TIMx_N), &TIM_BDTRInitStruct);
}
#endif


#ifdef TIMx_IT_PRIO
# if defined(TIMx_IRQ)
REDIRECT_ISRID(TIMx_IRQ, CHW_TIM<TIMx_N>::OnTIM);
# endif // TIMx_IRQ

# if defined(TIMx_CC_IRQ)
REDIRECT_ISRID(TIMx_CC_IRQ, CHW_TIM<TIMx_N>::OnTIM_CC);
# endif // TIMx_CC_IRQ
#endif

#undef TIMx_N
#undef TIMx_Ch1_PORT
#undef TIMx_Ch1N_PORT
#undef TIMx_Ch2_PORT
#undef TIMx_Ch2N_PORT
#undef TIMx_Ch3_PORT
#undef TIMx_Ch3N_PORT
#undef TIMx_Ch4_PORT
#undef TIMx_Ch4N_PORT
#undef TIMx_CH_COUNT
#undef TIMx_CHN_COUNT
#undef TIMx_IS_ADVANCED
#undef TIMx_HAS_REPETITION
#undef APBx_N
#undef TIMx_IRQ
#undef TIMx_CC_IRQ
#undef TIMx_MAXCOUNTER
#undef TIMx_TRIG_OUT_ENABLE
#undef TIMx_TRIG_OUT_UPDATE
#undef TIMx_TRIG_OUT_RESET
#undef TIMx_IN_TRIG
#undef TIMx_ENABLE_TRIG
#undef TIMx_IT_PRIO
