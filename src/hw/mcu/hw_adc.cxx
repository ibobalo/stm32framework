// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define ADCx_N            1
// #define ADCx_EXT_TRIG     T3_CC1 /* optional */
// #define ADCx_IT_PRIO      3,0
// #define ADCx_DMA          1,1
// #include "hw_adc.cxx"

#include "stdafx.h"
#include "hw_adc.h"
#include "hw/hw_macros.h"
#include "util/utils.h"
#include "hw/debug.h"
#include <string.h>

#if !defined(ADCx_N)
#error params must be defined : ADCx_N
//next defines for editor hints
#define ADCx_N                 1
#define ADCx_EXT_TRIG          T3
#define ADCx_IT_PRIO           3,0
#define ADCx_DMA               1,1
#endif

#define ADCx                  CCC(A,DC,ADCx_N)
#define ADCx_CLK              CCC(RCC_APB2,Periph_ADC,ADCx_N)

#if defined(STM32F0)

#if (ADCx_N != 1)
#  error invalid ADCx_N
#endif
#ifdef ADCx_DMA
#define ADCx_DMA_CLK          CC(RCC_AHBPeriph_DMA,FIRST(ADCx_DMA))
#define ADCx_DMA_STREAM       CCCC(DMA,FIRST(ADCx_DMA),_Channel,SECOND(ADCx_DMA))
#define ADCx_DMA_IRQn         CCCCC(DMA,FIRST(ADCx_DMA),_Channel,SECOND(ADCx_DMA),_IRQn)
#define ADCx_DMA_IRQID        CCCCC(DMA,FIRST(ADCx_DMA),_Channel,SECOND(ADCx_DMA),_IRQ)
#define ADCx_DMA_IT_TCIF      CCCC(DMA,FIRST(ADCx_DMA),_IT_TC,SECOND(ADCx_DMA))
#define ADCx_DMA_FLAG_TCIF    CCCC(DMA,FIRST(ADCx_DMA),_FLAG_TC,SECOND(ADCx_DMA))
#endif // ADCx_DMA
#define ADCx_IRQn             ADC1_IRQn
#define ADCx_IRQID            ADC1_IRQ

#elif defined(STM32F4)

#if (ADCx_N > 3)
#  error invalid ADCx_N
#endif
#define ADCx_DMA_CLK          CC(RCC_AHB1Periph_DMA,FIRST(ADCx_DMA))
#define ADCx_DMA_STREAM       CCCC(DMA,FIRST(ADCx_DMA),_Stream,SECOND(ADCx_DMA))
#define ADCx_DMA_IRQn         CCCCC(DMA,FIRST(ADCx_DMA),_Stream,SECOND(ADCx_DMA),_IRQn)
#define ADCx_DMA_IRQID        CCCCC(DMA,FIRST(ADCx_DMA),_Stream,SECOND(ADCx_DMA),_IRQ)
#define ADCx_DMA_IT_TCIF      ADCx_DMA_STREAM, CC(DMA_IT_TCIF,SECOND(ADCx_DMA))
#define ADCx_DMA_FLAG_TCIF    ADCx_DMA_STREAM, CC(DMA_FLAG_TCIF,SECOND(ADCx_DMA))
#define ADCx_IRQn             ADC_IRQn
#define ADCx_IRQID            ADC_IRQ

#endif

#if defined(ADCx_DMA) && !defined(ADCx_DMA_IT_PRIO)
# if defined(ADCx_IT_PRIO)
#  define ADCx_DMA_IT_PRIO      ADCx_IT_PRIO
# else
#  define ADCx_DMA_IT_PRIO      0,0
#  pragma message ("ADCx_DMA requires ADCx_DMA_IT_PRIO, forced 0,0")
# endif
#endif

//#define ADC_DEBUG_INFO(...) DEBUG_INFO("ADC" STR(ADCx_N) ":" __VA_ARGS__)
#define ADC_DEBUG_INFO(...) {}

//static uint8_t g_anADCChannelsToADCMapping[ADC_CHANNELS_COUNT];
//static uint8_t g_anADCChannelsToIndexMapping[ADC_CHANNELS_COUNT];
#define NOT_MAPPED 0xFF
//static unsigned g_nADCTempChannel = 0;

template<> void CHW_ADC<ADCx_N>::Init(bool bContinuous);
template<> void CHW_ADC<ADCx_N>::Deinit();
template<> void CHW_ADC<ADCx_N>::Init_ADC();
template<> void CHW_ADC<ADCx_N>::Init_Channels();
template<> template<uint8_t ChIndex, uint32_t ADC_Channel_x> void CHW_ADC<ADCx_N>::Init_Channel(uint8_t nScanRank);
template<> void CHW_ADC<ADCx_N>::Flush_ADC();
template<> void CHW_ADC<ADCx_N>::Start_ADC();
template<> void CHW_ADC<ADCx_N>::Stop_ADC();
template<> template<uint8_t ChIndex, uint32_t ADC_Channel_x> void CHW_ADC<ADCx_N>::Deinit_Channel();
template<> void CHW_ADC<ADCx_N>::Deinit_Channels();
template<> void CHW_ADC<ADCx_N>::Deinit_ADC();
template<> void CHW_ADC<ADCx_N>::Init_DMA();
template<> void CHW_ADC<ADCx_N>::Start_DMA();
template<> void CHW_ADC<ADCx_N>::Stop_DMA();
template<> void CHW_ADC<ADCx_N>::Deinit_DMA();
template<> void CHW_ADC<ADCx_N>::SetSamplesBuffer(volatile value_type* pBuffer, unsigned nMaxSamplesCount);
template<> void CHW_ADC<ADCx_N>::NotifyError();
template<> void CHW_ADC<ADCx_N>::NotifySample();
template<> void CHW_ADC<ADCx_N>::NotifySequence();
template<> void CHW_ADC<ADCx_N>::NotifyBuffer();
template<> void CHW_ADC<ADCx_N>::NotifyChannels();
template<> void CHW_ADC<ADCx_N>::NotifyChannelsMultiple();

/*static*/
template<>
void CHW_ADC<ADCx_N>::Init(bool bContinuous)
{
	m_bContinuous = bContinuous;

	SetSamplesBuffer(NULL, 0); //m_auiInternalDataBuffer

	Init_ADC();
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::Deinit()
{
	ASSERTE(!m_bStarted);

	ASSERTE(m_nScanInputsCount == 0);

	Deinit_ADC();
}


//#define CASE_CH(N)  case N: DetachAIN<N>(); break;
//	switch(nIndex) {
//		REPEAT1(16, CASE_CH);
//	default:
//		ASSERTE(false);
//	}
//#undef CASE_CH

template<>
void CHW_ADC<ADCx_N>::Init_ADC()
{
	Deinit_ADC();
	// ADC
	RCC_APB2PeriphClockCmd(ADCx_CLK, ENABLE);
#ifdef STM32F0
	ADC_ClockModeConfig(ADCx, ADC_ClockMode_SynClkDiv2);
//	ADC_ClockModeConfig(ADCx, ADC_ClockMode_SynClkDiv4);
#endif
#ifdef STM32F4
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	ADC_CommonStructInit(&ADC_CommonInitStructure);
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
    ADC_CommonInit(&ADC_CommonInitStructure);
#endif
}

template<>
void CHW_ADC<ADCx_N>::Init_Channels()
{
	ADC_InitTypeDef ADC_InitStructure;
	ADC_StructInit(&ADC_InitStructure);
#ifdef ADCx_EXT_TRIG
	ADC_InitStructure.ADC_ExternalTrigConv = CCC(ADC_ExternalTrigConv_,ADCx_EXT_TRIG,_TRGO);
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
#else
    ADC_InitStructure.ADC_ContinuousConvMode = m_bContinuous ? ENABLE : DISABLE;
#endif
#ifdef STM32F4
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_NbrOfConversion = m_nScanInputsCount;
#endif
    ADC_Init(ADCx, &ADC_InitStructure);

//	ADC_ClockModeConfig(ADCx, ADC_ClockMode_SynClkDiv4);
#if defined(ADCx_IT_PRIO)
	NVIC_INIT(ADCx_IRQn, FIRST(ADCx_IT_PRIO), SECOND(ADCx_IT_PRIO,0));
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_IRQn, "define ADCx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow ADC notifications");
#endif

#if defined(STM32F0)
	ASSERTE(ADC_GetFlagStatus(ADCx, ADC_FLAG_ADEN) == 0);
	ADC_GetCalibrationFactor(ADCx);
	//ERRATA: After the ADCAL is cleared, wait for a minimum of four ADC clock cycles before setting the ADEN bit.
	for (unsigned volatile n = 0; n < 100; ++n) {};
# ifdef ADCx_EXT_TRIG
	ADC_ContinuousModeCmd(ADCx, DISABLE);
	ADC_DMARequestModeConfig(ADCx, ADC_DMAMode_Circular);
# else // !ADCx_EXT_TRIG
	ADC_ContinuousModeCmd(ADCx, m_bContinuous ? ENABLE : DISABLE);
	ADC_DMARequestModeConfig(ADCx, ADC_DMAMode_Circular);
# endif
#endif
#if defined(STM32F4)
	ADC_ContinuousModeCmd(ADCx, m_bContinuous ? ENABLE : DISABLE);
	ADC_DMARequestAfterLastTransferCmd(ADCx, ENABLE);
#endif

	ADC_DEBUG_INFO("Init freq:", CHW_Clocks::GetAPB2Frequency()/4);

    unsigned nScanRank = 0;
#define INIT_SCAN(N) \
	if (m_abScanEnabled[N]) { \
		++nScanRank; \
		Init_Channel<N, ADC_Channel_##N>(nScanRank); \
		ADC_DEBUG_INFO("Channel " STR(N) " configured. N:", nScanRank); \
	}

	REPEAT(ADC_CHANNELS_COUNT, INIT_SCAN);
#undef INIT_SCAN
}

template<>
template<uint8_t ChIndex, uint32_t ADC_Channel_x>
void CHW_ADC<ADCx_N>::Init_Channel(uint8_t nScanRank)
{
	STATIC_ASSERTE(ChIndex >= 0 && ChIndex < ADC_CHANNELS_COUNT, ChIndex_outofrange);
	ASSERTE(nScanRank > 0 && nScanRank <= ADC_CHANNELS_USED);

	IF_STM32F0(ADC_ChannelConfig(ADCx, ADC_Channel_x, CHW_AIN<ChIndex>::GetSampleTime());)
	IF_STM32F4(ADC_RegularChannelConfig(ADCx, ADC_Channel_x, nScanRank, CHW_AIN<ChIndex>::GetSampleTime());)

	CHW_AIN<ChIndex>::SetScanRank(nScanRank);
	CHW_AIN<ChIndex>::SetAdcDataPtr(m_pauiADCSamplesBuff + nScanRank - 1, m_nScanInputsCount, m_nChannelSamplesCount);
	CHW_AIN<ChIndex>::Connect();
}

template<>
void CHW_ADC<ADCx_N>::Flush_ADC()
{
	ADC_ClearFlag(ADCx, ADC_FLAG_OVR);
	ADC_GetConversionValue(ADCx); // clear EOC flag
}

template<>
void CHW_ADC<ADCx_N>::Start_ADC()
{
#ifdef ADCx_IT_PRIO
# if defined(STM32F0) && defined(ADCx_DMA)
	if (m_pSampleNotifier) {
		ADC_ITConfig(ADCx, ADC_IT_EOC, ENABLE);
		ADC_DEBUG_INFO("IT EOC enabled");
	}
	if (m_pSequenceNotifier) {
		ADC_ITConfig(ADCx, ADC_IT_EOSEQ, ENABLE);
		ADC_DEBUG_INFO("IT EOSEQ enabled");
	}
# elif defined(STM32F0) && !defined(ADCx_DMA)
	ADC_ITConfig(ADCx, ADC_IT_EOC, ENABLE);
	if (m_pSequenceNotifier) {
		ADC_ITConfig(ADCx, ADC_IT_EOSEQ, ENABLE);
		ADC_DEBUG_INFO("IT both EOC and EOSEQ enabled");
	}
# elif defined(STM32F4) && defined(ADCx_DMA)
	if (m_pSampleNotifier || m_pSequenceNotifier) {
		ADC_EOCOnEachRegularChannelCmd(ADCx, m_pSampleNotifier ? DISABLE : ENABLE); // EOC once per group
		ADC_ITConfig(ADCx, ADC_IT_EOC, ENABLE); // group sequence if the EOCS = 0
		ADC_DEBUG_INFO("IT EOC as EOSEQ enabled");
	}
# elif defined(STM32F4) && !defined(ADCx_DMA)
	ADC_EOCOnEachRegularChannelCmd(ADCx, ENABLE); // EOC for each channel
	ADC_ITConfig(ADCx, ADC_IT_EOC, ENABLE);
	ADC_DEBUG_INFO("IT EOC enabled");
# endif
	if (m_pErrorNotifier) {
		ADC_ITConfig(ADCx, ADC_IT_OVR, ENABLE);
		ADC_DEBUG_INFO("IT OVR enabled");
	}
#endif // ADCx_IT_PRIO

#ifdef ADCx_DMA
	ADC_DMACmd(ADCx, ENABLE);
	ADC_DEBUG_INFO("DMA enabled");
#endif

#if defined(STM32F0)
	unsigned n = 0;
	// ERRATA: Keep setting the ADEN bit until the ADRDY flag goes high
	ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADEN));
	while (++n < 10000 && !ADC_GetFlagStatus(ADCx, ADC_FLAG_ADRDY)) {
//		Software is allowed to set ADEN only when all bits of ADC_CR registers are 0
//		(ADCAL=0, ADSTP=0, ADSTART=0, ADDIS=0 and ADEN=0)
		ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADCAL));
		ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADSTP));
		ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADSTART));
		ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADDIS));
		ADC_Cmd(ADCx, ENABLE);
		while(!ADC_GetFlagStatus(ADCx,ADC_FLAG_ADEN) && ++n < 10000);
	}
	ADC_DEBUG_INFO("ADC enabled, n:", n);
# ifndef ADCx_EXT_TRIG
//	Software is allowed to set ADSTART only when ADEN=1 and ADDIS=0 (ADC is enabled and
//	there is no pending request to disable the ADC)
	ASSERTE(ADC_GetFlagStatus(ADCx, ADC_FLAG_ADEN));
	ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADDIS));
# endif
	ADC_StartOfConversion(ADCx);
	ADC_DEBUG_INFO("Convertion started");
#endif
#if defined(STM32F4)
	ADC_Cmd(ADCx, ENABLE);
# ifndef ADCx_EXT_TRIG
    ADC_SoftwareStartConv(ADCx);
# endif
#endif
}

template<>
void CHW_ADC<ADCx_N>::Stop_ADC()
{
#if defined(STM32F0)
	unsigned n = 0;
//	Software is allowed to set ADSTP only when ADSTART=1 and ADDIS=0 (ADC is enabled and
//	may be converting and there is no pending request to disable the ADC)
	if (ADC_GetFlagStatus(ADCx, ADC_FLAG_ADSTART)) {
		ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADDIS));
		ADC_StopOfConversion(ADCx);
		while(ADC_GetFlagStatus(ADCx,ADC_FLAG_ADSTP) && ++n < 10000);
	}

//	Software is allowed to set ADDIS only when ADEN=1 and ADSTART=0 (which ensures that no
//	conversion is ongoing)
	ASSERTE(ADC_GetFlagStatus(ADCx, ADC_FLAG_ADEN));
	ASSERTE(!ADC_GetFlagStatus(ADCx, ADC_FLAG_ADSTART));
	ADC_Cmd(ADCx, DISABLE);
	while(ADC_GetFlagStatus(ADCx,ADC_FLAG_ADDIS) && ++n < 10000);

//	Software is allowed to set ADEN only when all bits of ADC_CR registers are 0 (ADCAL=0,
//	ADSTP=0, ADSTART=0, ADDIS=0 and ADEN=0)
	do { ADC_ClearFlag(ADCx, ADC_FLAG_ADRDY); } while(ADC_GetFlagStatus(ADCx,ADC_FLAG_ADRDY) && ++n < 10000);
//	do { ADC_ClearFlag(ADCx, ADC_FLAG_EOSMP); } while(ADC_GetFlagStatus(ADCx,ADC_FLAG_EOSMP) && ++n < 10000);
//	do { ADC_ClearFlag(ADCx, ADC_FLAG_EOSEQ); } while(ADC_GetFlagStatus(ADCx,ADC_FLAG_EOSEQ) && ++n < 10000);
#endif
#if defined(STM32F4)
	ADC_Cmd(ADCx, DISABLE);
#endif

	ADC_DMACmd(ADCx, DISABLE);

	ADC_ITConfig(ADCx, ADC_IT_OVR, DISABLE);
	ADC_ITConfig(ADCx, ADC_IT_EOC, DISABLE);
	IF_STM32F0(ADC_ITConfig(ADCx, ADC_IT_EOSEQ, DISABLE));
	ADC_DEBUG_INFO("IT EOC disabled");
}

template<>
template<uint8_t ChIndex, uint32_t ADC_Channel_x>
void CHW_ADC<ADCx_N>::Deinit_Channel()
{
	IF_STM32F0(ADCx->CHSELR &= ~(uint32_t)ADC_Channel_x;); // no function in stdperiph lib (reverse ADC_ChannelConfig)
	CHW_AIN<ChIndex>::Disconnect();
}

template<>
void CHW_ADC<ADCx_N>::Deinit_Channels()
{
#define DEINIT_SCAN(N) \
	if (m_abScanEnabled[N]) { \
		Deinit_Channel<N, ADC_Channel_##N>(); \
	}

	REPEAT(ADC_CHANNELS_COUNT, DEINIT_SCAN);
}

template<>
void CHW_ADC<ADCx_N>::Deinit_ADC()
{
	ADC_DeInit(IF_STM32F0(ADCx));

	RCC_APB2PeriphClockCmd(ADCx_CLK, DISABLE);
}

#ifdef ADCx_DMA
template<>
void CHW_ADC<ADCx_N>::Init_DMA()
{
	Deinit_DMA();

	IF_STM32F0(RCC_AHBPeriphClockCmd(ADCx_DMA_CLK, ENABLE));
	IF_STM32F0(SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_ADC1, (FIRST(ADCx_DMA)==2)?ENABLE:DISABLE));
	IF_STM32F4(RCC_AHB1PeriphClockCmd(ADCx_DMA_CLK, ENABLE));

	DMA_InitTypeDef DMA_InitStructure;
	DMA_StructInit(&DMA_InitStructure);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(ADCx->DR);
	IF_STM32F0(DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)m_pauiADCSamplesBuff);
	IF_STM32F4(DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)m_pauiADCSamplesBuff);
	IF_STM32F0(DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC);
	IF_STM32F4(DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory);
	DMA_InitStructure.DMA_BufferSize = m_bContinuous ? m_nADCSamplesToRead : m_nScanInputsCount;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_Init(ADCx_DMA_STREAM, &DMA_InitStructure);

	NVIC_INIT(ADCx_DMA_IRQn, FIRST(ADCx_DMA_IT_PRIO), SECOND(ADCx_DMA_IT_PRIO,0));
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_DMA_IRQn, "define ADCx_DMA_IT_PRIO and ADCx_DMA in brd_" STR(BOARD) ".cpp to allow ADC notifications");

	ADC_DEBUG_INFO("DMA Init done. N:", m_bContinuous ? m_nADCSamplesToRead : m_nScanInputsCount);
}

template<>
void CHW_ADC<ADCx_N>::Start_DMA()
{
	IF_STM32F0(ASSERTE((ADCx_DMA_STREAM->CCR & DMA_CCR_EN) == 0, "channel must be disabled"));
	IF_STM32F4(ASSERTE((ADCx_DMA_STREAM->CR & DMA_SxCR_EN) == 0, "channel must be disabled"));
	//DMA_SetCurrDataCounter can only be used when the DMAy_Channelx is disabled

	if (!m_bContinuous) {
		DMA_SetCurrDataCounter(ADCx_DMA_STREAM, m_nScanInputsCount);
		ADC_DEBUG_INFO("DMA n:", m_nScanInputsCount);
	}
	if (m_pBufferNotifier) {
		DMA_ITConfig(ADCx_DMA_STREAM, DMA_IT_TC, ENABLE);
		ADC_DEBUG_INFO("IT DMA enabled");
	}
	DMA_Cmd(ADCx_DMA_STREAM, ENABLE);
	ADC_DEBUG_INFO("DMA enabled");
}

template<>
void CHW_ADC<ADCx_N>::Stop_DMA()
{
	DMA_ITConfig(ADCx_DMA_STREAM, DMA_IT_TC, DISABLE);
	DMA_Cmd(ADCx_DMA_STREAM, DISABLE);
	ADC_DEBUG_INFO("DMA disabled");
}

template<>
void CHW_ADC<ADCx_N>::Deinit_DMA()
{
	DMA_DeInit(ADCx_DMA_STREAM); //Set DMA registers to default values
}
#endif // ADCx_DMA

/*static*/
template<>
void CHW_ADC<ADCx_N>::SetSampleNotifier(IADCSampleNotifier* pNotifier)
{
	m_pSampleNotifier = pNotifier;
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_IRQn, "define ADCx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow ADC notifications");
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::SetSequenceNotifier(IADCSequenceNotifier* pNotifier)
{
	m_pSequenceNotifier = pNotifier;
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_IRQn, "define ADCx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow ADC notifications");
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::SetBufferNotifier(IADCBufferNotifier* pNotifier)
{
	m_pBufferNotifier = pNotifier;
#ifdef ADCx_DMA
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_DMA_IRQn, "define ADCx_DMA_IT_PRIO and ADCx_DMA in brd_" STR(BOARD) ".cpp to allow ADC notifications");
#else
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_IRQn, "define ADCx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow ADC notifications");
#endif
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::SetErrorNotifier(IADCErrorNotifier* pNotifier)
{
	m_pErrorNotifier = pNotifier;
	ASSERT_IF_NOT_IRQn_REDIRECTED(ADCx_IRQn, "define ADCx_IT_PRIO in brd_" STR(BOARD) ".cpp to allow ADC notifications");
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::InitAINs()
{
	ASSERTE(!m_bChannelsReady);
#ifdef USE_FULL_ASSERT
	unsigned nScanInputsCount = 0;
	for (unsigned n = 0; n < ARRAY_SIZE(m_abScanEnabled); ++n) if (m_abScanEnabled[n]) ++nScanInputsCount;
	ASSERTE(m_nScanInputsCount == nScanInputsCount);
#endif
	ASSERTE(m_nScanInputsCount);
	ASSERTE(m_nScanInputsCount <= ADC_CHANNELS_USED);
	m_nChannelSamplesCount = m_bContinuous ? (m_nADCSamplesBuffSize / m_nScanInputsCount) : 1;
	m_nADCSamplesToRead = m_nChannelSamplesCount * m_nScanInputsCount;

	ASSERTE(m_nChannelSamplesCount > 0);

	Init_Channels();
#ifdef ADCx_DMA
	Init_DMA();
#endif

	m_bChannelsReady = true;

	ADC_DEBUG_INFO("Prepared N:", m_nScanInputsCount);
	ADC_DEBUG_INFO("Samples per ch:", m_nChannelSamplesCount);
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::Start()
{
	ASSERTE(m_bChannelsReady);
	ASSERTE(!m_bStarted);

	m_bStarted = true;
	m_nDataRead = 0;

	Flush_ADC();
#ifdef ADCx_DMA
	Start_DMA();
#endif
	Start_ADC();

	ADC_DEBUG_INFO("Started N:", m_bContinuous ? m_nADCSamplesToRead : m_nScanInputsCount);
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::Stop()
{
	ASSERTE(m_bChannelsReady);
	ASSERTE(m_bStarted);

	ADC_DEBUG_INFO("Stoping");

	Stop_ADC();
#ifdef ADCx_DMA
	Stop_DMA();
#endif

	m_bStarted = false;

}

template<>
void CHW_ADC<ADCx_N>::DeinitAINs()
{
	/*static*/
	ASSERTE(m_bChannelsReady);
	ASSERTE(m_nScanInputsCount);

	m_bChannelsReady = false;

	Deinit_Channels();

	m_nChannelSamplesCount = 0;
	m_nADCSamplesToRead = 0;
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::SetSamplesBuffer(volatile value_type* pBuffer, unsigned nMaxSamplesCount)
{
	ASSERTE(!m_bChannelsReady);

	if (pBuffer) {
		m_pauiADCSamplesBuff = pBuffer;
		m_nADCSamplesBuffSize = nMaxSamplesCount;
	} else {
		m_pauiADCSamplesBuff = &(m_auiInternalDataBuffer[0]);
		m_nADCSamplesBuffSize = ARRAY_SIZE(m_auiInternalDataBuffer);
	}
}

/*static*/
template<>
unsigned CHW_ADC<ADCx_N>::GetSamplesCount()
{
#ifdef ADCx_DMA
	return (m_bContinuous ? m_nADCSamplesToRead : m_nScanInputsCount) - DMA_GetCurrDataCounter(ADCx_DMA_STREAM);
#else
	return m_nDataRead;
#endif
}

/*static*/
template<>
volatile const CHW_ADC<ADCx_N>::value_type* CHW_ADC<ADCx_N>::GetSamples()
{
	return m_pauiADCSamplesBuff;
}

/*static*/
template<>
CHW_ADC<ADCx_N>::value_type CHW_ADC<ADCx_N>::GetSample(unsigned nChannelIndex)
{
	ASSERTE(nChannelIndex < m_nADCSamplesToRead);
	return m_pauiADCSamplesBuff[nChannelIndex];
}

/*static*/
template<>
CHW_ADC<ADCx_N>::value_type CHW_ADC<ADCx_N>::GetRecentSample()
{
	ASSERTE(m_nScanInputsCount == 1);
#if defined(ADCx_DMA) || defined(ADCx_IT_PRIO)
	auto n = GetSamplesCount();
	return m_pauiADCSamplesBuff[n > 0 ? n - 1 : 0];
#else
	return ADCx->DR;
#endif
}

#ifdef ADCx_IT_PRIO
/*static*/
template<>
void CHW_ADC<ADCx_N>::DoReadSample(uint16_t uiValue)
{
	if (m_nDataRead >= m_nADCSamplesToRead) { m_nDataRead = 0; }
	ADC_DEBUG_INFO("Sample Read nn00xxxx:", uiValue + (m_nDataRead << 24));
	m_pauiADCSamplesBuff[m_nDataRead] = uiValue;
	++m_nDataRead;
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::OnADC()
{
	ADC_DEBUG_INFO("ADC IRQ");
	if (ADC_GetITStatus(ADCx,ADC_IT_OVR)) {
		ADC_ClearITPendingBit(ADCx, ADC_IT_OVR);
		NotifyError();
		ASSERT("ADC Overrun");
	}
#if defined(STM32F0) && defined(ADCx_DMA)
	// EOC bit auto cleared by DMA data transfers
	if (ADC_GetITStatus(ADCx,ADC_IT_EOSEQ)) {
		ADC_ClearITPendingBit(ADCx, ADC_IT_EOSEQ);
		NotifySequence();
	}
#elif defined(STM32F0) && !defined(ADCx_DMA)
	if (ADC_GetITStatus(ADCx,ADC_IT_EOC)) {
		ADC_ClearITPendingBit(ADCx, ADC_IT_EOC);
		DoReadSample(ADC_GetConversionValue(ADCx));
		NotifySample();
	}
	if (ADC_GetITStatus(ADCx,ADC_IT_EOSEQ)) {
		ADC_ClearITPendingBit(ADCx, ADC_IT_EOSEQ);
		NotifySequence();
		if (m_nDataRead == m_nADCSamplesToRead) NotifyBuffer();
	}
#elif defined(STM32F4) && defined(ADCx_DMA)
	// EOC bit auto cleared by DMA data transfers
//	if (ADC_GetITStatus(ADCx,ADC_IT_EOC)) {
//		ADC_ClearITPendingBit(ADCx, ADC_IT_EOC);
		if (m_pSampleNotifier) {
			// EOC for each channel by ADC_EOCOnEachRegularChannelCmd
			NotifySample();
		} else {
			// EOC once per group by ADC_EOCOnEachRegularChannelCmd
			NotifySequence();
		}
//	}
#elif defined(STM32F4) && !defined(ADCx_DMA)
	// EOC for each channel by ADC_EOCOnEachRegularChannelCmd
	if (ADC_GetITStatus(ADCx,ADC_IT_EOC)) {
		ADC_ClearITPendingBit(ADCx, ADC_IT_EOC);
		DoReadSample(ADC_GetConversionValue(ADCx));
		NotifySample();
		if (m_nDataRead % m_nScanInputsCount == 0) NotifySequence();
		if (m_nDataRead == m_nADCSamplesToRead) NotifyBuffer();
	}
#endif

	__DSB(); // Data Synchronization Barrier. Ensure ADC_IT_* statuses cleared and isr not trigger again
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::NotifyError()
{
	if (m_pErrorNotifier) {
		m_pErrorNotifier->NotifyADCError();
	}
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::NotifySample()
{
	if (m_pSampleNotifier) {
		m_pSampleNotifier->NotifyADCSample();
	}
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::NotifySequence()
{
	if (m_pSequenceNotifier) {
		m_pSequenceNotifier->NotifyADCSequence();
	}
	NotifyChannels();
}
#endif // ADCx_IT_PRIO

/*static*/
template<>
void CHW_ADC<ADCx_N>::NotifyBuffer()
{
	if (m_pBufferNotifier) {
		m_pBufferNotifier->NotifyADCBuffer();
	}
	NotifyChannelsMultiple();
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::NotifyChannels()
{
#define NOTIFY_CHANNELS(N) \
	if (m_abScanEnabled[N]) { CHW_AIN<N>::NotifySingleValue(); }

	REPEAT(ADC_CHANNELS_COUNT, NOTIFY_CHANNELS);
#undef NOTIFY_CHANNELS
}

/*static*/
template<>
void CHW_ADC<ADCx_N>::NotifyChannelsMultiple()
{
#define NOTIFY_CHANNELS(N) \
	if (m_abScanEnabled[N]) { CHW_AIN<N>::NotifyMultipleValues(m_nChannelSamplesCount); }

	REPEAT(ADC_CHANNELS_COUNT, NOTIFY_CHANNELS);
#undef NOTIFY_CHANNELS
}

#ifdef ADCx_DMA
/*static*/
template<>
void CHW_ADC<ADCx_N>::OnDMA()
{
	ADC_DEBUG_INFO("DMA IRQ");
	if (DMA_GetITStatus(ADCx_DMA_IT_TCIF)) {
		DMA_ClearITPendingBit(ADCx_DMA_IT_TCIF);

		if (m_pBufferNotifier) m_pBufferNotifier->NotifyADCBuffer();
		NotifyChannelsMultiple();
	}
	__DSB(); // Data Synchronization Barrier. Ensure DMA_IT_* statuses cleared and isr not trigger again
}
#endif // ADCx_DMA

//int CHW_ADCs::GetCPUTemp()
//{
//	ASSERTE(g_nADCTempChannel) // ADC not configured for temperature;
//
//	AverageADCValue(g_nADCTempChannel);
//	unsigned uiTempSense = g_uiADCValues[g_nADCTempChannel];
//#if defined STM32F0
//  /* Temperature sensor calibration value address */
//# define VDD_TEMP_CAL_mV 3300
//# define TEMP30_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7B8))
//# define TEMP110_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7C2))
//	static const int iAvgTempSlope80 = (*TEMP110_CAL_ADDR - *TEMP30_CAL_ADDR);
//	int t = uiTempSense;
//	if (VDD_TEMP_CAL_mV != AVDD_mV){
//		t = t * AVDD_mV / VDD_TEMP_CAL_mV;
//	}
//	t -= *TEMP30_CAL_ADDR;
//	return t * 800 / iAvgTempSlope80 + 300;
//#elif defined(STM32F4)
//# define VDD_TEMP_CAL_mV 3300
//# define TEMP25_CAL_uV 760000
//# define AVG_TEMP_SLOPE_uV_dC 2500
//	unsigned uiTempVoltage_uV = uiTempSense * (AVDD_mV * 1000) / 4095; //12 bit
//	int iDeltaV_uV = uiTempVoltage_uV - TEMP25_CAL_uV;
//	return iDeltaV_uV * 10 / AVG_TEMP_SLOPE_uV_dC + 250;
//#endif
//}

/////////////////////////////////////////////////////////////////////////////////
//
//     IRQ   ISR (interrupts handling)
//
/////////////////////////////////////////////////////////////////////////////////
#if defined(ADCx_IT_PRIO)
// ADC - single conversion done
REDIRECT_ISRID(ADCx_IRQID, CHW_ADC<ADCx_N>::OnADC);
#endif // ADCx_IT_PRIO
#if defined(ADCx_DMA) && defined(ADCx_DMA_IT_PRIO)
// ADC DMA - All data buffer converted
REDIRECT_ISRID(ADCx_DMA_IRQID, CHW_ADC<ADCx_N>::OnDMA);
#endif // ADCx_DMA

#undef ADCx_N
#undef ADCx_DMA
#undef ADCx_EXT_TRIG
#undef ADCx_IT_PRIO
#undef ADCx_DMA_IT_PRIO
