#ifndef HW_UART_H_INCLUDED
#define HW_UART_H_INCLUDED

#include "interfaces/serial.h"
#include <stdint.h>

template<class CHWDSerialx>
class THWSerialx_InterfaceWrapper :
		public virtual ISerial
{
public:
	static void Init() { CHWDSerialx::Init();}
	static void Deinit() { CHWDSerialx::Deinit();}
public: // ISerial
	virtual void SetSerialRxNotifier(ISerialRxNotifier* pNotifier) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::SetSerialRxNotifier(pNotifier)); CHWDSerialx::SetSerialRxNotifier(pNotifier);}
	virtual void SetSerialTxNotifier(ISerialTxNotifier* pNotifier) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::SetSerialTxNotifier(pNotifier)); CHWDSerialx::SetSerialTxNotifier(pNotifier);}
	virtual void SetConnectionParams(const TSerialConnectionParams* params) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::SetConnectionParams(params)); CHWDSerialx::SetConnectionParams(params);}
	virtual bool StartDataSend(const CConstChunk& TxChunk) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::StartDataSend(TxChunk)); return CHWDSerialx::StartDataSend(TxChunk);}
	virtual void StartDataRecv(const CIoChunk& RxChunk) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::StartDataRecv(RxChunk)); CHWDSerialx::StartDataRecv(RxChunk);}
	virtual void Control(bool bEnable) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::Control(bEnable)); CHWDSerialx::Control(bEnable);}
	virtual void ControlRx(bool bEnable) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::ControlRx(bEnable)); CHWDSerialx::ControlRx(bEnable);}
	virtual void ControlTx(bool bEnable) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::ControlTx(bEnable)); CHWDSerialx::ControlTx(bEnable);}
	virtual void SetRxByteTimeout(unsigned uiByteTimeoutTicks) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::SetRxByteTimeout(uiByteTimeoutTicks)); CHWDSerialx::SetRxByteTimeout(uiByteTimeoutTicks);}
	virtual void SetTxByteTimeout(unsigned uiByteTimeoutTicks) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::SetTxByteTimeout(uiByteTimeoutTicks)); CHWDSerialx::SetTxByteTimeout(uiByteTimeoutTicks);}
	virtual unsigned GetBaudrate() const  override {IMPLEMENTS_INTERFACE_METHOD(ISerial::GetBaudrate()); return CHWDSerialx::GetBaudrate();}
	virtual void SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier) override {IMPLEMENTS_INTERFACE_METHOD(ISerial::SetBaudrateNotifier(pNotifier)); CHWDSerialx::SetBaudrateNotifier(pNotifier);}
};


template<unsigned USART_N>
class CHW_USARTx :
		virtual public ISysTickNotifee
{
public:
	typedef CHW_USARTx<USART_N> type;
public:
	static type* Acquire() {
		type::Init();
		static type instance;
		return &instance;
	}
	static ISerial* AcquireInterface() {
		Acquire();
		static THWSerialx_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static void Release() {
		type::Deinit();
	}
public:
	static void SetSerialRxNotifier(ISerialRxNotifier* pNotifier);
	static void SetSerialTxNotifier(ISerialTxNotifier* pNotifier);
	static void SetConnectionParams(const TSerialConnectionParams* pParams);
	static bool StartDataSend(const CConstChunk& TxChunk);
	static void StartDataRecv(const CIoChunk& RxChunk);
	static void Control(bool bEnable);
	static void ControlRx(bool bEnable);
	static void ControlTx(bool bEnable);
	static void SetRxByteTimeout(unsigned uiByteTimeoutTicks);
	static void SetTxByteTimeout(unsigned uiByteTimeoutTicks);
	static unsigned GetBaudrate();
	static void SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier);

public: // ISysTickNotifee
	virtual void OnSysTick();

public:
	static void Init();
	static void Deinit();

	static bool IsSending() {return m_TxChunk.uiLength > 0;};
	static bool IsReceiving() {return m_RxChunk.uiLength > 0;};

public: // Interrupts
	static void OnUSART();
	static void OnDMATX();
	static void OnDMARX();

protected:
	static void Init_USARTx();
	static void Init_USARTx_IRQ();
	static void Init_USARTx_DMA();
	static void Deinit_USARTx();
	static void Cancel_USARTx();

	static void DoRXByte(uint8_t uiByteReceived);
	static void DoRXPart(unsigned uiBytesLeft);
	static void DoRXDone();
	static void DoRXIdle(unsigned uiBytesLeft);
	static void DoRXError(unsigned uiBytesLeft);
	static void DoRXTimeout(unsigned uiBytesLeft);
	static void DoTXByte();
	static void DoTXDone();
	static void DoTXComplete();
	static void DoTXError(unsigned uiBytesLeft);
	static void DoTXTimeout(unsigned uiBytesLeft);
	static void DoStartRx();
	static void DoStartTx();
	static void DoContinueRx();
	static void DoContinueTx();
	static void DoPrepareTXDMA();
	static void DoPrepareRXDMA();

	static void DoBaudrate(bool bReEnable);

private: // 485
	static void SwitchToTx();
	static void SwitchToRx();

private:
	static bool               m_bEnabled;
	static TSerialConnectionParams  m_tConnectionParams;

	static unsigned           m_uiTxByteTimeoutTicks;
	static unsigned           m_uiRxByteTimeoutTicks;

	static unsigned           m_uiAddress;
	static CConstChunk        m_TxChunk;
//	static bool               m_bTxDma;
	static bool               m_bTxDmaInited;

	static CIoChunk           m_RxChunk;
	static bool               m_bRxDma;
	static bool               m_bRxDmaInited;

	static bool               m_bAutoBaudrate;
	static unsigned           m_nAutoBaudrateIndex;
	static unsigned           m_nRxErrors;
	static unsigned           m_nRxErrorsTreshold;

	static unsigned           m_nTxTimeoutCountdown;
	static unsigned           m_nRxTimeoutCountdown;

	static ISerialRxNotifier*    m_pRxNotifier;
	static ISerialTxNotifier*    m_pTxNotifier;
	static ISerialConnectionParamsNotifier* m_pParamsNotifier;

private: // 485
	typedef enum {FSMSTATE_TX, FSMSTATE_RX} RS485FsmState;
	static RS485FsmState      m_eFSMState;
};


#define ACQUIRE_RELEASE_SERIAL_DECLARATION(N,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	using CSerial_##ID##__VA_ARGS__ = CHW_USARTx<N>; \
	static inline CSerial_##ID##__VA_ARGS__* AcquireSerial_##ID##__VA_ARGS__() {\
		return CSerial_##ID##__VA_ARGS__::Acquire(); } \
	static inline void ReleaseSerial_##ID##__VA_ARGS__() {CSerial_##ID##__VA_ARGS__::Release();} \
	static inline ISerial* AcquireSerialInterface_##ID##__VA_ARGS__() { \
		return CSerial_##ID##__VA_ARGS__::AcquireInterface(); } \
	static inline void ReleaseSerialInterface_##ID##__VA_ARGS__() { CSerial_##ID##__VA_ARGS__::Release(); }

#define ALIAS_SERIAL(ID,ALIAS) \
	using CSerial_##ALIAS = CSerial_##ID;

#define ACQUIRE_RELEASE_SERIAL_DEFINITION(CHWBRD,ID,...)

#endif /* HW_UART_H_INCLUDED */
