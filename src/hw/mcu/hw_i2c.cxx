#ifdef STM32F0
#include "hw_i2c_f0.cxx"
#else //STM32F4
// THIS IS MACRO FILE. DO NOT COMPILE IT DIRECTLY, BUT INCLUDE IN OTHER SOURCE
// EXAMPLE:
// //file: board.cpp
// #define I2Cx_N                     1
// #define I2Cx_SCL_GPIO_PORT         B,6,AF_1
// #define I2Cx_SDA_GPIO_PORT         B,7,AF_1
// #define I2Cx_IT_GROUP_PRIO         3
// //#define I2Cx_SLAVE               1
// //next defines is optional for DMA access
// #define I2Cx_DMA_TX                1,6,1
// #define I2Cx_DMA_RX                1,0,1
// #include "hw_i2c.cxx"
// II2C* getI2CInterface() {return &g_HW_I2C1;};

#if !defined(I2Cx_N) || \
	!defined(I2Cx_SCL_GPIO_PORT) || \
	!defined(I2Cx_SDA_GPIO_PORT) || \
	!defined(I2Cx_IT_GROUP_PRIO)
# error params must be defined : I2Cx_N, I2Cx_SCL_GPIO_PORT, I2Cx_SDA_GPIO_PORT, I2Cx_IT_GROUP_PRIO
// next defines for editor hints
# define I2Cx_N                     1
# define I2Cx_SCL_GPIO_PORT         B,6,AF_1
# define I2Cx_SDA_GPIO_PORT         B,7,AF_1
# define I2Cx_IT_GROUP_PRIO         3
# define I2Cx_DMA_TX                1,6,1
# define I2Cx_DMA_RX                1,0,1
#endif

#if defined(I2Cx_DMA_N) || \
	defined(I2Cx_DMA_TX_STREAM_N) || \
	defined(I2Cx_DMA_TX_CHANNEL_N) || \
	defined(I2Cx_DMA_RX_STREAM_N) || \
	defined(I2Cx_DMA_RX_CHANNEL_N)
# error I2Cx_DMA_N, I2Cx_DMA_TX_STREAM_N, I2Cx_DMA_TX_CHANNEL_N, I2Cx_DMA_RX_STREAM_N, I2Cx_DMA_RX_CHANNEL_N is obsolete.
# pragma message("Hint: use")
# pragma message("#define I2Cx_DMA_TX   DMA,STREAM,CHANNEL")
# pragma message("#define I2Cx_DMA_RX   DMA,STREAM,CHANNEL")
#endif

#if defined(I2Cx_DMA_TX) && CHECK_EMPTY(THIRD(I2Cx_DMA_TX))
# error invalid I2Cx_DMA_TX defined
# pragma message("Hint: valid format is comma separated 3 numbers for DMA controller, stream, channel")
# pragma message("#define I2Cx_DMA_TX   1,2,3")
#endif
#if defined(I2Cx_DMA_RX) && CHECK_EMPTY(THIRD(I2Cx_DMA_RX))
# error invalid I2Cx_DMA_RX defined
# pragma message("Hint: valid format is comma separated 3 numbers for DMA controller, stream, channel")
# pragma message("#define I2Cx_DMA_TX   1,2,3")
#endif

#include "hw_i2c.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include "util/utils.h"
#include "util/queue.h"
#include "util/callback.h"
#include <stddef.h>
#include <stdint.h>

// configuration
#define I2C_TIMEOUT_START          10
#define I2C_TIMEOUT_ADDRESS        2
#define I2C_TIMEOUT_TX(nBytes)     (nBytes * 32)
#define I2C_TIMEOUT_RX(nBytes)     (nBytes * 32)
#define I2C_TIMEOUT_STOP           20
#define I2C_TIMEOUT_BUS_RECOVER    200
#define I2C_BUS_RECOVER_VALIDATE   3
#define MinTXSizeForDmaUse         5
#define MinRXSizeForDmaUse         5
#define MAX_F0NBYTES               255

// aliases
#define I2Cx                       CC(I2C,I2Cx_N)
#define CHW_I2Cx                   CC(CHW_I2C,I2Cx_N)
#define CHW_I2Cx_Base              CC(CHW_I2Cx,_Base)
#define CHW_I2Cx_Slave             CC(CHW_I2Cx,_Slave)
#define CHW_I2Cx_Master            CC(CHW_I2Cx,_Master)
#define g_HW_I2Cx                  CC(g_HW_I2C,I2Cx_N)
#define CHW_I2Cx_InterfaceWrapper  CCC(CHW_I2C,I2Cx_N,_InterfaceWrapper)
#define g_HW_I2Cx_InterfaceWrapper CCC(g_HW_I2C,I2Cx_N,_InterfaceWrapper)
#define I2Cx_DMA_TX_Channel        CC(DMA_Channel_,THIRD(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_Stream         CCCC(DMA,FIRST(I2Cx_DMA_TX),_Stream,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_IRQn           CCCCC(DMA,FIRST(I2Cx_DMA_TX),_Stream,SECOND(I2Cx_DMA_TX),_IRQn)
#define I2Cx_DMA_TX_IRQHandler     CCCCC(DMA,FIRST(I2Cx_DMA_TX),_Stream,SECOND(I2Cx_DMA_TX),_IRQHandler)
#define I2Cx_DMA_TX_TC_FLAG        CC(DMA_FLAG_TCIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_HT_FLAG        CC(DMA_FLAG_HTIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_TE_FLAG        CC(DMA_FLAG_TEIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_TC_BIT         CC(DMA_IT_TCIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_TX_TE_BIT         CC(DMA_IT_TEIF,SECOND(I2Cx_DMA_TX))
#define I2Cx_DMA_RX_Channel        CC(DMA_Channel_,THIRD(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_Stream         CCCC(DMA,FIRST(I2Cx_DMA_RX),_Stream,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_IRQn           CCCCC(DMA,FIRST(I2Cx_DMA_RX),_Stream,SECOND(I2Cx_DMA_RX),_IRQn)
#define I2Cx_DMA_RX_IRQHandler     CCCCC(DMA,FIRST(I2Cx_DMA_RX),_Stream,SECOND(I2Cx_DMA_RX),_IRQHandler)
#define I2Cx_DMA_RX_TC_FLAG        CC(DMA_FLAG_TCIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_HT_FLAG        CC(DMA_FLAG_HTIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_TE_FLAG        CC(DMA_FLAG_TEIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_TC_BIT         CC(DMA_IT_TCIF,SECOND(I2Cx_DMA_RX))
#define I2Cx_DMA_RX_TE_BIT         CC(DMA_IT_TEIF,SECOND(I2Cx_DMA_RX))
/*----------- I2Cx Interrupt Priority -------------*/
#define I2Cx_IT_EVT_IRQn           IF_STM32F4(CCC(I2C,I2Cx_N,_EV_IRQn))       IF_STM32F0(CCC(I2C,I2Cx_N,_IRQn))
#define I2Cx_IT_EVT_IRQHandler     IF_STM32F4(CCC(I2C,I2Cx_N,_EV_IRQHandler)) IF_STM32F0(CCC(I2C,I2Cx_N,_IRQHandler))
#define I2Cx_IT_ERR_IRQn           CCC(I2C,I2Cx_N,_ER_IRQn)
#define I2Cx_IT_ERR_IRQHandler     CCC(I2C,I2Cx_N,_ER_IRQHandler)
#define I2Cx_IT_EVT_SUBPRIO             2   /* I2Cx EVT SUB-PRIORITY */
#define I2Cx_IT_ERR_SUBPRIO             0   /* I2Cx ERR SUB-PRIORITY */
#define I2Cx_IT_DMATX_SUBPRIO           1   /* I2Cx DMA TX SUB-PRIORITY */
#define I2Cx_IT_DMARX_SUBPRIO           1   /* I2Cx DMA RX SUB-PRIORITY */

#define I2C_FLAG_MASK  IF_STM32F4((uint32_t)0xFFFFFF) IF_STM32F0((uint32_t)0xFFFF)

//#define I2C_DEBUG_INFO(...) DEBUG_INFO("I2C" STR(I2Cx_N) ":" __VA_ARGS__)
#define I2C_DEBUG_INFO(...) {}

#define CHANGE_BUS_STATE(TO_STATE, timeout) { \
	m_nTimeoutCountdown = timeout; \
	if (m_eBusState != TO_STATE) { \
		m_eBusState = TO_STATE; \
		I2C_DEBUG_INFO("BUS state: " #TO_STATE); \
	} \
}
#define CHECKED_CHANGE_BUS_STATE(FROM_STATE, TO_STATE, timeout) { \
	ASSERTE(m_eBusState == FROM_STATE); \
	CHANGE_BUS_STATE(TO_STATE, timeout) \
}

#define CHANGE_MASTER_STATE(TO_STATE, timeout) { \
	m_nTimeoutCountdown = timeout; \
	if (m_eMasterState != TO_STATE) { \
		m_eMasterState = TO_STATE; \
		I2C_DEBUG_INFO("MASTER state: " #TO_STATE " a:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0); \
	} \
}
#define CHECKED_CHANGE_MASTER_STATE(FROM_STATE, TO_STATE, timeout) { \
	ASSERTE(m_eMasterState == FROM_STATE); \
	CHANGE_MASTER_STATE(TO_STATE, timeout) \
}

#define CHANGE_SLAVE_STATE(TO_STATE, timeout) { \
	m_nTimeoutCountdown = timeout; \
	if (m_eSlaveState != TO_STATE) { \
		m_eSlaveState = TO_STATE; \
		I2C_DEBUG_INFO("SLAVE state: " #TO_STATE); \
	} \
}
#define CHECKED_CHANGE_SLAVE_STATE(FROM_STATE, TO_STATE, timeout) { \
	ASSERTE(m_eSlaveState == FROM_STATE); \
	CHANGE_SLAVE_STATE(TO_STATE, timeout) \
}

#define ASSERT_EVENT_IS_HANDLED_I2C(msg) ASSERT_EVENT_IS_HANDLED(msg ". " IF_STM32F4("SR1:", I2Cx->SR1) IF_STM32F0("ISR:", I2Cx->ISR))

#define MAX_I2C_DEVICES 10

class CHW_I2Cx_Base
{
public:
	typedef IDeviceI2C::ErrorType ErrorType;

protected:
	typedef enum {
		BUSSTATE_DISABLED,
		BUSSTATE_INIT,
		BUSSTATE_IDLE,
		BUSSTATE_SLAVE1,
		BUSSTATE_SLAVE2,
		BUSSTATE_MASTER,
		BUSSTATE_RECOVER,
	} EBusState;

protected:
	static void Init_Base(unsigned uiClockSpeed);
	static void Init_Pins(bool bI2C);
	static bool Init_I2Cx(unsigned uiSlaveAddress1, unsigned uiSlaveAddress2);
#if defined(I2Cx_DMA_TX) || defined (I2Cx_DMA_RX)
	static void Init_I2Cx_DMA();
#endif
	static void Init_I2Cx_IRQ();
	static void Deinit_I2Cx();
	static void Deinit_Pins();

	static bool DoInit(unsigned uiSlaveAddress1, unsigned uiSlaveAddress2);
	static void DoSendAddressForRx(unsigned uiAddress);
	static void DoSendAddressForTx(unsigned uiAddress);
	static void DoSendAddressForCheck(unsigned uiAddress);
	static void DoTXByte();
	static void DoRXByte();
	static void UpdateTxMode();
	static void UpdateRxMode();
	static void DoPrepareTX();
	static void DoPrepareRX(bool bAgain = false);
#if defined(I2Cx_DMA_TX)
	static void DoEnableTxDMA();
	static void DoDisableTxDMA();
	static void DoPrepareTXDMA();
	static unsigned GetDMATxBytes();
#endif
#if defined(I2Cx_DMA_RX)
	static void DoEnableRxDMA();
	static void DoDisableRxDMA();
	static void DoPrepareRXDMA();
	static unsigned GetDMARxBytes();
#endif
	static void DoEnableEventInterrupt();
	static void DoDisableEventInterrupt();
	static void DoEnableBufferTxInterrupt();
	static void DoEnableBufferRxInterrupt();
	static void DoDisableBufferInterrupt();
	static void DoCancel();
	typedef enum {
		BUS_OK,
		BUSERROR_LOWSCL,
		BUSERROR_LOWSDA,
		BUSERROR_LOWBOTH,
	} EBusError;
	static EBusError DoCheckBusPinsState();
	static void DoClearAddrFlag();
	static void DoClearStopFlag();

protected:
	static unsigned           m_uiClockSpeed;

	static unsigned           m_nBusRecoverValidCounts;
#if defined(I2Cx_DMA_TX)
	static unsigned           m_uiMinTXSizeForDmaUse;
	static bool               m_bTxDmaReinitRequired;
	static bool               m_bUseTxDma;
#endif
#if defined(I2Cx_DMA_RX)
	static unsigned           m_uiMinRXSizeForDmaUse;
	static bool               m_bRxDmaReinitRequired;
	static bool               m_bUseRxDma;
#endif

	static const CChainedConstChunks* m_pTxChunks;
	static const CChainedIoChunks*    m_pRxChunks;
	static unsigned           m_uiTxTotalBytesLeft;
	static unsigned           m_uiTxChunkBytesSend;
	static unsigned           m_uiRxTotalBytesLeft;
	static unsigned           m_uiRxChunkBytesReceived;

#ifdef STM32F4
	static bool               m_bUseRx1Procedure;
	static bool               m_bUseRx2Procedure;
#endif
	static bool               m_bReinitRequired;
	static EBusState          m_eBusState;
	static EBusError          m_eBusError;
	static unsigned           m_nTimeoutCountdown;
};

class CHW_I2Cx_Slave :
		public CHW_I2Cx_Base
{

protected:
	typedef enum {
		SLAVESTATE_IDLE,
		SLAVESTATE_RX,
		SLAVESTATE_RX_DMA,
		SLAVESTATE_TX,
		SLAVESTATE_TX_DMA,
	} ESlaveState;

protected:
	static void Init_Slave(unsigned uiClockSpeed, ISlaveI2C* pSlaveI2CDevice1 = NULL, ISlaveI2C* pSlaveI2CDevice2 = NULL);
	static void PrepareSlaveReadSession(ISlaveI2C* pDeviceI2C);
	static void PrepareSlaveWriteSession(ISlaveI2C* pDeviceI2C);
//	static void DoStopMasterSession();

protected:
	static ESlaveState       m_eSlaveState;
	static ISlaveI2C*        m_pSlaveI2CDevice1;
	static ISlaveI2C*        m_pSlaveI2CDevice2;
};

class CHW_I2Cx_Master :
		public CHW_I2Cx_Base
{
protected:
	typedef enum {
		FSMSTATE_IDLE,
		FSMSTATE_START,
		FSMSTATE_DATA_TX_N,
#if defined(I2Cx_DMA_TX)
		FSMSTATE_DATA_TX_DMA,
#endif
		FSMSTATE_DATA_TX_DONE,
		FSMSTATE_START_AGAIN,
		FSMSTATE_DATA_RX_N,
#ifdef STM32F4
		FSMSTATE_TXADDRESS,
		FSMSTATE_RXADDRESS,
		FSMSTATE_DATA_RX_3,
		FSMSTATE_DATA_RX_2,
		FSMSTATE_DATA_RX_SINGLE,
#endif
#if defined(I2Cx_DMA_RX)
		FSMSTATE_DATA_RX_DMA,
#endif
		FSMSTATE_STOP,
	} EMasterState;

protected:
	static void Init_Master(unsigned uiClockSpeed);
	static bool PopNextMasterSession();
	static void PrepareMasterSession();
	static void DoStartMasterSession();
	static void DoStartAgainMasterSession();
//	static void DoStopMasterSession();

	static void NotifyMasterPktDone(IDeviceI2C* pDeviceI2C);
	static void NotifyMasterPktError(ErrorType e, IDeviceI2C* pDeviceToNotify);
	static void NotifyMasterI2CIdle();

protected:
	static EMasterState       m_eMasterState;
	static IDeviceI2C*        m_pCurrentI2CDevice;
	static TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> m_qSessionsQueue;
	static TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> m_qIdleNotifyQueue;
	static bool               m_bStartDiscarded;
};

class CHW_I2Cx :
//		public CHW_I2Cx_Base,
		public CHW_I2Cx_Slave,
		public CHW_I2Cx_Master,
		public ISysTickNotifee
{
public:
	static void Init(unsigned uiClockSpeed);
	static void Deinit();

	static void AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1 = NULL, ISlaveI2C* pSlaveI2CDevice2 = NULL);

	static bool Queue_I2C_Session(IDeviceI2C* pDeviceI2C);
	static void Cancel_I2C_Session(IDeviceI2C* pDeviceI2C);
	static void RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C);

protected:
	static void ProcessI2CIdleEvent(uint32_t uiEvent);
	static void ProcessI2CSlave1Event(uint32_t uiEvent);
	static void ProcessI2CSlave2Event(uint32_t uiEvent);
	static void ProcessI2CxSlaveDMATXTC(ISlaveI2C* pSlaveDevice);
	static void ProcessI2CxSlaveDMATXTE(ISlaveI2C* pSlaveDevice);
	static void ProcessI2CxSlaveDMARXTC(ISlaveI2C* pSlaveDevice);
	static void ProcessI2CxSlaveDMARXTE(ISlaveI2C* pSlaveDevice);
	static void ProcessI2CMasterEvent(uint32_t uiEvent);
	static void ProcessI2CxMasterDMATXTC();
	static void ProcessI2CxMasterDMATXTE();
	static void ProcessI2CxMasterDMARXTC();
	static void ProcessI2CxMasterDMARXTE();
	static void ProcessSlave1RxAddress();
	static void ProcessSlave2RxAddress();
	static void ProcessSlaveRxStartProcedure();
	static void ProcessSlaveRx(ISlaveI2C* pSlaveDevice);
	static void ProcessSlave1TxAddress();
	static void ProcessSlave2TxAddress();
	static void ProcessSlaveTxStartProcedure();
	static void ProcessSlaveTx(ISlaveI2C* pSlaveDevice);
	static void ProcessMasterTxProcedureFirst();
	static void ProcessMasterTxProcedureNext();
	static void ProcessMasterRxProcedure();
	static void ProcessMasterStop(bool bDoGenerateStop = true);
	static void ProcessMasterDiscardStartIfNecessary();
	static void ProcessBusFinalize();
	static bool ProcessBusStopIfNecessary();
	static void FinalizeCurrentSlaveOperation(ISlaveI2C* pSlaveDevice);


public: // ISysTickNotifee
	virtual void OnSysTick();

public: // IRQHandlers
	static void OnI2CxEvent();
	static void OnI2CxError();
	static void OnI2CxDMATX();
	static void OnI2CxDMARX();

private:

private:
	static bool ProcessBusInitIfNecessary();
	static bool ProcessQueuedSessions();
	static void ProcessAbortSession();
	static void ProcessAbortSlave1Session(ErrorType e);
	static void ProcessAbortSlave2Session(ErrorType e);
	static void ProcessAbortMasterSession(ErrorType e);
	static void ProcessAbortBusSession(ErrorType e);

private:
	static unsigned    m_uiSlaveAddress1;
	static unsigned    m_uiSlaveAddress2;
};


/****************************************************************************/

unsigned             CHW_I2Cx_Base::m_uiClockSpeed;
unsigned             CHW_I2Cx_Base::m_nBusRecoverValidCounts;
#if defined(I2Cx_DMA_TX)
unsigned             CHW_I2Cx_Base::m_uiMinTXSizeForDmaUse;
bool                 CHW_I2Cx_Base::m_bTxDmaReinitRequired;
bool                 CHW_I2Cx_Base::m_bUseTxDma;
#endif
#if defined(I2Cx_DMA_RX)
unsigned             CHW_I2Cx_Base::m_uiMinRXSizeForDmaUse;
bool                 CHW_I2Cx_Base::m_bRxDmaReinitRequired;
bool                 CHW_I2Cx_Base::m_bUseRxDma;
#endif
const CChainedConstChunks*  CHW_I2Cx_Base::m_pTxChunks;
const CChainedIoChunks*     CHW_I2Cx_Base::m_pRxChunks;
unsigned                    CHW_I2Cx_Base::m_uiTxTotalBytesLeft;
unsigned                    CHW_I2Cx_Base::m_uiTxChunkBytesSend;
unsigned                    CHW_I2Cx_Base::m_uiRxTotalBytesLeft;
unsigned                    CHW_I2Cx_Base::m_uiRxChunkBytesReceived;
#ifdef STM32F4
bool                        CHW_I2Cx_Base::m_bUseRx1Procedure;
bool                        CHW_I2Cx_Base::m_bUseRx2Procedure;
#endif
bool                        CHW_I2Cx_Base::m_bReinitRequired;
CHW_I2Cx_Base::EBusState    CHW_I2Cx_Base::m_eBusState;
CHW_I2Cx_Base::EBusError    CHW_I2Cx_Base::m_eBusError;
unsigned                    CHW_I2Cx_Base::m_nTimeoutCountdown;

ISlaveI2C*                  CHW_I2Cx_Slave::m_pSlaveI2CDevice1;
ISlaveI2C*                  CHW_I2Cx_Slave::m_pSlaveI2CDevice2;
CHW_I2Cx_Slave::ESlaveState CHW_I2Cx_Slave::m_eSlaveState;

CHW_I2Cx_Master::EMasterState        CHW_I2Cx_Master::m_eMasterState;
IDeviceI2C*                          CHW_I2Cx_Master::m_pCurrentI2CDevice;
TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> CHW_I2Cx_Master::m_qSessionsQueue;
TAtomicQueue<IDeviceI2C*, MAX_I2C_DEVICES> CHW_I2Cx_Master::m_qIdleNotifyQueue;
bool                                 CHW_I2Cx_Master::m_bStartDiscarded;

unsigned            CHW_I2Cx::m_uiSlaveAddress1;
unsigned            CHW_I2Cx::m_uiSlaveAddress2;

REDIRECT_ISR_STATIC(I2Cx_IT_EVT_IRQHandler, CHW_I2Cx, OnI2CxEvent);
#ifdef STM32F4
REDIRECT_ISR_STATIC(I2Cx_IT_ERR_IRQHandler, CHW_I2Cx, OnI2CxError);
#endif
#if defined(I2Cx_DMA_TX)
REDIRECT_ISR_STATIC(I2Cx_DMA_TX_IRQHandler, CHW_I2Cx, OnI2CxDMATX);
#endif
#if defined(I2Cx_DMA_RX)
REDIRECT_ISR_STATIC(I2Cx_DMA_RX_IRQHandler, CHW_I2Cx, OnI2CxDMARX);
#endif

CHW_I2Cx g_HW_I2Cx;

/****************************************************************************/

void CHW_I2Cx_Base::Init_Base(unsigned uiClockSpeed)
{
	m_uiClockSpeed = uiClockSpeed;
	m_bReinitRequired = false;
	m_eBusState = BUSSTATE_INIT;
	m_eBusError = BUS_OK;
	m_nTimeoutCountdown = 0;

#if defined(I2Cx_DMA_TX)
	m_uiMinTXSizeForDmaUse = MinTXSizeForDmaUse;
	m_bTxDmaReinitRequired = true;
#endif
#if defined(I2Cx_DMA_RX)
	m_uiMinRXSizeForDmaUse = MinRXSizeForDmaUse;
	m_bRxDmaReinitRequired = true;
#endif
	m_pTxChunks = NULL;
	m_uiTxTotalBytesLeft = 0;
	m_uiTxChunkBytesSend = 0;
	m_pRxChunks = NULL;
	m_uiRxTotalBytesLeft = 0;
	m_uiRxChunkBytesReceived = 0;
}

/*static*/
void CHW_I2Cx_Base::Init_Pins(bool bI2C)
{
	/* PA11 and PA12 remap on QFN28 and TSSOP20 packages */
#if (MPU_PINS_COUNT <= 28)
# if ((CC(0x0,FIRST(I2Cx_SCL_GPIO_PORT)) == 0x0A) && (SECOND(I2Cx_SCL_GPIO_PORT) == 11)) \
  || ((CC(0x0,FIRST(I2Cx_SDA_GPIO_PORT)) == 0x0A) && (SECOND(I2Cx_SCL_GPIO_PORT) == 12))
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG->CFGR1 |= SYSCFG_CFGR1_PA11_PA12_RMP;
# endif
#endif

	/* Initialize I2Cx GPIO */
	/* Enable I2Cx SCL and SDA Pin Clock */
	IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,FIRST(I2Cx_SCL_GPIO_PORT)) | CC(RCC_AHB1Periph_GPIO,FIRST(I2Cx_SDA_GPIO_PORT)), ENABLE));
	IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(I2Cx_SCL_GPIO_PORT)) | CC(RCC_AHBPeriph_GPIO,FIRST(I2Cx_SDA_GPIO_PORT)), ENABLE));
	if (bI2C) {
		INIT_GPIO_AF_OD_PU_FAST(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT), THIRD(I2Cx_SCL_GPIO_PORT,CC(AF_I2C,I2Cx_N)));
		INIT_GPIO_AF_OD_PU_FAST(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT), THIRD(I2Cx_SDA_GPIO_PORT,CC(AF_I2C,I2Cx_N)));
	} else {
		INIT_GPIO_IN_PU(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT));
		INIT_GPIO_IN_PU(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT));
	}
}

/*static*/
bool CHW_I2Cx_Base::Init_I2Cx(unsigned uiSlaveAddress1, unsigned uiSlaveAddress2)
{
	/* Initialize I2Cx Clock */
	RCC_APB1PeriphResetCmd(CC(RCC_APB1Periph_I2C,I2Cx_N), ENABLE);
	RCC_APB1PeriphResetCmd(CC(RCC_APB1Periph_I2C,I2Cx_N), DISABLE);
	RCC_APB1PeriphClockCmd(CC(RCC_APB1Periph_I2C,I2Cx_N), ENABLE);

	/* Initialize I2Cx device with parameters */
	I2C_InitTypeDef I2C_InitStructure;
	I2C_StructInit(&I2C_InitStructure);
#ifdef STM32F4
	I2C_InitStructure.I2C_ClockSpeed          = m_uiClockSpeed;                /* Initialize the I2C_ClockSpeed member */
	I2C_InitStructure.I2C_DutyCycle           = I2C_DutyCycle_2;               /* Initialize the I2C_DutyCycle member */
#else
//	const unsigned tClockSpeed_ns = 1000000000 / m_uiClockSpeed;
//	const unsigned tMaxSCL_ns = m_uiClockSpeed * 3;
//	const unsigned tSCL_ns = 1000000000 / (CHW_Clocks::GetCLKFrequency() / timing_PRESC);
	union TI2CTiming {
		struct TFlds {
			uint32_t SCLL     :8;
			uint32_t SCLH     :8;
			uint32_t SDADEL   :4;
			uint32_t SCLDEL   :4;
			uint32_t _rsrv    :4;
			uint32_t PRESC    :4;
			TFlds(uint32_t aPRESC, uint32_t aSCLDEL, uint32_t aSDADEL, uint32_t aSCLH, uint32_t aSCLL):
				SCLL(aSCLH),SCLH(aSCLH),SDADEL(aSDADEL),SCLDEL(aSCLDEL),_rsrv(0),PRESC(aPRESC) {}
		};

		TFlds flds;
		uint32_t timing;

		TI2CTiming(uint32_t aPRESC, uint32_t aSCLDEL, uint32_t aSDADEL, uint32_t aSCLH, uint32_t aSCLL):
			flds(aPRESC, aSCLDEL, aSDADEL, aSCLH, aSCLL) {}
		TI2CTiming(uint32_t aTiming) : timing(aTiming) {}
	};
	switch (CHW_Clocks::GetCLKFrequency()) {
	case 48000000:
		switch (m_uiClockSpeed) {
		case 10000:   I2C_InitStructure.I2C_Timing = TI2CTiming(0xB, 0x4, 0x2, 0xC3, 0xC7).timing; break;
		case 100000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0xB, 0x4, 0x2, 0x0F, 0x13).timing; break;
		case 400000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x5, 0x3, 0x3, 0x03, 0x09).timing; break;
		case 1000000: I2C_InitStructure.I2C_Timing = TI2CTiming(0x5, 0x1, 0x0, 0x01, 0x03).timing; break;
		default:      ASSERTE(false); // TODO implement I2C timing calculation for custom I2C speed
		}
		break;
	case 16000000:
		switch (m_uiClockSpeed) {
		case 10000:   I2C_InitStructure.I2C_Timing = TI2CTiming(0x3, 0x4, 0x2, 0xC3, 0xC7).timing; break;
		case 100000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x3, 0x4, 0x2, 0x0F, 0x13).timing; break;
		case 400000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x1, 0x3, 0x2, 0x03, 0x09).timing; break;
		case 1000000: I2C_InitStructure.I2C_Timing = TI2CTiming(0x0, 0x2, 0x0, 0x02, 0x04).timing; break;
		default:      ASSERTE(false); // TODO implement I2C timing calculation for custom I2C speed
		}
		break;
	case 8000000:
		switch (m_uiClockSpeed) {
		case 10000:   I2C_InitStructure.I2C_Timing = TI2CTiming(0x1, 0x4, 0x2, 0xC3, 0xC7).timing; break;
		case 100000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x1, 0x4, 0x2, 0x0F, 0x13).timing; break;
		case 400000:  I2C_InitStructure.I2C_Timing = TI2CTiming(0x0, 0x3, 0x1, 0x03, 0x09).timing; break;
		case 1000000: I2C_InitStructure.I2C_Timing = TI2CTiming(0x0, 0x1, 0x0, 0x03, 0x06).timing; break;
		default:      ASSERTE(false); // TODO implement I2C timing calculation for custom I2C speed
		}
		break;
	default:
		ASSERTE(false); // TODO implement I2C timing calculation for custom CLK freq
	}
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
#endif
	I2C_InitStructure.I2C_Mode                = I2C_Mode_I2C;                  /* Initialize the I2C_Mode member */
	I2C_InitStructure.I2C_OwnAddress1         = uiSlaveAddress1<<1;            /* Initialize the I2C_OwnAddress1 member */
	I2C_InitStructure.I2C_Ack                 = I2C_Ack_Enable;                /* Initialize the I2C_Ack member */
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;  /* Initialize the I2C_AcknowledgedAddress member */
	I2C_Init(I2Cx, &I2C_InitStructure);


	if (!uiSlaveAddress1) {
		/* Disable I2C events interrupt - it would be enabled on comm starts */
		DoDisableEventInterrupt();
	} else {
		if (uiSlaveAddress2) {
			ASSERTE(uiSlaveAddress1 != uiSlaveAddress2);
			IF_STM32F4(I2C_OwnAddress2Config(I2Cx, uiSlaveAddress2<<1));
			IF_STM32F0(I2C_OwnAddress2Config(I2Cx, uiSlaveAddress2<<1, I2C_OA2_NoMask));
			I2C_DualAddressCmd(I2Cx, ENABLE);
		}
		/* Enable I2C Interrupts now to receive sessions for the slave(s) */
		DoEnableEventInterrupt();
	}

#if defined(I2Cx_DMA_TX) || defined(I2Cx_DMA_RX)
	Init_I2Cx_DMA();
#endif
	Init_I2Cx_IRQ();

	Init_Pins(true);

	/* Enable I2Cx Device */
	I2C_Cmd(I2Cx, ENABLE);
	I2C_DEBUG_INFO("ENABLED");

	return true;
}

#if defined(I2Cx_DMA_TX) || defined(I2Cx_DMA_RX)
void CHW_I2Cx_Base::Init_I2Cx_DMA()
{
	/* Initialize I2Cx DMA Channels */
	/* Enable I2Cx DMA */
#if defined(I2Cx_DMA_TX)
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(I2Cx_DMA_TX)), ENABLE);
# if defined(I2Cx_DMA_RX) && FIRST(I2Cx_DMA_TX) != FIRST(I2Cx_DMA_RX)
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(I2Cx_DMA_RX)), ENABLE);
# endif
	/* Initialize I2Cx DMA Tx Stream */
	m_bTxDmaReinitRequired = true; // do it later - when TX op starts
#else
	RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_DMA,FIRST(I2Cx_DMA_RX)), ENABLE);
	/* Initialize I2Cx DMA Rx Stream */
	m_bRxDmaReinitRequired = true; // do it later - when RX op starts
#endif
}
#endif

void CHW_I2Cx_Base::Init_I2Cx_IRQ()
{

	/* Configure NVIC priority Group */
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	/* Enable the IRQ channel */
	NVIC_INIT(I2Cx_IT_EVT_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_EVT_SUBPRIO);
#ifdef STM32F4
	/* Configure NVIC for I2Cx ERR Interrupt */
	NVIC_INIT(I2Cx_IT_ERR_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_ERR_SUBPRIO);
#endif
#if defined(I2Cx_DMA_TX)
	/* Configure NVIC for DMA TX channel interrupt */
	NVIC_INIT(I2Cx_DMA_TX_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_DMATX_SUBPRIO);
#endif
#if defined(I2Cx_DMA_RX)
	/* Configure NVIC for DMA RX channel interrupt */
	NVIC_INIT(I2Cx_DMA_RX_IRQn, I2Cx_IT_GROUP_PRIO, I2Cx_IT_DMARX_SUBPRIO);
#endif
}

/*static*/
void CHW_I2Cx_Base::DoEnableEventInterrupt()
{
	I2C_DEBUG_INFO("Event IRQ enabled");
	IF_STM32F4(I2C_ITConfig(I2Cx, I2C_IT_EVT | I2C_IT_ERR, ENABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_ADDRI | I2C_IT_NACKI | I2C_IT_STOPI | I2C_IT_TCI | I2C_IT_ERRI, ENABLE));
	INDIRECT_CALL(CHW_I2Cx::OnI2CxEvent());
}

/*static*/
void CHW_I2Cx_Base::DoDisableEventInterrupt()
{
	IF_STM32F4(I2C_ITConfig(I2Cx, I2C_IT_EVT | I2C_IT_ERR, DISABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_ADDRI | I2C_IT_NACKI | I2C_IT_STOPI | I2C_IT_TCI | I2C_IT_ERRI, DISABLE));
	NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
#ifdef STM32F4
	NVIC_ClearPendingIRQ(I2Cx_IT_ERR_IRQn);
#endif
	I2C_DEBUG_INFO("Event IRQ disabled");
}

/*static*/
void CHW_I2Cx_Base::DoEnableBufferTxInterrupt()
{
	IF_STM32F4(I2C_ITConfig(I2Cx, I2C_IT_BUF, ENABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_TXI, ENABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_RXI, DISABLE));
	I2C_DEBUG_INFO("Buffer IRQ TX enabled");
}

/*static*/
void CHW_I2Cx_Base::DoEnableBufferRxInterrupt()
{
	IF_STM32F4(I2C_ITConfig(I2Cx, I2C_IT_BUF, ENABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_TXI, DISABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_RXI, ENABLE));
	I2C_DEBUG_INFO("Buffer IRQ RX enabled");
}

/*static*/
void CHW_I2Cx_Base::DoDisableBufferInterrupt()
{
	IF_STM32F4(I2C_ITConfig(I2Cx, I2C_IT_BUF, DISABLE));
	IF_STM32F0(I2C_ITConfig(I2Cx, I2C_IT_TXI | I2C_IT_RXI, DISABLE));
	NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
	I2C_DEBUG_INFO("Buffer IRQ disabled");
}

/*static*/
void CHW_I2Cx_Base::DoCancel()
{
	I2C_DEBUG_INFO("Cancel I2C");
#if defined(I2Cx_DMA_TX) || defined(I2Cx_DMA_RX)
	/* Disable DMA RX Channel TC, TE  */
# if defined(I2Cx_DMA_TX)
	DoDisableTxDMA();
	DMA_Cmd(I2Cx_DMA_TX_Stream, DISABLE);
	DMA_ITConfig(I2Cx_DMA_TX_Stream, DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMA_ClearITPendingBit(I2Cx_DMA_TX_Stream, I2Cx_DMA_TX_TC_BIT | I2Cx_DMA_TX_TE_BIT);
	NVIC_ClearPendingIRQ(I2Cx_DMA_TX_IRQn);
# endif
# if defined(I2Cx_DMA_RX)
	DoDisableRxDMA();
	DMA_Cmd(I2Cx_DMA_RX_Stream, DISABLE);
	DMA_ITConfig(I2Cx_DMA_RX_Stream, DMA_IT_TC | DMA_IT_TE, DISABLE);
	DMA_ClearITPendingBit(I2Cx_DMA_RX_Stream, I2Cx_DMA_RX_TC_BIT | I2Cx_DMA_RX_TE_BIT);
	NVIC_ClearPendingIRQ(I2Cx_DMA_RX_IRQn);
# endif
#endif
	/* Disable I2C Interrupts - it would be enabled on comm starts */
	IF_STM32F4(I2C_GetLastEvent(I2Cx)); // for clear START, STOP, ADDR flags
	DoDisableEventInterrupt();
	I2C_ReceiveData(I2Cx); // clear BTF, RXE
	IF_STM32F4(I2C_ClearITPendingBit(I2Cx, I2C_IT_SMBALERT | I2C_IT_TIMEOUT | I2C_IT_PECERR | I2C_IT_OVR | I2C_IT_AF | I2C_IT_ARLO | I2C_IT_BERR));
	IF_STM32F0(I2C_ClearITPendingBit(I2Cx, I2C_IT_ALERT | I2C_IT_TIMEOUT | I2C_IT_PECERR | I2C_IT_OVR | I2C_IT_NACKF | I2C_IT_ARLO | I2C_IT_BERR));
}

/*static*/
CHW_I2Cx_Base::EBusError CHW_I2Cx_Base::DoCheckBusPinsState()
{
	Init_Pins(false);
	/* Check bus state */
	IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,FIRST(I2Cx_SCL_GPIO_PORT)), ENABLE));
	IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(I2Cx_SCL_GPIO_PORT)), ENABLE));
	bool bStateScl = READ_GPIO_PORT(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT));

	IF_STM32F4(RCC_AHB1PeriphClockCmd(CC(RCC_AHB1Periph_GPIO,FIRST(I2Cx_SDA_GPIO_PORT)), ENABLE));
	IF_STM32F0(RCC_AHBPeriphClockCmd(CC(RCC_AHBPeriph_GPIO,FIRST(I2Cx_SDA_GPIO_PORT)), ENABLE));
	bool bStateSda = READ_GPIO_PORT(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT));

	return (bStateScl && bStateSda) ? BUS_OK : (bStateScl ? BUSERROR_LOWSDA : (bStateSda ? BUSERROR_LOWSCL : BUSERROR_LOWBOTH));
}

/*static*/
void CHW_I2Cx_Base::DoClearAddrFlag()
{
	IF_STM32F4( I2C_ReadRegister(I2Cx, I2C_Register_SR2) );
	IF_STM32F0( I2C_ClearFlag(I2Cx, I2C_FLAG_ADDR) );
}

/*static*/
void CHW_I2Cx_Base::DoClearStopFlag()
{
	IF_STM32F4(I2C_Cmd(I2Cx, ENABLE)); // clear STOPF bit by write to CR1
	IF_STM32F4(I2C_DEBUG_INFO("ENABLED"));
	IF_STM32F0( I2C_ClearFlag(I2Cx, I2C_FLAG_STOPF) );
}

bool CHW_I2Cx_Base::DoInit(unsigned uiSlaveAddress1, unsigned uiSlaveAddress2)
{
	Deinit_I2Cx();
	if (DoCheckBusPinsState() == BUS_OK) {
		Init_I2Cx(uiSlaveAddress1, uiSlaveAddress2);
		return true;
	}
	return false;
}

void CHW_I2Cx_Base::DoSendAddressForRx(unsigned uiAddress)
{
	DoPrepareRX();
#ifdef STM32F4
	I2C_DEBUG_INFO("Send 7bRx Address:", uiAddress);
	I2C_Send7bitAddress(I2Cx, uiAddress << 1, I2C_Direction_Receiver);
#endif
#ifdef STM32F0
	ASSERTE(m_uiRxTotalBytesLeft) ;
	unsigned uiLength = (m_uiRxTotalBytesLeft < MAX_F0NBYTES) ? m_uiRxTotalBytesLeft : MAX_F0NBYTES;
	I2C_TransferHandling(I2Cx, uiAddress << 1, uiLength, I2C_SoftEnd_Mode, I2C_Generate_Start_Read);
	I2C_DEBUG_INFO("Rx TransferHandling. NBYTES:", uiLength);
#endif
}

void CHW_I2Cx_Base::DoSendAddressForTx(unsigned uiAddress)
{
	DoPrepareTX();
#ifdef STM32F4
	I2C_DEBUG_INFO("Send 7bTx Address:", uiAddress);
	I2C_Send7bitAddress(I2Cx, uiAddress << 1, I2C_Direction_Transmitter);
#endif
#ifdef STM32F0
	ASSERTE(m_uiTxTotalBytesLeft) ;
	unsigned uiLength = (m_uiTxTotalBytesLeft < MAX_F0NBYTES) ? m_uiTxTotalBytesLeft : MAX_F0NBYTES;
	I2C_TransferHandling(I2Cx, uiAddress << 1, uiLength, I2C_Reload_Mode, I2C_Generate_Start_Write);
	I2C_DEBUG_INFO("Tx TransferHandling. NBYTES:", uiLength);
#endif
}

void CHW_I2Cx_Base::DoSendAddressForCheck(unsigned uiAddress)
{
	// no tx/rx - only check connection to the address
#ifdef STM32F4
	I2C_DEBUG_INFO("Send 7bTx Address:", uiAddress);
	I2C_Send7bitAddress(I2Cx, uiAddress << 1, I2C_Direction_Transmitter);
#endif
#ifdef STM32F0
	ASSERTE(m_uiTxTotalBytesLeft == 0) ;
	I2C_TransferHandling(I2Cx, uiAddress << 1, 0, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);
	I2C_DEBUG_INFO("Tx TransferHandling. Check only");
#endif
}

void CHW_I2Cx_Base::DoTXByte()
{
	ASSERTE(m_pTxChunks);
	ASSERTE(m_pTxChunks->pData);
	ASSERTE(m_pTxChunks->uiLength > m_uiTxChunkBytesSend);
	ASSERTE(m_uiTxTotalBytesLeft);
	uint8_t uiByteToSend = *(m_pTxChunks->pData + m_uiTxChunkBytesSend);
	I2C_DEBUG_INFO("TX next byte. (non DMA):", uiByteToSend);
	I2C_SendData(I2Cx, uiByteToSend);
	I2C_DEBUG_INFO("TX:",uiByteToSend);
	++m_uiTxChunkBytesSend;
	--m_uiTxTotalBytesLeft;
	I2C_DEBUG_INFO("TX chunk bytes send:", m_uiTxChunkBytesSend);
	I2C_DEBUG_INFO("TX chunk size:", m_pTxChunks->uiLength);
	if (m_uiTxChunkBytesSend >= m_pTxChunks->uiLength) {
		m_uiTxChunkBytesSend = 0;
		m_pTxChunks = m_pTxChunks->GetNextItem();
		if (m_pTxChunks) {
			UpdateTxMode();
			I2C_DEBUG_INFO("Next TX chunk:", (uint32_t)m_pTxChunks);
			I2C_DEBUG_INFO("data:", (uint32_t)m_pTxChunks->pData);
			I2C_DEBUG_INFO("len:", m_pTxChunks->uiLength);
#if defined(I2Cx_DMA_TX)
			I2C_DEBUG_INFO("dma?:", m_bUseTxDma);
#endif
		} else {
			ASSERTE(m_uiTxTotalBytesLeft == 0);
			I2C_DEBUG_INFO("Tx last chunk finished");
		}
	}
}

void CHW_I2Cx_Base::DoRXByte()
{
	ASSERTE(m_pRxChunks);
	ASSERTE(m_pRxChunks->pData);
	ASSERTE(m_pRxChunks->uiLength > m_uiRxChunkBytesReceived);
	ASSERTE(m_uiRxTotalBytesLeft);
	uint8_t uiByteReceived = I2C_ReceiveData(I2Cx);
	*(m_pRxChunks->pData + m_uiRxChunkBytesReceived) = uiByteReceived;
	++m_uiRxChunkBytesReceived;
	--m_uiRxTotalBytesLeft;
	if (m_uiRxChunkBytesReceived >= m_pRxChunks->uiLength) {
		m_uiRxChunkBytesReceived = 0;
		m_pRxChunks = m_pRxChunks->GetNextItem();
#if defined(I2Cx_DMA_RX)
		m_bUseRxDma = m_pRxChunks && (m_pRxChunks->uiLength >= m_uiMinRXSizeForDmaUse);
#endif
	}
	I2C_DEBUG_INFO("RX:",uiByteReceived);
}

void CHW_I2Cx_Base::UpdateTxMode()
{
	if (m_pTxChunks) {
#if defined(I2Cx_DMA_TX)
		m_bUseTxDma = (m_pTxChunks->uiLength >= m_uiMinTXSizeForDmaUse);
#endif
	}
}

void CHW_I2Cx_Base::UpdateRxMode()
{
	if (m_pRxChunks) {
		ASSERTE(m_pRxChunks->uiLength);
		ASSERTE(m_pRxChunks->uiLength <= m_uiRxTotalBytesLeft);
#if defined(I2Cx_DMA_RX)
		m_bUseRxDma = (m_pRxChunks->uiLength >= m_uiMinRXSizeForDmaUse);
#endif
#ifdef STM32F4
		m_bUseRx1Procedure = (m_uiRxTotalBytesLeft == 1);
		m_bUseRx2Procedure = (m_uiRxTotalBytesLeft == 2);
#endif
	}
}

void CHW_I2Cx_Base::DoPrepareTX()
{
	ASSERTE(m_pTxChunks);
#if defined(I2Cx_DMA_TX)
	if (m_bUseTxDma) {
		DoDisableBufferInterrupt();
		DoPrepareTXDMA();
		I2C_DMALastTransferCmd(I2Cx, (m_pTxChunks->GetNextItem() || m_pRxChunks)?DISABLE:ENABLE);
		DoEnableTxDMA();
	} else
#endif
	{
		DoEnableBufferTxInterrupt();
	}
}

void CHW_I2Cx_Base::DoPrepareRX(bool bAgain /*= false*/)
{
	ASSERTE(m_pRxChunks);
#if defined(I2Cx_DMA_RX)
	if (m_bUseRxDma) {
		DoPrepareRXDMA();
		DoEnableRxDMA();
	} else
#endif
	{
		DoEnableBufferRxInterrupt();
	}
}

#if defined(I2Cx_DMA_TX)
/*static*/
void CHW_I2Cx_Base::DoEnableTxDMA()
{
	IF_STM32F4(I2C_DMACmd(I2Cx, ENABLE));
	IF_STM32F0(I2C_DMACmd(I2Cx, I2C_DMAReq_Tx, ENABLE));
}

/*static*/
void CHW_I2Cx_Base::DoDisableTxDMA()
{
	IF_STM32F4(I2C_DMACmd(I2Cx, DISABLE));
	IF_STM32F0(I2C_DMACmd(I2Cx, I2C_DMAReq_Tx, DISABLE));
}

/*static*/
void CHW_I2Cx_Base::DoPrepareTXDMA()
{
	ASSERTE(m_pTxChunks);
	ASSERTE(m_pTxChunks->pData);
	ASSERTE(m_pTxChunks->uiLength);
	ASSERTE(m_pTxChunks->uiLength <= 0xFFFF);
	ASSERTE(m_uiTxTotalBytesLeft);
	ASSERTE(!m_uiTxChunkBytesSend);
	uint32_t uiMemoryBaseAddr = (uint32_t)(m_pTxChunks->pData);
	uint16_t uiLength = m_pTxChunks->uiLength;
	if (m_bTxDmaReinitRequired) {
		DMA_InitTypeDef DMAInitStructure;
		DMA_StructInit(&DMAInitStructure);
		DMAInitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
		DMAInitStructure.DMA_Memory0BaseAddr = uiMemoryBaseAddr;
		DMAInitStructure.DMA_BufferSize = uiLength;
		DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(I2Cx->DR);
		DMAInitStructure.DMA_Channel = I2Cx_DMA_TX_Channel;
		DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMAInitStructure.DMA_Mode = DMA_Mode_Normal;
		DMAInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		DMAInitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
		DMAInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
		DMAInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
		DMAInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
		/* Select I2Cx DR Address register as DMA PeripheralBaseAddress */
		DMA_Init(I2Cx_DMA_TX_Stream, &DMAInitStructure);

		/* Enable DMA TX Channel TC, TE  */
		DMA_ITConfig(I2Cx_DMA_TX_Stream, DMA_IT_TC | DMA_IT_TE, ENABLE);
	} else {
		DMA_MemoryTargetConfig(I2Cx_DMA_TX_Stream, uiMemoryBaseAddr, DMA_Memory_0);
		DMA_SetCurrDataCounter(I2Cx_DMA_TX_Stream, uiLength);
	}
	I2C_DEBUG_INFO("DMA TX start len:", uiLength);
	I2C_DEBUG_INFO("DMA TX start ptr:", uiMemoryBaseAddr);
	m_uiTxTotalBytesLeft -= uiLength;
	DMA_Cmd(I2Cx_DMA_TX_Stream, ENABLE);
}

/*static*/
unsigned CHW_I2Cx_Base::GetDMATxBytes()
{
	unsigned uiBytesLeft = DMA_GetCurrDataCounter(I2Cx_DMA_TX_Stream);
	ASSERTE(uiBytesLeft <= m_pTxChunks->uiLength);
	return m_pTxChunks->uiLength - uiBytesLeft;
}
#endif

#if defined(I2Cx_DMA_RX)
/*static*/
void CHW_I2Cx_Base::DoEnableRxDMA()
{
	IF_STM32F4(I2C_DMACmd(I2Cx, ENABLE));
	IF_STM32F0(I2C_DMACmd(I2Cx, I2C_DMAReq_Rx, ENABLE));
	I2C_DEBUG_INFO("DMA RX enabled");
}

/*static*/
void CHW_I2Cx_Base::DoDisableRxDMA()
{
	IF_STM32F4(I2C_DMACmd(I2Cx, DISABLE));
	IF_STM32F0(I2C_DMACmd(I2Cx, I2C_DMAReq_Rx, DISABLE));
	I2C_DEBUG_INFO("DMA RX disabled");
}

/*static*/
void CHW_I2Cx_Base::DoPrepareRXDMA()
{
	ASSERTE(m_pRxChunks);
	ASSERTE(m_pRxChunks->pData);
	ASSERTE(m_pRxChunks->uiLength);
	ASSERTE(m_pRxChunks->uiLength <= 0xFFFF);
	ASSERTE(m_uiRxTotalBytesLeft);
	ASSERTE(m_uiRxChunkBytesReceived == 0);
	uint32_t uiMemoryBaseAddr = (uint32_t)(m_pRxChunks->pData);
	uint16_t uiLength = m_pRxChunks->uiLength;
	bool bLastChunk = !m_pRxChunks->GetNextItem();
	if (m_bRxDmaReinitRequired) {
		DMA_InitTypeDef DMAInitStructure;
		DMA_StructInit(&DMAInitStructure);
		DMAInitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
		DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(I2Cx->DR);
		DMAInitStructure.DMA_Memory0BaseAddr = uiMemoryBaseAddr;
		DMAInitStructure.DMA_BufferSize = uiLength;
		DMAInitStructure.DMA_Channel = I2Cx_DMA_RX_Channel;
		DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
		DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		DMAInitStructure.DMA_Mode = DMA_Mode_Normal;
		DMAInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		DMAInitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
		DMAInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
		DMAInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
		DMAInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
		/* Select I2Cx DR Address register as DMA PeripheralBaseAddress */
		DMA_Init(I2Cx_DMA_RX_Stream, &DMAInitStructure);

		/* Enable DMA RX Channel TC, TE  */
		DMA_ITConfig(I2Cx_DMA_RX_Stream, DMA_IT_TC | DMA_IT_TE, ENABLE);
	} else {
		DMA_MemoryTargetConfig(I2Cx_DMA_RX_Stream, uiMemoryBaseAddr, DMA_Memory_0);
		DMA_SetCurrDataCounter(I2Cx_DMA_RX_Stream, uiLength);
	}
	I2C_DEBUG_INFO("DMA RX start len:", uiLength);
	I2C_DEBUG_INFO("DMA RX start ptr:", uiMemoryBaseAddr);
	m_uiRxTotalBytesLeft -= uiLength;
	I2C_DMALastTransferCmd(I2Cx, bLastChunk?ENABLE:DISABLE);
	DMA_Cmd(I2Cx_DMA_RX_Stream, ENABLE);
}

/*static*/
unsigned CHW_I2Cx_Base::GetDMARxBytes()
{
	unsigned uiBytesLeft = DMA_GetCurrDataCounter(I2Cx_DMA_RX_Stream);
	ASSERTE(uiBytesLeft <= m_pRxChunks->uiLength);
	return m_pRxChunks->uiLength - uiBytesLeft;
}
#endif

void CHW_I2Cx_Base::Deinit_I2Cx()
{
	DoCancel();

	/* Reset I2Cx Device */
	IF_STM32F4(I2C_SoftwareResetCmd(I2Cx, ENABLE); I2C_SoftwareResetCmd(I2Cx, DISABLE););
	IF_STM32F0(I2C_SoftwareResetCmd(I2Cx));

	/* Disable I2Cx Device */
	I2C_Cmd(I2Cx, DISABLE);
	I2C_DEBUG_INFO("DISABLED");
	I2C_DeInit(I2Cx);

	Deinit_Pins();

	/* Deinitialize DMA Channels */
#if defined(I2Cx_DMA_TX)
    DMA_DeInit(I2Cx_DMA_TX_Stream);
    m_bTxDmaReinitRequired = true;
#endif
#if defined(I2Cx_DMA_RX)
    DMA_DeInit(I2Cx_DMA_RX_Stream);
    m_bRxDmaReinitRequired = true;
#endif
}

/*static*/
void CHW_I2Cx_Base::Deinit_Pins()
{
	/* Deinitialize I2Cx GPIO */
	DEINIT_GPIO(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT));
	DEINIT_GPIO(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT));
}

/****************************************************************************/

void CHW_I2Cx_Slave::PrepareSlaveReadSession(ISlaveI2C* pDeviceI2C)
{
	ASSERTE(m_eBusState == BUSSTATE_SLAVE1 || m_eBusState == BUSSTATE_SLAVE2);
	ASSERTE(m_uiTxChunkBytesSend == 0);
	ASSERTE(m_uiRxChunkBytesReceived == 0);
	IF_STM32F4(ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)));
//	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF));
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));

	if (pDeviceI2C->GetDataToSend(&m_pTxChunks)) {
#ifdef DEBUG
		unsigned uiTxLength = 0;
		const CChainedConstChunks* pNextTxChunk = m_pTxChunks;
		while (pNextTxChunk) {
			ASSERTE(pNextTxChunk->pData);
			ASSERTE(pNextTxChunk->uiLength);
			uiTxLength += pNextTxChunk->uiLength;
			pNextTxChunk = pNextTxChunk->GetNextItem();
		}
		I2C_DEBUG_INFO("Slave has more data to send. Length:", uiTxLength);
		const CChainedIoChunks* pNextRxChunk = m_pRxChunks;
		while (pNextRxChunk) {
			ASSERTE(pNextRxChunk->pData);
			ASSERTE(pNextRxChunk->uiLength);
			pNextRxChunk = pNextRxChunk->GetNextItem();
		}
#endif
		UpdateTxMode();
	} else {
		m_pTxChunks = NULL;
	}
}

void CHW_I2Cx_Slave::PrepareSlaveWriteSession(ISlaveI2C* pDeviceI2C)
{
	ASSERTE(m_eBusState == BUSSTATE_SLAVE1 || m_eBusState == BUSSTATE_SLAVE2);
	ASSERTE(m_uiTxChunkBytesSend == 0);
	ASSERTE(m_uiRxChunkBytesReceived == 0);
	IF_STM32F4(ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)));
	IF_STM32F4(ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF)));
//	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));

	if (pDeviceI2C->GetBufferToReceive(&m_pRxChunks)) {
#ifdef DEBUG
		const CChainedConstChunks* pNextTxChunk = m_pTxChunks;
		while (pNextTxChunk) {
			ASSERTE(pNextTxChunk->pData);
			ASSERTE(pNextTxChunk->uiLength);
			pNextTxChunk = pNextTxChunk->GetNextItem();
		}
		unsigned uiRxLength = 0;
		const CChainedIoChunks* pNextRxChunk = m_pRxChunks;
		while (pNextRxChunk) {
			ASSERTE(pNextRxChunk->pData);
			ASSERTE(pNextRxChunk->uiLength);
			uiRxLength += pNextRxChunk->uiLength;
			pNextRxChunk = pNextRxChunk->GetNextItem();
		}
		I2C_DEBUG_INFO("Slave is ready to receive more data. Capacity:", uiRxLength);
#endif
		UpdateRxMode();
	} else {
		m_pRxChunks = NULL;
		m_uiRxTotalBytesLeft = 0;
	}
}

void CHW_I2Cx_Slave::Init_Slave(unsigned uiClockSpeed, ISlaveI2C* pSlaveI2CDevice1 /*=NULL*/, ISlaveI2C* pSlaveI2CDevice2 /*=NULL*/)
{
	m_pSlaveI2CDevice1 = pSlaveI2CDevice1;
	m_pSlaveI2CDevice2 = pSlaveI2CDevice2;
	m_eSlaveState = SLAVESTATE_IDLE;

}

/****************************************************************************/

void CHW_I2Cx_Master::PrepareMasterSession()
{
	IF_STM32F0(ASSERTE(m_eBusState == BUSSTATE_IDLE));
	IF_STM32F4(ASSERTE(m_eBusState == BUSSTATE_MASTER));
	ASSERTE(m_eMasterState == FSMSTATE_START);
	ASSERTE(!m_bStartDiscarded);
	ASSERTE(m_pCurrentI2CDevice);
	ASSERTE(m_uiTxTotalBytesLeft == 0);
	ASSERTE(m_uiTxChunkBytesSend == 0);
	ASSERTE(m_uiRxTotalBytesLeft == 0);
	ASSERTE(m_uiRxChunkBytesReceived == 0);
	IF_STM32F4(ASSERTE(I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)));
	IF_STM32F4(ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF)));
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));

	m_pCurrentI2CDevice->GetI2CPacket(&m_pTxChunks, &m_pRxChunks);
#ifdef DEBUG
	const CChainedConstChunks* pNextTxChunk = m_pTxChunks;
	while (pNextTxChunk) {
		ASSERTE(pNextTxChunk->pData);
		ASSERTE(pNextTxChunk->uiLength);
		pNextTxChunk = pNextTxChunk->GetNextItem();
	}
	const CChainedIoChunks* pNextRxChunk = m_pRxChunks;
	while (pNextRxChunk) {
		ASSERTE(pNextRxChunk->pData);
		ASSERTE(pNextRxChunk->uiLength);
		pNextRxChunk = pNextRxChunk->GetNextItem();
	}
#endif
	m_uiRxTotalBytesLeft = m_pRxChunks ? CollectChainTotalLength(m_pRxChunks) : 0;
	m_uiTxTotalBytesLeft = m_pTxChunks ? CollectChainTotalLength(m_pTxChunks) : 0;

	UpdateTxMode();
	UpdateRxMode();
}

void CHW_I2Cx_Master::DoStartMasterSession()
{
	IF_STM32F4(ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)));
	IF_STM32F4(ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF)));
	ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));
	I2C_DEBUG_INFO("Start. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
	DoEnableEventInterrupt();
#ifdef STM32F4
	I2C_GenerateSTART(I2Cx, ENABLE);
#endif
#ifdef STM32F0
	PrepareMasterSession();
	if (m_pTxChunks) { // for master do TX before RX
		DoSendAddressForTx(m_pCurrentI2CDevice->GetI2CAddress());
	} else if (m_pRxChunks) {
		DoSendAddressForRx(m_pCurrentI2CDevice->GetI2CAddress());
	} else { // check
		DoSendAddressForCheck(m_pCurrentI2CDevice->GetI2CAddress());
	}
#endif
	// continue processing in CHW_I2Cx::OnI2CxEvent interrupt handler
}

void CHW_I2Cx_Master::DoStartAgainMasterSession()
{
#ifdef STM32F4
	I2C_GenerateSTART(I2Cx, ENABLE);
#endif
#ifdef STM32F0
	if (m_pRxChunks) {
		DoSendAddressForRx(m_pCurrentI2CDevice->GetI2CAddress());
	} else {
		DoSendAddressForTx(m_pCurrentI2CDevice->GetI2CAddress());
	}
#endif
}
/*void CHW_I2Cx::DoStopSession()
{
	DoDisableTxDMA();
	DoDisableRxDMA();
	I2C_ITConfig(I2Cx, I2C_IT_EVT | I2C_IT_ERR, DISABLE);
}*/

void CHW_I2Cx_Master::NotifyMasterPktDone(IDeviceI2C* pDeviceToNotify)
{
	ASSERTE(pDeviceToNotify);
	pDeviceToNotify->OnI2CDone();
}

void CHW_I2Cx_Master::NotifyMasterPktError(ErrorType e, IDeviceI2C* pDeviceToNotify)
{
	ASSERTE(pDeviceToNotify);
	pDeviceToNotify->OnI2CError(e);
}

void CHW_I2Cx_Master::NotifyMasterI2CIdle()
{
	IDeviceI2C* pDeviceI2C;
	if (m_qIdleNotifyQueue.Pop(pDeviceI2C)) {
		pDeviceI2C->OnI2CIdle();
		I2C_DEBUG_INFO("idle callback :", pDeviceI2C->GetI2CAddress());
	}
}

void CHW_I2Cx_Master::Init_Master(unsigned uiClockSpeed)
{
	m_eMasterState = FSMSTATE_IDLE;
	m_pCurrentI2CDevice = NULL;
}

bool CHW_I2Cx_Master::PopNextMasterSession()
{
	IDeviceI2C* pI2CDevice;
	if (!m_qSessionsQueue.Pop(pI2CDevice)) {
		I2C_DEBUG_INFO("Session queue empty");
		return false;
	}
	ASSERTE(pI2CDevice);
	if (AtomicCmpSet(&m_pCurrentI2CDevice, (IDeviceI2C*)NULL, pI2CDevice)) {
		I2C_DEBUG_INFO("Session queue pop:", pI2CDevice->GetI2CAddress());
		ASSERTE(m_pCurrentI2CDevice);
		return true;
	}
	ASSERT("unexpected m_pCurrentI2CDevice changed");
	// push device back to queue
	m_qSessionsQueue.Push(pI2CDevice);
	return false;
}

/****************************************************************************/

bool CHW_I2Cx::Queue_I2C_Session(IDeviceI2C* pDeviceI2C)
{
	ASSERTE(pDeviceI2C);
	bool bResult = true;
	if (ProcessBusInitIfNecessary() && AtomicCmpSet(&m_pCurrentI2CDevice, (IDeviceI2C*)NULL, pDeviceI2C)) {
		m_bStartDiscarded = false;
		CHANGE_MASTER_STATE(FSMSTATE_START, I2C_TIMEOUT_START);
		DoStartMasterSession();
	} else {
		I2C_DEBUG_INFO("session queue push:", pDeviceI2C->GetI2CAddress());
		bResult = m_qSessionsQueue.Push(pDeviceI2C);
		ASSERTE(bResult)
	}
	return bResult;
}

void CHW_I2Cx::Cancel_I2C_Session(IDeviceI2C* pDeviceI2C)
{
	ASSERTE(pDeviceI2C);
	bool bAborted = false;
	if (AtomicCmpSet(&m_pCurrentI2CDevice, pDeviceI2C, (IDeviceI2C*)NULL)) {
		if (m_eBusState == BUSSTATE_MASTER) {
			ProcessAbortSession();
			bAborted = true;
		}
	} else {
		for (unsigned n = 0; n < MAX_I2C_DEVICES; ++n) {
			IDeviceI2C* pQueuedDeviceI2C;
			if (m_qSessionsQueue.Pop(pQueuedDeviceI2C)) {
				if (pQueuedDeviceI2C != pDeviceI2C) {
					I2C_DEBUG_INFO("session queue re-push:", pQueuedDeviceI2C->GetI2CAddress());
					m_qSessionsQueue.Push(pQueuedDeviceI2C);
				} else {
					I2C_DEBUG_INFO("session queue pop (abort):", pQueuedDeviceI2C->GetI2CAddress());
					bAborted = true;
					break;
				}
			}
		}
	}
	if (bAborted) {
		NotifyMasterPktError(ErrorType::I2C_ABORT, pDeviceI2C);
	}
}

void CHW_I2Cx::RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C)
{
	bool bResult = m_qIdleNotifyQueue.Push(pDeviceI2C);
	ASSERTE(bResult);
	if (bResult && m_eBusState == BUSSTATE_IDLE) {
		NotifyMasterI2CIdle();
	}
}

void CHW_I2Cx::OnSysTick()
{
	IMPLEMENTS_INTERFACE_METHOD(ISysTickNotifee::OnSysTick());

	bool bTimeoutTrigger = (m_nTimeoutCountdown == 1);

	if (m_nTimeoutCountdown) {
		--m_nTimeoutCountdown;
	}

	switch (m_eBusState) {
		case BUSSTATE_DISABLED:
			break;
		case BUSSTATE_INIT:
			m_bReinitRequired = false;
			if (ProcessBusInitIfNecessary()) {
				ProcessQueuedSessions();
			}
			break;
		case BUSSTATE_IDLE:
			if (m_eMasterState == FSMSTATE_STOP && !ProcessBusStopIfNecessary() && bTimeoutTrigger) {
				DEBUG_INFO("I2C:Error timeout. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
				ProcessAbortMasterSession(ErrorType::I2C_ERROR_TIMEOUT);
			} else if (m_bReinitRequired) {
				DEBUG_INFO("I2C:Reinit required");
				CHECKED_CHANGE_BUS_STATE(BUSSTATE_IDLE, BUSSTATE_INIT, 0);
			}
			break;
		case BUSSTATE_SLAVE1:
			if (bTimeoutTrigger && m_pSlaveI2CDevice1) {
				DEBUG_INFO("I2C:Error timeout. Slave 1 addr:", m_pSlaveI2CDevice1->GetI2CAddress());
				ProcessAbortSlave1Session(ErrorType::I2C_ERROR_TIMEOUT);
			}
			break;
		case BUSSTATE_SLAVE2:
			if (bTimeoutTrigger && m_pSlaveI2CDevice2) {
				DEBUG_INFO("I2C:Error timeout. Slave 2 addr:", m_pSlaveI2CDevice2->GetI2CAddress());
				ProcessAbortSlave2Session(ErrorType::I2C_ERROR_TIMEOUT);
			}
			break;
		case BUSSTATE_MASTER:
			// do nothing
			if (bTimeoutTrigger) {
				I2C_DEBUG_INFO("Error timeout. e:", m_eMasterState);
				ProcessAbortMasterSession(I2C_ERROR_TIMEOUT);
			}
			break;
		case BUSSTATE_RECOVER: {
			INIT_GPIO_OUT_OD_PU(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT), 1);
			INIT_GPIO_OUT_OD_PU(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT), 1);

			EBusError eError = DoCheckBusPinsState();
			switch (eError) {
				case BUS_OK:
					m_nBusRecoverValidCounts++;
					if (m_nBusRecoverValidCounts > I2C_BUS_RECOVER_VALIDATE  ) {
						CHECKED_CHANGE_BUS_STATE(BUSSTATE_RECOVER, BUSSTATE_INIT, 0);
						m_bReinitRequired = false;
						if (ProcessBusInitIfNecessary()) {
							ProcessQueuedSessions();
						}
					}
					break;
				case BUSERROR_LOWSCL:
					// SCL LOW. slave stuck. blink SDA to try unstuck
					if (m_eBusError != eError) {
						I2C_DEBUG_INFO("SCL LOW");
					}
					if (bTimeoutTrigger) {
						INIT_GPIO_OUT_OD_PU(FIRST(I2Cx_SDA_GPIO_PORT), SECOND(I2Cx_SDA_GPIO_PORT), 0);
						m_nTimeoutCountdown = 500;
						I2C_DEBUG_INFO("Blinking by SDA for recover");
					}
					m_nBusRecoverValidCounts = 0;
					break;
				case BUSERROR_LOWSDA:
					// SDA LOW. slave stuck. blink SCL to try unstuck
					if (m_eBusError != eError) {
						I2C_DEBUG_INFO("SDA LOW");
					}
					if (bTimeoutTrigger) {
						INIT_GPIO_OUT_OD_PU(FIRST(I2Cx_SCL_GPIO_PORT), SECOND(I2Cx_SCL_GPIO_PORT), 0);
						I2C_DEBUG_INFO("Blinking by SCL for recover");
						m_nTimeoutCountdown = 500;
					}
					m_nBusRecoverValidCounts = 0;
					break;
				case BUSERROR_LOWBOTH:
					if (m_eBusError != eError) {
						I2C_DEBUG_INFO("SCL LOW and SDA LOW");
					}
					m_nBusRecoverValidCounts = 0;
					break;
			}
			m_eBusError = eError;
		} break;
	}
}

void CHW_I2Cx::OnI2CxEvent()
{
	uint32_t uiEvent = IF_STM32F4(I2C_GetLastEvent(I2Cx)) IF_STM32F0(I2Cx->ISR);
	do {
		IS_EVENT_HANDLER;
		I2C_DEBUG_INFO("Event IRQ: event:", uiEvent);
		switch (m_eBusState) {
			case BUSSTATE_DISABLED:
			case BUSSTATE_INIT:
				break;
			case BUSSTATE_IDLE:
				ProcessI2CIdleEvent(uiEvent);
				MARK_EVENT_IS_HANDLED;
				break;
			case BUSSTATE_SLAVE1:
				ProcessI2CSlave1Event(uiEvent);
				MARK_EVENT_IS_HANDLED;
				break;
			case BUSSTATE_SLAVE2:
				ProcessI2CSlave2Event(uiEvent);
				MARK_EVENT_IS_HANDLED;
				break;
			case BUSSTATE_MASTER:
				ProcessI2CMasterEvent(uiEvent);
				MARK_EVENT_IS_HANDLED;
				break;
			case BUSSTATE_RECOVER:
				break;
		}
		ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C event");
		uiEvent = IF_STM32F4(I2C_GetLastEvent(I2Cx)) IF_STM32F0(I2C_ReadRegister(I2Cx, I2C_Register_ISR)); // next event
	} while ((uiEvent & I2C_FLAG_ADDR & I2C_FLAG_MASK) != 0);
	if (m_eBusState == BUSSTATE_IDLE) {
		ProcessQueuedSessions();
	}
	I2C_DEBUG_INFO("Event IRQ finished. " IF_STM32F4("SR1:", I2Cx->SR1) IF_STM32F0("ISR:", I2Cx->ISR));
}

void CHW_I2Cx::ProcessI2CIdleEvent(uint32_t uiEvent)
{
	IS_EVENT_HANDLER;
	I2C_DEBUG_INFO("Idle event:", uiEvent);
#ifdef STM32F4
	if (uiEvent & (I2C_FLAG_SB & I2C_FLAG_MASK)) {
		uiEvent &= ~((I2C_FLAG_SB | I2C_FLAG_MSL |I2C_FLAG_BUSY) & I2C_FLAG_MASK);
		// master start done
		if (!m_bStartDiscarded) {
			ASSERTE(m_eMasterState == FSMSTATE_START);
			ASSERTE(m_uiTxChunkBytesSend == 0);
			CHECKED_CHANGE_BUS_STATE(BUSSTATE_IDLE, BUSSTATE_MASTER, 0);
			PrepareMasterSession();
			if (m_pTxChunks) { // for master do TX before RX
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_START, FSMSTATE_TXADDRESS, I2C_TIMEOUT_ADDRESS);
				DoSendAddressForTx(m_pCurrentI2CDevice->GetI2CAddress());
			} else if (m_pRxChunks) {
				ASSERTE(m_uiRxChunkBytesReceived == 0);
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_START, FSMSTATE_RXADDRESS, I2C_TIMEOUT_ADDRESS);
				DoSendAddressForRx(m_pCurrentI2CDevice->GetI2CAddress());
			} else { // check
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_START, FSMSTATE_TXADDRESS, I2C_TIMEOUT_ADDRESS);
				DoSendAddressForCheck(m_pCurrentI2CDevice->GetI2CAddress());
			}
		} else {
			m_bStartDiscarded = false;
			I2C_DEBUG_INFO("Generate STOP");
			I2C_GenerateSTOP(I2Cx, ENABLE);       // Set STOP high
			CHECKED_CHANGE_MASTER_STATE(FSMSTATE_START, FSMSTATE_STOP, 0);
		}
		__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_SB status cleared and isr not trigger again
		MARK_EVENT_IS_HANDLED;
	}
	if (uiEvent & (I2C_FLAG_ADDR & I2C_FLAG_MASK)) {
		//slave mode
		uiEvent &= ~((I2C_FLAG_ADDR | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
		if (uiEvent & (I2C_FLAG_TRA & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_TRA | I2C_FLAG_TXE) & I2C_FLAG_MASK);
			ProcessMasterDiscardStartIfNecessary();
			ProcessSlave1TxAddress();
		} else {
			ProcessMasterDiscardStartIfNecessary();
			ProcessSlave1RxAddress();
		}
		MARK_EVENT_IS_HANDLED;
	}
	if (uiEvent & (I2C_FLAG_DUALF & I2C_FLAG_MASK)) {
		uiEvent &= ~((I2C_FLAG_DUALF | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
		if (uiEvent & (I2C_FLAG_TRA & I2C_FLAG_MASK)) {
			uiEvent &= ~(I2C_FLAG_TRA & I2C_FLAG_MASK);
			ProcessMasterDiscardStartIfNecessary();
			ProcessSlave2TxAddress();
			MARK_EVENT_IS_HANDLED;
		} else {
			ProcessMasterDiscardStartIfNecessary();
			ProcessSlave2RxAddress();
		}
		MARK_EVENT_IS_HANDLED;
	}
	if (uiEvent & (I2C_FLAG_GENCALL & I2C_FLAG_MASK)) {
		uiEvent &= ~((I2C_FLAG_GENCALL | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
		ProcessMasterDiscardStartIfNecessary();
//		if (m_pSlaveI2CDevice1) ProcessSlave1GeneralAddress();
//		if (m_pSlaveI2CDevice2) ProcessSlave2GeneralAddress();
		MARK_EVENT_IS_HANDLED;
	}
#endif
#ifdef STM32F0
	bool bStopProcessed = ProcessBusStopIfNecessary();
	if (bStopProcessed) {
		uiEvent &= ~((I2C_FLAG_STOPF | I2C_FLAG_TXE) & I2C_FLAG_MASK);
	}
	if (uiEvent & (I2C_FLAG_ADDR & I2C_FLAG_MASK)) {
		//slave mode
		uiEvent &= ~((I2C_FLAG_ADDR | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
		uint8_t uiSlaveAddr = I2C_GetAddressMatched(I2Cx);
		bool bRx = uiEvent & I2C_ISR_DIR;
		ProcessMasterDiscardStartIfNecessary();
		if (uiSlaveAddr == m_pSlaveI2CDevice1->GetI2CAddress()) {
			if (bRx) {
				ProcessSlave1RxAddress();
			} else {
				ProcessSlave1TxAddress();
			}
		} else {
			ASSERTE(m_pSlaveI2CDevice2);
			ASSERTE(uiSlaveAddr == m_pSlaveI2CDevice2->GetI2CAddress());
			if (bRx) {
				ProcessSlave2RxAddress();
			} else {
				ProcessSlave2TxAddress();
			}
		}
		MARK_EVENT_IS_HANDLED;
	} else if (!bStopProcessed) {
		// master start done
		if (!m_bStartDiscarded) {
			ASSERTE(m_eMasterState == FSMSTATE_START);
			ASSERTE(m_uiTxChunkBytesSend == 0);
			CHECKED_CHANGE_BUS_STATE(BUSSTATE_IDLE, BUSSTATE_MASTER, 0);
			if (m_pTxChunks) { // for master do TX before RX
				ProcessMasterTxProcedureFirst(); // -> FSMSTATE_DATA_TX_DMA or FSMSTATE_DATA_TX_N
			} else if (m_pRxChunks) {
				ASSERTE(m_uiRxChunkBytesReceived == 0);
				ProcessMasterRxProcedure(); // -> FSMSTATE_DATA_RX_DMA or FSMSTATE_DATA_RX_SINGLE or FSMSTATE_DATA_RX_2 or FSMSTATE_DATA_RX_N
			} else {
				// nothing to TX/RX - it was only check connection to the address - going to STOP?
				ProcessMasterStop();
			}
			uiEvent = 0;
		} else {
			m_bStartDiscarded = false;
			I2C_GenerateSTOP(I2Cx, ENABLE);       // Set STOP high
			CHECKED_CHANGE_MASTER_STATE(FSMSTATE_START, FSMSTATE_STOP, 0);
		}
		__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_SB status cleared and isr not trigger again
		MARK_EVENT_IS_HANDLED;
	} else {
		MARK_EVENT_IS_HANDLED;
	}
#endif // STM32F0

	if (uiEvent) {
		ASSERT("invalid idle event not handled");
		ProcessAbortBusSession(ErrorType::I2C_ABORT);
	}
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C idle event");
}

void CHW_I2Cx::ProcessI2CSlave1Event(uint32_t uiEvent)
{
	IS_EVENT_HANDLER;

	I2C_DEBUG_INFO("Slave 1 event:", uiEvent);

	do {
#ifdef STM32F4
		if (uiEvent & (I2C_FLAG_RXNE & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_RXNE | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
			ProcessSlaveRx(m_pSlaveI2CDevice1);
			if (uiEvent & (I2C_FLAG_BTF & I2C_FLAG_MASK)) {
				uiEvent &= ~(I2C_FLAG_BTF & I2C_FLAG_MASK);
				ProcessSlaveRx(m_pSlaveI2CDevice1);
			}
			MARK_EVENT_IS_HANDLED;
		}

		if (uiEvent & (I2C_FLAG_AF & I2C_FLAG_MASK)) {
			uiEvent &= ~(I2C_FLAG_AF & I2C_FLAG_MASK);
			I2C_ClearFlag(I2Cx, I2C_FLAG_AF);
			NVIC_ClearPendingIRQ(I2Cx_IT_ERR_IRQn);
			I2C_DEBUG_INFO("Slave 1 got NO Acknowledge. Transmission done.");
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice1);
			MARK_EVENT_IS_HANDLED;
			I2C_DEBUG_INFO("Event IRQ finished. SR1:", I2Cx->SR1);
		}

		if (uiEvent & (I2C_FLAG_ADDR & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_ADDR | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
			if (uiEvent & (I2C_FLAG_TRA & I2C_FLAG_MASK)) {
				uiEvent &= ~(I2C_FLAG_TRA & I2C_FLAG_MASK);
				//clear ADDR flag by read both SR1 and SR2. (cleared by software sequence). (in fact it was already done by I2C_GetLastEvent)
				ProcessSlave1TxAddress();
				MARK_EVENT_IS_HANDLED;
			} else {
				//clear ADDR flag by read both SR1 and SR2. (cleared by software sequence). it was already done by I2C_GetLastEvent
				ProcessSlave1RxAddress();
				MARK_EVENT_IS_HANDLED;
			}
		}

		if (uiEvent & (I2C_FLAG_DUALF & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_DUALF | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
			if (uiEvent & (I2C_FLAG_TRA & I2C_FLAG_MASK)) {
				uiEvent &= ~(I2C_FLAG_TRA & I2C_FLAG_MASK);
				FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice1);
				ProcessSlave2TxAddress();
				MARK_EVENT_IS_HANDLED;
			} else {
				FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice1);
				ProcessSlave2RxAddress();
				MARK_EVENT_IS_HANDLED;
			}
		}

		if (uiEvent & (I2C_FLAG_GENCALL & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_GENCALL | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice1);
			MARK_EVENT_IS_HANDLED;
		}

		if (uiEvent & (I2C_FLAG_TXE & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_TXE | I2C_FLAG_BUSY | I2C_FLAG_TRA) & I2C_FLAG_MASK);
			ProcessSlaveTx(m_pSlaveI2CDevice1);
			if (uiEvent & (I2C_FLAG_BTF & I2C_FLAG_MASK)) {
				uiEvent &= ~(I2C_FLAG_BTF & I2C_FLAG_MASK);
				ProcessSlaveTx(m_pSlaveI2CDevice1);
			}
			MARK_EVENT_IS_HANDLED;
		}

		if (uiEvent & (I2C_FLAG_STOPF & I2C_FLAG_MASK)) {
			uiEvent &= ~((I2C_FLAG_STOPF | I2C_FLAG_BUSY) & I2C_FLAG_MASK);
			I2C_Cmd(I2Cx, ENABLE); // clear STOPF bit by write to CR1
			I2C_DEBUG_INFO("Slave 1 STOP");
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice1);
			MARK_EVENT_IS_HANDLED;
		}
#endif //STM32F4
	} while(false);

	if (uiEvent) {
		ASSERT("invalid slave event not handled");
		ProcessAbortBusSession(ErrorType::I2C_ABORT);
	}
	__DSB(); // Data Synchronization Barrier. Ensure I2C_CR2_ITBUFEN status cleared and isr not trigger again
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C slave 1 event");
}

void CHW_I2Cx::ProcessI2CSlave2Event(uint32_t uiEvent)
{
	IS_EVENT_HANDLER;

	I2C_DEBUG_INFO("Slave 2 event:", uiEvent);
#ifdef STM32F4
	//TODO change feom event based to flags based, like for slave1
	switch (uiEvent) {
		case I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED:
//			//clear ADDR flag by read both SR1 and SR2. (cleared by software sequence). it was already done by I2C_GetLastEvent
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
			ProcessSlave1RxAddress();
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_RECEIVER_SECONDADDRESS_MATCHED:
			ProcessSlave2RxAddress();
			MARK_EVENT_IS_HANDLED;
			break;
			//continue without break
		case I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED:
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
			ProcessSlave1TxAddress();
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED:
			ProcessSlave2TxAddress();
			MARK_EVENT_IS_HANDLED;
			break;
//		case I2C_EVENT_SLAVE_GENERALCALLADDRESS_MATCHED:
//			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
//			MARK_EVENT_IS_HANDLED;
//			break;
		case (I2C_FLAG_RXNE & I2C_FLAG_MASK):
		case I2C_EVENT_SLAVE_BYTE_RECEIVED:
		case I2C_EVENT_SLAVE_BYTE_RECEIVED | (I2C_FLAG_BTF & I2C_FLAG_MASK):
			ProcessSlaveRx(m_pSlaveI2CDevice2);
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_BYTE_RECEIVED | I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED:
			ProcessSlaveRx(m_pSlaveI2CDevice2);
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
			ProcessSlave1TxAddress();
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_BYTE_RECEIVED | I2C_EVENT_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED:
			ProcessSlaveRx(m_pSlaveI2CDevice2);
			ProcessSlave2TxAddress();
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_BYTE_TRANSMITTED:
		case I2C_EVENT_SLAVE_BYTE_TRANSMITTING:
			ProcessSlaveTx(m_pSlaveI2CDevice2);
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_ACK_FAILURE:
		case I2C_EVENT_SLAVE_ACK_FAILURE | I2C_EVENT_SLAVE_BYTE_TRANSMITTED:
		case I2C_EVENT_SLAVE_ACK_FAILURE | I2C_EVENT_SLAVE_BYTE_TRANSMITTING:
			I2C_DEBUG_INFO("Slave 2 got NO Acknowledge. Transmission done.");
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
			MARK_EVENT_IS_HANDLED;
			break;
		case I2C_EVENT_SLAVE_STOP_DETECTED | I2C_EVENT_SLAVE_BYTE_RECEIVED:
		case I2C_EVENT_SLAVE_STOP_DETECTED | (I2C_FLAG_RXNE & I2C_FLAG_MASK):
			ProcessSlaveRx(m_pSlaveI2CDevice2);
			//continue without break
		case I2C_EVENT_SLAVE_STOP_DETECTED:
		case I2C_EVENT_SLAVE_STOP_DETECTED | (I2C_FLAG_BUSY & I2C_FLAG_MASK):
		case I2C_EVENT_SLAVE_STOP_DETECTED | (I2C_FLAG_BUSY & I2C_FLAG_MASK) | (I2C_FLAG_ADDR & I2C_FLAG_MASK):
			I2C_Cmd(I2Cx, ENABLE); // clear STOPF bit by write to CR1
			I2C_DEBUG_INFO("Slave 2 STOP");
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
			MARK_EVENT_IS_HANDLED;
			break;
		default:
			ASSERT("invalid slave event");
			ProcessAbortBusSession(ErrorType::I2C_ABORT);
			break;
	}
	__DSB(); // Data Synchronization Barrier. Ensure I2C_CR2_ITBUFEN status cleared and isr not trigger again
#endif // STM32F4
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C slave 2 event");
}

void CHW_I2Cx::FinalizeCurrentSlaveOperation(ISlaveI2C* pSlaveDevice)
{
	if (pSlaveDevice) {
		switch (m_eSlaveState) {
			case SLAVESTATE_IDLE:
				break;
			case SLAVESTATE_RX:
				pSlaveDevice->OnI2CSessionDone(m_uiRxChunkBytesReceived);
				break;
			case SLAVESTATE_RX_DMA:
#if defined(I2Cx_DMA_RX)
				pSlaveDevice->OnI2CSessionDone(GetDMARxBytes());
#endif
				break;
			case SLAVESTATE_TX:
				pSlaveDevice->OnI2CSessionDone(m_uiTxChunkBytesSend);
				break;
			case SLAVESTATE_TX_DMA:
#if defined(I2Cx_DMA_TX)
				pSlaveDevice->OnI2CSessionDone(GetDMATxBytes());
#endif
				break;
		}
		m_pTxChunks = NULL;
		m_pRxChunks = NULL;
		m_uiTxTotalBytesLeft = 0;
		m_uiTxChunkBytesSend = 0;
		m_uiRxTotalBytesLeft = 0;
		m_uiRxChunkBytesReceived = 0;
	}
	CHANGE_SLAVE_STATE(SLAVESTATE_IDLE, 0);
	ProcessBusFinalize(); // -> BUSSTATE_IDLE or BUSSTATE_MASTER
}

void CHW_I2Cx::ProcessI2CMasterEvent(uint32_t uiEvent)
{
	IS_EVENT_HANDLER;
	I2C_DEBUG_INFO("Master event:", uiEvent);
	switch (m_eMasterState) {
		case FSMSTATE_IDLE:
			break;
		case FSMSTATE_START:
			ASSERT("handled in idle bus state");
			break;
#ifdef STM32F4
		case FSMSTATE_TXADDRESS:
			if (uiEvent & (I2C_FLAG_ADDR & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: ADDR (Tx). " IF_STM32F4("SR1:", I2Cx->SR1) IF_STM32F0("ISR:", I2Cx->ISR));
				DoClearAddrFlag();
				if (m_pTxChunks) {
					ProcessMasterTxProcedureFirst(); // -> FSMSTATE_DATA_TX_DMA or FSMSTATE_DATA_TX_N
				} else {
					// nothing to TX - it was only check connection to the address - going to STOP
					ProcessMasterStop();
				}
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_TXADDRESS");
			break;
#endif
		case FSMSTATE_DATA_TX_N:
			if (uiEvent & (I2C_FLAG_TXE & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: TxE");
				ProcessMasterTxProcedureNext(); // -> FSMSTATE_DATA_TX_DMA or FSMSTATE_DATA_TX_N or FSMSTATE_DATA_TX_DONE
				__DSB(); // Data Synchronization Barrier. Ensure I2C_CR2_ITBUFEN status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
			}
#ifdef STM32F0
			if (uiEvent & (I2C_FLAG_TCR & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: TX TCR :", m_uiTxTotalBytesLeft);
				if (m_pTxChunks) {
					ASSERTE(m_uiTxTotalBytesLeft);
					uint16_t uiAddress = m_pCurrentI2CDevice->GetI2CAddress();
					unsigned uiLength = m_uiTxTotalBytesLeft < MAX_F0NBYTES ? m_uiTxTotalBytesLeft : MAX_F0NBYTES;
					I2C_TransferHandling(I2Cx, uiAddress << 1, uiLength, I2C_Reload_Mode, I2C_No_StartStop);
//					I2C_NumberOfBytesConfig(I2Cx, uiLength);
					I2C_DEBUG_INFO("Next TX NBYTES:", uiLength);
				} else {
					DoDisableBufferInterrupt();
					CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DONE, I2C_TIMEOUT_STOP);
				}
				MARK_EVENT_IS_HANDLED;
			}
			if (uiEvent & (I2C_FLAG_NACKF & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: TX NACKF Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
				ProcessAbortBusSession(ErrorType::I2C_ERROR_ACK);
				__DSB(); // Data Synchronization Barrier. Ensure I2C_CR2_ITBUFEN status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
			} else
#endif
			if (uiEvent & (I2C_FLAG_STOPF & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: TX STOPF");
				DoClearStopFlag();
				CHANGE_MASTER_STATE(FSMSTATE_IDLE, 0);
				IDeviceI2C* pDeviceI2C = NULL;
				AtomicExchange(&m_pCurrentI2CDevice, &pDeviceI2C);
				NotifyMasterPktDone(pDeviceI2C);
				ProcessBusFinalize(); // -> BUSSTATE_IDLE or BUSSTATE_MASTER
				__DSB(); // Data Synchronization Barrier. Ensure I2C_CR2_ITBUFEN status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_DATA_TX_N");
			break;
		case FSMSTATE_DATA_TX_DONE:
#ifdef STM32F4
			if (uiEvent & (I2C_FLAG_BTF & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: BTF");
				I2C_ReceiveData(I2Cx); // clear BTF
				if (m_pRxChunks) {
					DoStartAgainMasterSession();
					CHECKED_CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DONE, FSMSTATE_START_AGAIN, I2C_TIMEOUT_START);
					I2C_DEBUG_INFO("Going to start again. Event:", I2C_GetLastEvent(I2Cx));
				} else {
					ProcessMasterStop();
				}
				__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_BTF status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
			}
#endif
#ifdef STM32F0
			if (m_pRxChunks) {
				DoStartAgainMasterSession();
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DONE, FSMSTATE_START_AGAIN, I2C_TIMEOUT_START);
				I2C_DEBUG_INFO("Going to start again. Event:", I2Cx->ISR);
			} else {
				ProcessMasterStop();
			}
			MARK_EVENT_IS_HANDLED;
#endif
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_DATA_TX_DONE");
			break;
		case FSMSTATE_START_AGAIN:
#ifdef STM32F0
			ASSERTE(!(uiEvent & (I2C_FLAG_ADDR & I2C_FLAG_MASK)));
			ProcessMasterRxProcedure(); // -> FSMSTATE_DATA_RX_DMA or FSMSTATE_DATA_RX_N
			MARK_EVENT_IS_HANDLED;
#endif
#ifdef STM32F4
			if (uiEvent & (I2C_FLAG_SB & I2C_FLAG_MASK)) { // start done
				I2C_DEBUG_INFO("Event IRQ: SB:");
				ASSERTE(!m_pTxChunks);
				ASSERTE(m_pRxChunks);
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_START_AGAIN, FSMSTATE_RXADDRESS, I2C_TIMEOUT_ADDRESS);
				DoSendAddressForRx(m_pCurrentI2CDevice->GetI2CAddress());
				__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_SB status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
			} else if (uiEvent & (I2C_FLAG_BTF & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: BTF");
				I2C_ReceiveData(I2Cx); // clear BTF
				__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_BTF status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_START_AGAIN");
			break;
		case FSMSTATE_RXADDRESS: // Wait until ADDR = 1 (SCL stretched low until the ADDR flag is cleared)
			if (uiEvent & (I2C_FLAG_ADDR & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: ADDR (Rx)");
				ASSERTE(m_pRxChunks);
				ASSERTE(m_pRxChunks->uiLength);
				ASSERTE(!m_pTxChunks);
				ProcessMasterRxProcedure(); // -> FSMSTATE_DATA_RX_DMA or FSMSTATE_DATA_RX_SINGLE or FSMSTATE_DATA_RX_2 or FSMSTATE_DATA_RX_N
				__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_ADDR status cleared and isr not trigger again
				MARK_EVENT_IS_HANDLED;
//			} else if (I2C_ReadRegister(I2Cx, I2C_Register_SR1) == 0x00) {
//				DEBUG_INFO("I2C:IRQ Event ignored (BUSY and MSL) while FSMSTATE_RXADDRESS. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
//				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_RXADDRESS");
			break;
#endif
		case FSMSTATE_DATA_RX_N:
			if (uiEvent & (I2C_FLAG_RXNE & I2C_FLAG_MASK)) {
				ASSERTE(m_pRxChunks);
				ASSERTE(m_pRxChunks->uiLength > m_uiRxChunkBytesReceived);
				I2C_DEBUG_INFO("Event IRQ: RxNE");
#ifdef STM32F4
				ASSERTE(m_uiRxTotalBytesLeft > 2);
				if (m_uiRxTotalBytesLeft == 3) {
					// three last bytes sequence
					DoDisableBufferInterrupt();
					CHECKED_CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_N, FSMSTATE_DATA_RX_3, I2C_TIMEOUT_RX(3));
					__DSB(); // Data Synchronization Barrier. Ensure I2C_IT_BUF cleared and isr not trigger again
				} else
#endif // STM32F4
				{
					I2C_DEBUG_INFO("non DMA RX next byte:",
							((m_pRxChunks->uiLength - m_uiRxChunkBytesReceived) << 16) |
							*((const uint8_t*)m_pRxChunks->pData + m_uiRxChunkBytesReceived));
					DoRXByte();
#ifdef STM32F0
					if (!m_pRxChunks) {
						ASSERTE(m_uiRxTotalBytesLeft == 0);
						ProcessMasterStop();
						MARK_EVENT_IS_HANDLED;
					}
#endif
#if defined(I2Cx_DMA_RX)
					if (m_bUseRxDma) {
						DoDisableBufferInterrupt();
						DoPrepareRXDMA();
						DoEnableRxDMA();
						// DMA already configured, just wait DMA TC event;
						CHECKED_CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_N, FSMSTATE_DATA_RX_DMA, I2C_TIMEOUT_RX(m_pRxChunks->uiLength));
					} else
#endif
					{
						// wait for next BYTE_RECEIVED event
					}
				}
				MARK_EVENT_IS_HANDLED;
#ifdef STM32F0
			} else if (uiEvent & (I2C_FLAG_TC & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: RX TC");
				ASSERT("unexpected RX TC");
			} else if (uiEvent & (I2C_FLAG_TCR & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: RX TCR");
				if (m_pTxChunks) {
					ASSERTE(m_uiRxTotalBytesLeft);
					uint16_t uiAddress = m_pCurrentI2CDevice->GetI2CAddress();
					unsigned uiLength = (m_uiRxTotalBytesLeft < MAX_F0NBYTES) ? m_uiRxTotalBytesLeft : MAX_F0NBYTES;
					I2C_TransferHandling(I2Cx, uiAddress << 1, uiLength, I2C_Reload_Mode, I2C_No_StartStop);
//					I2C_NumberOfBytesConfig(I2Cx, uiLength);
					I2C_DEBUG_INFO("Next RX NBYTES :", uiLength);
				} else {
					ASSERT("unexpected RX TCR");
					if (!m_pRxChunks) {
						ProcessMasterStop();
						MARK_EVENT_IS_HANDLED;
					}
				}
				MARK_EVENT_IS_HANDLED;
#endif
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_DATA_RX_N");
			break;
#ifdef STM32F4
		case FSMSTATE_DATA_RX_3: // Wait until BTF = 1 (data N-2 in DR, data N-1 in shift register, SCL stretched low until data N-2 is read)
			if (uiEvent & (I2C_FLAG_BTF & I2C_FLAG_MASK)) {
				ASSERTE(m_uiRxTotalBytesLeft == 3);
				I2C_DEBUG_INFO("Event IRQ: BTF");
				I2C_AcknowledgeConfig(I2Cx, DISABLE); // Set ACK low
				DoRXByte();                           // Read data N-2
				// ignore possible DMA, finish procedure in IRQ mode
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_3, FSMSTATE_DATA_RX_2, I2C_TIMEOUT_RX(3));
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_DATA_RX_3");
			break;
		case FSMSTATE_DATA_RX_2: // Wait until BTF = 1 (data N-1 in DR, data N in shift register, SCL stretched low until a data N-1 is read
			if (uiEvent & (I2C_FLAG_BTF & I2C_FLAG_MASK)) {
				ASSERTE(m_uiRxTotalBytesLeft == 2);
				I2C_DEBUG_INFO("Event IRQ: BTF");
				// non DMA RX all bytes received. Going to STOP
				I2C_GenerateSTOP(I2Cx, ENABLE);       // Set STOP high
				DoRXByte();                           // Read data N-1 (next is last)
				ASSERTE(m_pRxChunks);
				ASSERTE(m_uiRxTotalBytesLeft == 1);
				ASSERTE(!m_pRxChunks->GetNextItem());
				// ignore possible DMA, finish procedure in IRQ mode
				I2C_DEBUG_INFO("non DMA RX pre last byte");
				ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF));
				ASSERTE(I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));
				DoRXByte();                           // Read data N (last)
				ASSERTE(!m_pRxChunks);
				ASSERTE(m_uiRxTotalBytesLeft == 0);
				I2C_DEBUG_INFO("non DMA RX last byte");
				ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF));
				ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));
				ProcessMasterStop(false);
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_DATA_RX_2");
			break;
		case FSMSTATE_DATA_RX_SINGLE:
			if (uiEvent & (I2C_FLAG_RXNE & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: RxNE");
				ASSERTE(m_uiRxTotalBytesLeft == 1);
				// one byte sequence
				ASSERTE(!(I2Cx->CR1 & I2C_CR1_ACK)); // ACK should be cleared before ADDR clear
				// non DMA RX all bytes received. Going to STOP
				ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF));
				DoRXByte();
				ASSERTE(m_uiRxTotalBytesLeft == 0);
				I2C_DEBUG_INFO("non DMA RX single byte");
				ASSERTE(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));
				ProcessMasterStop(false);
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Unexpected I2C event type while FSMSTATE_DATA_RX_SINGLE");
			break;
#endif // STM32F4
		case FSMSTATE_STOP: // Wait until MSL = 0
#ifdef STM32F4
			ASSERT("STOP processed in bus idle");
#endif
			if (uiEvent & (I2C_FLAG_STOPF & I2C_FLAG_MASK)) {
				I2C_DEBUG_INFO("Event IRQ: STOPF");
				DoClearStopFlag();
				CHECKED_CHANGE_MASTER_STATE(FSMSTATE_STOP, FSMSTATE_IDLE, 0);
				IDeviceI2C* pDeviceI2C = NULL;
				AtomicExchange(&m_pCurrentI2CDevice, &pDeviceI2C);
				NotifyMasterPktDone(pDeviceI2C);
				ProcessBusFinalize(); // -> BUSSTATE_IDLE or BUSSTATE_MASTER
				MARK_EVENT_IS_HANDLED;
			}
			break;
		default: {
			if (!(uiEvent & ~((uint32_t)0x00070000))  /*ignore TRA, BUSY and MSL flags*/ ) {
				I2C_DEBUG_INFO("Event to ignore:", uiEvent);
				MARK_EVENT_IS_HANDLED;
			}
			ASSERT_EVENT_IS_HANDLED_I2C("Invalid I2C fsm state for event");
		} break;
	}

	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C master event");
}

void CHW_I2Cx::OnI2CxError()
{
	IS_EVENT_HANDLER;

	I2C_DEBUG_INFO("Error IRQ. " IF_STM32F4("SR1:", I2Cx->SR1) IF_STM32F0("ISR:", I2Cx->ISR));
	ProcessBusStopIfNecessary();
	if (I2C_GetFlagStatus(I2Cx, I2C_FLAG_BERR)) {
		I2C_ClearFlag(I2Cx, I2C_FLAG_BERR);
		I2C_DEBUG_INFO("Bus error. Resetting. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
		ProcessAbortBusSession(ErrorType::I2C_ERROR_BUS);
		MARK_EVENT_IS_HANDLED;
	}
	if (I2C_GetFlagStatus(I2Cx, I2C_FLAG_ARLO)) {
		I2C_ClearFlag(I2Cx, I2C_FLAG_ARLO);
		I2C_DEBUG_INFO("Arbitration lost. Resetting. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
		ProcessAbortBusSession(ErrorType::I2C_ERROR_ARB);
		MARK_EVENT_IS_HANDLED;
	}
	if (I2C_GetFlagStatus(I2Cx, IF_STM32F4(I2C_FLAG_AF) IF_STM32F0(I2C_FLAG_NACKF))) {
		I2C_ClearFlag(I2Cx, IF_STM32F4(I2C_FLAG_AF) IF_STM32F0(I2C_FLAG_NACKF));
		switch (m_eBusState) {
		case BUSSTATE_SLAVE1:
			I2C_DEBUG_INFO("Slave 1 got NO Acknowledge. Transmission done.");
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice1);
			NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
			break;
		case BUSSTATE_SLAVE2:
			I2C_DEBUG_INFO("Slave 2 got NO Acknowledge. Transmission done.");
			FinalizeCurrentSlaveOperation(m_pSlaveI2CDevice2);
			NVIC_ClearPendingIRQ(I2Cx_IT_EVT_IRQn);
			break;
		default:
			I2C_DEBUG_INFO("Acknowledge failure. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
			ProcessAbortBusSession(ErrorType::I2C_ERROR_ACK);
			// todo reset?
			break;
		}
		MARK_EVENT_IS_HANDLED;
	}
	if (I2C_GetFlagStatus(I2Cx, I2C_FLAG_OVR)) {
		I2C_ClearFlag(I2Cx, I2C_FLAG_OVR);
		I2C_DEBUG_INFO("OVERRUN. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
		// todo reset?
		ProcessAbortBusSession(ErrorType::I2C_ERROR_ACK);
		MARK_EVENT_IS_HANDLED;
	}
	if (I2C_GetFlagStatus(I2Cx, I2C_FLAG_PECERR)) {
		I2C_ClearFlag(I2Cx, I2C_FLAG_PECERR);
		I2C_DEBUG_INFO("PECERR. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
		// todo reset?
		ProcessAbortBusSession(ErrorType::I2C_ERROR_ACK);
		MARK_EVENT_IS_HANDLED;
	}
	if (I2C_GetFlagStatus(I2Cx, I2C_FLAG_TIMEOUT)) {
		I2C_ClearFlag(I2Cx, I2C_FLAG_TIMEOUT);
		I2C_DEBUG_INFO("TIMEOUT. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
		// todo reset?
		ProcessAbortBusSession(ErrorType::I2C_ERROR_ACK);
		MARK_EVENT_IS_HANDLED;
	}
	if (I2C_GetFlagStatus(I2Cx, IF_STM32F4(I2C_FLAG_SMBALERT) IF_STM32F0(I2C_FLAG_ALERT))) {
		I2C_ClearFlag(I2Cx, IF_STM32F4(I2C_FLAG_SMBALERT) IF_STM32F0(I2C_FLAG_ALERT));
		I2C_DEBUG_INFO("SMBALERT. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
		// todo reset?
		ProcessAbortBusSession(ErrorType::I2C_ERROR_ACK);
		MARK_EVENT_IS_HANDLED;
	}

	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C error");
	I2C_DEBUG_INFO("Error IRQ finished. " IF_STM32F4("SR1:", I2Cx->SR1) IF_STM32F0("ISR:", I2Cx->ISR));
}

#if defined(I2Cx_DMA_TX)
void CHW_I2Cx::OnI2CxDMATX()
{
	IS_EVENT_HANDLER;
	bool bTC = DMA_GetITStatus(I2Cx_DMA_TX_Stream, I2Cx_DMA_TX_TC_BIT);
	if (bTC) {
		DMA_ClearITPendingBit(I2Cx_DMA_TX_Stream, I2Cx_DMA_TX_TC_BIT);
	}
	bool bTE = DMA_GetITStatus(I2Cx_DMA_TX_Stream, I2Cx_DMA_TX_TE_BIT);
	if (bTE) {
		DMA_ClearITPendingBit(I2Cx_DMA_TX_Stream, I2Cx_DMA_TX_TE_BIT);
	}
	if (bTC && bTE) { I2C_DEBUG_INFO("DMATXIRQ: TC TE"); }
	else if (bTC) { I2C_DEBUG_INFO("DMATXIRQ: TC"); }
	else if (bTE) { I2C_DEBUG_INFO("DMATXIRQ: TE"); }
	else { I2C_DEBUG_INFO("DMATXIRQ: ??"); }
	switch (m_eBusState) {
		case BUSSTATE_DISABLED:
		case BUSSTATE_INIT:
		case BUSSTATE_IDLE:
			break;
		case BUSSTATE_SLAVE1:
			if (bTC) ProcessI2CxSlaveDMATXTC(m_pSlaveI2CDevice1);
			if (bTE) ProcessI2CxSlaveDMATXTE(m_pSlaveI2CDevice1);
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case BUSSTATE_SLAVE2:
			if (bTC) ProcessI2CxSlaveDMATXTC(m_pSlaveI2CDevice2);
			if (bTE) ProcessI2CxSlaveDMATXTE(m_pSlaveI2CDevice2);
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case BUSSTATE_MASTER:
			if (bTC) ProcessI2CxMasterDMATXTC();
			if (bTE) ProcessI2CxMasterDMATXTE();
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case BUSSTATE_RECOVER:
			break;
	}
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C Tx DMA IRQ");
}

void CHW_I2Cx::ProcessI2CxSlaveDMATXTC(ISlaveI2C* pSlaveDevice)
{
	ASSERTE(m_eSlaveState == SLAVESTATE_TX_DMA);
	ASSERT("TODO");
}

void CHW_I2Cx::ProcessI2CxSlaveDMATXTE(ISlaveI2C* pSlaveDevice)
{
	ASSERTE(m_eSlaveState == SLAVESTATE_TX_DMA);
	ASSERT("TODO");
}

void CHW_I2Cx::ProcessI2CxMasterDMATXTC()
{
	ASSERTE(m_eMasterState == FSMSTATE_DATA_TX_DMA);
	m_pTxChunks = m_pTxChunks->GetNextItem();
	m_uiTxChunkBytesSend = 0;
	I2C_DEBUG_INFO("Next Tx chunk:", (uint32_t)m_pTxChunks);
	if (m_pTxChunks) {
		I2C_DEBUG_INFO("data:", (uint32_t)m_pTxChunks->pData);
		I2C_DEBUG_INFO("len:", m_pTxChunks->uiLength);
		I2C_DEBUG_INFO("dma?:", m_bUseTxDma);
	} else {
		ASSERTE(m_uiTxTotalBytesLeft == 0);
		m_uiTxTotalBytesLeft = 0;
	}
	if (m_pTxChunks) {
		ASSERTE(m_uiTxTotalBytesLeft >= m_pTxChunks->uiLength);
		DoPrepareTXDMA();
		m_uiTxTotalBytesLeft -= m_pTxChunks->uiLength;
		I2C_DMALastTransferCmd(I2Cx, (m_pTxChunks->GetNextItem() || m_pRxChunks)?DISABLE:ENABLE);
		m_nTimeoutCountdown = I2C_TIMEOUT_TX(m_pTxChunks->uiLength);
	} else {
		ASSERTE(m_uiTxTotalBytesLeft == 0);
		DoDisableTxDMA();
		CHECKED_CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DMA, FSMSTATE_DATA_TX_DONE, I2C_TIMEOUT_STOP);
	}
}

void CHW_I2Cx::ProcessI2CxMasterDMATXTE()
{
	ASSERTE(m_eMasterState == FSMSTATE_DATA_TX_DMA);
	ProcessAbortMasterSession(ErrorType::I2C_ERROR_DMA);
}
#endif

#if defined(I2Cx_DMA_RX)
void CHW_I2Cx::OnI2CxDMARX()
{
	IS_EVENT_HANDLER;
	I2C_DEBUG_INFO("I2C DMA Rx");
	bool bTC = DMA_GetITStatus(I2Cx_DMA_RX_Stream, I2Cx_DMA_RX_TC_BIT);
	if (bTC) {
		DMA_ClearITPendingBit(I2Cx_DMA_RX_Stream, I2Cx_DMA_RX_TC_BIT);
	}
	bool bTE = DMA_GetITStatus(I2Cx_DMA_RX_Stream, I2Cx_DMA_RX_TE_BIT);
	if (bTE) {
		DMA_ClearITPendingBit(I2Cx_DMA_RX_Stream, I2Cx_DMA_RX_TE_BIT);
	}
	switch (m_eBusState) {
		case BUSSTATE_DISABLED:
		case BUSSTATE_INIT:
		case BUSSTATE_IDLE:
			break;
		case BUSSTATE_SLAVE1:
			if (bTC) ProcessI2CxSlaveDMARXTC(m_pSlaveI2CDevice1);
			if (bTE) ProcessI2CxSlaveDMARXTE(m_pSlaveI2CDevice1);
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case BUSSTATE_SLAVE2:
			if (bTC) ProcessI2CxSlaveDMARXTC(m_pSlaveI2CDevice2);
			if (bTE) ProcessI2CxSlaveDMARXTE(m_pSlaveI2CDevice2);
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case BUSSTATE_MASTER:
			if (bTC) ProcessI2CxMasterDMARXTC();
			if (bTE) ProcessI2CxMasterDMARXTE();
			if (bTC || bTE) MARK_EVENT_IS_HANDLED;
			break;
		case BUSSTATE_RECOVER:
			break;
	}
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C Rx DMA IRQ");
}

void CHW_I2Cx::ProcessI2CxSlaveDMARXTC(ISlaveI2C* pSlaveDevice)
{
	IS_EVENT_HANDLER;
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C Rx slave DMA TC IRQ");
}

void CHW_I2Cx::ProcessI2CxSlaveDMARXTE(ISlaveI2C* pSlaveDevice)
{
	IS_EVENT_HANDLER;
	ASSERT_EVENT_IS_HANDLED_I2C("Unhandled I2C Rx slave DMA TE IRQ");
}

void CHW_I2Cx::ProcessI2CxMasterDMARXTC()
{
	ASSERTE(m_eMasterState == FSMSTATE_DATA_RX_DMA);
	m_pRxChunks = m_pRxChunks->GetNextItem();
	m_uiRxChunkBytesReceived = 0;
	if (m_pRxChunks) {
		ASSERTE(m_uiRxTotalBytesLeft >= m_pRxChunks->uiLength);
		DoPrepareRXDMA();
		m_uiRxTotalBytesLeft -= m_pRxChunks->uiLength;
		m_nTimeoutCountdown = I2C_TIMEOUT_RX(m_pRxChunks->uiLength);

	} else {
		ASSERTE(m_uiRxTotalBytesLeft == 0);
		DoDisableRxDMA();
		ProcessMasterStop();
	}
	//DoStopSession();
}

void CHW_I2Cx::ProcessI2CxMasterDMARXTE()
{
	// todo rx error processing
	ASSERTE("I2C DMA Rx error");
	ProcessAbortMasterSession(ErrorType::I2C_ABORT);
}
#endif

void CHW_I2Cx::ProcessMasterTxProcedureFirst()
{
	ASSERTE(m_pTxChunks);
	ASSERTE(m_pTxChunks->uiLength);
#if defined(I2Cx_DMA_TX)
	if (m_bUseTxDma) {
		CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DMA, I2C_TIMEOUT_TX(m_pTxChunks->uiLength));
		// DMA already configured and started, just wait DMA TC event;
	} else
#endif
	{
		CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_N, I2C_TIMEOUT_TX(m_pTxChunks->uiLength));
		DoTXByte();
#if defined(I2Cx_DMA_TX)
		if (m_bUseTxDma) {
			//next chunk is DMA
			DoDisableBufferInterrupt();
			DoPrepareTXDMA();
			I2C_DMALastTransferCmd(I2Cx, (m_pTxChunks->GetNextItem() || m_pRxChunks)?DISABLE:ENABLE);
			// start DMA and change to FSMSTATE_DATA_TX_DMA later, in FSMSTATE_DATA_TX_N
		}
#endif
	}
}

void CHW_I2Cx::ProcessMasterTxProcedureNext()
{
#if defined(I2Cx_DMA_TX)
	if (m_bUseTxDma) {
		ASSERTE(m_pTxChunks);
		// DMA already configured in FSMSTATE_TXADDRESS after first TX. start DMA and wait DMA TC event;
		DoDisableBufferInterrupt();
		CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DMA, I2C_TIMEOUT_TX(m_pTxChunks->uiLength));
		DoEnableTxDMA();
	} else
#endif
	if (m_pTxChunks) {
		ASSERTE(m_pTxChunks->uiLength);
		DoTXByte();
#if defined(I2Cx_DMA_TX)
		if (m_bUseTxDma) {
			//next chunk is DMA
			DoDisableBufferInterrupt();
			CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DMA, I2C_TIMEOUT_TX(m_pTxChunks->uiLength));
			DoPrepareTXDMA();
			I2C_DMALastTransferCmd(I2Cx, (m_pTxChunks->GetNextItem() || m_pRxChunks)?DISABLE:ENABLE);
			DoEnableTxDMA();
		}
#endif
	} else {
		DoDisableBufferInterrupt();
		CHANGE_MASTER_STATE(FSMSTATE_DATA_TX_DONE, I2C_TIMEOUT_STOP);
	}
}

void CHW_I2Cx::ProcessSlave1RxAddress()
{
	ASSERTE(m_pSlaveI2CDevice1);
	I2C_DEBUG_INFO("Slave WR:", m_pSlaveI2CDevice1->GetI2CAddress());
	CHANGE_BUS_STATE(BUSSTATE_SLAVE1, 0);
	PrepareSlaveWriteSession(m_pSlaveI2CDevice1);
	if (m_pRxChunks) {
		ProcessSlaveRxStartProcedure(); // -> SLAVESTATE_RX or SLAVESTATE_RX_DMA
	} else {
		I2C_DEBUG_INFO("Slave write ignored. Addr:", m_pSlaveI2CDevice1->GetI2CAddress());
		I2C_ReceiveData(I2Cx); // clear RxNE flag
//				ProcessSlaveRxEndProcedure(); // -> SLAVESTATE_IDLE
	}
}

void CHW_I2Cx::ProcessSlave2RxAddress()
{
	ASSERTE(m_pSlaveI2CDevice2);
	I2C_DEBUG_INFO("Slave WR:", m_pSlaveI2CDevice2->GetI2CAddress());
	CHANGE_BUS_STATE(BUSSTATE_SLAVE2, 0);
	PrepareSlaveWriteSession(m_pSlaveI2CDevice2);
	if (m_pRxChunks) {
		ProcessSlaveRxStartProcedure(); // -> SLAVESTATE_RX or SLAVESTATE_RX_DMA
	} else {
		I2C_DEBUG_INFO("Slave write ignored. Addr:", m_pSlaveI2CDevice2->GetI2CAddress());
		I2C_ReceiveData(I2Cx); // clear RxNE flag
//				ProcessSlaveRxEndProcedure(); // -> SLAVESTATE_IDLE
	}
}

void CHW_I2Cx::ProcessSlaveRxStartProcedure()
{
	DoPrepareRX();

	unsigned uiRxSize =  m_pRxChunks->uiLength;
#if defined(I2Cx_DMA_RX)
	if (m_bUseRxDma) {
		CHANGE_SLAVE_STATE(SLAVESTATE_RX_DMA, I2C_TIMEOUT_RX(uiRxSize));
		// DMA already configured in DoPrepareRx, just wait DMA TC event;
	} else
#endif
	{
//		if (uiRxSize == 1) {
//			I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
//			I2C_AcknowledgeConfig(I2Cx, DISABLE);
//		} else if (uiRxSize == 2) {
//			I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Next);
//			I2C_AcknowledgeConfig(I2Cx, DISABLE);
//		} else {
			I2C_AcknowledgeConfig(I2Cx, ENABLE);     // Set ACK  high
//		}
//		DoEnableBufferRxInterrupt(); Already done in DoPrepareRX()
		CHANGE_SLAVE_STATE(SLAVESTATE_RX, I2C_TIMEOUT_RX(uiRxSize));
	}
}

void CHW_I2Cx::ProcessSlaveRx(ISlaveI2C* pSlaveDevice)
{
	ASSERTE(m_eSlaveState == SLAVESTATE_RX);
	I2C_DEBUG_INFO("Slave RxNE");
	if (!m_pRxChunks) {
		PrepareSlaveWriteSession(pSlaveDevice);
	}
	if (m_pRxChunks) {
		DoRXByte();
	} else {
		// slave rx buffer overflow
		I2C_DEBUG_INFO("Slave write ignored. Addr:", pSlaveDevice->GetI2CAddress());
		I2C_ReceiveData(I2Cx); // clear RxNE flag
	}
}

void CHW_I2Cx::ProcessSlave1TxAddress()
{
	ASSERTE(m_pSlaveI2CDevice1);
	I2C_DEBUG_INFO("Slave RD:", m_pSlaveI2CDevice1->GetI2CAddress());
	CHANGE_BUS_STATE(BUSSTATE_SLAVE1, 0);
	PrepareSlaveReadSession(m_pSlaveI2CDevice1);
	if (m_pTxChunks) {
		ProcessSlaveTxStartProcedure(); // -> SLAVESTATE_TX or SLAVESTATE_TX_DMA
	} else {
		I2C_DEBUG_INFO("Slave has nothing to read. Addr:", m_pSlaveI2CDevice1->GetI2CAddress());
		I2C_SendData(I2Cx, 0);  // clear TxNE flag
		I2C_SendData(I2Cx, 0);  // clear TxNE flag
		CHANGE_SLAVE_STATE(SLAVESTATE_TX, I2C_TIMEOUT_TX(1));
//		ProcessSlaveTxEndProcedure(); // -> SLAVESTATE_IDLE
	}
}

void CHW_I2Cx::ProcessSlave2TxAddress()
{
	ASSERTE(m_pSlaveI2CDevice2);
	I2C_DEBUG_INFO("Slave RD:", m_pSlaveI2CDevice2->GetI2CAddress());
	CHANGE_BUS_STATE(BUSSTATE_SLAVE2, 0);
	PrepareSlaveReadSession(m_pSlaveI2CDevice2);
	if (m_pTxChunks) {
		ProcessSlaveTxStartProcedure(); // -> SLAVESTATE_TX or SLAVESTATE_TX_DMA
	} else {
		I2C_DEBUG_INFO("Slave has nothing to read. Addr:", m_pSlaveI2CDevice2->GetI2CAddress());
		I2C_SendData(I2Cx, 0);  // clear TxNE flag
		I2C_SendData(I2Cx, 0);  // clear TxNE flag
		CHANGE_SLAVE_STATE(SLAVESTATE_TX, I2C_TIMEOUT_TX(1));
//		ProcessSlaveTxEndProcedure(); // -> SLAVESTATE_IDLE
	}
}

void CHW_I2Cx::ProcessSlaveTxStartProcedure()
{
	DoPrepareTX();

	ASSERTE(m_pTxChunks);
	ASSERTE(m_pTxChunks->uiLength);
#if defined(I2Cx_DMA_TX)
	if (m_bUseTxDma) {
		// DMA already configured in DoPrepareTX. start DMA and wait DMA TC event;
		DoDisableBufferInterrupt();
		CHANGE_SLAVE_STATE(SLAVESTATE_TX_DMA, I2C_TIMEOUT_RX(m_pTxChunks->uiLength));
		DoEnableTxDMA();
	} else
#endif
	{
		DoTXByte();
#if defined(I2Cx_DMA_TX)
		if (m_bUseTxDma) {
			//next chunk is DMA
			DoDisableBufferInterrupt();
			CHANGE_SLAVE_STATE(SLAVESTATE_TX_DMA, I2C_TIMEOUT_RX(m_pTxChunks->uiLength));
			DoPrepareTXDMA();
			I2C_DMALastTransferCmd(I2Cx, (m_pTxChunks->GetNextItem() || m_pRxChunks)?DISABLE:ENABLE);
			DoEnableTxDMA();
		} else
#endif
		{
			CHANGE_SLAVE_STATE(SLAVESTATE_TX, I2C_TIMEOUT_TX(m_pTxChunks->uiLength));
		}
	}
}

void CHW_I2Cx::ProcessSlaveTx(ISlaveI2C* pSlaveDevice)
{
	ASSERTE(m_eSlaveState == SLAVESTATE_TX);
	I2C_DEBUG_INFO("Slave TXE");
	if (!m_pTxChunks) {
		PrepareSlaveReadSession(pSlaveDevice);
	}
	if (m_pTxChunks) {
		DoTXByte();
	} else {
		// slave tx buffer overrun
		I2C_DEBUG_INFO("Slave has nothing to read. Addr:", pSlaveDevice->GetI2CAddress());
		I2C_SendData(I2Cx, 0);  // clear TxNE flag
	}
}

void CHW_I2Cx::ProcessMasterRxProcedure()
{
	unsigned uiRxSize =  m_pRxChunks->uiLength;
#ifdef STM32F4
	if (m_bUseRx1Procedure) {
		ASSERTE(uiRxSize == 1);
		// In case a single byte has to be received, the Acknowledge disable is made during EV6 (before ADDR flag is cleared) and the STOP condition generation is made after EV6
#if defined(I2Cx_DMA_RX)
		if (m_bUseRxDma) {
			CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_DMA, I2C_TIMEOUT_RX(uiRxSize));
			// DMA already configured in DoPrepareRx, just wait DMA TC event;
		} else
#endif
		{
			DoEnableBufferRxInterrupt();
			CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_SINGLE, I2C_TIMEOUT_RX(uiRxSize));
		}
		I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
		I2C_AcknowledgeConfig(I2Cx, DISABLE);
	} else if (m_bUseRx2Procedure) {
		ASSERTE(uiRxSize == 2 || (uiRxSize == 1 && m_pRxChunks->GetNextItem() && m_pRxChunks->GetNextItem()->uiLength == 1 && !m_pRxChunks->GetNextItem()->GetNextItem()));
#if defined(I2Cx_DMA_RX)
		if (m_bUseRxDma) {
			// DMA already configured in DoPrepareRx, just wait DMA TC event;
			CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_DMA, I2C_TIMEOUT_RX(uiRxSize));
		} else
#endif
		{
			CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_2, I2C_TIMEOUT_RX(uiRxSize));
		}
		I2C_AcknowledgeConfig(I2Cx, DISABLE);     // Set ACK low, set POS high
		I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Next);
	} else
#endif //STM32F4
	{
#if defined(I2Cx_DMA_RX)
		if (m_bUseRxDma) {
			// DMA already configured in DoPrepareRx, just wait DMA TC event;
			CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_DMA, I2C_TIMEOUT_RX(uiRxSize));
		} else
#endif
		{
#ifdef STM32F4
			I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
#endif //STM32F4
			I2C_AcknowledgeConfig(I2Cx, ENABLE);     // Set ACK  high
			DoEnableBufferRxInterrupt();
			CHANGE_MASTER_STATE(FSMSTATE_DATA_RX_N, I2C_TIMEOUT_RX(uiRxSize));
		}
	}
	DoClearAddrFlag();
#ifdef STM32F4
	if (m_bUseRx1Procedure) {
		I2C_GenerateSTOP(I2Cx, ENABLE);
	}
#endif //STM32F4
}

void CHW_I2Cx::ProcessMasterStop(bool bDoGenerateStop)
{
	ASSERTE(m_eMasterState != FSMSTATE_IDLE && m_eMasterState != FSMSTATE_STOP);
	if (bDoGenerateStop) {
		I2C_DEBUG_INFO("Generate STOP");
		I2C_GenerateSTOP(I2Cx, ENABLE);
		CHANGE_MASTER_STATE(FSMSTATE_STOP, 0);
	} else {
		CHANGE_MASTER_STATE(FSMSTATE_IDLE, 0);
	}
#ifdef STM32F4
	IDeviceI2C* pDeviceI2C = NULL;
	AtomicExchange(&m_pCurrentI2CDevice, &pDeviceI2C);
	NotifyMasterPktDone(pDeviceI2C);
	ProcessBusFinalize(); // -> BUSSTATE_IDLE or BUSSTATE_MASTER
#endif
#ifdef STM32F0
	// stop will be processed in event irq
#endif
}

void CHW_I2Cx::ProcessMasterDiscardStartIfNecessary()
{
	if (m_eMasterState == FSMSTATE_START) {
		m_bStartDiscarded = true;
		I2C_GenerateSTART(I2Cx, DISABLE);
		CHANGE_MASTER_STATE(FSMSTATE_IDLE, 0);
		I2C_DEBUG_INFO("Master Start discarded");
	}
}

void CHW_I2Cx::ProcessBusFinalize()
{
#if defined(I2Cx_DMA_TX)
	DoDisableTxDMA();
#endif
#if defined (I2Cx_DMA_RX)
	DoDisableRxDMA();
#endif
	DoDisableBufferInterrupt();
	CHANGE_BUS_STATE(BUSSTATE_IDLE, m_eMasterState == FSMSTATE_STOP ? I2C_TIMEOUT_STOP : 0);

	if (!m_pSlaveI2CDevice1) {
		DoDisableEventInterrupt();
	}
}

bool CHW_I2Cx::ProcessBusInitIfNecessary()
{
	bool bResult;

	if (m_eBusState != BUSSTATE_INIT) {
		bResult = (m_eBusState == BUSSTATE_IDLE);
	} else if (DoInit(m_uiSlaveAddress1, m_uiSlaveAddress2)) {
		CHECKED_CHANGE_BUS_STATE(BUSSTATE_INIT, BUSSTATE_IDLE, 0);
		bResult = true;
	} else {
		m_nBusRecoverValidCounts = 0;
		Deinit_I2Cx();
		CHECKED_CHANGE_BUS_STATE(BUSSTATE_INIT, BUSSTATE_RECOVER, I2C_TIMEOUT_BUS_RECOVER);
		// continue in OnSysTick interrupt handler
		bResult = false;
	}

	return bResult;
}

bool CHW_I2Cx::ProcessBusStopIfNecessary()
{
	bool bResult = (1
			&& m_eBusState == BUSSTATE_IDLE
			&& m_eMasterState == FSMSTATE_STOP
			&& IF_STM32F4(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_MSL)) IF_STM32F0(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF))
	);
	if (bResult) {
		I2C_DEBUG_INFO("bus STOP detected");
		CHANGE_MASTER_STATE(FSMSTATE_IDLE, 0);
#if defined (I2Cx_DMA_TX)
		DoDisableTxDMA();
#endif
#if defined (I2Cx_DMA_RX)
		DoDisableRxDMA();
#endif
		DoDisableBufferInterrupt();
		if (!m_pSlaveI2CDevice1) {
			DoDisableEventInterrupt();
		}
		DoClearStopFlag();
		CHANGE_BUS_STATE(BUSSTATE_IDLE, I2C_TIMEOUT_STOP);
	}
	return bResult;
}

bool CHW_I2Cx::ProcessQueuedSessions()
{
	bool bResult;
	if (m_eBusState != BUSSTATE_IDLE) {
		bResult = false;
	} else if (PopNextMasterSession()) {
		ASSERTE(m_eMasterState == FSMSTATE_IDLE || m_eMasterState == FSMSTATE_STOP);
		m_bStartDiscarded = false;
		CHANGE_MASTER_STATE(FSMSTATE_START, I2C_TIMEOUT_START);
		DoStartMasterSession();
		I2C_DEBUG_INFO("session queue pop:", m_pCurrentI2CDevice->GetI2CAddress());
		bResult = true;
	} else {
		I2C_DEBUG_INFO("session queue empty");
		NotifyMasterI2CIdle();
		bResult = false;
	}
	return bResult;
}

void CHW_I2Cx::ProcessAbortSession()
{
//	ASSERT("I2C Error");
	I2C_DEBUG_INFO("Session aborted. Addr:", m_pCurrentI2CDevice?m_pCurrentI2CDevice->GetI2CAddress():0);
	DoCancel();
	CHANGE_SLAVE_STATE(SLAVESTATE_IDLE, 0);
	CHANGE_MASTER_STATE(FSMSTATE_IDLE, 0);
	CHANGE_BUS_STATE(BUSSTATE_INIT, 0);
}

void CHW_I2Cx::ProcessAbortMasterSession(ErrorType e)
{
	ProcessAbortSession();
	IDeviceI2C* pDeviceI2C = NULL;
	AtomicExchange(&m_pCurrentI2CDevice, &pDeviceI2C);
	m_pTxChunks = NULL;
	m_pRxChunks = NULL;
	m_uiRxChunkBytesReceived = 0;
	m_uiTxChunkBytesSend = 0;
	m_uiRxTotalBytesLeft = 0;
	m_uiTxTotalBytesLeft = 0;
	NotifyMasterPktError(e, pDeviceI2C);
}


void CHW_I2Cx::ProcessAbortSlave1Session(ErrorType e)
{
	ProcessAbortSession();
	m_pSlaveI2CDevice1->OnI2CError(e);
}

void CHW_I2Cx::ProcessAbortSlave2Session(ErrorType e)
{
	ProcessAbortSession();
	m_pSlaveI2CDevice2->OnI2CError(e);
}

void CHW_I2Cx::ProcessAbortBusSession(ErrorType e)
{
	switch (m_eBusState) {
		case BUSSTATE_DISABLED:
		case BUSSTATE_INIT:
		case BUSSTATE_IDLE:
		case BUSSTATE_RECOVER:
			break;
		case BUSSTATE_SLAVE1:
			ProcessAbortSlave1Session(e);
			break;
		case BUSSTATE_SLAVE2:
			ProcessAbortSlave2Session(e);
			break;
		case BUSSTATE_MASTER:
			ProcessAbortMasterSession(e);
			break;
	}
}

/*static*/
void CHW_I2Cx::Init(unsigned uiClockSpeed)
{
	Init_Base(uiClockSpeed);
	Init_Slave(uiClockSpeed);
	Init_Master(uiClockSpeed);

	m_pSlaveI2CDevice1 = NULL;
	m_pSlaveI2CDevice2 = NULL;

	ProcessBusInitIfNecessary();

	CHW_Clocks::RegisterSysTickCallback(&g_HW_I2Cx);        INDIRECT_CALL(g_HW_I2Cx.OnSysTick());
	// continue in OnSysTick interrupt handler
}

/*static*/
void CHW_I2Cx::AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1 /*= NULL*/, ISlaveI2C* pSlaveI2CDevice2 /*= NULL*/)
{
	Init_Slave(m_uiClockSpeed, pSlaveI2CDevice1, pSlaveI2CDevice2);

	m_uiSlaveAddress1 = pSlaveI2CDevice1 ? pSlaveI2CDevice1->GetI2CAddress() : 0;
	m_uiSlaveAddress2 = pSlaveI2CDevice2 ? pSlaveI2CDevice2->GetI2CAddress() : 0;

	m_bReinitRequired = true;
}

/*static*/
void CHW_I2Cx::Deinit()
{
	CHW_Clocks::UnregisterSysTickCallback(&g_HW_I2Cx);

	Deinit_I2Cx();

	m_eBusState = BUSSTATE_DISABLED;
	m_eSlaveState = SLAVESTATE_IDLE;
	m_eMasterState = FSMSTATE_IDLE;
}

#endif //STM32F4

class CHW_I2Cx_InterfaceWrapper :
		public II2C
{
public: // II2C
	virtual void Init(unsigned uiClockSpeed);
	virtual void Deinit();

	virtual void AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1 = NULL, ISlaveI2C* pSlaveI2CDevice2 = NULL);

	virtual void Queue_I2C_Session(IDeviceI2C* pDeviceI2C);
	virtual void Cancel_I2C_Session(IDeviceI2C* pDeviceI2C);
	virtual void RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C);
};

/*virtual*/
void CHW_I2Cx_InterfaceWrapper::Init(unsigned uiClockSpeed)
{
	IMPLEMENTS_INTERFACE_METHOD(II2C::Init(uiClockSpeed));
	g_HW_I2Cx.Init(uiClockSpeed);
}

/*virtual*/
void CHW_I2Cx_InterfaceWrapper::Deinit()
{
	IMPLEMENTS_INTERFACE_METHOD(II2C::Deinit());
	g_HW_I2Cx.Deinit();
}

/*virtual*/
void CHW_I2Cx_InterfaceWrapper::AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1 /*= NULL*/, ISlaveI2C* pSlaveI2CDevice2 /*= NULL*/)
{
	IMPLEMENTS_INTERFACE_METHOD(II2C::AssignSlaveDevice(pSlaveI2CDevice1, pSlaveI2CDevice2));
	g_HW_I2Cx.AssignSlaveDevice(pSlaveI2CDevice1, pSlaveI2CDevice2);
}

/*virtual*/
void CHW_I2Cx_InterfaceWrapper::Queue_I2C_Session(IDeviceI2C* pDeviceI2C)
{
	IMPLEMENTS_INTERFACE_METHOD(II2C::Queue_I2C_Session(pDeviceI2C));
	g_HW_I2Cx.Queue_I2C_Session(pDeviceI2C);
}

/*virtual*/
void CHW_I2Cx_InterfaceWrapper::Cancel_I2C_Session(IDeviceI2C* pDeviceI2C)
{
	IMPLEMENTS_INTERFACE_METHOD(II2C::Cancel_I2C_Session(pDeviceI2C));
	g_HW_I2Cx.Cancel_I2C_Session(pDeviceI2C);
}

/*virtual*/
void CHW_I2Cx_InterfaceWrapper::RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C)
{
	IMPLEMENTS_INTERFACE_METHOD(II2C::RequestI2CIdleNotify(pDeviceI2C));
	g_HW_I2Cx.RequestI2CIdleNotify(pDeviceI2C);
}

CHW_I2Cx_InterfaceWrapper g_HW_I2Cx_InterfaceWrapper;

#undef I2Cx_N
#undef I2Cx_SCL_GPIO_PORT
#undef I2Cx_SDA_GPIO_PORT
#undef I2Cx_SLAVE
#undef I2Cx_DMA_TX
#undef I2Cx_DMA_RX
