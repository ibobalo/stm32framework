#ifndef HW_DIN_H_INCLUDED
#define HW_DIN_H_INCLUDED

#include "interfaces/gpio.h" // notifiers
#include "hw_ports.h" // interface wrappers

template<class CHWDINx>
class THWDINx_InterfaceWrapper :
		public virtual IDigitalIn
{
public:
	static void Init(bool bPullUp = false, bool bPullDown = false) { CHWDINx::Init(bPullUp, bPullDown);}
	static void Deinit() { CHWDINx::Deinit();}
public: // IDigitalIn::IValueSource<bool>
	virtual bool GetValue() const {IMPLEMENTS_INTERFACE_METHOD(IDigitalIn::IValueSource<bool>::GetValue()); return CHWDINx::GetValue();}
};

template<class CHWDINEXx>
class THWDINxEX_InterfaceWrapper :
		public THWDINx_InterfaceWrapper<CHWDINEXx>,
		public IDigitalInProvider
{
public: // IDigitalInProvider::IValueProvider<bool>
	virtual void SetValueTargetInterface(IValueReceiver<bool>* pValueTarget) {
		IMPLEMENTS_INTERFACE_METHOD(IDigitalInProvider::IValueProvider<bool>::SetValueTargetInterface(pValueTarget));
		CHWDINEXx::SetValueTargetInterface(pValueTarget);
	}
public: // IDigitalInProvider
	virtual void SetNotifier(IDigitalInNotifier* pNotifier, uint8_t uiGroupPriority = 0, uint8_t uiSubPriority = 0, bool bOnRise = true, bool bOnFall = true) {
		IMPLEMENTS_INTERFACE_METHOD(IDigitalInProvider::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority, bOnRise, bOnFall));
		CHWDINEXx::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority);
	};
};


template<class PORTx, unsigned PINn>
class CHW_DIN
{
public:
	typedef CHW_DIN<PORTx, PINn> type;
	static inline type* Acquire(bool bPullUp = false, bool bPullDown = false) {
		type::Init(bPullUp, bPullDown);
		static type instance;
		return &instance;
	}
	static inline IDigitalIn* AcquireInterface(bool bPullUp = false, bool bPullDown = false) {
		Acquire(bPullUp, bPullDown);
		static THWDINx_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static inline void Release() {
		type::Deinit();
	}
public:
	static void Init(bool bPullUp, bool bPullDown);
	static void Deinit();
public:
	static bool GetValue();
};


template<class PORTx, unsigned PINn>
class CHW_DIN_EXTI :
	public CHW_DIN<PORTx, PINn>
{
public:
	typedef CHW_DIN_EXTI<PORTx, PINn> typeEx;
	static typeEx* Acquire(bool bPullUp = false, bool bPullDown = false) {
		typeEx::Init(bPullUp, bPullDown);
		static typeEx instance;
		return &instance;
	}
	static IDigitalInProvider* AcquireInterfaceEx(bool bPullUp = false, bool bPullDown = false) {
		Acquire(bPullUp, bPullDown);
		static THWDINxEX_InterfaceWrapper<typeEx> wrapperInstance;
		return &wrapperInstance;
	}
public:
	static void SetValueTargetInterface(IValueReceiver<bool>* pValueTarget);
	static void SetNotifier(IDigitalInNotifier* pNotifier, uint8_t uiGroupPriority = 0, uint8_t uiSubPriority = 0, bool bOnRise = true, bool bOnFall = true);
public: /* Interrupts */
	static void OnEXTI();
private:
	static void Init_EXTI(unsigned uiGroupPriority, unsigned uiSubPriority, bool bOnRise, bool bOnFall);
	static void Deinit_EXTI();

	static IValueReceiver<bool>* m_pValueTarget;
	static IDigitalInNotifier* m_pNotifier;
	static uint8_t m_uiDefaultItPrio;
	static uint8_t m_uiDefaultItSubPrio;
};


template<class PORTx, unsigned PINn>
void CHW_DIN<PORTx, PINn>::Init(bool bPullUp, bool bPullDown)
{
	PORTx::RCC_Cmd(ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIOPin<PINn>::GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = bPullUp?GPIO_PuPd_UP:(bPullDown?GPIO_PuPd_DOWN:GPIO_PuPd_NOPULL);
	GPIO_Init((GPIO_TypeDef *)PORTx::GPIOx, &GPIO_InitStructure);
}
template<class PORTx, unsigned PINn>
void CHW_DIN<PORTx, PINn>::Deinit()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIOPin<PINn>::GPIO_Pin_x;
	GPIO_Init((GPIO_TypeDef *)PORTx::GPIOx, &GPIO_InitStructure);
}
template<class PORTx, unsigned PINn>
bool CHW_DIN<PORTx, PINn>::GetValue()
{
	return GPIO_ReadInputDataBit((GPIO_TypeDef *)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x);
}


/*static*/
template<class PORTx, unsigned PINn>
void CHW_DIN_EXTI<PORTx, PINn>::SetValueTargetInterface(IValueReceiver<bool>* pValueTarget)
{
	m_pValueTarget = pValueTarget;

	if (m_pValueTarget || m_pNotifier) {
		Init_EXTI(m_uiDefaultItPrio, m_uiDefaultItSubPrio, true, true);
	} else {
		Deinit_EXTI();
	}
}

/*static*/
template<class PORTx, unsigned PINn>
void CHW_DIN_EXTI<PORTx, PINn>::SetNotifier(IDigitalInNotifier* pNotifier, uint8_t uiGroupPriority, uint8_t uiSubPriority, bool bOnRise, bool bOnFall)
{
	m_pNotifier = pNotifier;

	if (m_pValueTarget || m_pNotifier) {
		Init_EXTI(uiGroupPriority, uiSubPriority, bOnRise, bOnFall);
	} else {
		Deinit_EXTI();
	}
}

/*static*/
template<class PORTx, unsigned PINn>
void CHW_DIN_EXTI<PORTx, PINn>::Init_EXTI(unsigned uiGroupPriority, unsigned uiSubPriority, bool bOnRise, bool bOnFall)
{
	EXTITrigger_TypeDef eTriggerMode;
	if      (bOnRise && bOnFall) eTriggerMode = EXTI_Trigger_Rising_Falling;
	else if (bOnRise && !bOnFall) eTriggerMode = EXTI_Trigger_Rising;
	else if (!bOnRise && bOnFall) eTriggerMode = EXTI_Trigger_Falling;
	else return Deinit_EXTI();

	SYSCFG_EXTILineConfig(PORTx::EXTI_PortSourceGPIOx, GPIOPin<PINn>::EXTI_PinSourcex);
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_StructInit(&EXTI_InitStructure);
	EXTI_InitStructure.EXTI_Line = GPIOPin<PINn>::EXTI_Linex;
	EXTI_InitStructure.EXTI_Trigger = eTriggerMode;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_ClearPendingIRQ(GPIOPin<PINn>::EXTI_IRQn);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = GPIOPin<PINn>::EXTI_IRQn;
	IF_STM32F0(
		NVIC_InitStructure.NVIC_IRQChannelPriority = uiGroupPriority;
		UNUSED(uiSubPriority);
	)
	IF_STM32F4(
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = uiGroupPriority;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = uiSubPriority;
	)
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure); INDIRECT_CALL(OnEXTI());

	ASSERT_IF_NOT_IRQn_REDIRECTED(GPIOPin<PINn>::EXTI_IRQn, "DIx_IT_PRIO must be defined in board definition for this DI");
}

/*static*/
template<class PORTx, unsigned PINn>
void CHW_DIN_EXTI<PORTx, PINn>::Deinit_EXTI()
{
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_StructInit(&EXTI_InitStructure);
	EXTI_InitStructure.EXTI_Line = GPIOPin<PINn>::EXTI_Linex;
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);
	NVIC_DisableIRQ(GPIOPin<PINn>::EXTI_IRQn);
}

template<class PORTx, unsigned PINn>
void CHW_DIN_EXTI<PORTx, PINn>::OnEXTI()
{
	if (EXTI_GetITStatus(GPIOPin<PINn>::EXTI_Linex)) {
		EXTI_ClearITPendingBit(GPIOPin<PINn>::EXTI_Linex);
		if (m_pNotifier) {
			bool bState = typeEx::GetValue();
			if (bState) m_pNotifier->OnRise();
			else m_pNotifier->OnFall();
		}
		if (m_pValueTarget) {
			bool bState = typeEx::GetValue();
			m_pValueTarget->SetValue(bState);
		}
		__DSB(); /* Data Synchronization Barrier. Ensure EXTI_LineX status cleared and isr not trigger again */ \
	}
}

/*static*/ template<class PORTx, unsigned PINn>
IValueReceiver<bool>* CHW_DIN_EXTI<PORTx, PINn>::m_pValueTarget;
/*static*/ template<class PORTx, unsigned PINn>
IDigitalInNotifier* CHW_DIN_EXTI<PORTx, PINn>::m_pNotifier;
/*static*/ template<class PORTx, unsigned PINn>
uint8_t CHW_DIN_EXTI<PORTx, PINn>::m_uiDefaultItPrio;
/*static*/ template<class PORTx, unsigned PINn>
uint8_t CHW_DIN_EXTI<PORTx, PINn>::m_uiDefaultItSubPrio;

#define ACQUIRE_RELEASE_DIGITAL_IN_DECLARATION(PORT,PIN,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	using CDigitalIn_##ID##__VA_ARGS__ = CHW_DIN<GPIOPort##PORT, PIN>; \
	static inline CDigitalIn_##ID##__VA_ARGS__* AcquireDigitalIn_##ID##__VA_ARGS__(bool bPullUp = false, bool bPullDown = false) { \
		return CDigitalIn_##ID##__VA_ARGS__::Acquire(bPullUp, bPullDown); } \
	static inline void ReleaseDigitalIn_##ID##__VA_ARGS__() { CDigitalIn_##ID##__VA_ARGS__::Release(); } \
	static inline IDigitalIn* AcquireDigitalInInterface_##ID##__VA_ARGS__(bool bPullUp = false, bool bPullDown = false) { \
		return CDigitalIn_##ID##__VA_ARGS__::AcquireInterface(bPullUp, bPullDown); } \
	static inline void ReleaseDigitalInInterface_##ID##__VA_ARGS__() { CDigitalIn_##ID##__VA_ARGS__::Release(); }

#define ACQUIRE_RELEASE_DIGITAL_IN_EX_DECLARATION(PORT,PIN,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	using CDigitalIn_##ID##__VA_ARGS__ = CHW_DIN_EXTI<GPIOPort##PORT, PIN>; \
	static inline CDigitalIn_##ID##__VA_ARGS__* AcquireDigitalIn_##ID##__VA_ARGS__(bool bPullUp = false, bool bPullDown = false) { \
		return CDigitalIn_##ID##__VA_ARGS__::Acquire(bPullUp, bPullDown); } \
	static inline void ReleaseDigitalIn_##ID##__VA_ARGS__() { CDigitalIn_##ID##__VA_ARGS__::Release(); } \
	static inline IDigitalInProvider* AcquireDigitalInInterface_##ID##__VA_ARGS__(bool bPullUp = false, bool bPullDown = false) { \
		return CDigitalIn_##ID##__VA_ARGS__::AcquireInterfaceEx(bPullUp, bPullDown); } \
	static inline void ReleaseDigitalInInterface_##ID##__VA_ARGS__() { CDigitalIn_##ID##__VA_ARGS__::Release(); }

#define ALIAS_DIGITAL_IN(ID, ALIAS) \
	using CDigitalIn_##ALIAS = CDigitalIn_##ID;

#define ALIAS_DIGITAL_IN_EX(ID, ALIAS) \
	using CDigitalIn_##ALIAS = CDigitalIn_##ID;

#define ACQUIRE_RELEASE_DIGITAL_IN_DEFINITION(PORT,PIN,CHWBRD,ID,...)  /* DEPRECATED, use CDigitalIn_X::Acquire(...) */
#define ACQUIRE_RELEASE_DIGITAL_IN_EX_DEFINITION(PORT,PIN,CHWBRD,ID,...)  /* DEPRECATED, use CDigitalInEx_X::Acquire(...) */

#endif // HW_DIN_H_INCLUDED
