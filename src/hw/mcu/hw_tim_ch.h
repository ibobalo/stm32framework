#ifndef HW_TIM_CH_H_INCLUDED
#define HW_TIM_CH_H_INCLUDED

#include "interfaces/timer.h"
#include "util/macros.h"
#include <limits>

//extern
template<unsigned TIM_N> class CHW_TIM;

template<class CHWTIMxCHx>
class THWTIMxCHx_InterfaceWrapper :
	public virtual ITimerChannel
{
public:
	typedef typename CHWTIMxCHx::CHostTimer CHWTIMx;
public: // ITimerChannel
	virtual void SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority));
		CHWTIMxCHx::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority);
	}
	virtual void Enable(bool bEnable = true) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::Enable(bEnable));
		CHWTIMxCHx::Enable(bEnable);
	}
	virtual void SetOutputInvertion(bool bInvert) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::SetOutputInvertion(bInvert));
		CHWTIMxCHx::SetOutputInvertion(bInvert);
	}
	virtual unsigned GetTimerPeriod() const {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::GetTimerPeriod());
		return CHWTIMxCHx::CHostTimer::GetPeriod();
	}
	virtual unsigned GetTimerCounterMax() const {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::GetTimerCounterMax());
		return CHWTIMxCHx::CHostTimer::GetCounterMax();
	}
public: // ITimerChannel::IValueSource<unsigned>,    // actual out power
	virtual unsigned GetValue() const {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::IValueSource::GetValue());
		return CHWTIMxCHx::GetValue();
	}
public: // ITimerChannel::IValueReceiver<unsigned>   // command value
	virtual void SetValue(unsigned uiValue) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::IValueReceiver<unsigned>::SetValue(uiValue));
		CHWTIMxCHx::SetValue(uiValue);
	}
public:
	static void Init(unsigned uiValue, bool bInverted, bool bOpenDrain = false, bool bFast = false);
};

template<class CHWTIMxCHx>
class THWTIMxCHxAdv_InterfaceWrapper :
	public virtual THWTIMxCHx_InterfaceWrapper<CHWTIMxCHx>,
	public virtual ITimerChannelAdv
{
public:
	typedef typename CHWTIMxCHx::CHostTimer CHWTIMx;
public: // ITimerChannelAdv
	virtual void SetPulsesCount(unsigned nPulsesCount) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannelAdv::SetPulsesCount(nPulsesCount));
		CHWTIMxCHx::CHostTimer::SetPulsesCount(nPulsesCount);
	}
	virtual void SetTimerDeadtimeClks(unsigned uiDeadtimeClks) {
		IMPLEMENTS_INTERFACE_METHOD(ITimerChannelAdv::SetTimerDeadtimeClks(uiDeadtimeClks));
		CHWTIMxCHx::CHostTimer::SetDeadtimeClks(uiDeadtimeClks);
	}
};

template<unsigned TIM_N, unsigned CH_N>
class CHW_TIMCH
{
public:
	typedef CHW_TIMCH<TIM_N,CH_N> type;
	typedef CHW_TIM<TIM_N> CHostTimer;
	friend class CHW_TIM<TIM_N>;
	static const unsigned nTimer = TIM_N;
	static const unsigned nChannel = CH_N;
public:
	static type* AcquireAdv(unsigned uiValue, bool bInverted, bool bOpenDrain, bool bFast, bool bSetIdleStates, bool bIdleState) {
		type::InitAdv(uiValue, bInverted, bOpenDrain, bFast, bSetIdleStates, bIdleState);
		static type instance;
		return &instance;
	}
	static type* Acquire(unsigned uiValue, bool bInvert = false, bool bOpenDrain = false, bool bFast = false) {
		type::Init(uiValue, bInvert, bOpenDrain, bFast);
		static type instance;
		return &instance;
	}
	static ITimerChannel* AcquireInterface(unsigned uiValue, bool bInvert = false, bool bOpenDrain = false, bool bFast = false) {
		Acquire(uiValue, bInvert, bOpenDrain, bFast);
		static THWTIMxCHx_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static ITimerChannelAdv* AcquireInterfaceAdv(unsigned uiValue, bool bInverted, bool bOpenDrain, bool bFast, bool bSetIdleStates, bool bIdleState) {
		AcquireAdv(uiValue, bInverted, bOpenDrain, bFast, bSetIdleStates, bIdleState);
		static THWTIMxCHxAdv_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static void Release() {
		type::Deinit();
	}
public:
	static void InitAdv(unsigned uiValue, bool bInvert, bool bOpenDrain = false, bool bFast = false, bool bSetIdleStates = false, bool bIdleState = false);
	static void Init(unsigned uiValue, bool bInvert = false, bool bOpenDrain = false, bool bFast = false);
public:
	static void Deinit();
	static void SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0);
	static void Enable(bool bEnable = true);
	static void SetOutputInvertion(bool bInvert);
	static unsigned GetValue();
	static void SetValue(unsigned uiValue);
	static void SetValue(bool bValue) {SetValue(bValue ? CHostTimer::GetPeriod() : 0);}
	static void SetDutyCycle(float fDuty) { SetValue((unsigned)(CHostTimer::GetPeriod() * LIMIT(fDuty, 0.0, 1.0))); }
	static void SetDutyCycle(double fDuty) { SetValue((unsigned)(CHostTimer::GetPeriod() * LIMIT(fDuty, 0.0, 1.0))); }
	template<class valueT>
	static void SetDutyCycle(valueT uiDuty) {
		static constexpr unsigned uiMaxValue = std::numeric_limits<valueT>::max();
		unsigned uiValue = (
				(CHostTimer::GetCounterMax() > 0xFFFF || uiMaxValue > 0xFFFF)
				? (unsigned)((uint64_t)CHostTimer::GetPeriod() * uiDuty / uiMaxValue)
				: (unsigned)((uint32_t)CHostTimer::GetPeriod() * uiDuty / uiMaxValue));
		SetValue(uiValue);
	}
protected:
	static void ControlInterrupt();
	static void OnTIM_CC();
private:
	static ITimerChannelNotifier* m_pNotifier;
};
/*static*/
template<unsigned TIM_N, unsigned CH_N> ITimerChannelNotifier* CHW_TIMCH<TIM_N, CH_N>::m_pNotifier;


#define ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(TIM_N,CH_N,ID) \
		using CTimerChannel_##ID = CHW_TIMCH<TIM_N,CH_N>; \
	IF_TIM_IS_ADVANCED(TIM_N) ( \
		static inline CTimerChannel_##ID* AcquireAdvTimerChannel_##ID(unsigned uiValue, bool bInverted = false, bool bOpenDrain = false, bool bFast = false, bool bSetIdleStates = false, bool bIdleState = false) { \
			return CTimerChannel_##ID::AcquireAdv(uiValue, bInverted, bOpenDrain, bFast, bSetIdleStates, bIdleState); } \
	) \
		static inline CTimerChannel_##ID* AcquireTimerChannel_##ID(unsigned uiValue, bool bInverted = false, bool bOpenDrain = false, bool bFast = false) { \
			return CTimerChannel_##ID::Acquire(uiValue, bInverted, bOpenDrain, bFast); } \
		static inline void ReleaseTimerChannel_##ID() {CTimerChannel_##ID::Release(); } \
	IF_TIM_IS_ADVANCED(TIM_N) ( \
		static inline ITimerChannelAdv* AcquireAdvTimerChannelInterface_##ID(unsigned uiValue, bool bInverted = false, bool bOpenDrain = false, bool bFast = false, bool bSetIdleStates = false, bool bIdleState = false) { \
			return CTimerChannel_##ID::AcquireInterfaceAdv(uiValue, bInverted, bOpenDrain, bFast, bSetIdleStates, bIdleState); } \
	) \
		static inline ITimerChannel* AcquireTimerChannelInterface_##ID(unsigned uiValue, bool bInverted = false, bool bOpenDrain = false, bool bFast = false) { \
			return CTimerChannel_##ID::AcquireInterface(uiValue, bInverted, bOpenDrain, bFast); } \
		static void ReleaseTimerChannelInterface_##ID() {CTimerChannel_##ID::Release();}

#define ALIAS_TIMER_CHANNEL(ID,ALIAS) \
		using CTimerChannel_##ALIAS = CTimerChannel_##ID; \

#define ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHWBRD,TIM_N,CH_N,ID)  /* DEPRECATED, use CTimer_X::Acquire(...) */

#endif /* HW_TIM_CH_H_INCLUDED */
