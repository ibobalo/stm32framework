#ifndef HW_DOUT_H_INCLUDED
#define HW_DOUT_H_INCLUDED

#include "interfaces/gpio.h"
#include "hw_ports.h" // interface wrappers

template<class CHWDOUTx>
class THWDOUTx_InterfaceWrapper :
		public virtual IDigitalOut
{
public: // IDigitalOut
	virtual void Set() {IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::Set()); CHWDOUTx::Set();}
	virtual void Reset() {IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::Reset()); CHWDOUTx::Reset();}
	virtual void Toggle() {IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::Toggle()); CHWDOUTx::Toggle();}
public: // IValueReceiver<bool>
	virtual void SetValue(bool bValue) {IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IValueReceiver<bool>::SetValue(bValue)); CHWDOUTx::SetValue(bValue);}
public: // IValueSource<bool>
	virtual bool GetValue() const {IMPLEMENTS_INTERFACE_METHOD(IDigitalOut::IValueSource<bool>::GetValue()); return CHWDOUTx::GetValue();}
};

template<class PORTx, unsigned PINn>
class CHW_DOUT
{
public:
	typedef CHW_DOUT<PORTx, PINn> type;
	static inline type* Acquire(bool bInitState, bool bOpenDrain = false, bool bPullUp = false, bool bPullDown = false, bool bFast = false) {
		type::Init(bInitState, bOpenDrain, bPullUp, bPullDown, bFast);
		static type instance;
		return &instance;
	}
	static inline IDigitalOut* AcquireInterface(bool bInitState, bool bOpenDrain = false, bool bPullUp = false, bool bPullDown = false, bool bFast = false) {
		Acquire(bInitState, bOpenDrain, bPullUp, bPullDown, bFast);
		static THWDOUTx_InterfaceWrapper<type> wrapperInstance;
		return &wrapperInstance;
	}
	static inline void Release() {
		type::Deinit();
	}
public:
	static void Init(bool bInitState, bool bOpenDrain = false, bool bPullUp = false, bool bPullDown = false, bool bFast = false);
	static void Deinit();
public:
	static void Set();
	static void Reset();
	static void Toggle();
	static void SetValue(bool bValue);
	static bool GetValue();
};

/* static */
template<class PORTx, unsigned PINn>
void CHW_DOUT<PORTx, PINn>::Init(bool bInitState, bool bOpenDrain, bool bPullUp, bool bPullDown, bool bFast)
{
	PORTx::RCC_Cmd(ENABLE);
	GPIO_WriteBit((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x, bInitState?Bit_SET:Bit_RESET);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIOPin<PINn>::GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = bOpenDrain?GPIO_OType_OD:GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = bFast?GPIO_Speed_50MHz:GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = bPullUp?GPIO_PuPd_UP:(bPullDown?GPIO_PuPd_DOWN:GPIO_PuPd_NOPULL);
	GPIO_Init((GPIO_TypeDef*)PORTx::GPIOx, &GPIO_InitStructure);
}
/*static*/
template<class PORTx, unsigned PINn>
void CHW_DOUT<PORTx, PINn>::Deinit()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIOPin<PINn>::GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init((GPIO_TypeDef*)PORTx::GPIOx, &GPIO_InitStructure);
}
/*static*/
template<class PORTx, unsigned PINn>
bool CHW_DOUT<PORTx, PINn>::GetValue()
{
	return GPIO_ReadInputDataBit((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x);
}
/*static*/
template<class PORTx, unsigned PINn>
void CHW_DOUT<PORTx, PINn>::Set()
{
//	DOUT_DEBUG_INFO("    SET:", 1);
	GPIO_SetBits((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x);
}
/*static*/ \
template<class PORTx, unsigned PINn>
void CHW_DOUT<PORTx, PINn>::Reset()
{
//	DOUT_DEBUG_INFO("  RESET:", 0);
	GPIO_ResetBits((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x);
}
/*static*/ \
template<class PORTx, unsigned PINn>
void CHW_DOUT<PORTx, PINn>::Toggle()
{
//	DOUT_DEBUG_INFO(" TOGGLE:", !GPIO_ReadOutputDataBit((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x));
	IF_STM32F0( GPIO_WriteBit((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x,
			GPIO_ReadOutputDataBit((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x)?Bit_RESET:Bit_SET));
	IF_STM32F4( GPIO_ToggleBits((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x) );
}
/*static*/
template<class PORTx, unsigned PINn>
void CHW_DOUT<PORTx, PINn>::SetValue(bool bValue)
{
//	DOUT_DEBUG_INFO(" CHANGE:", bValue?1:0);
	GPIO_WriteBit((GPIO_TypeDef*)PORTx::GPIOx, GPIOPin<PINn>::GPIO_Pin_x, bValue?Bit_SET:Bit_RESET);
}


#define ACQUIRE_RELEASE_DIGITAL_OUT_DECLARATION(PORT,PIN,ID,...) /* ##__VA_ARGS__ used for prevent early ID expantion */ \
	using CDigitalOut_##ID##__VA_ARGS__ = CHW_DOUT<GPIOPort##PORT, PIN>; \
	static inline CDigitalOut_##ID##__VA_ARGS__* AcquireDigitalOut_##ID##__VA_ARGS__(bool bInitState, bool bOpenDrain = false, bool bPullUp = false, bool bPullDown = false, bool bFast = false) { \
		return CDigitalOut_##ID##__VA_ARGS__::Acquire(bInitState, bOpenDrain, bPullUp, bPullDown, bFast); } \
	static inline void ReleaseDigitalOut_##ID##__VA_ARGS__() {CDigitalOut_##ID##__VA_ARGS__::Release();} \
	static inline IDigitalOut* AcquireDigitalOutInterface_##ID##__VA_ARGS__(bool bInitState, bool bOpenDrain = false, bool bPullUp = false, bool bPullDown = false, bool bFast = false) { \
		return CDigitalOut_##ID##__VA_ARGS__::AcquireInterface(bInitState, bOpenDrain, bPullUp, bPullDown, bFast); } \
	static inline void ReleaseDigitalOutInterface_##ID##__VA_ARGS__() { CDigitalOut_##ID##__VA_ARGS__::Release(); }

#define ALIAS_DIGITAL_OUT(ID,ALIAS) \
	using CDigitalOut_##ALIAS = CDigitalOut_##ID;

#define ACQUIRE_RELEASE_DIGITAL_OUT_DEFINITION(PORT,PIN,CHWBRD,ID,...) /* DEPRECATED, use CDigitalOut_X::Acquire(...) */

#endif /* HW_DOUT_H_INCLUDED */
