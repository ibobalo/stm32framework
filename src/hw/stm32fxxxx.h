#ifndef __STM32FXXXX_H_INCLUDED
#define __STM32FXXXX_H_INCLUDED

#ifdef STM32F030
#  define STM32F0x0
#  define STM32F030xx
#  define IF_STM32F030(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F030(ARGS...)
#endif

#ifdef STM32F031
#  define STM32F031xx
#  define IF_STM32F031(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F031(ARGS...)
#endif

#ifdef STM32F042
#  define STM32F042xx
#  define IF_STM32F042(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F042(ARGS...)
#endif

#ifdef STM32F051
#  define STM32F051xx
#  define IF_STM32F051(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F051(ARGS...)
#endif

#ifdef STM32F070
#  define STM32F0x0
#  define STM32F070xx
#  define IF_STM32F070(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F070(ARGS...)
#endif

#ifdef STM32F072
#  define STM32F072xx
#  define IF_STM32F072(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F072(ARGS...)
#endif

#ifdef STM32F091
#  define STM32F091xx
#  define IF_STM32F091(ARGS...) ARGS
#  define STM32F0
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F091(ARGS...)
#endif

#ifdef STM32F401
#  define STM32F401xx
#  define IF_STM32F401(ARGS...) ARGS
#  define STM32F4
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F401(ARGS...)
#endif

#ifdef STM32F405
#  define STM32F405xx
#  define STM32F40_41xxx
#  define IF_STM32F405(ARGS...) ARGS
#  define STM32F4
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F405(ARGS...)
#endif

#ifdef STM32F407
#  define STM32F407xx
#  define STM32F40_41xxx
#  define IF_STM32F407(ARGS...) ARGS
#  define STM32F4
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F407(ARGS...)
#endif

#ifdef STM32F446
#  define STM32F446xx
#  define IF_STM32F446(ARGS...) ARGS
#  define STM32F4
#  define _MPU_SUPPORTED
#else
#  define IF_STM32F446(ARGS...)
#endif

#ifndef _MPU_SUPPORTED
#  error undefined or unsupported MPU
#endif

#ifdef STM32F0
#  include "stm32f0xx_conf.h"
#  include "stm32f0xx.h"
//#  include "system_stm32f0xx.h"
#  define IF_STM32F0(ARGS...) ARGS
#else
#  define IF_STM32F0(ARGS...)
#endif

#ifdef STM32F4
#  include "stm32f4xx_conf.h"
#  include "stm32f4xx.h"
//#  include "system_stm32f4xx.h"
#  define IF_STM32F4(ARGS...) ARGS
#else
#  define IF_STM32F4(ARGS...)
#endif

#endif //__STM32FXXXX_H_INCLUDED
