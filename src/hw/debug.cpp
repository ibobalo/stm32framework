#include "stdafx.h"
#include "debug.h"
#include "hw.h"
#include "util/macros.h"
#include "util/utils.h"
#include "util/atomic.h"
#include <stdint.h>

#ifndef DEBUG_INFOS_HISTORY
#if RAM_SIZE > 32
#  define DEBUG_INFOS_HISTORY  500
#elif RAM_SIZE > 16
#  define DEBUG_INFOS_HISTORY  100
#else
#  define DEBUG_INFOS_HISTORY  10
#endif
#endif // DEBUG_INFOS_HISTORY

void debug_SendChar (char ch)
{
	IF_STM32F0(UNUSED(ch));
	IF_STM32F4(ITM_SendChar((uint32_t)ch));
}
void debug_SendString (const char* pzsString)
{
	const char* pch = pzsString;
	while (*pch != '\0') {
		debug_SendChar (*pch);
		++pch;
	}
}
void debug_SendUInt (const unsigned int uiValue)
{
	ToString<> converter;
	debug_SendString(converter.Convert(uiValue));
}

typedef struct {
	unsigned    t;
	const char* m;
//	const char* n;
	union {
		int         i;
		void*       v;
	};
	const char* f;
	unsigned    l;
} CDebugInfo;
static CDebugInfo g_aDebugInfos[DEBUG_INFOS_HISTORY];
volatile static CDebugInfo* g_pDebugInfo = &(g_aDebugInfos[0]);

#define g_LastDebugInfos g_aDebugInfos[];

#ifdef __cplusplus
extern "C" {
#endif // cplusplus

void debug_info(const char* file, unsigned line, const char* fn, const char* msg, int i)
{
	UNUSED(fn);
/*
	debug_SendString("CHECK: ");
	debug_SendUInt(uiTime);
	debug_SendChar(' ');
	debug_SendString(file);
	debug_SendChar(':');
	debug_SendUInt(line);
	debug_SendChar('(');
	debug_SendString(fn);
	debug_SendString(") ");
	debug_SendString(msg);
	debug_SendString("\r\n");
*/

	CDebugInfo* pDebugInfo;
	AtomicAddCircle(const_cast<CDebugInfo**>(&g_pDebugInfo), 1, &(g_aDebugInfos[0]), &(g_aDebugInfos[DEBUG_INFOS_HISTORY - 1]), &pDebugInfo);

	pDebugInfo->t = CHW_MCU::GetCLKCounter32();
//	pDebugInfo->n = fn;
	pDebugInfo->m = msg;
	pDebugInfo->i = i;
	pDebugInfo->f = file;
	pDebugInfo->l = line;
}

unsigned get_debug_info_internals(unsigned uiFromMsgIndex, const void** pRetData)
{
	ASSERTE(uiFromMsgIndex < DEBUG_INFOS_HISTORY);

	unsigned uiCurrentMsgIndex = g_pDebugInfo - &(g_aDebugInfos[0]);

	if (uiFromMsgIndex == uiCurrentMsgIndex) {
		return 0;
	}
	*pRetData = &(g_aDebugInfos[uiFromMsgIndex]);
	if (uiFromMsgIndex < uiCurrentMsgIndex) {
		return uiCurrentMsgIndex - uiFromMsgIndex;
	}
	return DEBUG_INFOS_HISTORY - uiFromMsgIndex;
}

unsigned get_debug_info_wrapped_index(unsigned uiUnwrappedMsgIndex)
{
	return uiUnwrappedMsgIndex % DEBUG_INFOS_HISTORY;
}

unsigned get_debug_info_record_size()
{
	return sizeof(CDebugInfo);
}

#ifdef __cplusplus
} // extern "C"
#endif // cplusplus

