#ifndef TICK_TIMEOUT_H_INCLUDED
#define TICK_TIMEOUT_H_INCLUDED

#include "util/timeout.h"
#include "tick_timesource.h"

typedef TTimeoutTemplate<CTickTimeSource> CTimeout;

#endif // TICK_TIMEOUT_H_INCLUDED
