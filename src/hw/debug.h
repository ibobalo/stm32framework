#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED

//#define IRQ_DEBUG_INFO(...) DEBUG_INFO("IRQ:" __VA_ARGS__)
#define IRQ_DEBUG_INFO(...) {}

#if !defined(NO_DEBUG_LOGGING) && (defined(DEBUG) || defined(DEBUG_LOGGING))

#ifdef __cplusplus
extern "C" {
#endif // cplusplus

#define DEBUG_INFO_V(MSG, VALUE, ...) debug_info(__FILE__, __LINE__, __FUNCTION__, MSG, VALUE)
#define DEBUG_INFO(MSG, ...) DEBUG_INFO_V(MSG, ##__VA_ARGS__, 0)
void debug_info(const char* file, unsigned line, const char* fn, const char* msg, int v);

unsigned get_debug_info_internals(unsigned uiFromMsgIndex, const void** pRetData);
unsigned get_debug_info_wrapped_index(unsigned uiUnwrappedMsgIndex);
unsigned get_debug_info_record_size();

#ifdef __cplusplus
} // extern "C"
#endif // cplusplus

#else

#define DEBUG_INFO(...) {}

#endif

#endif // DEBUG_H_INCLUDED
