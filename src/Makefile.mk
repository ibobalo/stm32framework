#$(warning src/Makefile included with APP=$(APP) for $(MAKECMDGOALS))

include src/app/Makefile.mk
ifneq ($(APP),)
include src/modules/Makefile.mk
include src/brd/Makefile.mk
endif
include src/hw/Makefile.mk

INCLUDE_PATH += \
	src

SRC += $(addprefix src/, \
	init.cpp \
	util/utils.cpp \
	$(if $(filter YES,$(USE_TEST)),proc_testhw.cpp,) \
)
