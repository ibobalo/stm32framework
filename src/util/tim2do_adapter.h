#pragma once

template<class TTimCh, class TDo, bool bInvert = false>
class TTimerChannel2DigitalOut_Adapter :
		public ITimerNotifier,
		public ITimerChannelNotifier
{
public:
	using CDo = TDo;
	using CTim = typename TTimCh::CHostTimer;
	using CTimCh = TTimCh;
public:
	void Init(bool bOpenDrain = bInvert, bool bPullUp = false, bool bPullDown = false, bool bFast = false);
public: // ITimerNotifier
	virtual void NotifyTimer() override;
public: // ITimerChannelNotifier
	virtual void NotifyTimerChannel(unsigned nChannel) override;
};

template<class TTimCh, class TDo, bool bInvert>
void TTimerChannel2DigitalOut_Adapter<TTimCh, TDo, bInvert>::Init(bool bOpenDrain, bool bPullUp, bool bPullDown, bool bFast)
{
	CDo::Acquire(bInvert, bOpenDrain, bPullUp, bPullDown, bFast);
	CTim::SetCounter(0);
	CTim::SetNotifier(this, 2);    INDIRECT_CALL(NotifyTimer());
	CTimCh::SetNotifier(this, 3);  INDIRECT_CALL(NotifyTimerChannel(CTimCh::nChannel));
}

/*virtual*/
template<class TTimCh, class TDo, bool bInvert>
void TTimerChannel2DigitalOut_Adapter<TTimCh, TDo, bInvert>::NotifyTimer()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());

//	bool bOn = (CTim::GetCounter() < CTimCh::GetValue());
//	CDo::SetValue(bOn != bInvert);
	CDo::SetValue(!bInvert);
}

/*virtual*/
template<class TTimCh, class TDo, bool bInvert>
void TTimerChannel2DigitalOut_Adapter<TTimCh, TDo, bInvert>::NotifyTimerChannel(unsigned nChannel)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannelNotifier::NotifyTimerChannel(nChannel));

	CDo::SetValue(bInvert);
}


