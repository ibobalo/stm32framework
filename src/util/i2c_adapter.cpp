#include "i2c_adapter.h"
#include "interfaces/devregs.h"

void CI2CRegsAdapter::Init(unsigned uiI2CAddress, IDeviceRegs* pRegs)
{
	m_pRegs = pRegs;
	m_uiI2CAddress = uiI2CAddress;
	m_eState = CI2CRegsAdapter::ST_INIT;

	m_cicRegAddressChunks.Assign(&m_uiRegAddress, sizeof(m_uiRegAddress));
	m_cicRegAddressChunks.Break();

	m_cccRegReadValueChunks.Assign((uint8_t*)m_aDataBuffer, sizeof(m_aDataBuffer));
	m_cccRegReadValueChunks.Break();
	m_cicRegWriteValueChunks.Assign(m_aDataBuffer, sizeof(m_aDataBuffer));
	m_cicRegWriteValueChunks.Break();
}

void CI2CRegsAdapter::SetI2CAddress(unsigned uiI2CAddress)
{
	m_uiI2CAddress = uiI2CAddress;
}

/*virtual*/
unsigned CI2CRegsAdapter::GetI2CAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(ISlaveI2C::GetI2CAddress());
	return m_uiI2CAddress;
}

/*virtual*/
bool CI2CRegsAdapter::GetBufferToReceive(const CChainedIoChunks** ppRetChunksToRecv)
{
	IMPLEMENTS_INTERFACE_METHOD(ISlaveI2C::GetBufferToReceive(ppRetChunksToRecv));
	ASSERTE(ppRetChunksToRecv);

	bool bResult = false;

	switch (m_eState) {
	case CI2CRegsAdapter::ST_INIT:
		ASSERTE(m_uiRegSize == 0);
		*ppRetChunksToRecv = &m_cicRegAddressChunks;
		m_eState = CI2CRegsAdapter::ST_ADDR;
		bResult = true;
		break;
	case CI2CRegsAdapter::ST_ADDR: // write first reg after addr
		m_eState = CI2CRegsAdapter::ST_WRITE;
		*ppRetChunksToRecv = &m_cicRegWriteValueChunks;
		bResult = true;
		break;
	case CI2CRegsAdapter::ST_WRITE: { // autoincrement write reg address
		if (SetRegValues(ARRAY_SIZE(m_aDataBuffer))) {
			*ppRetChunksToRecv = &m_cicRegWriteValueChunks;
		}
	} break;
	case CI2CRegsAdapter::ST_READ:
		ASSERTE(false); // direction changed without OnI2CSessionDone
		m_eState = CI2CRegsAdapter::ST_ERROR;
		break;
	case CI2CRegsAdapter::ST_ERROR:
		// do nothing
		break;
	}
	return bResult;
}

/*virtual*/
bool CI2CRegsAdapter::GetDataToSend(const CChainedConstChunks** ppRetChunksToSend)
{
	IMPLEMENTS_INTERFACE_METHOD(ISlaveI2C::GetDataToSend(ppRetChunksToSend));

	bool bResult = false;

	switch (m_eState) {
	case CI2CRegsAdapter::ST_INIT: // use previous reg address
	case CI2CRegsAdapter::ST_ADDR:
		m_eState = CI2CRegsAdapter::ST_READ;
		m_uiRegSize = m_pRegs->GetRegValue(m_uiRegAddress, (uint8_t*)m_aDataBuffer, sizeof(m_aDataBuffer));
		if (m_uiRegSize) {
			m_cccRegReadValueChunks.uiLength = MIN2(m_uiRegSize, ARRAY_SIZE(m_aDataBuffer));
			*ppRetChunksToSend = &m_cccRegReadValueChunks;
			bResult = true;
		}
		break;
	case CI2CRegsAdapter::ST_WRITE:
		m_eState = CI2CRegsAdapter::ST_READ;
		SetRegValues(ARRAY_SIZE(m_aDataBuffer));
		// continue without break;
	case CI2CRegsAdapter::ST_READ: // autoincrement read next reg address
		if (m_uiRegSize) {
			if (m_uiRegSize <= ARRAY_SIZE(m_aDataBuffer)) {
				++m_uiRegAddress;
			}
			m_uiRegSize = m_pRegs->GetRegValue(m_uiRegAddress, (uint8_t*)m_aDataBuffer, sizeof(m_aDataBuffer));
			if (m_uiRegSize) {
				m_cccRegReadValueChunks.uiLength = MIN2(m_uiRegSize, ARRAY_SIZE(m_aDataBuffer));
				*ppRetChunksToSend = &m_cccRegReadValueChunks;
				bResult = true;
			}
		}
		break;
	case CI2CRegsAdapter::ST_ERROR:
		// do nothing
		break;
	}
	return bResult;
}

/*virtual*/
void CI2CRegsAdapter::OnI2CSessionDone(unsigned nBytesCompleted)
{
	IMPLEMENTS_INTERFACE_METHOD(ISlaveI2C::OnI2CSessionDone(nBytesCompleted));

	if (m_eState == CI2CRegsAdapter::ST_WRITE) {
		SetRegValues(nBytesCompleted);
	}
	m_eState = CI2CRegsAdapter::ST_INIT;
	m_uiRegSize = 0;
}

/*virtual*/
void CI2CRegsAdapter::OnI2CError(ErrorType e)
{
	IMPLEMENTS_INTERFACE_METHOD(ISlaveI2C::OnI2CError(e));
	m_uiRegSize = 0;
	m_eState = CI2CRegsAdapter::ST_INIT;
}

bool CI2CRegsAdapter::SetRegValues(unsigned uiDataSize)
{
	bool bResult = true;
	unsigned uiDataOffset = 0;
	do {
		unsigned uiProcessedSize = m_pRegs->SetRegValue(m_uiRegAddress, (uint8_t*)m_aDataBuffer + uiDataOffset, uiDataSize);
		if (!uiProcessedSize) { // error. ignore all following data
			bResult = false;
			break;
		} else if (uiProcessedSize > uiDataSize) { // more data required
			break;
		} else { // success
			++m_uiRegAddress;
			uiDataSize -= uiProcessedSize;
			uiDataOffset += uiProcessedSize;
		}
	} while (uiDataSize);
	return bResult;
}
