#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED

#include "hw/debug.h"

#ifdef __cplusplus
extern "C" {
#endif

void Default_Handler(void);

#ifdef  USE_FULL_ASSERT
/*
 * ASSERTS
 */
#if !defined(NO_ASSERT_LOGGING)
# define ASSERT_DEBUG_INFO(msg, ...) {DEBUG_INFO("ASSERT:" msg, ##__VA_ARGS__);}
#else
# define ASSERT_DEBUG_INFO(...) {}
#endif

#define ASSERT(msg, ...) {ASSERT_DEBUG_INFO(msg, ##__VA_ARGS__); assert_failed((uint8_t *)__FILE__, __LINE__);}
#define ASSERTE(condition, ...) {if (!(condition) ) {ASSERT(#condition " " __VA_ARGS__);}}
#define assert_failed(...) Default_Handler()


#else

#define ASSERT(msg, ...) {}
#define ASSERTE(condition, ...) {}

#endif

#ifdef __cplusplus
}
#endif

#endif // CORE_H_INCLUDED
