#ifndef MOVEPROFILE_H_INCLUDED
#define MOVEPROFILE_H_INCLUDED

#include <math.h>

typedef struct TProfilePoint_t TProfilePoint;
typedef struct TProfile_t TProfile;

#define EPSILON_T 0.001

unsigned SolveSquareEquation(float a, float b, float c, float& x1, float& x2);
unsigned SolveSquareEquationOpt(float a, float b, float c, float& x1, float& x2);
unsigned CalculateStopMoveProfile(const TProfilePoint_t& from, float dcc, TProfilePoint* points);
unsigned CalculateTrapezoidMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points);
unsigned CalculateFromStopTriangleMoveProfile(const TProfilePoint_t& from, float tgt, float acc, float dcc, TProfilePoint* points);
unsigned CalculateTriangleMoveProfile(const TProfilePoint_t& from, float tgt, float acc, float dcc, TProfilePoint* points);
unsigned CalculateFromStopMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points);
unsigned CalculateUnidirectionMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points);
unsigned CalculateMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points);

typedef struct TProfilePoint_t
{
public:
	void Reset() { t = p = v = 0.0; }
	void Assign(float s_t, float s_p, float s_v) { t = s_t; p = s_p; v = s_v; }
	void Assign(const TProfilePoint_t& src) { t = src.t; p = src.p; v = src.v; }
	void CalculateAccelerate(const TProfilePoint_t& from, float ta, float a);
	void CalculateStop(const TProfilePoint_t& from, float dcc);
	void CalculateMiddle(float mid_t, const TProfilePoint_t& prev, const TProfilePoint_t& next);
	float ApproximateAccTo(const TProfilePoint_t& next) const;
public:
	float t;
	float p;
	float v;
} TProfilePoint;


typedef struct TProfile_t
{
public:
	void Reset() {
		len = 0;
	}
	bool CalculateStop(const TProfilePoint_t& from, float dcc) {
		len = ::CalculateStopMoveProfile(from, dcc, points);
		return len > 0;
	}
	bool CalculateTrapezoid(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc) {
		len = ::CalculateTrapezoidMoveProfile(from, tgt, vel, acc, dcc, points);
		return len > 0;
	}
	bool CalculateTriangleFromStop(const TProfilePoint_t& from, float tgt, float acc, float dcc) {
		len = ::CalculateFromStopTriangleMoveProfile(from, tgt, acc, dcc, points);
		return len > 0;
	}
	bool CalculateTriangle(const TProfilePoint_t& from, float tgt, float acc, float dcc) {
		len = ::CalculateTriangleMoveProfile(from, tgt, acc, dcc, points);
		return len > 0;
	}
	bool CalculateFromStop(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc) {
		len = ::CalculateFromStopMoveProfile(from, tgt, vel, acc, dcc, points);
		return len > 0;
	}
	bool CalculateUnidirection(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc) {
		len = ::CalculateUnidirectionMoveProfile(from, tgt, vel, acc, dcc, points);
		return len > 0;
	}
	bool Calculate(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc) {
		len = ::CalculateMoveProfile(from, tgt, vel, acc, dcc, points);
		return len > 0;
	}
public:
	unsigned len;
	TProfilePoint points[5];
} TProfile;


#endif // MOVEPROFILE_H_INCLUDED
