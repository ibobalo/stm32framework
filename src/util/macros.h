#ifndef MACROS_H_INCLUDED
#define MACROS_H_INCLUDED


#define MACRO_CLUE2(a, b)           a ## b
#define MACRO_CLUE3(a, b, c)        a ## b ## c
#define MACRO_CLUE4(a, b, c, d)     a ## b ## c ## d
#define MACRO_CLUE5(a, b, c, d, e)  a ## b ## c ## d ## e
#define CC(a, b)             MACRO_CLUE2(a, b)
#define CCC(a, b, c)         MACRO_CLUE3(a, b, c)
#define CCCC(a, b, c, d)     MACRO_CLUE4(a, b, c, d)
#define CCCCC(a, b, c, d, e) MACRO_CLUE5(a, b, c, d, e)

#define EMPTY()
#define DEFER(x) x EMPTY()
#define OBSTRUCT(...) __VA_ARGS__ DEFER(EMPTY)()
#define EXPAND(...) __VA_ARGS__
#define EAT(...)
#define COMMA ,

#define MACRO_FIRST(a, ...) a
#define FIRST(a, ...) MACRO_FIRST(a,EMPTY())
#define MACRO_SECOND(a, b, ...) b
#define SECOND(...) MACRO_SECOND(__VA_ARGS__,EMPTY(),EMPTY())
#define MACRO_THIRD(a, b, c, ...) c
#define THIRD(...) MACRO_THIRD(__VA_ARGS__,EMPTY(),EMPTY(),EMPTY())
#define MACRO_FORTH(a, b, c, d, ...) d
#define FORTH(...) MACRO_FORTH(__VA_ARGS__,EMPTY(),EMPTY(),EMPTY(),EMPTY())
#define MACRO_REST(a, ...) __VA_ARGS__
#define REST(a, ...) MACRO_REST(a,EMPTY())

# define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)
# define CHECK_N(x, n, ...) n
#define CHECK(...) CHECK_N(__VA_ARGS__, 0,)
# define _PROBE(x) x, 1,
#define NOT(x) CHECK(PRIMITIVE_CAT(_NOT_, x))
# define _NOT_0 _PROBE(~)
# define _NOT_ _PROBE(~)
#define CHECK_EMPTY(x) CHECK(PRIMITIVE_CAT(CHECK_EMPTY_, x))
# define CHECK_EMPTY_ _PROBE(~)
#define IS_PAREN(x) CHECK(_IS_PAREN_PROBE x)
# define _IS_PAREN_PROBE(...) _PROBE(~)
#define COMPL(b) PRIMITIVE_CAT(COMPL_, b)
#define COMPL_0 1
#define COMPL_1 0
#define BOOL(condition) COMPL(NOT(condition))
#define IIF(condition) PRIMITIVE_CAT(_IIF_, condition)
# define _IIF_0(t, ...) __VA_ARGS__
# define _IIF_1(t, ...) t
#define IF(condition) IIF(BOOL(condition))
//IF(1)(true) -> true
//IF(0)(true) ->
//IF(0)(true,false) -> false
#define IF_ELSE(condition) IIF_ELSE(BOOL(condition))
# define IIF_ELSE(condition) PRIMITIVE_CAT(_IIF_ELSE_, condition)
# define _IIF_ELSE_1(...) __VA_ARGS__ _IIF_ELSE_1_ELSE
# define _IIF_ELSE_0(...)             _IIF_ELSE_0_ELSE
# define _IIF_ELSE_1_ELSE(...)
# define _IIF_ELSE_0_ELSE(...) __VA_ARGS__
//IF_ELSE(1)(true)(false) -> true
//IF_ELSE(0)(true)(false) -> false
#define WHEN(condition) IF(condition)(EXPAND, EAT)
#define WHEN_NOT(condition) IF(condition)(EAT, EXPAND)
//WHEN(1)(macro(x)) -> expanded macro(x)
//WHEN(0)(macro(x)) ->
#define WHEN_ELSE(condition) IWHEN_ELSE(BOOL(condition))
# define IWHEN_ELSE(condition) PRIMITIVE_CAT(_IWHEN_ELSE_, condition)
# define _IWHEN_ELSE_1(...) EXPAND(__VA_ARGS__) _IWHEN_ELSE_1_ELSE
# define _IWHEN_ELSE_0(...) EAT(__VA_ARGS__)    _IWHEN_ELSE_0_ELSE
# define _IWHEN_ELSE_1_ELSE(...) EAT(__VA_ARGS__)
# define _IWHEN_ELSE_0_ELSE(...) EXPAND(__VA_ARGS__)
//WHEN_ESLE(1)(expr1)(expr2) -> expanded expr1
//WHEN_ESLE(0)(expr1)(expr2) -> expanded expr2

#define _ARG256( \
		_00, _01, _02, _03, _04, _05, _06, _07, _08, _09, _0A, _0B, _0C, _0D, _0E, _0F, \
		_10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _1A, _1B, _1C, _1D, _1E, _1F, \
		_20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _2A, _2B, _2C, _2D, _2E, _2F, \
		_30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _3A, _3B, _3C, _3D, _3E, _3F, \
		_40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _4A, _4B, _4C, _4D, _4E, _4F, \
		_50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _5A, _5B, _5C, _5D, _5E, _5F, \
		_60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _6A, _6B, _6C, _6D, _6E, _6F, \
		_70, _71, _72, _73, _74, _75, _76, _77, _78, _79, _7A, _7B, _7C, _7D, _7E, _7F, \
		_80, _81, _82, _83, _84, _85, _86, _87, _88, _89, _8A, _8B, _8C, _8D, _8E, _8F, \
		_90, _91, _92, _93, _94, _95, _96, _97, _98, _99, _9A, _9B, _9C, _9D, _9E, _9F, \
		_A0, _A1, _A2, _A3, _A4, _A5, _A6, _A7, _A8, _A9, _AA, _AB, _AC, _AD, _AE, _AF, \
		_B0, _B1, _B2, _B3, _B4, _B5, _B6, _B7, _B8, _B9, _BA, _BB, _BC, _BD, _BE, _BF, \
		_C0, _C1, _C2, _C3, _C4, _C5, _C6, _C7, _C8, _C9, _CA, _CB, _CC, _CD, _CE, _CF, \
		_D0, _D1, _D2, _D3, _D4, _D5, _D6, _D7, _D8, _D9, _DA, _DB, _DC, _DD, _DE, _DF, \
		_E0, _E1, _E2, _E3, _E4, _E5, _E6, _E7, _E8, _E9, _EA, _EB, _EC, _ED, _EE, _EF, \
		_F0, _F1, _F2, _F3, _F4, _F5, _F6, _F7, _F8, _F9, _FA, _FB, _FC, _FD, _FE, _FF, \
		...) _FF
#define HAS_COMMA(...) _ARG256(__VA_ARGS__, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
#define ISEMPTY(...) _ISEMPTY(HAS_COMMA(__VA_ARGS__), HAS_COMMA(_TRIGGER_PARENTHESIS_ __VA_ARGS__), HAS_COMMA(__VA_ARGS__ (/*empty*/)), HAS_COMMA(_TRIGGER_PARENTHESIS_ __VA_ARGS__ (/*empty*/)))
#define _TRIGGER_PARENTHESIS_(...) ,
#define _ISEMPTY(_0, _1, _2, _3) HAS_COMMA(CCCCC(_IS_EMPTY_CASE_, _0, _1, _2, _3))
#define _IS_EMPTY_CASE_0001 ,

#define MACRO_INC(x) PRIMITIVE_CAT(_INC_, x)
#  define _INC_0 1
#  define _INC_1 2
#  define _INC_2 3
#  define _INC_3 4
#  define _INC_4 5
#  define _INC_5 6
#  define _INC_6 7
#  define _INC_7 8
#  define _INC_8 9
#  define _INC_9 10
#  define _INC_10 11
#  define _INC_11 12
#  define _INC_12 13
#  define _INC_13 14
#  define _INC_14 15
#  define _INC_15 16
#  define _INC_16 17
#  define _INC_17 18
#  define _INC_18 19
#  define _INC_19 20
#  define _INC_20 21
#  define _INC_21 22
#  define _INC_22 23
#  define _INC_23 24
#  define _INC_24 25
#  define _INC_25 26
#  define _INC_26 27
#  define _INC_27 28
#  define _INC_28 29
#  define _INC_29 30
#  define _INC_30 31
#  define _INC_31 32
#  define _INC_32 33
#  define _INC_33 34
#  define _INC_34 35
#  define _INC_35 36
#  define _INC_36 37
#  define _INC_37 38
#  define _INC_38 39
#  define _INC_39 40
#  define _INC_40 41
#  define _INC_41 42
#  define _INC_42 43
#  define _INC_43 44
#  define _INC_44 45
#  define _INC_45 46
#  define _INC_46 47
#  define _INC_47 48
#  define _INC_48 49
#  define _INC_49 50
#  define _INC_50 51
#  define _INC_51 52
#  define _INC_52 53
#  define _INC_53 54
#  define _INC_54 55
#  define _INC_55 56
#  define _INC_56 57
#  define _INC_57 58
#  define _INC_58 59
#  define _INC_59 60
#  define _INC_60 61
#  define _INC_61 62
#  define _INC_62 63
#  define _INC_63 64
#  define _INC_64 65
#  define _INC_65 66
#  define _INC_66 67
#  define _INC_67 68
#  define _INC_68 69
#  define _INC_69 70
#  define _INC_70 71
#  define _INC_71 72
#  define _INC_72 73
#  define _INC_73 74
#  define _INC_74 75
#  define _INC_75 76
#  define _INC_76 77
#  define _INC_77 78
#  define _INC_78 79
#  define _INC_79 80
#  define _INC_80 81
#  define _INC_81 82
#  define _INC_82 83
#  define _INC_83 84
#  define _INC_84 85
#  define _INC_85 86
#  define _INC_86 87
#  define _INC_87 88
#  define _INC_88 89
#  define _INC_89 90
#  define _INC_90 91
#  define _INC_91 92
#  define _INC_92 93
#  define _INC_93 94
#  define _INC_94 95
#  define _INC_95 96
#  define _INC_96 97
#  define _INC_97 98
#  define _INC_98 99
#  define _INC_99 100
#  define _INC_100 101
#  define _INC_101 102
#  define _INC_102 103
#  define _INC_103 104
#  define _INC_104 105
#  define _INC_105 106
#  define _INC_106 107
#  define _INC_107 108
#  define _INC_108 109
#  define _INC_109 110
#  define _INC_110 111
#  define _INC_111 112
#  define _INC_112 113
#  define _INC_113 114
#  define _INC_114 115
#  define _INC_115 116
#  define _INC_116 117
#  define _INC_117 118
#  define _INC_118 119
#  define _INC_119 120
#  define _INC_120 121
#  define _INC_121 122
#  define _INC_122 123
#  define _INC_123 124
#  define _INC_124 125
#  define _INC_125 126
#  define _INC_126 127
#  define _INC_127 128
#  define _INC_128 129
#  define _INC_129 130
#  define _INC_130 131
#  define _INC_131 132
#  define _INC_132 133
#  define _INC_133 134
#  define _INC_134 135
#  define _INC_135 136
#  define _INC_136 137
#  define _INC_137 138
#  define _INC_138 139
#  define _INC_139 140
#  define _INC_140 141
#  define _INC_141 142
#  define _INC_142 143
#  define _INC_143 144
#  define _INC_144 145
#  define _INC_145 146
#  define _INC_146 147
#  define _INC_147 148
#  define _INC_148 149
#  define _INC_149 150
#  define _INC_150 151
#  define _INC_151 152
#  define _INC_152 153
#  define _INC_153 154
#  define _INC_154 155
#  define _INC_155 156
#  define _INC_156 157
#  define _INC_157 158
#  define _INC_158 159
#  define _INC_159 160
#  define _INC_160 161
#  define _INC_161 162
#  define _INC_162 163
#  define _INC_163 164
#  define _INC_164 165
#  define _INC_165 166
#  define _INC_166 167
#  define _INC_167 168
#  define _INC_168 169
#  define _INC_169 170
#  define _INC_170 171
#  define _INC_171 172
#  define _INC_172 173
#  define _INC_173 174
#  define _INC_174 175
#  define _INC_175 176
#  define _INC_176 177
#  define _INC_177 178
#  define _INC_178 179
#  define _INC_179 180
#  define _INC_180 181
#  define _INC_181 182
#  define _INC_182 183
#  define _INC_183 184
#  define _INC_184 185
#  define _INC_185 186
#  define _INC_186 187
#  define _INC_187 188
#  define _INC_188 189
#  define _INC_189 190
#  define _INC_190 191
#  define _INC_191 192
#  define _INC_192 193
#  define _INC_193 194
#  define _INC_194 195
#  define _INC_195 196
#  define _INC_196 197
#  define _INC_197 198
#  define _INC_198 199
#  define _INC_199 200
#  define _INC_200 201
#  define _INC_201 202
#  define _INC_202 203
#  define _INC_203 204
#  define _INC_204 205
#  define _INC_205 206
#  define _INC_206 207
#  define _INC_207 208
#  define _INC_208 209
#  define _INC_209 210
#  define _INC_210 211
#  define _INC_211 212
#  define _INC_212 213
#  define _INC_213 214
#  define _INC_214 215
#  define _INC_215 216
#  define _INC_216 217
#  define _INC_217 218
#  define _INC_218 219
#  define _INC_219 220
#  define _INC_220 221
#  define _INC_221 222
#  define _INC_222 223
#  define _INC_223 224
#  define _INC_224 225
#  define _INC_225 226
#  define _INC_226 227
#  define _INC_227 228
#  define _INC_228 229
#  define _INC_229 230
#  define _INC_230 231
#  define _INC_231 232
#  define _INC_232 233
#  define _INC_233 234
#  define _INC_234 235
#  define _INC_235 236
#  define _INC_236 237
#  define _INC_237 238
#  define _INC_238 239
#  define _INC_239 240
#  define _INC_240 241
#  define _INC_241 242
#  define _INC_242 243
#  define _INC_243 244
#  define _INC_244 245
#  define _INC_245 246
#  define _INC_246 247
#  define _INC_247 248
#  define _INC_248 249
#  define _INC_249 250
#  define _INC_250 251
#  define _INC_251 252
#  define _INC_252 253
#  define _INC_253 254
#  define _INC_254 255
#  define _INC_255 256
#  define _INC_256 257
#  define _INC_257 258
#  define _INC_258 259
#  define _INC_259 260
#  define _INC_260 261
#  define _INC_261 262
#  define _INC_262 263
#  define _INC_263 264
#  define _INC_264 265
#  define _INC_265 266
#  define _INC_266 267
#  define _INC_267 268
#  define _INC_268 269
#  define _INC_269 270
#  define _INC_270 271
#  define _INC_271 272
#  define _INC_272 273
#  define _INC_273 274
#  define _INC_274 275
#  define _INC_275 276
#  define _INC_276 277
#  define _INC_277 278
#  define _INC_278 279
#  define _INC_279 280
#  define _INC_280 281
#  define _INC_281 282
#  define _INC_282 283
#  define _INC_283 284
#  define _INC_284 285
#  define _INC_285 286
#  define _INC_286 287
#  define _INC_287 288
#  define _INC_288 289
#  define _INC_289 290
#  define _INC_290 291
#  define _INC_291 292
#  define _INC_292 293
#  define _INC_293 294
#  define _INC_294 295
#  define _INC_295 296
#  define _INC_296 297
#  define _INC_297 298
#  define _INC_298 299
#  define _INC_299 300
#  define _INC_300 301
//MACRO_INC(8) -> 9

#define MACRO_ADD(x, y) PRIMITIVE_CAT(_MACRO_ADD_,y)(x)
#  define _MACRO_ADD_0(x) x
#  define _MACRO_ADD_1(x) MACRO_INC(x)
#  define _MACRO_ADD_2(x) MACRO_INC(MACRO_INC(x))
#  define _MACRO_ADD_3(x) MACRO_INC(MACRO_INC(MACRO_INC(x)))
#  define _MACRO_ADD_4(x) MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(x))))
#  define _MACRO_ADD_5(x) MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(x)))))
#  define _MACRO_ADD_6(x) MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(x))))))
#  define _MACRO_ADD_7(x) MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(x)))))))
#  define _MACRO_ADD_8(x) MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(x))))))))
#  define _MACRO_ADD_9(x) MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(MACRO_INC(x)))))))))
//MACRO_ADD(4,3) -> 9

#define MACRO_DEC(x) PRIMITIVE_CAT(_DEC_, x)
#  define _DEC_0 0
#  define _DEC_1 0
#  define _DEC_2 1
#  define _DEC_3 2
#  define _DEC_4 3
#  define _DEC_5 4
#  define _DEC_6 5
#  define _DEC_7 6
#  define _DEC_8 7
#  define _DEC_9 8
#  define _DEC_10 9
#  define _DEC_11 10
#  define _DEC_12 11
#  define _DEC_13 12
#  define _DEC_14 13
#  define _DEC_15 14
#  define _DEC_16 15
#  define _DEC_17 16
#  define _DEC_18 17
#  define _DEC_19 18
#  define _DEC_20 19
#  define _DEC_21 20
#  define _DEC_22 21
#  define _DEC_23 22
#  define _DEC_24 23
#  define _DEC_25 24
#  define _DEC_26 25
#  define _DEC_27 26
#  define _DEC_28 27
#  define _DEC_29 28
#  define _DEC_30 29
#  define _DEC_31 30
#  define _DEC_32 31
#  define _DEC_33 32
#  define _DEC_34 33
#  define _DEC_35 34
#  define _DEC_36 35
#  define _DEC_37 36
#  define _DEC_38 37
#  define _DEC_39 38
#  define _DEC_40 39
#  define _DEC_41 40
#  define _DEC_42 41
#  define _DEC_43 42
#  define _DEC_44 43
#  define _DEC_45 44
#  define _DEC_46 45
#  define _DEC_47 46
#  define _DEC_48 47
#  define _DEC_49 48
#  define _DEC_50 49
#  define _DEC_51 50
#  define _DEC_52 51
#  define _DEC_53 52
#  define _DEC_54 53
#  define _DEC_55 54
#  define _DEC_56 55
#  define _DEC_57 56
#  define _DEC_58 57
#  define _DEC_59 58
#  define _DEC_60 59
#  define _DEC_61 60
#  define _DEC_62 61
#  define _DEC_63 62
#  define _DEC_64 63
#  define _DEC_65 64
#  define _DEC_66 65
#  define _DEC_67 66
#  define _DEC_68 67
#  define _DEC_69 68
#  define _DEC_70 69
#  define _DEC_71 70
#  define _DEC_72 71
#  define _DEC_73 72
#  define _DEC_74 73
#  define _DEC_75 74
#  define _DEC_76 75
#  define _DEC_77 76
#  define _DEC_78 77
#  define _DEC_79 78
#  define _DEC_80 79
#  define _DEC_81 80
#  define _DEC_82 81
#  define _DEC_83 82
#  define _DEC_84 83
#  define _DEC_85 84
#  define _DEC_86 85
#  define _DEC_87 86
#  define _DEC_88 87
#  define _DEC_89 88
#  define _DEC_90 89
#  define _DEC_91 90
#  define _DEC_92 91
#  define _DEC_93 92
#  define _DEC_94 93
#  define _DEC_95 94
#  define _DEC_96 95
#  define _DEC_97 96
#  define _DEC_98 97
#  define _DEC_99 98
#  define _DEC_100 99
#  define _DEC_101 100
#  define _DEC_102 101
#  define _DEC_103 102
#  define _DEC_104 103
#  define _DEC_105 104
#  define _DEC_106 105
#  define _DEC_107 106
#  define _DEC_108 107
#  define _DEC_109 108
#  define _DEC_110 109
#  define _DEC_111 110
#  define _DEC_112 111
#  define _DEC_113 112
#  define _DEC_114 113
#  define _DEC_115 114
#  define _DEC_116 115
#  define _DEC_117 116
#  define _DEC_118 117
#  define _DEC_119 118
#  define _DEC_120 119
#  define _DEC_121 120
#  define _DEC_122 121
#  define _DEC_123 122
#  define _DEC_124 123
#  define _DEC_125 124
#  define _DEC_126 125
#  define _DEC_127 126
#  define _DEC_128 127
#  define _DEC_129 128
#  define _DEC_130 129
#  define _DEC_131 130
#  define _DEC_132 131
#  define _DEC_133 132
#  define _DEC_134 133
#  define _DEC_135 134
#  define _DEC_136 135
#  define _DEC_137 136
#  define _DEC_138 137
#  define _DEC_139 138
#  define _DEC_140 139
#  define _DEC_141 140
#  define _DEC_142 141
#  define _DEC_143 142
#  define _DEC_144 143
#  define _DEC_145 144
#  define _DEC_146 145
#  define _DEC_147 146
#  define _DEC_148 147
#  define _DEC_149 148
#  define _DEC_150 149
#  define _DEC_151 150
#  define _DEC_152 151
#  define _DEC_153 152
#  define _DEC_154 153
#  define _DEC_155 154
#  define _DEC_156 155
#  define _DEC_157 156
#  define _DEC_158 157
#  define _DEC_159 158
#  define _DEC_160 159
#  define _DEC_161 160
#  define _DEC_162 161
#  define _DEC_163 162
#  define _DEC_164 163
#  define _DEC_165 164
#  define _DEC_166 165
#  define _DEC_167 166
#  define _DEC_168 167
#  define _DEC_169 168
#  define _DEC_170 169
#  define _DEC_171 170
#  define _DEC_172 171
#  define _DEC_173 172
#  define _DEC_174 173
#  define _DEC_175 174
#  define _DEC_176 175
#  define _DEC_177 176
#  define _DEC_178 177
#  define _DEC_179 178
#  define _DEC_180 179
#  define _DEC_181 180
#  define _DEC_182 181
#  define _DEC_183 182
#  define _DEC_184 183
#  define _DEC_185 184
#  define _DEC_186 185
#  define _DEC_187 186
#  define _DEC_188 187
#  define _DEC_189 188
#  define _DEC_190 189
#  define _DEC_191 190
#  define _DEC_192 191
#  define _DEC_193 192
#  define _DEC_194 193
#  define _DEC_195 194
#  define _DEC_196 195
#  define _DEC_197 196
#  define _DEC_198 197
#  define _DEC_199 198
#  define _DEC_200 199
#  define _DEC_201 200
#  define _DEC_202 201
#  define _DEC_203 202
#  define _DEC_204 203
#  define _DEC_205 204
#  define _DEC_206 205
#  define _DEC_207 206
#  define _DEC_208 207
#  define _DEC_209 208
#  define _DEC_210 209
#  define _DEC_211 210
#  define _DEC_212 211
#  define _DEC_213 212
#  define _DEC_214 213
#  define _DEC_215 214
#  define _DEC_216 215
#  define _DEC_217 216
#  define _DEC_218 217
#  define _DEC_219 218
#  define _DEC_220 219
#  define _DEC_221 220
#  define _DEC_222 221
#  define _DEC_223 222
#  define _DEC_224 223
#  define _DEC_225 224
#  define _DEC_226 225
#  define _DEC_227 226
#  define _DEC_228 227
#  define _DEC_229 228
#  define _DEC_230 229
#  define _DEC_231 230
#  define _DEC_232 231
#  define _DEC_233 232
#  define _DEC_234 233
#  define _DEC_235 234
#  define _DEC_236 235
#  define _DEC_237 236
#  define _DEC_238 237
#  define _DEC_239 238
#  define _DEC_240 239
#  define _DEC_241 240
#  define _DEC_242 241
#  define _DEC_243 242
#  define _DEC_244 243
#  define _DEC_245 244
#  define _DEC_246 245
#  define _DEC_247 246
#  define _DEC_248 247
#  define _DEC_249 248
#  define _DEC_250 249
#  define _DEC_251 250
#  define _DEC_252 251
#  define _DEC_253 252
#  define _DEC_254 253
#  define _DEC_255 254
#  define _DEC_256 255
#  define _DEC_257 256
#  define _DEC_258 257
#  define _DEC_259 258
#  define _DEC_260 259
#  define _DEC_261 260
#  define _DEC_262 261
#  define _DEC_263 262
#  define _DEC_264 263
#  define _DEC_265 264
#  define _DEC_266 265
#  define _DEC_267 266
#  define _DEC_268 267
#  define _DEC_269 268
#  define _DEC_270 269
#  define _DEC_271 270
#  define _DEC_272 271
#  define _DEC_273 272
#  define _DEC_274 273
#  define _DEC_275 274
#  define _DEC_276 275
#  define _DEC_277 276
#  define _DEC_278 277
#  define _DEC_279 278
#  define _DEC_280 279
#  define _DEC_281 280
#  define _DEC_282 281
#  define _DEC_283 282
#  define _DEC_284 283
#  define _DEC_285 284
#  define _DEC_286 285
#  define _DEC_287 286
#  define _DEC_288 287
#  define _DEC_289 288
#  define _DEC_290 289
#  define _DEC_291 290
#  define _DEC_292 291
#  define _DEC_293 292
#  define _DEC_294 293
#  define _DEC_295 294
#  define _DEC_296 295
#  define _DEC_297 296
#  define _DEC_298 297
#  define _DEC_299 298
#  define _DEC_300 299
//MACRO_DEC(8) -> 7

#define MACRO_SUB(x, y) PRIMITIVE_CAT(_MACRO_SUB_,y)(x)
#  define _MACRO_SUB_0(x) x
#  define _MACRO_SUB_1(x) MACRO_DEC(x)
#  define _MACRO_SUB_2(x) MACRO_DEC(MACRO_DEC(x))
#  define _MACRO_SUB_3(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(x)))
#  define _MACRO_SUB_4(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(x))))
#  define _MACRO_SUB_5(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(x)))))
#  define _MACRO_SUB_6(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(x))))))
#  define _MACRO_SUB_7(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(x)))))))
#  define _MACRO_SUB_8(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(x))))))))
#  define _MACRO_SUB_9(x) MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(MACRO_DEC(x)))))))))
//MACRO_SUB(4,3) -> 1

#define EVAL(...) _MACRO_EVAL32(__VA_ARGS__)
#  define _MACRO_EVAL1024(...) _MACRO_EVAL512(_MACRO_EVAL512(__VA_ARGS__))
#  define _MACRO_EVAL512(...) _MACRO_EVAL256(_MACRO_EVAL256(__VA_ARGS__))
#  define _MACRO_EVAL256(...) _MACRO_EVAL128(_MACRO_EVAL128(__VA_ARGS__))
#  define _MACRO_EVAL128(...) _MACRO_EVAL64(_MACRO_EVAL64(__VA_ARGS__))
#  define _MACRO_EVAL64(...) _MACRO_EVAL32(_MACRO_EVAL32(__VA_ARGS__))
#  define _MACRO_EVAL32(...) _MACRO_EVAL16(_MACRO_EVAL16(__VA_ARGS__))
#  define _MACRO_EVAL16(...) _MACRO_EVAL8(_MACRO_EVAL8(__VA_ARGS__))
#  define _MACRO_EVAL8(...) _MACRO_EVAL4(_MACRO_EVAL4(__VA_ARGS__))
#  define _MACRO_EVAL4(...) _MACRO_EVAL2(_MACRO_EVAL2(__VA_ARGS__))
#  define _MACRO_EVAL2(...) _MACRO_EVAL1(_MACRO_EVAL1(__VA_ARGS__))
#  define _MACRO_EVAL1(...) __VA_ARGS__

#define SELECT(index, ...) EVAL(MACRO_SELECT(index, ##__VA_ARGS__))
#  define MACRO_SELECT(index, first, ...) \
	IF_ELSE(index) ( \
		OBSTRUCT(_SELECT_INDIRECT) () ( \
			MACRO_DEC(index), __VA_ARGS__, \
		) \
	) ( \
		first \
	)
#  define _SELECT_INDIRECT() MACRO_SELECT
//SELECT(1, null, first, second, third) -> first
//SELECT(3, null, first, second, third) -> third
//SELECT(4, null, first, second, third) ->
//SELECT(MACRO_ADD(1,2), null, first, second, third)) -> third

#define MAP(macro, ...) EVAL(MACRO_MAP(macro, __VA_ARGS__))
#  define MACRO_MAP(macro, first, ...) \
	OBSTRUCT(macro)(first) \
	WHEN(NOT(ISEMPTY(__VA_ARGS__))) ( \
		OBSTRUCT(_MAP_INDIRECT)()(macro, __VA_ARGS__) \
	)
#  define _MAP_INDIRECT() MACRO_MAP
//MAP(M, A, B, C) -> M(A) M(B) M(C)

#define MAP_ARG(macro, arg, ...) EVAL(MACRO_MAP_ARG(macro, arg, __VA_ARGS__))
#  define MACRO_MAP_ARG(macro, arg, first, ...) \
	OBSTRUCT(macro)(arg, first) \
	WHEN(NOT(ISEMPTY(__VA_ARGS__))) ( \
		OBSTRUCT(_MAP_ARG_INDIRECT)()(macro, arg, __VA_ARGS__) \
	)
#  define _MAP_ARG_INDIRECT() MACRO_MAP_ARG
//MAP_ARG(M, x, A, B, C) -> M(x, A) M(x, B) M(x, C)

#define MAP_SEP(macro, separator, ...) EVAL(MACRO_MAP_SEP(macro, separator, __VA_ARGS__))
#  define MACRO_MAP_SEP(macro, separator, first, ...) \
	OBSTRUCT(macro)(first) \
	WHEN(NOT(ISEMPTY(__VA_ARGS__))) ( \
		separator OBSTRUCT(_MAP_SEP_INDIRECT)()(macro, separator, __VA_ARGS__) \
	)
#  define _MAP_SEP_INDIRECT() MACRO_MAP_SEP
//MAP_SEP(M, +, A, B, C) -> M(A) + M(B) + M(C)

#define COUNT(...) (0 MAP(MACRO_COUNTER, ##__VA_ARGS__))
#  define MACRO_COUNTER(x) +1
#define SUM(...) (0 MAP(MACRO_ACCUMULATE, ##__VA_ARGS__))
#  define MACRO_ACCUMULATE(x) + (x)
#define SUM_SQ(...) (0 MAP(MACRO_ACCUMULATE_SQ, ##__VA_ARGS__))
#  define MACRO_ACCUMULATE_SQ(x) + (x)*(x)
//COUNT(a,b,c,d,0,1) -> (0 + 1 + 1 + 1 + 1 + 1 + 1)
//SUM(a,b,c,d,0,1) -> (0 + (a) + (b) + (c) + (d) + (0) + (1))
//SUM_SQ(a,b,0,1) -> (0 + (a)*(a) + (b)*(b) + (0)*(0) + (1)*(1))

#define REPEAT(count, macro, ...) EVAL(MACRO_REPEAT(count, macro, ##__VA_ARGS__))
#  define MACRO_REPEAT(count, macro, ...) \
	WHEN(count) ( \
		OBSTRUCT(_REPEAT_INDIRECT) () ( \
			MACRO_DEC(count), macro, ##__VA_ARGS__ \
		) \
		OBSTRUCT(macro) (MACRO_DEC(count), ##__VA_ARGS__) \
	)
#  define _REPEAT_INDIRECT() MACRO_REPEAT
//REPEAT(4, M) -> M(0) M(1) M(2) M(3)
//REPEAT(4, X, v) -> X(0,v) X(1,v) X(2,v) X(3,v)
//REPEAT(4, MACRO_INC) -> 1 2 3 4
//REPEAT(4, MACRO_ADD, 2) -> 2 3 4 5

#define REPEAT1(count, macro, ...) EVAL(MACRO_REPEAT1(count, macro, ##__VA_ARGS__))
#  define MACRO_REPEAT1(count, macro, ...) \
	WHEN(count) ( \
		OBSTRUCT(_REPEAT1_INDIRECT) () ( \
			MACRO_DEC(count), macro, ##__VA_ARGS__ \
		) \
		OBSTRUCT(macro) (count, ##__VA_ARGS__) \
	)
#  define _REPEAT1_INDIRECT() MACRO_REPEAT1
//REPEAT1(4, M, v) -> M(1,v) M(2,v) M(3,v) M(4,v)

#define REPEAT_SEP(count, separator, macro, ...) EVAL(MACRO_REPEAT_SEP(count, separator, macro, ##__VA_ARGS__))
#  define MACRO_REPEAT_SEP(count, separator, macro, ...) \
	WHEN(count) ( \
		OBSTRUCT(_REPEAT_SEP_INDIRECT) () ( \
			MACRO_DEC(count), separator, macro, ##__VA_ARGS__ \
		) \
		WHEN(MACRO_DEC(count))(separator) OBSTRUCT(macro) (MACRO_DEC(count), ##__VA_ARGS__) \
	)
#  define _REPEAT_SEP_INDIRECT() MACRO_REPEAT_SEP
//REPEAT_SEP(4, +, M, v) -> M(0,v) + M(1,v) + M(2,v) + M(3,v)

#define REPEAT_COMMA(count, macro, ...) EVAL(MACRO_REPEAT_COMMA(count, macro, ##__VA_ARGS__))
#  define MACRO_REPEAT_COMMA(count, macro, ...) \
	WHEN(count) ( \
		OBSTRUCT(_REPEAT_COMMA_INDIRECT) () ( \
			MACRO_DEC(count), macro, ##__VA_ARGS__ \
		) \
		WHEN(MACRO_DEC(count))(COMMA) OBSTRUCT(macro)(MACRO_DEC(count), ##__VA_ARGS__) \
	)
#  define _REPEAT_COMMA_INDIRECT() MACRO_REPEAT_COMMA
//REPEAT_COMMA(4, M, v) -> M(0,v), M(1,v), M(2,v), M(3,v)

#define WHILE(pred, op, ...) EVAL(MACRO_WHILE(pred, op, ##__VA_ARGS__))
#  define MACRO_WHILE(pred, op, ...) \
    IF(pred(__VA_ARGS__)) ( \
        OBSTRUCT(WHILE_INDIRECT) () ( \
            pred, op, op(__VA_ARGS__) \
        ), \
        ##__VA_ARGS__ \
    )
#  define WHILE_INDIRECT() MACRO_WHILE


#define STR(x) _MACRO_STR(x)
#  define _MACRO_STR(x) #x

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define ARRAY_INDEX(x,a) (&(x) - &(a[0]))
#define SIZE_ALIGN(size, alignment) ( ((size + (alignment-1)) / alignment) * alignment )

/*
 * Hints
 * compiler/indexer hints
 * no actual code generated, source code navigation and integrity checks only
 */
#define IMPLEMENTS_INTERFACE_METHOD(method,...) {if (0) {method,##__VA_ARGS__;}}
#define OVERRIDE_METHOD(method,...) {if(0){method,##__VA_ARGS__;}}
#define INDIRECT_CALL(method,...) {if(0){method,##__VA_ARGS__;}}
#define UNUSED(expr) do { (void)(expr); } while (0)

#define ABS(a)      ({ __typeof__ (a) _a = (a); _a >= 0 ? _a : -_a; })
#define SIGN(a)     ({ __typeof__ (a) _a = (a); _a > 0 ? 1 : (_a < 0 ? -1 : 0); })
#define MAX2(a,b)    ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _a : _b; })
#define MIN2(a,b)    ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a < _b ? _a : _b; })
#define LIMIT_UP(x, xmax) ({ __typeof__ (x) _x = (x); ((_x > (xmax)) ? (xmax) : _x); })
#define LIMIT(x, xmin, xmax) ({ __typeof__ (x) _x = (x); STATIC_ASSERTE((xmax) >= (xmin), invalid_range); (_x < (xmin)) ? (xmin) : ((_x > (xmax)) ? (xmax) : _x); })
#define SWAP(a,b)   { __typeof__ (a) _a = (a); a = b; b = _a;}
#define INRANGE(x, xmin, xmax) ({ __typeof__ (x) _x = (x); (_x >= (xmin) && _x <= (xmax));})
#define WRAP(x, xmin, xmax) ({ __typeof__ (x) _x = (x); STATIC_ASSERTE((xmax) > (xmin), invalid_range); (_x < (xmin)) ? (_x + (xmax - xmin)) : ((_x > (xmax)) ? (_x - (xmax - xmin)) : _x); })
#define UNWRAP(dx, range) ({ __typeof__ (dx) _d = (dx); STATIC_ASSERTE((range) > 0, invalid_range); (_d < -(range)/2) ? (_d + (range)) : ((_d > (range)/2) ? (_d - (range)) : _d); })

#ifdef DEBUG
# define IS_EVENT_HANDLER static unsigned m_nUnhandledEvents = 0; ++m_nUnhandledEvents;
# define MARK_EVENT_IS_HANDLED m_nUnhandledEvents = 0
# define ASSERT_EVENT_IS_HANDLED(...) {if (m_nUnhandledEvents > 0) { ASSERT("Unhandled event:" __VA_ARGS__); }}
# define IF_DEBUG(...) {__VA_ARGS__;}
#else
# define IS_EVENT_HANDLER {}
# define MARK_EVENT_IS_HANDLED {}
# define ASSERT_EVENT_IS_HANDLED(...) {}
# define IF_DEBUG(...) {}
#endif

#define STATIC_ASSERTE(condition, name) typedef char static_assertion_##name[(condition)?1:-1] __attribute__((unused))

#define IS_MEMORY_ALIGNED(Ptr, Alignment)	(((unsigned)(Ptr) % (Alignment)) == 0)
#define MEMORY_ALIGN(Size, Alignment)	(((size_t)(Size) + ((Alignment) - 1)) & ~(size_t)((Alignment) - 1))

#endif /* MACROS_H_INCLUDED */
