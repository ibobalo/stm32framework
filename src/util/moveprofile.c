#include "moveprofile.h"
#include <math.h>

#ifndef ASSERT
# define ASSERT(...) {}
#endif
#ifndef ASSERTE
# define ASSERTE(...) {}
#endif


unsigned SolveSquareEquation(float a, float b, float c, float& x1, float& x2)
{
	float D = b * b - a * c * 4;
	if (D > 0.0) {
		float sqrtD = sqrt(D);
		float a2 = a * 2;
		x1 = (-b + sqrtD) / a2;
		x2 = (-b - sqrtD) / a2;
		return 2;
	} else if (D == 0.0) {
		float a2 = a * 2;
		x1 = -b * a2;
		return 1;
	} else {
		return 0;
	}
}

unsigned SolveSquareEquationOpt(float a, float b, float c, float& x1, float& x2, float epsilon)
{
	float k = 0.5 * b;
	float Dk = k * k - a * c;
	if (Dk > epsilon) {
		float sqrtDk = sqrt(Dk);
		float divA = 1.0 / a;
		x1 = (-k + sqrtDk) * divA;
		x2 = (-k - sqrtDk) * divA;
		return 2;
	} else if (Dk >= -epsilon) {
		x1 = -b * a * 2;
		return 1;
	} else {
		return 0;
	}
}

void TProfilePoint_t::CalculateAccelerate(const TProfilePoint_t& from, float ta, float a)
{
	float dv = a * ta;
	t = from.t + ta;
	p = from.p + (from.v + dv * 0.5) * ta;
	v = from.v + dv;
}

void TProfilePoint_t::CalculateStop(const TProfilePoint_t& from, float dcc)
{
	float dcc_s = (from.v < 0) == (dcc < 0.0) ? dcc : -dcc;
	float stop_time =  from.v / dcc_s;
	t = from.t + stop_time;
	p = from.p + from.v * 0.5 * stop_time;
	v = 0;
}

void TProfilePoint_t::CalculateMiddle(float mid_t, const TProfilePoint_t& prev, const TProfilePoint_t& next)
{
	t = mid_t;
	float dt1 = t - prev.t;
	float dt2 = next.t - t;
	if (dt1 >= 0.0 && dt2 > 0.0) {
		float f = dt1 / (dt1 + dt2);
		float dv = (next.v - prev.v) * f;
		p = prev.p + (prev.v + dv * 0.5) * dt1;
		v = prev.v + dv;
	} else if (dt1 < 0) {
		p = prev.p;
		v = prev.v;
	} else {
		p = next.p;
		v = next.v;

	}
}

float TProfilePoint_t::ApproximateAccTo(const TProfilePoint_t& next) const
{
	float dt = next.t - t;
	float dv = next.v - v;
	float acc = (dt != 0.0) ? dv / dt : 0.0;
	return acc;
}


unsigned CalculateStopMoveProfile(const TProfilePoint_t& from, float dcc, TProfilePoint* points)
{
	points[0].Assign(from);
	points[1].CalculateStop(from, dcc);
	return 2;
}

unsigned CalculateTrapezoidMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points)
{
	float dp = tgt - from.p;
	float vel_s = (dp < 0.0) == (vel < 0.0) ? vel : -vel;
	float dv = vel_s - from.v;
	float dcc_s = (vel_s < 0.0) == (dcc < 0.0) ? -dcc : dcc;
	float acc_s = (vel_s < 0.0) == (dv < 0.0)
			? ((vel_s < 0) == (acc < 0.0) ? acc : -acc)
			: dcc_s;
	float ta = dv / acc_s;
	if (ta < EPSILON_T) ta == 0;
	float dpa = (from.v + dv * 0.5) * ta;
	float td = -vel_s / dcc_s;
	if (td < EPSILON_T) td == 0;
	float dpd = vel_s * td * 0.5;
	float dpv = dp - dpa - dpd;
	float tv = dpv / vel_s;
	if (tv < -EPSILON_T) return 0;
	if (tv < EPSILON_T) tv == 0;
	unsigned n = 0;
	points[n++].Assign(from);
	if (ta != 0.0)
		points[n++].Assign(from.t + ta, from.p + dpa, vel_s);
	if (tv != 0.0)
		points[n++].Assign(from.t + ta + tv, tgt - dpd, vel_s);
	if (td != 0.0)
		points[n++].Assign(from.t + ta + tv + td, tgt, 0);
	return n;
}

unsigned CalculateFromStopTriangleMoveProfile(const TProfilePoint_t& from, float tgt, float acc, float dcc, TProfilePoint* points)
{
	ASSERTE(from.v == 0.0);
	float dp = tgt - from.p;
	float acc_s = (dp < 0) == (acc < 0.0) ? acc : -acc;
	float dcc_s = (dp < 0) == (dcc < 0.0) ? -dcc : dcc;
	float A = 0.5 * acc_s * (1.0 - acc_s / dcc_s);
	float CdivA = dp / A;
	float ta;
	if (CdivA > (EPSILON_T * EPSILON_T)) {
		ta = sqrt(CdivA);
	} else if (CdivA >= -(EPSILON_T * EPSILON_T)) {
		ta = 0;
	} else {
		ASSERT("SOLVE FAULT");
		return 0;
	}
	points[0].Assign(from);
	if (ta > 0.0) {
		points[1].CalculateAccelerate(from, ta, acc_s);
		points[2].CalculateStop(points[1], dcc_s);
		points[2].p = tgt;
		return 3;
	} else if (from.v != 0.0) {
		points[1].CalculateStop(points[0], dcc_s);
		points[1].p = tgt;
		return 2;
	} else {
		return 1;
	}
}

unsigned CalculateTriangleMoveProfile(const TProfilePoint_t& from, float tgt, float acc, float dcc, TProfilePoint* points)
{
	float dp = tgt - from.p;
	float acc_s = (dp < 0) == (acc < 0.0) ? acc : -acc;
	float dcc_s = (dp < 0) == (dcc < 0.0) ? -dcc : dcc;
	float inv_dcc = 1.0 / dcc_s;
	float a_d = 1.0 - acc_s * inv_dcc;
	float A = 0.5 * acc_s * a_d;
	float B = from.v * a_d;
	float C = from.p - tgt - from.v * from.v * 0.5 * inv_dcc;
	float ta1, ta2;
	unsigned nta = SolveSquareEquationOpt(A, B, C, ta1, ta2, EPSILON_T * EPSILON_T);
	float ta;
	if (nta == 1) {
		ta = ta1;
	} else if (nta == 2 && ta1 >= -EPSILON_T && ta2 <= EPSILON_T) {
		ta = ta1;
	} else if (nta == 2 && ta1 <= EPSILON_T && ta2 >= -EPSILON_T) {
		ta = ta2;
	} else {
		ASSERT("SOLVE FAULT");
		return 0;
	}
	if (ta >= EPSILON_T && ta <= EPSILON_T) ta = 0.0;
	points[0].Assign(from);
	if (ta > 0.0) {
		points[1].CalculateAccelerate(from, ta, acc_s);
		points[2].CalculateStop(points[1], dcc_s);
		points[2].p = tgt;
		return 3;
	} else if (from.v != 0.0) {
		points[1].CalculateStop(points[0], dcc_s);
		points[1].p = tgt;
		return 2;
	} else {
		ASSERTE(ta >= 0.0);
		return 1;
	}
}

unsigned CalculateMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points)
{
	TProfilePoint stop;
	stop.CalculateStop(from, dcc);
	if ((from.v > 0.0 && stop.p > tgt) || (from.v < 0.0 && stop.p < tgt)) {
		points[0].Assign(from);
		unsigned n = CalculateFromStopMoveProfile(stop, tgt, vel, acc, dcc, points + 1);
		if (n) return n + 1;
		ASSERT("PROFILE CALC FAULT");
	} else {
		unsigned n = CalculateUnidirectionMoveProfile(from, tgt, vel, acc, dcc, points);
		if (n) return n;
		ASSERT("PROFILE CALC FAULT");
	}
	return 0;
}

unsigned CalculateFromStopMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points)
{
	ASSERTE(from.v == 0.0);
	unsigned n;
	n = CalculateTrapezoidMoveProfile(from, tgt, vel, acc, dcc, points);
	if (n) return n;
	n = CalculateFromStopTriangleMoveProfile(from, tgt, acc, dcc, points);
	ASSERTE(fabs(points[1].v) <= fabs(vel));
	if (n) return n;
	ASSERT("PROFILE CALC FAULT");
	return 0;
}

unsigned CalculateUnidirectionMoveProfile(const TProfilePoint_t& from, float tgt, float vel, float acc, float dcc, TProfilePoint* points)
{
	unsigned n;
	n = CalculateTrapezoidMoveProfile(from, tgt, vel, acc, dcc, points);
	if (n) return n;
	n = CalculateTriangleMoveProfile(from, tgt, acc, dcc, points);
	ASSERTE(fabs(points[1].v) <= vel);
	if (n) return n;
	ASSERT("PROFILE CALC FAULT");
	return 0;
}
