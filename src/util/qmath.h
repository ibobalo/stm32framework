#ifndef _QMATH_H_INCLUDED
#define _QMATH_H_INCLUDED

#include <math.h>
#include <type_traits>

template<class BaseType, unsigned nFractionalBits, unsigned nOtherFractionalBits = nFractionalBits>
class TFixedPoint
{
public:
	static_assert(std::is_integral<BaseType>::value, "Integral type required.");
	static_assert(nFractionalBits > 0, "Fractional part required.");
	static_assert(nFractionalBits < sizeof(BaseType)*8, "Fractional part required.");
	typedef TFixedPoint<BaseType, nFractionalBits, nOtherFractionalBits> QType;
	static constexpr bool bSigned = std::is_signed<BaseType>::value;
	static constexpr unsigned nTotalBits = sizeof(BaseType) * 8;
	static constexpr unsigned nIntegerBits = bSigned ? (nTotalBits - nFractionalBits - 1) : (nTotalBits - nFractionalBits);
	static constexpr BaseType nIntegerMax = (BaseType(1) << nIntegerBits) - 1;
	static constexpr BaseType nFractionalParts = (BaseType(1) << nFractionalBits);
	static constexpr BaseType MaskFractional = nFractionalParts - 1;
	static constexpr BaseType MaskInteger = (BaseType(1) << (nTotalBits - nFractionalBits) - 1) << nFractionalBits;
public:
	constexpr TFixedPoint() {}
	constexpr TFixedPoint(const QType& src) : m_q(src.m_q) {}
	explicit constexpr TFixedPoint(const BaseType i) : m_q(i * nFractionalParts) {}
	explicit constexpr TFixedPoint(const BaseType i, const BaseType f) : m_q(i * nFractionalParts + f) {}
	explicit constexpr TFixedPoint(const float f) : m_q(roundf(f * nFractionalParts)) {}
	explicit constexpr TFixedPoint(const double d) : m_q(round(d * nFractionalParts)) {}
	template<unsigned nSrcFractionalBits>
	explicit constexpr TFixedPoint(const TFixedPoint<BaseType, nSrcFractionalBits>& src) :
		m_q((nSrcFractionalBits > nFractionalBits) ?
				src.m_q / ((BaseType(1) << (nSrcFractionalBits - nFractionalBits))) :
				src.m_q * ((BaseType(1) << (nFractionalBits - nSrcFractionalBits)))){}
public:
	inline constexpr BaseType i() const {return (m_q / nFractionalParts);}
	inline constexpr BaseType f() const {return (m_q % nFractionalParts);}
	inline const QType& operator = (const BaseType x) {m_q = x * nFractionalParts; return *this;}
	inline const QType& operator += (const BaseType x) {m_q += x * nFractionalParts; return *this;}
	inline const QType& operator -= (const BaseType x) {m_q -= x * nFractionalParts; return *this;}
	inline const QType& operator *= (const BaseType x) {m_q *= x; return *this;}
	inline const QType& operator /= (const BaseType x) {m_q /= x; return *this;}
	inline const QType& operator += (const QType& q) {m_q += q.m_q; return *this;}
	inline const QType& operator -= (const QType& q) {m_q -= q.m_q; return *this;}
	inline constexpr bool operator > (const QType q) const {return m_q > q.m_q;}
	inline constexpr bool operator < (const QType q) const {return m_q < q.m_q;}
	inline constexpr bool operator >= (const QType q) const {return m_q >= q.m_q;}
	inline constexpr bool operator <= (const QType q) const {return m_q <= q.m_q;}
	inline constexpr bool operator == (const QType q) const {return m_q == q.m_q;}
	inline constexpr bool operator != (const QType q) const {return m_q != q.m_q;}
	inline explicit constexpr operator BaseType() const {return i();}
	inline explicit constexpr operator float() const {return (float(m_q) / nFractionalParts);}
	inline explicit constexpr operator double() const {return (double(m_q) / nFractionalParts);}
	inline explicit constexpr operator bool() const {return (m_q != 0);}
public:
	template<typename T> inline constexpr QType operator + (const T& x) const {return QType(*this) += x;}
	template<typename T> inline constexpr QType operator - (const T& x) const {return QType(*this) -= x;}
	template<typename T> inline constexpr QType operator * (const T& x) const {return QType(*this) *= x;}
	template<typename T> inline constexpr QType operator / (const T& x) const {return QType(*this) /= x;}
	template<typename T> inline constexpr bool operator > (const T x) const {return operator > (QType(x));}
	template<typename T> inline constexpr bool operator < (const T x) const {return operator < (QType(x));}
	template<typename T> inline constexpr bool operator >= (const T x) const {return operator >= (QType(x));}
	template<typename T> inline constexpr bool operator <= (const T x) const {return operator <= (QType(x));}
	template<typename T> inline constexpr bool operator == (const T x) const {return operator == (QType(x));}
	template<typename T> inline constexpr bool operator != (const T x) const {return operator != (QType(x));}
	template<typename T> inline constexpr QType& operator += (const T x) const {return *this += (QType(x));}
	template<typename T> inline constexpr QType& operator -= (const T x) const {return *this -= (QType(x));}
	template<unsigned nSrcFractionalBits>
	inline const QType& operator += (const TFixedPoint<BaseType, nSrcFractionalBits>& q) {
		return *this += QType(q);
	}
	template<unsigned nSrcFractionalBits>
	inline const QType& operator -= (const TFixedPoint<BaseType, nSrcFractionalBits>& q) {
		return *this -= QType(q);
	}
	template<unsigned nSrcFractionalBits>
	inline const QType& operator *= (const TFixedPoint<BaseType, nSrcFractionalBits>& q) {
		m_q = TFixedPoint<BaseType, nFractionalBits, nSrcFractionalBits>::qmul(m_q, q.m_q);
		return *this;
	}
	template<unsigned nSrcFractionalBits>
	inline const QType& operator /= (const TFixedPoint<BaseType, nSrcFractionalBits>& q) {
		BaseType r = q.m_q / (BaseType(1) << nSrcFractionalBits) / 2;
		m_q = TFixedPoint<BaseType, nFractionalBits, nSrcFractionalBits>::qdiv((bSigned && (m_q < 0) != (q.m_q < 0)) ? (m_q - r) : (m_q + r), q.m_q);
		return *this;
	}
protected:
	template<class FriendBaseType, unsigned nFriendFractionalBits, unsigned nFriendOtherFractionalBits> friend class TFixedPoint;
	static inline const BaseType qmul(const BaseType x, const BaseType y) {
		if (is_shrinkable(x)) {
			return TFixedPoint<BaseType, nFractionalBits, nOtherFractionalBits - 1>::qmul(x/2, y);
		} else if (is_shrinkable(y)) {
			return TFixedPoint<BaseType, nFractionalBits, nOtherFractionalBits - 1>::qmul(x, y/2);
		} else {
			return x * y / (BaseType(1) << nOtherFractionalBits);
		}
	}
	static inline const BaseType qdiv(const BaseType x, const BaseType y) {
		if (is_extendable(x)) {
			return TFixedPoint<BaseType, nFractionalBits, nOtherFractionalBits - 1>::qdiv(x*2, y);
		} else if (is_shrinkable(y)) {
			return TFixedPoint<BaseType, nFractionalBits, nOtherFractionalBits - 1>::qdiv(x, y/2);
		} else {
			return x / y * (BaseType(1) << nOtherFractionalBits);
		}
	}
	static inline constexpr bool is_shrinkable(BaseType x) { return (x & 1) == 0; }
	static inline constexpr bool is_extendable(BaseType x) { return ((bSigned ? (x^(x<<1)) : x) & (BaseType(1)<<(nTotalBits-1))) == 0; }
	BaseType m_q;
};

template<class BaseType, unsigned nFractionalBits>
class TFixedPoint<BaseType, nFractionalBits, 0>
{
protected:
	template<class FriendBaseType, unsigned nFriendFractionalBits, unsigned nFriendOtherFractionalBits> friend class TFixedPoint;
	static inline const BaseType qmul(const BaseType x, const BaseType y) {
		return x * y;
	}
	static inline const BaseType qdiv(const BaseType x, const BaseType y) {
		return x / y;
	}
};


typedef TFixedPoint<int, 10> IQ10;
typedef TFixedPoint<unsigned, 10> UQ10;
typedef TFixedPoint<int, 20> IQ20;
typedef TFixedPoint<unsigned, 20> UQ20;

#endif
