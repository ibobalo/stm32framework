#include "moveprofile.h"
#include "macros.h"
#include <iostream>                                                                                                                                                                
#include <sstream>
#include <iomanip>                                                                                                                                                                 
#include <type_traits>
#include <cstdlib> // rand
                                                                                                                                                                                   
void Default_Handler(void) {};                                                                                                                                                     
                                                                                                                                                                                   
static unsigned nCases = 0;                                                                                                                                                        
static unsigned nErrors = 0;                                                                                                                                                       

bool EpsilonEQ(float l, float r, float epsilon = 0.001) { return (l >= (r - epsilon) && l <= (r + epsilon)); }
bool EpsilonLT(float l, float r, float epsilon = 0.001) { return (l <  r - epsilon); }
bool EpsilonLE(float l, float r, float epsilon = 0.001) { return (l <= r - epsilon); }
bool EpsilonGT(float l, float r, float epsilon = 0.001) { return (l >  r + epsilon); }
bool EpsilonGE(float l, float r, float epsilon = 0.001) { return (l >= r + epsilon); }
bool RelativeEQ(float l, float r, float epsilon = 0.001) { return (l >= (r - fabs(r)*epsilon) && l <= (r + fabs(r)*epsilon)); }
bool RelativeLT(float l, float r, float epsilon = 0.001) { return (l <  r - fabs(r)*epsilon); }
bool RelativeLE(float l, float r, float epsilon = 0.001) { return (l <= r - fabs(r)*epsilon); }
bool RelativeGT(float l, float r, float epsilon = 0.001) { return (l >  r + fabs(r)*epsilon); }
bool RelativeGE(float l, float r, float epsilon = 0.001) { return (l >= r + fabs(r)*epsilon); }
bool AbsLT(float l, float r, float epsilon = 0.001) { return (fabs(l) <   fabs(r) - fabs(r)*epsilon); }
bool AbsLE(float l, float r, float epsilon = 0.001) { return (fabs(l) <=  fabs(r) - fabs(r)*epsilon); }
bool AbsGT(float l, float r, float epsilon = 0.001) { return (fabs(l) >   fabs(r) + fabs(r)*epsilon); }
bool AbsGE(float l, float r, float epsilon = 0.001) { return (fabs(l) >=  fabs(r) + fabs(r)*epsilon); }
bool operator == (const TProfilePoint& l, const TProfilePoint& r) {
	return EpsilonEQ(l.t, r.t) && EpsilonEQ(l.p, r.p) && EpsilonEQ(l.v, r.v);
}
const TProfilePoint& operator + (const TProfilePoint& r) {
	return r;
}
std::ostream& operator << (std::ostream& o, const TProfilePoint& pp) {
	o << "t:" << pp.t << ",p:" << pp.p << ",v:" << pp.v;
	return o;
}
std::ostream& operator << (std::ostream& o, const TProfile& mp) {
	for (unsigned n = 0; n < mp.len; ++n) {
		const TProfilePoint& pp = mp.points[n];
		o << "t:" << pp.t << ",p:" << pp.p << ",v:" << pp.v;
		if (n + 1 < mp.len) {
			const TProfilePoint& pp2 = mp.points[n+1];
			float acc = pp.ApproximateAccTo(pp2);
			o << ",a:" << acc << "; ";
		}
	}
	return o;
}

const char* ValidateProfile(
		const TProfile& mp,
		float accmax,
		float dccmax,
		float tgt = 0.0,
		float vmax = 0.0,
		float pmin = 0.0,
		float pmax = 0.0
) {
	for (unsigned n = 0; n < mp.len; ++n) {
		const TProfilePoint& pp2 = mp.points[n];
		if (pp2.t > mp.points[mp.len-1].t) return "over time";
		if (pmin < pmax && (RelativeLT(pp2.p, pmin) || RelativeGT(pp2.p, pmax))) return "pos limits";
		if (pmin > pmax && (RelativeLT(pp2.p, pmax) || RelativeGT(pp2.p, pmin))) return "pos limits";
		if (vmax > 0 && EpsilonGT(fabs(pp2.v), vmax)) return "vel limits";
		if (n > 0) {
			const TProfilePoint& pp1 = mp.points[n-1];
			float dt = pp2.t - pp1.t;
			if (dt < 0.0) return "reverse time";
			float dv = pp2.v - pp1.v;
			if (dt == 0.0 && dv != 0.0) return "velocity step";
			float acc = pp1.ApproximateAccTo(pp2);
			if (pp1.v >= 0.0 && dv > 0.0 && AbsGT(acc, accmax, 0.01)) return "over+ acceleration";
			if (pp1.v >  0.0 && dv < 0.0 && AbsGT(acc, dccmax, 0.01)) return "over+ deceleration";
			if (pp1.v <  0.0 && dv > 0.0 && AbsGT(acc, dccmax, 0.01)) return "over- deceleration";
			if (pp1.v <= 0.0 && dv < 0.0 && AbsGT(acc, accmax, 0.01)) return "over- acceleration";
		}
		if (n + 1 == mp.len) { // last
			if (tgt != 0.0 && !EpsilonEQ(pp2.p, tgt)) return "Target";
			if (pp2.v != 0.0) return "Stop";
		}
	}
	return NULL;
}

const char* CompareProfiles(
		const TProfile& mp1,
		const TProfile& mp2,
		float allow_error = 0.001
) {
	if (mp1.len != mp2.len) return "len diff";
	TProfilePoint min;
	TProfilePoint max;
	min.Assign(mp1.points[0]);
	max.Assign(mp1.points[0]);
	for (unsigned n = 1; n < mp1.len; ++n) {
		const TProfilePoint& pp1 = mp1.points[n];
		if (pp1.t < min.t) min.t = pp1.t;
		if (pp1.p < min.p) min.p = pp1.p;
		if (pp1.v < min.v) min.v = pp1.v;
		if (pp1.t > max.t) max.t = pp1.t;
		if (pp1.p > max.p) max.p = pp1.p;
		if (pp1.v > max.v) max.v = pp1.v;
	}
	float epsilon_t = MAX2(EPSILON_T, (max.t - min.t) * allow_error);
	float epsilon_p = (max.p - min.p) * allow_error;
	float epsilon_v = (max.v - min.v) * allow_error;
	for (unsigned n = 0; n < mp1.len; ++n) {
		const TProfilePoint& pp1 = mp1.points[n];
		const TProfilePoint& pp2 = mp2.points[n];
		float vel = n ? mp1.points[n-1].ApproximateAccTo(pp1) : 0.0;
		float acc = n ? mp1.points[n-1].ApproximateAccTo(pp1) : 0.0;
		if (!EpsilonEQ(pp1.t, pp2.t, epsilon_t)) return "time diff";
		if (!EpsilonEQ(pp1.p, pp2.p, MAX2(fabs(vel*EPSILON_T), epsilon_p))) return "pos diff";
		if (!EpsilonEQ(pp1.v, pp2.v, MAX2(fabs(acc*EPSILON_T), epsilon_v))) return "vel diff";
	}
	return NULL;
}

template<class Tres>
class TestCase                                                                                                                                                                     
{                                                                                                                                                                                  
	virtual void prepare_fn() {}
	virtual Tres test_fn() = 0;
	virtual const char* check_fn() {return NULL;}
public:
	TestCase(const char* name, Tres expected):m_name(name),m_fault(false),m_expected(expected) {}
	virtual ~TestCase() {}
	virtual std::ostream& operator << (std::ostream& o) const = 0;
	void on_success() {
//		std::cout << "Valid: " << *this << std::endl;
	}
	void on_fault(const char* reason) {
		std::cerr << "ERROR: " << *this << " " << reason << std::endl;
		++nErrors;
		m_fault = true;
	}
	bool do_test() {
		prepare_fn();
		Tres res = test_fn();
		const char* reason = check_fn();
		if (reason) {
			on_fault(reason);
		} else if (res == m_expected) {
			on_success();
		} else {
			std::stringstream ssreason;
			ssreason << res << " != " << m_expected;
			on_fault(ssreason.str().c_str());
		}
		if (m_fault) {
			test_fn(); // set debugger breakpoint here
			check_fn();
		}
		++nCases;                                                                                                                                                                  
		return !m_fault;
	}
protected:
	const char* m_name;
	bool m_fault;
	Tres m_expected;
};

template<class Tres>
std::ostream& operator << (std::ostream& o, const TestCase<Tres>& t) {
    t << o;
    return o;
}

class TestCasePointAcc : public TestCase<const TProfilePoint&> {
public:
	TestCasePointAcc(TProfilePoint from, float dt, float acc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("Acc", expected),m_from(from), m_dt(dt), m_acc(acc) {}
	virtual ~TestCasePointAcc() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateAccelerate(m_from, m_dt, m_acc);
		return m_temp;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "," << m_dt << "," << m_acc;
		return o;
	}
	TProfilePoint m_temp;
	TProfilePoint m_from;
	float m_dt;
	float m_acc;
};

class TestCasePointStop : public TestCase<const TProfilePoint&> {
public:
	TestCasePointStop(TProfilePoint from, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("Stop", expected),m_from(from), m_dcc(dcc) {}
	virtual ~TestCasePointStop() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateStop(m_from, m_dcc);
		return m_temp;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "," << m_dcc;
		return o;
	}
	TProfilePoint m_temp;
	TProfilePoint m_from;
	float m_dcc;
};

class TestCasePointMid : public TestCase<const TProfilePoint&> {
public:
	TestCasePointMid(TProfilePoint prev, TProfilePoint next, float dt, const TProfilePoint& expected):TestCase<const TProfilePoint&>("Mid", expected),m_prev(prev),m_next(next),m_dt(dt) {}
	virtual ~TestCasePointMid() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateMiddle(m_dt, m_prev, m_next);
		return m_temp;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_prev << ">" << m_next;
		return o;
	}
	TProfilePoint m_temp;
	TProfilePoint m_prev;
	TProfilePoint m_next;
	float m_dt;
};

class TestCaseProfileStop : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileStop(TProfilePoint from, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("STOP", expected),m_from(from),m_dcc(dcc) {}
	virtual ~TestCaseProfileStop() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateStop(m_from, m_dcc);
		return m_temp.points[1];
	}
	virtual const char* check_fn() override {
		return ValidateProfile(m_temp, m_dcc, m_dcc);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_dcc;
};

class TestCaseProfileTriangleFromStop : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileTriangleFromStop(TProfilePoint from, float tgt, float acc, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("TRNG0", expected),m_from(from),m_tgt(tgt),m_acc(acc),m_dcc(dcc) {}
	virtual ~TestCaseProfileTriangleFromStop() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateTriangleFromStop(m_from, m_tgt, m_acc, m_dcc);
		return m_temp.points[m_temp.len == 1 ? 0 : 1];
	}
	virtual const char* check_fn() override {
		if (m_temp.len < 1) return "Empty Profile";
		return ValidateProfile(m_temp, m_acc, m_dcc, m_tgt);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "->" << m_tgt << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_acc;
	float m_dcc;
};

class TestCaseProfileTriangle : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileTriangle(TProfilePoint from, float tgt, float acc, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("TRNG", expected),m_from(from),m_tgt(tgt),m_acc(acc),m_dcc(dcc) {}
	virtual ~TestCaseProfileTriangle() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateTriangle(m_from, m_tgt, m_acc, m_dcc);
		return m_temp.points[m_temp.len == 1 ? 0 : 1];
	}
	virtual const char* check_fn() override {
		if (m_temp.len < 1) {
			return "Empty Profile";
		}
		return ValidateProfile(m_temp, m_acc, m_dcc, m_tgt);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "->" << m_tgt << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_acc;
	float m_dcc;
};

class TestCaseProfileTrapezoid : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileTrapezoid(TProfilePoint from, float tgt, float vel, float acc, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("TRPZ", expected),m_from(from),m_tgt(tgt),m_vel(vel),m_acc(acc),m_dcc(dcc) {}
	virtual ~TestCaseProfileTrapezoid() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateTrapezoid(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return m_temp.points[m_temp.len == 1 ? 0 : 1];
	}
	virtual const char* check_fn() override {
		if (m_temp.len < 1) {
			return "Empty Profile";
		}
		return ValidateProfile(m_temp, m_acc, m_dcc, m_tgt, MAX2(fabs(m_vel), fabs(m_from.v)), m_from.p, m_tgt);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "->" << m_tgt << "v" << m_vel << "a" << m_acc << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_vel;
	float m_acc;
	float m_dcc;
};

class TestCaseProfileFromStop : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileFromStop(TProfilePoint from, float tgt, float vel, float acc, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("CALC0", expected),m_from(from),m_tgt(tgt),m_vel(vel),m_acc(acc),m_dcc(dcc) {}
	virtual ~TestCaseProfileFromStop() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateFromStop(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return m_temp.points[m_temp.len == 1 ? 0 : 1];
	}
	virtual const char* check_fn() override {
		if (m_temp.len < 1) {
			return "Empty Profile";
		}
		return ValidateProfile(m_temp, m_acc, m_dcc, m_tgt, MAX2(fabs(m_vel), fabs(m_from.v)), m_from.p, m_tgt);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "->" << m_tgt << "v" << m_vel << "a" << m_acc << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_vel;
	float m_acc;
	float m_dcc;
};

class TestCaseProfileUnidirection : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileUnidirection(TProfilePoint from, float tgt, float vel, float acc, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("CALC1", expected),m_from(from),m_tgt(tgt),m_vel(vel),m_acc(acc),m_dcc(dcc) {}
	virtual ~TestCaseProfileUnidirection() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.CalculateUnidirection(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return m_temp.points[m_temp.len == 1 ? 0 : 1];
	}
	virtual const char* check_fn() override {
		if (m_temp.len < 1) {
			return "Empty Profile";
		}
		return ValidateProfile(m_temp, m_acc, m_dcc, m_tgt, MAX2(fabs(m_vel), fabs(m_from.v)), m_from.p, m_tgt);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "->" << m_tgt << "v" << m_vel << "a" << m_acc << "d" << m_dcc << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_vel;
	float m_acc;
	float m_dcc;
};

class TestCaseProfileAny : public TestCase<const TProfilePoint&> {
public:
	TestCaseProfileAny(TProfilePoint from, float tgt, float vel, float acc, float dcc, const TProfilePoint& expected):TestCase<const TProfilePoint&>("CALC", expected),m_from(from),m_tgt(tgt),m_vel(vel),m_acc(acc),m_dcc(dcc) {}
	virtual ~TestCaseProfileAny() {}
protected:
	virtual const TProfilePoint& test_fn() override {
		m_temp.Calculate(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return m_temp.points[m_temp.len == 1 ? 0 : (m_temp.points[1].v != 0.0 ? 1 : 2)];
	}
	virtual const char* check_fn() override {
		if (m_temp.len < 1) {
			return "Empty Profile";
		}
		float pmin = MIN2(m_from.p, m_tgt);
		float pmax = MAX2(m_from.p, m_tgt);
		float pstop = m_from.v * fabs(m_from.v / m_dcc) * 0.5;
		if (pstop < 0) pmin += pstop; else pmax += pstop;
		return ValidateProfile(m_temp, m_acc, m_dcc, m_tgt, MAX2(fabs(m_vel), fabs(m_from.v)), pmin, pmax);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<const TProfilePoint&>::m_name << " " << +m_from << "->" << m_tgt << "v" << m_vel << "a" << m_acc << ": " << m_temp;
		return o;
	}
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_vel;
	float m_acc;
	float m_dcc;
};

float rnd(float range) {
	return range * (std::rand() + 1) / (RAND_MAX + 1U);
}
float rndU() {
	switch (std::rand() & 0x03) {
//	case 0 : return rnd(1E-1);
//	case 1 : return rnd(1E4);
	default: return rnd(100.0);
	}
}
float rnd() {
	return (std::rand() & 0x1) ? -rndU() : rndU();
}

class TestCaseProfileRandom : public TestCase<float> {
public:
	TestCaseProfileRandom(const char* name, float expected):TestCase<float>(name, expected), m_tgt(0.0), m_vel(0.0), m_acc(0.0), m_dcc(0.0) {}
	virtual ~TestCaseProfileRandom() {}
protected:
	virtual const char* check_fn() override {
		if (m_temp.len < 1) {
			return "Empty Profile";
		}
		float pmin = MIN2(m_from.p, m_tgt);
		float pmax = MAX2(m_from.p, m_tgt);
		float pstop = m_from.v * fabs(m_from.v / m_dcc) * 0.5;
		if (pstop < 0) pmin += pstop; else pmax += pstop;
		const char* reason = ValidateProfile(m_temp, m_acc, m_dcc, m_tgt, MAX2(fabs(m_vel), fabs(m_from.v)), pmin, pmax);
		if (reason) {
			return reason;
		}
		return CompareProfiles(m_ref, m_temp);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<float>::m_name << 	"\n exp:" << m_ref << "\n res:" << m_temp << "\n ";
		return o;
	}
	TProfile m_ref;
	TProfile m_temp;
	TProfilePoint m_from;
	float m_tgt;
	float m_vel;
	float m_acc;
	float m_dcc;
};

class TestCaseProfileRandomAD : public TestCaseProfileRandom {
public:
	TestCaseProfileRandomAD():TestCaseProfileRandom("AD", 0.0), m_ta(0.0), m_td(0.0) {}
	virtual ~TestCaseProfileRandomAD() {}
protected:
	virtual void prepare_fn() override {
		m_tgt = rnd();
		m_dcc = -rnd();
		m_ta = rndU() + EPSILON_T;
		m_td = rndU() + EPSILON_T;
		m_ref.points[2].Assign(rndU() + m_ta + m_td, m_tgt, 0.0);
		m_ref.points[1].CalculateAccelerate(m_ref.points[2], -m_td, m_dcc);
		m_vel = m_ref.points[1].v;
		do {
			float v0 = (m_vel < 0.0) ? -rnd(-m_vel) : rnd(m_vel);
			m_acc = (m_vel - v0) / m_td ;
			m_ref.points[0].CalculateAccelerate(m_ref.points[1], -m_ta, m_acc);
		} while (m_acc == 0.0 || (m_ref.points[0].v < 0.0) != (m_vel < 0.0));
		m_from.Assign(m_ref.points[0]);
		m_ref.len = 3;
	}
	virtual float test_fn() override {
		m_temp.Calculate(m_from, m_tgt, m_vel * 2, m_acc, m_dcc);
		return 0.0;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<float>::m_name << " " <<
				"A=" << m_acc << "/" << std::setprecision(10) << m_ta << "s,V=" << m_vel << ",D=" << m_dcc << "/" << m_td << "s,->" << m_tgt <<
				"\n exp:" << m_ref << "\n res:" << m_temp << "\n ";
		return o;
	}
protected:
	float m_ta;
	float m_td;
};

class TestCaseProfileRandomAVD : public TestCaseProfileRandom {
public:
	TestCaseProfileRandomAVD():TestCaseProfileRandom("AVD", 0.0), m_ta(0.0), m_tv(0.0), m_td(0.0) {}
	virtual ~TestCaseProfileRandomAVD() {}
protected:
	virtual void prepare_fn() override {
		m_tgt = rnd();
		m_dcc = -rnd();
		m_ta = rndU() + EPSILON_T;
		m_tv = rndU() + EPSILON_T;
		m_td = rndU() + EPSILON_T;
		m_ref.points[3].Assign(rndU() + m_ta + m_tv + m_td, m_tgt, 0.0);
		m_ref.points[2].CalculateAccelerate(m_ref.points[3], -m_td, m_dcc);
		m_ref.points[1].CalculateAccelerate(m_ref.points[2], -m_tv, 0.0);
		m_vel = m_ref.points[1].v;
		do {
			float v0 = (m_vel < 0.0) ? -rnd(-m_vel) : rnd(m_vel);
			m_acc = (m_vel - v0) / m_td ;
			m_ref.points[0].CalculateAccelerate(m_ref.points[1], -m_ta, m_acc);
		} while (m_acc == 0.0 || (m_ref.points[0].v < 0.0) != (m_vel < 0.0));
		m_from.Assign(m_ref.points[0]);
		m_ref.len = 4;
	}
	virtual float test_fn() override {
		m_temp.Calculate(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return 0.0;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<float>::m_name << " " <<
				"A=" << m_acc << "/" << std::setprecision(10) << m_ta << "s,V=" << m_vel << "/" << m_tv << "s,D=" << m_dcc << "/" << m_td << "s,->" << m_tgt <<
				"\n exp:" << m_ref << "\n res:" << m_temp << "\n ";
		return o;
	}
protected:
	float m_ta;
	float m_tv;
	float m_td;
};

class TestCaseProfileRandomDVD : public TestCaseProfileRandom {
public:
	TestCaseProfileRandomDVD():TestCaseProfileRandom("DVD", 0.0), m_ta(0.0), m_tv(0.0), m_td(0.0) {}
	virtual ~TestCaseProfileRandomDVD() {}
protected:
	virtual void prepare_fn() override {
		m_tgt = rnd();
		m_dcc = -rnd();
		m_ta = rndU() + EPSILON_T;
		m_tv = rndU() + EPSILON_T;
		m_td = rndU() + EPSILON_T;
		m_ref.points[3].Assign(rndU() + m_ta + m_tv + m_td, m_tgt, 0.0);
		m_ref.points[2].CalculateAccelerate(m_ref.points[3], -m_td, m_dcc);
		m_ref.points[1].CalculateAccelerate(m_ref.points[2], -m_tv, 0.0);
		m_vel = m_ref.points[1].v;
		m_acc = m_dcc;
		m_ref.points[0].CalculateAccelerate(m_ref.points[1], -m_ta, m_acc);
		m_from.Assign(m_ref.points[0]);
		m_ref.len = 4;
	}
	virtual float test_fn() override {
		m_temp.Calculate(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return 0.0;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<float>::m_name << " " <<
				"A=" << m_acc << "/" << std::setprecision(10) << m_ta << "s,V=" << m_vel << "/" << m_tv << "s,D=" << m_dcc << "/" << m_td << "s,->" << m_tgt <<
				"\n exp:" << m_ref << "\n res:" << m_temp << "\n ";
		return o;
	}
protected:
	float m_ta;
	float m_tv;
	float m_td;
};

class TestCaseProfileRandomSAD : public TestCaseProfileRandom {
public:
	TestCaseProfileRandomSAD():TestCaseProfileRandom("SAD", 0.0), m_ta(0.0), m_td(0.0) {}
	virtual ~TestCaseProfileRandomSAD() {}
protected:
	virtual void prepare_fn() override {
		m_tgt = rnd();
		m_dcc = -rnd();
		m_ts = rndU() + EPSILON_T;
		m_ta = rndU() + EPSILON_T;
		m_td = rndU() + EPSILON_T;
		m_ref.points[3].Assign(rndU() + m_ts + m_ta + m_td, m_tgt, 0.0);
		m_ref.points[2].CalculateAccelerate(m_ref.points[3], -m_td, m_dcc);
		m_vel = m_ref.points[2].v;
		m_acc = m_vel / m_ta;
		m_ref.points[1].CalculateAccelerate(m_ref.points[2], -m_ta, m_acc);
		m_ref.points[1].v = 0.0;
		m_ref.points[0].CalculateAccelerate(m_ref.points[1], -m_ts, -m_dcc);
		m_from.Assign(m_ref.points[0]);
		m_ref.len = 4;
	}
	virtual float test_fn() override {
		m_temp.Calculate(m_from, m_tgt, m_vel * 2, m_acc, m_dcc);
		return 0.0;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<float>::m_name << " " <<
				"A=" << m_acc << "/" << std::setprecision(10) << m_ta << "s,V=" << m_vel << ",D=" << m_dcc << "/" << m_td << "s,->" << m_tgt <<
				"\n exp:" << m_ref << "\n res:" << m_temp << "\n ";
		return o;
	}
protected:
	float m_ts;
	float m_ta;
	float m_td;
};

class TestCaseProfileRandomSAVD : public TestCaseProfileRandom {
public:
	TestCaseProfileRandomSAVD():TestCaseProfileRandom("SAVD", 0.0), m_ta(0.0), m_td(0.0) {}
	virtual ~TestCaseProfileRandomSAVD() {}
protected:
	virtual void prepare_fn() override {
		m_tgt = rnd();
		m_dcc = -rnd();
		m_ts = rndU() + EPSILON_T;
		m_ta = rndU() + EPSILON_T;
		m_tv = rndU() + EPSILON_T;
		m_td = rndU() + EPSILON_T;
		m_ref.points[4].Assign(rndU() + m_ts + m_ta + m_td, m_tgt, 0.0);
		m_ref.points[3].CalculateAccelerate(m_ref.points[4], -m_td, m_dcc);
		m_vel = m_ref.points[3].v;
		m_acc = m_vel / m_ta;
		m_ref.points[2].CalculateAccelerate(m_ref.points[3], -m_tv, 0.0);
		m_ref.points[1].CalculateAccelerate(m_ref.points[2], -m_ta, m_acc);
		m_ref.points[1].v = 0.0;
		m_ref.points[0].CalculateAccelerate(m_ref.points[1], -m_ts, -m_dcc);
		m_from.Assign(m_ref.points[0]);
		m_ref.len = 5;
	}
	virtual float test_fn() override {
		m_temp.Calculate(m_from, m_tgt, m_vel, m_acc, m_dcc);
		return 0.0;
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << TestCase<float>::m_name << " " <<
				"A=" << m_acc << "/" << std::setprecision(10) << m_ta << "s,V=" << m_vel << ",D=" << m_dcc << "/" << m_td << "s,->" << m_tgt <<
				"\n exp:" << m_ref << "\n res:" << m_temp << "\n ";
		return o;
	}
protected:
	float m_ts;
	float m_ta;
	float m_tv;
	float m_td;
};

bool test_all()
{
	//            from:t     p     v     ta   acc  to:t     p     v
	TestCasePointAcc({0.0,  0.0,  0.0}, 1.0,  1.0,  {1.0,  0.5,  1.0}).do_test();
	TestCasePointAcc({1.0,  0.0,  0.0}, 1.0,  1.0,  {2.0,  0.5,  1.0}).do_test();
	TestCasePointAcc({0.0,  2.0,  0.0}, 1.0,  1.0,  {1.0,  2.5,  1.0}).do_test();
	TestCasePointAcc({1.0,  2.0,  0.0}, 1.0,  1.0,  {2.0,  2.5,  1.0}).do_test();
	TestCasePointAcc({0.0, -2.0,  0.0}, 1.0,  1.0,  {1.0, -1.5,  1.0}).do_test();
	TestCasePointAcc({1.0, -2.0,  0.0}, 1.0,  1.0,  {2.0, -1.5,  1.0}).do_test();
	TestCasePointAcc({0.0,  0.0,  1.0}, 1.0,  1.0,  {1.0,  1.5,  2.0}).do_test();
	TestCasePointAcc({1.0,  0.0,  1.0}, 1.0,  1.0,  {2.0,  1.5,  2.0}).do_test();
	TestCasePointAcc({0.0,  2.0,  1.0}, 1.0,  1.0,  {1.0,  3.5,  2.0}).do_test();
	TestCasePointAcc({1.0,  2.0,  1.0}, 1.0,  1.0,  {2.0,  3.5,  2.0}).do_test();
	TestCasePointAcc({0.0,  0.0, -1.0}, 1.0,  1.0,  {1.0, -0.5,  0.0}).do_test();
	TestCasePointAcc({1.0,  0.0, -1.0}, 1.0,  1.0,  {2.0, -0.5,  0.0}).do_test();
	TestCasePointAcc({0.0,  2.0, -1.0}, 1.0,  1.0,  {1.0,  1.5,  0.0}).do_test();
	TestCasePointAcc({1.0,  2.0, -1.0}, 1.0,  1.0,  {2.0,  1.5,  0.0}).do_test();
	//            from:t     p     v     ta   acc  to:t     p     v
	TestCasePointAcc({0.0,  0.0,  0.0}, 1.0, -1.0,  {1.0, -0.5, -1.0}).do_test();
	TestCasePointAcc({1.0,  0.0,  0.0}, 1.0, -1.0,  {2.0, -0.5, -1.0}).do_test();
	TestCasePointAcc({0.0,  2.0,  0.0}, 1.0, -1.0,  {1.0,  1.5, -1.0}).do_test();
	TestCasePointAcc({1.0,  2.0,  0.0}, 1.0, -1.0,  {2.0,  1.5, -1.0}).do_test();
	TestCasePointAcc({0.0, -2.0,  0.0}, 1.0, -1.0,  {1.0, -2.5, -1.0}).do_test();
	TestCasePointAcc({1.0, -2.0,  0.0}, 1.0, -1.0,  {2.0, -2.5, -1.0}).do_test();
	TestCasePointAcc({0.0,  0.0,  1.0}, 1.0, -1.0,  {1.0,  0.5,  0.0}).do_test();
	TestCasePointAcc({1.0,  0.0,  1.0}, 1.0, -1.0,  {2.0,  0.5,  0.0}).do_test();
	TestCasePointAcc({0.0,  2.0,  1.0}, 1.0, -1.0,  {1.0,  2.5,  0.0}).do_test();
	TestCasePointAcc({1.0,  2.0,  1.0}, 1.0, -1.0,  {2.0,  2.5,  0.0}).do_test();
	TestCasePointAcc({0.0,  0.0, -1.0}, 1.0, -1.0,  {1.0, -1.5, -2.0}).do_test();
	TestCasePointAcc({1.0,  0.0, -1.0}, 1.0, -1.0,  {2.0, -1.5, -2.0}).do_test();
	TestCasePointAcc({0.0,  2.0, -1.0}, 1.0, -1.0,  {1.0,  0.5, -2.0}).do_test();
	TestCasePointAcc({1.0,  2.0, -1.0}, 1.0, -1.0,  {2.0,  0.5, -2.0}).do_test();
	//            from:t     p     v     ta   acc  to:t     p     v
	TestCasePointAcc({0.0,  0.0,  1.0}, 2.0, -1.0,  {2.0,  0.0, -1.0}).do_test();
	TestCasePointAcc({1.0,  0.0,  1.0}, 2.0, -1.0,  {3.0,  0.0, -1.0}).do_test();
	TestCasePointAcc({0.0,  2.0,  1.0}, 2.0, -1.0,  {2.0,  2.0, -1.0}).do_test();
	TestCasePointAcc({1.0,  2.0,  1.0}, 2.0, -1.0,  {3.0,  2.0, -1.0}).do_test();
	TestCasePointAcc({0.0,  0.0, -1.0}, 2.0,  1.0,  {2.0,  0.0,  1.0}).do_test();
	TestCasePointAcc({1.0,  0.0, -1.0}, 2.0,  1.0,  {3.0,  0.0,  1.0}).do_test();
	TestCasePointAcc({0.0,  2.0, -1.0}, 2.0,  1.0,  {2.0,  2.0,  1.0}).do_test();
	TestCasePointAcc({1.0,  2.0, -1.0}, 2.0,  1.0,  {3.0,  2.0,  1.0}).do_test();
	//             from:t     p     v    dcc  to:t     p     v
	TestCasePointStop({0.0,  0.0,  0.0}, -1.0,  {0.0,  0.0,  0.0}).do_test();
	TestCasePointStop({0.0,  0.0,  1.0}, -1.0,  {1.0,  0.5,  0.0}).do_test();
	TestCasePointStop({0.0,  0.0,  1.0}, -2.0,  {0.5,  0.25, 0.0}).do_test();
	TestCasePointStop({0.0,  0.0,  2.0}, -2.0,  {1.0,  1.0,  0.0}).do_test();
	TestCasePointStop({1.0,  0.0,  2.0}, -2.0,  {2.0,  1.0,  0.0}).do_test();
	TestCasePointStop({1.0,  1.0,  2.0}, -2.0,  {2.0,  2.0,  0.0}).do_test();
	TestCasePointStop({1.0,  1.0,  4.0}, -2.0,  {3.0,  5.0,  0.0}).do_test();
	TestCasePointStop({1.0, -1.0,  2.0}, -2.0,  {2.0,  0.0,  0.0}).do_test();
	TestCasePointStop({1.0, -1.0,  4.0}, -2.0,  {3.0,  3.0,  0.0}).do_test();
	//             from:t     p     v    dcc  to:t     p     v
	TestCasePointStop({0.0,  0.0, -0.0},  1.0,  {0.0,  0.0,  0.0}).do_test();
	TestCasePointStop({0.0,  0.0, -1.0},  1.0,  {1.0, -0.5,  0.0}).do_test();
	TestCasePointStop({0.0,  0.0, -1.0},  2.0,  {0.5, -0.25, 0.0}).do_test();
	TestCasePointStop({0.0,  0.0, -2.0},  2.0,  {1.0, -1.0,  0.0}).do_test();
	TestCasePointStop({1.0,  0.0, -2.0},  2.0,  {2.0, -1.0,  0.0}).do_test();
	TestCasePointStop({1.0, -1.0, -2.0},  2.0,  {2.0, -2.0,  0.0}).do_test();
	TestCasePointStop({1.0, -1.0, -4.0}, -2.0,  {3.0, -5.0,  0.0}).do_test();
	TestCasePointStop({1.0,  1.0, -2.0},  2.0,  {2.0,  0.0,  0.0}).do_test();
	TestCasePointStop({1.0,  1.0, -4.0},  2.0,  {3.0, -3.0,  0.0}).do_test();
	//            prev:t     p     v  next:t    p     v     t   mid:t     p     v
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0,  0.0,  0.0},  0.0,  {0.0,  0.0,  0.0}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0,  0.0,  0.0},  0.5,  {0.5,  0.0,  0.0}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0,  0.0,  0.0},  1.0,  {1.0,  0.0,  0.0}).do_test();
	TestCasePointMid({0.0,  1.0,  0.0}, {1.0,  1.0,  0.0},  0.0,  {0.0,  1.0,  0.0}).do_test();
	TestCasePointMid({0.0,  1.0,  0.0}, {1.0,  1.0,  0.0},  0.5,  {0.5,  1.0,  0.0}).do_test();
	TestCasePointMid({0.0,  1.0,  0.0}, {1.0,  1.0,  0.0},  1.0,  {1.0,  1.0,  0.0}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0,  0.5,  1.0},  0.0,  {0.0,  0.0,  0.0}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0, -0.5, -1.0},  0.0,  {0.0,  0.0,  0.0}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0,  0.5,  1.0},  0.5,  {0.5,  0.125, 0.5}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0, -0.5, -1.0},  0.5,  {0.5, -0.125,-0.5}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0,  0.5,  1.0},  1.0,  {1.0,  0.5,  1.0}).do_test();
	TestCasePointMid({0.0,  0.0,  0.0}, {1.0, -0.5, -1.0},  1.0,  {1.0, -0.5, -1.0}).do_test();

	//               from:t     p     v     dcc  to:t     p     v
	TestCaseProfileStop({0.0,  0.0,  0.0},  1.0,  {0.0,  0.0,  0.0}).do_test();
	TestCaseProfileStop({0.0,  0.0,  1.0},  1.0,  {1.0,  0.5,  0.0}).do_test();
	TestCaseProfileStop({0.0,  0.0, -1.0},  1.0,  {1.0, -0.5,  0.0}).do_test();
	TestCaseProfileStop({0.0,  0.0,  1.0},  2.0,  {0.5,  0.25, 0.0}).do_test();
	TestCaseProfileStop({0.0,  0.0, -1.0},  2.0,  {0.5, -0.25, 0.0}).do_test();
	TestCaseProfileStop({0.0,  0.0,  2.0},  2.0,  {1.0,  1.0,  0.0}).do_test();
	TestCaseProfileStop({0.0,  0.0, -2.0},  2.0,  {1.0, -1.0,  0.0}).do_test();
	TestCaseProfileStop({1.0,  0.0,  2.0},  2.0,  {2.0,  1.0,  0.0}).do_test();
	TestCaseProfileStop({1.0,  0.0, -2.0},  2.0,  {2.0, -1.0,  0.0}).do_test();
	TestCaseProfileStop({1.0,  1.0,  2.0},  2.0,  {2.0,  2.0,  0.0}).do_test();
	TestCaseProfileStop({1.0, -1.0, -2.0},  2.0,  {2.0, -2.0,  0.0}).do_test();
	TestCaseProfileStop({1.0,  1.0,  4.0},  2.0,  {3.0,  5.0,  0.0}).do_test();
	TestCaseProfileStop({1.0, -1.0, -4.0},  2.0,  {3.0, -5.0,  0.0}).do_test();
	TestCaseProfileStop({1.0, -1.0,  2.0},  2.0,  {2.0,  0.0,  0.0}).do_test();
	TestCaseProfileStop({1.0,  1.0, -2.0},  2.0,  {2.0,  0.0,  0.0}).do_test();
	TestCaseProfileStop({1.0, -1.0,  4.0},  2.0,  {3.0,  3.0,  0.0}).do_test();
	TestCaseProfileStop({1.0,  1.0, -4.0},  2.0,  {3.0, -3.0,  0.0}).do_test();

	//                    from:t     p     v     tgt  vel  acc  dcc maxv:t     p      v
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0},  1.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0}, -1.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0},  1.0, 0.5, 1.0, 2.0,  {0.5,   0.125, 0.5}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0}, -1.0, 0.5, 1.0, 2.0,  {0.5,  -0.125,-0.5}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 2.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 2.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({1.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   0.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({1.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({1.0,  1.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   1.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({1.0, -1.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  1.0},  1.0, 1.0, 1.0, 1.0,  {0.5,   0.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0, -1.0}, -1.0, 1.0, 1.0, 1.0,  {0.5,  -0.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  2.0},  3.0, 1.0, 1.0, 1.0,  {1.0,   1.5,   1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0, -2.0}, -3.0, 1.0, 1.0, 1.0,  {1.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0,  2.0},  3.0, 1.0, 1.0, 2.0,  {0.5,   0.75,  1.0}).do_test();
	TestCaseProfileTrapezoid({0.0,  0.0, -2.0}, -3.0, 1.0, 1.0, 2.0,  {0.5,  -0.75, -1.0}).do_test();

	//                           from:t     p     v     tgt  acc  dcc maxv:t     p      v
	TestCaseProfileTriangleFromStop({0.0,  0.0,  0.0},  0.0, 1.0, 1.0,  {0.0,   0.0,   0.0}).do_test();
	TestCaseProfileTriangleFromStop({0.0,  0.0,  0.0},  1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileTriangleFromStop({0.0,  0.0,  0.0}, -1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileTriangleFromStop({0.0,  0.0,  0.0},  1.0, 1.0, 2.0,  {1.155, 0.667, 1.155}).do_test();
	TestCaseProfileTriangleFromStop({0.0,  0.0,  0.0}, -1.0, 1.0, 2.0,  {1.155,-0.667,-1.155}).do_test();

	//                   from:t     p     v     tgt  acc  dcc maxv:t     p      v
	TestCaseProfileTriangle({0.0,  0.0,  0.0},  0.0, 1.0, 1.0,  {0.0,   0.0,   0.0}).do_test();
	TestCaseProfileTriangle({0.0,  0.0,  0.0},  1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileTriangle({0.0,  0.0,  0.0}, -1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileTriangle({0.0,  0.0,  0.0},  1.0, 1.0, 2.0,  {1.155, 0.667, 1.155}).do_test();
	TestCaseProfileTriangle({0.0,  0.0,  0.0}, -1.0, 1.0, 2.0,  {1.155,-0.667,-1.155}).do_test();
	TestCaseProfileTriangle({0.0,  0.0,  1.0},  1.0, 1.0, 1.0,  {0.22474, 0.25,  1.22474}).do_test();
	TestCaseProfileTriangle({0.0,  0.0, -1.0}, -1.0, 1.0, 1.0,  {0.22474,-0.25, -1.22474}).do_test();
	TestCaseProfileTriangle({0.0,  0.0,  1.0},  1.0, 1.0, 2.0,  {0.41421, 0.5,   1.41421}).do_test();
	TestCaseProfileTriangle({0.0,  0.0, -1.0}, -1.0, 1.0, 2.0,  {0.41421,-0.5,  -1.41421}).do_test();

	//                   from:t     p     v     tgt  vel  acc  dcc maxv:t     p      v
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  0.0, 1.0, 1.0, 1.0,  {0.0,   0.0,   0.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  1.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0}, -1.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  1.0, 0.5, 1.0, 2.0,  {0.5,   0.125, 0.5}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0}, -1.0, 0.5, 1.0, 2.0,  {0.5,  -0.125,-0.5}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 2.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 2.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileFromStop({1.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   0.5,   1.0}).do_test();
	TestCaseProfileFromStop({1.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileFromStop({1.0,  1.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   1.5,   1.0}).do_test();
	TestCaseProfileFromStop({1.0, -1.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  1.0,99.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0}, -1.0,99.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0},  1.0,99.0, 1.0, 2.0,  {1.155, 0.667, 1.155}).do_test();
	TestCaseProfileFromStop({0.0,  0.0,  0.0}, -1.0,99.0, 1.0, 2.0,  {1.155,-0.667,-1.155}).do_test();

	//                       from:t     p     v     tgt  vel  acc  dcc maxv:t     p      v
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  0.0, 1.0, 1.0, 1.0,  {0.0,   0.0,   0.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  1.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0}, -1.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  1.0, 0.5, 1.0, 2.0,  {0.5,   0.125, 0.5}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0}, -1.0, 0.5, 1.0, 2.0,  {0.5,  -0.125,-0.5}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 2.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 2.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({1.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   0.5,   1.0}).do_test();
	TestCaseProfileUnidirection({1.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({1.0,  1.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   1.5,   1.0}).do_test();
	TestCaseProfileUnidirection({1.0, -1.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  1.0},  1.0, 1.0, 1.0, 1.0,  {0.5,   0.5,   1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0, -1.0}, -1.0, 1.0, 1.0, 1.0,  {0.5,  -0.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  2.0},  3.0, 1.0, 1.0, 1.0,  {1.0,   1.5,   1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0, -2.0}, -3.0, 1.0, 1.0, 1.0,  {1.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  2.0},  3.0, 1.0, 1.0, 2.0,  {0.5,   0.75,  1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0, -2.0}, -3.0, 1.0, 1.0, 2.0,  {0.5,  -0.75, -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  1.0,99.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0}, -1.0,99.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0},  1.0,99.0, 1.0, 2.0,  {1.155, 0.667, 1.155}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  0.0}, -1.0,99.0, 1.0, 2.0,  {1.155,-0.667,-1.155}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  1.0},  1.0,99.0, 1.0, 1.0,  {0.22474, 0.25,  1.22474}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0, -1.0}, -1.0,99.0, 1.0, 1.0,  {0.22474,-0.25, -1.22474}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0,  1.0},  1.0,99.0, 1.0, 2.0,  {0.41421, 0.5,   1.41421}).do_test();
	TestCaseProfileUnidirection({0.0,  0.0, -1.0}, -1.0,99.0, 1.0, 2.0,  {0.41421,-0.5,  -1.41421}).do_test();

	//                       from:t     p     v     tgt  vel  acc  dcc maxv:t     p      v
	TestCaseProfileAny({0.0,  0.0,  0.0},  0.0, 1.0, 1.0, 1.0,  {0.0,   0.0,   0.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0},  1.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0}, -1.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0},  1.0, 0.5, 1.0, 2.0,  {0.5,   0.125, 0.5}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0}, -1.0, 0.5, 1.0, 2.0,  {0.5,  -0.125,-0.5}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0},  2.0, 1.0, 1.0, 2.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 2.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({1.0,  0.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({1.0,  0.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({1.0,  1.0,  0.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   1.5,   1.0}).do_test();
	TestCaseProfileAny({1.0, -1.0,  0.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  1.0},  1.0, 1.0, 1.0, 1.0,  {0.5,   0.5,   1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0, -1.0}, -1.0, 1.0, 1.0, 1.0,  {0.5,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  2.0},  3.0, 1.0, 1.0, 1.0,  {1.0,   1.5,   1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0, -2.0}, -3.0, 1.0, 1.0, 1.0,  {1.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  2.0},  3.0, 1.0, 1.0, 2.0,  {0.5,   0.75,  1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0, -2.0}, -3.0, 1.0, 1.0, 2.0,  {0.5,  -0.75, -1.0}).do_test();
	TestCaseProfileAny({0.0,  1.5, -1.0},  2.0, 1.0, 1.0, 1.0,  {2.0,   1.5,   1.0}).do_test();
	TestCaseProfileAny({0.0, -1.5,  1.0}, -2.0, 1.0, 1.0, 1.0,  {2.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  1.0, -2.0},  2.0, 1.0, 1.0, 2.0,  {2.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({0.0, -1.0,  2.0}, -2.0, 1.0, 1.0, 2.0,  {2.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({1.0,  1.0, -2.0},  2.0, 1.0, 1.0, 2.0,  {3.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({1.0, -1.0,  2.0}, -2.0, 1.0, 1.0, 2.0,  {3.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({1.0,  2.0, -2.0},  2.0, 1.0, 1.0, 2.0,  {3.0,   1.5,   1.0}).do_test();
	TestCaseProfileAny({1.0, -2.0,  2.0}, -2.0, 1.0, 1.0, 2.0,  {3.0,  -1.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0},  1.0,99.0, 1.0, 1.0,  {1.0,   0.5,   1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0}, -1.0,99.0, 1.0, 1.0,  {1.0,  -0.5,  -1.0}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0},  1.0,99.0, 1.0, 2.0,  {1.155, 0.667, 1.155}).do_test();
	TestCaseProfileAny({0.0,  0.0,  0.0}, -1.0,99.0, 1.0, 2.0,  {1.155,-0.667,-1.155}).do_test();
	TestCaseProfileAny({0.0,  0.0,  1.0},  1.0,99.0, 1.0, 1.0,  {0.22474, 0.25,  1.22474}).do_test();
	TestCaseProfileAny({0.0,  0.0, -1.0}, -1.0,99.0, 1.0, 1.0,  {0.22474,-0.25, -1.22474}).do_test();
	TestCaseProfileAny({0.0,  1.0, -2.0},  1.0,99.0, 1.0, 2.0,  {2.155, 0.667, 1.155}).do_test();
	TestCaseProfileAny({0.0, -1.0,  2.0}, -1.0,99.0, 1.0, 2.0,  {2.155,-0.667,-1.155}).do_test();
	TestCaseProfileAny({0.0,  0.0,  1.0},  1.0,99.0, 1.0, 2.0,  {0.41421, 0.5,   1.41421}).do_test();
	TestCaseProfileAny({0.0,  0.0, -1.0}, -1.0,99.0, 1.0, 2.0,  {0.41421,-0.5,  -1.41421}).do_test();

	for (unsigned n = 0; n < 10000; ++n) {
		if (!TestCaseProfileRandomAD().do_test()) break;
		if (!TestCaseProfileRandomAVD().do_test()) break;
		if (!TestCaseProfileRandomDVD().do_test()) break;
		if (!TestCaseProfileRandomSAD().do_test()) break;
		if (!TestCaseProfileRandomSAVD().do_test()) break;
	}

	std::cout << std::dec << "Total: " << nCases;                                                                                                                                  
	if (nErrors) std::cout << ". Errors: " << nErrors;                                                                                                                             
	std::cout << std::endl;                                                                                                                                                        
                                                                                                                                                                                   
	return nErrors == 0;                                                                                                                                                           
}                                                                                                                                                                                  

#define PosLimitMinus -100.0
#define PosLimitPlus   100.0
#define AccMax         10
#define DccMax         20
void demo(float commandVelocity)
{
	TProfile movingProfile;
	movingProfile.len = 2;
	//                              t    p    v
	movingProfile.points[0].Assign(0.0, 1.0, 1.0);
	movingProfile.points[1].Assign(0.1, 1.5, 0.0);

	float t = 0.3; //GetCurrentT();
	TProfilePoint current;
	current.CalculateMiddle(t, movingProfile.points[0], movingProfile.points[1]);
	// cuurent: t = 0.3; p = 1.25; v = 0.7

	float newTargetPos;
	if (commandVelocity < 0.0) {
		newTargetPos = PosLimitMinus;
	} else {
		newTargetPos = PosLimitPlus;
	}
	TProfile newMovingProfile;
	newMovingProfile.len = CalculateMoveProfile(current, newTargetPos, commandVelocity, AccMax, DccMax, newMovingProfile.points);
}

int main()                                                                                                                                                                         
{                                                                                                                                                                                  
	return test_all() ? 0 : 1;                                                                                                                                                     
}                                                                                                                                                                                  
