#ifndef ENDIANS_H_INCLUDED
#define ENDIANS_H_INCLUDED

#include <stdint.h>

static inline constexpr uint16_t  b_swap2(const uint16_t& s)  {return ((s>>8)&0xFF) | ((s<<8)&0xFF00);}
static inline constexpr uint32_t  b_swap4(const uint32_t& l)  {return ((l>>24)&0xff) | ((l>>8)&0xff00) | ((l<<8)&0xff0000) | ((l<<24)&0xff000000); }
static inline constexpr uint64_t  b_swap8(const uint64_t& ll) {return (((uint64_t)b_swap4(ll)) << 32) | ((uint64_t)b_swap4(ll >> 32)); }

static inline constexpr uint16_t  htoles(const uint16_t& s)    {return s;}
static inline constexpr uint32_t  htolel(const uint32_t& l)    {return l;}
static inline constexpr uint64_t  htolell(const uint64_t& ll)  {return ll;}
//static inline constexpr uint32_t  htolef(const float& f)       {return f;}
//static inline constexpr uint64_t  htoled(const double& d)      {return d;}
static inline constexpr uint16_t  letohs(const uint16_t& s)    {return s;}
static inline constexpr uint32_t  letohl(const uint32_t& l)    {return l;}
static inline constexpr uint64_t  letohll(const uint64_t& ll)  {return ll;}
//static inline constexpr float     letohf(const uint32_t& f)    {return f;}
//static inline constexpr double    letohd(const uint64_t& d)    {return d;}

static inline constexpr uint16_t  htobes(const uint16_t& s)    {return b_swap2(s);}
static inline constexpr uint32_t  htobel(const uint32_t& l)    {return b_swap4(l);}
static inline constexpr uint64_t  htobell(const uint64_t& ll)  {return b_swap8(ll);}
//static inline constexpr float     htobef(const float& f)       {return f;}
//static inline constexpr double    htobed(const double& d)      {return d;}
static inline constexpr uint16_t  betohs(const uint16_t& s)    {return b_swap2(s);}
static inline constexpr uint32_t  betohl(const uint32_t& l)    {return b_swap4(l);}
static inline constexpr uint64_t  betohll(const uint64_t& ll)  {return b_swap8(ll);}
//static inline constexpr float     betohf(const float& f)       {return f;} //byteorder defined by IEEE standard // union {uint32_t ui; float f;} res; res.ui = b_swap4(f); return res.f;}
//static inline constexpr double    betohd(const double& d)      {return d;} //byteorder defined by IEEE standard // union {uint64_t ui; double d;} res; res.ui = b_swap8(d); return res.d;}

static inline constexpr uint16_t  htons(const uint16_t& s)    {return htobes(s);}
static inline constexpr uint32_t  htonl(const uint32_t& l)    {return htobel(l);}
static inline constexpr uint64_t  htonll(const uint64_t& ll)  {return htobell(ll);}
//static inline constexpr uint32_t  htonf(const float& f)       {return f;}
//static inline constexpr uint64_t  htond(const double& d)      {return d;}
static inline constexpr uint16_t  ntohs(const uint16_t& s)    {return betohs(s);}
static inline constexpr uint32_t  ntohl(const uint32_t& l)    {return betohl(l);}
static inline constexpr uint64_t  ntohll(const uint64_t& ll)  {return betohll(ll);}
//static inline constexpr float     ntohf(const uint32_t& f)    {return f;}
//static inline constexpr double    ntohd(const uint64_t& d)    {return d;}

#ifdef __cplusplus
static inline constexpr int16_t   htoles(const int16_t& s)     {return s;}
static inline constexpr int32_t   htolel(const int32_t& l)     {return l;}
static inline constexpr int64_t   htolell(const int64_t& ll)   {return ll;}
static inline constexpr int16_t   letohs(const int16_t& s)     {return s;}
static inline constexpr int32_t   letohl(const int32_t& l)     {return l;}
static inline constexpr int64_t   letohll(const int64_t& ll)   {return ll;}
static inline constexpr int16_t   htobes(const int16_t& s)     {return (int16_t) b_swap2(s);}
static inline constexpr int32_t   htobel(const int32_t& l)     {return (int32_t) b_swap4(l);}
static inline constexpr int64_t   htobell(const int64_t& ll)   {return (int64_t) b_swap8(ll);}
static inline constexpr int16_t   betohs(const int16_t& s)     {return (int16_t) b_swap2(s);}
static inline constexpr int32_t   betohl(const int32_t& l)     {return (int32_t) b_swap4(l);}
static inline constexpr int64_t   betohll(const int64_t& ll)   {return (int64_t) b_swap8(ll);}
static inline constexpr int16_t   htons(const int16_t& s)     {return htobes(s);}
static inline constexpr int32_t   htonl(const int32_t& l)     {return htobel(l);}
static inline constexpr int64_t   htonll(const int64_t& ll)   {return htobell(ll);}
static inline constexpr int16_t   ntohs(const int16_t& s)     {return htobes(s);}
static inline constexpr int32_t   ntohl(const int32_t& l)     {return htobel(l);}
static inline constexpr int64_t   ntohll(const int64_t& ll)   {return htobell(ll);}
#endif

#ifdef __cplusplus

template <class valueT>
struct endian_fns {};

template <> struct endian_fns<uint8_t> {
	static inline constexpr uint8_t htobe(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t betoh(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t htole(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t letoh(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t hton(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t ntoh(const uint8_t& value) {return (value);} \
};
template <> struct endian_fns<int8_t> {
	static inline constexpr uint8_t htobe(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t betoh(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t htole(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t letoh(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t hton(const uint8_t& value) {return (value);} \
	static inline constexpr uint8_t ntoh(const uint8_t& value) {return (value);} \
};
#define ENDIANS_TEMPLATE_FNs(valueT, SUFFIX) \
template <> struct endian_fns<valueT> { \
	static inline constexpr valueT htobe(const valueT& value) {return htobe##SUFFIX(value);} \
	static inline constexpr valueT betoh(const valueT& value) {return betoh##SUFFIX(value);} \
	static inline constexpr valueT htole(const valueT& value) {return htole##SUFFIX(value);} \
	static inline constexpr valueT letoh(const valueT& value) {return letoh##SUFFIX(value);} \
	static inline constexpr valueT hton(const valueT& value) {return hton##SUFFIX(value);} \
	static inline constexpr valueT ntoh(const valueT& value) {return ntoh##SUFFIX(value);} \
};
ENDIANS_TEMPLATE_FNs(uint16_t, s)
ENDIANS_TEMPLATE_FNs(uint32_t, l)
ENDIANS_TEMPLATE_FNs(uint64_t, ll)
ENDIANS_TEMPLATE_FNs(int16_t,  s)
ENDIANS_TEMPLATE_FNs(int32_t,  l)
ENDIANS_TEMPLATE_FNs(int64_t,  ll)

template <class valueT, valueT (h2nFn)(const valueT&), valueT (n2hFn)(const valueT&) = h2nFn>
class BYTEORDERED_TYPE_TEMPLATE {
private:
	valueT m_byteorderedValue; // stored in network order
public:
	const valueT& operator = (valueT value) {m_byteorderedValue = h2nFn(value); return m_byteorderedValue;}
	operator valueT () const {return n2hFn(m_byteorderedValue);}
};

typedef BYTEORDERED_TYPE_TEMPLATE<int16_t, letohs, htoles> LE_I16;
typedef BYTEORDERED_TYPE_TEMPLATE<uint16_t, letohs, htoles> LE_U16;
typedef BYTEORDERED_TYPE_TEMPLATE<int32_t, letohl, htolel> LE_I32;
typedef BYTEORDERED_TYPE_TEMPLATE<uint32_t, letohl, htolel> LE_U32;
typedef BYTEORDERED_TYPE_TEMPLATE<int64_t, letohll, htolell> LE_I64;
typedef BYTEORDERED_TYPE_TEMPLATE<uint64_t, letohll, htolell> LE_U64;

typedef BYTEORDERED_TYPE_TEMPLATE<int16_t, betohs, htobes> BE_I16;
typedef BYTEORDERED_TYPE_TEMPLATE<uint16_t, betohs, htobes> BE_U16;
typedef BYTEORDERED_TYPE_TEMPLATE<int32_t, betohl, htobel> BE_I32;
typedef BYTEORDERED_TYPE_TEMPLATE<uint32_t, betohl, htobel> BE_U32;
typedef BYTEORDERED_TYPE_TEMPLATE<int64_t, betohll, htobell> BE_I64;
typedef BYTEORDERED_TYPE_TEMPLATE<uint64_t, betohll, htobell> BE_U64;

typedef BYTEORDERED_TYPE_TEMPLATE<int16_t, ntohs, htons> NET_I16;
typedef BYTEORDERED_TYPE_TEMPLATE<uint16_t, ntohs, htons> NET_U16;
typedef BYTEORDERED_TYPE_TEMPLATE<int32_t, ntohl, htonl> NET_I32;
typedef BYTEORDERED_TYPE_TEMPLATE<uint32_t, ntohl, htonl> NET_U32;
typedef BYTEORDERED_TYPE_TEMPLATE<int64_t, ntohll, htonll> NET_I64;
typedef BYTEORDERED_TYPE_TEMPLATE<uint64_t, ntohll, htonll> NET_U64;

#endif

#endif //ENDIANS_H_INCLUDED
