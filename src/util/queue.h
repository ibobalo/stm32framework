#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include "core.h"
#include "atomic.h"

template<class T, const int MaxCount=16>
class TQueue
{
public:
	TQueue():m_uiHeadIdx(0),m_uiTailIdx(0),m_nCount(0) {};
public:
	void Push(const T& value) {ASSERTE(m_nCount < MaxCount); m_aValues[m_uiHeadIdx] = value; Inc(m_uiHeadIdx); ++m_nCount;};
	unsigned Count() const { return m_nCount;};
	bool IsFull() const { return m_nCount == MaxCount;};
	bool IsEmpty() const { return m_nCount == 0; };
	T Pop() { ASSERTE(m_nCount > 0); T ret = m_aValues[m_uiTailIdx]; Inc(m_uiTailIdx); --m_nCount; return ret;};
	const T& First() const { ASSERTE(m_nCount > 0); return m_aValues[m_uiTailIdx];};
	void Discard(unsigned n = 1) { ASSERTE(m_nCount >= n); Inc(m_uiTailIdx, n); --m_nCount;};
private:
	static void Inc(unsigned& uiIndex) { uiIndex = (uiIndex + 1) % MaxCount;}
	static void Inc(unsigned& uiIndex, unsigned n) { uiIndex = (uiIndex + n) % MaxCount;}
	T m_aValues[MaxCount];
	unsigned m_uiHeadIdx;
	unsigned m_uiTailIdx;
	unsigned m_nCount;
};

template<class T, const int MaxCount=16>
class TAtomicQueue
{
public:
	enum {Capacity = MaxCount};
	bool Push(const T& value);
	bool Pop(T& retValue);
	void Clear() {unsigned tmp = 0; AtomicExchange(&m_oAtom.raw, &tmp);}
	bool IsEmpty() const {return m_oAtom.nCount == 0;}
private:
	typedef union atom_t {
		unsigned raw;
		struct {
			unsigned nCount :14;
			unsigned nPushLocks :2;
			unsigned uiHeadIdx :14;
		};
		enum {MaxLocks = (1<<2)-1, MaxIndex = (1<<14)-1};
		bool operator == (volatile atom_t& src) const {return raw == src.raw;}
	} CAtom;
	CAtom m_oAtom;
	volatile T m_aValues[MaxCount];
	STATIC_ASSERTE(CAtom::MaxIndex >= MaxCount, MaxCount_TooBig);
};




template<class T, const int MaxCount>
bool TAtomicQueue<T,MaxCount>::Push(const T& value) {
	bool bResult = true;
	CAtom oldAtom = m_oAtom;
	CAtom newAtom;
	do {
		if (oldAtom.nCount < MaxCount && oldAtom.nPushLocks < CAtom::MaxLocks) {
			newAtom = oldAtom;
			++newAtom.nCount;
			++newAtom.nPushLocks;
		} else {
			bResult = false;
			break;
		}
	} while (!AtomicCmpExchange(&m_oAtom.raw, &oldAtom.raw, newAtom.raw));
	if (bResult) {
		unsigned uiTailIndex = oldAtom.uiHeadIdx + oldAtom.nCount;
		if (uiTailIndex >= MaxCount) uiTailIndex -= MaxCount;
		m_aValues[uiTailIndex] = value;
		oldAtom = m_oAtom;
		do {
			newAtom = oldAtom;
			--newAtom.nPushLocks;
		} while (!AtomicCmpExchange(&m_oAtom.raw, &oldAtom.raw, newAtom.raw));
	}
	return bResult;
};

template<class T, const int MaxCount>
bool TAtomicQueue<T,MaxCount>::Pop(T& retValue) {
	bool bResult = true;
	CAtom oldAtom = m_oAtom;
	CAtom newAtom;
	do {
		if (oldAtom.nCount > 0 && oldAtom.nPushLocks == 0) {
			retValue = m_aValues[oldAtom.uiHeadIdx];
			newAtom = oldAtom;
			--newAtom.nCount;
			if (++newAtom.uiHeadIdx >= MaxCount) newAtom.uiHeadIdx = 0;
		} else {
			bResult = false;
			break;
		}
	} while (!AtomicCmpExchange(&m_oAtom, &oldAtom, newAtom));
	return bResult;
}
#endif // QUEUE_H_INCLUDED
