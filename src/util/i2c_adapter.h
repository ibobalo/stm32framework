#ifndef _I2C_ADAPTER_H_INCLUDED
#define _I2C_ADAPTER_H_INCLUDED

#include "interfaces/i2c.h"

#define I2CADAPTER_MAXREGSIZE 32

//extern
class IDeviceRegs;

class CI2CRegsAdapter :
		public ISlaveI2C
{
public:
	void Init(unsigned uiI2CAddress, IDeviceRegs* pRegs);
	void SetI2CAddress(unsigned uiI2CAddress);
public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const;
	virtual bool GetDataToSend(const CChainedConstChunks** ppRetChunksToSend);
	virtual bool GetBufferToReceive(const CChainedIoChunks** ppRetChunksToRecv);

	virtual void OnI2CSessionDone(unsigned nBytesCompleted);
	virtual void OnI2CError(EI2CErrorType e);

private:
	typedef enum {
		ST_INIT,
		ST_ADDR,
		ST_READ,
		ST_WRITE,
		ST_ERROR,
	} ESTATE;
private:
	bool SetRegValues(unsigned uiDataSize);
private:
	IDeviceRegs*        m_pRegs;
	unsigned            m_uiI2CAddress;
	ESTATE              m_eState;

	volatile uint8_t    m_uiRegAddress;
	CChainedIoChunks    m_cicRegAddressChunks;

	volatile uint8_t    m_aDataBuffer[I2CADAPTER_MAXREGSIZE];
	CChainedConstChunks m_cccRegReadValueChunks;
	CChainedIoChunks    m_cicRegWriteValueChunks;
	unsigned            m_uiRegSize;

};

#endif // _I2C_ADAPTER_H_INCLUDED
