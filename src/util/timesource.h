#ifndef TIMESOURCE_H_INCLUDED
#define TIMESOURCE_H_INCLUDED

template <class time_type = unsigned, class time_delta_type = int>
class TTimeSourceTemplate
{
public:
	typedef time_type time_t;
	typedef time_delta_type time_delta_t;
	inline static time_t GetTimeNow();
	inline static time_delta_t GetDeltaFromNano(uint64_t ullNano);
	inline static time_delta_t GetDeltaFromMilli(unsigned uiMilli);
	inline static time_delta_t GetDeltaFromSeconds(float fSeconds);
	inline static uint64_t GetNanoFromDelta(time_delta_t td);
	inline static unsigned GetMilliFromDelta(time_delta_t td);
	inline static float GetSecondsFromDelta(time_delta_t td);
};


#endif // TIMESOURCE_H_INCLUDED
