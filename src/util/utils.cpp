#include "stdafx.h"
#include "utils.h"
#include <stddef.h>
#include <math.h>
#include <string.h>

extern "C"
{
unsigned strnlen(const char *str, unsigned max_len)
{
    const char * end = (const char *)memchr(str, '\0', max_len);
    if (end == NULL)
        return max_len;
    else
        return end - str;
}
}

template<bool bSigned, bool bAutoRadix>
unsigned NumberParserT(const char* pString, unsigned uiLength, unsigned* puiResult, bool* pbNegative)
{
	*puiResult = 0;
	if (bSigned && pbNegative) *pbNegative = false;
	unsigned uiRadix = 10;
	unsigned uiResult = 0;
	for (unsigned n = 0; n < uiLength; ++n) {
		uiResult *= uiRadix;
		char ch = (char)pString[n];
		switch (ch) {
			case '-':
				if (bSigned && !uiResult) {if (pbNegative) *pbNegative = true;}
				else return n;
				break;
			case '+':
				if (bSigned && !uiResult) {if (pbNegative) *pbNegative = false;}
				else return n;
				break;
			case '0':
				if (bAutoRadix && !uiResult && uiRadix == 10) uiRadix = 8;
				break;
			case '1':
				uiResult += (ch - '0');
				break;
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
				if (!bAutoRadix || uiRadix > 2) uiResult += (ch - '0');
				else return n;
				break;
			case '8':
			case '9':
				if (!bAutoRadix || uiRadix > 8) uiResult += (ch - '0');
				else return n;
				break;
			case 'a':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
				if (!bAutoRadix) return n;
				if (uiRadix > 10) uiResult += (ch - 'a');
				else return n;
				break;
			case 'A':
			case 'C':
			case 'D':
			case 'E':
			case 'F':
				if (!bAutoRadix || uiRadix > 10) uiResult += (ch - 'A');
				break;
			case 'b':
				if (!bAutoRadix) return n;
				if (uiRadix > 10) uiResult += (ch - 'a');
				else if (!uiResult) uiRadix = 2;
				else return n;
				break;
			case 'B':
				if (!bAutoRadix) return n;
				if (uiRadix > 10) uiResult += (ch - 'A');
				else if (!uiResult) uiRadix = 2;
				else return n;
				break;
			case 'x':
			case 'X':
				if (bAutoRadix && !uiResult) uiRadix = 16;
				break;
			default:
				return n;
		}
		*puiResult = uiResult;
	}
	return uiLength;
}

unsigned NumberParserU(unsigned& uiResult, const char* pString, unsigned uiLength)
{
	unsigned uiParsedLength = NumberParserT<false, true>(pString, uiLength, &uiResult, NULL);
	return uiParsedLength;
}

unsigned NumberParserI(int& iResult, const char* pString, unsigned uiLength)
{
	bool bNegative;
	unsigned uiNumber;
	unsigned uiParsedLength = NumberParserT<true, true>(pString, uiLength, &uiNumber, &bNegative);
	iResult = bNegative?-(int)uiNumber:(int)uiNumber;
	return uiParsedLength;
}

unsigned NumberParserF(float& fResult, const char* pString, unsigned uiLength)
{
	unsigned uiPreDot;
	bool bNegative;
	unsigned uiPreDotLength = NumberParserT<true, false>(pString, uiLength, &uiPreDot, &bNegative);
	if (uiPreDotLength + 1 >= uiLength || (pString[uiPreDotLength] != '.' && pString[uiPreDotLength] != ',')) {
		fResult = bNegative ? -(float)uiPreDot : (float)uiPreDot;
		return uiPreDotLength;
	}
	unsigned uiPostDot;
	unsigned uiPostDotLength = NumberParserT<false, false>(pString + uiPreDotLength + 1, uiLength - uiPreDotLength - 1, &uiPostDot, NULL);
	if (uiPostDotLength && uiPostDot) {
		fResult = (float)uiPostDot;
		for (unsigned n = 0; n < uiPostDotLength; ++n) fResult /= 10.0F;
		fResult += (float)uiPreDot;
	}
	return uiPreDotLength + 1 + uiPostDotLength;
}



#define AppendChar(ch)  ((pStr <= pBufferLast) ? (*pStr = ch, ++pStr, true ) : false)
#define PrependChar(ch) ((pStr > pBuffer) ? (--pStr, *pStr = ch, true) : false)
template<class T, bool bSigned, unsigned nBase=10, char ch10='A'>
char* NumberConvertT(T tValue, char* const pBuffer, unsigned uiBufferSize, unsigned* puiResultLength)
{
	char* pStr;
	unsigned uiLength;
	T tRemain;
	char* const pBufferLast = pBuffer + uiBufferSize - 1;
	pStr = pBufferLast;
	*pStr = '\0';
	uiLength = 0;
	tRemain = tValue;
	if (bSigned && tRemain < 0) tRemain = -tRemain;
	while (tRemain) {
		char ch = (tRemain%nBase) + '0';
		if (nBase > 10 && ch > '9') {ch += ch10 - '9' - 1;}
		if (PrependChar(ch)) {
			tRemain /= nBase;
		} else {
			break;
		}
	}
	uiLength = pBuffer + uiBufferSize - 1 - pStr;
	if (!uiLength) {
		PrependChar('0');
		++uiLength;
	}
	if (bSigned && tValue < 0) {
		if (PrependChar('-')) {++uiLength;} else {pBuffer[0] = '<';}
	}
	if (tRemain) {
		pBuffer[0] = '>';
	}
	if (puiResultLength) *puiResultLength = uiLength;
	return(pStr);
}
template<class T>
const char* NumberFConvertT(T tValue, char* const pBuffer, unsigned uiBufferSize, unsigned uiPrecision, unsigned* puiResultLength)
{
	char* pStr;
	unsigned uiLength;
	char* const pBufferLast = pBuffer + uiBufferSize - 1;
	unsigned uiRemain = (unsigned)( (tValue < 0) ? -tValue : tValue);
	T tFract = ((tValue < 0) ? (-tValue - unsigned(-tValue)) : (tValue - unsigned(tValue))) * 10;
	char* pDot = (uiPrecision + 2 > uiBufferSize) ? pBuffer : (pBufferLast - 1 - uiPrecision);
//	ASSERTE(pDot >= pBuffer && pDot <= pBufferLast);
	*pDot = '.';
	pStr = pDot + 1;
	while (tFract != 0.0 && AppendChar((char)(tFract) + '0')) {
		tFract = (tFract - int(tFract)) * 10;
	}
	uiLength = pStr - pDot;
	if (!AppendChar('\0')) {*pBufferLast = '\0'; --uiLength;}
	pStr = pDot;
	while (uiRemain && PrependChar((char)(uiRemain%10) + '0')) {
		uiRemain /= 10;
	}
	uiLength += pDot - pStr;
	if (pStr == pDot) {
		if (PrependChar('0')) {++uiLength;}
	}
	if (tValue < 0) {
		if (PrependChar('-')) {++uiLength;} else {pBuffer[0] = '<';}
	}
	if (uiRemain) {
		pBuffer[0] = '>';
	}
	if (puiResultLength) *puiResultLength = uiLength;
	return(pStr);
}

const char* NumberConvertU(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<unsigned, false>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertI(int iValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<int, true>(iValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertB(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<unsigned, false, 2>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertO(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<unsigned, false, 8>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertH(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<unsigned, false, 16>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertUU(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<uint64_t, false>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertII(int64_t iValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<int64_t, true>(iValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertBB(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<uint64_t, false, 2>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertOO(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<uint64_t, false, 8>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertHH(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength)
{
	return NumberConvertT<uint64_t, false, 16>(uiValue, pBuffer, uiSize, puiResultLength);
}
const char* NumberConvertF(float fValue, char* pBuffer, unsigned uiSize, unsigned nPrecision, unsigned* puiResultLength)
{
	return NumberFConvertT<float>(fValue, pBuffer, uiSize, nPrecision, puiResultLength);
}
const char* NumberConvertD(double dValue, char* pBuffer, unsigned uiSize, unsigned nPrecision, unsigned* puiResultLength)
{
	return NumberFConvertT<double>(dValue, pBuffer, uiSize, nPrecision, puiResultLength);
}

bool StringAlignL(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill)
{
	bool bFit = (uiSrcLength < uiSize);
	if (bFit) {
		unsigned uiLengthToCopy =  uiSrcLength;
		unsigned uiLengthToFill = uiSize - 1 - uiLengthToCopy;
		memcpy(pBuffer, pSrcStr, uiLengthToCopy);
		if (uiLengthToFill) {
			memset(pBuffer + uiLengthToCopy, cFill, uiLengthToFill);
		}
	} else {
		memcpy(pBuffer, pSrcStr, uiSize - 1);
	}
	pBuffer[uiSize - 1] = '\0';
	return bFit;
}
bool StringAlignR(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill)
{
	bool bFit = (uiSrcLength < uiSize);
	if (bFit) {
		unsigned uiLengthToCopy =  uiSrcLength;
		unsigned uiLengthToFill = uiSize - 1 - uiLengthToCopy;
		if (uiLengthToFill) {
			memset(pBuffer, cFill, uiLengthToFill);
		}
		memcpy(pBuffer + uiLengthToFill, pSrcStr, uiLengthToCopy);
	} else {
		unsigned uiLengthToSkip =  uiSrcLength - uiSize + 1;
		memcpy(pBuffer, pSrcStr + uiLengthToSkip, uiSize - 1);
	}
	pBuffer[uiSize - 1] = '\0';
	return bFit;
}
bool StringAlignC(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill)
{
	bool bFit = (uiSrcLength < uiSize);
	if (bFit) {
		unsigned uiLengthToCopy =  uiSrcLength;
		unsigned uiLengthToFill = uiSize - 1 - uiLengthToCopy;
		unsigned uiLengthToFillL = uiLengthToFill / 2;
		unsigned uiLengthToFillR = uiLengthToFill - uiLengthToFillL;
		if (uiLengthToFillL) {
			memset(pBuffer, cFill, uiLengthToFillL);
		}
		memcpy(pBuffer + uiLengthToFillL, pSrcStr, uiLengthToCopy);
		if (uiLengthToFillR) {
			memset(pBuffer + uiLengthToFillL + uiLengthToCopy, cFill, uiLengthToFill);
		}
	} else {
		unsigned uiLengthToSkip =  uiSrcLength - uiSize + 1;
		unsigned uiLengthToSkipL =  uiLengthToSkip / 2;
		memcpy(pBuffer, pSrcStr + uiLengthToSkipL, uiSize - 1);
	}
	pBuffer[uiSize - 1] = '\0';
	return bFit;
}
