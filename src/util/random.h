#ifndef RANDOM_H_INCLUDED
#define RANDOM_H_INCLUDED

#include <stdint.h>
#include <limits>

template<class T = uint16_t, unsigned M = 2145, unsigned S = 17>
class TRandSource
{
public:
	T GetRand(T uiRange) {DoRand(); return m_uiRandSeed % uiRange;};
	float GetRandF(float dRange = 1.0F) {DoRand(); return dRange * m_uiRandSeed / (float)(std::numeric_limits<T>::max());};
private:
	void DoRand() {m_uiRandSeed = (M * m_uiRandSeed + S);};
	T m_uiRandSeed;
};

#endif
