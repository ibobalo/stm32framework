#ifndef DISPLAY_FILTER_H_INCLUDED
#define DISPLAY_FILTER_H_INCLUDED

#include "interfaces/common.h"
#include "interfaces/display.h"
#include "util/utils.h"

template<class valueT, unsigned uiLength, unsigned uiPrecision = 0, EAlignStringType eAlign = AST_LEFT, char cFill = ' '>
class TDisplayField :
	public IValueReceiver<valueT>, // value
	public IValueReceiver<bool>    // valid
{
public:
	typedef valueT value_type;
public:
	void Init(IDisplayDeviceMonochrome* pDisplay, const CCoordinates& Position);
public: // IValueReceiver
	virtual void SetValue(value_type tValue);
	virtual void SetValue(bool bValid);

private:
	IDisplayDeviceMonochrome*     m_pDisplay;
	CCoordinates                  m_Position;
	bool                          m_bValueValid;
	value_type                    m_tValueDisplayed;
	AlignString<uiLength, eAlign> m_AlignString;
};

template<class valueT, unsigned uiLength, unsigned uiPrecision, EAlignStringType eAlign, char cFill>
void TDisplayField<valueT, uiLength, uiPrecision, eAlign, cFill>::Init(IDisplayDeviceMonochrome* pDisplay, const CCoordinates& Position)
{
	m_pDisplay = pDisplay;
	m_Position = Position;
	m_bValueValid = false;
}

/*virtual*/
template<class valueT, unsigned uiLength, unsigned uiPrecision, EAlignStringType eAlign, char cFill>
void TDisplayField<valueT, uiLength, uiPrecision, eAlign, cFill>::SetValue(value_type tValue)
{
	if (!m_bValueValid || m_tValueDisplayed != tValue) {
		m_AlignString.Align(ToString<15, uiPrecision>(tValue), cFill);
		m_pDisplay->GotoXY(m_Position);
		m_pDisplay->PutString(m_AlignString.GetValue(), true);
		m_bValueValid = true;
		m_tValueDisplayed = tValue;
	}
}

/*virtual*/
template<class valueT, unsigned uiLength, unsigned uiPrecision, EAlignStringType eAlign, char cFill>
void TDisplayField<valueT, uiLength, uiPrecision, eAlign, cFill>::SetValue(bool bValid)
{
	if (m_bValueValid != bValid) {
		if (m_bValueValid) {
			m_pDisplay->GotoXY(m_Position);
			for (unsigned n = 0; n < uiLength; ++n) {
				m_pDisplay->PutChar('-', true);
			}
		}
		m_bValueValid = bValid;
	}
}

#endif // DISPLAY_FIELD_H_INCLUDED
