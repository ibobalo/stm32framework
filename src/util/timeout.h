#ifndef TIMEOUT_H_INCLUDED
#define TIMEOUT_H_INCLUDED

#include "timesource.h"

template <class CTimeSource, class time_type = typename CTimeSource::time_t, class time_delta_type = typename CTimeSource::time_delta_t>
class TTimeoutTemplate
{
public:
	inline void Init(const time_delta_type& tTimeout) { m_tdTimeout = tTimeout; SetElapsed();}
	inline void Start()                               { m_ttFinishTime = CTimeSource::GetTimeNow() + m_tdTimeout + 1; }
	inline void Start(const time_delta_type& tTimeout){ m_tdTimeout = tTimeout; Start(); }
	inline void SetElapsed()                          { m_ttFinishTime = CTimeSource::GetTimeNow() - 1; }
	inline time_delta_type GetTimeLeft() const { return (time_delta_type)(m_ttFinishTime - CTimeSource::GetTimeNow()); }
	inline const time_delta_type& GetTimeout() const  { return m_tdTimeout; }
	inline const time_type& GetFinishTime() const     { return m_ttFinishTime; }
	inline bool GetIsElapsed() const                  { return (GetTimeLeft() <= 0); }
	inline bool GetIsDefined() const                  { return (m_tdTimeout > 0); }
private:
	time_delta_type  m_tdTimeout;
	time_type        m_ttFinishTime;
};

#endif // TIMEOUT_H_INCLUDED
