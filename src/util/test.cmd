g++ qmath.cpp -fmax-errors=2 -std=gnu++11 -lgcc -g -O0 -o qmath.exe && (qmath.exe || gdb qmath.exe --ex "b test_all" --ex r)
g++ crc.cpp -fmax-errors=2 -std=gnu++11 -lgcc -g -O0 -o crc.exe && (crc.exe || gdb crc.exe)
g++ moveprofile.c moveprofile.cpp -fmax-errors=2 -std=gnu++11 -lgcc -g -O0 -o moveprofile.exe && (moveprofile.exe || gdb moveprofile.exe --ex "b test_all" --ex r)
