#include "crc.h"
#include <iostream>
#include <iomanip>

void Default_Handler(void) {};

template<class T>
bool test(const char* name = "")
{
	auto c1 = T::Calculate("123456789", 9);
	auto c2 = T::CalculateFast("123456789", 9);
//	ASSERTE(c1 == T::check && c2 == T::check);
	if (c1 == T::check && c2 == T::check) {
		std::cout << "Valid: " << name << " " << std::hex << +T::check << std::endl;
		return true;
	}
	unsigned w = (T::BITS_USED + 3) / 4;
	std::cerr << "Error: " << name << " ["
		<< +T::BITS_USED << ","
		<< std::hex << std::uppercase << std::setfill('0')
		<< std::setw(w) << +T::poly << ","
		<< std::setw(w) << +T::init << ","
		<< (T::refin?"true,":"false,")
		<< (T::refout?"true,":"false,")
		<< std::setw(w) << +T::xorout
		<< "] "
		<< std::setw(w) << +c1 << ","
		<< std::setw(w) << +c2 << "!="
		<< std::setw(w) << +T::check
		<< std::endl;
//	for (unsigned n = 0; n < 16; ++n) {
//		std::cerr << " " << std::hex << std::uppercase << std::setfill('0') << std::setw(2) << +T::table[n];
//	}
//	std::cerr << std::endl;
	return false;
}

bool test_crcs()
{
	unsigned n = 0, m = 0;
	bool result = true;
	result = (++n, test<CRC3_GSM         >("CRC-3/GSM"          )) && (++m, result);
	result = (++n, test<CRC3_ROHC        >("CRC-3/ROHS"         )) && (++m, result);
	result = (++n, test<CRC4_INTERLAKEN  >("CRC-4/INTERLAKEN"   )) && (++m, result);
	result = (++n, test<CRC4_ITU         >("CRC-4/ITU"          )) && (++m, result);
	result = (++n, test<CRC5_EPC         >("CRC-5/EPC"          )) && (++m, result);
	result = (++n, test<CRC5_ITU         >("CRC-5/ITU"          )) && (++m, result);
	result = (++n, test<CRC5_USB         >("CRC-5/USB"          )) && (++m, result);
	result = (++n, test<CRC6_CDMA2000A   >("CRC-6/CDMA2000-A"   )) && (++m, result);
	result = (++n, test<CRC6_CDMA2000B   >("CRC-6/CDMA2000-B"   )) && (++m, result);
	result = (++n, test<CRC6_DARC        >("CRC-6/DARC"         )) && (++m, result);
	result = (++n, test<CRC6_ITU         >("CRC-6/ITU"          )) && (++m, result);
	result = (++n, test<CRC6_GSM         >("CRC-6/GSM"          )) && (++m, result);
	result = (++n, test<CRC7             >("CRC-7"              )) && (++m, result);
	result = (++n, test<CRC7_ROHC        >("CRC-7/ROHC"         )) && (++m, result);
	result = (++n, test<CRC7_UMTS        >("CRC-7/UMTS"         )) && (++m, result);
	result = (++n, test<CRC8             >("CRC-8"              )) && (++m, result);
	result = (++n, test<CRC8_AES         >("CRC-8/AES"          )) && (++m, result);
	result = (++n, test<CRC8_AUTOSTAR    >("CRC-8/AUTOSTAR"     )) && (++m, result);
	result = (++n, test<CRC8_BLUETOOTH   >("CRC-8/BLUETOOTH"    )) && (++m, result);
	result = (++n, test<CRC8_CDMA2000    >("CRC-8/CDMA2000"     )) && (++m, result);
	result = (++n, test<CRC8_DARC        >("CRC-8/DARC"         )) && (++m, result);
	result = (++n, test<CRC8_DVBS2       >("CRC-8/DVB-S2"       )) && (++m, result);
	result = (++n, test<CRC8_GSMA        >("CRC-8/GSM-A"        )) && (++m, result);
	result = (++n, test<CRC8_GSMB        >("CRC-8/GSM-B"        )) && (++m, result);
	result = (++n, test<CRC8_ITU         >("CRC-8/ITU"          )) && (++m, result);
	result = (++n, test<CRC8_ICODE       >("CRC-8/I-CODE"       )) && (++m, result);
	result = (++n, test<CRC8_LTE         >("CRC-8/LTE"          )) && (++m, result);
	result = (++n, test<CRC8_MAXIM       >("CRC-8/MAXIM"        )) && (++m, result);
	result = (++n, test<CRC8_MIRAFARE    >("CRC-8/MIRAFARE"     )) && (++m, result);
	result = (++n, test<CRC8_NRSC5       >("CRC-8/NRSC-5"       )) && (++m, result);
	result = (++n, test<CRC8_OPENSAFETY  >("CRC-8/OPENSAFETY"   )) && (++m, result);
	result = (++n, test<CRC8_ROHC        >("CRC-8/ROHC"         )) && (++m, result);
	result = (++n, test<CRC8_SAEJ1850    >("CRC-8/SAE-J1850"    )) && (++m, result);
	result = (++n, test<CRC8_WCDMA       >("CRC-8/WCDMA"        )) && (++m, result);
	result = (++n, test<CRC10            >("CRC-10"             )) && (++m, result);
	result = (++n, test<CRC10_CDMA2000   >("CRC-10/CDMA2000"    )) && (++m, result);
	result = (++n, test<CRC10_GSM        >("CRC-10/GSM"         )) && (++m, result);
	result = (++n, test<CRC11            >("CRC-11"             )) && (++m, result);
	result = (++n, test<CRC11_UMTS       >("CRC-11/UMTS"        )) && (++m, result);
	result = (++n, test<CRC12_3GPP       >("CRC-12/3GPP"        )) && (++m, result);
	result = (++n, test<CRC12_CDMA2000   >("CRC-12/CDMA2000"    )) && (++m, result);
	result = (++n, test<CRC12_DECT       >("CRC-12/DECT"        )) && (++m, result);
	result = (++n, test<CRC12_GSM        >("CRC-12/GSM"         )) && (++m, result);
	result = (++n, test<CRC13_BBC        >("CRC-13/BBC"         )) && (++m, result);
	result = (++n, test<CRC14_DARC       >("CRC-14/DARC"        )) && (++m, result);
	result = (++n, test<CRC14_GSM        >("CRC-14/GSM"         )) && (++m, result);
	result = (++n, test<CRC15_CAN        >("CRC-15/CAN"         )) && (++m, result);
	result = (++n, test<CRC15_MPT1327    >("CRC-15/MPT1327"     )) && (++m, result);
	result = (++n, test<CRC16            >("CRC-16"             )) && (++m, result);
	result = (++n, test<CRC16_AUGCCITT   >("CRC-16/AUGCCITT"    )) && (++m, result);
	result = (++n, test<CRC16_BUYPASS    >("CRC-16/BUYPASS"     )) && (++m, result);
	result = (++n, test<CRC16_CCITT      >("CRC-16/CCITT"       )) && (++m, result);
	result = (++n, test<CRC16_CCITTFALSE >("CRC-16/CCITT-FALSE" )) && (++m, result);
	result = (++n, test<CRC16_CDMA2000   >("CRC-16/CDMA2000"    )) && (++m, result);
	result = (++n, test<CRC16_CMS        >("CRC-16/CMS"         )) && (++m, result);
	result = (++n, test<CRC16_DDS110     >("CRC-16/DDS110"      )) && (++m, result);
	result = (++n, test<CRC16_DECTR      >("CRC-16/DECT-R"      )) && (++m, result);
	result = (++n, test<CRC16_DECTX      >("CRC-16/DECT-X"      )) && (++m, result);
	result = (++n, test<CRC16_DNP        >("CRC-16/DNP"         )) && (++m, result);
	result = (++n, test<CRC16_EN13757    >("CRC-16/EN13757"     )) && (++m, result);
	result = (++n, test<CRC16_GENIBUS    >("CRC-16/GENIBUS"     )) && (++m, result);
	result = (++n, test<CRC16_GSM        >("CRC-16/GSM"         )) && (++m, result);
	result = (++n, test<CRC16_IBMSDLC    >("CRC-16/IBMSDLC"     )) && (++m, result);
	result = (++n, test<CRC16_ISOIEC14443>("CRC-16/ISOIEC14443" )) && (++m, result);
	result = (++n, test<CRC16_LJ1200     >("CRC-16/LJ1200"      )) && (++m, result);
	result = (++n, test<CRC16_MAXIM      >("CRC-16/MAXIM"       )) && (++m, result);
	result = (++n, test<CRC16_MCRF4XX    >("CRC-16/MCRF4XX"     )) && (++m, result);
	result = (++n, test<CRC16_MODBUS     >("CRC-16/MODBUS"      )) && (++m, result);
	result = (++n, test<CRC16_NRSC5      >("CRC-16/NRSC-5"      )) && (++m, result);
	result = (++n, test<CRC16_OPENSAFETYA>("CRC-16/OPENSAFETY-A")) && (++m, result);
	result = (++n, test<CRC16_OPENSAFETYB>("CRC-16/OPENSAFETY-B")) && (++m, result);
	result = (++n, test<CRC16_PROFIBUS   >("CRC-16/PROFIBUS"    )) && (++m, result);
	result = (++n, test<CRC16_RIELLO     >("CRC-16/RIELLO"      )) && (++m, result);
	result = (++n, test<CRC16_T10DIF     >("CRC-16/T10DIF"      )) && (++m, result);
	result = (++n, test<CRC16_TELEDISK   >("CRC-16/TELEDISK"    )) && (++m, result);
	result = (++n, test<CRC16_TMS37157   >("CRC-16/TMS37157"    )) && (++m, result);
	result = (++n, test<CRC16_XMODEM     >("CRC-16/XMODEM"      )) && (++m, result);
	result = (++n, test<CRC16_USB        >("CRC-16/USB"         )) && (++m, result);
	result = (++n, test<CRC17_CANFD      >("CRC-17/CANFD"       )) && (++m, result);
	result = (++n, test<CRC21_CANFD      >("CRC-21/CANFD"       )) && (++m, result);
	result = (++n, test<CRC24            >("CRC-24"             )) && (++m, result);
	result = (++n, test<CRC24_BLE        >("CRC-24/BLE"         )) && (++m, result);
	result = (++n, test<CRC24_FLEXRAYA   >("CRC-24/FLEXRAYA"    )) && (++m, result);
	result = (++n, test<CRC24_FLEXRAYB   >("CRC-24/FLEXRAYB"    )) && (++m, result);
	result = (++n, test<CRC24_INTERLAKEN >("CRC-24/INTERLAKEN"  )) && (++m, result);
	result = (++n, test<CRC24_LTEA       >("CRC-24/LTE-A"       )) && (++m, result);
	result = (++n, test<CRC24_LTEB       >("CRC-24/LTE-B"       )) && (++m, result);
	result = (++n, test<CRC24_OS9        >("CRC-24/OS-9"        )) && (++m, result);
	result = (++n, test<CRC30_CDMA       >("CRC-30/CDMA"        )) && (++m, result);
	result = (++n, test<CRC31_PHILIPS    >("CRC-31/PHILIPS"     )) && (++m, result);
	result = (++n, test<CRC32            >("CRC-32"             )) && (++m, result);
	result = (++n, test<CRC32_AIXM       >("CRC-32/AIXM"        )) && (++m, result);
	result = (++n, test<CRC32_AUTOSAR    >("CRC-32/AUTOSAR"     )) && (++m, result);
	result = (++n, test<CRC32_BASE91D    >("CRC-32/BASE91-D"    )) && (++m, result);
	result = (++n, test<CRC32_BZIP2      >("CRC-32/BZIP2"       )) && (++m, result);
	result = (++n, test<CRC32_CDROMEDC   >("CRC-32/CD-ROM-EDC"  )) && (++m, result);
	result = (++n, test<CRC32_CKSUM      >("CRC-32/CKSUM"       )) && (++m, result);
	result = (++n, test<CRC32_ISCSI      >("CRC-32/ISCSI"       )) && (++m, result);
	result = (++n, test<CRC32_JAMCRC     >("CRC-32/JAMCRC"      )) && (++m, result);
	result = (++n, test<CRC32_MPEG2      >("CRC-32/MPEG2"       )) && (++m, result);
	result = (++n, test<CRC32_XFER       >("CRC-32/XFER"        )) && (++m, result);
	result = (++n, test<CRC40_GSM        >("CRC-40/GSM"         )) && (++m, result);
	result = (++n, test<CRC64            >("CRC-64"             )) && (++m, result);
	result = (++n, test<CRC64_GOISO      >("CRC-64/GO-ISO"      )) && (++m, result);
	result = (++n, test<CRC64_WE         >("CRC-64/WE"          )) && (++m, result);
	result = (++n, test<CRC64_XZ         >("CRC-64/XZ"          )) && (++m, result);

	std::cout << std::dec << "Total: " << n << ". Errors: " << n - m << std::endl;

	return result;
}

int main()
{
	return test_crcs() ? 0 : 1;
}
    
