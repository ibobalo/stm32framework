#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

/*
 * usage:
 *
 * struct CDataChunkBuffer {
 *  uint8_t*  p; // data ptr
 *  unsigned  s; // data size
 * };
 *
 * ListedItem<CDataChunkBuffer> d1, d2, d3;
 * d1.p = "aa";   d1.s = 2;
 * d2.p = "bbbb"; d2.s = 4;
 * d3.p = "c";    d3.s = 1;
 * d1.append(d2); d2.append(d3);
 */
template<class T>
class ListedItem:
		public T
{
public:
	typedef T TItemType;
	typedef ListedItem<T> TListedItemType;
public:
	ListedItem() : T(), m_pNextItem(0) {};
public:
	void Append(TListedItemType* item) { m_pNextItem = item; };
/*	void Extend(TListedItemType* item) { TListedItemType* p = this;  while(p->m_pNextItem) {p = p->m_pNextItem;} p->m_pNextItem = item; };*/
	void Break() {m_pNextItem = 0;};
	const TListedItemType* GetNextItem() const { return m_pNextItem; };
	TListedItemType* GetNextItem() { return m_pNextItem; };
private:
	TListedItemType* m_pNextItem;
};

#endif // LIST_H_INCLUDED
