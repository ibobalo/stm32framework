#include "qmath.h"                                                                                                                                                                 
#include "math.h"
#include <iostream>                                                                                                                                                                
#include <iomanip>                                                                                                                                                                 
#include <type_traits>
                                                                                                                                                                                   
void Default_Handler(void) {};                                                                                                                                                     
                                                                                                                                                                                   
static unsigned nCases = 0;                                                                                                                                                        
static unsigned nErrors = 0;                                                                                                                                                       
                                                                                                                                                                                   
template<class Tres>
class TestCase                                                                                                                                                                     
{                                                                                                                                                                                  
	virtual Tres test_fn() = 0;
public:
	TestCase(const char* name, Tres expected):m_name(name),m_expected(expected) {}
	virtual ~TestCase() {}
	virtual std::ostream& operator << (std::ostream& o) const = 0;
	bool do_test() {
		Tres res = test_fn();
		bool bResult = (res == m_expected);
		if (bResult) {                                                                                                                                                             
//			std::cout << "Valid: " << *this << " = " << +res << std::endl;
		} else {                                                                                                                                                                   
			std::cerr << "ERROR: " << *this << " = " << +res << " != " << +m_expected << std::endl;
			++nErrors;                                                                                                                                                             
		}                                                                                                                                                                          
		++nCases;                                                                                                                                                                  
		return bResult;                                                                                                                                                            
	}
protected:
	const char* m_name;
	Tres m_expected;
};

template<class Tres>
std::ostream& operator<< (std::ostream& o, const TestCase<Tres>& t) {
    t << o;
    return o;
}
                                                                                                                                                                                   
template<class T, unsigned N, class IType = T, class OType = IType>
class TestCaseUno : public TestCase<OType> {
public:
	typedef TFixedPoint<T, N> QType;
	TestCaseUno(const char* name, IType x, OType expected):TestCase<OType>(name, expected),m_x(x) {}
	virtual ~TestCaseUno() {}
	virtual OType test_fn(QType x) = 0;
protected:
	virtual OType test_fn() override {
		return test_fn(m_x);
	}
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << (QType::bSigned?"i":"u") << "Q" << QType::nIntegerBits << "." << N << ": " << TestCase<OType>::m_name << " " << + m_x.i();
		return o;}
	QType m_x;
};                                                                                                                                                                                 
                                                                                                                                                                                   
template<class T, unsigned N1, unsigned N2, class IType = T, class OType = IType>
class TestCaseDuo : public TestCase<OType> {
public:
	typedef TFixedPoint<T, N1> QType1;
	typedef TFixedPoint<T, N2> QType2;
	TestCaseDuo(const char* name, IType x, IType y, OType expected):TestCase<OType>(name, expected),m_x(x),m_y(y) {}
	virtual OType test_fn(QType1 x, QType2 y) = 0;
protected:
	virtual OType test_fn() override {
		return test_fn(m_x, m_y);
	}                                                                                                                                                                              
	virtual std::ostream& operator << (std::ostream& o) const override {
		o << (QType1::bSigned?"i":"u") << "Q" << QType1::nIntegerBits << "." << N1;
		if (N2 != N1) o << ",Q" << QType2::nIntegerBits << "." << N2;
		o << ": " << +m_x.i() << " " << TestCase<OType>::m_name << " " << +m_y.i();
		return o;}
	QType1 m_x;
	QType2 m_y;
};                                                                                                                                                                                 
                                                                                                                                                                                   
template<class T, unsigned N, class IType = T, class OType = IType>
class TestConvert : public TestCaseUno<T, N, IType, OType> {
public:
	using QType = typename TestCaseUno<T, N, IType, OType>::QType;
	TestConvert(IType x, OType expected) : TestCaseUno<T, N, IType, OType>("->", x, expected) {}
	virtual OType test_fn(QType x) override {
		return OType(x);
	}                                                                                                                                                                              
};                                                                                                                                                                                 
                                                                                                                                                                                   
template<class T, unsigned N1, unsigned N2 = N1, class IType = T, class OType = IType>
class TestAdd : public TestCaseDuo<T, N1, N2, IType, OType> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType2;
	TestAdd(IType x, IType y, OType expected) : TestCaseDuo<T, N1, N2, IType, OType>("+", x, y, expected) {}
	virtual OType test_fn(QType1 x, QType2 y) override {
		return OType(x + y);
	}                                                                                                                                                                              
};                                                                                                                                                                                 

template<class T, unsigned N1, unsigned N2 = N1, class IType = T, class OType = IType>
class TestSub : public TestCaseDuo<T, N1, N2, IType, OType> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType2;
	TestSub(IType x, IType y, OType expected) : TestCaseDuo<T, N1, N2, IType, OType>("-", x, y, expected) {}
	virtual OType test_fn(QType1 x, QType2 y) override {
		return OType(x - y);
	}                                                                                                                                                                              
};                                                                                                                                                                                 

template<class T, unsigned N1, unsigned N2 = N1, class IType = T, class OType = IType>
class TestMul : public TestCaseDuo<T, N1, N2, IType, OType> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType2;
	TestMul(IType x, IType y, OType expected) : TestCaseDuo<T, N1, N2, IType, OType>("*", x, y, expected) {}
	virtual OType test_fn(QType1 x, QType2 y) override {
		return OType(x * y);
	}                                                                                                                                                                              
};                                                                                                                                                                                 

template<class T, unsigned N1, unsigned N2 = N1, class IType = T, class OType = IType>
class TestDiv : public TestCaseDuo<T, N1, N2, IType, OType> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, OType>::QType2;
	TestDiv(IType x, IType y, OType expected) : TestCaseDuo<T, N1, N2, IType, OType>("/", x, y, expected) {}
	virtual OType test_fn(QType1 x, QType2 y) override {
		return OType(x / y);
	}                                                                                                                                                                              
};                                                                                                                                                                                 
                                                                                                                                                                                   
template<class T, unsigned N, class IType = T>
class TestLZ : public TestCaseUno<T, N, IType, bool> {
public:
	using QType = typename TestCaseUno<T, N, IType, bool>::QType;
	TestLZ(IType x, bool expected) : TestCaseUno<T, N, IType, bool>("<0", x, expected) {}
	virtual bool test_fn(QType x) override {
		return x < T(0);
	}
};

template<class T, unsigned N, class IType = T>
class TestGZ : public TestCaseUno<T, N, IType, bool> {
public:
	using QType = typename TestCaseUno<T, N, IType, bool>::QType;
	TestGZ(IType x, bool expected) : TestCaseUno<T, N, IType, bool>(">0", x, expected) {}
	virtual bool test_fn(QType x) override {
		return x > T(0);
	}
};

template<class T, unsigned N1, unsigned N2 = N1, class IType = T>
class TestLT : public TestCaseDuo<T, N1, N2, IType, bool> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType2;
	TestLT(IType x, IType y, bool expected) : TestCaseDuo<T, N1, N2, IType, bool>("<", x, y, expected) {}
	virtual bool test_fn(QType1 x, QType2 y) override {
		return x < y;
	}
};

template<class T, unsigned N1, unsigned N2 = N1, class IType = T>
class TestLE : public TestCaseDuo<T, N1, N2, IType, bool> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType2;
	TestLE(IType x, IType y, bool expected) : TestCaseDuo<T, N1, N2, IType, bool>("<=", x, y, expected) {}
	virtual bool test_fn(QType1 x, QType2 y) override {
		return x <= y;
	}
};

template<class T, unsigned N1, unsigned N2 = N1, class IType = T>
class TestGT : public TestCaseDuo<T, N1, N2, IType, bool> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType2;
	TestGT(IType x, IType y, bool expected) : TestCaseDuo<T, N1, N2, IType, bool>(">", x, y, expected) {}
	virtual bool test_fn(QType1 x, QType2 y) override {
		return x > y;
	}
};

template<class T, unsigned N1, unsigned N2 = N1, class IType = T>
class TestGE : public TestCaseDuo<T, N1, N2, IType, bool> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType2;
	TestGE(IType x, IType y, bool expected) : TestCaseDuo<T, N1, N2, IType, bool>(">=", x, y, expected) {}
	virtual bool test_fn(QType1 x, QType2 y) override {
		return x >= y;
	}
};

template<class T, unsigned N1, unsigned N2 = N1, class IType = T>
class TestEQ : public TestCaseDuo<T, N1, N2, IType, bool> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType2;
	TestEQ(IType x, IType y, bool expected) : TestCaseDuo<T, N1, N2, IType, bool>("==", x, y, expected) {}
	virtual bool test_fn(QType1 x, QType2 y) override {
		return x == y;
	}
};

template<class T, unsigned N1, unsigned N2 = N1, class IType = T>
class TestNEQ : public TestCaseDuo<T, N1, N2, IType, bool> {
public:
	using QType1 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType1;
	using QType2 = typename TestCaseDuo<T, N1, N2, IType, bool>::QType2;
	TestNEQ(IType x, IType y, bool expected) : TestCaseDuo<T, N1, N2, IType, bool>("!=", x, y, expected) {}
	virtual bool test_fn(QType1 x, QType2 y) override {
		return x != y;
	}
};

                                                                                                                                                                                   
template<class T, unsigned N, bool bSigned = std::is_signed<T>::value>
class TestConverts
{
public:
	static void do_test() {
		TestConvert<T, N        >( 1,    1).do_test();
		TestConvert<T, N, float >( 1.0,  1.0).do_test();
		TestConvert<T, N, double>( 1.0,  1.0).do_test();
	}
};

template<class T, unsigned N>
class TestConverts<T, N, true>
{
public:
	static void do_test() {
		TestConverts<T, N, false>::do_test();
		TestConvert<T, N        >(-1,    -1).do_test();
		TestConvert<T, N, float >(-1.0,  -1.0).do_test();
		TestConvert<T, N, double>(-1.0,  -1.0).do_test();
	}
};

template<class T, unsigned N, bool bSigned = std::is_signed<T>::value>
class TestCompares
{
public:
	static void do_test() {
		TestLZ<T, N        >(1, false ).do_test();
		TestGZ<T, N        >(1, true ).do_test();
		TestLT<T, N        >(1, 1, false).do_test();
		TestLT<T, N        >(1, 0, false).do_test();
		TestLT<T, N        >(1, 2, true ).do_test();
		TestLE<T, N        >(1, 1, true).do_test();
		TestLE<T, N        >(1, 0, false).do_test();
		TestLE<T, N        >(1, 2, true ).do_test();
		TestGT<T, N        >(1, 1, false).do_test();
		TestGT<T, N        >(1, 0, true).do_test();
		TestGT<T, N        >(1, 2, false).do_test();
		TestGE<T, N        >(1, 1, true ).do_test();
		TestGE<T, N        >(1, 0, true ).do_test();
		TestGE<T, N        >(1, 2, false).do_test();
		TestEQ<T, N        >(1, 1, true).do_test();
		TestEQ<T, N        >(1, 0, false).do_test();
		TestEQ<T, N        >(1, 2, false).do_test();
		TestNEQ<T, N       >(1, 1, false).do_test();
		TestNEQ<T, N       >(1, 0, true).do_test();
		TestNEQ<T, N       >(1, 2, true).do_test();
	}
};

template<class T, unsigned N>
class TestCompares<T, N, true>
{
public:
	static void do_test() {
		TestCompares<T, N, false>::do_test();
		TestLZ<T, N        >(-1, true ).do_test();
		TestGZ<T, N        >(-1, false ).do_test();
		TestLT<T, N        >(-1, -1,  false).do_test();
		TestLT<T, N        >(-1,  0,  true ).do_test();
		TestLT<T, N        >(-1, -2,  false).do_test();
		TestLT<T, N        >(-1,  1,  true ).do_test();
		TestLT<T, N        >( 1, -1,  false).do_test();
		TestLE<T, N        >(-1, -1,  true ).do_test();
		TestLE<T, N        >(-1,  0,  true ).do_test();
		TestLE<T, N        >(-1, -2,  false).do_test();
		TestLE<T, N        >(-1,  1,  true ).do_test();
		TestLE<T, N        >( 1, -1,  false ).do_test();
		TestGT<T, N        >(-1, -1,  false).do_test();
		TestGT<T, N        >(-1,  0,  false).do_test();
		TestGT<T, N        >(-1, -2,  true).do_test();
		TestGT<T, N        >(-1,  1,  false).do_test();
		TestGT<T, N        >( 1, -1,  true ).do_test();
		TestGE<T, N        >(-1, -1,  true).do_test();
		TestGE<T, N        >(-1,  0,  false).do_test();
		TestGE<T, N        >(-1, -2,  true).do_test();
		TestGE<T, N        >(-1,  1,  false).do_test();
		TestGE<T, N        >( 1, -1,  true ).do_test();
		TestEQ<T, N        >(-1, -1,  true).do_test();
		TestEQ<T, N        >(-1,  0,  false).do_test();
		TestEQ<T, N        >(-1, -2,  false).do_test();
		TestEQ<T, N        >(-1,  1,  false).do_test();
		TestEQ<T, N        >( 1, -1,  false).do_test();
		TestNEQ<T, N       >(-1, -1,  false).do_test();
		TestNEQ<T, N       >(-1,  0,  true).do_test();
		TestNEQ<T, N       >(-1, -2,  true).do_test();
		TestNEQ<T, N       >(-1,  1,  true).do_test();
		TestNEQ<T, N       >( 1, -1,  true).do_test();
	}
};

template<class T, unsigned N, bool bSigned = std::is_signed<T>::value>
class TestSimpleMath
{
public:
	static void do_test() {
		TestAdd<T, N>( 1,  2,  3).do_test();
		TestSub<T, N>( 3,  2,  1).do_test();
		TestMul<T, N>( 2,  3,  6).do_test();
		TestDiv<T, N>( 6,  3,  2).do_test();
	}
};

template<class T, unsigned N>
class TestSimpleMath<T, N, true>
{
public:
	static void do_test() {
		TestSimpleMath<T, N, false>::do_test();
		TestAdd<T, N>(-1, -2, -3).do_test();
		TestAdd<T, N>( 1, -2, -1).do_test();
		TestAdd<T, N>(-1,  2,  1).do_test();
		TestSub<T, N>(-3, -2, -1).do_test();
		TestSub<T, N>( 1,  3, -2).do_test();
		TestSub<T, N>(-1, -3,  2).do_test();
		TestMul<T, N>(-2, -3,  6).do_test();
		TestMul<T, N>( 2, -3, -6).do_test();
		TestMul<T, N>(-2,  3, -6).do_test();
		TestDiv<T, N>(-6, -3,  2).do_test();
		TestDiv<T, N>( 6, -3, -2).do_test();
		TestDiv<T, N>(-6,  3, -2).do_test();
	}
};

template<class T, unsigned N, bool bSigned = std::is_signed<T>::value>
class TestFractionalMath
{
public:
	static void do_test() {
		constexpr float df = 1.0 / (T(1) << N);
		TestConvert<T, N>(0.5, 0.5).do_test();
		TestConvert<T, N>(df, df).do_test();
		TestConvert<T, N>(1.0 - df, 1.0 - df).do_test();
		TestConvert<T, N>(1.0 + df, 1.0 + df).do_test();
	}
};

template<class T, unsigned N>
class TestFractionalMath<T, N, true>
{
public:
	static void do_test() {
		TestFractionalMath<T,N,false>::do_test();
		constexpr float df = 1.0 / (T(1) << N);
		TestConvert<T, N>(-0.5, -0.5).do_test();
		TestConvert<T, N>(-df, -df).do_test();
		TestConvert<T, N>(-1.0 + df, -1.0 + df).do_test();
		TestConvert<T, N>(-1.0 - df, -1.0 - df).do_test();
	}
};

template<class T, unsigned N, bool bSigned = std::is_signed<T>::value>
class TestRounds
{
public:
	static void do_test() {
		constexpr float df = 1.0 / (T(1) << N);
		TestConvert<T, N, float>( 1.0 + df,      1.0 + df).do_test();
		TestConvert<T, N, float>( 1.0 + df*0.4,  1.0     ).do_test();
		TestConvert<T, N, float>( 1.0 + df*0.6,  1.0 + df).do_test();
		TestConvert<T, N, float>( 3.0 - df,      3.0 - df).do_test();
		TestConvert<T, N, float>( 3.0 + df*0.4,  3.0     ).do_test();
		TestConvert<T, N, float>( 3.0 + df*0.6,  3.0 + df).do_test();
	}
};

template<class T, unsigned N>
class TestRounds<T, N, true>
{
public:
	static void do_test() {
		TestRounds<T, N, false>::do_test();
		constexpr float df = 1.0 / (T(1) << N);
		TestConvert<T, N, float>( -1.0 - df,      -1.0 - df).do_test();
		TestConvert<T, N, float>( -1.0 - df*0.4,  -1.0     ).do_test();
		TestConvert<T, N, float>( -1.0 - df*0.6,  -1.0 - df).do_test();
		TestConvert<T, N, float>( -3.0 + df,      -3.0 + df).do_test();
		TestConvert<T, N, float>( -3.0 - df*0.4,  -3.0     ).do_test();
		TestConvert<T, N, float>( -3.0 - df*0.6,  -3.0 - df).do_test();
	}
};

template<class T, unsigned N, bool bSigned = std::is_signed<T>::value>
class TestAdvancedMath
{
public:
	static void do_test() {
		constexpr float df = 1.0 / (T(1) << N);
		TestMul<T, N,N, float>(df,  df,   0.0      ).do_test();
		TestMul<T, N,N, float>(7.0, df,   7.0 * df ).do_test();
		TestMul<T, N,N, float>(df,  7.0,  7.0 * df ).do_test();
		TestDiv<T, N,N, float>(6.0*df,   6.0,    df                ).do_test();
		TestDiv<T, N,N, float>(6.0,      5.0,    roundf(1.2/df)*df ).do_test();
	}
};

template<class T, unsigned N>
class TestAdvancedMath<T,N,true>
{
public:
	static void do_test() {
		constexpr float df = 1.0 / (T(1) << N);
		TestAdvancedMath<T,N,false>::do_test();
		TestMul<T, N,N, float>( df,  -df,   0.0      ).do_test();
		TestMul<T, N,N, float>(-df,   df,   0.0      ).do_test();
		TestMul<T, N,N, float>(-df,  -df,   0.0      ).do_test();
		TestMul<T, N,N, float>( 7.0, -df,  -7.0 * df ).do_test();
		TestMul<T, N,N, float>(-7.0,  df,  -7.0 * df ).do_test();
		TestMul<T, N,N, float>(-7.0, -df,   7.0 * df ).do_test();
		TestMul<T, N,N, float>( df,  -7.0, -7.0 * df ).do_test();
		TestMul<T, N,N, float>(-df,   7.0, -7.0 * df ).do_test();
		TestMul<T, N,N, float>(-df,  -7.0,  7.0 * df ).do_test();
		TestDiv<T, N,N, float>( 6.0, -5.0, -roundf(1.2/df)*df).do_test();
		TestDiv<T, N,N, float>(-6.0,  5.0, -roundf(1.2/df)*df).do_test();
		TestDiv<T, N,N, float>(-6.0, -5.0,  roundf(1.2/df)*df).do_test();
	}
};

template<class T, unsigned N1, unsigned N2, bool bSigned = std::is_signed<T>::value>
class TestMixedMath
{
public:
	static void do_test() {
		TestAdd<T, N1,N2, float>( 1.0,  2.0,  3.0).do_test();
		TestAdd<T, N1,N2, float>( 1.5,  2.5,  4.0).do_test();
		TestSub<T, N1,N2, float>( 3.0,  2.0,  1.0).do_test();
		TestSub<T, N1,N2, float>( 4.0,  2.5,  1.5).do_test();
	}
};

template<class T, unsigned N1, unsigned N2>
class TestMixedMath<T,N1,N2,true>
{
public:
	static void do_test() {
		TestMixedMath<T,N1,N2,false>::do_test();
		TestAdd<T, N1,N2, float>( -1.0,  -2.0,  -3.0).do_test();
		TestAdd<T, N1,N2, float>( -1.5,  -2.5,  -4.0).do_test();
		TestSub<T, N1,N2, float>( -3.0,  -2.0,  -1.0).do_test();
		TestSub<T, N1,N2, float>( -4.0,  -2.5,  -1.5).do_test();
	}
};

template<class T, unsigned N>
class TestSet
{
public:
	static void do_test() {
		TestConverts<T,N>::do_test();
		TestCompares<T,N>::do_test();
		TestSimpleMath<T, N>::do_test();
		TestFractionalMath<T, N>::do_test();
		TestRounds<T,N>::do_test();
		TestAdvancedMath<T,N>::do_test();
	}
};

bool test_all()
{
	TestSet<unsigned, 1>::do_test();
	TestSet<int, 1>::do_test();

	TestSet<uint8_t, 1>::do_test();
	TestSet<int8_t, 1>::do_test();
	TestSet<uint16_t, 1>::do_test();
	TestSet<int16_t, 1>::do_test();
	TestSet<uint32_t, 1>::do_test();
	TestSet<int32_t, 1>::do_test();
	TestSet<uint64_t, 1>::do_test();
	TestSet<int64_t, 1>::do_test();

	TestSet<uint8_t, 2>::do_test();
	TestSet<int8_t, 2>::do_test();
	TestSet<uint16_t, 2>::do_test();
	TestSet<int16_t, 2>::do_test();
	TestSet<uint32_t, 2>::do_test();
	TestSet<int32_t, 2>::do_test();
	TestSet<uint64_t, 2>::do_test();
	TestSet<int64_t, 2>::do_test();

	TestSet<uint8_t, 5>::do_test();
	TestSet<int8_t, 4>::do_test();
	TestSet<uint16_t, 13>::do_test();
	TestSet<int16_t, 12>::do_test();
	TestSet<uint32_t, 29>::do_test();
	TestSet<int32_t, 28>::do_test();
	TestSet<uint64_t, 61>::do_test();
	TestSet<int64_t, 60>::do_test();

	TestMixedMath<uint8_t, 1,2>::do_test();
	TestMixedMath<int8_t, 1,2>::do_test();

	std::cout << std::dec << "Total: " << nCases;                                                                                                                                  
	if (nErrors) std::cout << ". Errors: " << nErrors;                                                                                                                             
	std::cout << std::endl;                                                                                                                                                        
                                                                                                                                                                                   
	return nErrors == 0;                                                                                                                                                           
}                                                                                                                                                                                  
                                                                                                                                                                                   
int main()                                                                                                                                                                         
{                                                                                                                                                                                  
	return test_all() ? 0 : 1;                                                                                                                                                     
}                                                                                                                                                                                  
                                                                                                                                                                                   
                                                                                                                                                                                   
