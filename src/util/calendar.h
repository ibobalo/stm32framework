#ifndef CALENDAR_H_INCLUDED
#define CALENDAR_H_INCLUDED

#include <stdint.h>
#include "macros.h"

constexpr uint8_t DAYS_PER_MONTH[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
#define DPM(M) DAYS_PER_MONTH[M]
#define SDPM(M) SUM(REPEAT_COMMA(M, DPM))
constexpr uint16_t DAYS_TILL_1ST_OF_MONTH[12] = {0, SDPM(1), SDPM(2), SDPM(3), SDPM(4), SDPM(5), SDPM(6), SDPM(7), SDPM(8), SDPM(9), SDPM(10), SDPM(11)};

inline constexpr unsigned ToSeconds(unsigned hours, unsigned minutes, unsigned seconds)
{
	return hours * 3600 + minutes * 60 + seconds;
}

inline constexpr unsigned ToMilliSeconds(unsigned hours, unsigned minutes, unsigned seconds, unsigned milli = 0)
{
	return ToSeconds(hours, minutes, seconds) * 1000 + milli;
}

inline constexpr unsigned IsLeapYear(unsigned Y)
{
	return (
		(Y % 400) == 0 ? 1 : (
		(Y % 100) == 0 ? 0 : (
		(Y % 4) == 0 ? 1 : 0)));
}

inline constexpr unsigned ToDaysSince1970(unsigned Y, uint8_t M, uint8_t D)
{
	return (
	(ASSERTE(Y >= 1970)),
	(ASSERTE(M >= 1 && M <= 12)),
	(ASSERTE(D >= 1 && D <= 31)),
	((Y - 1970) * 365 // days per year
		+ (Y - 1969) / 4
		- (Y - 1901) / 100
		+ (Y - 1601) / 400
		+ DAYS_TILL_1ST_OF_MONTH[M - 1] + ((M > 2 && IsLeapYear(Y)) ? 1 : 0)
		+ D - 1)
	);
}

inline constexpr long ToUnixTimestamp(unsigned uiDaysSince1970, unsigned uiSecondsSinceMidnight)
{
	return (uiDaysSince1970 * 86400 // seconds per day
			+ uiSecondsSinceMidnight);
}

inline constexpr long ToUnixTimestamp(unsigned Y, uint8_t M, uint8_t D, unsigned hours, unsigned minutes, unsigned seconds)
{
	return ToUnixTimestamp(ToDaysSince1970(Y, M, D), ToSeconds(hours, minutes, seconds));
}

static_assert(ToDaysSince1970(1970, 1, 1)  == 0, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1970, 1, 2)  == 1, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1970, 2, 1)  == 31, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1970, 2, 28) == 58, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1970, 3, 1)  == 59, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1970, 12, 31) == 364, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1971, 1, 1)  == 365, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1972, 1, 1)  == 730, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1972, 2, 29) == 789, "ToDaysSince1970 autotest failed");
static_assert(ToDaysSince1970(1972, 3, 1)  == 790, "ToDaysSince1970 autotest failed");
static_assert(ToUnixTimestamp(1970, 1, 1,   0,0,0) == 0, "ToUnixTimestamp autotest failed");
static_assert(ToUnixTimestamp(2001, 9, 9,   1,46,40) == 1000000000, "ToUnixTimestamp autotest failed");
static_assert(ToUnixTimestamp(2004, 1, 10, 13,37,04) == 0x40000000, "ToUnixTimestamp autotest failed");
static_assert(ToUnixTimestamp(2005, 3, 18,  1,58,31) == 1111111111, "ToUnixTimestamp autotest failed");
static_assert(ToUnixTimestamp(2021, 3, 21, 19,02,32) == 1616353352, "ToUnixTimestamp autotest failed");

#endif // CALENDAR_H_INCLUDED
