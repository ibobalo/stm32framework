#pragma once
#include <stdint.h>
#include <limits>
#include "../hw/mcu/mcu.h"

template<unsigned UniqId = 0>
class TPerformanceCounter
{
public:
	static inline void Reset() {
		m_nCount = 0;
	}
	static inline unsigned GetCount() {
		return m_nCount;
	}
	static inline void Inc() {
		m_nCount ++;;
	}
	static inline void Add(unsigned uiCounts) {
		m_nCount += uiCounts;
	}
protected:
	static volatile unsigned m_nCount;
};
template<unsigned UniqId>
volatile unsigned TPerformanceCounter<UniqId>::m_nCount = 0;



template<unsigned UniqId = 0, typename TValue = unsigned, typename TSum = uint64_t>
class TPerformanceStat :
	public TPerformanceCounter<UniqId>
{
public:
	static inline void Reset() {
		ENTER_CRITICAL_SECTION;
			TPerformanceCounter<UniqId>::Reset();
			m_tLast = 0;
			m_tMin = std::numeric_limits<TValue>::max();
			m_tMax = std::numeric_limits<TValue>::min();
			m_tSum = 0;
		LEAVE_CRITICAL_SECTION;
	}
	static inline TSum GetTotal() {
		return m_tSum;
	}
	static inline TValue GetLast() {
		return m_tLast;
	}
	static inline TValue GetMin() {
		return m_tMin;
	}
	static inline TValue GetMax() {
		return m_tMax;
	}
	static inline TValue GetAverage() {
		unsigned cnt;
		TSum sum;
		ENTER_CRITICAL_SECTION;
			cnt = TPerformanceCounter<UniqId>::GetCount();
			sum = m_tSum;
		LEAVE_CRITICAL_SECTION;
		return cnt ? sum / cnt : 0;
	}
	static inline void Append(TValue tValue) {
		ENTER_CRITICAL_SECTION;
			m_tLast = tValue;
			if (tValue < m_tMin) m_tMin = tValue;
			if (tValue > m_tMax) m_tMax = tValue;
			m_tSum += tValue;
			TPerformanceCounter<UniqId>::Inc();
		LEAVE_CRITICAL_SECTION;
	}
protected:
	static volatile TValue m_tLast;
	static volatile TValue m_tMin;
	static volatile TValue m_tMax;
	static volatile TSum m_tSum;
};
template<unsigned UniqId, typename TValue, typename TSum>
volatile TValue TPerformanceStat<UniqId, TValue, TSum>::m_tLast = 0;
template<unsigned UniqId, typename TValue, typename TSum>
volatile TValue TPerformanceStat<UniqId, TValue, TSum>::m_tMin = std::numeric_limits<TValue>::max();
template<unsigned UniqId, typename TValue, typename TSum>
volatile TValue TPerformanceStat<UniqId, TValue, TSum>::m_tMax = std::numeric_limits<TValue>::min();
template<unsigned UniqId, typename TValue, typename TSum>
volatile TSum TPerformanceStat<UniqId, TValue, TSum>::m_tSum = 0;



template<unsigned UniqId = 0>
class TPerformanceTimerBase :
	public TPerformanceStat<UniqId>
{
protected:
	static inline void Start(unsigned uiTicks) {m_uiStartTicks = uiTicks;}
	static inline void FinishDown(unsigned uiFinishTicks, unsigned uiPeriod) {
		// downcounter, usually start > finish
		if (m_uiStartTicks < uiFinishTicks) m_uiStartTicks += uiPeriod;
		unsigned uiTicks = m_uiStartTicks - uiFinishTicks;
		TPerformanceStat<UniqId>::Append(uiTicks);
	}
	static inline void FinishUp(unsigned uiFinishTicks, unsigned uiPeriod) {
		// upcounter, usually finish > start
		if (uiFinishTicks < m_uiStartTicks) uiFinishTicks += uiPeriod;
		unsigned uiTicks = uiFinishTicks - m_uiStartTicks;
		TPerformanceStat<UniqId>::Append(uiTicks);
	}
protected:
	static unsigned m_uiStartTicks;
};
template<unsigned UniqId>
unsigned TPerformanceTimerBase<UniqId>::m_uiStartTicks = 0;



template<unsigned UniqId = 0>
class TPerformanceTimerSysTick :
		public TPerformanceTimerBase<UniqId>
{
public:
	inline static void Start() {
		TPerformanceTimerBase<UniqId>::Start(SysTick->VAL);}
	inline void static Finish() {
		TPerformanceTimerBase<UniqId>::FinishDown(SysTick->VAL, SysTick->LOAD);}
};



namespace demo
{
	namespace PerfMon {
		using Demo1 = TPerformanceStat<0>;
		using DemoFn1 = TPerformanceTimerSysTick<1>;
		using DemoFn2 = TPerformanceTimerSysTick<2>;
	}

	inline void demo_fn1() {
		PerfMon::Demo1::Append(5);
		PerfMon::DemoFn1::Start();
		PerfMon::DemoFn1::Finish();
	}
	inline void test_fn2() {
		PerfMon::DemoFn2::Start();
		PerfMon::DemoFn2::Finish();
	}
}
