#ifndef BTREE_H_INCLUDED
#define BTREE_H_INCLUDED

#include "pair.h"

/*
 * usage:
 *
 * TBTreeItem<char, int> d1 = make_btreeitem('+', 1);
 * TBTreeItem<char, int> d2 = make_btreeitem('0', 0);
 * TBTreeItem<char, int> d3 = make_btreeitem('-', -1);
 * d2.Append(d1); d2.append(d3);
 * x = d2.Find('+', 0);
 */

template<class T>
struct Less
{
   static bool IsLess(const T& lhs, const T& rhs) { return lhs < rhs;}
};
template<class Tid, class Tdata>
struct Less< pair<Tid,Tdata>>
{
	template<class T2>
	static bool IsLess(const pair<Tid,Tdata>& lhs, const pair<Tid,T2>& rhs) { return lhs.first < rhs.first;}
};


template<class Tkey, class Tdata = void*, class Compare = Less<Tkey> >
struct TBTreeItem
{
public:
	typedef Tkey keyType;
	typedef Tdata valueType;
	typedef TBTreeItem<Tkey, Tdata, Compare> thisType;
public:
	bool Has(Tkey tKey) const {return FindItem(tKey);}
	const valueType& Get() const {return m_tValue;}
	valueType& Get() {return m_tValue;}
	const valueType& GetDefault(const keyType& tKey, const valueType& tDefault) const { const thisType* p = FindItem(tKey); return p ? p->Get() : tDefault; }
public:
	void AppendItem(thisType* pItem) {if (Compare::IsLess(pItem->m_tKey, m_tKey)) AppendLess(pItem); else AppendGreater(pItem);}
	const thisType* FindItem(Tkey tKey) const {return Compare::IsLess(tKey, m_tKey) ? FindLess(tKey) : (Compare::IsLess(m_tKey, tKey) ? FindGreater(tKey) : this);}
	const thisType* RootItem() const {const thisType* p = this; while (p->m_pTopItem) p = p->m_pTopItem; return p;}
	const thisType* FirstItem() const {const thisType* p = this; while (p->m_pLessItem) p = p->m_pLessItem; return p;}
	const thisType* NextItem() const {
		if (m_pGreaterItem) return m_pGreaterItem->FirstItem();
			const thisType* p = this;
			while (true) {
				if (!p->m_pTopItem)
					return (thisType*)(0);
				if (p == p->m_pTopItem->m_pLessItem)
					return p->m_pTopItem;
				ASSERTE(p == p->m_pTopItem->m_pGreaterItem);
				p = m_pTopItem;
			}
			return p;
	}
private:
	void AppendLess(thisType* pItem) { if (!m_pLessItem) {m_pLessItem = pItem; pItem->m_pTopItem = this;} else m_pLessItem->Append(pItem); }
	void AppendGreater(thisType* pItem) { if (!m_pGreaterItem) {m_pGreaterItem = pItem; pItem->m_pTopItem = this;} else m_pGreaterItem->Append(pItem); }
	const thisType* FindLess(Tkey tKey) const { return m_pLessItem ? m_pLessItem->FindItem(tKey) : (thisType*)(0); }
	const thisType* FindGreater(Tkey tKey) const { return m_pGreaterItem ? m_pGreaterItem->FindItem(tKey) : (thisType*)(0); }
public: /* must be public to allow  {} initializer */
	keyType         m_tKey;
	valueType       m_tValue;
	const thisType* m_pLessItem;
	const thisType* m_pTopItem;
	const thisType* m_pGreaterItem;
};

#endif // MAP_H_INCLUDED
