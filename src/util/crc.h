#ifndef CRC_H_INCLUDED
#define CRC_H_INCLUDED

#include <stdint.h>
#include "bitops.h"
#include "macros.h"

#ifdef __cplusplus

template<typename T, unsigned BitsLength, T Polynom, bool Reflect>
class CCRCPolynomial
{
public:
	using type = T;
	using TB = TBitsType<T>;
	enum {
		BITS_USED = BitsLength,
		BITS_UNUSED = TB::BITS_COUNT - BitsLength,
	};
	static const T poly = Polynom;

	template<bool UseTable>
	static inline constexpr T Process(uint8_t d) { return (UseTable ? m_auiBytesTable[d] : AccumulateByte(d)); }

//protected:
	static inline constexpr T AccumulateBit(T crc);
	static inline constexpr T AccumulateNibble(uint8_t d);
	static inline constexpr T AccumulateByte(uint8_t d);
#define CI(N) AccumulateByte(0x##N),
#define RI(N) MAP(CI,N##0,N##1,N##2,N##3,N##4,N##5,N##6,N##7,N##8,N##9,N##A,N##B,N##C,N##D,N##E,N##F)
	static constexpr T m_auiBytesTable[256] = {RI(0) RI(1) RI(2) RI(3) RI(4) RI(5) RI(6) RI(7) RI(8) RI(9) RI(A) RI(B) RI(C) RI(D) RI(E) RI(F)};
#define NI(N) AccumulateNibble(0x##N),
	static constexpr T m_auiNibblesTable[16] = {NI(0) NI(1) NI(2) NI(3) NI(4) NI(5) NI(6) NI(7) NI(8) NI(9) NI(A) NI(B) NI(C) NI(D) NI(E) NI(F)};

	static_assert(BitsLength <= TB::BITS_COUNT, "Invalid BitsLength");
};

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
class CCRC :
		public CCRCPolynomial<T, BitsLength, Polynom, ReflectIn>
{
public:
	using TP = CCRCPolynomial<T, BitsLength, Polynom, ReflectIn>;
	using typename TP::TB;
	static const T init = Initial;
	static const bool refin = ReflectIn;
	static const bool refout = ReflectOut;
	static const T xorout = XorOut;
	static const T check = Check;

	static T Calculate(const void* data, unsigned len);
	static T CalculateFast(const void* data, unsigned len);

	static constexpr T Init();
	template<bool UseTable>
	static constexpr T Accumulate(T crc, uint8_t data);
	template<bool UseTable>
	static T Accumulate(T crc, const void* buf, unsigned len);
	static constexpr T Finalize(T crc);
private:
	template<bool UseTable>
	static constexpr bool Test();

	static_assert(Test<false>(), "CRC self test");
	static_assert(Test<true>(), "CRC fast self test");
};

template <typename T, unsigned BitsLength, T Polynom, bool Reflect>
constexpr T CCRCPolynomial<T, BitsLength, Polynom, Reflect>::AccumulateBit(T crc)
{
	return (Reflect
			? (TB::GetBit(crc, 0)
					? (crc >> 1) ^ TB::Reflect(Polynom, BitsLength)
					: (crc >> 1)
			  )
			: ((TB::GetBit(crc, BitsLength > 8 ? BitsLength - 1 : 7))
					? (crc << 1) ^ (Polynom << (BitsLength < 8 ? BITS_UNUSED : 0))
					: (crc << 1)
			  )
	);
}

template <typename T, unsigned BitsLength, T Polynom, bool Reflect>
constexpr T CCRCPolynomial<T, BitsLength, Polynom, Reflect>::AccumulateNibble(uint8_t d)
{
	return (
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
					Reflect
					? (T)d
					: (BitsLength > 8
					 ?(T(d) << (BitsLength - 8))
					 :(T(d))
					)
			))))
	);
}
static_assert(CCRCPolynomial<uint32_t, 32, 0x04C11DB7, false>::AccumulateNibble(0) == 0x00000000, "Nibble table self test");
//static_assert(CCRCPolynomial<uint32_t, 32, 0x04C11DB7, false>::AccumulateNibble(1) == 0x04C11DB7, "Nibble table self test");
template <typename T, unsigned BitsLength, T Polynom, bool Reflect>
constexpr T CCRCPolynomial<T, BitsLength, Polynom, Reflect>::AccumulateByte(uint8_t d)
{
	return (
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
			AccumulateBit(
					Reflect
					? (T)d
					: (BitsLength > 8
					 ?(T(d) << (BitsLength - 8))
					 :(T(d))
					)
			))))))))
	);
}
template <typename T, unsigned BitsLength, T Polynom, bool Reflect>
constexpr T CCRCPolynomial<T, BitsLength, Polynom, Reflect>::m_auiBytesTable[256];
template <typename T, unsigned BitsLength, T Polynom, bool Reflect>
constexpr T CCRCPolynomial<T, BitsLength, Polynom, Reflect>::m_auiNibblesTable[16];

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
template<bool UseTable>
constexpr T CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::Accumulate(T crc, uint8_t data)
{
	return (ReflectIn
			? ((crc >> 8) ^ TP::template Process<UseTable>(crc ^ data))
			: ((crc << 8) ^ TP::template Process<UseTable>(uint8_t(crc >> (BitsLength > 8 ? BitsLength - 8 : 0)) ^ data))
	);
}

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
template<bool UseTable>
T CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::Accumulate(T crc, const void* buf, unsigned len)
{
	uint8_t* p = (uint8_t*)buf;
	while (len--) {
		crc = Accumulate<UseTable>(crc, *p++);
	}
	return crc;
}

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
constexpr T CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::Init()
{

	return ReflectIn ? TB::Reflect(Initial, BitsLength) : (Initial << ((BitsLength < 8) ? 8 - BitsLength : 0));
}

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
constexpr T CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::Finalize(T crc)
{
	return XorOut ^ TB::GetLoBits(
			((ReflectOut != ReflectIn)
				? TB::Reflect(crc, BitsLength)
				: crc
			) >> ((!ReflectIn && BitsLength < 8) ? 8 - BitsLength : 0), BitsLength);
}

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
T CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::Calculate(const void* buf, unsigned len)
{
	T crc = Init();
	crc = Accumulate<false>(crc, buf, len);
	return Finalize(crc);
}

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
T CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::CalculateFast(const void* buf, unsigned len)
{
	T crc = Init();
	crc = Accumulate<true>(crc, buf, len);
	return Finalize(crc);
}

template<typename T, unsigned BitsLength, T Polynom, T Initial, bool ReflectIn, bool ReflectOut, T XorOut, T Check>
template<bool UseTable>
constexpr bool CCRC<T, BitsLength, Polynom, Initial, ReflectIn, ReflectOut, XorOut, Check>::Test()
{
	return Check == Finalize(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
			Accumulate<UseTable>(
					Init(), '1'),'2'),'3'),'4'),'5'),'6'),'7'),'8'),'9')
	);
}

typedef CCRC<uint8_t, 3, 0x03, 0x00, false, false, 0x07, 0x04> CRC3_GSM;
typedef CCRC<uint8_t, 3, 0x03, 0x07, true,  true,  0x00, 0x06> CRC3_ROHC;
typedef CCRC<uint8_t, 4, 0x03, 0x0F, false, false, 0x0F, 0x0B> CRC4_INTERLAKEN;
typedef CCRC<uint8_t, 4, 0x03, 0x00, true,  true,  0x00, 0x07> CRC4_ITU, CRC4_G704;
typedef CCRC<uint8_t, 5, 0x09, 0x09, false, false, 0x00, 0x00> CRC5_EPC;
typedef CCRC<uint8_t, 5, 0x15, 0x00, true,  true,  0x00, 0x07> CRC5_ITU, CRC5_G704;
typedef CCRC<uint8_t, 5, 0x05, 0x1F, true,  true,  0x1F, 0x19> CRC5_USB;
typedef CCRC<uint8_t, 6, 0x27, 0x3F, false, false, 0x00, 0x0D> CRC6_CDMA2000A;
typedef CCRC<uint8_t, 6, 0x07, 0x3F, false, false, 0x00, 0x3B> CRC6_CDMA2000B;
typedef CCRC<uint8_t, 6, 0x19, 0x00, true,  true,  0x00, 0x26> CRC6_DARC;
typedef CCRC<uint8_t, 6, 0x03, 0x00, true,  true,  0x00, 0x06> CRC6_ITU, CRC6_G704;
typedef CCRC<uint8_t, 6, 0x2F, 0x00, false, false, 0x3F, 0x13> CRC6_GSM;
typedef CCRC<uint8_t, 7, 0x09, 0x00, false, false, 0x00, 0x75> CRC7, CRC7_MMC;
typedef CCRC<uint8_t, 7, 0x4F, 0x7F, true,  true,  0x00, 0x53> CRC7_ROHC;
typedef CCRC<uint8_t, 7, 0x45, 0x00, false, false, 0x00, 0x61> CRC7_UMTS;
typedef CCRC<uint8_t, 8, 0x07, 0x00, false, false, 0x00, 0xF4> CRC8, CRC8_SMBUS;
typedef CCRC<uint8_t, 8, 0x1D, 0xFF, true,  true,  0x00, 0x97> CRC8_AES, CRC8_EBU, CRC8_TECH3250;
typedef CCRC<uint8_t, 8, 0x2F, 0xFF, false, false, 0xFF, 0xDF> CRC8_AUTOSTAR;
typedef CCRC<uint8_t, 8, 0xA7, 0x00, true,  true,  0x00, 0x26> CRC8_BLUETOOTH;
typedef CCRC<uint8_t, 8, 0x9B, 0xFF, false, false, 0x00, 0xDA> CRC8_CDMA2000;
typedef CCRC<uint8_t, 8, 0x39, 0x00, true,  true,  0x00, 0x15> CRC8_DARC;
typedef CCRC<uint8_t, 8, 0xD5, 0x00, false, false, 0x00, 0xBC> CRC8_DVBS2;
typedef CCRC<uint8_t, 8, 0x1D, 0x00, false, false, 0x00, 0x37> CRC8_GSMA;
typedef CCRC<uint8_t, 8, 0x49, 0x00, false, false, 0xFF, 0x94> CRC8_GSMB;
typedef CCRC<uint8_t, 8, 0x07, 0x00, false, false, 0x55, 0xA1> CRC8_ITU;
typedef CCRC<uint8_t, 8, 0x1D, 0xFD, false, false, 0x00, 0x7E> CRC8_ICODE;
typedef CCRC<uint8_t, 8, 0x9B, 0x00, false, false, 0x00, 0xEA> CRC8_LTE;
typedef CCRC<uint8_t, 8, 0x31, 0x00, true,  true,  0x00, 0xA1> CRC8_MAXIM, CRC8_DALLAS;
typedef CCRC<uint8_t, 8, 0x1D, 0xC7, false, false, 0x00, 0x99> CRC8_MIRAFARE;
typedef CCRC<uint8_t, 8, 0x31, 0xFF, false, false, 0x00, 0xF7> CRC8_NRSC5;
typedef CCRC<uint8_t, 8, 0x2F, 0x00, false, false, 0x00, 0x3E> CRC8_OPENSAFETY;
typedef CCRC<uint8_t, 8, 0x07, 0xFF, true,  true,  0x00, 0xD0> CRC8_ROHC;
typedef CCRC<uint8_t, 8, 0x1D, 0xFF, false, false, 0xFF, 0x4B> CRC8_SAEJ1850;
typedef CCRC<uint8_t, 8, 0x9B, 0x00, true,  true,  0x00, 0x25> CRC8_WCDMA;
typedef CCRC<uint16_t, 10, 0x0233, 0x0000, false, false, 0x0000, 0x0199> CRC10, CRC10_ATM, CRC10_I610;
typedef CCRC<uint16_t, 10, 0x03D9, 0x03FF, false, false, 0x0000, 0x0233> CRC10_CDMA2000;
typedef CCRC<uint16_t, 10, 0x0175, 0x0000, false, false, 0x03FF, 0x012A> CRC10_GSM;
typedef CCRC<uint16_t, 11, 0x0385, 0x001A, false, false, 0x0000, 0x05A3> CRC11, CRC11_FLEXRAY;
typedef CCRC<uint16_t, 11, 0x0307, 0x0000, false, false, 0x0000, 0x0061> CRC11_UMTS;
typedef CCRC<uint16_t, 12, 0x080F, 0x0000, false, true,  0x0000, 0x0DAF> CRC12_3GPP, CRC12_UMTS;
typedef CCRC<uint16_t, 12, 0x0F13, 0x0FFF, false, false, 0x0000, 0x0D4D> CRC12_CDMA2000;
typedef CCRC<uint16_t, 12, 0x080F, 0x0000, false, false, 0x0000, 0x0F5B> CRC12_DECT;
typedef CCRC<uint16_t, 12, 0x0D31, 0x0000, false, false, 0x0FFF, 0x0B34> CRC12_GSM;
typedef CCRC<uint16_t, 13, 0x1CF5, 0x0000, false, false, 0x0000, 0x04FA> CRC13_BBC;
typedef CCRC<uint16_t, 14, 0x0805, 0x0000, true,  true,  0x0000, 0x082D> CRC14_DARC;
typedef CCRC<uint16_t, 14, 0x202D, 0x0000, false, false, 0x3FFF, 0x30AE> CRC14_GSM;
typedef CCRC<uint16_t, 15, 0x4599, 0x0000, false, false, 0x0000, 0x059E> CRC15_CAN;
typedef CCRC<uint16_t, 15, 0x6815, 0x0000, false, false, 0x0001, 0x2566> CRC15_MPT1327;
typedef CCRC<uint16_t, 16, 0x8005, 0x0000, true,  true,  0x0000, 0xBB3D> CRC16, CRC16_ARC, CRC16_LHA, CRC16_IBM;
typedef CCRC<uint16_t, 16, 0x1021, 0x1D0F, false, false, 0x0000, 0xE5CC> CRC16_AUGCCITT, CRC16_SPIFUJITSU;
typedef CCRC<uint16_t, 16, 0x8005, 0x0000, false, false, 0x0000, 0xFEE8> CRC16_BUYPASS, CRC16_UMTS, CRC16_VERIFONE;
typedef CCRC<uint16_t, 16, 0x1021, 0x0000, true,  true,  0x0000, 0x2189> CRC16_CCITT, CRC16_CCITTTRUE, CRC16_KERMIT, CRC16_V41LSB;
typedef CCRC<uint16_t, 16, 0x1021, 0xFFFF, false, false, 0x0000, 0x29B1> CRC16_CCITTFALSE, CRC16_IBM3740, CRC16_AUTOSAR;
typedef CCRC<uint16_t, 16, 0xC867, 0xFFFF, false, false, 0x0000, 0x4C06> CRC16_CDMA2000;
typedef CCRC<uint16_t, 16, 0x8005, 0xFFFF, false, false, 0x0000, 0xAEE7> CRC16_CMS;
typedef CCRC<uint16_t, 16, 0x8005, 0x800d, false, false, 0x0000, 0x9ECF> CRC16_DDS110;
typedef CCRC<uint16_t, 16, 0x0589, 0x0000, false, false, 0x0001, 0x007E> CRC16_DECTR, RCRC16;
typedef CCRC<uint16_t, 16, 0x0589, 0x0000, false, false, 0x0000, 0x007F> CRC16_DECTX, XCRC16;
typedef CCRC<uint16_t, 16, 0x3D65, 0x0000, true,  true,  0xFFFF, 0xEA82> CRC16_DNP;
typedef CCRC<uint16_t, 16, 0x3D65, 0x0000, false, false, 0xFFFF, 0xC2B7> CRC16_EN13757;
typedef CCRC<uint16_t, 16, 0x1021, 0xFFFF, false, false, 0xFFFF, 0xD64E> CRC16_GENIBUS, CRC16_DARC, CRC16_EPC, CRC16_ICODE;
typedef CCRC<uint16_t, 16, 0x1021, 0x0000, false, false, 0xFFFF, 0xCE3C> CRC16_GSM;
typedef CCRC<uint16_t, 16, 0x1021, 0xFFFF, true,  true,  0xFFFF, 0x906E> CRC16_IBMSDLC, CRC16_ISOHDLC, CRC16_B, CRC16_X25, CRC16_B;
typedef CCRC<uint16_t, 16, 0x1021, 0xC6C6, true,  true,  0x0000, 0xBF05> CRC16_ISOIEC14443, CRC16_A;
typedef CCRC<uint16_t, 16, 0x6F63, 0x0000, false, false, 0x0000, 0xBDF4> CRC16_LJ1200;
typedef CCRC<uint16_t, 16, 0x8005, 0x0000, true,  true,  0xFFFF, 0x44C2> CRC16_MAXIM;
typedef CCRC<uint16_t, 16, 0x1021, 0xFFFF, true,  true,  0x0000, 0x6F91> CRC16_MCRF4XX;
typedef CCRC<uint16_t, 16, 0x8005, 0xFFFF, true,  true,  0x0000, 0x4B37> CRC16_MODBUS;
typedef CCRC<uint16_t, 16, 0x080B, 0xFFFF, true,  true,  0x0000, 0xA066> CRC16_NRSC5;
typedef CCRC<uint16_t, 16, 0x5935, 0x0000, false, false, 0x0000, 0x5D38> CRC16_OPENSAFETYA;
typedef CCRC<uint16_t, 16, 0x755B, 0x0000, false, false, 0x0000, 0x20FE> CRC16_OPENSAFETYB;
typedef CCRC<uint16_t, 16, 0x1DCF, 0xFFFF, false, false, 0xFFFF, 0xA819> CRC16_PROFIBUS, CRC16_IEC61158;
typedef CCRC<uint16_t, 16, 0x1021, 0xB2AA, true,  true,  0x0000, 0x63D0> CRC16_RIELLO;
typedef CCRC<uint16_t, 16, 0x8BB7, 0x0000, false, false, 0x0000, 0xD0DB> CRC16_T10DIF;
typedef CCRC<uint16_t, 16, 0xA097, 0x0000, false, false, 0x0000, 0x0FB3> CRC16_TELEDISK;
typedef CCRC<uint16_t, 16, 0x1021, 0x89EC, true,  true,  0x0000, 0x26B1> CRC16_TMS37157;
typedef CCRC<uint16_t, 16, 0x8005, 0xFFFF, true,  true,  0xFFFF, 0xB4C8> CRC16_USB;
typedef CCRC<uint16_t, 16, 0x1021, 0x0000, false, false, 0x0000, 0x31C3> CRC16_XMODEM, CRC16_ZMODEM, CRC16_ACORN, CRC16_LTE;
typedef CCRC<uint32_t, 17, 0x1685B, 0x00000, false, false, 0x00000, 0x04F03> CRC17_CANFD;
typedef CCRC<uint32_t, 21, 0x102899, 0x000000, false, false, 0x000000, 0x0ED841> CRC21_CANFD;
typedef CCRC<uint32_t, 24, 0x864CFB, 0xB704CE, false,  false,  0x000000, 0x21CF02> CRC24, CRC24_OPENPGP;
typedef CCRC<uint32_t, 24, 0x00065B, 0x555555, true,   true,   0x000000, 0xC25A56> CRC24_BLE;
typedef CCRC<uint32_t, 24, 0x5D6DCB, 0xFEDCBA, false,  false,  0x000000, 0x7979BD> CRC24_FLEXRAYA;
typedef CCRC<uint32_t, 24, 0x5D6DCB, 0xABCDEF, false,  false,  0x000000, 0x1F23B8> CRC24_FLEXRAYB;
typedef CCRC<uint32_t, 24, 0x328B63, 0xFFFFFF, false,  false,  0xFFFFFF, 0xB4F3E6> CRC24_INTERLAKEN;
typedef CCRC<uint32_t, 24, 0x864CFB, 0x000000, false,  false,  0x000000, 0xCDE703> CRC24_LTEA;
typedef CCRC<uint32_t, 24, 0x800063, 0x000000, false,  false,  0x000000, 0x23EF52> CRC24_LTEB;
typedef CCRC<uint32_t, 24, 0x800063, 0xFFFFFF, false,  false,  0xFFFFFF, 0x200FA5> CRC24_OS9;
typedef CCRC<uint32_t, 30, 0x2030B9C7, 0x3FFFFFFF, false, false, 0x3FFFFFFF, 0x04C34ABF> CRC30_CDMA;
typedef CCRC<uint32_t, 31, 0x04C11DB7, 0x7FFFFFFF, false, false, 0x7FFFFFFF, 0x0CE9E46C> CRC31_PHILIPS;
typedef CCRC<uint32_t, 32, 0x04C11DB7, 0xFFFFFFFF, true,  true,  0xFFFFFFFF, 0xCBF43926> CRC32, CRC32_ADCCP, CRC32_ISOHDLC, CRC32_V42, CRC32_XZ, CRC32_PKZIP;
typedef CCRC<uint32_t, 32, 0x814141AB, 0x00000000, false, false, 0x00000000, 0x3010BF7F> CRC32_AIXM, CRC32_Q;
typedef CCRC<uint32_t, 32, 0xF4ACFB13, 0xFFFFFFFF, true,  true,  0xFFFFFFFF, 0x1697D06A> CRC32_AUTOSAR;
typedef CCRC<uint32_t, 32, 0xA833982B, 0xFFFFFFFF, true,  true,  0xFFFFFFFF, 0x87315576> CRC32_BASE91D, CRC32_D;
typedef CCRC<uint32_t, 32, 0x04C11DB7, 0xFFFFFFFF, false, false, 0xFFFFFFFF, 0xFC891918> CRC32_BZIP2, CRC32_AAL5, CRC32_DECTB, CRC32_B;
typedef CCRC<uint32_t, 32, 0x8001801B, 0x00000000, true,  true,  0x00000000, 0x6EC2EDC4> CRC32_CDROMEDC;
typedef CCRC<uint32_t, 32, 0x04C11DB7, 0x00000000, false, false, 0xFFFFFFFF, 0x765E7680> CRC32_CKSUM, CRC32_POSIX;
typedef CCRC<uint32_t, 32, 0x1EDC6F41, 0xFFFFFFFF, true,  true,  0xFFFFFFFF, 0xE3069283> CRC32_ISCSI, CRC32_BASE91C, CRC32_CASTAGNOLI, CRC32_INTERLAKEN, CRC32_C;
typedef CCRC<uint32_t, 32, 0x04C11DB7, 0xFFFFFFFF, true,  true,  0x00000000, 0x340BC6D9> CRC32_JAMCRC;
typedef CCRC<uint32_t, 32, 0x04C11DB7, 0xFFFFFFFF, false, false, 0x00000000, 0x0376E6E7> CRC32_MPEG2;
typedef CCRC<uint32_t, 32, 0x000000AF, 0x00000000, false, false, 0x00000000, 0xBD0BE338> CRC32_XFER;
typedef CCRC<uint64_t, 40, 0x0004820009, 0x0000000000, false, false, 0xFFFFFFFFFF, 0xD4164FC646> CRC40_GSM;
typedef CCRC<uint64_t, 64, 0x42F0E1EBA9EA3693, 0x0000000000000000, false, false, 0x0000000000000000, 0x6C40DF5F0B497347> CRC64, CRC64_ECMA182;
typedef CCRC<uint64_t, 64, 0x000000000000001B, 0xFFFFFFFFFFFFFFFF, true,  true,  0xFFFFFFFFFFFFFFFF, 0xB90956C775A41001> CRC64_GOISO;
typedef CCRC<uint64_t, 64, 0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, false, false, 0xFFFFFFFFFFFFFFFF, 0x62EC59E3F1A4F00A> CRC64_WE;
typedef CCRC<uint64_t, 64, 0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, true,  true,  0xFFFFFFFFFFFFFFFF, 0x995DC9BBDF1939FA> CRC64_XZ;
//typedef CCRC<uint32_t[3], 82, (0x0308C,0x01110114,0x01440411), (0x00000,0x00000000,0x00000000), true,  true,  (0x00000,0x00000000,0x00000000), (0x09EA8,0x3F625023,0x801FD612)> CRC82_DARC;

#endif //__cplusplus

#endif // CRC_H_INCLUDED
