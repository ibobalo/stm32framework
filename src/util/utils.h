#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <stddef.h>
#include "macros.h"

#ifdef __cplusplus
extern "C"
{
#endif

unsigned strnlen(const char *str, unsigned max_len);

unsigned NumberParserU(unsigned& uiResult, const char* pString, unsigned uiLength);
unsigned NumberParserI(int& iResult, const char* pString, unsigned uiLength);
unsigned NumberParserF(float& fResult, const char* pString, unsigned uiLength);

// attention: result may be not equal to pBuffer
const char* NumberConvertU(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertB(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertO(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertH(unsigned uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertI(int iValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertUU(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertBB(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertOO(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertHH(uint64_t uiValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertII(int64_t iValue, char* pBuffer, unsigned uiSize, unsigned* puiResultLength = NULL);
const char* NumberConvertF(float fValue, char* pBuffer, unsigned uiSize, unsigned nPrecision = 3, unsigned* puiResultLength = NULL);
const char* NumberConvertD(double dValue, char* pBuffer, unsigned uiSize, unsigned nPrecision = 3, unsigned* puiResultLength = NULL);

bool StringAlignL(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill = ' ');
bool StringAlignR(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill = ' ');
bool StringAlignC(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill = ' ');

#ifdef __cplusplus
} // extern "C";
#endif

#ifdef __cplusplus

template<class T, class T2>
inline T union_cast(T2 x) {
	static_assert(sizeof(T) == sizeof(T2), "size not match");
	union u_t {T x1; T2 x2; u_t(T2 x):x2(x){}} u(x);
	return u.x1;
}

typedef enum {AST_LEFT, AST_RIGHT, AST_CENTER} EAlignStringType;
template<EAlignStringType eAlignStringType>
bool StringAlign(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill = ' ');
template<>
inline bool StringAlign<AST_LEFT>(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill) {
	return StringAlignL(pSrcStr, uiSrcLength, pBuffer, uiSize, cFill);
}
template<>
inline bool StringAlign<AST_RIGHT>(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill) {
	return StringAlignR(pSrcStr, uiSrcLength, pBuffer, uiSize, cFill);
}
template<>
inline bool StringAlign<AST_CENTER>(const char* pSrcStr, unsigned uiSrcLength, char* pBuffer, unsigned uiSize, char cFill) {
	return StringAlignC(pSrcStr, uiSrcLength, pBuffer, uiSize, cFill);
}

template<unsigned nCapacity>
class FixedStringBuf
{
	STATIC_ASSERTE(nCapacity > 0, invalid_Capacity);
public:
	operator const char*() const {return m_pStart;}
	const char* GetValue() const {return m_pStart;};
	unsigned GetLength() const {return m_uiLenght;};
	unsigned GetCapacity() const {return nCapacity;};
protected:
	void Clear() { m_buf[nCapacity] = '\0'; m_uiLenght = 0; m_pStart = m_buf + nCapacity; }
protected:
	char          m_buf[nCapacity + 1];
	unsigned      m_uiLenght;
	const char*   m_pStart;
};

template<unsigned nCapacity=15, unsigned nPrecision=0>
class ToString :
		public FixedStringBuf<nCapacity>
{
private:
	typedef FixedStringBuf<nCapacity> base;
	using base::Clear;
	using base::m_buf;
	using base::m_uiLenght;
	using base::m_pStart;
public:
	ToString() {Clear();}
	ToString(float fValue) { m_pStart = NumberConvertF(fValue, m_buf, nCapacity + 1, nPrecision, &m_uiLenght); }
	ToString(double dValue) { m_pStart = NumberConvertD(dValue, m_buf, nCapacity + 1, nPrecision, &m_uiLenght); }
	const char* Convert(float fValue) { m_pStart = NumberConvertF(fValue, m_buf, nCapacity + 1, nPrecision, &m_uiLenght); return m_pStart;}
	const char* Convert(double dValue) { m_pStart = NumberConvertD(dValue, m_buf, nCapacity + 1, nPrecision, &m_uiLenght); return m_pStart;}
};

template<unsigned nCapacity>
class ToString<nCapacity, 0>:
	public FixedStringBuf<nCapacity>
{
private:
	typedef FixedStringBuf<nCapacity> base;
	using base::Clear;
	using base::m_buf;
	using base::m_uiLenght;
	using base::m_pStart;
public:
	typedef enum {BASE_BIN, BASE_OCT, BASE_HEX} EBase;
public:
	ToString() {Clear();}
	ToString(char cValue) { m_buf[nCapacity] = '\0'; m_buf[nCapacity - 1] = cValue; m_pStart = m_buf + nCapacity - 1; m_uiLenght = 1; }
	ToString(uint8_t uiValue) { m_pStart = NumberConvertU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(uint16_t uiValue) { m_pStart = NumberConvertU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(unsigned uiValue) { m_pStart = NumberConvertU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(uint64_t uiValue) { m_pStart = NumberConvertUU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(uint8_t uiValue, EBase eBase) { m_pStart = ((eBase==BASE_BIN)?NumberConvertB:(eBase==BASE_OCT)?NumberConvertO:NumberConvertH)(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(uint16_t uiValue, EBase eBase) { m_pStart = ((eBase==BASE_BIN)?NumberConvertB:(eBase==BASE_OCT)?NumberConvertO:NumberConvertH)(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(unsigned uiValue, EBase eBase) { m_pStart = ((eBase==BASE_BIN)?NumberConvertB:(eBase==BASE_OCT)?NumberConvertO:NumberConvertH)(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(uint64_t uiValue, EBase eBase) { m_pStart = ((eBase==BASE_BIN)?NumberConvertBB:(eBase==BASE_OCT)?NumberConvertOO:NumberConvertHH)(uiValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(int16_t iValue) { m_pStart = NumberConvertI(iValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(int iValue) { m_pStart = NumberConvertI(iValue, m_buf, nCapacity + 1, &m_uiLenght); }
	ToString(int64_t iValue) { m_pStart = NumberConvertII(iValue, m_buf, nCapacity + 1, &m_uiLenght); }

	const char* Convert(char cValue) { m_buf[nCapacity] = '\0'; m_buf[nCapacity - 1] = cValue; m_pStart = m_buf + nCapacity; m_uiLenght = 1; return m_pStart;}
	const char* Convert(uint8_t uiValue) { m_pStart = NumberConvertU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
	const char* Convert(uint16_t uiValue) { m_pStart = NumberConvertU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
	const char* Convert(unsigned uiValue) { m_pStart = NumberConvertU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
	const char* Convert(uint64_t uiValue) { m_pStart = NumberConvertUU(uiValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
	const char* Convert(int16_t iValue) { m_pStart = NumberConvertI(iValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
	const char* Convert(int iValue) { m_pStart = NumberConvertI(iValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
	const char* Convert(int64_t iValue) { m_pStart = NumberConvertII(iValue, m_buf, nCapacity + 1, &m_uiLenght); return m_pStart;}
};

template<int uiLen, EAlignStringType eAlignType = AST_LEFT>
class AlignString
{
	STATIC_ASSERTE(uiLen > 0, invalid_Len);
public:
	AlignString() {Align("", 0, ' ');};
	AlignString(const char* pSrcStr, char cFill = ' ') {
			Align(pSrcStr, strnlen(pSrcStr, uiLen + 1), cFill);}
	AlignString(const char* pSrcStr, unsigned uiSrcLength, char cFill = ' ') {
			Align(pSrcStr, uiSrcLength, cFill);}
	template<unsigned nCapacity>
	AlignString(const FixedStringBuf<nCapacity>& Src, char cFill = ' ') {
			Align(Src.GetValue(), Src.GetLength(), cFill);}

	bool Align(const char* pSrcStr, unsigned uiSrcLength, char cFill = ' ') {
		return StringAlign<eAlignType>(pSrcStr, uiSrcLength, m_buf, uiLen + 1, cFill);
	}
	template<unsigned nCapacity>
	bool Align(const FixedStringBuf<nCapacity>& Src, char cFill = ' ') {
		return StringAlign<eAlignType>(Src.GetValue(), Src.GetLength(), m_buf, uiLen + 1, cFill);
	}
	operator const char*() const {return m_buf;}
	const char* GetValue() const {return m_buf;}
	unsigned GetLength() const {return uiLen;}

private:
	char          m_buf[uiLen + 1];
};

template<class T> static inline void objcpy(T* dst, const T* src, unsigned n) __attribute__ ((always_inline));
template<class T> static inline void objcpy(T* dst, const T* src, unsigned n)
{
	const T *p = (const T*)src;
	T* q = dst;
	while (n--) { *q++ = *p++; }
}
template<class T> static inline void objset(T* dst, T c, unsigned n) __attribute__ ((always_inline));
template<class T> static inline void objset(T* dst, T c, unsigned n)
{
	T* q = dst;
	while (n--) { *q++ = c; }
}

template<class T> static inline int objcmp(const T* p, const T* q, unsigned n) __attribute__ ((always_inline));
template<class T> static inline int objcmp(const T* p, const T* q, unsigned n)
{
	while (n--) { if (*p++ == *q++) continue; return (*(p-1) < *(q-1)) ? -1 : 1; }
	return 0;
}

#endif //__cplusplus

#endif // UTILS_H_INCLUDED
