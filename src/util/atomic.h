#ifndef _ATOMIC_H_INCLUDED
#define _ATOMIC_H_INCLUDED

#include <stdlib.h> //null

template<typename T, typename T2>
__attribute__((always_inline)) inline T atomic_cast(T2 x) {
	static_assert(sizeof(T) == sizeof(T2), "size not match");
	char* pc = (char*)&x;
	return *(T*)pc;
}

//common implementation using interrupt disable
template <typename T, unsigned size> struct CLDREXnImplementation {
	__attribute__((always_inline)) static inline T LDREXn(volatile T* ptr) { __disable_irq(); return *const_cast<T*>(ptr); }
	__attribute__((always_inline)) static inline uint32_t STREXn(T value, volatile T* ptr) { *const_cast<T*>(ptr) = value; __enable_irq(); return 0; }
	__attribute__((always_inline)) static inline void CLREXn() { __enable_irq(); }
};
#if defined(STM32F4)
// ARM CORTEX-M3 specific using LDREX* STREX*
template <typename T> struct CLDREXnImplementation<T,1> {
	__attribute__((always_inline)) static inline T LDREXn(volatile T* ptr) { return atomic_cast<T>(__LDREXB(atomic_cast<volatile uint8_t*>(ptr))); }
	__attribute__((always_inline)) static inline uint32_t STREXn(T value, volatile T* ptr) { return __STREXB(atomic_cast<uint8_t>(value), atomic_cast<volatile uint8_t*>(ptr)); }
	__attribute__((always_inline)) static inline void CLREXn() { __CLREX(); }
};
template <typename T> struct CLDREXnImplementation<T,2> {
	__attribute__((always_inline)) static inline T LDREXn(volatile T* ptr) { return (T)(__LDREXH(atomic_cast<volatile uint16_t*>(ptr))); }
	__attribute__((always_inline)) static inline uint32_t STREXn(T value, volatile T* ptr) { return __STREXH(atomic_cast<uint16_t>(value), atomic_cast<volatile uint16_t*>(ptr)); }
	__attribute__((always_inline)) static inline void CLREXn() { __CLREX(); }
};
template <typename T> struct CLDREXnImplementation<T,4> {
	__attribute__((always_inline)) static inline T LDREXn(volatile T* ptr) { return atomic_cast<T>(__LDREXW(atomic_cast<volatile uint32_t*>(ptr))); }
	__attribute__((always_inline)) static inline uint32_t STREXn(T value, volatile T* ptr) { return __STREXW(atomic_cast<uint32_t>(value), atomic_cast<volatile uint32_t*>(ptr)); }
	__attribute__((always_inline)) static inline void CLREXn() { __CLREX(); }
};
#endif

template <typename T>
struct CAtomicImplementation {
	static inline void Exchange(volatile T* ptr, T* pValueRetOldValue) {
		T oldValue;
		do {
			oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(*pValueRetOldValue, ptr));
		*pValueRetOldValue = oldValue;
	}
	static inline void Exchange(volatile T* ptr, T value, T* pRetOldValue) {
		T oldValue;
		do {
			oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(value, ptr));
		*pRetOldValue = oldValue;
	}
	static inline bool CmpExchange(volatile T* ptr, T* expectedRetActual, T value) {
		bool bResult = true;
		T oldValue;
		do {
			oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
			if (oldValue == *expectedRetActual) {
				// do nothing
			} else {
				CLDREXnImplementation<T,sizeof(T)>::CLREXn();
				*expectedRetActual = oldValue;
				bResult = false;
				break;
			}
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(value, ptr));
		return bResult;
	}
	static inline bool CmpSet(volatile T* ptr, T expected, T value, T* pRetOldValue = NULL) {
		bool bResult = true;
		T oldValue;
		do {
			oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
			if (oldValue != expected) {
				CLDREXnImplementation<T,sizeof(T)>::CLREXn();
				bResult = false;
				break;
			}
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(value, ptr));
		if (pRetOldValue) *pRetOldValue = oldValue;
		return bResult;
	}
	static inline T Inc(volatile T* ptr) {
		T value;
		do {
			value = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
			++value;
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(value, ptr));
		return value;
	}
	template <typename T2>
	static inline T Add(volatile T* ptr, T2 value, T* pRetOldValue = NULL) {
		T oldValue, newValue;
		do {
			oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
			newValue = oldValue + value;
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(newValue, ptr));
		if (pRetOldValue) *pRetOldValue = oldValue;
		return newValue;
	}
	template <typename T2>
	static inline T AddCircle(volatile T* ptr, T2 value, T minValue, T maxValue, T* pRetOldValue = NULL)
	{
		T oldValue, newValue;
		T2 size = (maxValue - minValue + 1);
		while (value > size) value -= size;
		do {
			oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
			if (oldValue <= maxValue - value) {
				newValue = oldValue + value;
			} else {
				newValue = oldValue + value - size;
			}
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(newValue, ptr));
		if (pRetOldValue) *pRetOldValue = oldValue;
		return newValue;
	}
	static inline T Dec(volatile T* ptr) {
		T value;
		do {
			value = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
			--value;
		} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(value, ptr));
		return value;
	}
	template <typename T2>
	static inline T Sub(volatile T* ptr, T2 value, T* pRetOldValue = NULL) {
		{
			T oldValue, newValue;
			do {
				oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
				newValue = oldValue - value;
			} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(newValue, ptr));
			if (pRetOldValue) *pRetOldValue = oldValue;
			return newValue;
		}
	}
	static inline T And(volatile T* ptr, T value, T* pRetOldValue = NULL) {
		{
			T oldValue, newValue;
			do {
				oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
				newValue = oldValue & value;
			} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(newValue, ptr));
			if (pRetOldValue) *pRetOldValue = oldValue;
			return newValue;
		}
	}
	static inline T Or(volatile T* ptr, T value, T* pRetOldValue = NULL) {
		{
			T oldValue, newValue;
			do {
				oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
				newValue = oldValue | value;
			} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(newValue, ptr));
			if (pRetOldValue) *pRetOldValue = oldValue;
			return newValue;
		}
	}
	static inline T Xor(volatile T* ptr, T value, T* pRetOldValue = NULL) {
		{
			T oldValue, newValue;
			do {
				oldValue = CLDREXnImplementation<T,sizeof(T)>::LDREXn(ptr);
				newValue = oldValue ^ value;
			} while(CLDREXnImplementation<T,sizeof(T)>::STREXn(newValue, ptr));
			if (pRetOldValue) *pRetOldValue = oldValue;
			return newValue;
		}
	}
};

template<class T>
static inline void AtomicExchange(volatile T* ptr, T* pValueRetOldValue) {CAtomicImplementation<T>::Exchange(ptr, pValueRetOldValue);}
template<class T>
static inline void AtomicExchange(volatile T* ptr, T value, T* pRetOldValue) {CAtomicImplementation<T>::Exchange(ptr, value, pRetOldValue);}
template<class T>
static inline bool AtomicCmpExchange(volatile T* ptr, T* expectedRetActual, T value) {return CAtomicImplementation<T>::CmpExchange(ptr, expectedRetActual, value);}
template<typename T>
static inline bool AtomicCmpSet(volatile T* ptr, T expected, T value, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::CmpSet(ptr, expected, value, pRetOldValue);}
template<typename T>
static inline T AtomicInc(volatile T* ptr) {return CAtomicImplementation<T>::Inc(ptr);}
template<typename T, typename T2>
static inline T AtomicAdd(volatile T* ptr, T2 value, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::Add(ptr, value, pRetOldValue);}
template<typename T, typename T2>
static inline T AtomicAddCircle(volatile T* ptr, T2 value, T minValue, T maxValue, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::AddCircle(ptr, value, minValue, maxValue, pRetOldValue);}
template<class T>
static inline T AtomicDec(volatile T* ptr) {return CAtomicImplementation<T>::Dec(ptr);}
template<typename T, typename T2>
static inline T AtomicSub(volatile T* ptr, T2 value, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::Sub(ptr, value, pRetOldValue);}
template<class T>
static inline T AtomicAnd(volatile T* ptr, T value, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::And(ptr, value, pRetOldValue);}
template<class T>
static inline T AtomicOr(volatile T* ptr, T value, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::Or(ptr, value, pRetOldValue);}
template<class T>
static inline T AtomicXor(volatile T* ptr, T value, T* pRetOldValue = NULL) {return CAtomicImplementation<T>::Xor(ptr, value, pRetOldValue);}


#endif /* _ATOMIC_H_INCLUDED */
