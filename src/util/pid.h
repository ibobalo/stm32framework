#ifndef PID_H_INCLUDED
#define PID_H_INCLUDED

#include "macros.h"

template<class valueT = float, class timeT = int>
class TIntegrate
{
public:
	typedef valueT value_type;
	typedef timeT  time_type;
public:
	void Init(value_type vtInitialValue, time_type ttInitialTime) {
		m_ttRecentTime = ttInitialTime;
		m_vtRecentValue = vtInitialValue;
		m_vtIntegratedValue = 0;
		m_bHasLimits = 0;
	}
	void SetIntagrationLimits(value_type vtIntegrationLimitMin, value_type vtIntegrationLimitMax) {
		m_vtIntegrationLimitMin = vtIntegrationLimitMin;
		m_vtIntegrationLimitMax = vtIntegrationLimitMax;
		m_bHasLimits = (m_vtIntegrationLimitMax > m_vtIntegrationLimitMin);
		ApplyIntegrationLimitIfNecessary();
	}
	void SetIntegrationLimit(value_type vtIntegrationLimit) {SetIntagrationLimits(-vtIntegrationLimit, vtIntegrationLimit);}

	void IntegrateNextValue(value_type vtValue, time_type ttTime) {
		time_type dt = ttTime - m_ttRecentTime;
		m_vtIntegratedValue += (m_vtRecentValue + vtValue) * dt / (value_type)2;
		m_ttRecentTime = ttTime;
		m_vtRecentValue = vtValue;
		ApplyIntegrationLimitIfNecessary();
	}
	const value_type& GetIntegratedValue() const {return m_vtIntegratedValue;}

	void Set(value_type vtValue) {m_vtIntegratedValue = vtValue;}
	void Limit(value_type vtIntegrationLimitMin, value_type vtIntegrationLimitMax) {m_vtIntegratedValue = LIMIT(m_vtIntegratedValue, vtIntegrationLimitMin, vtIntegrationLimitMax);}
private:
	void ApplyIntegrationLimitIfNecessary() {if (m_bHasLimits) { Limit(m_vtIntegrationLimitMin, m_vtIntegrationLimitMax);} }

	time_type  m_ttRecentTime;
	value_type m_vtRecentValue;
	value_type m_vtIntegratedValue;
	value_type m_vtIntegrationLimitMin;
	value_type m_vtIntegrationLimitMax;
	bool       m_bHasLimits;
};

template<class valueT = float, class timeT = int>
class TDeriviate
{
public:
	typedef valueT value_type;
	typedef timeT  time_type;
public:
	void Init(value_type vtInitialValue, time_type ttInitialTime) {
		m_ttRecentTime = ttInitialTime;
		m_vtRecentValue = vtInitialValue;
		m_vtDeriviation = 0;
	}
	void DeriviateNextValue(value_type vtValue, time_type ttTime) {
		time_type dt = ttTime - m_ttRecentTime;
		if (dt > (time_type)0) {
			m_vtDeriviation = (vtValue - m_vtRecentValue) / dt;
			m_ttRecentTime = ttTime;
			m_vtRecentValue = vtValue;
		}
	}
	const value_type& GetDeriviationValue() const {return m_vtDeriviation;}
private:
	time_type   m_ttRecentTime;
	value_type  m_vtRecentValue;
	value_type  m_vtDeriviation;
};

template<class valueT = float, class timeT = int>
class TPidController
{
public:
	typedef valueT value_type;
	typedef timeT  time_type;
public:
	void Init(value_type vtInitialValue, time_type ttInitialTime, value_type vtKP, value_type vtKI, value_type vtKD, value_type vtIntegrationLimit) {
		m_vtKP = vtKP;
		m_vtKI = vtKI;
		m_vtKD = vtKD;
		m_Integrate.Init(vtInitialValue, ttInitialTime);
		m_Integrate.SetIntegrationLimit(vtIntegrationLimit);
		m_Deriviate.Init(vtInitialValue, ttInitialTime);
	}

	value_type Control(value_type vtDesiredValue, value_type vtActualValue, time_type ttTime) {
		value_type vtError = vtDesiredValue - vtActualValue;
		value_type vtResult = vtError * m_vtKP;
		if (m_vtKI != (value_type)0) {
			m_Integrate.IntegrateNextValue(vtError, ttTime);
			vtResult += m_Integrate.GetIntegratedValue() * m_vtKI;
		}
		if (m_vtKD != (value_type)0) {
			m_Deriviate.DeriviateNextValue(vtError, ttTime);
			vtResult += m_Deriviate.GetDeriviationValue() * m_vtKD;
		}
		return vtResult;
	}
private:
	value_type m_vtKP;
	value_type m_vtKI;
	value_type m_vtKD;

	TIntegrate<value_type, time_type>  m_Integrate;
	TDeriviate<value_type, time_type>  m_Deriviate;
};

#endif // PID_H_INCLUDED
