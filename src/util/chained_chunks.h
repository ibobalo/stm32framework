#ifndef CHAINED_CHUNKS_H_INCLUDED
#define CHAINED_CHUNKS_H_INCLUDED

/*
 * usage:
 *
 *  void ComposeAndSendComplexPacket() {
 *    CChainedChunks cc1;
 *    cc1.Assign(("send me first", 13))
 *    cc1.Append(GetInternalPkt());
 *    SendPkt(&cc1);
 * };
 */

#include "util/list.h"
#include "util/macros.h"
#include "util/utils.h"
#include <stdint.h>
#include <algorithm>
#include <type_traits>

template <class T = uint8_t>
struct chunk_t {
	typedef T CDataType;
	typedef typename std::remove_const<T>::type CTargetType;

	T*             pData;
	unsigned       uiLength;

	void Assign(T* _pData, unsigned _uiLength)   {pData = _pData; uiLength = _uiLength;};
	void Assign(const chunk_t<T>& _Other)   {pData = _Other.pData; uiLength = _Other.uiLength;};
	void Clean()   {pData = 0; uiLength = 0;};
	unsigned DiscardData(unsigned nMaxItemsCount) {unsigned nDiscardedItems = MIN2(nMaxItemsCount, uiLength); pData += nDiscardedItems; uiLength -= nDiscardedItems; return nDiscardedItems;}
	unsigned CopyData(CTargetType* pTgtBuffer, unsigned nMaxItemsCount) const {unsigned nCopiedItems = MIN2(nMaxItemsCount, uiLength); objcpy(pTgtBuffer, pData, nCopiedItems); return nCopiedItems;}
	unsigned PopData(CTargetType* pTgtBuffer, unsigned nMaxItemsCount) {
			unsigned nPoppedItems = MIN2(nMaxItemsCount, uiLength);
			objcpy(pTgtBuffer, pData, nPoppedItems); pData += nPoppedItems; uiLength -= nPoppedItems;
			return nPoppedItems;}
	unsigned PushData(const CDataType* pSrcBuffer, unsigned nMaxItemsCount) {
			unsigned nPushedItems = (nMaxItemsCount < uiLength) ? nMaxItemsCount : uiLength;
			objcpy(pData, pSrcBuffer, nPushedItems); pData += nPushedItems; uiLength -= nPushedItems;
			return nPushedItems;}
	bool operator == (const chunk_t<T>& _Other) const { return uiLength == _Other.uiLength && objcmp<T>(pData, _Other.pData, uiLength) == 0;}
	bool operator < (const chunk_t<T>& _Other) const { int i = objcmp<T>(pData, _Other.pData, MIN2(uiLength, _Other.uiLength)); return i < 0 || (i == 0 && uiLength < _Other.uiLength);}
};

typedef chunk_t<uint8_t>           CChunk;
typedef chunk_t<volatile uint8_t>  CIoChunk;
typedef chunk_t<const uint8_t>     CConstChunk;

#define STR_CHUNK(name, str) const CConstChunk name = {(const uint8_t*)(str), sizeof(str) - 1}

template<class T>
unsigned CollectChainTotalLength(const T* pChainHeadChunk);
template<class T>
unsigned DiscardChainData(T* pChainHeadChunk, unsigned uiInc);
template<class T>
unsigned CopyChainData(const T* pChainHeadChunk, typename T::CTargetType* pTgtBuffer, unsigned nMaxItemsCount);
template<class T>
unsigned PopChainData(T* pChainHeadChunk, typename T::CTargetType* pTgtBuffer, unsigned nMaxItemsCount);

typedef ListedItem< CChunk >       CChainedChunks;
typedef ListedItem< CIoChunk >     CChainedIoChunks;
typedef ListedItem< CConstChunk >  CChainedConstChunks;









/*
 * templates implementation
 */

template<class T>
unsigned CollectChainTotalLength(const T* pChainHeadChunk)
{
	unsigned uiResult = pChainHeadChunk->uiLength;
	const T* pCurrentChunk = pChainHeadChunk->GetNextItem();
	while (pCurrentChunk) {
		uiResult += pCurrentChunk->uiLength;
		pCurrentChunk = pCurrentChunk->GetNextItem();
	}
	return uiResult;
}

template<class T>
unsigned DiscardChainData(T* pChainHeadChunk, unsigned nMaxItemsCount)
{
	unsigned nDiscardsLeft = nMaxItemsCount - pChainHeadChunk->DiscardData(nMaxItemsCount);
	while (nDiscardsLeft > 0) {
		T* pNextChunk = pChainHeadChunk->GetNextItem();
		if (pNextChunk) {
			*pChainHeadChunk = *pNextChunk;
			nDiscardsLeft -= pChainHeadChunk->DiscardData(nDiscardsLeft);
		} else {
			break;
		}
	}
	return nMaxItemsCount - nDiscardsLeft;
}

template<class T>
unsigned CopyChainData(const T* pChainHeadChunk, typename T::CTargetType* pTgtBuffer, unsigned nMaxItemsCount)
{
	const T* pCurrentChunk = pChainHeadChunk;
	typename T::CTargetType* pTgt = pTgtBuffer;
	unsigned nItemsLeft = nMaxItemsCount;
	while (nItemsLeft && pCurrentChunk) {
		unsigned n = pCurrentChunk->CopyData(pTgt, nItemsLeft);
		pTgt += n * sizeof(*pCurrentChunk->pData);
		nItemsLeft -= n;
		pCurrentChunk = pCurrentChunk->GetNextItem();
	}
	return nMaxItemsCount - nItemsLeft;
}

template<class T>
unsigned PopChainData(T* pChainHeadChunk, typename T::CTargetType* pTgtBuffer, unsigned nMaxItemsCount)
{
	unsigned n = pChainHeadChunk->PopData(pTgtBuffer, nMaxItemsCount);
	typename T::CTargetType* pTgt = pTgtBuffer + n;
	unsigned nItemsLeft = nMaxItemsCount - n;
	const T* pNextChunk = pChainHeadChunk->GetNextItem();
	while (nItemsLeft && pNextChunk) {
		*pChainHeadChunk = *pNextChunk;
		n = pChainHeadChunk->PopData(pTgt, nItemsLeft);
		pTgt += n;
		nItemsLeft -= n;
		pNextChunk = pChainHeadChunk->GetNextItem();
	}
	if (!pChainHeadChunk->uiLength && pNextChunk) {
		*pChainHeadChunk = *pNextChunk;
	}
	return nMaxItemsCount - nItemsLeft;
}

template<class T>
unsigned PushChainData(T* pChainHeadChunk, const typename T::CDataType* pSrcBuffer, unsigned nMaxItemsCount)
{
	unsigned n = pChainHeadChunk->PushData(pSrcBuffer, nMaxItemsCount);
	uint8_t* pSrc = (uint8_t*)pSrcBuffer + n;
	unsigned nItemsLeft = nMaxItemsCount - n;
	const T* pNextChunk = pChainHeadChunk->GetNextItem();
	while (nItemsLeft && pNextChunk) {
		*pChainHeadChunk = *pNextChunk;
		n = pChainHeadChunk->PushData(pSrc, nItemsLeft);
		pSrc += n;
		nItemsLeft -= n;
		pNextChunk = pChainHeadChunk->GetNextItem();
	}
	if (!pChainHeadChunk->uiLength && pNextChunk) {
		*pChainHeadChunk = *pNextChunk;
	}
	return nMaxItemsCount - nItemsLeft;
}

#endif // CHAINED_CHUNKS_H_INCLUDED
