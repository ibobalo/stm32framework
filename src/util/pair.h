#ifndef PAIR_H_INCLUDED
#define PAIR_H_INCLUDED

template <class T1, class T2 = T1>
struct pair
{
public:
	T1 first;
	T2 second;
//public:
//	pair() {};
//	pair(T1 v1, T2 v2) {first = v1; second = v2;}
//	const pair<T1,T2>&
//	     operator = (const pair<T1,T2>& other) {first = other.first; second = other.second; return *this;}
};

template <class T1, class T2> pair<T1,T2> make_pair(T1 v1, T2 v2) {pair<T1,T2> x = {v1, v2}; return x;}
template <class T1, class T2> pair<T1,T2> operator + (const pair<T1,T2>& l, const pair<T1,T2>& r) {return pair<T1,T2>(l.first + r.first, l.second + r.second);}
template <class T1, class T2> pair<T1,T2> operator - (const pair<T1,T2>& l, const pair<T1,T2>& r) {return pair<T1,T2>(l.first - r.first, l.second - r.second);}
template <class T1, class T2> pair<T1,T2> operator * (const pair<T1,T2>& l, const pair<T1,T2>& r) {return pair<T1,T2>(l.first * r.first, l.second * r.second);}
template <class T1, class T2> pair<T1,T2> operator / (const pair<T1,T2>& l, const pair<T1,T2>& r) {return pair<T1,T2>(l.first / r.first, l.second / r.second);}
template <class T1, class T2, class T> pair<T1,T2> operator + (const pair<T1,T2>& l, const T& r) {return pair<T1,T2>(l.first + r, l.second + r);}
template <class T1, class T2, class T> pair<T1,T2> operator - (const pair<T1,T2>& l, const T& r) {return pair<T1,T2>(l.first - r, l.second - r);}
template <class T1, class T2, class T> pair<T1,T2> operator * (const pair<T1,T2>& l, const T& r) {return pair<T1,T2>(l.first * r, l.second * r);}
template <class T1, class T2, class T> pair<T1,T2> operator / (const pair<T1,T2>& l, const T& r) {return pair<T1,T2>(l.first / r, l.second / r);}
template <class T1, class T2, class T> pair<T1,T2> operator + (const T& l, const pair<T1,T2>& r) {return pair<T1,T2>(l + r.first, l + r.second);}
template <class T1, class T2, class T> pair<T1,T2> operator - (const T& l, const pair<T1,T2>& r) {return pair<T1,T2>(l - r.first, l - r.second);}
template <class T1, class T2, class T> pair<T1,T2> operator * (const T& l, const pair<T1,T2>& r) {return pair<T1,T2>(l * r.first, l * r.second);}
template <class T1, class T2, class T> pair<T1,T2> operator / (const T& l, const pair<T1,T2>& r) {return pair<T1,T2>(l / r.first, l / r.second);}
template <class T1, class T2>          bool operator == (const pair<T1,T2>& l, const pair<T1,T2>& r) {return l.first == r.first && l.second == r.second;}
template <class T1, class T2>          bool operator != (const pair<T1,T2>& l, const pair<T1,T2>& r) {return l.first != r.first || l.second != r.second;}
template <class T1, class T2>          bool operator >  (const pair<T1,T2>& l, const pair<T1,T2>& r) {return l.first >  r.first || l.second >  r.second;}
template <class T1, class T2>          bool operator >= (const pair<T1,T2>& l, const pair<T1,T2>& r) {return l.first >= r.first || l.second >= r.second;}
template <class T1, class T2>          bool operator <  (const pair<T1,T2>& l, const pair<T1,T2>& r) {return l.first <  r.first || l.second <  r.second;}
template <class T1, class T2>          bool operator <= (const pair<T1,T2>& l, const pair<T1,T2>& r) {return l.first <= r.first || l.second <= r.second;}
template <class T1, class T2, class T> bool operator == (const pair<T1,T2>& l, const T& r) {return l.first == r && l.second == r;}
template <class T1, class T2, class T> bool operator != (const pair<T1,T2>& l, const T& r) {return l.first != r || l.second != r;}
template <class T1, class T2, class T> bool operator >  (const pair<T1,T2>& l, const T& r) {return l.first >  r || l.second >  r;}
template <class T1, class T2, class T> bool operator >= (const pair<T1,T2>& l, const T& r) {return l.first >= r || l.second >= r;}
template <class T1, class T2, class T> bool operator <  (const pair<T1,T2>& l, const T& r) {return l.first <  r || l.second <  r;}
template <class T1, class T2, class T> bool operator <= (const pair<T1,T2>& l, const T& r) {return l.first <= r || l.second <= r;}
template <class T1, class T2, class T> bool operator == (const T& l, const pair<T1,T2>& r) {return l == r.first && l == r.second;}
template <class T1, class T2, class T> bool operator != (const T& l, const pair<T1,T2>& r) {return l != r.first || l != r.second;}
template <class T1, class T2, class T> bool operator >  (const T& l, const pair<T1,T2>& r) {return l >  r.first || l >  r.second;}
template <class T1, class T2, class T> bool operator >= (const T& l, const pair<T1,T2>& r) {return l >= r.first || l >= r.second;}
template <class T1, class T2, class T> bool operator <  (const T& l, const pair<T1,T2>& r) {return l <  r.first || l <  r.second;}
template <class T1, class T2, class T> bool operator <= (const T& l, const pair<T1,T2>& r) {return l <= r.first || l <= r.second;}


template <class T1, class T2=T1, class T3=T2>
struct tripple
{
public:
	T1 first;
	T2 second;
	T3 third;
//public:
//	tripple() {};
//	tripple(const T1& v1, const T2& v2, const T3& v3) : first(v1), second(v2), third(v3) {};
//	const tripple<T1,T2,T3>&
//	     operator = (const tripple<T1,T2,T3>& other) {first = other.first; second = other.second; return *this;}
};

template <class T1, class T2, class T3> tripple<T1,T2,T3> make_tripple(T1 v1, T2 v2, T3 v3) {tripple<T1,T2,T3> x = {v1, v2, v3}; return x;}
template <class T1, class T2, class T3> tripple<T1,T2,T3> operator + (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return make_tripple(l.first + r.first, l.second + r.second, l.third + r.third);}
template <class T1, class T2, class T3> tripple<T1,T2,T3> operator - (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return make_tripple(l.first - r.first, l.second - r.second, l.third - r.third);}
template <class T1, class T2, class T3> tripple<T1,T2,T3> operator * (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return make_tripple(l.first * r.first, l.second * r.second, l.third * r.third);}
template <class T1, class T2, class T3> tripple<T1,T2,T3> operator / (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return make_tripple(l.first / r.first, l.second / r.second, l.third / r.third);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator +  (const T& l, const tripple<T1,T2,T3>& r) {return make_tripple(r + l.first, r + l.second, r + l.third);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator -  (const T& l, const tripple<T1,T2,T3>& r) {return make_tripple(r - l.first, r - l.second, r - l.third);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator *  (const T& l, const tripple<T1,T2,T3>& r) {return make_tripple(r * l.first, r * l.second, r * l.third);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator /  (const T& l, const tripple<T1,T2,T3>& r) {return make_tripple(r / l.first, r / l.second, r / l.third);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator +  (const tripple<T1,T2,T3>& l, const T& r) {return make_tripple(l.first + r, l.second + r, l.third + r);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator -  (const tripple<T1,T2,T3>& l, const T& r) {return make_tripple(l.first - r, l.second - r, l.third - r);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator *  (const tripple<T1,T2,T3>& l, const T& r) {return make_tripple(l.first * r, l.second * r, l.third * r);}
template <class T1, class T2, class T3, class T> tripple<T1,T2,T3> operator /  (const tripple<T1,T2,T3>& l, const T& r) {return make_tripple(l.first / r, l.second / r, l.third / r);}
template <class T1, class T2, class T3>          bool operator == (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return l.first == r.first && l.second == r.second && l.third == r.third;}
template <class T1, class T2, class T3>          bool operator != (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return l.first != r.first || l.second != r.second || l.third != r.third;}
template <class T1, class T2, class T3>          bool operator >  (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return l.first >  r.first || l.second >  r.second || l.third >  r.third;}
template <class T1, class T2, class T3>          bool operator >= (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return l.first >= r.first || l.second >= r.second || l.third >= r.third;}
template <class T1, class T2, class T3>          bool operator <  (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return l.first <  r.first || l.second <  r.second || l.third <  r.third;}
template <class T1, class T2, class T3>          bool operator <= (const tripple<T1,T2,T3>& l, const tripple<T1,T2,T3>& r) {return l.first <= r.first || l.second <= r.second || l.third <= r.third;}
template <class T1, class T2, class T3, class T> bool operator == (const tripple<T1,T2,T3>& l, const T& r) {return l.first == r && l.second == r && l.third == r;}
template <class T1, class T2, class T3, class T> bool operator != (const tripple<T1,T2,T3>& l, const T& r) {return l.first != r || l.second != r || l.third != r;}
template <class T1, class T2, class T3, class T> bool operator >  (const tripple<T1,T2,T3>& l, const T& r) {return l.first >  r || l.second >  r || l.third == r;}
template <class T1, class T2, class T3, class T> bool operator >= (const tripple<T1,T2,T3>& l, const T& r) {return l.first >= r || l.second >= r || l.third == r;}
template <class T1, class T2, class T3, class T> bool operator <  (const tripple<T1,T2,T3>& l, const T& r) {return l.first <  r || l.second <  r || l.third <  r;}
template <class T1, class T2, class T3, class T> bool operator <= (const tripple<T1,T2,T3>& l, const T& r) {return l.first <= r || l.second <= r || l.third <= r;}
template <class T1, class T2, class T3, class T> bool operator == (const T& l, const tripple<T1,T2,T3>& r) {return l == r.first && l == r.second && l == r.third;}
template <class T1, class T2, class T3, class T> bool operator != (const T& l, const tripple<T1,T2,T3>& r) {return l != r.first || l != r.second || l != r.third;}
template <class T1, class T2, class T3, class T> bool operator >  (const T& l, const tripple<T1,T2,T3>& r) {return l >  r.first || l >  r.second || l >  r.third;}
template <class T1, class T2, class T3, class T> bool operator >= (const T& l, const tripple<T1,T2,T3>& r) {return l >= r.first || l >= r.second || l >= r.third;}
template <class T1, class T2, class T3, class T> bool operator <  (const T& l, const tripple<T1,T2,T3>& r) {return l <  r.first || l <  r.second || l <  r.third;}
template <class T1, class T2, class T3, class T> bool operator <= (const T& l, const tripple<T1,T2,T3>& r) {return l <= r.first || l <= r.second || l <= r.third;}

#endif // PAIR_H_INCLUDED
