#ifndef PIPES_H_INCLUDED
#define PIPES_H_INCLUDED

#include "core.h"
#include "macros.h"
#include "utils.h"
#include "chained_chunks.h"
#include <string.h>

class IStreamSource;
class IStreamReceiver;
class CStreamFilter;

class IStreamReceiver
{
public: // IStreamReceiver
	virtual unsigned Push(const CConstChunk& ccData) = 0;
};

class IStreamSource
{
public: // IStreamSource
	virtual void Bind(IStreamReceiver* pStreamReceiver) = 0;
};

class CStreamSource :
		public IStreamSource
{
public: // IStreamSource
	virtual void Bind(IStreamReceiver* pStreamReceiver) {
		IMPLEMENTS_INTERFACE_METHOD(IStreamSource::Bind(pStreamReceiver));
		m_pReceiver = pStreamReceiver;
	}

protected:
	unsigned Broadcast(const CConstChunk& ccData) {ASSERTE(m_pReceiver); return m_pReceiver->Push(ccData);};

private:
	IStreamReceiver* m_pReceiver;
};

class CStreamFilter :
		public IStreamReceiver,
		public CStreamSource
{
};


/*
 * collect data to internal buffer (cache)
 * Broadcast() the collected data with Flush() method
 * Broadcast() the collected data immediate if bAutoFlush is true
 * keep the rest data (not committed by Broadcast()) in cache
 * if bStaticLock is true, keep pushed data in cache memory until Release() method call
 *    (for example after HW DMA data sending completed)
 */

template<unsigned MaxCacheSize>
class CCache
{
public:
	void Init() {m_uiCachedDataLength = m_uiLockedDataLength = m_uiReservedBufferSize = 0;}
	const uint8_t* LockData() {ASSERTE(!m_uiLockedDataLength); m_uiLockedDataLength = m_uiCachedDataLength; return m_uiLockedDataLength ? m_CacheBuffer : 0;}
	void ExtendDataLock(unsigned uiLength) {m_uiLockedDataLength += uiLength;}
	unsigned GetLockedDataLength() const {return m_uiLockedDataLength;}
	CConstChunk LockDataChunk() {CConstChunk cc = {LockData(), m_uiLockedDataLength}; return cc;}
	void ReleaseData() {RemoveFromCache(m_uiLockedDataLength); m_uiLockedDataLength = 0;}
	void ReleaseData(unsigned uiLength) {ASSERTE(uiLength <= m_uiLockedDataLength); RemoveFromCache(uiLength); m_uiLockedDataLength = 0;}
	void Reset()   {ASSERTE(!m_uiLockedDataLength); ASSERTE(!m_uiReservedBufferSize); m_uiCachedDataLength = 0;}
	CConstChunk GetCachedChunk() const {CConstChunk cc = {m_CacheBuffer,m_uiCachedDataLength}; return cc;}
	const uint8_t* GetCachedData() const {return m_CacheBuffer;}
	unsigned GetCachedDataLength() const {return m_uiCachedDataLength;}
	static unsigned GetCacheCapacity() {return MaxCacheSize;}
//	uint8_t* GetAvailableData() {return m_CacheBuffer + m_uiCachedDataSize;}
//	unsigned GetAvailableSize() const {return MaxCacheSize - m_uiCachedDataSize;}
	uint8_t* Reserve() {ASSERTE(!m_uiReservedBufferSize); m_uiReservedBufferSize = MaxCacheSize - m_uiCachedDataLength; return m_uiReservedBufferSize ? m_CacheBuffer + m_uiCachedDataLength : 0;}
	volatile uint8_t* Reserve(unsigned uiSize) {ASSERTE(!m_uiReservedBufferSize); ASSERTE(uiSize); m_uiReservedBufferSize = MIN2(uiSize, MaxCacheSize - m_uiCachedDataLength); return m_uiReservedBufferSize ? m_CacheBuffer + m_uiCachedDataLength : 0;}
	unsigned GetReservedSize() const {return m_uiReservedBufferSize;}
	CIoChunk ReserveChunk() {CIoChunk cc = {Reserve(), m_uiReservedBufferSize}; return cc;}
	CIoChunk ReserveChunk(unsigned uiSize) {CIoChunk cc = {Reserve(uiSize), m_uiReservedBufferSize}; return cc;}
	void CancelReserved() {m_uiReservedBufferSize = 0;}
	void AppendReserved() {m_uiCachedDataLength += m_uiReservedBufferSize; m_uiReservedBufferSize = 0;}
	void AppendReserved(unsigned uiLength) {ASSERTE(uiLength <= m_uiReservedBufferSize); m_uiCachedDataLength += uiLength; m_uiReservedBufferSize = 0;}
//	void IncreaseCachedSize(unsigned uiSize) {m_uiCachedDataSize += uiSize;}
//	void UnappendToCache(unsigned uiSize) {
//		if (uiSize <= m_uiCachedDataLength) {
//			m_uiCachedDataLength -= uiSize;
//		} else {
//			m_uiCachedDataLength = 0;
//		}
//	}
	unsigned AppendToCache(const CConstChunk& ccData) {return AppendToCache(ccData.pData, ccData.uiLength);}
	unsigned AppendToCache(const CConstChunk& ccData, unsigned uiOffset) {return AppendToCache(ccData.pData + uiOffset, ccData.uiLength - uiOffset);}
	unsigned AppendToCache(const uint8_t* pData, unsigned uiLength) {
		unsigned uiFreeCacheSize = MaxCacheSize - m_uiCachedDataLength;
		if (uiLength <= uiFreeCacheSize) {
			memcpy(m_CacheBuffer + m_uiCachedDataLength, pData, uiLength);
			m_uiCachedDataLength += uiLength;
			return uiLength;
		} else {
			memcpy(m_CacheBuffer + m_uiCachedDataLength, pData, uiFreeCacheSize);
			m_uiCachedDataLength += uiFreeCacheSize;
			return uiFreeCacheSize;
		}
	}
	void RemoveFromCache(unsigned uiLength) {
		ASSERTE(uiLength <= m_uiCachedDataLength);
		m_uiCachedDataLength -= uiLength;
		if (m_uiCachedDataLength) {
			memcpy(m_CacheBuffer, m_CacheBuffer + uiLength, m_uiCachedDataLength);
		}
	}

	uint8_t  m_CacheBuffer[MaxCacheSize];
	unsigned m_uiCachedDataLength;
	unsigned m_uiLockedDataLength;
	unsigned m_uiReservedBufferSize;
};

template<unsigned MaxCacheSize, bool bAutoFlush = true, bool bStaticLock = false>
class CStreamCache :
		public CCache<MaxCacheSize>,
		public CStreamFilter
{
	typedef CCache<MaxCacheSize> CParentCache;
public: // IStreamReceiver
	virtual unsigned Push(const CConstChunk& ccData) {
		IMPLEMENTS_INTERFACE_METHOD(IStreamReceiver::Push(ccData));
		unsigned uiPushedDataLength = 0;
		if (bAutoFlush && !bStaticLock && !CParentCache::GetCachedDataLength()) {
			// instant flow
			uiPushedDataLength = Broadcast(ccData);
			if (uiPushedDataLength < ccData.uiLength) {
				uiPushedDataLength += CParentCache::AppendToCache(ccData, uiPushedDataLength);
			}
		} else {
			// cached flow
			while (uiPushedDataLength < ccData.uiLength) {
				unsigned uiAppendedLength = CParentCache::AppendToCache(ccData, uiPushedDataLength);
				if (uiAppendedLength) {
					uiPushedDataLength += uiAppendedLength;
					if (bAutoFlush) {
						if (bStaticLock) {
							CConstChunk ccAfterLockData = CParentCache::GetCachedChunk();
							ccAfterLockData.DiscardData(CParentCache::GetLockedDataLength());
							unsigned uiBroadcastedSize = Broadcast(ccAfterLockData);
							if (uiBroadcastedSize) {
								CParentCache::ExtendDataLock(uiBroadcastedSize);
							}
							break;
						} else {
							unsigned uiBroadcastedSize = Broadcast(CParentCache::GetCachedChunk());
							if (uiBroadcastedSize) {
								CParentCache::RemoveFromCache(uiBroadcastedSize);
							} else {
								break;
							}
						}
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return uiPushedDataLength;
	}
public:
	unsigned Flush()   {
		CConstChunk ccData = CParentCache::GetCachedChunk();
		if (bStaticLock) {
			ccData.DiscardData(CParentCache::GetLockedDataLength());
		}
		unsigned uiBroadcastedLength = Broadcast(ccData);
		if (bStaticLock) {
			CParentCache::ExtendDataLock(uiBroadcastedLength);
		} else {
			CParentCache::RemoveFromCache(uiBroadcastedLength);
		}
		return uiBroadcastedLength;
	}
};

#endif /* PIPES_H_INCLUDED */
