#ifndef BITOPS_H_INCLUDED
#define BITOPS_H_INCLUDED

#include <stdint.h>

#ifdef __cplusplus

template <typename T>
bool CheckBitsParityOdd(T tValue)
{
	T x = tValue;
	if (sizeof(x) > 4) x ^= x >> 32;
	if (sizeof(x) > 2) x ^= x >> 16;
	if (sizeof(x) > 1) x ^= x >> 8;
	x ^= x >> 4;
	x ^= x >> 2;
	x ^= x >> 1;
	return x & 1;
}


template<typename T>
static inline constexpr T ReflectBits(T x);

static constexpr uint8_t g_NibbleReflectTable[] = {0x0, 0x8, 0x4, 0xC, 0x2, 0xA, 0x6, 0xE, 0x1, 0x9, 0x5, 0xD, 0x3, 0xB, 0x7, 0xF};
template<>
constexpr uint8_t ReflectBits<uint8_t>(uint8_t byte)
{
	return (g_NibbleReflectTable[byte & 0x0F] << 4) | g_NibbleReflectTable[byte >> 4];
}
template<>
constexpr uint16_t ReflectBits<uint16_t>(uint16_t word)
{
  return (((uint16_t)ReflectBits<uint8_t>((uint8_t)(word & 0xFF)) << 8) | ReflectBits<uint8_t>((uint8_t)(word >> 8)));
}
template<>
constexpr uint32_t ReflectBits<uint32_t>(uint32_t dword)
{
  return (((uint32_t)ReflectBits<uint16_t>((uint16_t)(dword & 0xFFFF)) << 16) | ReflectBits<uint16_t>((uint16_t)(dword >> 16)));
}

template<>
constexpr uint64_t ReflectBits<uint64_t>(uint64_t qword)
{
  return (((uint64_t)ReflectBits<uint32_t>((uint32_t)(qword & 0xFFFFFFFF)) << 32) | ReflectBits<uint32_t>((uint32_t)(qword >> 32)));
}

template<class T, class Tmask> static inline constexpr T SetBits1(T& v, const Tmask mask) __attribute__ ((always_inline));
template<class T, class Tmask> static inline constexpr T SetBits1(T& v, const Tmask mask)
{
	return (v |= static_cast<T>(mask));
}

template<class T, class Tmask> static inline constexpr T SetBits0(T& v, const Tmask mask) __attribute__ ((always_inline));
template<class T, class Tmask> static inline constexpr T SetBits0(T& v, const Tmask mask)
{
	return (v &= ~static_cast<T>(mask));
}

template<class T, class Tmask> static inline constexpr T SetBits01(T& v, const Tmask mask0, const Tmask mask1) __attribute__ ((always_inline));
template<class T, class Tmask> static inline constexpr T SetBits01(T& v, const Tmask mask0, const Tmask mask1)
{
	return SetBits0(v, mask0),SetBits1(v, mask1);
}

template<class T, class Tmask> static inline constexpr T SetBits(T& v, const Tmask mask, bool state) __attribute__ ((always_inline));
template<class T, class Tmask> static inline constexpr T SetBits(T& v, const Tmask mask, bool state)
{
	return state ? SetBits1(v, mask) : SetBits0(v, mask);
}

template<class T, class Tmask> static inline constexpr bool CheckBitsAny(const T v, const Tmask mask) __attribute__ ((always_inline));
template<class T, class Tmask> static inline constexpr bool CheckBitsAny(const T v, const Tmask mask)
{
	return (v & static_cast<T>(mask)) != 0;
}

template<class T, class Tmask> static inline constexpr bool CheckBitsAll(const T v, const Tmask mask) __attribute__ ((always_inline));
template<class T, class Tmask> static inline constexpr bool CheckBitsAll(const T v, const Tmask mask)
{
	return (v & static_cast<T>(mask)) == static_cast<T>(mask);
}

template<typename T>
class TBitsType
{
public:
	enum {
		BITS_COUNT = sizeof(T) * 8,
	};
	static inline constexpr T GetBitMask(unsigned bit_index) {return (T(1) << bit_index);}
	static inline constexpr bool GetBit(T x, unsigned bit_index) {return x & GetBitMask(bit_index);}
	static inline constexpr bool LSB(T x) {return x & GetBitMask(0);}
	static inline constexpr bool MSB(T x) {return x & mask(BITS_COUNT - 1);}
	static inline constexpr bool GetLoBit(T x) {return x & GetBitMask(0);}
	static inline constexpr bool GetHiBit(T x) {return x & mask(BITS_COUNT - 1);}
	static inline constexpr T GetLoBits(T x, unsigned count) {return x & (AllBits1()>>(BITS_COUNT - count));}
	static inline constexpr T GetHiBits(T x, unsigned count) {return x >> (BITS_COUNT - count);}
	static inline constexpr uint8_t GetLoByte(T x) {return GetLoBits(x, 8);}
	static inline constexpr uint8_t GetHiByte(T x) {return GetHiBits(x, 8);}
	static inline constexpr T Reflect(T x) {return ReflectBits(x);}
	static inline constexpr T Reflect(T x, unsigned count) {return ReflectBits(x) >> (BITS_COUNT - count);}
	static inline constexpr T AllBits1() {return ~T(0);}
	static inline constexpr T SetBit0(T x, unsigned bit_index) {return x & ~GetBitMask(bit_index);}
	static inline constexpr T SetBit1(T x, unsigned bit_index) {return x | GetBitMask(bit_index);}
	static inline constexpr T SetBit(T x, unsigned bit_index, bool state) {return state ? SetBit1(x, bit_index) : SetBit0(x, bit_index);}
	static inline constexpr T SetBits0(T x, const T mask0) {return x & ~mask0;}
	static inline constexpr T SetBits1(T x, const T mask1) {return x | mask1;}
	static inline constexpr T SetBits01(T x, const T mask0, const T mask1) {return SetBits1(SetBits0(x, mask0), mask1);}
	static inline constexpr T SetBits(T x, const T mask, bool state) {return state ? SetBits1(x, mask) : SetBits0(x, mask);}
	static inline constexpr T InvertBit(T x, unsigned bit_index) {return x ^ GetBitMask(bit_index);}
	static inline constexpr T InvertBits(T x) {return ~x;}
	static inline constexpr T InvertBits(T x, const T mask) {return x ^ mask;}
	template<unsigned n>
	static inline constexpr T ShiftRight(T x) {return x >> n;}
	template<unsigned n>
	static inline constexpr T ShiftLeft(T x) {return x << n;}

};

template<typename T, class Tmask = T>
class TBitFlags
{
public:
	typedef Tmask Flags;
public:
	bool CheckAny(const Tmask mask) const {return CheckBitsAny(m_flags, mask);}
	bool CheckAll(const Tmask mask) const {return CheckBitsMask(m_flags, mask);}
	void Set(const Tmask mask) { SetBits1(m_flags, mask);}
	void Clear(const Tmask mask) {SetBits0(m_flags, mask);}

private:
	T m_flags;
};

#endif //__cplusplus

#endif // BITOPS_H_INCLUDED
