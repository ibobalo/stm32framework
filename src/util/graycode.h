#ifndef GRAYCODE_H_INCLUDED
#define GRAYCODE_H_INCLUDED

#include <stdint.h>

template<class T>
T GrayCodeEncode(T tValue)
{
	T tGrayCoded = tValue ^ (tValue >> 1);
	return tGrayCoded;
}

template<class T>
T GrayCodeDecode(T tGrayCoded)
{
	T tValue = tGrayCoded;
    for (T tMask = tValue >> 1; tMask != 0; tMask = tMask >> 1) {
    	tValue = tValue ^ tMask;
    }
    return tValue;
}

extern const uint8_t g_aui8GrayDecode[256];
static inline uint8_t GrayCodeDecode8(uint8_t ui8GrayCoded)
{
	uint8_t ui8Value = g_aui8GrayDecode[ui8GrayCoded];
	return ui8Value;
}

#endif // GRAYCODE_H_INCLUDED
