#ifndef ADC_AVG_H_INCLUDED
#define ADC_AVG_H_INCLUDED

#include "interfaces/common.h"
#include "util/atomic.h"


template <class T, class valueT = T>
class TValueAverager :
		public IValueReceiver<valueT>
{
public: // IValueReceiver
	virtual void SetValue(valueT tValue) {
		IMPLEMENTS_INTERFACE_METHOD(IValueReceiver<valueT>::SetValue(tValue));
		AtomicAdd(&m_tAveragingSum, (T)tValue);
		AtomicInc(&m_nAveragingCount);
	}
public:
	valueT AverageAccumulated();
	valueT GetAverageValue() const {return m_tAverageValue;};
private:
	volatile T m_tAveragingSum;
	volatile unsigned m_nAveragingCount;
	volatile valueT m_tAverageValue;
};
template <class T, class valueT>
valueT TValueAverager<T, valueT>::AverageAccumulated()
{
	unsigned nCount;
	do {
		T tSum = m_tAveragingSum;
		nCount = m_nAveragingCount;
		if (!nCount) return m_tAverageValue; // nothing to sum
		m_tAverageValue = tSum / nCount;
	} while (!AtomicCmpSet(&m_nAveragingCount, nCount, 0U)); // repeat if changed while calc
	do {
		m_nAveragingCount = 0;
		m_tAveragingSum = 0;
	} while (!AtomicCmpSet(&m_nAveragingCount, 0U, 0U)); // repeat if changed while calc

//	DEBUG_INFO("Average Value:", m_tAverageValue);
	return m_tAverageValue;
}

#endif /* ADC_AVG_H_INCLUDED */
