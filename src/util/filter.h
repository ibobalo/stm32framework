#ifndef FILTER_H_INCLUDED
#define FILTER_H_INCLUDED

#include "core.h"
#include "macros.h"
#include <math.h>

template<typename valueT = float>
class TAvg;
template <typename T, typename TResult = T>
class TBiquadFilter;
template <typename T, typename TResult = T>
class TBiquadFilterLPF;
template <typename T, typename TResult = T>
class TBiquadFilterLPF1;
template <typename T, typename TResult = T>
class TBiquadFilterNotch;
template <typename T, typename TResult = T>
class TBiquadFilterBFP;
template <unsigned nOrder, typename T, typename TResult = T>
class TButterworthFilterLPF;


template<typename valueT>
class TAvg
{
public:
	typedef valueT value_type;
public:
	void Reset() {
		m_vtAverage = 0;
		m_nCount = 0;
	}
	void Accumulate(const value_type& vtValue) {
		value_type d = vtValue - m_vtAverage;
		++m_nCount;
		m_vtAverage += d / m_nCount;
	}
	const value_type& GetAverage() const {return m_vtAverage;}

	inline const TAvg<valueT>& operator += (value_type vtValue) {Accumulate(vtValue); return *this;};
private:

	value_type m_vtAverage;
	value_type m_nCount;
};


template <typename T, typename TResult>
class TBiquadFilter
{
public:
	typedef T sampletype;
	typedef TResult resulttype;
public:
	void Reset(T sample);
	TResult Apply(T sample);
	TResult GetResult() const {return m_y1;};

protected:
	bool Initialize(T b0, T b1, T b2, T a1, T a2);

private:
	T       m_b0 = 0;
	T       m_b1 = 0;
	T       m_b2 = 0;
	T       m_a1 = 0;
	T       m_a2 = 0;
	T       m_x1 = 0;
	T       m_x2 = 0;
	TResult m_y1 = 0;
	TResult m_y2 = 0;
};


template <typename T, typename TResult>
class TBiquadFilterLPF :
		public TBiquadFilter<T, TResult>
{
public:
	bool Initialize(float sampleFreq, float cutoffFreq, float q = M_SQRT1_2);
};

template <typename T, typename TResult>
class TBiquadFilterLPF1 :
		public TBiquadFilter<T, TResult>
{
public:
	bool Initialize(float sampleFreq, float cutoffFreq);
};

template <typename T, typename TResult>
class TBiquadFilterNotch :
		public TBiquadFilter<T, TResult>
{
public:
	bool Initialize(float sampleFreq, float centerFreq, float q);
};

template <typename T, typename TResult>
class TBiquadFilterBPF :
		public TBiquadFilter<T, TResult>
{
public:
	bool Initialize(float sampleFreq, float centerFreq, float q);
};

template <class TCascade, class TCascadeOdd, unsigned nCascades, bool bOdd>
struct TBiquadFilterCascades {};
template <class TCascade, class TCascadeOdd, unsigned nCascades>
struct TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true> // for odd orders
{
public:
	typedef typename TCascade::sampletype sampletype;
	typedef typename TCascade::resulttype resulttype;

	bool Initialize(float sampleFreq, float cutoffFreq);
	void Reset(sampletype sample);
	resulttype Apply(sampletype sample);
	resulttype GetResult() const {return m_aBqCascades[nCascades-1].GetResult();}

protected:
	TCascadeOdd  m_tFirstBqCascade;
	TCascade     m_aBqCascades[nCascades];
};
template <class TCascade, class TCascadeOdd, unsigned nCascades>
struct TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false> // for even orders
{
public:
	typedef typename TCascade::sampletype sampletype;
	typedef typename TCascade::resulttype resulttype;

	bool Initialize(float sampleFreq, float cutoffFreq);
	void Reset(sampletype sample);
	resulttype Apply(sampletype sample);
	resulttype GetResult() const {return m_aBqCascades[nCascades-1].GetResult();}

protected:
	TCascade m_aBqCascades[nCascades];
};

template <unsigned nOrder, typename T, typename TResult>
class TButterworthFilterLPF :
		public TBiquadFilterCascades<TBiquadFilterLPF<T, TResult>, TBiquadFilterLPF1<T, TResult>, nOrder / 2, nOrder % 2>
{
};






template <typename T, typename TResult>
void TBiquadFilter<T, TResult>::Reset(T sample)
{
	m_x1 = sample;
	m_x2 = sample;
	m_y1 = sample;
	m_y2 = sample;
}

template <typename T, typename TResult>
TResult TBiquadFilter<T, TResult>::Apply(T sample)
{
	TResult result = m_b0 * sample + m_b1 * m_x1 + m_b2 * m_x2 - m_a1 * m_y1 - m_a2 * m_y2;
	m_x2 = m_x1;
	m_x1 = sample;
	m_y2 = m_y1;
	m_y1 = result;
	return result;
}

template <typename T, typename TResult>
bool TBiquadFilter<T, TResult>::Initialize(T b0, T b1, T b2, T a1, T a2)
{
	m_b0 = b0;
	m_b1 = b1;
	m_b2 = b2;
	m_a1 = a1;
	m_a2 = a2;
	return true;
}

template <typename T, typename TResult>
bool TBiquadFilterLPF<T, TResult>::Initialize(float sampleFreq, float filterFreq, float q)
{
	if (filterFreq * 2 >= sampleFreq) {
		ASSERTE("cutoff frequency should be less than half of sampleFreq");
		return false;
	}
	double omega = 2.0 * M_PI * filterFreq / sampleFreq;
	double sinOmega = sin(omega);
	double cosOmega = cos(omega);
	double alpha = sinOmega / 2.0L / q;
	double a0 = 1.0L + alpha;
	float b0 = (1.0L - cosOmega) / 2.0L / a0;
	float b1 = (1.0L - cosOmega) / a0;
	float b2 = b0;
	float a1 = -2.0L * cosOmega / a0;
	float a2 = (1.0L - alpha) / a0;

	return TBiquadFilter<T, TResult>::Initialize(b0, b1, b2, a1, a2);
}

template <typename T, typename TResult>
bool TBiquadFilterLPF1<T, TResult>::Initialize(float sampleFreq, float filterFreq)
{
	float omega = 2.0 * M_PI * filterFreq / sampleFreq;
	float sinOmega = sin(omega);
	float cosOmega = cos(omega);
	const float a = 2.0 * sinOmega / (cosOmega + 1.0);   // a = 2 * tan(omega / 2)
	float k = 1.0 / (a + 2.0);
	float b0 = a * k;
	float b1 = b0;
	float b2 = 0;
	float a1 = (a - 2.0) * k;
	float a2 = 0;

	return TBiquadFilter<T, TResult>::Initialize(b0, b1, b2, a1, a2);
}

template <typename T, typename TResult>
bool TBiquadFilterNotch<T, TResult>::Initialize(float sampleFreq, float filterFreq, float q)
{
	float omega = 2.0 * M_PI * filterFreq / sampleFreq;
	float sinOmega = sin(omega);
	float cosOmega = cos(omega);
	float alpha = sinOmega * 0.5 / q;
	float k = T(1.0) / (T(1.0) + alpha);
	float b0 = k;
	float b1 = -T(2.0) * cosOmega * k;
	float b2 =  k;
	float a1 = b1;
	float a2 = (T(1.0) - alpha) * k;

	return TBiquadFilter<T, TResult>::Initialize(b0, b1, b2, a1, a2);
}

template <typename T, typename TResult>
bool  TBiquadFilterBPF<T, TResult>::Initialize(float sampleFreq, float filterFreq, float q)
{
	float omega = 2.0 * M_PI * filterFreq / sampleFreq;
	float sinOmega = sin(omega);
	float cosOmega = cos(omega);
	float alpha = sinOmega * 0.5 / q;
	float k = 1.0 / (1.0 + alpha);
	float b0 = alpha * k;
	float b1 = 0;
	float b2 = -alpha * k;
	float a1 = -2.0 * cosOmega * k;
	float a2 = (1.0 - alpha) * k;

	return TBiquadFilter<T, TResult>::Initialize(b0, b1, b2, a1, a2);
}

template <class TCascade, class TCascadeOdd, unsigned nCascades>
bool TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true>::Initialize(float sampleFreq, float cutoffFreq)
{
	if (cutoffFreq * 2.0f >= sampleFreq) return false;
	if (!m_tFirstBqCascade.Initialize(sampleFreq, cutoffFreq)) return false;
	const float fAngle = M_PI / (nCascades * 2 + 1);
	for (unsigned n = 0; n < nCascades; ++n) {
		const float fTheta = (fAngle + n*fAngle);
		const float q = 0.5f / cos(fTheta);
		if (!m_aBqCascades[n].Initialize(sampleFreq, cutoffFreq, q)) return false;
	}
	return true;
}
template <class TCascade, class TCascadeOdd, unsigned nCascades>
bool TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false>::Initialize(float sampleFreq, float cutoffFreq)
{
	if (cutoffFreq * 2.0f >= sampleFreq) return false;
	const float fAngleHalf = M_PI / (4 * nCascades);
	for (unsigned n = 0; n < nCascades; ++n) {
		const float fTheta = (fAngleHalf + 2*n*fAngleHalf);
		const float q = 0.5f / cos(fTheta);
		if (!m_aBqCascades[n].Initialize(sampleFreq, cutoffFreq, q)) return false;
	}
	return true;
}

template <class TCascade, class TCascadeOdd, unsigned nCascades>
void TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true>::Reset(TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true>::sampletype sample)
{
	m_tFirstBqCascade.Reset(sample);
	for (unsigned n = 0; n < nCascades; ++n) {
		m_aBqCascades[n].Reset(sample);
	}
}
template <class TCascade, class TCascadeOdd, unsigned nCascades>
void TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false>::Reset(TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false>::sampletype sample)
{
	for (unsigned n = 0; n < nCascades; ++n) {
		m_aBqCascades[n].Reset(sample);
	}
}

template <class TCascade, class TCascadeOdd, unsigned nCascades>
typename TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true>::resulttype TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true>::Apply(TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, true>::sampletype sample)
{
	resulttype value = m_tFirstBqCascade.Apply(sample);
	for (unsigned n = 0; n < nCascades; ++n) {
		value = m_aBqCascades[n].Apply(sampletype(value));
	}
	return value;
}
template <class TCascade, class TCascadeOdd, unsigned nCascades>
typename TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false>::resulttype TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false>::Apply(TBiquadFilterCascades<TCascade, TCascadeOdd, nCascades, false>::sampletype sample)
{
	resulttype value = sample;
	for (unsigned n = 0; n < nCascades; ++n) {
		value = m_aBqCascades[n].Apply(sampletype(value));
	}
	return value;
}

#endif // FILTER_H_INCLUDED
