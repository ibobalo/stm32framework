#ifndef PIPED_UART_H_INCLUDED
#define PIPED_UART_H_INCLUDED

#include "util/pipes.h"
#include "hw/mcu/hw_uart.h"


//#define PIPEDUART_DEBUG_INFO(...) DEBUG_INFO("PIPEDUART: " __VA_ARGS__)
#define PIPEDUART_DEBUG_INFO(...) {}

template<unsigned MaxIncomeCacheSize, bool bAutoFlush = true>
class TPipedSerialRxAdapter:
		public CStreamCache<MaxIncomeCacheSize,bAutoFlush>,
		public ISerialRxNotifier
{
	typedef CStreamCache<MaxIncomeCacheSize,bAutoFlush> CParentCache;
public:
	void Init(ISerial* pSerial, IStreamReceiver* pDataReceiver) {
		m_pSerial = pSerial;
		if (pDataReceiver) CParentCache::Bind(pDataReceiver);
		m_pSerial->SetSerialRxNotifier(this);
	}

public: // ISerialRxNotifier
	virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxPart(uiBytesLeft, pRetNextChunk));
		PIPEDUART_DEBUG_INFO("Rx part");
		unsigned uiReservedSize = CParentCache::GetReservedSize();
		CParentCache::AppendReserved(uiReservedSize - uiBytesLeft);
		CParentCache::Flush();
		return ContinueRxChunk(pRetNextChunk);
	}
	virtual bool NotifyRxDone(CIoChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxDone(pRetNextChunk));
		PIPEDUART_DEBUG_INFO("Rx done");
		CParentCache::AppendReserved();
		CParentCache::Flush();
		CParentCache::Reset();
		return ContinueRxChunk(pRetNextChunk);
	}
	virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxIdle(uiBytesLeft, pRetNextChunk));
		unsigned uiReservedSize = CParentCache::GetReservedSize();
		PIPEDUART_DEBUG_INFO("Rx idle :", uiLength);
		CParentCache::AppendReserved(uiReservedSize - uiBytesLeft);
		CParentCache::Flush();
		CParentCache::Reset();
		return ContinueRxChunk(pRetNextChunk);
	}
	virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxOverflow(ccData, pRetNextChunk));
		PIPEDUART_DEBUG_INFO("Rx overflow :", uiLength);
		unsigned uiLength = CParentCache::Push(ccData);
		if (uiLength < ccData.uiLength) {
			Error("RX overflow");
		} else {
			return ContinueRxChunk(pRetNextChunk);
		}
		return false;
	}
	virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxTimeout(uiBytesLeft, pRetNextChunk));
		PIPEDUART_DEBUG_INFO("Rx timeout", uiDataRx);
		unsigned uiReservedSize = CParentCache::GetReservedSize();
		CParentCache::AppendReserved(uiReservedSize - uiBytesLeft);
		CParentCache::Flush();
		return ContinueRxChunk(pRetNextChunk);
	}
	virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxError(uiBytesLeft, pRetNextChunk));
		unsigned uiReservedSize = CParentCache::GetReservedSize();
		CParentCache::AppendReserved(uiReservedSize - uiBytesLeft);
		CParentCache::Flush();
		Error("Rx error");
		return ContinueRxChunk(pRetNextChunk);
	}
private:
	bool ContinueRxChunk(CIoChunk* pRetNextChunk) {
		PIPEDUART_DEBUG_INFO("RXing data...:", uiAvailableSize);
		uint8_t* pReservedData = CParentCache::Reserve();
		if (pReservedData) {
			unsigned uiReservedSize = CParentCache::GetReservedSize();
			pRetNextChunk->Assign(pReservedData, uiReservedSize);
//			m_pUART->StartDataRecv(GetAvailableData(), uiAvailableSize);
			return true;
		}
		return false;
	}
	void Error(const char* reason) {
		PIPEDUART_DEBUG_INFO("Error");
//		++m_nErrors;
	}

	ISerial*    m_pSerial;
	unsigned    m_uiRxTimeoutTicks;
};

template<unsigned MaxOutgoingCacheSize>
class TPipedSerialTxAdapter:
		public IStreamReceiver,
		public CCache<MaxOutgoingCacheSize>,
		public ISerialTxNotifier
{
	typedef CCache<MaxOutgoingCacheSize> CParentCache;
public:
	void Init(ISerial* pSerial) {
		m_pSerial = pSerial;
		m_pSerial->SetSerialTxNotifier(this);
	}

public: // IStreamReceiver
	virtual unsigned Push(const CConstChunk& ccData) {
		IMPLEMENTS_INTERFACE_METHOD(IStreamReceiver::Push(ccData));
		unsigned uiPushedDataLength = CParentCache::AppendToCache(ccData);
		if (!CParentCache::GetLockedDataLength()) {
			CConstChunk ccLockedData = CParentCache::LockDataChunk();
			if (m_pSerial->StartDataSend(ccLockedData)) {
				// ok
			} else {
				CParentCache::ReleaseData();
				uiPushedDataLength = 0;
			}
		}
		return uiPushedDataLength;
	}

public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
		PIPEDUART_DEBUG_INFO("Tx done");
		CParentCache::ReleaseData();
		return ContinueTxChunk(pRetNextChunk);
	}
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
		PIPEDUART_DEBUG_INFO("Tx complete");
		CParentCache::ReleaseData();
		return ContinueTxChunk(pRetNextChunk);
	}
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
		unsigned uiLockedLength = CParentCache::GetLockedDataLength();
		CParentCache::ReleaseData(uiLockedLength - uiBytesLeft);
		CParentCache::Reset();
		Error("Tx timeout");
		return ContinueTxChunk(pRetNextChunk);
	}
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk) {
		IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
		unsigned uiLockedLength = CParentCache::GetLockedDataLength();
		CParentCache::ReleaseData(uiLockedLength - uiBytesLeft);
		CParentCache::Reset();
		Error("Tx error");
		return ContinueTxChunk(pRetNextChunk);
	}
private:
	bool ContinueTxChunk(CConstChunk* pRetNextChunk) {
		const uint8_t* pLockedData = CParentCache::LockData();
		if (pLockedData) {
			unsigned uiLockedLength = CParentCache::GetLockedDataLength();
			m_pSerial->SetTxByteTimeout(m_uiTxTimeoutTicks);
			pRetNextChunk->Assign(pLockedData, uiLockedLength);
//			m_pUART->StartDataSend(GetCachedData(), uiCachedSize);
			PIPEDUART_DEBUG_INFO("TXing pkt...:", uiLockedLength);
			return true;
		}
		return false;
	}
	void Error(const char* reason) {
		PIPEDUART_DEBUG_INFO("Error");
//		++m_nErrors;
	}

	ISerial*    m_pSerial;
	unsigned    m_uiTxTimeoutTicks;
};

#endif // PIPED_UART_H_INCLUDED
