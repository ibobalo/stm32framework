#ifndef HW_I2C_INTERFACES_H_INCLUDED
#define HW_I2C_INTERFACES_H_INCLUDED

#include "util/chained_chunks.h"
#include <stdint.h>

typedef enum {I2C_ERROR_TIMEOUT, I2C_ERROR_ACK, I2C_ERROR_BUS, I2C_ERROR_ARB, I2C_ERROR_DMA, I2C_ABORT} EI2CErrorType;

class ISlaveI2C;
class IDeviceI2C;
class II2C;

class ISlaveI2C
{
public:
	typedef EI2CErrorType ErrorType;
public:
//	virtual ~ISlaveI2C();

public: // ISlaveI2C
	virtual unsigned GetI2CAddress() const = 0; /* address is 7 bit */
	virtual bool GetDataToSend(const CChainedConstChunks** ppRetChunksToSend) = 0;
	virtual bool GetBufferToReceive(const CChainedIoChunks** ppRetChunksToRecv) = 0;

	virtual void OnI2CSessionDone(unsigned nBytesCompleted) = 0;
	virtual void OnI2CError(EI2CErrorType e) = 0;
};

class IDeviceI2C
{
public:
	typedef EI2CErrorType ErrorType;
public:
//	virtual ~IDeviceI2C();

public: // IDeviceI2C
	virtual unsigned GetI2CAddress() const = 0; /* address is 7 bit */
	virtual bool GetI2CPacket(
			const CChainedConstChunks** ppRetChunksToSend,
			const CChainedIoChunks**    ppRetChunksToRecv
	) = 0;
	virtual void OnI2CDone() = 0;
	virtual void OnI2CError(EI2CErrorType e) = 0;
	virtual void OnI2CIdle() = 0;
};


class II2C
{
public:
//	virtual ~II2C();
public: // II2C
	virtual void Init(unsigned uiClockSpeed) = 0;
	virtual void Deinit() = 0;
// slave:
	virtual void AssignSlaveDevice(ISlaveI2C* pSlaveI2CDevice1 = NULL, ISlaveI2C* pSlaveI2CDevice2 = NULL) = 0;
// master:
	virtual void Queue_I2C_Session(IDeviceI2C* pDeviceI2C) = 0;
	virtual void Cancel_I2C_Session(IDeviceI2C* pDeviceI2C) = 0;
	virtual void RequestI2CIdleNotify(IDeviceI2C* pDeviceI2C) = 0;
};

#endif /* HW_I2C_INTERFACES_H_INCLUDED */
