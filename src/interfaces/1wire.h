#ifndef _1WIRE_H_INCLUDED
#define _1WIRE_H_INCLUDED

#include <stdint.h>
#include "util/callback.h"
#include "tick_timesource.h"

class IDevice1WMaster
{
public:
	typedef Callback<void (bool)> TDoneNotification;
	typedef Callback<void (bool, uint8_t)> TReadNotification;
	enum {
		TRIPLET_FIRST_BIT   = 0x01,      //  slave address bit
		TRIPLET_SECOND_BIT  = 0x02,      // ~slave address bit (0x03 mean
		TRIPLET_CHOSEN_BIT  = 0x04,      // chosen direction bit
	} TRIPLET_RESULT_BITS;

public: // IDevice1WMaster
	virtual bool IsReady() const = 0;
	virtual bool BusResetDetect(TDoneNotification cbDone) = 0;
	virtual bool TxBit(bool bit, TDoneNotification cbDone) = 0;
	virtual bool TxBitPU(bool bit, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone) = 0;
	virtual bool TxByte(uint8_t uiByte, TDoneNotification cbDone) = 0;
	virtual bool TxBytePU(uint8_t uiByte, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone) = 0;
	virtual bool TxBytes(const void* pData, unsigned nLength, TDoneNotification cbDone) = 0;
	virtual bool TxBytesPU(const void* pData, unsigned nLength, ticktime_delta_t tdStrongPUDuration, TDoneNotification cbDone) = 0;
	virtual bool RxBit(TReadNotification cbReadDone) = 0;
	virtual bool RxByte(TReadNotification cbReadDone) = 0;
	virtual bool RxBytes(void* pBuffer, unsigned nLength, TDoneNotification cbDone) = 0;
	virtual bool Triplet(bool bWriteBitIf00, TReadNotification cbReadDone) = 0;
};

class IDevice1WROM
{
public:
	typedef Callback<void (bool)> TDoneNotification;

public: // IDevice1WROM
	virtual bool SelectID(uint64_t ID) = 0;
	virtual void GetSelectedID(uint64_t& retID) const = 0;
	virtual bool GetDetectedID(uint64_t& retID) const = 0;
	virtual bool Detect(TDoneNotification cbDone) = 0;
	virtual bool Select(TDoneNotification cbDone) = 0;
	virtual bool FindFirst(TDoneNotification cbDone) = 0;
	virtual bool FindNext(TDoneNotification cbDone) = 0;
};


#endif /* _1WIRE_H_INCLUDED */
