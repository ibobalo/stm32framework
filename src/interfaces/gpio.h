#ifndef HW_GPIO_INTERFACES_H_INCLUDED
#define HW_GPIO_INTERFACES_H_INCLUDED

#include "interfaces/notify.h"
#include "interfaces/common.h"
#include <stdint.h>

class IDigitalInNotifier
{
public: // IDigitalInNotifier
	virtual void OnRise() = 0;
	virtual void OnFall() = 0;
};

class IDigitalIn :
		virtual public IValueSource<bool>
{
};

class IDigitalInProvider :
		public IDigitalIn,
		public IValueProvider<bool>
{
public: // IDigitalInProvider
	virtual void SetNotifier(IDigitalInNotifier* pNotifier, uint8_t uiGroupPriority = 0, uint8_t uiSubPriority = 0, bool bOnRise = true, bool bOnFall = true) = 0;
};


class IDigitalOut :
		virtual public IValueReceiver<bool>,
		virtual public IValueSource<bool>
{
public: // IDigitalOut
	virtual void Set() = 0;
	virtual void Reset() = 0;
	virtual void Toggle() = 0;
};

#endif /* HW_GPIO_INTERFACES_H_INCLUDED */
