#ifndef HW_SERIAL_INTERFACES_H_INCLUDED
#define HW_SERIAL_INTERFACES_H_INCLUDED

#include "util/chained_chunks.h"


class ISerialRxNotifier
{
public: // ISerialRxNotifier
	virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) = 0; //returns true in next chunk set
	virtual bool NotifyRxDone(CIoChunk* pRetNextChunk) = 0; //returns true in next chunk set
	virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) = 0;
	virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) = 0;
	virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) = 0;
	virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk) = 0;
};

class ISerialTxNotifier
{
public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk) = 0; // transmit of last byte still in progress
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk) = 0; // last byte transmited, TX line idle
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk) = 0;
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk) = 0;
};
typedef struct _TSerialConnectionParams
{
public:
	typedef enum {IF_STM32F4(stop0_5,) stop1, stop2, stop1_5} StopBits;
	typedef enum {parityNo, parityEven, parityOdd} ParityBits;
public:
//	_TSerialConnectionParams (unsigned a_uiBitRate, StopBits a_eStopBits, ParityBits a_eParityBits) :
//		uiBitRate(a_uiBitRate), eStopBits(a_eStopBits), eParityBits(a_eParityBits) {}
	unsigned    uiBaudRate;
	StopBits    eStopBits;
	ParityBits  eParityBits;
} TSerialConnectionParams;

class ISerialConnectionParamsNotifier
{
public: // ISerialConnectionParamsNotifier
	virtual void NotifyConnectionParams(const TSerialConnectionParams* tParams) = 0;
};

class ISerial
{
public:
	typedef TSerialConnectionParams ConnectionParams;
	typedef TSerialConnectionParams::StopBits StopBits;
	typedef TSerialConnectionParams::ParityBits ParityBits;
public: // ISerial
	virtual void SetSerialRxNotifier(ISerialRxNotifier* pNotifier) = 0;
	virtual void SetSerialTxNotifier(ISerialTxNotifier* pNotifier) = 0;
	virtual void SetConnectionParams(const TSerialConnectionParams* params) = 0;
	virtual bool StartDataSend(const CConstChunk& TxChunk) = 0;
	virtual void StartDataRecv(const CIoChunk& RxChunk) = 0;
	virtual void Control(bool bEnable) = 0;
	virtual void ControlRx(bool bEnable) = 0;
	virtual void ControlTx(bool bEnable) = 0;
	virtual void SetRxByteTimeout(unsigned uiByteTimeoutTicks) = 0;
	virtual void SetTxByteTimeout(unsigned uiByteTimeoutTicks) = 0;
	virtual unsigned GetBaudrate() const = 0;
	virtual void SetBaudrateNotifier(ISerialConnectionParamsNotifier* pNotifier) = 0;
};

#endif /* HW_SERIAL_INTERFACES_H_INCLUDED */
