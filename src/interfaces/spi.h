#ifndef HW_SPI_INTERFACES_H_INCLUDED
#define HW_SPI_INTERFACES_H_INCLUDED

#include "util/chained_chunks.h"
#include <stdint.h>

class ISPI;
class IDeviceSPI;

class ISPI
{
public:
	typedef enum {
		MODE0,  MODE_0_0 = MODE0,
		MODE1,  MODE_0_1 = MODE1,
		MODE2,  MODE_1_0 = MODE2,
		MODE3,  MODE_1_1 = MODE3,
	} ESPIMode;
	typedef enum {
		MSB_1st,
		LSB_1st
	} EBitsOrder;
public: // ISPI
	virtual void Init(unsigned uiSpeed) = 0;
	virtual void Deinit() = 0;
	virtual bool Start_SPIx_Session(IDeviceSPI* pDeviceSPI, const CChainedConstChunks* pHeadTxChunk, const CChainedIoChunks* pHeadRxChunk) = 0;
	virtual void RequestSPIIdleNotify(IDeviceSPI* pDeviceSPI) = 0;
};

class IDeviceSPI
{
public:
//	virtual ~IDeviceSPI();
public: // IDeviceSPI
	virtual bool GetSPIParams(unsigned &uiMaximumBitrate, ISPI::ESPIMode &eMode, ISPI::EBitsOrder &eBitsOrder, unsigned &nBitsCount) const = 0;
	virtual void Select() = 0;
	virtual void Release() = 0;
	virtual void OnSPIDone() = 0;
	virtual void OnSPIError() = 0;
	virtual void OnSPIIdle() = 0;
};

#endif /* HW_SPI_INTERFACES_H_INCLUDED */
