#ifndef _INTERFACES_DEVREGS_H_INCLUDED
#define _INTERFACES_DEVREGS_H_INCLUDED

#include <stdint.h>

class IDeviceRegs
{
public: // IDeviceRegs
	virtual unsigned GetRegValue(unsigned uiRegNumber, uint8_t* pRetDataBuffer, unsigned uiBufferCapacity) const = 0;
	virtual unsigned SetRegValue(unsigned uiRegNumber, const uint8_t* pDataBuffer, unsigned uiSize) = 0; // return: 0 - error, <=uiSize - success, uiSize+1 - more data required
};



#endif /* _INTERFACES_DEVREGS_H_INCLUDED */
