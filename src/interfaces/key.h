#ifndef KEY_INTERFACES_H_INCLUDED
#define KEY_INTERFACES_H_INCLUDED

#include <stddef.h> // NULL

template <class ticktime_t>
class IKeyHandler
{
public: // IKeyHandler
	virtual void OnButtonPress(ticktime_t ttTime) = 0;
	virtual void OnButtonRelease(ticktime_t ttTime) = 0;
	virtual void OnButtonHold(ticktime_t ttTime) = 0;
};

template <class ticktime_t>
class IKey
{
public: // IKey
	virtual bool IsPressed() const = 0;
	virtual void AssignKeyHandler(IKeyHandler<ticktime_t>* pHandler) = 0;
	virtual void UnassignKeyHandler() = 0;

};

template <class ticktime_t>
class IKeyboardHandler
{
public:
	typedef enum {KE_NONE, KE_PRESS, KE_RELEASE, KE_HOLD} EKeyEvent;
public: // IKeyboardHandler
	virtual void OnKeyEvent(unsigned nKey, EKeyEvent eEvent, ticktime_t ttTime) = 0;
};

template <class ticktime_t>
class IKeyboard
{
public: // IKeyboard
	virtual void Init() = 0;
	virtual void SetAllowedKeysMask(uint32_t uiMaskAllowed) = 0;
	virtual void Control(bool bEnable) = 0;

	virtual unsigned GetKeysCount() const = 0;
	virtual IKey<ticktime_t>* GetKeyInterface(unsigned nKey) = 0;
	virtual void AssignKeyboardHandler(IKeyboardHandler<ticktime_t>* pGroupHandler) = 0;
};

#endif // KEY_INTERFACES_H_INCLUDED
