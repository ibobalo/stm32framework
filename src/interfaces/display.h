#ifndef DISPLAY_INTERFACES_H_INCLUDED
#define DISPLAY_INTERFACES_H_INCLUDED

template<class TCoordinates, class TColor>
class IDisplayDevice
{
public: // IDisplayDevice<TCoordinates, TColor>
	virtual void Clear(TColor tColor) = 0;
	virtual void Update() = 0;
};

template<class TCoordinates, class TColor>
class ITextDisplayDevice :
		virtual public IDisplayDevice<TCoordinates, TColor>
{
public: // ITextDisplayDevice<TCoordinates, TColor>
	virtual const TCoordinates& GetTextSize() const = 0;
	virtual const TCoordinates& GetTextXY() const = 0;
	virtual void GotoXY(const TCoordinates& tPoint1) = 0;
	virtual void PutChar(char ch, bool tColor) = 0;
	virtual void PutString(const char* s, bool tColor) = 0;
};

template<class TCoordinates, class TColor>
class IDrawDisplayDevice :
		virtual public IDisplayDevice<TCoordinates, TColor>
{
public: // IDrawDisplayDevice<TCoordinates, TColor>
	virtual const TCoordinates& GetSize() const = 0;
	virtual void DrawPixel(const TCoordinates& tCoords, TColor tColor) = 0;
	virtual void DrawLine(const TCoordinates& tPoint1, const TCoordinates& tPoint2, bool tColor) = 0;
	virtual void DrawRect(const TCoordinates& tPosition, const TCoordinates& tSize, bool tColor) = 0;
};

class CCoordinates
{
public:
	int x;
	int y;
};

typedef IDisplayDevice<CCoordinates, bool> IDisplayDeviceMonochrome;
typedef ITextDisplayDevice<CCoordinates, bool> ITextDisplayDeviceMonochrome;
typedef IDrawDisplayDevice<CCoordinates, bool> IDrawDisplayDeviceMonochrome;

#endif //DISPLAY_INTERFACES_H_INCLUDED
