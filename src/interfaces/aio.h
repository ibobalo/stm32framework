#ifndef HW_AIO_INTERFACES_H_INCLUDED
#define HW_AIO_INTERFACES_H_INCLUDED

#include "interfaces/notify.h"
#include "interfaces/common.h"
#include <stdint.h>

class IADCSampleNotifier;
class IADCSequenceNotifier;
class IADCBufferNotifier;
class IADCErrorNotifier;
class IADC;
class IAnalogIn;
class IDAC;
class IAnalogOut;

class IADCSampleNotifier
{
public: // IADCSampleNotifier
	virtual void NotifyADCSample() = 0;
};
class IADCSequenceNotifier
{
public: // IADCSequenceNotifier
	virtual void NotifyADCSequence() = 0;
};
class IADCBufferNotifier
{
public: // IADCBufferNotifier
	virtual void NotifyADCBuffer() = 0;
};
class IADCErrorNotifier
{
public: // IADCErrorNotifier
	virtual void NotifyADCError() = 0;
};

class IAnalogIn :
		public IValueSource<uint16_t>
{
public: // IAnalogIn
	virtual value_type GetMaxValue() const = 0;
};
class IAnalogInProvider :
		virtual public IAnalogIn,
		virtual public IValueProvider<IAnalogIn::value_type>
{
public:	using value_type = IAnalogIn::value_type;
};

class IAnalogOut :
		public IValueReceiver<uint16_t>
{
public: // IAnalogOut
	virtual value_type GetMaxValue() const = 0;
};

class IADC
{
public: //IADC
	virtual void AttachAIN(unsigned ChIndex) = 0;
	virtual void DetachAIN(unsigned ChIndex) = 0;

	virtual void SetSampleNotifier(IADCSampleNotifier* pNotifier) = 0;
	virtual void SetSequenceNotifier(IADCSequenceNotifier* pNotifier) = 0;
	virtual void SetBufferNotifier(IADCBufferNotifier* pNotifier) = 0;
	virtual void SetErrorNotifier(IADCErrorNotifier* pNotifier) = 0;

	virtual void SetSamplesBuffer(volatile uint16_t* pBuffer, unsigned uiMaxSamplesCount) = 0;

	virtual void InitAINs() = 0;
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void DeinitAINs() = 0;

	virtual unsigned GetSamplesCount() const = 0;
	virtual volatile const uint16_t* GetSamples() const = 0;
	virtual uint16_t GetSample(unsigned nScanIndex) const = 0;
};

#endif /* HW_AIO_INTERFACES_H_INCLUDED */
