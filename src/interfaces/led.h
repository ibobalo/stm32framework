#ifndef LED_INTERFACES_H_INCLUDED
#define LED_INTERFACES_H_INCLUDED

#include <stddef.h> // NULL
#include "hw/tick_timesource.h"

class ILed
{
public: // ILed
	virtual void Set() = 0;
	virtual void Clear() = 0;
	virtual void Blink(unsigned nCount = 0) = 0;
	virtual void FastBlink(unsigned nCount = 0) = 0;
	virtual void Pulse(unsigned nDuration) = 0;
// aliases
	template<int nDuration> void Pulse() {Pulse(nDuration);};
};

class ILeds
{
public: // ILeds
	virtual void Init() = 0;

	virtual ILed* AcquireLedInterface(unsigned nIndex) = 0;
	virtual void ReleaseLedInterface(unsigned nIndex) = 0;
// indexed control
//	virtual void SetKbdLed(unsigned nIndex) = 0;
//	virtual void ClearKbdLed(unsigned nIndex) = 0;
//	virtual void BlinkKbdLed(unsigned nIndex) = 0;
//	virtual void FastBlinkKbdLed(unsigned nIndex) = 0;
//	virtual void PulseKbdLed(unsigned nIndex, unsigned nPulses) = 0;

// aliases
//	template<int nIndex> void ApplyKbdLed(bool bState) {if(bState) {SetKbdLed(nIndex); } else {ClearKbdLed(nIndex); }};
//	template<int nIndex> void SetKbdLed()       {SetKbdLed(nIndex);};
//	template<int nIndex> void ClearKbdLed()     {ClearKbdLed(nIndex);};
//	template<int nIndex> void BlinkKbdLed()     {BlinkKbdLed(nIndex);};
//	template<int nIndex> void FastBlinkKbdLed() {FastBlinkKbdLed(nIndex);};
//	template<int nIndex> void PulseKbdLed()     {PulseKbdLed(nIndex, 1);};
//	template<int nIndex,int nPulses> void PulseKbdLed()     {PulseKbdLed(nIndex, nPulses);};
};

#endif // LED_INTERFACES_H_INCLUDED
