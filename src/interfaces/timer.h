#ifndef TIMER_INTERFACES_H_INCLUDED
#define TIMER_INTERFACES_H_INCLUDED

#include <stdint.h>
#include "interfaces/common.h"

//extern
class ITimerChannel;

class ITimerNotifier
{
public: // ITimerNotifier
	virtual void NotifyTimer() = 0;
};

class ITimerChannelNotifier
{
public: // ITimerChannelNotifier
	virtual void NotifyTimerChannel(unsigned nChannel) = 0;
};

class ITimer
{
public: // ITimer
	virtual void SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0) = 0;
	virtual void SetPeriodClks(uint64_t ullPeriodClks) = 0;
	virtual void SetPrescaler(unsigned ulPrescaler) = 0;
	virtual void SetPeriod(unsigned ulPeriod) = 0;
	virtual void SetCounter(unsigned uiCounter) = 0;
	virtual unsigned GetCounter() const = 0;
	virtual unsigned GetCounterMax() const = 0;
	virtual unsigned GetPeriod() const = 0;
	virtual unsigned ClksToCounts(unsigned uiClks) const = 0;
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void Idle() = 0;
	virtual void Work() = 0;
};

class ITimerAdv :
		public virtual ITimer
{
public: // ITimerAdv
	virtual void SetPulsesCount(unsigned nPulsesCount) = 0;
	virtual void SetDeadtimeClks(unsigned uiDeadtimeClks) = 0;
	virtual void UseCustomIdleStates(bool bResetOnUpdate = false) = 0;
};

class ITimerChannel :
		public IValueSource<unsigned>,    // actual out power
		public IValueReceiver<unsigned>   // command value
{
public: // ITimerChannel
	virtual void SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0) = 0;
	virtual void Enable(bool bEnable = true) = 0;
	virtual void SetOutputInvertion(bool bInvert) = 0;
	virtual unsigned GetTimerPeriod() const = 0;
	virtual unsigned GetTimerCounterMax() const = 0;
};

class ITimerChannelAdv :
		public virtual ITimerChannel
{
public: // ITimerChannelAdv
	virtual void SetPulsesCount(unsigned nPulsesCount) = 0;
	virtual void SetTimerDeadtimeClks(unsigned uiDeadtimeClks) = 0;
};

inline void SetTimerChannelScaled(ITimerChannel* pTimerChannel, float dValue) {unsigned uiValue = pTimerChannel->GetTimerPeriod() * dValue; pTimerChannel->SetValue(uiValue);}

#endif /* TIMER_INTERFACES_H_INCLUDED */
