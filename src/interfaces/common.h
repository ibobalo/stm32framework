#ifndef COMMON_INTERFACES_H_INCLUDED
#define COMMON_INTERFACES_H_INCLUDED

#include <stddef.h> // NULL

template <class T>
class IValueSource
{
public:
	typedef T value_type;
public: // IValueSource
	virtual value_type GetValue() const = 0;
};

template <class T>
class IValueReceiver
{
public:
	typedef T value_type;
public: // IValueReceiver
	virtual void SetValue(value_type tValue) = 0;
};

template <class T>
class IValueRequester
{
public:
	typedef T value_type;
public: // IValueRequester
	virtual void SetValueSourceInterface(const IValueSource<value_type>* pValueSource) = 0;
};

template <class T>
class IValueProvider
{
public:
	typedef T value_type;
public: // IValueProvider
	virtual void SetValueTargetInterface(IValueReceiver<value_type>* pValueTarget) = 0;
};

template<class T, class Tout=T>
class ITransformer
{
public:
	typedef T value_type;
public: // ITransformer
	virtual Tout Transform(const value_type tInValue) const = 0;
};

#endif /* COMMON_INTERFACES_H_INCLUDED */
