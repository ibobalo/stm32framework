#ifndef NOTIFY_H_INCLUDED
#define NOTIFY_H_INCLUDED

#include "util/core.h"

template<class TKey=void*, const int Instance=0>
class IEventNotificationTarget
{
public: // IEventNotificationTarget
	virtual void OnEvent(TKey hKey) = 0;
};

template<class TKey=void*, const int MAX_TARGETS=1, const int Instance=0>
class TEventNotifier
{
public:
	void Init() {m_nNotificationTargetsCount = 0;};
	void SetNotificationTarget(IEventNotificationTarget<TKey, Instance>* pNotificationTarget, TKey hKey) {
		ASSERTE(m_nNotificationTargetsCount < MAX_TARGETS);
		ASSERTE(pNotificationTarget);
		m_pNotificationTarget[m_nNotificationTargetsCount] = pNotificationTarget;
		m_pNotificationTargetKeys[m_nNotificationTargetsCount] = hKey;
		++m_nNotificationTargetsCount;
	}
protected:
	void NotifyEvent() {
		for (unsigned n = 0; n < m_nNotificationTargetsCount; ++n) {
			IEventNotificationTarget<TKey, Instance>* pNotificationTarget = m_pNotificationTarget[n];
			ASSERTE(pNotificationTarget);
			pNotificationTarget->OnEvent(m_pNotificationTargetKeys[n]);
		}
	}
private:
	unsigned                                      m_nNotificationTargetsCount;
	IEventNotificationTarget<TKey, Instance>*     m_pNotificationTarget[MAX_TARGETS];
	TKey                                          m_pNotificationTargetKeys[MAX_TARGETS];
};

template<class T, class TKey=void*, const int Instance=0>
class IValueNotificationTarget
{
public:
	typedef T CValueType;
public: // IValueNotificationTarget
	virtual void OnValue(const CValueType& newValue, TKey hKey) = 0;
};


template<class T, class TKey=void*, const int MAX_TARGETS=1, const int Instance=0>
class TValueNotifier
{
public:
	typedef T CValueType;
public:
	void Init() {m_nNotificationTargetsCount = 0;};
	void SetNotificationTarget(IValueNotificationTarget<CValueType, TKey, Instance>* pNotificationTarget, TKey hKey) {
		ASSERTE(m_nNotificationTargetsCount < MAX_TARGETS);
		ASSERTE(pNotificationTarget);
		m_pNotificationTarget[m_nNotificationTargetsCount] = pNotificationTarget;
		m_pNotificationTargetKeys[m_nNotificationTargetsCount] = hKey;
		++m_nNotificationTargetsCount;
	}
protected:
	void NotifyValue(const CValueType& newValue) {
		for (unsigned n = 0; n < m_nNotificationTargetsCount; ++n) {
			IValueNotificationTarget<CValueType, TKey, Instance>* pNotificationTarget = m_pNotificationTarget[n];
			ASSERTE(pNotificationTarget);
			pNotificationTarget->OnValue(newValue, m_pNotificationTargetKeys[n]);
		}
	}
private:
	unsigned                                               m_nNotificationTargetsCount;
	IValueNotificationTarget<CValueType, TKey, Instance>*  m_pNotificationTarget[MAX_TARGETS];
	TKey                                                   m_pNotificationTargetKeys[MAX_TARGETS];
};

template<class T, class TKey=void*, const int Instance=0>
class IValueChangedNotificationTarget
{
public:
	typedef T CValueType;
public: // IValueChangedNotificationTarget
	virtual void OnValueChanged(const CValueType& oldValue, const CValueType& newValue, TKey hKey) = 0;
};


template<class T, class TKey=void*, const int MAX_TARGETS=1, const int Instance=0>
class TValueChangeNotifier
{
public:
	typedef T CValueType;
public:
	void Init() {m_nNotificationTargetsCount = 0;};
	void SetNotificationTarget(IValueChangedNotificationTarget<CValueType, TKey, Instance>* pNotificationTarget, TKey hKey) {
		ASSERTE(m_nNotificationTargetsCount < MAX_TARGETS);
		ASSERTE(pNotificationTarget);
		m_pNotificationTarget[m_nNotificationTargetsCount] = pNotificationTarget;
		m_pNotificationTargetKeys[m_nNotificationTargetsCount] = hKey;
		++m_nNotificationTargetsCount;
	}
protected:
	void NotifyValueChange(const CValueType& oldValue, const CValueType& newValue) {
		for (unsigned n = 0; n < m_nNotificationTargetsCount; ++n) {
			IValueChangedNotificationTarget<CValueType, TKey, Instance>* pNotificationTarget = m_pNotificationTarget[n];
			ASSERTE(pNotificationTarget);
			pNotificationTarget->OnValueChanged(oldValue, newValue, m_pNotificationTargetKeys[n]);
		}
	}
private:
	unsigned                                                      m_nNotificationTargetsCount;
	IValueChangedNotificationTarget<CValueType, TKey, Instance>*  m_pNotificationTarget[MAX_TARGETS];
	TKey                                                          m_pNotificationTargetKeys[MAX_TARGETS];
};

#endif // NOTIFY_H_INCLUDED
