#include "stdafx.h"
#include "btn_exti.h"

void CBtnExti::Init(bool bInverted, COnChangeCallback cbOnChange)
{
	m_cbOnChange = cbOnChange;
	m_bInverted = bInverted;
	BTN::Acquire(bInverted, false);
	bool bBtnState = BTN::GetValue();
	m_cbOnChange(bBtnState != bInverted);
	BTN::SetNotifier(this);
}

/*virtual*/
void CBtnExti::OnRise()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInNotifier::OnRise());
	DEBUG_INFO("BTN Rise");
	m_cbOnChange(!m_bInverted);
}

/*virtual*/
void CBtnExti::OnFall()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInNotifier::OnFall());
	DEBUG_INFO("BTN Fall");
	m_cbOnChange(m_bInverted);
}


CBtnExti g_BtnExti;
