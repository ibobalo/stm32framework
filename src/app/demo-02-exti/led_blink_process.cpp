#include "stdafx.hpp"
#include "led_blink_process.h"

void CLedBlinkProcess::Init()
{
	CTickProcess::Init(&g_TickProcessScheduller);

	LED::Acquire(false);
}

void CLedBlinkProcess::EnableBlinking(bool bEnable)
{
	DEBUG_INFO("Enable:", bEnable ? 1 : 0);
	if (bEnable) {
		LED::Set();
		ContinueDelay(200); INDIRECT_CALL(SingleStep(0));
	} else {
		Cancel();
		LED::Reset();
	}
}

void CLedBlinkProcess::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));

	LED::Toggle();

	ContinueDelay(200);
}


CLedBlinkProcess g_LedBlinkProcess;
