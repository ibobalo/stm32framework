#include "stdafx.h"
#include "serial_hello.h"

void CSerialHello::Init()
{
	UART::Acquire();

	const TSerialConnectionParams params = {
    		.uiBaudRate = 9600,
			.eStopBits = TSerialConnectionParams::StopBits::stop1,
			.eParityBits = TSerialConnectionParams::ParityBits::parityNo
    };

	UART::SetConnectionParams(&params);
	UART::SetSerialTxNotifier(this);

	SendHelloMsg();
}

void CSerialHello::SendHelloMsg()
{
	CConstChunk TxChunk;
	TxChunk.Assign(reinterpret_cast<const uint8_t*>("\r\nHello world\r\n"), 15);
	UART::StartDataSend(TxChunk);
}

/*virtual*/
bool CSerialHello::NotifyTxDone(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
	return false;
}

/*virtual*/
bool CSerialHello::NotifyTxComplete(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
	// ignore
	return false;
}

/*virtual*/
bool CSerialHello::NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
	return false;
}

/*virtual*/
bool CSerialHello::NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
	return false;
}


CSerialHello g_SerialHello;
