#pragma once

#include "hw/hw.h"
#include "util/callback.h"

class CSerialHello :
		public ISerialTxNotifier
{
public:
	typedef Callback<void (uint8_t)> COnRxCallback;

public:
	void Init();
	void SendHelloMsg();

public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);

private:
	using UART = CHW_Brd::CSerial_1;
};

extern CSerialHello g_SerialHello;
