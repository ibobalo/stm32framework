#pragma once

#include "hw/hw.h"
#include "util/callback.h"

class CBtnExti :
	public IDigitalInNotifier
{
public:
	typedef Callback<void (bool)> COnChangeCallback;

public:
	void Init(bool bInverted, COnChangeCallback cbOnChange);

public: // IDigitalInNotifier
	virtual void OnRise() override;
	virtual void OnFall() override;

private:
	using BTN = CHW_Brd::CDigitalIn_BTN;

	COnChangeCallback          m_cbOnChange;
	bool                       m_bInverted;
};

extern CBtnExti g_BtnExti;
