#include "stdafx.h"
#include "led_blink_tim.h"

void CLedBlinkTimer::Init()
{
	LED::Acquire(false);
	TIM::Acquire();
	TIM::SetNotifier(this, 1, 1);
	// 4 Hz
	uint64_t ulPeriodClks = CHW_Clocks::GetCLKFrequency() / 4;
	TIM::SetPeriodClks(ulPeriodClks);
}

void CLedBlinkTimer::EnableBlinking(bool bEnable)
{
	DEBUG_INFO("Enable:", bEnable ? 1 : 0);
	if (bEnable) {
		LED::Set();
		TIM::SetCounter(0);
		TIM::Start(); INDIRECT_CALL(NotifyTimer());
	} else {
		TIM::Stop();
		LED::Reset();
	}
}

/*virtual*/
void CLedBlinkTimer::NotifyTimer()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());
	DEBUG_INFO("TIM");
	LED::Toggle();
}


CLedBlinkTimer g_LedBlinkTimer;
