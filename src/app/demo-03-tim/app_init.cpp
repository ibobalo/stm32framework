#include "stdafx.h"
#include "btn_exti.h"
#include "led_blink_tim.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	constexpr bool bButtonToGND = (
#ifdef USE_BOARD_nucleof072rb
		true
#else
		false
#endif
	);

	g_LedBlinkTimer.Init();
	CBtnExti::COnChangeCallback cb = BIND_MEM_CB(&CLedBlinkTimer::EnableBlinking, &g_LedBlinkTimer);
	g_BtnExti.Init(bButtonToGND, cb);
}

} // extern "C"
