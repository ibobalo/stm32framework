#pragma once

#include "hw/hw.h"

class CLedBlinkTimer :
		public ITimerNotifier
{
public:
	void Init();
	void EnableBlinking(bool bEnable);
public: // ITimerNotifier
	virtual void NotifyTimer() override;

private:
	using LED = CHW_Brd::CDigitalOut_LED;
	using TIM = CHW_Brd::CTimer_1;
};

extern CLedBlinkTimer g_LedBlinkTimer;
