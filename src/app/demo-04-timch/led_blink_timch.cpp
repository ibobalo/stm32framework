#include "stdafx.h"
#include "led_blink_timch.h"

void CLedBlinkTimerCh::Init()
{
	LED::Acquire(false);
	TIM::Acquire();
	TIM::SetNotifier(this, 1, 1);  INDIRECT_CALL(NotifyTimer());
	// 4 Hz
	uint64_t ulPeriodClks = CHW_Clocks::GetCLKFrequency() / 4;
	TIM::SetPeriodClks(ulPeriodClks);
	// short bursts
	unsigned uiLedOnDurationCnts = TIM::GetPeriod() / 8;
	TIMCH::Acquire(uiLedOnDurationCnts);
	TIMCH::SetNotifier(this, 1, 0);  INDIRECT_CALL(NotifyTimerChannel(0));
}

void CLedBlinkTimerCh::EnableBlinking(bool bEnable)
{
	DEBUG_INFO("Enable:", bEnable ? 1 : 0);
	if (bEnable) {
		LED::Set();
		TIM::SetCounter(0);
		TIM::Start(); INDIRECT_CALL(NotifyTimer(), NotifyTimerChannel(0));
	} else {
		TIM::Stop();
		LED::Reset();
	}
}

/*virtual*/
void CLedBlinkTimerCh::NotifyTimer()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());
	DEBUG_INFO("TIM");
	LED::SetValue(TIM::GetCounter() < TIMCH::GetValue());
}

/*virtual*/
void CLedBlinkTimerCh::NotifyTimerChannel(unsigned nChannel)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannelNotifier::NotifyTimerChannel(nChannel));
	DEBUG_INFO("TIMCH:", nChannel);
	LED::SetValue(TIM::GetCounter() < TIMCH::GetValue());
}

CLedBlinkTimerCh g_LedBlinkTimerCh;
