#include "stdafx.h"
#include "btn_exti.h"
#include "led_blink_timch.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	constexpr bool bButtonToGND = (
#ifdef USE_BOARD_nucleof072rb
		true
#else
		false
#endif
	);

	g_LedBlinkTimerCh.Init();
	CBtnExti::COnChangeCallback cb = BIND_MEM_CB(&CLedBlinkTimerCh::EnableBlinking, &g_LedBlinkTimerCh);
	g_BtnExti.Init(bButtonToGND, cb);
}

} // extern "C"
