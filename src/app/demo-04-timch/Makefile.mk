BOARDS=nucleof072rb f4disco
USE_PROCESS_SCHEDULLER=YES
USE_EXTI_BTN=YES
USE_TIM1=YES
FLASH_PROTECTION=NO

SRC += $(addprefix $(APP_PATH)/, \
	app_init.cpp \
	btn_exti.cpp \
	led_blink_timch.cpp \
)
