#include "stdafx.h"
#include "ain_sam.h"

void CAInSam::Init(COnValueCallback cbOnValue)
{
	m_cbOnValue = cbOnValue;
	unsigned uiSampleTime = (
			IF_STM32F0(ADC_SampleTime_239_5Cycles)
			IF_STM32F4(ADC_SampleTime_480Cycles)
	);
	TAI0::Acquire(uiSampleTime);

	TADC::Acquire();
	TADC::AttachAIN<TAI0::ChIndex>();
	TADC::SetSampleNotifier(this);  INDIRECT_CALL(NotifyADCSample());
	TADC::InitAINs();
	TADC::Start();
}

/*virtual*/
void CAInSam::NotifyADCSample()
{
	IMPLEMENTS_INTERFACE_METHOD(IADCSampleNotifier::NotifyADCSample());
	DEBUG_INFO("Sample");
	m_cbOnValue(TAI0::GetValue() >> 4);
}

CAInSam g_AInSam;
