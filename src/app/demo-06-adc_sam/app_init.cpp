#include "stdafx.h"
#include "ain_sam.h"
#include "led_value_timch.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	g_LedValueTimerCh.Init();
	CAInSam::COnValueCallback cb = BIND_MEM_CB(&CLedValueTimerCh::SetValue, &g_LedValueTimerCh);
	g_AInSam.Init(cb);
}

} // extern "C"
