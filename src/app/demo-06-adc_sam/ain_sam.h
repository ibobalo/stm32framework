#pragma once

#include "hw/hw.h"
#include "util/callback.h"

class CAInSam :
	public IADCSampleNotifier
{
public:
	typedef Callback<void (uint8_t)> COnValueCallback;

public:
	void Init(COnValueCallback cbOnChange);

public: // IADCSampleNotifier
	virtual void NotifyADCSample() override;

private:
	using TAI0 = CHW_Brd::CAnalogIn_AI0;
	using TADC = CHW_Brd::CADC_1;

	COnValueCallback          m_cbOnValue;
};

extern CAInSam g_AInSam;
