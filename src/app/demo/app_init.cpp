#include "stdafx.h"
#include <stdint.h>
#include "hw/hw.h"
#include "tick_process.h"

// globals
#define LEDPWMFREQ           50
#define HBRIDGE_PWM_FREQ     1000
#define SENS_24V_LIMIT       2048
#define SENS_CURRENT_MIN     64 /* 50mV / 3300mV * 4095 */
#define SENS_CURRENT_MAX     3000
#define SENS_USB_LIMIT       2000
#define SENS_POWER_LIMIT     3000
#define DEBOUNCE_POWER       100
#define DEBOUNCE_USB         100
#define DEBOUNCE_NOCURRENT   500
#define DEBOUNCE_OVERCURRENT 10
#define TIMEOUT_BEGIN        (200 + DEBOUNCE_NOCURRENT)
#define TIMEOUT_DOWN         6000
#define TIMEOUT_UP           6000
#define TIMEOUT_POWEROFF     100

class CDemoControlProcess :
	public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
//	~DemoControlProcess();

public:
	void Init();
public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime);
private:
	void ProcessBegin();
	void ProcessGoingDown();
	void ProcessDoneDown();
	void ProcessGoingUp();
	void ProcessPoweringOff();
	void ProcessError();
	void DoPowerOn();
	void DoStartDown();
	void ContinueDown();
	void DoStop();
	void DoStartUp();
	void ContinueUp();
	unsigned ContinueUpDown(unsigned uiPrevValue);
	void DoPowerOff();
	void DoError(unsigned nRecoverDelay =  10000);

private:
	CHW_Brd::CDigitalOut_DIO1*   m_pPowerSupplyEnableDO;
	CHW_Brd::CDigitalOut_DIO2*   m_pReservedDO;
	CHW_Brd::CDigitalOut_IO3*    m_pHBridgeEnableDO;
	CHW_Brd::CTimer_3*           m_pHBridgeTimer;
	CHW_Brd::CTimerChannel_IO4*  m_pHBridgeRPWMChannel;
	CHW_Brd::CTimerChannel_IO5*  m_pHBridgeLPWMChannel;
	CHW_Brd::CADC_1*             m_pADC;
	CHW_Brd::CAnalogIn_IO6*      m_pHBridgeASensAI;
	CHW_Brd::CAnalogIn_IO7*      m_pHBridgeVSensAI;
	CHW_Brd::CAnalogIn_IO8*      m_pVccUsbSensAI;
//	CHW_Brd::CDigitalIn_IO8*     m_pVccUsbSensDI;
	unsigned        m_uiChannelPeriod;
	unsigned        m_uiChannelStep;
	typedef enum {
		STATE_BEGIN,
		STATE_GOING_DOWN,
		STATE_DONE_DOWN,
		STATE_GOING_UP,
		STATE_POWERING_OFF,
		STATE_ERROR,
	} EState;
	EState          m_eState;

	bool            m_bPowerIsOn;
	bool            m_bPowerIsOnDebounced;
	unsigned        m_nPowerDebouncer;

	bool            m_bUsbIsOn;
	bool            m_bUsbIsOnDebounced;
	unsigned        m_nUsbDebouncer;

	bool            m_bIsNoCurrent;
	bool            m_bIsNoCurrentDebounced;
	unsigned        m_nNoCurrentDebouncer;

	bool            m_bIsOverCurrent;
	bool            m_bIsOverCurrentDebounced;
	unsigned        m_nOverCurrentDebouncer;

	unsigned        m_nTimeoutCounter;
	unsigned        m_nRecoverCounter;
};
static CDemoControlProcess g_DemoControlProcess;

void CDemoControlProcess::Init()
{
	CParentProcess::Init(&g_TickProcessScheduller);

	m_pPowerSupplyEnableDO = CHW_Brd::AcquireDigitalOut_DIO1(true, true);
	m_pReservedDO = CHW_Brd::AcquireDigitalOut_DIO2(true, true);
	m_pHBridgeEnableDO = CHW_Brd::AcquireDigitalOut_IO3(false);
	m_pHBridgeTimer = CHW_Brd::AcquireTimer_3();
	m_pHBridgeRPWMChannel = CHW_Brd::AcquireTimerChannel_IO4(0);
	m_pHBridgeLPWMChannel = CHW_Brd::AcquireTimerChannel_IO5(0);
	m_pADC = CHW_Brd::AcquireADC_1();
	m_pHBridgeASensAI = CHW_Brd::AcquireAnalogIn_IO6();
	m_pHBridgeVSensAI = CHW_Brd::AcquireAnalogIn_IO7();
	m_pVccUsbSensAI = CHW_Brd::AcquireAnalogIn_IO8();
//	m_pVccUsbSensDI = CHW_Brd::AcquireDigitalIn_IO8();

	m_pReservedDO->Set();
	m_pHBridgeTimer->SetPeriodClks(CHW_MCU::GetCLKFrequency() / HBRIDGE_PWM_FREQ);
	m_pHBridgeTimer->Start();
	m_uiChannelPeriod = m_pHBridgeTimer->GetPeriod();
	m_uiChannelStep = m_uiChannelPeriod / 100;
	m_pADC->Start();
	m_eState = STATE_BEGIN;
	m_bPowerIsOn = false;
	m_bPowerIsOnDebounced = false;
	m_nPowerDebouncer = 0;
	m_bUsbIsOn = true;
	m_bUsbIsOnDebounced = true;
	m_nUsbDebouncer = 0;
	m_bIsNoCurrent = true;
	m_bIsNoCurrentDebounced = true;
	m_nNoCurrentDebouncer = 0;
	m_bIsOverCurrent = false;
	m_bIsOverCurrentDebounced = false;
	m_nOverCurrentDebouncer = 0;
	m_nTimeoutCounter = 0;
	m_nRecoverCounter = 0;

	ContinueDelay(10);
}

bool Debounce(bool bCurrent, bool bDebounced, unsigned& nDebounceCounter, unsigned nLimit)
{
	if (bCurrent == bDebounced) {
		if (nDebounceCounter) --nDebounceCounter; // slow recover
	} else if (nDebounceCounter < nLimit) {
		++nDebounceCounter; // debouncing
	} else {
		nDebounceCounter = 0;
		return bCurrent;
	}
	return bDebounced;
}

void CDemoControlProcess::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CDemoControlProcess");

	m_bPowerIsOn = (m_pHBridgeVSensAI->GetValue() > SENS_POWER_LIMIT);
	m_bUsbIsOn = (m_pVccUsbSensAI->GetValue() > SENS_USB_LIMIT);
//	m_bUsbIsOn = m_pVccUsbSensDI->GetValue();
	unsigned uiCurrent = m_pHBridgeASensAI->GetValue();
	m_bIsNoCurrent   = (uiCurrent <= SENS_CURRENT_MIN);
	m_bIsOverCurrent = (uiCurrent > SENS_CURRENT_MAX);

	m_bPowerIsOnDebounced = Debounce(m_bPowerIsOn, m_bPowerIsOnDebounced, m_nPowerDebouncer, DEBOUNCE_POWER);
	m_bUsbIsOnDebounced = Debounce(m_bUsbIsOn, m_bUsbIsOnDebounced, m_nUsbDebouncer, DEBOUNCE_USB);
	m_bIsNoCurrentDebounced = Debounce(m_bIsNoCurrent, m_bIsNoCurrentDebounced, m_nNoCurrentDebouncer, DEBOUNCE_NOCURRENT);
	m_bIsOverCurrentDebounced = Debounce(m_bIsOverCurrent, m_bIsOverCurrentDebounced, m_nOverCurrentDebouncer, DEBOUNCE_OVERCURRENT);


//	unsigned uiBlnkN = ttTime%1000;
//	bool bBlink1 = (uiBlnkN < 20);
//	bool bBlinkOnce = (uiBlnkN < 300);
//	bool bBlinkTwice = (uiBlnkN < 750) && (uiBlnkN%400 < 350);
//	bool bBlinkTripple = (uiBlnkN < 750) && (uiBlnkN%300 < 250);
//	bool bBlink9 = (uiBlnkN < 980);
//
//	if (bPowerIsOn && bUsbIsOn) m_pHBridgeEnableDO->SetValue(bBlink9);
//	else if (bPowerIsOn) m_pHBridgeEnableDO->SetValue(bBlinkOnce);
//	else if (bUsbIsOn) m_pHBridgeEnableDO->SetValue(bBlinkTwice);
//	else m_pHBridgeEnableDO->SetValue(bBlink1);

//	m_pHBridgeEnableDO->SetValue(m_bIsNoCurrent?bBlink9:bBlink1);

	switch (m_eState) {
		case STATE_BEGIN:
			ProcessBegin();
			break;
		case STATE_GOING_DOWN:
			ProcessGoingDown();
			break;
		case STATE_DONE_DOWN:
			ProcessDoneDown();
			break;
		case STATE_GOING_UP:
			ProcessGoingUp();
			break;
		case STATE_POWERING_OFF:
			ProcessPoweringOff();
			break;
		case STATE_ERROR:
			ProcessError();
			break;
	}

	ContinueAt(ttTime + 10);

	PROCESS_DEBUG_INFO("<<< Process: CDemoControlProcess");
}

void CDemoControlProcess::ProcessBegin()
{
	if (++m_nTimeoutCounter > TIMEOUT_BEGIN) {
		DoError();
		DEBUG_INFO("Timeout while begin");
	} else if (!(m_bIsNoCurrent && m_bIsNoCurrentDebounced)) {
		DoStop(); // wait no current
	} else if (!m_bPowerIsOn) {
		DoPowerOn(); // power ON
	} else if (!m_bPowerIsOnDebounced) {
		// keep waiting power ON
	} else if (m_bPowerIsOn != m_bPowerIsOnDebounced || m_bUsbIsOn != m_bUsbIsOnDebounced) {
		// keep waiting power debouncers
	} else if (m_bUsbIsOnDebounced) {
		DoStartDown();
		m_bIsNoCurrentDebounced = false; // reset debouncer
		m_nTimeoutCounter = 0;
		m_eState = STATE_GOING_DOWN;
		DEBUG_INFO("Start DOWN");
	} else {
		DoStartUp();
		m_bIsNoCurrentDebounced = false; // reset debouncer
		m_nTimeoutCounter = 0;
		m_eState = STATE_GOING_UP;
		DEBUG_INFO("Start UP");
	}
}

void CDemoControlProcess::ProcessGoingDown()
{
	if (++m_nTimeoutCounter > TIMEOUT_DOWN) {
		DoError();
		DEBUG_INFO("Timeout while DOWN");
	} else if (m_bIsOverCurrentDebounced) {
		DoError();
		DEBUG_INFO("OVERCURRENT while DOWN");
	} else if (!m_bPowerIsOnDebounced) {
		DoError();
		DEBUG_INFO("Power lost while DOWN");
	} else if (!m_bUsbIsOnDebounced) {
		DoStop();
		m_nTimeoutCounter = 0;
		m_eState = STATE_BEGIN;
		DEBUG_INFO("Off while going DOWN");
	} else if (m_bIsNoCurrentDebounced) {
		DoStop();
		m_eState = STATE_DONE_DOWN;
		DEBUG_INFO("DOWN done");
	} else {
		ContinueDown();
	}
}

void CDemoControlProcess::ProcessDoneDown()
{
	if (m_bIsOverCurrentDebounced) {
		DoError();
		DEBUG_INFO("OVERCURRENT while UP");
	} else if (!m_bPowerIsOnDebounced) {
		DoError();
		DEBUG_INFO("Power lost while UP");
	} else if (!m_bUsbIsOnDebounced) {
		m_nTimeoutCounter = 0;
		m_eState = STATE_BEGIN;
		DEBUG_INFO("OFF got, going UP");
	} else {
		// keep active while ON
	}
}

void CDemoControlProcess::ProcessGoingUp()
{
	if (++m_nTimeoutCounter > TIMEOUT_UP) {
		DoError();
		DEBUG_INFO("Timeout while UP");
	} else if (m_bIsOverCurrentDebounced) {
		DoError();
		DEBUG_INFO("OVERCURRENT while UP");
	} else if (!m_bPowerIsOnDebounced) {
		DoError();
		DEBUG_INFO("Power lost while UP");
	} else if (m_bUsbIsOnDebounced) {
		DoStop();
		m_nTimeoutCounter = 0;
		m_eState = STATE_BEGIN;
		DEBUG_INFO("On while going UP");
	} else if (m_bIsNoCurrentDebounced) {
		DoStop();
		DoPowerOff();
		m_nTimeoutCounter = 0;
		m_eState = STATE_POWERING_OFF;
		DEBUG_INFO("UP done, Power OFF");
	} else {
		ContinueUp();

	}
}

void CDemoControlProcess::ProcessPoweringOff()
{
	if (++m_nTimeoutCounter > TIMEOUT_POWEROFF) {
		DoError();
		DEBUG_INFO("Timeout while Power OFF");
	} else if (m_bUsbIsOnDebounced) {
		DoPowerOn();
		m_nTimeoutCounter = 0;
		m_eState = STATE_BEGIN;
		DEBUG_INFO("Power ON again");
	}
}

void CDemoControlProcess::ProcessError()
{
	ASSERTE(false);
	if (m_nRecoverCounter) {
		--m_nRecoverCounter;
	} else {
		m_eState = STATE_BEGIN;
		m_nTimeoutCounter = 0;
		DEBUG_INFO("Recover from Error");
	}
}

void CDemoControlProcess::DoPowerOn()
{
	m_pPowerSupplyEnableDO->Reset(); // inverted open drain
}
void CDemoControlProcess::DoStartDown()
{
	m_pHBridgeEnableDO->Set();
	m_pHBridgeRPWMChannel->SetValue(0U);
	m_pHBridgeLPWMChannel->SetValue(m_uiChannelStep);
}
void CDemoControlProcess::ContinueDown() {
	unsigned uiNewValue = ContinueUpDown(m_pHBridgeLPWMChannel->GetValue());
	m_pHBridgeLPWMChannel->SetValue(uiNewValue);
}
void CDemoControlProcess::DoStop()
{
	m_pHBridgeRPWMChannel->SetValue(0U);
	m_pHBridgeLPWMChannel->SetValue(0U);
	m_pHBridgeEnableDO->Reset();
}
void CDemoControlProcess::DoStartUp()
{
	m_pHBridgeEnableDO->Set();
	m_pHBridgeRPWMChannel->SetValue(m_uiChannelStep);
	m_pHBridgeLPWMChannel->SetValue(0U);
}
void CDemoControlProcess::ContinueUp()
{
	unsigned uiNewValue = ContinueUpDown(m_pHBridgeRPWMChannel->GetValue());
	m_pHBridgeRPWMChannel->SetValue(uiNewValue);
}
unsigned CDemoControlProcess::ContinueUpDown(unsigned uiPrevValue)
{
	bool bIncrease = !m_bIsOverCurrent;
	unsigned uiResult = uiPrevValue;
	ASSERTE(m_uiChannelPeriod >= uiPrevValue);
	if (bIncrease) {
		unsigned uiLeft = m_uiChannelPeriod - uiPrevValue;
		if (uiLeft) {
			unsigned uiStep = (uiLeft > m_uiChannelStep) ? m_uiChannelStep : uiLeft;
			uiResult = uiPrevValue + uiStep;
		}
	} else if (uiPrevValue >= m_uiChannelStep) {
		uiResult = uiPrevValue - m_uiChannelStep;
	}
	return uiResult;
}
void CDemoControlProcess::DoPowerOff()
{
	m_pPowerSupplyEnableDO->Set(); // inverted open drain
}
void CDemoControlProcess::DoError(unsigned nRecoverDelay)
{
	DoStop();
	DoPowerOff();
	m_nRecoverCounter = nRecoverDelay;
	m_eState = STATE_ERROR;
}


extern "C" {

void app_init(void)
{
//	g_DemoControlProcess.Init();
}

} // extern "C"
