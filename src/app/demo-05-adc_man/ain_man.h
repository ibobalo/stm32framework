#pragma once

#include "hw/hw.h"
#include "util/callback.h"
#include "tick_process.h"

class CAInMan :
		public CTickProcess
{
public:
	typedef Callback<void (uint8_t)> COnValueCallback;
public:
	void Init(COnValueCallback cbOnChange);
public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime) override;

private:
	using TAI0 = CHW_Brd::CAnalogIn_AI0;
	using TADC = CHW_Brd::CADC_1;

	COnValueCallback          m_cbOnValue;
};

extern CAInMan g_AInMan;
