#include "stdafx.h"
#include "ain_man.h"
#include "led_value_timch.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	g_LedValueTimerCh.Init();
	CAInMan::COnValueCallback cb = BIND_MEM_CB(&CLedValueTimerCh::SetValue, &g_LedValueTimerCh);
	g_AInMan.Init(cb);
}

} // extern "C"
