#include "stdafx.h"
#include "ain_man.h"

void CAInMan::Init(COnValueCallback cbOnValue)
{
	CTickProcess::Init(&g_TickProcessScheduller);

	m_cbOnValue = cbOnValue;

	constexpr unsigned uiSampleTime = (
			IF_STM32F0(ADC_SampleTime_239_5Cycles)
			IF_STM32F4(ADC_SampleTime_480Cycles)
	);
	TAI0::Acquire(uiSampleTime);

	TADC::Acquire(false);
	TADC::AttachAIN<TAI0::ChIndex>();
	TADC::InitAINs();
	TADC::Start();

	ContinueDelay(10);
}

void CAInMan::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));

	TADC::Stop();

	uint16_t uiValue = TADC::GetRecentSample();
	m_cbOnValue(uiValue >> 4);

	TADC::Start();
	ContinueDelay(10);
}


CAInMan g_AInMan;
