BOARDS=nucleof072rb f4disco
USE_PROCESS_SCHEDULLER=YES
USE_TIM1=YES
USE_ADC1=YES
USE_ADC_WITHOUT_IT=YES
USE_ADC_WITHOUT_DMA=YES
FLASH_PROTECTION=NO

SRC += $(addprefix $(APP_PATH)/, \
	app_init.cpp \
	ain_man.cpp \
	led_value_timch.cpp \
)
