#include "stdafx.h"
#include "ain_seq.h"

void CAInSeq::Init(COnValueCallback cbOnValue)
{
	m_cbOnValue = cbOnValue;
	unsigned uiSampleTime = (
			IF_STM32F0(ADC_SampleTime_239_5Cycles)
			IF_STM32F4(ADC_SampleTime_480Cycles)
	);
	TAI0::Acquire(uiSampleTime);
	TAI1::Acquire(uiSampleTime);
	TAIt::Acquire(uiSampleTime);
	TAIv::Acquire(uiSampleTime);

	TADC::Acquire();
	TADC::AttachAIN<TAI0::ChIndex>();
	TADC::AttachAIN<TAI1::ChIndex>();
	TADC::AttachAIN<TAIt::ChIndex>();
	TADC::AttachAIN<TAIv::ChIndex>();
	TADC::SetSequenceNotifier(this);  INDIRECT_CALL(NotifyADCSequence());
	TADC::SetErrorNotifier(this);  INDIRECT_CALL(NotifyADCError());
	TADC::InitAINs();
	TADC::Start();
}

/*virtual*/
void CAInSeq::NotifyADCSequence()
{
	IMPLEMENTS_INTERFACE_METHOD(IADCSequenceNotifier::NotifyADCSequence());
	DEBUG_INFO("Sequence");
	m_cbOnValue(TAI0::GetValue() >> 4);
	static auto n = 0;
	if (++n > 3) {
		ASSERT("DEBUG");
	}
}

/*virtual*/
void CAInSeq::NotifyADCError()
{
	IMPLEMENTS_INTERFACE_METHOD(IADCErrorNotifier::NotifyADCError());
	DEBUG_INFO("ADC OVR");
	ASSERT("ADC OVR")
}

CAInSeq g_AInSeq;
