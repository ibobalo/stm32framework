#include "stdafx.h"
#include "led_value_timch.h"

void CLedValueTimerCh::Init()
{
	LED::Acquire(false);
	TIM::Acquire();
	TIM::SetNotifier(this, 1, 0);  INDIRECT_CALL(NotifyTimer());
	// 64 Hz
	uint64_t ulPeriodClks = CHW_Clocks::GetCLKFrequency() / 64;
	TIM::SetPeriodClks(ulPeriodClks);
	// short bursts
	TIMCH::Acquire(0);
	TIMCH::SetNotifier(this, 1, 1);  INDIRECT_CALL(NotifyTimerChannel(0));

	TIM::Start(); INDIRECT_CALL(NotifyTimer(), NotifyTimerChannel(0));
}

void CLedValueTimerCh::SetValue(uint8_t uiValue)
{
	DEBUG_INFO("Value:", uiValue);
	TIMCH::SetDutyCycle(uiValue);
}

/*virtual*/
void CLedValueTimerCh::NotifyTimer()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());
	LED::SetValue(TIM::GetCounter() < TIMCH::GetValue());
}

/*virtual*/
void CLedValueTimerCh::NotifyTimerChannel(unsigned nChannel)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannelNotifier::NotifyTimerChannel(nChannel));
	LED::SetValue(TIM::GetCounter() < TIMCH::GetValue());
}

CLedValueTimerCh g_LedValueTimerCh;
