#pragma once

#include "hw/hw.h"

class CLedValueTimerCh :
		public ITimerNotifier,
		public ITimerChannelNotifier
{
public:
	void Init();
	void SetValue(uint8_t uiValue);
public: // ITimerNotifier
	virtual void NotifyTimer() override;
public: // ITimerChannelNotifier
	virtual void NotifyTimerChannel(unsigned nChannel) override;

private:
	using LED = CHW_Brd::CDigitalOut_LED;
	using TIM = CHW_Brd::CTimer_1;
	using TIMCH = CHW_Brd::CTimerChannel_PWM1_1;
};

extern CLedValueTimerCh g_LedValueTimerCh;
