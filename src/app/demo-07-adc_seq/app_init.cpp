#include "stdafx.h"
#include "ain_seq.h"
#include "led_value_timch.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	g_LedValueTimerCh.Init();
	CAInSeq::COnValueCallback cb = BIND_MEM_CB(&CLedValueTimerCh::SetValue, &g_LedValueTimerCh);
	g_AInSeq.Init(cb);
}

} // extern "C"
