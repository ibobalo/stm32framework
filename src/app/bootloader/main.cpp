//#pragma GCC optimize ("Os")

#include "stdafx.h"
#include "util/macros.h"
#include "util/core.h"
#include "hw/hw.h"
#include "hw/hw_macros.h"
#include "stm32_linker.h"
#include "stm32fxxxx.h"
#include <stdlib.h>

/*

LZMA e firmware.bin file.lzma -lc0 -lp2

download file.lzma to target system and flash it to firmware_update flash area
restart target system.

boot manager will:
  - check update presence;
  - decode (unzip) firmware image to ram;
  - decrypt (AES) unpacked image;
  - check integrity (CRC) on new firmware;
  - check new firmware grants (UNIQ_ID, etc);
  - flash new firmware to working area;
  - erase firmware_update flash area;
  - start new firmware

NOTE:
  RAM usage ~ dict_size + 1846 + (768 << (lc + lp)))

*/

#if defined(USE_BOOTLOADER_ZIP) || defined(USE_BOOTLOADER_AES)
#  define BUF_SIZE (17*1024)
#else
#  define BUF_SIZE (2*1024)
#endif

#ifdef USE_BOOTLOADER_AES
static uint8_t g_aDecryptedBuf[BUF_SIZE];
#endif //USE_BOOTLOADER_AES

#ifdef USE_BOOTLOADER_ZIP
static uint8_t g_aDecompressedBuf[BUF_SIZE];
#endif // USE_BOOTLOADER_ZIP

#include "sha256.h"
#ifdef USE_BOOTLOADER_AES
#  include "aes256.h"
#endif // USE_BOOTLOADER_AES
#ifdef USE_BOOTLOADER_ZIP
#  define TINFL_HEADER_FILE_ONLY
#  ifdef __cplusplus
     extern "C" {
#  endif
#  include "tinfl.c"
#  ifdef __cplusplus
     }
#  endif
#  undef TINFL_HEADER_FILE_ONLY
#endif // USE_BOOTLOADER_ZIP

static inline void Bootloader_StartFirmware() __attribute__ ((noreturn));
static inline void BootJump(uint32_t SP, uint32_t RH) __attribute__ ((noreturn));
#define LED_FN(COLOR,PORT) \
		void InitLed##COLOR() { \
			IF_STM32F0(RCC->AHBENR   |= CC(RCC_AHBPeriph_GPIO,FIRST(PORT))); \
			IF_STM32F4(RCC->AHB1ENR  |= CC(RCC_AHB1Periph_GPIO,FIRST(PORT))); \
			CC(GPIO,FIRST(PORT))->MODER   |= (((uint32_t)GPIO_Mode_OUT)           << (SECOND(PORT) * 2)); \
			CC(GPIO,FIRST(PORT))->OTYPER  |= (uint16_t)(((uint16_t)GPIO_OType_PP) <<  SECOND(PORT)     ); \
			CC(GPIO,FIRST(PORT))->OSPEEDR |= ((uint32_t)(GPIO_Speed_2MHz)         << (SECOND(PORT) * 2)); \
			CC(GPIO,FIRST(PORT))->PUPDR   |= (((uint32_t)GPIO_PuPd_NOPULL)        << (SECOND(PORT) * 2)); \
		} \
		void SetLED##COLOR() { \
			GPIO_WriteBit(CC(GPIO,FIRST(PORT)), CC(GPIO_Pin_,SECOND(PORT)), BOOL(THIRD(PORT))?Bit_RESET:Bit_SET); \
		} \
		void ClearLED##COLOR() { \
			GPIO_WriteBit(CC(GPIO,FIRST(PORT)), CC(GPIO_Pin_,SECOND(PORT)), BOOL(THIRD(PORT))?Bit_SET:Bit_RESET); \
		}

#if defined(BOOTLOADER_LED_PORT)
LED_FN(,BOOTLOADER_LED_PORT)
#endif
#if defined(BOOTLOADER_LED_ERROR_PORT)
LED_FN(Error,BOOTLOADER_LED_ERROR_PORT)
#endif
#if defined(BOOTLOADER_LED_SUCCESS_PORT)
LED_FN(Success,BOOTLOADER_LED_SUCCESS_PORT)
#endif

void InitLeds()
{
#if defined(BOOTLOADER_LED_PORT)
	InitLed(); ClearLED();
#endif
#if defined(BOOTLOADER_LED_ERROR_PORT)
	InitLedError(); ClearLEDError();
#endif
#if defined(BOOTLOADER_LED_SUCCESS_PORT)
	InitLedSuccess(); ClearLEDSuccess();
#endif
}
enum ELedState {
	LED_PROCESSING,
	LED_PROCESSING_SKIP,
	LED_PROCESSING_SECURED,
	LED_PROCESSING_SUCCESS,
	LED_PROCESSING_ERROR,
	LED_DONE_SUCCESS,
	LED_DONE_ERROR,
} g_eLedState;
void DisplayLedState()
{
#if defined(BOOTLOADER_LED_PORT)
	static unsigned g_nBlink = 0;
	InitLeds();
	switch (g_eLedState) {
	case LED_PROCESSING:
	case LED_PROCESSING_SKIP:
	case LED_PROCESSING_SECURED:
		if (++g_nBlink % 32 < 16 ) SetLED();
		break;
	case LED_PROCESSING_SUCCESS:
#if defined(BOOTLOADER_LED_SUCCESS_PORT)
		if (++g_nBlink % 32 < 16 ) SetLED();
		SetLEDSuccess();
#else
		if (++g_nBlink % 32 < 24 ) SetLED();
#endif
		break;
	case LED_PROCESSING_ERROR:
#if defined(BOOTLOADER_LED_ERROR_PORT)
		if (++g_nBlink % 32 < 16) SetLED();
		SetLEDError();
#else
		if (++g_nBlink % 32 < 2) SetLED();
#endif
		break;
	case LED_DONE_SUCCESS:
#if defined(BOOTLOADER_LED_SUCCESS_PORT)
		SetLEDSuccess();
#else
		SetLED();
#endif
		break;
	case LED_DONE_ERROR:
#if defined(BOOTLOADER_LED_ERROR_PORT)
		SetLEDError();
#else
		if (++g_nBlink % 8 < 2) SetLED();
#endif
		break;
	}
#endif // BOOTLOADER_LED_PORT
}
void SetLedState(ELedState eLedState)
{
	g_eLedState = eLedState;
	DisplayLedState();
}
void Delay()
{
	for (unsigned m = 0; m < 32; ++m) {
		for (unsigned n = 0; n < 70000; ++n)
			__NOP();
		DisplayLedState();
	}
	IWDG_ReloadCounter();
}

typedef struct {
	bool bSkipFlash;
	bool bValid;
	bool bSecured;
	bool bSigned;
	bool bFirmwareErased;
} meta_context_t;
//META_HEADER_FORMAT    = "<LB"  # size, type
typedef enum {
	MT_NONE, MT_SEGMENT, MT_ERASE, MT_UNIQIDS, MT_MCUIDS, MT_SHA256, MT_CRYPTO, MT_Z,
	MT__EOF = 0xFF
} meta_t;
typedef struct {
	uint32_t  uiSize;
	meta_t    eMetaType;
	uint8_t   _pad[4 - sizeof(eMetaType)];
	uint8_t   meta_data[];
} __attribute__ ((packed)) meta_header_t;
//	META_SEGMENT_FORMAT   = "<L"     # offset
typedef struct {
	uint32_t  uiTgtAddr;
	uint8_t   data[];
} __attribute__ ((packed)) meta_segment_t;
//	META_ERASE_FORMAT     = "<B"     # sector
typedef uint8_t  meta_erase_pages_t;
//	META_UNIQIDS_FORMAT   = "<6B"    # UniqID
typedef CHW_MCU::uniqid_t meta_uniqids_t;
//	META_MCUIDS_FORMAT    = "<H"     # McuID
typedef uint16_t meta_mcuids_t;
//	META_SHA256_FORMAT    = "<32s"   # sha256_digest
typedef struct {
	sha256_digest_t         tDigest;
	uint8_t                 data[];
} __attribute__ ((packed)) meta_sha256_t;
//  META_CRYPTO_FORMAT    = "<L16s"  # unpaded_size, salt,
typedef struct {
	uint32_t          uiUnpadedSize;
	uint8_t           aVI[16];
	uint8_t           data[];
} __attribute__ ((packed)) meta_crypto_t;

const uint8_t* GetBootloaderPtr()
{
	return GetLinkerSymbol(const uint8_t*, firmware_start);
}

inline unsigned GetBootloaderSize() { return GetLinkerSymbol(unsigned, firmware_size); }
inline unsigned GetBootloaderReservedSize() { return GetLinkerSymbol(unsigned, reserved_size); }
inline unsigned GetFwUpdateSize() { return UPDATE_PARTITION_SIZE * 1024; }
inline unsigned GetFwCodeSize() { return GetBootloaderReservedSize() - GetFwUpdateSize(); }
inline unsigned GetBootloaderFirstPageNumber() { return PtrToPageNumber(GetBootloaderPtr()); }
inline unsigned GetBootloaderLastPageNumber() { return PtrToPageNumber(GetBootloaderPtr() + GetBootloaderSize() - 1); }
inline const uint8_t* GetFwCodePtr() { return GetLinkerSymbol(const uint8_t*, reserved_start); }
inline unsigned GetFwCodeFirstPageNumber() { return PtrToPageNumber(GetFwCodePtr()); }
inline unsigned GetFwCodeLastPageNumber() { return PtrToPageNumber(GetFwCodePtr() + GetFwCodeSize() - 1); }
inline const uint8_t* GetFwUpdatePtr() { return GetLinkerSymbol(const uint8_t*, reserved_start) + GetLinkerSymbol(unsigned, reserved_size) - GetFwUpdateSize(); }
inline unsigned GetFwUpdateFirstPageNumber() { return PtrToPageNumber(GetFwUpdatePtr()); }
inline unsigned GetFwUpdateLastPageNumber() { return PtrToPageNumber(GetFwUpdatePtr() + GetFwUpdateSize() - 1); }

bool CheckUpdateUploaded()
{
	bool bResult = false;
	const uint8_t* pFwUpdate = GetFwUpdatePtr();
	unsigned uiSize = GetFwUpdateSize();
	if (uiSize) {
		if (uiSize > 1024) uiSize = 1024; // check only first 1K
		while (uiSize >= sizeof(uint64_t)) {
			if (*(const uint64_t*)pFwUpdate != 0xFFFFFFFFFFFFFFFFULL) {
				bResult = true;
				break;
			}
			uiSize -= sizeof(uint64_t); pFwUpdate += sizeof(uint64_t);
		}
	}
	return bResult;
}
bool CheckAddressIsBootloader(uint32_t uiAddr)
{
	return (uiAddr >= (uint32_t)GetBootloaderPtr() && uiAddr < (uint32_t)GetBootloaderPtr() + GetBootloaderSize());
}
bool CheckPageIsBootloader(uint16_t uiPage)
{
	return (uiPage >= GetBootloaderFirstPageNumber() && uiPage <= GetBootloaderLastPageNumber());
}
bool CheckSectorIsBootloader(uint16_t uiSector)
{
	return (uiSector >= PageNumberToSectorNumber(GetBootloaderFirstPageNumber()) && uiSector <= PageNumberToSectorNumber(GetBootloaderLastPageNumber()));
}
bool CheckAddressIsFirmware(uint32_t uiAddr)
{
	return (uiAddr >= (uint32_t)GetFwCodePtr() && uiAddr < (uint32_t)GetFwCodePtr() + GetFwCodeSize());
}
bool CheckPageIsFirmware(uint16_t uiPage)
{
	return (uiPage >= GetFwCodeFirstPageNumber() && uiPage <= GetFwCodeLastPageNumber());
}
bool CheckSectorIsFirmware(uint16_t uiSector)
{
	return (uiSector >= PageNumberToSectorNumber(GetFwCodeFirstPageNumber()) && uiSector <= PageNumberToSectorNumber(GetFwCodeLastPageNumber()));
}
bool CheckAddressIsFwUpdate(uint32_t uiAddr)
{
	return (uiAddr >= (uint32_t)GetFwUpdatePtr() && uiAddr < (uint32_t)GetFwUpdatePtr() + GetFwUpdateSize());
}
bool CheckPageIsFwUpdate(uint16_t uiPage)
{
	return (uiPage >= GetFwUpdateFirstPageNumber() && uiPage <= GetFwUpdateLastPageNumber());
}
bool CheckSectorIsFwUpdate(uint16_t uiSector)
{
	return (uiSector >= PageNumberToSectorNumber(GetFwUpdateFirstPageNumber()) && uiSector <= PageNumberToSectorNumber(GetFwUpdateLastPageNumber()));
}

static bool FlashMemCpy(void* pTgt, const void* pValue, unsigned uiSize)
{
	bool bResult = true;
	uint8_t* pTarget = (uint8_t*)pTgt;
	uint8_t* pSource = (uint8_t*)pValue;
	unsigned nBytesLeft = uiSize;
	FLASH_Unlock();
	if (IS_MEMORY_ALIGNED(pTarget, 4) && IS_MEMORY_ALIGNED(pSource, 4)) while (bResult && nBytesLeft >= 4) {
		if (FLASH_ProgramWord((uint32_t)pTarget, *(const uint32_t*)pSource) == FLASH_COMPLETE) {
			nBytesLeft -= 4;
			pTarget += 4;
			pSource += 4;
		} else {
			bResult = false;
			break;
		}
	}
	if (IS_MEMORY_ALIGNED(pTarget, 2) && IS_MEMORY_ALIGNED(pSource, 2)) while (bResult && nBytesLeft >= 2) {
		if (FLASH_ProgramHalfWord((uint32_t)pTarget, *(const uint16_t*)pSource) == FLASH_COMPLETE) {
			nBytesLeft -= 2;
			pTarget += 2;
			pSource += 2;
		} else {
			bResult = false;
			break;
		}
	}
#if defined(STM32F4)
	while (bResult && nBytesLeft) {
		if (FLASH_ProgramByte((uint32_t)pTarget, *(const uint8_t*)pSource) == FLASH_COMPLETE) {
			nBytesLeft -= 1;
			pTarget += 1;
			pSource += 1;
		} else {
			bResult = false;
			break;
		}
	}
#endif // STM32F0
	FLASH_Lock();
	return bResult && !nBytesLeft;
}
static bool EraseUpdateImage()
{
	bool bResult = true;
	FLASH_Unlock();
	for (unsigned uiPage = GetFwUpdateFirstPageNumber(); uiPage < GetFwUpdateLastPageNumber(); ++uiPage) {
		bResult = \
				IF_STM32F0(FLASH_ErasePage(uiPage))
				IF_STM32F4(FLASH_EraseSector(uiPage << 3, VoltageRange_3) == FLASH_COMPLETE)
			|| bResult;
	}
	FLASH_Lock();
	return bResult;
}
static bool EraseFirmware()
{
	bool bResult = true;
	FLASH_Unlock();
	for (unsigned uiPage = GetFwCodeFirstPageNumber(); uiPage < GetFwCodeLastPageNumber(); ++uiPage) {
		bResult = \
				IF_STM32F0(FLASH_ErasePage(uiPage))
				IF_STM32F4(FLASH_EraseSector(uiPage << 3, VoltageRange_3) == FLASH_COMPLETE)
			|| bResult;
	}
	FLASH_Lock();
	return bResult;
}
static bool ProcessMetaSegment(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context);
static bool ProcessMetaErase(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context);
static bool ProcessMetaUniqIds(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context);
static bool ProcessMetaMcuIds(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context);
static bool ProcessMetaDigest(const uint8_t* pData, uint32_t uiMetaSize, meta_context_t &context);
static bool ProcessMetaCrypto(const uint8_t* pMetaData, uint32_t uiMetaSize, meta_context_t& context);
static bool ProcessMetaZ(const uint8_t* pMetaData, uint32_t uiMetaSize, meta_context_t& context);
bool ProcessSingleMetaRecord(meta_t eMetaType, uint32_t uiMetaSize, const uint8_t* pMetaData, meta_context_t& context);
bool ProcessAllMetaRecords(const meta_header_t* pFirstMetaRecord, unsigned uiMaxSize, meta_context_t& context);
bool ProcessFwUpdateImage();

bool ProcessMetaSegment(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context)
{
	const meta_segment_t* pMetaSegment = (const meta_segment_t*) pMetaData;
	unsigned uiSize = uiMetaSize - sizeof(meta_segment_t);
	bool bResult = (1
			&& context.bValid
			&& context.bSecured
			&& context.bSigned
			&& (uiMetaSize >= sizeof(meta_segment_t))
	);
	if (bResult) {
		uint8_t* pTgtData = (uint8_t*)(pMetaSegment->uiTgtAddr);
		const uint8_t* pSrcData = pMetaSegment->data;
		bResult = (0
			|| ( 1
				&& CheckAddressIsBootloader(pMetaSegment->uiTgtAddr)
				&& CheckAddressIsBootloader(pMetaSegment->uiTgtAddr + uiSize)) // ignore bootloader rewrite
			|| ( CheckAddressIsFirmware(pMetaSegment->uiTgtAddr)
			  && CheckAddressIsFirmware(pMetaSegment->uiTgtAddr + uiSize)
			  && ( 0
			      || context.bSkipFlash
			      || FlashMemCpy(pTgtData, pSrcData, uiSize)
			     )
			  )
		);
	}
	return bResult;
}
bool ProcessMetaErase(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context)
{
	const meta_erase_pages_t* aErasePages = (const meta_erase_pages_t*) pMetaData;
	unsigned uiPageCount = uiMetaSize / sizeof(aErasePages[0]);
	bool bResult = (1
			&& context.bValid
			&& context.bSecured
			&& context.bSigned
			&& uiPageCount >= 0
	);
	if (bResult) {
		FLASH_Unlock();
		for (unsigned n = 0; n < uiPageCount; ++n) {
			unsigned uiPage = aErasePages[n] & 0x0F;
			if (!CheckPageIsBootloader(uiPage)) {
				bResult = context.bSkipFlash ||
						IF_STM32F0(FLASH_ErasePage(uiPage))
						IF_STM32F4(FLASH_EraseSector(uiPage << 3, VoltageRange_3));
			} else {
				bResult = false; // unable to erase boot sector
			}
		}
		FLASH_Lock();
	}
	return bResult;
}
bool ProcessMetaUniqIds(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context)
{
#ifdef UID_ADDR
	const meta_uniqids_t* aUniqIds = (const meta_uniqids_t*) pMetaData;
	unsigned uiUniqIdsCount = uiMetaSize / sizeof(aUniqIds[0]);
	bool bResult = (1
			&& context.bValid
			&& context.bSecured
			&& context.bSigned
			&& uiUniqIdsCount > 0
	);
	if (bResult) {
		bool bFound = false;
		for (unsigned n = 0; n < uiUniqIdsCount; ++n) {
			if (aUniqIds[n] == CHW_MCU::GetUniqueId()) {
				bFound = true;
				break;
			}
		}
		bResult = bFound;
	}
	return bResult;
#else
	return false;
#endif
}

bool ProcessMetaMcuIds(const uint8_t* pMetaData, unsigned uiMetaSize, meta_context_t& context)
{
	const meta_mcuids_t* aMcuIds = (const meta_mcuids_t*) pMetaData;
	unsigned uiMcuIdsCount = uiMetaSize / sizeof(aMcuIds[0]);
	bool bResult = (1
			&& context.bValid
			&& context.bSecured
			&& context.bSigned
			&& uiMcuIdsCount > 0
	);
	if (bResult) {
		bool bFound = false;
		for (unsigned n = 0; n < uiMcuIdsCount; ++n) {
			if (aMcuIds[n] == (DBGMCU->IDCODE & 0x0FFF)) {
				bFound = true;
				break;
			}
		}
		bResult = bFound;
	}
	return bResult;
}
bool ProcessMetaDigest(const uint8_t* pMetaData, uint32_t uiMetaSize, meta_context_t& context)
{
	const meta_sha256_t* pMetaCrc = (const meta_sha256_t*) pMetaData;
	unsigned uiSize = uiMetaSize - sizeof(meta_sha256_t);
	const meta_header_t* pMetaRecord = (const meta_header_t*)pMetaCrc->data;
	bool bResult = (1
			&& (uiMetaSize >= sizeof(meta_sha256_t))
	);
	if (bResult) {
		sha256_digest_t digest;
		sha256(pMetaCrc->data, uiSize, &digest);
		if (digest == pMetaCrc->tDigest) {
			meta_context_t inner_context = context;
			inner_context.bValid = true;
			if (context.bSecured) { // only secured digest is meaningful
				inner_context.bSigned = true;
			}
			bResult = ProcessAllMetaRecords(pMetaRecord, uiSize, inner_context);
		} else {
			bResult = false;
		}
	}
	return bResult;
}
void memcpy16(void *pDest, const void *pSrc)
{
	uint32_t* puiDest = (uint32_t*) pDest;
	const uint32_t* puiSrc = (const uint32_t*) pSrc;
	puiDest[0] = puiSrc[0];
	puiDest[1] = puiSrc[1];
	puiDest[2] = puiSrc[2];
	puiDest[3] = puiSrc[3];
}
void memxor16(void *pDest, const void *pSrcLeft, const void *pSrcRight)
{
	uint32_t* puiDest = (uint32_t*) pDest;
	const uint32_t* puiSrcLeft = (const uint32_t*) pSrcLeft;
	const uint32_t* puiSrcRight = (const uint32_t*) pSrcRight;
	puiDest[0] = puiSrcLeft[0] ^ puiSrcRight[0];
	puiDest[1] = puiSrcLeft[1] ^ puiSrcRight[1];
	puiDest[2] = puiSrcLeft[2] ^ puiSrcRight[2];
	puiDest[3] = puiSrcLeft[3] ^ puiSrcRight[3];
}

bool ProcessMetaCrypto(const uint8_t* pMetaData, uint32_t uiMetaSize, meta_context_t& context)
{
#ifdef USE_BOOTLOADER_AES
	const meta_crypto_t* pMetaCrypto = (const meta_crypto_t*) pMetaData;
	unsigned uiSize = uiMetaSize - sizeof(meta_crypto_t);
	const uint8_t* pCryptedData = (const uint8_t*)pMetaCrypto->data;
	bool bResult = (1
			&& context.bValid
			&& (uiMetaSize >= sizeof(meta_crypto_t))
	);
	if (bResult) {
#if defined(UPDATE_KEY)
		static uint8_t aes_key[] = {UPDATE_KEY};
#elif defined(UPDATE_PASSWORD)
		static const char g_UPDATE_PASSWORD[] = STR(UPDATE_PASSWORD);
		sha256_digest_t key;
		sha256((const uint8_t *)g_UPDATE_PASSWORD, sizeof(g_UPDATE_PASSWORD) - 1, &key);
		uint8_t* aes_key = (uint8_t*)&key;
#else
# error UPDATE_KEY or UPDATE_PASSWORD definition required
#endif
		meta_context_t internal_context = context;
		internal_context.bSecured = true;
		if (uiSize <= sizeof(g_aDecryptedBuf)) {
	    	// copy crypted data to output
			aes256_context ctx;
			aes256_init(&ctx, aes_key);
			static uint8_t g_aAes256CbfTemp[16];
			memcpy16(g_aAes256CbfTemp, pMetaCrypto->aVI);
			for (unsigned n = 0; n < uiSize; n += 16) {
				aes256_encrypt_ecb(&ctx, (uint8_t*)g_aAes256CbfTemp);
				memxor16(&g_aDecryptedBuf[n], g_aAes256CbfTemp, pCryptedData + n);
				memcpy16(g_aAes256CbfTemp, pCryptedData + n);
			}
		    aes256_done(&ctx);
//		 && aes256decrypt(pCryptedData, uiSize, aes_key, pMetaCrypto->auiSalt, g_aDecryptedBuf)
			const meta_header_t* pDecryptedMetaRecord = (const meta_header_t*)g_aDecryptedBuf;
			bResult = ProcessAllMetaRecords(pDecryptedMetaRecord, pMetaCrypto->uiUnpadedSize, internal_context);
		} else {
			bResult = false;
		}
	}
	return bResult;
#else // !USE_BOOTLOADER_AES
	return false;
#endif // USE_BOOTLOADER_AES
}
bool ProcessMetaZ(const uint8_t* pMetaData, uint32_t uiMetaSize, meta_context_t& context)
{
#ifdef USE_BOOTLOADER_ZIP
	bool bResult = false;
	if (context.bValid) {
		const uint8_t* pCompressedData = (const uint8_t*)pMetaData;
		size_t uiCompressedSize = uiMetaSize;
		const uint8_t* pDecompressedData = (const uint8_t*)g_aDecompressedBuf;
		size_t ulDecompressedSize = sizeof(g_aDecompressedBuf);
		static tinfl_decompressor g_tDecompressor;
		tinfl_init(&g_tDecompressor);
		tinfl_status eStatus = tinfl_decompress(&g_tDecompressor,
				(const mz_uint8*)pCompressedData, &uiCompressedSize,
				(mz_uint8*)pDecompressedData, (mz_uint8*)pDecompressedData, &ulDecompressedSize,
				TINFL_FLAG_PARSE_ZLIB_HEADER | TINFL_FLAG_USING_NON_WRAPPING_OUTPUT_BUF);
		if (eStatus == TINFL_STATUS_DONE) {
			const meta_header_t* pDecompressedMetaRecord = (const meta_header_t*)pDecompressedData;
			bResult = ProcessAllMetaRecords(pDecompressedMetaRecord, ulDecompressedSize, context);
		} else {
			bResult = false;
		}
	}
	return bResult;
#else // !USE_BOOTLOADER_ZIP
	return false;
#endif // USE_BOOTLOADER_ZIP
}
bool ProcessSingleMetaRecord(meta_t eMetaType, uint32_t uiMetaSize, const uint8_t* pMetaData, meta_context_t& context)
{
	SetLedState(context.bSecured ? LED_PROCESSING_SECURED : (context.bSkipFlash ? LED_PROCESSING : LED_PROCESSING_SKIP));
	switch (eMetaType) {
		case MT_NONE:       return true;
		case MT_SEGMENT:    return ProcessMetaSegment(pMetaData, uiMetaSize, context);
		case MT_ERASE:      return ProcessMetaErase(pMetaData, uiMetaSize, context);
		case MT_MCUIDS:     return ProcessMetaMcuIds(pMetaData, uiMetaSize, context);
		case MT_UNIQIDS:    return ProcessMetaUniqIds(pMetaData, uiMetaSize, context);
		case MT_SHA256:     return ProcessMetaDigest(pMetaData, uiMetaSize, context);
		case MT_CRYPTO:     return ProcessMetaCrypto(pMetaData, uiMetaSize, context);
		case MT_Z:          return ProcessMetaZ(pMetaData, uiMetaSize, context);
		case MT__EOF:       return true;
	}
	return false;
}
bool ProcessAllMetaRecords(const meta_header_t* pFirstMetaRecord, unsigned uiMaxSize, meta_context_t& context)
{
	bool bResult = false;
	const meta_header_t* pMetaRecord = pFirstMetaRecord;
	unsigned uiSizeLeft = uiMaxSize;
	while (pMetaRecord && uiSizeLeft) {
		bResult = (uiSizeLeft >= sizeof(meta_header_t));
		if (!bResult) break;
		if (pMetaRecord->eMetaType == MT__EOF) break;
		bResult = (pMetaRecord->uiSize && uiSizeLeft - sizeof(meta_header_t) >= pMetaRecord->uiSize);
		if (!bResult) break;
		bResult = ProcessSingleMetaRecord(pMetaRecord->eMetaType, pMetaRecord->uiSize, pMetaRecord->meta_data, context);
		if (!bResult) break;
		uiSizeLeft -= sizeof(meta_header_t) + pMetaRecord->uiSize;
		pMetaRecord = (const meta_header_t*)(pMetaRecord->meta_data + pMetaRecord->uiSize);
	}
	return bResult;
}
bool ProcessFwUpdateImage()
{
	bool bResult;
	const meta_header_t* pMetaRecord = (const meta_header_t*)GetFwUpdatePtr();
	unsigned uiSizeLeft = GetFwUpdateSize();
	meta_context_t context = {false};
	context.bSkipFlash = true;
	bResult = ProcessAllMetaRecords(pMetaRecord, uiSizeLeft, context);
	if (bResult) {
		context.bSkipFlash = false;
		bResult = 1
			&& EraseFirmware()
			&& ProcessAllMetaRecords(pMetaRecord, uiSizeLeft, context);
	}
	return bResult;
}

/*void Bootloader_RemapIsrTableToRam()
{
	// Copy interrupt vector table to the RAM.
	const uint32_t* pSrc = GetLinkerSymbol(const uint32_t*, isr_vector_start);
	const uint32_t* pSrcEnd = GetLinkerSymbol(const uint32_t*, isr_vector_end);
	volatile uint32_t* pTgt = GetLinkerSymbol(volatile uint32_t*, ram_isr_vector_start);

	while (pSrc < pSrcEnd) {
		*pTgt++ = *pSrc++;
	}
	SYSCFG_MemoryRemapConfig(SYSCFG_MemoryRemap_SRAM);
	IF_STM32F4(NVIC_SetVectorTable(NVIC_VectTab_RAM, 0));
}
*/
static inline void Bootloader_StartFirmware()
{
//	Bootloader_RemapIsrTableToRam();

	uint32_t* fw_isr_tab = (uint32_t*)GetFwCodePtr();
	uint32_t SP = fw_isr_tab[0];
	uint32_t RH = fw_isr_tab[1];
	if (SP != 0xFFFFFFFF || RH != 0xFFFFFFFF ) {
		BootJump(fw_isr_tab[0], fw_isr_tab[1]);
	} else {
		// no firmware flashed
		SetLedState(LED_DONE_ERROR);
		while (true) Delay(); // Infinite Blink Error
	}
}

void BootJump( uint32_t SP, uint32_t RH )
{
	/* Load application stack pointer.*/
	__set_MSP(SP);
	/* Load application Reset_Handler and jump to it.*/
	typedef void (*fn_ptr_t)(void);
	fn_ptr_t Firmware_Reset_Handler = (fn_ptr_t) RH;
	Firmware_Reset_Handler();
	while(true) {} // remove 'noreturn' warning
}


#define FLASH_WAIT ( \
	IF_STM32F0(FLASH_WaitForLastOperation(0) == FLASH_COMPLETE) \
	IF_STM32F4(FLASH_WaitForLastOperation() == FLASH_COMPLETE) \
)

#ifdef __cplusplus
extern "C" {
#endif

void main(void)
{
//	SetLedState(LED_PROCESSING); for (unsigned n = 0; n < 20; ++n) Delay(); // Blink Result
//	SetLedState(LED_PROCESSING_SUCCESS); for (unsigned n = 0; n < 20; ++n) Delay(); // Blink Result
//	SetLedState(LED_DONE_SUCCESS); for (unsigned n = 0; n < 20; ++n) Delay(); // Blink Result
//	SetLedState(LED_PROCESSING); for (unsigned n = 0; n < 20; ++n) Delay(); // Blink Result
//	SetLedState(LED_PROCESSING_ERROR); for (unsigned n = 0; n < 20; ++n) Delay(); // Blink Result
//	SetLedState(LED_DONE_ERROR); for (unsigned n = 0; n < 20; ++n) Delay(); // Blink Result
#ifndef DEBUG
	bool bOpConfigNeeded = !FLASH_OB_GetRDP();
	if (bOpConfigNeeded) {  // on 1st restart after reflash
		SetLedState(LED_PROCESSING);
		FLASH_OB_Unlock();
		bool bResult = FLASH_WAIT;
		//Set BOR (brownout reset) to Vbor1: 2.1 - 2.4V
		IF_STM32F4(
			FLASH_OB_BORConfig(OB_BOR_LEVEL1);
			bResult = FLASH_WAIT && bResult;
		)
		FLASH_OB_UserConfig(OB_IWDG_HW, OB_STOP_NoRST, OB_STDBY_NoRST);
		bResult = FLASH_WAIT && bResult;
		//Set RDP (read protection) to level 1
		FLASH_OB_RDPConfig(OB_RDP_Level_1);
		bResult = FLASH_WAIT && bResult;
		FLASH_OB_Launch();
		bResult = FLASH_WAIT && bResult;
		FLASH_OB_Lock();
		bResult = FLASH_WAIT && bResult;
		if (bResult) {
			SetLedState(LED_DONE_SUCCESS);
			for (unsigned n = 0; n < 10; ++n) Delay(); // Blink Success
		} else {
			SetLedState(LED_DONE_ERROR);
			while (true) Delay(); // Infinite Blink Error
		}
	}
#endif
	if (!CheckUpdateUploaded()) {
		// do nothing
	} else {
		SetLedState(LED_PROCESSING);
		bool bResult = ProcessFwUpdateImage();
		SetLedState(bResult ? LED_PROCESSING_SUCCESS : LED_PROCESSING_ERROR);
		EraseUpdateImage();
		SetLedState(bResult ? LED_DONE_SUCCESS : LED_DONE_ERROR);
		for (unsigned n = 0; n < 10; ++n) Delay(); // Blink Result
	}

	Bootloader_StartFirmware();
}

void fault(void)
{
	SetLedState(LED_DONE_ERROR);
	while (true) Delay(); // Infinite Blink
}

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus
