#include "stdafx.h"
#include "util/core.h"
#include "stm32_linker.h"
#include "stm32f_irqs.h"
#include "stm32fxxxx.h"

#ifdef __cplusplus
extern "C" {
#endif

void Reset_Handler(void);
void Defaut_Handler(void);
extern void main(void);
extern void fault(void);


#define ISR_ENTRY_DECLARE(name)     extern void name(void);
#define ISR_TABLE_ENTRY(name)       name,
#define ISR_DEFAULT(name)           void name(void) __attribute__((weak, alias("Defaut_Handler")));

ISR_TABLE_ITERATOR(Handler,ISR_ENTRY_DECLARE);

__attribute__ ((section(".isr_vector"), used))
void (* const g_pfnBootloaderIsrVectors[])(void) =
{
	GetLinkerSymbol(void(*)(), estack),  /* Initial value for stack pointer register */
	Reset_Handler,            /* Reset */
	ISR_TABLE_ITERATOR(Handler,ISR_TABLE_ENTRY)  /* Interrupts */
};

ISR_TABLE_ITERATOR(Handler,ISR_DEFAULT);

void Reset_Handler(void)
{
	__set_MSP((uint32_t)g_pfnBootloaderIsrVectors[0]);
	/* Copy the data segment initializers from flash to SRAM */
	const int* pSrc = GetLinkerSymbol(const int*, boot_sidata);
	int* pDst = GetLinkerSymbol(int*, boot_sdata);
	for (; pDst < GetLinkerSymbol(int*, boot_edata); ++pSrc, ++pDst) {
		*pDst = *pSrc;
	}
	/* Zero fill the bss segment. */
	for (int* pDst = GetLinkerSymbol(int*, boot_sbss); pDst < GetLinkerSymbol(int*, boot_ebss); ++pDst) {
		*pDst = 0;
	}
	/* call static constructors. */
	for (void (**p_fn)() = GetLinkerSymbol(void (**)(), boot_init_array_start); p_fn < GetLinkerSymbol(void (**)(), boot_init_array_end); ++p_fn) {
		(*p_fn)();
	}
	SystemInit();

	main();
}

void __attribute__((weak)) Defaut_Handler(void)
{
	fault();
}

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus
