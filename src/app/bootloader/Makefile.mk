help: helpbootloader
helpbootloader:
	@echo ==== bootloder ==========
	@echo make someapp USE_BOOTLOADER=YES
	@echo make flash_bootloader
	@echo make flash
	@echo make update
	@echo make flashupdate_myapp gdb_bootloader
	@echo make updateupload_myapp_release

FLASH_PROTECTION?=NO
ifeq ($(APP), bootloader)
BOARD?=DUMMY
ifeq (,$(BOARD))
MPUS?=STM32F407VG STM32F042F6 STM32F030F4
endif
DEFINES += \
	MINIZ_NO_MALLOC \
	$(if $(filter STM32F0%, $(MPU)), INTERRUPTS_REDIRECTION, INTERRUPTS_DISABLE)
SRC += $(addprefix $(APP_PATH)/, \
	main.cpp \
	sha256.cpp \
	aes256.c \
	tinfl.c \
)
OPTIMIZATION=LTO
#ignore tinfl.c compilation warnings
%/tinfl.o %/tinfl.o.asm: CFLAGS += -w

else # APP != bootloader

ifneq ($(DEBUG),)
ifneq ($(APP),)
ifneq ($(BOARD),)
ifneq ($(MPU),)
.PHONY: update updateflash flashupdate
all: bootloaderall
flash: bootloaderflash
all: update
update: $(TARGET).update.bin $(TARGET).update.dfu $(TARGET).update.hex
updateflash flashupdate: $(TARGET).update.bin.flashed.$(STLINK_UID).ver
endif # MPU
endif # BOARD
endif # APP
endif # DEBUG
endif # APP == bootloader

UPDATE_KEY_PATH?=$(APPS_PATH) $(HOME) ..
UPDATE_KEY_FILE?=$(wildcard $(addsuffix /update.key,$(UPDATE_KEY_PATH)))
UPDATE_KEY?=$(call CAT,$(UPDATE_KEY_FILE))
UPDATE_PASSWORD_FILE?=$(wildcard $(addsuffix /update-password.txt,$(UPDATE_KEY_PATH)))
#UPDATE_PASSWORD?=$(call CAT,$(UPDATE_PASSWORD_FILE))
UPDATE_UIDS?=$(STLINK_UID)

KNOWN_UNIQ_DEVICES = \
	$(firstword 2F0039001547    my Discovery2) \
	$(firstword 1c002e001547    rover pcb - mykolajiv) \
	$(firstword 1c002d001547    rover pcb - lviv)  

BOOTLOADER_PARTITION_LAYOUT ?= $(strip \
	$(if $(filter 1_16,    $(FLASH_PAGE)_$(FLASH_SIZE)), 4 6, ) \
	$(if $(filter 2_16,    $(FLASH_PAGE)_$(FLASH_SIZE)), 4 6, ) \
	$(if $(filter 1_32,    $(FLASH_PAGE)_$(FLASH_SIZE)), 4 14, ) \
	$(if $(filter 2_32,    $(FLASH_PAGE)_$(FLASH_SIZE)), 4 14, ) \
	$(if $(filter 1_64,    $(FLASH_PAGE)_$(FLASH_SIZE)), 4 30, ) \
	$(if $(filter 2_64,    $(FLASH_PAGE)_$(FLASH_SIZE)), 4 30, ) \
	$(if $(filter 1_128,   $(FLASH_PAGE)_$(FLASH_SIZE)), 16 38, ) \
	$(if $(filter 2_128,   $(FLASH_PAGE)_$(FLASH_SIZE)), 16 38, ) \
	$(if $(filter 128_64,  $(FLASH_PAGE)_$(FLASH_SIZE)), 16 16, ) \
	$(if $(filter 128_128, $(FLASH_PAGE)_$(FLASH_SIZE)), 16 64, ) \
	$(if $(filter 128_256, $(FLASH_PAGE)_$(FLASH_SIZE)), 16 128, ) \
	$(if $(filter 128_512, $(FLASH_PAGE)_$(FLASH_SIZE)), 16 128, ) \
	$(if $(filter 128_1024,$(FLASH_PAGE)_$(FLASH_SIZE)), 16 256, ) \
	$(if $(filter 128_2048,$(FLASH_PAGE)_$(FLASH_SIZE)), 16 256, ) \
)

BOOTLOADER_PARTITION_SIZE = $(word 1,$(BOOTLOADER_PARTITION_LAYOUT))
FIRMWARE_PARTITION_SIZE = $(call subtract, $(FLASH_SIZE), $(call plus, $(BOOTLOADER_PARTITION_SIZE), $(UPDATE_PARTITION_SIZE)))
UPDATE_PARTITION_SIZE = $(word 2,$(BOOTLOADER_PARTITION_LAYOUT))

USE_BOOTLOADER_AES = $(strip $(if $(call gt, $(BOOTLOADER_PARTITION_SIZE),6), YES))
USE_BOOTLOADER_ZIP = $(strip $(if $(call gt, $(BOOTLOADER_PARTITION_SIZE),12), YES))

DEFINES += \
	BOOTLOADER_PARTITION_SIZE=$(BOOTLOADER_PARTITION_SIZE) \
	FIRMWARE_PARTITION_SIZE=$(FIRMWARE_PARTITION_SIZE) \
	UPDATE_PARTITION_SIZE=$(UPDATE_PARTITION_SIZE) \
	$(if $(filter bootloader,$(APP)), \
		FLASH_OFFSET=0 \
		FLASH_RESERVED=$(call CALCULATE, $(FLASH_SIZE)-$(BOOTLOADER_PARTITION_SIZE)) \
		$(if $(UPDATE_KEY), UPDATE_KEY=$(call PY_PRINT, (",".join(["0x{:02X}".format(ord(b)) for b in binascii.unhexlify("$(UPDATE_KEY)")])))) \
		$(if $(UPDATE_PASSWORD), UPDATE_PASSWORD="$(UPDATE_PASSWORD)") \
	, \
		FLASH_OFFSET=$(BOOTLOADER_PARTITION_SIZE) \
		FLASH_RESERVED=$(UPDATE_PARTITION_SIZE) \
	) \

TARGET_INFO += \
	USE_BOOTLOADER=$(USE_BOOTLOADER) \
	BOOTLOADER_PARTITION_SIZE=$(BOOTLOADER_PARTITION_SIZE) \
	FIRMWARE_PARTITION_SIZE=$(FIRMWARE_PARTITION_SIZE) \
	UPDATE_PARTITION_SIZE=$(UPDATE_PARTITION_SIZE) \
	USE_BOOTLOADER_AES=$(USE_BOOTLOADER_ZIP) \
	USE_BOOTLOADER_ZIP=$(USE_BOOTLOADER_ZIP) \
	FLASH_PAGE=$(FLASH_PAGE) \
	FLASH_OFFSET=$(patsubst FLASH_OFFSET=%,%,$(filter FLASH_OFFSET=%,$(DEFINES))) \
	FLASH_RESERVED=$(patsubst FLASH_RESERVED=%,%,$(filter FLASH_RESERVED=%,$(DEFINES))) \
	FLASH_UPDATE_START=$(FLASH_UPDATE_START) \

FLASH_START = $(if $(filter bootloader,$(APP)),0x08000000,0x$(call CALCULATE, 0x08000000 + $(BOOTLOADER_PARTITION_SIZE)*1024, 08X))
FLASH_UPDATE_START = 0x$(call CALCULATE, 0x08000000 + ($(FLASH_SIZE) - $(UPDATE_PARTITION_SIZE))*1024, 08X)

.PHONY: bootloaderall bootloaderflash flashbootloader updateupload uploadupdate
bootloaderall:
	@echo ##### build bootloader for board $(BOARD) MPU $(MPU) #####
	@$(MAKE_CMD) APP=bootloader BOARD=$(BOARD) MPU=$(MPU) DEBUG=$(DEBUG) all
bootloaderflash flashbootloader:
	@echo ##### flashbootloader for board $(BOARD) MPU $(MPU) #####
	@$(MAKE_CMD) APP=bootloader BOARD=$(BOARD) MPU=$(MPU) DEBUG=$(DEBUG) flash
updateupload uploadupdate: update
	python "tools/fol.py" $(TARGET).update.bin
	python "tools/fol.py" -v $(TARGET).update.bin
	python "tools/fol.py" -r
%.update.bin %.update.hex: %.hex
	$(START)
	python "tools/hex2update.py" -a $(FLASH_UPDATE_START) \
		$(addprefix -D ,$(MPU_DEVID)) \
		$(addprefix -U ,$(UPDATE_UIDS)) \
		$(if $(filter YES,$(USE_BOOTLOADER_AES)), --aes \
			$(if $(UPDATE_KEY), -k $(UPDATE_KEY), \
			$(if $(UPDATE_PASSWORD), -p "$(UPDATE_PASSWORD)", \
			$(error no UPDATE_KEY or UPDATE_PASSWORD defined))) \
		, --no-aes) \
		$(if $(filter YES,$(USE_BOOTLOADER_ZIP)),--zip,--no-zip) \
		--verbose -o $@ $<
%.update.dfu: %.update.bin
	$(START)
	python "tools/dfu.py" -D 0x483:0xDF11 -b $(FLASH_UPDATE_START):$< $@
	