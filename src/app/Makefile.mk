ifneq ($(APP_PATH),)
APP ?= $(notdir $(APP_PATH))
APPS_PATH ?= $(dir $(APP_PATH))
else
APPS_PATH ?= src/app
ifneq ($(APP),)
APP_PATH ?= $(firstword $(filter %/$(APP),$(call SUBMKS,$(sort $(APPS_PATH)) src/app)))
endif
endif

APPS_SUGGESTED=$(notdir $(call SUBMKS,$(sort $(APPS_PATH))))
APPS_ALLOWED=$(sort $(APPS_SUGGESTED) $(notdir $(call SUBMKS,src/app)))

ifneq ($(APP),)
ifneq (,$(filter-out $(APPS_ALLOWED) all, $(APP)))
$(error invalid project $(APP). Allowed are: $(APPS_ALLOWED))  
endif

ifneq ($(wildcard $(APP_PATH)/Makefile.mk),)
include $(APP_PATH)/Makefile.mk
endif

ifeq ($(USE_BOOTLOADER),YES)
include src/app/bootloader/Makefile.mk
endif

INCLUDE_PATH += \
	$(APP_PATH)
DEFINES += \
    USE_APP_$(subst -,_,$(APP)) \
    APP=$(subst -,_,$(APP))

endif #APP
