#pragma once

#include "hw/hw.h"
#include "tick_process.h"

class CLedBlinkProcess :
	public CTickProcess
{
public:
	void Init();
	void EnableBlinking(bool bEnable);
public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime) override;

private:
	using LED = CHW_Brd::CDigitalOut_LED;
};

extern CLedBlinkProcess g_LedBlinkProcess;
