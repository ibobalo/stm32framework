#include "stdafx.h"
#include "btn_process.h"
#include "led_blink_process.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	constexpr bool bButtonToGND = (
#ifdef USE_BOARD_nucleof072rb
		true
#else
		false
#endif
	);

	g_LedBlinkProcess.Init();
	CBtnProcess::COnChangeCallback cb = BIND_MEM_CB(&CLedBlinkProcess::EnableBlinking, &g_LedBlinkProcess);
	g_BtnProcess.Init(bButtonToGND, cb);
}

} // extern "C"
