#include "stdafx.hpp"
#include "btn_process.h"

void CBtnProcess::Init(bool bInverted, COnChangeCallback cbOnChange)
{
	CTickProcess::Init(&g_TickProcessScheduller);

	m_cbOnChange = cbOnChange;
	m_bInverted = bInverted;
	BTN::Acquire(bInverted, false);
	m_bState = BTN::GetValue();
	m_cbOnChange(m_bState != bInverted);
	ContinueDelay(20);
}

void CBtnProcess::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));

	bool bBtnState = BTN::GetValue();
	if (bBtnState != m_bState) {
		m_bState = bBtnState;
		m_cbOnChange(bBtnState != m_bInverted);
	}

	ContinueDelay(20);
}


CBtnProcess g_BtnProcess;
