#pragma once

#include "hw/hw.h"
#include "tick_process.h"
#include "util/callback.h"

class CBtnProcess :
	public CTickProcess
{
public:
	using COnChangeCallback = Callback<void (bool)>;

public:
	void Init(bool bInverted, COnChangeCallback cbOnChange);
public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime) override;

private:
	using BTN = CHW_Brd::CDigitalIn_BTN;

	COnChangeCallback          m_cbOnChange;
	bool                       m_bInverted;
	bool                       m_bState;
};

extern CBtnProcess g_BtnProcess;
