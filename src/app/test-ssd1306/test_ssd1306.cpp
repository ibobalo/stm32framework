#include "stdafx.h"
#include "test_ssd1306.h"
#include "hw/hw.h"

#include "hw/devices/ssd1306/dev_ssd1306.h"
#include "util/utils.h"

void CTestSSD1306Process::Init()
{
	CTickProcess::Init(&g_TickProcessScheduller);
	II2C* pI2C = CHW_Brd::AcquireI2CInterface_I2C1(400000);
	m_pSSD1306Display = AcquireSSD1306TextDisplayInterface(pI2C);
	m_pSSD1306Display->PutString("init.. " STR(GIT_TIMESTAMP) "\n", true);

	ContinueNow();
}

/*virtual*/
void CTestSSD1306Process::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));

	unsigned uiTime = CHW_Clocks::GetSysTick();

	m_pSSD1306Display->PutChar(uiTime & 0x7F, true);

	ContinueDelay(100);
}

#define PUT_ROTATOR_CHAR(CNT) \
	static unsigned nRotator = 0; \
	m_pSSD1306Display->PutChar("|/-\\|/-\\"[nRotator%8], true); \
	static unsigned nLastCount = 0; \
	unsigned nCountDelta = CNT - nLastCount; \
	nLastCount = CNT; \
	if (nCountDelta) ++nRotator;
#define PUT_FREQ(LEN) \
	static CTimeType ttLastTime = 0; \
	CTimeType ttNow = CTimeSource::GetTimeNow(); \
	CTimeDelta td = ttNow - ttLastTime; \
	ttLastTime = ttNow; \
	unsigned freq = nCountDelta * 1000 / CTimeSource::GetMilliFromDelta(td); \
	m_pSSD1306Display->PutString("f:", false); \
	m_pSSD1306Display->PutString(AlignString<LEN>(ToString<LEN>(freq)), false); \

CTestSSD1306Process g_TestSSD1306Process;
