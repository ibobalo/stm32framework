#ifndef _TEST_SSD1306_H_INCLUDED
#define _TEST_SSD1306_H_INCLUDED

#include "stdafx.h"
#include <stdint.h>
#include "tick_process.h"
#include "interfaces/display.h"

class CTestSSD1306Process :
	public CTickProcess
{
public:
	void Init();
public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);
private:
	ITextDisplayDeviceMonochrome* m_pSSD1306Display;
};

extern CTestSSD1306Process g_TestSSD1306Process;

#endif // _TEST_SSD1306_H_INCLUDED
