#include "stdafx.h"
#include "serial_btnled.h"

void CSerialBtnLed::Init(COnRxCallback cbOnRx)
{
	m_cbOnRx = cbOnRx;

	UART::Acquire();

	const TSerialConnectionParams params = {
    		.uiBaudRate = 9600,
			.eStopBits = TSerialConnectionParams::StopBits::stop1,
			.eParityBits = TSerialConnectionParams::ParityBits::parityNo
    };

	UART::SetConnectionParams(&params);
	UART::SetSerialRxNotifier(this);
	UART::SetSerialTxNotifier(this);

	CIoChunk RxChunk;
	RxChunk.Assign(m_aRxBuff, 1);
	UART::StartDataRecv(RxChunk);

	SendHelpMsg();
}

void CSerialBtnLed::SendHelpMsg()
{
	if (UART::IsSending()) return;
	CConstChunk TxChunk;
	TxChunk.Assign(reinterpret_cast<const uint8_t*>("\r\n0 - led off, 1 - led on, ? - check button state\r\n"), 51);
	UART::StartDataSend(TxChunk);
}

void CSerialBtnLed::SendBtnMsg(bool bBtnState)
{
	if (UART::IsSending()) return;
	CConstChunk TxChunk;
	if (bBtnState) {
		TxChunk.Assign(reinterpret_cast<const uint8_t*>("Button pressed\r\n"), 16);
	} else {
		TxChunk.Assign(reinterpret_cast<const uint8_t*>("Button released\r\n"), 17);
	}
	UART::StartDataSend(TxChunk);
}

/*virtual*/
bool CSerialBtnLed::NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{ //returns true in next chunk set
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxPart(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialBtnLed::NotifyRxDone(CIoChunk* pRetNextChunk)
{ //returns true in next chunk set
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxDone(pRetNextChunk));
	const uint8_t uiRxChar = m_aRxBuff[0];
	m_cbOnRx(uiRxChar);
	// receive next char
	pRetNextChunk->Assign(m_aRxBuff, 1);
	return true;
}

/*virtual*/
bool CSerialBtnLed::NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxIdle(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialBtnLed::NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxTimeout(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialBtnLed::NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxError(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialBtnLed::NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxOverflow(ccData, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialBtnLed::NotifyTxDone(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
	return false;
}

/*virtual*/
bool CSerialBtnLed::NotifyTxComplete(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
	// ignore
	return false;
}

/*virtual*/
bool CSerialBtnLed::NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
	return false;
}

/*virtual*/
bool CSerialBtnLed::NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
	return false;
}


CSerialBtnLed g_SerialBtnLed;
