#include "stdafx.h"
#include "serial_btnled.h"
#include "util/callback.h"

extern "C" {

constexpr bool bBtnInverted = (
#ifdef USE_BOARD_nucleof072rb
		true
#else
		false
#endif
);

void on_rx(uint8_t ch) {
	switch (ch) {
	case '1':
		CHW_Brd::CDigitalOut_LED::Set();
		break;
	case '0':
		CHW_Brd::CDigitalOut_LED::Reset();
		break;
	case '?':
		g_SerialBtnLed.SendBtnMsg(CHW_Brd::CDigitalIn_BTN::GetValue() != bBtnInverted);
		break;
	default:
		g_SerialBtnLed.SendHelpMsg();
		break;
	}
}

void app_init(void)
{
	CHW_Brd::CDigitalOut_LED::Init(false);
	CHW_Brd::CDigitalIn_BTN::Init(false, false);

	CSerialBtnLed::COnRxCallback cbOnRx = BIND_FREE_CB(&on_rx);
	g_SerialBtnLed.Init(cbOnRx);
//	CBtnExti::COnChangeCallback cbOnBtn = BIND_MEM_CB(&CSerialHello::SendBtnMsg, &g_UartHello);
//	g_BtnExti.Init(cbOnBtn);
}

} // extern "C"
