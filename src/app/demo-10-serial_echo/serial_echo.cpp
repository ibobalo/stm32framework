#include "stdafx.h"
#include "serial_echo.h"

void CSerialEcho::Init(COnRxCallback cbOnRx)
{
	m_cbOnRx = cbOnRx;

	UART::Acquire();

	const TSerialConnectionParams params = {
    		.uiBaudRate = 9600,
			.eStopBits = TSerialConnectionParams::StopBits::stop1,
			.eParityBits = TSerialConnectionParams::ParityBits::parityNo
    };

	UART::SetConnectionParams(&params);
	UART::SetSerialRxNotifier(this);
	UART::SetSerialTxNotifier(this);

	CIoChunk RxChunk;
	RxChunk.Assign(m_aRxBuff, 1);
	UART::StartDataRecv(RxChunk);
}

void CSerialEcho::SendEchoChar(uint8_t uiEcho)
{
	if (UART::IsSending()) return;
	m_aTxBuff[0] = uiEcho;
	CConstChunk TxChunk;
	TxChunk.Assign(m_aTxBuff, 1);
	UART::StartDataSend(TxChunk);
}

/*virtual*/
bool CSerialEcho::NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{ //returns true in next chunk set
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxPart(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialEcho::NotifyRxDone(CIoChunk* pRetNextChunk)
{ //returns true in next chunk set
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxDone(pRetNextChunk));
	const uint8_t uiRxChar = m_aRxBuff[0];
	m_cbOnRx(uiRxChar);
	SendEchoChar(uiRxChar);
	// receive next char
	pRetNextChunk->Assign(m_aRxBuff, 1);
	return true;
}

/*virtual*/
bool CSerialEcho::NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxIdle(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialEcho::NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxTimeout(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialEcho::NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxError(uiBytesLeft, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialEcho::NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxOverflow(ccData, pRetNextChunk));
	return true; // keep RXing
}

/*virtual*/
bool CSerialEcho::NotifyTxDone(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
	return false;
}

/*virtual*/
bool CSerialEcho::NotifyTxComplete(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
	// ignore
	return false;
}

/*virtual*/
bool CSerialEcho::NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
	return false;
}

/*virtual*/
bool CSerialEcho::NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
	return false;
}


CSerialEcho g_SerialEcho;
