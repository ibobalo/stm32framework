#include "stdafx.h"
#include "serial_echo.h"
#include "util/callback.h"

extern "C" {

void on_rx(uint8_t ch) {
	CHW_Brd::CDigitalOut_LED::Toggle();
}

void app_init(void)
{
	CHW_Brd::CDigitalOut_LED::Init(false);

	CSerialEcho::COnRxCallback cbOnRx = BIND_FREE_CB(&on_rx);
	g_SerialEcho.Init(cbOnRx);
}

} // extern "C"
