#pragma once

#include "hw/hw.h"
#include "util/callback.h"

class CSerialEcho :
		public ISerialRxNotifier,
		public ISerialTxNotifier
{
public:
	typedef Callback<void (uint8_t)> COnRxCallback;

public:
	void Init(COnRxCallback cbOnRx);
	void SendEchoChar(uint8_t uiEcho);

public: // ISerialRxNotifier
	virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxDone(CIoChunk* pRetNextChunk);
	virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk);
public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);

private:
	using UART = CHW_Brd::CSerial_1;
	COnRxCallback     m_cbOnRx;
	volatile uint8_t  m_aRxBuff[1];
	uint8_t           m_aTxBuff[1];
};

extern CSerialEcho g_SerialEcho;
