#include "hw/hw.h"

extern "C" {

void main(void)
{
	CHW_Brd::CDigitalOut_LED::Init(false);
	CHW_Brd::CDigitalIn_BTN::Init(false, false);

	while(true) {
		bool b = (CHW_Brd::CDigitalIn_BTN::GetValue());
		CHW_Brd::CDigitalOut_LED::SetValue(b);
	}
}

} // extern "C"
