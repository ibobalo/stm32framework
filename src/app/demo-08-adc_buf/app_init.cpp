#include "stdafx.h"
#include "ain_buf.h"
#include "led_value_timch.h"
#include "util/callback.h"

extern "C" {

void app_init(void)
{
	g_LedValueTimerCh.Init();
	CAInBuf::COnValueCallback cb = BIND_MEM_CB(&CLedValueTimerCh::SetValue, &g_LedValueTimerCh);
	g_AInBuf.Init(cb);
}

} // extern "C"
