#pragma once

#include "hw/hw.h"
#include "util/callback.h"

class CAInBuf :
	public IADCBufferNotifier,
	public IADCErrorNotifier
{
public:
	typedef Callback<void (uint8_t)> COnValueCallback;

public:
	void Init(COnValueCallback cbOnChange);

public: // IADCBufferNotifier
	virtual void NotifyADCBuffer() override;
public: // IADCErrorNotifier
	virtual void NotifyADCError() override;

private:
	using TAI0 = CHW_Brd::CAnalogIn_AI0;
	using TAI1 = CHW_Brd::CAnalogIn_AI1;
	using TAIt = CHW_Brd::CAnalogIn_TEMPSENS;
	using TAIv = CHW_Brd::CAnalogIn_VREFINT;
	using TADC = CHW_Brd::CADC_1;

	COnValueCallback          m_cbOnValue;
	volatile uint16_t         m_auiSamples[100 * 4];
};

extern CAInBuf g_AInBuf;
