#include "stdafx.h"
#include "ain_buf.h"

void CAInBuf::Init(COnValueCallback cbOnValue)
{
	m_cbOnValue = cbOnValue;
	unsigned uiSampleTime = (
			IF_STM32F0(ADC_SampleTime_239_5Cycles)
			IF_STM32F4(ADC_SampleTime_480Cycles)
	);
	TAI0::Acquire(uiSampleTime);
	TAI1::Acquire(uiSampleTime);
	TAIt::Acquire(uiSampleTime);
	TAIv::Acquire(uiSampleTime);

	TADC::Acquire();
	TADC::AttachAIN<TAI0::ChIndex>();
	TADC::AttachAIN<TAI1::ChIndex>();
	TADC::AttachAIN<TAIt::ChIndex>();
	TADC::AttachAIN<TAIv::ChIndex>();
	TADC::SetSamplesBuffer(m_auiSamples, ARRAY_SIZE(m_auiSamples));
	TADC::SetBufferNotifier(this);  INDIRECT_CALL(NotifyADCBuffer());
	TADC::SetErrorNotifier(this);  INDIRECT_CALL(NotifyADCError());
	TADC::InitAINs();
	TADC::Start();
}

/*virtual*/
void CAInBuf::NotifyADCBuffer()
{
	IMPLEMENTS_INTERFACE_METHOD(IADCBufferNotifier::NotifyADCBuffer());
	DEBUG_INFO("Buffer");
	m_cbOnValue(m_auiSamples[0] >> 4);
}

/*virtual*/
void CAInBuf::NotifyADCError()
{
	IMPLEMENTS_INTERFACE_METHOD(IADCErrorNotifier::NotifyADCError());
	DEBUG_INFO("Error");
	ASSERT("ADC OVR")
	// TODO
}

CAInBuf g_AInBuf;
