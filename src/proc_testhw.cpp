#include "stdafx.h"
#include "proc_testhw.h"
#include "util/macros.h"
#include "util/utils.h"
#include "hw/hw.h"
#include "zero_sync_pwm.h"
#include "proc_ramp.h"
#include "proc_keyscanner.h"
//#include "proc_dimmer_ctrl.h"
#include "hw/devices/hw_enc28j60.h"
#include "net/net_stack.h"
#include <stdio.h>

class CHWTestProcess :
	public IHWTestProcess,
	public CTickProcess
{
	typedef CTickProcess CParentProcess;
public: // IHWTest
	virtual void Init();
public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime);
private:
	void StopOtherProcesses();
	void RestartOtherProcesses();
	void SingleStep_Start(CTimeType uiTime);
	void SingleStep_TriacsOn(CTimeType uiTime);
	void SingleStep_KeyLedsOn(CTimeType uiTime);
	void SingleStep_TriacsOff(CTimeType uiTime);
	void SingleStep_KeyLedsOff(CTimeType uiTime);
	void SingleStep_TriacsRaise(CTimeType uiTime);
	void SingleStep_TriacsFall(CTimeType uiTime);
	void SingleStep_WaitTriacsDone(CTimeType uiTime);
	void SingleStep_NetworkTx(CTimeType uiTime);
	void SingleStep_Finish(CTimeType uiTime);

	unsigned m_nStep;
	unsigned m_nStepSub;
};

void CHWTestProcess::Init()
{
	IMPLEMENTS_INTERFACE_METHOD(IHWTestProcess::Init());

	CParentProcess::Init(&g_TickProcessScheduller);
	m_nStep = 0;
	m_nStepSub = 0;
	ContinueNow();
}

void CHWTestProcess::StopOtherProcesses()
{
	GetKeyboardInterface()->Control(false);
#ifdef USE_DIMMER
	GetDimmerControlerInterface()->Control(false);
#endif
}

void CHWTestProcess::RestartOtherProcesses()
{
	GetKeyboardInterface()->Control(true);
#ifdef USE_DIMMER
	GetDimmerControlerInterface()->Control(true);
#endif
}

void CHWTestProcess::SingleStep_Start(CTimeType uiTime)
{
	StopOtherProcesses();

#ifdef USE_USB
	HW->UsbDataSent("Testing HW...\r\n", 15);
   	HW->UsbDataSent("  CLK ", 6);
   	ToString<> conv(HW->GetCLKFrequency() / 1000000);
   	HW->UsbDataSent(conv.GetValue(), conv.GetLength());
   	HW->UsbDataSent("MHz, tick=", 10);
   	conv.Convert(HW->GetSysTick());
   	HW->UsbDataSent(conv.GetValue(), conv.GetLength());
   	HW->UsbDataSent(", CLK=", 6);
   	conv.Convert(HW->GetCLKCounter64());
   	HW->UsbDataSent(conv.GetValue(), conv.GetLength());
   	HW->UsbDataSent("\r\n", 2);
#ifdef PRINTF_TO_USB
	printf("Testing HW...\r\n");
	printf("  CLK %lu MGz (%lu Hz)\r\n", HW->GetCLKFrequency() / 1000000, HW->GetCLKFrequency());
#endif // PRINTF_TO_USB
#endif // USE_USB

	for (unsigned n = 0; n < 8; ++n) {
		HW->ClearKbdLed(n);
	}

	for (unsigned n = 0; n < 8; ++n) {
		HW->Set_UIO_Low(n, true);
	}

	++m_nStep;
	m_nStepSub = 0;
	ContinueNow();
}

void CHWTestProcess::SingleStep_TriacsOn(CTimeType uiTime)
{
	if (m_nStepSub < TRIAC_CHANNEL_COUNT) {
		IRamp* pDimmerRamp = GetDimmerRamp(m_nStepSub);
		pDimmerRamp->SetValueInstant(1.0);
		++m_nStepSub;
		ContinueAt(uiTime + 200);
	} else {
		++m_nStep;
		m_nStepSub = 0;
		ContinueNow();
	}
}

unsigned g_auiKeyTestSequence[8] = {0,2,4,6,1,3,5,7};

void CHWTestProcess::SingleStep_KeyLedsOn(CTimeType uiTime)
{
	if (m_nStepSub < ARRAY_SIZE(g_auiKeyTestSequence)) {
		HW->SetKbdLed(g_auiKeyTestSequence[m_nStepSub]);
		++m_nStepSub;
		ContinueAt(uiTime + 400);
	} else {
		++m_nStep;
		m_nStepSub = 0;
		ContinueNow();
	}
}

void CHWTestProcess::SingleStep_TriacsOff(CTimeType uiTime)
{
	if (m_nStepSub < TRIAC_CHANNEL_COUNT) {
		IRamp* pDimmerRamp = GetDimmerRamp(m_nStepSub);
		pDimmerRamp->SetValueInstant(0.0);
		++m_nStepSub;
		ContinueAt(uiTime + 200);
	} else {
		++m_nStep;
		m_nStepSub = 0;
		ContinueNow();
	}
}

void CHWTestProcess::SingleStep_KeyLedsOff(CTimeType uiTime)
{
	if (m_nStepSub < ARRAY_SIZE(g_auiKeyTestSequence)) {
		HW->ClearKbdLed(g_auiKeyTestSequence[m_nStepSub]);
		++m_nStepSub;
		ContinueAt(uiTime + 400);
	} else {
		++m_nStep;
		m_nStepSub = 0;
		ContinueNow();
	}
}

void CHWTestProcess::SingleStep_TriacsRaise(CTimeType uiTime)
{
	for (unsigned n = 0; n < TRIAC_CHANNEL_COUNT; ++n) {
		IRamp* pDimmerRamp = GetDimmerRamp(n);
		pDimmerRamp->SetValueWithEndTime(1.0, uiTime + 2000);
	}
	++m_nStep;
	ContinueAt(uiTime + 2000);
}

void CHWTestProcess::SingleStep_TriacsFall(CTimeType uiTime)
{
	for (unsigned n = 0; n < TRIAC_CHANNEL_COUNT; ++n) {
		IRamp* pDimmerRamp = GetDimmerRamp(n);
		pDimmerRamp->SetValueWithRate(0.0, 1.0/10000.0);
	}
	++m_nStep;
	ContinueAt(uiTime + 10);
}

void CHWTestProcess::SingleStep_WaitTriacsDone(CTimeType uiTime)
{
	for (unsigned n = 0; n < TRIAC_CHANNEL_COUNT; ++n) {
		IRamp* pDimmerRamp = GetDimmerRamp(n);
		if (!pDimmerRamp->IsStopped()) {
			ContinueAfter(pDimmerRamp);
			return;
		}
	}
	++m_nStep;
	ContinueNow();
}

void CHWTestProcess::SingleStep_NetworkTx(CTimeType uiTime)
{
	if (m_nStepSub < 3) {
#ifdef USE_NET
		static CChainedConstChunks UdpTestChunk;

		UdpTestChunk.Assign((const uint8_t*)"Test ping", 10);
		UdpTestChunk.Break();
		GetNetworkStackInterface()->SendUDP(IP_ADDR(192,168,1,1), 5006, &UdpTestChunk);
//		GetNetworkInterfaceENC28J60()->PushTxPkt(NULL, NULL, &UdpTestChunk);
#endif // USE_NET
		++m_nStepSub;
		ContinueAt(uiTime + 1000);
	} else {
		++m_nStep;
		m_nStepSub = 0;
		ContinueNow();
	}
}

void CHWTestProcess::SingleStep_Finish(CTimeType uiTime)
{
	for (unsigned n = 0; n < 8; ++n) {
		HW->Get_UIO_Value(n);
	}

#ifdef USE_USB
	HW->UsbDataSent("Test HW done\r\n", 15);
#endif //USE_USB

	m_nStep = 0;

	RestartOtherProcesses();

	// do not continue
}


void CHWTestProcess::SingleStep(CTimeType uiTime)
{
	switch (m_nStep) {
	case 0:
		SingleStep_Start(uiTime);
		break;
	case 1:
		SingleStep_TriacsOn(uiTime);
		break;
	case 2:
		SingleStep_KeyLedsOn(uiTime);
		break;
	case 3:
		SingleStep_TriacsOff(uiTime);
		break;
	case 4:
		SingleStep_KeyLedsOff(uiTime);
		break;
	case 5:
		SingleStep_TriacsRaise(uiTime);
		break;
	case 6:
		SingleStep_TriacsFall(uiTime);
		break;
	case 7:
		SingleStep_WaitTriacsDone(uiTime);
		break;
	case 8:
		SingleStep_NetworkTx(uiTime);
		break;
	case 9:
		SingleStep_Finish(uiTime);
		break;
	default:
		m_nStep = 0;
		break;
	}
}


CHWTestProcess g_HWTestProcess;
IHWTestProcess* GetHWTestInterface() { return &g_HWTestProcess; };
