#include "brd_demo.h"

#include "stdafx.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"

#define TIMx_N          1
# define TIMx_Ch2_PORT   A,9,AF_2
# define TIMx_Ch3_PORT   A,10,AF_2
//# define TIMx_Ch4_PORT   A,11
#include "hw/mcu/hw_tim.cxx"
ACQUIRE_RELEASE_TIMER_DEFINITION(CHW_Brd_Demo,1)
ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,1,2,CANRX)
//ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,1,4,CANRX)
ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,1,3,CANTX)

#define TIMx_N          3
# define TIMx_Ch1_PORT   A,6,AF_1
# define TIMx_Ch2_PORT   A,7,AF_1
# define TIMx_Ch4_PORT   B,1,AF_1
#include "hw/mcu/hw_tim.cxx"
ACQUIRE_RELEASE_TIMER_DEFINITION(CHW_Brd_Demo,3)
ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,3,1,IO5)
ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,3,2,IO4)
ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,3,4,IO3)

#define TIMx_N          14
# define TIMx_Ch1_PORT   A,4,AF_4
//# define TIMx_Ch1_PORT   A,7,AF_4
//# define TIMx_Ch1_PORT   B,1,AF_0
#include "hw/mcu/hw_tim.cxx"
ACQUIRE_RELEASE_TIMER_DEFINITION(CHW_Brd_Demo,14)
ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,14,1,IO7)
//ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,14,1,IO4)
//ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,14,1,IO3)

#define TIMx_N          16
//# define TIMx_Ch1_PORT   A,6,AF_5
#include "hw/mcu/hw_tim.cxx"
ACQUIRE_RELEASE_TIMER_DEFINITION(CHW_Brd_Demo,16)
//ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,16,1,IO5)

#define TIMx_N          17
//# define TIMx_Ch1_PORT   A,7,AF_5
#include "hw/mcu/hw_tim.cxx"
ACQUIRE_RELEASE_TIMER_DEFINITION(CHW_Brd_Demo,17)
//ACQUIRE_RELEASE_TIMER_CHANNEL_DEFINITION(CHW_Brd_Demo,17,1,IO4)


#ifdef USE_USART

/*----------- USARTx Interrupt Priority -------------*/
#define USARTx_IT_GROUP_PRIO              3   /* USARTx GROUP PRIORITY */
#define USARTx_IT_SUBPRIO                 2   /* USARTx SUB-PRIORITY */

/*************************************************************************************/

// USART1 - RS485
#define UxART       USART
#define UxARTx_N        1
#  define UxARTx_PORT_TX  A,2,AF1
#  define UxARTx_PORT_RX  A,3,AF1
#  define UxARTx_PORT_RTS A,1
#  define UxARTx_IT_GROUP_PRIO  USARTx_IT_GROUP_PRIO
#  define UxARTx_IT_SUBPRIO     USARTx_IT_SUBPRIO
//#  define UxARTx_DMA_N            1
// DMA1.6 used by I2C1
//#  define UxARTx_DMA_TX_STREAM_N  6
//#  define UxARTx_DMA_TX_CHANNEL_N 4
//#  define UxARTx_DMA_RX_STREAM_N  5
//#  define UxARTx_DMA_RX_CHANNEL_N 4
#include "hw/mcu/hw_uart.cxx"

#endif //USE_UART

#ifdef USE_ADC
#define AINx_PORT B,1,9
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(9, CHW_Brd_Demo, IO3)
#define AINx_PORT A,7,7
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(7, CHW_Brd_Demo, IO4)
#define AINx_PORT A,6,6
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(6, CHW_Brd_Demo, IO5)
#define AINx_PORT A,5,5
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(5, CHW_Brd_Demo, IO6)
#define AINx_PORT A,4,4
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(4, CHW_Brd_Demo, IO7)
#define AINx_PORT A,1,1
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(1, CHW_Brd_Demo, IO8)
#define AINx_PORT A,3,3
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(3, CHW_Brd_Demo, IO9)
#define AINx_PORT A,2,2
#include "hw/mcu/hw_adc_in.cxx"
ACQUIRE_RELEASE_ANALOG_IN_DEFINITION(2, CHW_Brd_Demo, IO10)
#define ADCx_N 1
#include "hw/mcu/hw_adc.cxx"
ACQUIRE_RELEASE_ADC_DEFINITION(1, CHW_Brd_Demo, 1)
#endif // USE_ADC

#ifdef USE_I2C
# define I2Cx_N                     1
# define I2Cx_SCL_GPIO_PORT         F,1,AF_1
# define I2Cx_SDA_GPIO_PORT         F,0,AF_1
# define I2Cx_IT_GROUP_PRIO         3
//# define I2Cx_DMA_TX                1,2,I2C
//# define I2Cx_DMA_RX                1,3,I2C
# include "hw/mcu/hw_i2c.cxx"
ACQUIRE_RELEASE_I2C_DEFINITION(1, CHW_Brd_Demo, I2C1)
#endif // USE_I2C

void CHW_Brd_Demo::Init()
{
	CHW_MCU::Init();
}

#ifdef USE_USART
void CHW_Brd_Demo::InitUSART(unsigned uiIndex, ISerialRxNotifier* pRxNotifier, ISerialTxNotifier* pTxNotifier)
{
	switch (uiIndex) {
	case 0:
		g_HW_USART1.Init();
		g_HW_USART1.SetSerialNotifier(pCallback);
		break;
	default:
		break;
	};
}

ISerial* CHW_Brd_Demo::GetUSART(unsigned uiIndex)
{
	ISerial* pSerial;
	switch (uiIndex) {
	case 0:
		pSerial = &g_HW_USART1;
		break;
	default:
		pSerial = NULL;
	};
	return pSerial;
}
#endif // USE_USART
