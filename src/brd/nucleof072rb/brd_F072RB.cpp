#include "stdafx.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"

#include "brd_F072RB.h"

#if defined(USE_EXTI_BTN)
#define USE_EXTI13
#endif

#if defined(USE_EXTI) || defined(USE_EXTI0)
#define DINx_PORT B,0
#  define DINx_IT_PRIO 1,1
#  include "hw/mcu/hw_din.cxx"
#endif
#if defined(USE_EXTI) || defined(USE_EXTI2)
#define DINx_PORT B,2
#  define DINx_IT_PRIO 1,1
#  include "hw/mcu/hw_din.cxx"
#endif
#if defined(USE_EXTI) || defined(USE_EXTI4)
#define DINx_PORT A,4
#  define DINx_IT_PRIO 1,1
#  include "hw/mcu/hw_din.cxx"
#endif
#if defined(USE_EXTI) || defined(USE_EXTI13)
#define DINx_PORT C,13
#  define DINx_IT_PRIO 1,1
#  include "hw/mcu/hw_din.cxx"
#endif

#if defined(USE_ADC) || defined(USE_ADC1)
#define AINx_PORT  A,0,0
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,1,1
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,2,2
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,3,3
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,4,4
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,5,5
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,6,6
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  A,7,7
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  B,0,8
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  B,1,9
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  C,0,10
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  C,1,11
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  C,2,12
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  C,3,13
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  C,4,14
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_PORT  C,5,15
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_CHANNEL ADC_CHANNEL_N_TEMPSENS
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_CHANNEL ADC_CHANNEL_N_VREFINT
#  include "hw/mcu/hw_adc_in.cxx"
#define AINx_CHANNEL ADC_CHANNEL_N_VBAT
#  include "hw/mcu/hw_adc_in.cxx"
#endif

#if defined(USE_ADC) || defined(USE_ADC1)
#define ADCx_N 1
#ifndef USE_ADC_WITHOUT_IT
#  define ADCx_IT_PRIO           1,1
#endif
#ifndef USE_ADC_WITHOUT_DMA
#  define ADCx_DMA               1,1
#  ifndef USE_ADC_WITHOUT_DMA_IT
#    define ADCx_DMA_IT_PRIO     1,1
#  endif
#endif
#  include "hw/mcu/hw_adc.cxx"
#endif // USE_ADC


#if defined(USE_TIM) || defined(USE_TIM1)
#define TIMx_N          1
#  define TIMx_Ch1_PORT   A,8,AF_2
#  define TIMx_Ch1N_PORT  B,13,AF_2
#  define TIMx_Ch2_PORT   A,9,AF_2
#  define TIMx_Ch3_PORT   A,10,AF_2
#  define TIMx_Ch4_PORT   A,11,AF_2
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_TIM) || defined(USE_TIM2)
#define TIMx_N          2
#  define TIMx_Ch2_PORT   A,1,AF_2
#  define TIMx_Ch3_PORT   B,10,AF_2
#  define TIMx_Ch4_PORT   B,11,AF_2
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_TIM) || defined(USE_TIM3)
#define TIMx_N          3
#  define TIMx_Ch1_PORT   B,4,AF_1
#  define TIMx_Ch2_PORT   B,5,AF_1
#  define TIMx_Ch3_PORT   B,0,AF_1
#  define TIMx_Ch4_PORT   C,9,AF_0
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_TIM) || defined(USE_TIM14)
#define TIMx_N          14
#  define TIMx_Ch1_PORT   A,4,AF_4
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_TIM) || defined(USE_TIM15)
#define TIMx_N          15
#  define TIMx_Ch1_PORT   A,2,AF_0
#  define TIMx_Ch1N_PORT  A,1,AF_5
#  define TIMx_Ch2_PORT   A,3,AF_0
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_TIM) || defined(USE_TIM16)
#define TIMx_N          16
#  define TIMx_Ch1_PORT   B,8,AF_2
#  define TIMx_Ch1N_PORT  B,6,AF_2
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_TIM) || defined(USE_TIM17)
#define TIMx_N          17
#  define TIMx_Ch1_PORT   B,9,AF_2
#  define TIMx_Ch1N_PORT  B,7,AF_2
#  define TIMx_IT_PRIO   3,1
#  include "hw/mcu/hw_tim.cxx"
#endif

#if defined(USE_USART) || defined(USE_USART1)
#define UxARTx_N        1
#  define UxART         USART
#  define UxARTx_PORT_TX  A,9,AF_1
#  define UxARTx_PORT_RX  A,10,AF_1
//#  define UxARTx_PORT_RTS A,12,AF_1 /* optional 485 */
#  define UxARTx_IT_PRIO  2,0
#  include "hw/mcu/hw_uart.cxx"
#endif

#if defined(USE_USART) || defined(USE_USART2)
#define UxARTx_N        2
#  define UxART         USART
#  define UxARTx_PORT_TX  A,2,AF_1
#  define UxARTx_PORT_RX  A,3,AF_1
//#  define UxARTx_PORT_RTS A,1,AF_1 /* optional 485 */
#  define UxARTx_IT_PRIO  2,1
//next defines is optional for DMA access
#  include "hw/mcu/hw_uart.cxx"
#endif

#if defined(USE_USART) || defined(USE_USART3)
#define UxARTx_N        3
#  define UxART         USART
#  define UxARTx_PORT_TX  C,4,AF_1
#  define UxARTx_PORT_RX  C,5,AF_1
#  define UxARTx_IT_PRIO  2,2
#  include "hw/mcu/hw_uart.cxx"
#endif

#if defined(USE_USART) || defined(USE_USART4)
#define UxARTx_N        4
#  define UxART         USART
#  define UxARTx_PORT_TX  A,0,AF_4
#  define UxARTx_PORT_RX  A,1,AF_4
//#  define UxARTx_PORT_RTS A,15,AF_4 /* optional 485 */
#  define UxARTx_IT_PRIO  2,3
//#  define UxARTx_DMA_TX   1,7
//#  define UxARTx_DMA_RX   1,6
#  include "hw/mcu/hw_uart.cxx"
#endif

#if defined(USE_I2C) || defined(USE_I2C1)
# define I2Cx_N                     1
#  define I2Cx_SCL_GPIO_PORT         B,8,AF_1
#  define I2Cx_SDA_GPIO_PORT         B,9,AF_1
#  define I2Cx_IT_GROUP_PRIO         3
#  include "hw/mcu/hw_i2c.cxx"
ACQUIRE_RELEASE_I2C_DEFINITION(1, CHW_Brd_NucleoF072RB, I2C1)
#endif

#if defined(USE_I2C) || defined(USE_I2C2)
# define I2Cx_N                     2
#  define I2Cx_SCL_GPIO_PORT         B,13,AF_5
#  define I2Cx_SDA_GPIO_PORT         B,14,AF_5
#  define I2Cx_IT_GROUP_PRIO         3
//#  define I2Cx_DMA_TX                1,4
//#  define I2Cx_DMA_RX                1,5
#  include "hw/mcu/hw_i2c.cxx"
ACQUIRE_RELEASE_I2C_DEFINITION(2, CHW_Brd_NucleoF072RB, I2C2)
#endif

#if defined(USE_SPI) || defined(USE_SPI1)
# define SPIx_N                     1
#  define SPIx_SCK_GPIO_PORT         B,3,AF_0
#  define SPIx_MISO_GPIO_PORT        B,4,AF_0
#  define SPIx_MOSI_GPIO_PORT        B,5,AF_0
#  define SPIx_DMA_TX                1,2
#  define SPIx_DMA_RX                1,3
#  include "hw/mcu/hw_spi.cxx"
ACQUIRE_RELEASE_SPI_DEFINITION(1, CHW_Brd_NucleoF072RB, SPI1)
#endif

#if defined(USE_SPI) || defined(USE_SPI2)
# define SPIx_N                     2
#  define SPIx_SCK_GPIO_PORT         B,13,AF_0
#  define SPIx_MISO_GPIO_PORT        B,14,AF_0
#  define SPIx_MOSI_GPIO_PORT        B,15,AF_0
#  include "hw/mcu/hw_spi.cxx"
ACQUIRE_RELEASE_SPI_DEFINITION(2, CHW_Brd_NucleoF072RB, SPI2)
#endif // USE_SPI


void CHW_Brd_NucleoF072RB::Init()
{
	CHW_MCU::Init();
#ifdef DEBUG
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_DBGMCU, ENABLE);
	DBGMCU_APB1PeriphConfig(DBGMCU_TIM2_STOP, ENABLE);
	DBGMCU_APB1PeriphConfig(DBGMCU_TIM3_STOP, ENABLE);
	DBGMCU_APB1PeriphConfig(DBGMCU_TIM6_STOP, ENABLE);
	DBGMCU_APB1PeriphConfig(DBGMCU_TIM7_STOP, ENABLE);
	DBGMCU_APB1PeriphConfig(DBGMCU_TIM14_STOP, ENABLE);
	DBGMCU_APB2PeriphConfig(DBGMCU_TIM1_STOP, ENABLE);
	DBGMCU_APB2PeriphConfig(DBGMCU_TIM15_STOP, ENABLE);
	DBGMCU_APB2PeriphConfig(DBGMCU_TIM16_STOP, ENABLE);
	DBGMCU_APB2PeriphConfig(DBGMCU_TIM17_STOP, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_DBGMCU, DISABLE);
#endif
}
