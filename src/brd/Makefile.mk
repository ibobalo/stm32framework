ifneq ($(BOARD_PATH),)
BOARD ?= $(notdir $(BOARD_PATH))
BOARDS_PATH ?= $(dir $(BOARD_PATH))
else
BOARDS_PATH ?= src/brd
ifneq ($(BOARD),)
BOARD_PATH ?= $(firstword $(filter %/$(BOARD),$(call SUBMKS,$(sort $(BOARDS_PATH)) src/brd)))
endif
endif

BOARDS_SUGGESTED = $(notdir $(call SUBMKS,$(sort $(BOARDS_PATH))))
BOARDS_ALLOWED = $(BOARDS_SUGGESTED) $(notdir $(call SUBMKS,src/brd))

ifneq (,$(BOARD))
ifneq (DUMMY,$(BOARD))
ifneq (,$(filter-out $(BOARDS_ALLOWED), $(BOARD)))
$(error invalid board $(BOARD) for $(APP). Allowed are: $(BOARDS_ALLOWED))
endif

ifneq ($(wildcard $(BOARD_PATH)/Makefile.mk),)
include $(BOARD_PATH)/Makefile.mk
endif

INCLUDE_PATH += \
	$(BOARD_PATH)
DEFINES += \
	USE_BOARD_$(subst -,_,$(BOARD)) \
	BOARD=$(subst -,_,$(BOARD))
endif # DUMMY BOARD
else #!BOARD
ifeq (,$(filter $(APP),bootloader))
BOARDS ?= $(BOARDS_SUGGESTED)
endif #bootloader
endif #BOARD
