#pragma once
/*
STM STM32F4DISCOVERY Discovery kit (aka STM32F407G-DISC1)
PINOUT:
GND----------------* *--------------------GND     GND---------------------* *-------------------GND
VDD----------------* *--------------------VDD     5V----------------------* *-------------------5V
GND----------------* *--------------------NRST    3V3---------------------* *-------------------3V3
PC1----------------* *--------------------PC0     PH0(OSC)----------------* *--------------(OSC)PH1
PC3----------------* *--------------------PC2     PC14(OSCRTC)------------* *-----------(OSCRTC)PC15
PA1----------------* *--------------------PA0     PE6---------------------* *-------------------PC13
PA3----------------* *--------------------PA2     PE4---------------------* *-------------------PE5
PA5----------------* *--------------------PA4     PE2---------------------* *-------------------PE3
PA7----------------* *--------------------PA6     PE0---------------------* *-------------------PE1
PC5----------------* *--------------------PC4     PB8---------------------* *-------------------PB9
PB1----------------* *--------------------PB0     BOOT0-------------------* *-------------------VDD
GND----------------* *--------------------PB2     PB6---------------------* *-------------------PB7
PE7----------------* *--------------------PE8     PB4---------------------* *-------------------PB5
PE9----------------* *--------------------PE10    PD7---------------------* *-------------------PB3
PE11---------------* *--------------------PE12    PD5---------------------* *-------------------PD6
PE13---------------* *--------------------PE14    PD3---------------------* *-------------------PD4
PE15---------------* *--------------------PB10    PD1---------------------* *-------------------PD2
PB11---------------* *--------------------PB12    PC12--------------------* *-------------------PD0
PB13---------------* *--------------------PB14    PC10--------------------* *-------------------PC11
PB15---------------* *--------------------PD8     PA14--------------------* *-------------------PA15
PD9----------------* *--------------------PD10    PA10--------------------* *-------------------PA13
PD11---------------* *--------------------PD12    PA8---------------------* *-------------------PA9
PD13---------------* *--------------------PD14    PC8---------------------* *-------------------PC9
PD15---------------* *                            PC6---------------------* *-------------------PC7
GND----------------* *--------------------GND     GND---------------------* *-------------------GND
*/

#include "hw/mcu/mcu.h"
#include "hw/mcu/hw_din_dout.h"
#include "hw/mcu/hw_adc.h"
#include "hw/mcu/hw_adc_in.h"
#include "hw/mcu/hw_tim.h"
#include "hw/mcu/hw_uart.h"
#include "hw/mcu/hw_i2c.h"

#define BOOTLOADER_LED_PORT D,15

class CHW_Brd_F4Disco :
	public CHW_MCU
{
public:
	static void Init();

public: // GPIO
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_EX_DECLARATION(A,0,PA_0);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,1,PA_1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,2,PA_2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,3,PA_3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,4,PA_4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,5,PA_5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,6,PA_6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,7,PA_7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,8,PA_8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,9,PA_9);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,10,PA_10);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,11,PA_11);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,12,PA_12);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,13,PA_13);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,14,PA_14);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(A,15,PA_15);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,0,PB_0);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,1,PB_1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_EX_DECLARATION(B,2,PB_2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,3,PB_3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_EX_DECLARATION(B,4,PB_4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,5,PB_5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,6,PB_6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,7,PB_7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,8,PB_8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,9,PB_9);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,10,PB_10);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,11,PB_11);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,12,PB_12);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,13,PB_13);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,14,PB_14);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(B,15,PB_15);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,0,PC_0);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,1,PC_1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,2,PC_2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,3,PC_3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,4,PC_4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,5,PC_5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,6,PC_6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,7,PC_7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,8,PC_8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,9,PC_9);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,10,PC_10);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,11,PC_11);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,12,PC_12);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,13,PC_13);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,14,PC_14);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(C,15,PC_15);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,0,PD_0);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,1,PD_1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,2,PD_2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,3,PD_3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,4,PD_4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,5,PD_5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,6,PD_6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,7,PD_7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,8,PD_8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,9,PD_9);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,10,PD_10);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,11,PD_11);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,12,PD_12);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,13,PD_13);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,14,PD_14);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(D,15,PD_15);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,0,PE_0);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,1,PE_1);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,2,PE_2);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,3,PE_3);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,4,PE_4);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,5,PE_5);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,6,PE_6);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,7,PE_7);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,8,PE_8);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,9,PE_9);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,10,PE_10);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,11,PE_11);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,12,PE_12);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,13,PE_13);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,14,PE_14);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(E,15,PE_15);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(H,0,PH_0);
	ACQUIRE_RELEASE_DIGITAL_IN_OUT_DECLARATION(H,1,PH_1);

public: // ONBOARD
	ALIAS_DIGITAL_OUT(PD_13,LED);
	ALIAS_DIGITAL_OUT(PD_13,LD3);
	ALIAS_DIGITAL_OUT(PD_13,LEDO);
	ALIAS_DIGITAL_OUT(PD_12,LD4);
	ALIAS_DIGITAL_OUT(PD_12,LEDG);
	ALIAS_DIGITAL_OUT(PD_14,LD5);
	ALIAS_DIGITAL_OUT(PD_14,LEDR);
	ALIAS_DIGITAL_OUT(PD_15,LD6);
	ALIAS_DIGITAL_OUT(PD_15,LEDB);
	ALIAS_DIGITAL_IN_EX(PA_0,BTN);

#if defined(USE_ADC) || defined(USE_ADC1) || defined(USE_ADC2) || defined(USE_ADC3)
public: // AIO
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(0,AI0);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(1,AI1);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(2,AI2);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(3,AI3);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(4,AI4);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(5,AI5);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(6,AI6);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(7,AI7);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(8,AI8);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(9,AI9);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(10,AI10);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(11,AI11);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(12,AI12);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(13,AI13);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(14,AI14);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(15,AI15);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(ADC_CHANNEL_N_TEMPSENS,TEMPSENS);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(ADC_CHANNEL_N_VREFINT,VREFINT);
	ACQUIRE_RELEASE_ANALOG_IN_DECLARATION(ADC_CHANNEL_N_VBAT,VBAT);
	ACQUIRE_RELEASE_ADC_DECLARATION(1, 1);
	ACQUIRE_RELEASE_ADC_DECLARATION(2, 2);
	ACQUIRE_RELEASE_ADC_DECLARATION(3, 3);
#endif

public: // TIMERS
	ACQUIRE_RELEASE_TIMER_DECLARATION(1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,1,PWM1_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,2,PWM1_2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,3,PWM1_3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(1,4,PWM1_4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,1,PWM2_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,2,PWM2_2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,3,PWM2_3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(2,4,PWM2_4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,1,PWM3_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,2,PWM3_2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,3,PWM3_3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(3,4,PWM3_4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(4);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,1,PWM4_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,2,PWM4_2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,3,PWM4_3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(4,4,PWM4_4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(5);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(5,1,PWM5_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(5,2,PWM5_2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(5,3,PWM5_3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(5,4,PWM5_4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(6);
	ACQUIRE_RELEASE_TIMER_DECLARATION(7);
	ACQUIRE_RELEASE_TIMER_DECLARATION(8);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(8,1,PWM8_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(8,2,PWM8_2);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(8,3,PWM8_3);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(8,4,PWM8_4);
	ACQUIRE_RELEASE_TIMER_DECLARATION(9);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(9,1,PWM9_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(9,2,PWM9_2);
	ACQUIRE_RELEASE_TIMER_DECLARATION(10);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(10,1,PWM10_1);
	ACQUIRE_RELEASE_TIMER_DECLARATION(11);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(11,1,PWM11_1);
	ACQUIRE_RELEASE_TIMER_DECLARATION(12);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(12,1,PWM12_1);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(12,2,PWM12_2);
	ACQUIRE_RELEASE_TIMER_DECLARATION(13);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(13,1,PWM13_1);
	ACQUIRE_RELEASE_TIMER_DECLARATION(14);
	ACQUIRE_RELEASE_TIMER_CHANNEL_DECLARATION(14,1,PWM14_1);

public: // SERIALS
	ACQUIRE_RELEASE_SERIAL_DECLARATION(1, 1);
	ACQUIRE_RELEASE_SERIAL_DECLARATION(2, 2);
	ACQUIRE_RELEASE_SERIAL_DECLARATION(3, 3);
	ACQUIRE_RELEASE_SERIAL_DECLARATION(4, 4);
	ACQUIRE_RELEASE_SERIAL_DECLARATION(5, 5);
	ACQUIRE_RELEASE_SERIAL_DECLARATION(6, 6);

#ifdef USE_I2C
	ACQUIRE_RELEASE_I2C_DECLARATION(1, I2C1);
	ACQUIRE_RELEASE_I2C_DECLARATION(2, I2C2);
	ACQUIRE_RELEASE_I2C_DECLARATION(3, I2C3);
#endif // USE_I2C
#ifdef USE_SPI
	ACQUIRE_RELEASE_SPI_DECLARATION(1, SPI1);
	ACQUIRE_RELEASE_SPI_DECLARATION(2, SPI2);
	ACQUIRE_RELEASE_SPI_DECLARATION(3, SPI3);
#endif // USE_SPI
};

#ifdef USE_BOARD_f4disco
typedef CHW_Brd_F4Disco CHW_Brd;
#endif
