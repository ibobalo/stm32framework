#include "stdafx.h"
#include "hw/stm32f_irqs.h"
#include "hw/hw.h"

extern "C" {

#include "stm32_linker.h"

void Reset_Handler(void) __attribute__((weak, nothrow));
void Safe_Deinit(void) __attribute__((weak, nothrow));
void Default_Handler(void);
extern int main(void);

#define ISR_TABLE_ENTRY(name)       name,
#ifdef INTERRUPTS_REDIRECTION
void RedirectorToReservedHandler(void) __attribute__((naked));
#define ISR_DEFAULT(name)           void name(void) __attribute__((weak, nothrow, alias("RedirectorToReservedHandler")));
#else
#define ISR_DEFAULT(name)           void name(void) __attribute__((weak, nothrow, alias("Default_Handler")));
#endif

typedef void TIsrFn(void);
__attribute__ ((section(".isr_vector"), used))
const TIsrFn* g_pfnVectors[] = {
	GetLinkerSymbol(TIsrFn*, estack),  /* Initial value for stack pointer register */
	Reset_Handler,                        /* Reset */
	ISR_TABLE_ITERATOR_ALL(ISR_TABLE_ENTRY, Handler)  /* Interrupts */
};

#if defined(INTERRUPTS_DYNAMIC) || (defined(STM32F0) && defined(FLASH_OFFSET) && (FLASH_OFFSET > 0))
__attribute__ ((section(".ram_isr_vector"), used))
TIsrFn* g_pfnRamVectors[ARRAY_SIZE(g_pfnVectors)];
#endif

ISR_TABLE_ITERATOR_ALL(ISR_DEFAULT, Handler);

#define ISR_TABLE_ENUM_ENTRY(name)       ISR_##name,
typedef enum {
	ISR__estack,
	ISR__reset,
	ISR_TABLE_ITERATOR_ALL(ISR_TABLE_ENUM_ENTRY, Handler)
	ISR__MAX
} EIsrEntryId;

#if defined(INTERRUPTS_DYNAMIC) || (defined(STM32F0) && defined(FLASH_OFFSET) && (FLASH_OFFSET > 0))
void SetISRHandler(EIsrEntryId eIsrEntryId, TIsrFn* pIsrFn)
{
	g_pfnRamVectors[eIsrEntryId] = pIsrFn;
}
#endif

#define ISR_SHARED_SUB_DEFAULT(host_name, sub_name) void sub_name##SubHandler(void) __attribute__((weak, nothrow, alias("Empty_Handler")));
#define ISR_SHARED_HOST_DEFAULT(host_name) MAP_ARG(ISR_SHARED_SUB_DEFAULT, host_name, ISR_SHARED_SUBS_##host_name)
ISR_SHARED_ITERATOR(ISR_SHARED_HOST_DEFAULT);


/* Copy the data segment initializers from flash to SRAM */
void CopyDataInit()
{
	const int* pSrc = GetLinkerSymbol(const int*, sidata);
	int* pDst = GetLinkerSymbol(int*, sdata);
	for (; pDst < GetLinkerSymbol(int*, edata); ++pSrc, ++pDst) {
		*pDst = *pSrc;
	}
}
/* Zero fill the bss segment. */
void ZeroDataInit()
{
	for (int* pDst = GetLinkerSymbol(int*, sbss); pDst < GetLinkerSymbol(int*, ebss); ++pDst) {
		*pDst = 0;
	}
}
/* call static constructors. */
void StaticConstructorsInit()
{
	for (void (**p_fn)() = GetLinkerSymbol(void (**)(), init_array_start); p_fn < GetLinkerSymbol(void (**)(), init_array_end); ++p_fn) {
		(*p_fn)();
	}
}

#if defined(INTERRUPTS_DYNAMIC) || (defined(STM32F0) && defined(FLASH_OFFSET) && (FLASH_OFFSET > 0))
void RemapIsrTableToRam()
{
	// Copy interrupt vector table to the RAM.
	for (unsigned n = 0; n < ARRAY_SIZE(g_pfnVectors); ++n) {
		g_pfnRamVectors[n] = g_pfnVectors[n];
	}
	IF_STM32F0( SYSCFG_MemoryRemapConfig(SYSCFG_MemoryRemap_SRAM) );
	IF_STM32F4( NVIC_SetVectorTable(NVIC_VectTab_RAM, 0) );
}
#endif

void RemapIsrTableIfNecessary()
{
#ifdef INTERRUPTS_DYNAMIC
	RemapIsrTableToRam();
#elif defined(FLASH_OFFSET) && (FLASH_OFFSET > 0)
	/* restore app isr table after SystemInit() switch it back to bootloder.*/
	IF_STM32F0( RemapIsrTableToRam() );
	IF_STM32F4( NVIC_SetVectorTable(NVIC_VectTab_FLASH, (uint32_t)g_pfnVectors - NVIC_VectTab_FLASH) );
#endif
}

void Reset_Handler(void)
{
	CopyDataInit();
	ZeroDataInit();
	StaticConstructorsInit();
	SystemInit();   /* Call the clock system initialization function.*/
	RemapIsrTableIfNecessary();
	main();
}

void Safe_Deinit(void)
{
	GPIO_DeInit(GPIOA);
	GPIO_DeInit(GPIOB);
	GPIO_DeInit(GPIOC);
	GPIO_DeInit(GPIOD);
	GPIO_DeInit(GPIOE);
}

#ifdef INTERRUPTS_REDIRECTION
void RedirectorToReservedHandler(void) __attribute__((nothrow));
void RedirectorToReservedHandler(void)
{
//	typedef uint32_t IrqHandlerAddrType;
//	static constexpr const IrqHandlerAddrType* programOffset = GetLinkerSymbol(const IrqHandlerAddrType*, reserved_start);
//	IPSR_Type ipsr;
//	ipsr.w = __get_IPSR();
//	const IrqHandlerAddrType* redirectedAddr = programOffset + (ipsr.b.ISR >> 2);
//	__ASM volatile ("BX %0\n" : : "r" (redirectedAddr) : "memory");

	/* Note: Only use scratch registers: r0, r1, r2, r3 */
	register void *program_offset asm("r2") = (void *) (GetLinkerSymbol(const uint8_t*, reserved_start) - GetLinkerSymbol(const uint8_t*, firmware_start));
	asm (
			".syntax unified    \n\t"
			"MRS    r0, PSR     \n\t" /* Get current IRQ no from PSR */
			"MOVS   r1, #0x3F   \n\t" /* Mask the interrupt number only */
			"ANDS   r0, r1      \n\t"
			"LSLS   r0, r0, #2  \n\t" /* Irq address position = IRQ No * 4 */
			"LDR    r0, [r0, r2]\n\t" /* Fetch the user vector offset */
			"BX     r0          \n\t" /* Jump to user interrupt vector */
			: /* output */
			: /* input */
			: /* clobbered reg */"r0","r1","r2"
	);
	UNUSED(program_offset); // suppress unused warning
}
#endif

void Empty_Handler(void)
{
}

void Default_Handler(void)
{
	static volatile unsigned ActiveISRLevel = SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk;
	if(ActiveISRLevel) {
		DEBUG_INFO("Inside ISR:", ActiveISRLevel);
	}
	Safe_Deinit();
#ifdef DEBUG
	while(true) {/* infinite loop */};
#else
	NVIC_SystemReset();
#endif
}

void __cxa_pure_virtual()
{
	Default_Handler();
}

} // extern "C"
