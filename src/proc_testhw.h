#ifndef PROCESS_TESTHW_H_INCLUDED
#define PROCESS_TESTHW_H_INCLUDED

#include "tick_process.h"

class IHWTestProcess
{
public:
	virtual void Init() = 0;
};

IHWTestProcess* GetHWTestInterface();

#endif // PROCESS_TESTHW_H_INCLUDED
