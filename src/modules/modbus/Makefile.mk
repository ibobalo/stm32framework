USE_USART += $(if $(filter YES,$(USE_MODBUS)), \
	YES \
)
USE_485 += $(if $(filter YES,$(USE_MODBUS)), \
	YES \
)
USE_PROCESS_SCHEDULLER += $(if $(filter YES,$(USE_MODBUS)), \
	YES \
)

INCLUDE_PATH += $(if $(filter YES,$(USE_MODBUS)), \
	src/modules/modbus \
)
SRC += $(if $(filter YES,$(USE_MODBUS)), $(addprefix src/modules/modbus/, \
	proc_modbus.cpp \
	modbus_slave.cpp \
))
