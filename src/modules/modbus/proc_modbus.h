#ifndef PROCESS_MODBUS_H_INCLUDED
#define PROCESS_MODBUS_H_INCLUDED
#ifdef USE_MODBUS

#include "modbus_slave.h"

//extern
class ISerial;

class IModbusSlaveProcessor
{
public: // IModbusSlaveProcessor
	virtual void Init(ISerial* pSerial, IModbusSlave* pSlave) = 0;
};

class IModbusMasterProcessor
{
public: // IModbusMasterProcessor
	virtual void Init(ISerial* pSerial) = 0;
	virtual void SetRequestsPeriod(unsigned uiPeriodTicks) = 0;
	virtual void AddSlaveInspector(IModbusSlaveProxy* pSlave) = 0;
};

IModbusSlaveProcessor* GetModbusSlaveProcessor();
IModbusMasterProcessor* GetModbusMasterProcessor();

#endif // USE_MODBUS
#endif // PROCESS_MODBUS_H_INCLUDED
