#include "stdafx.h"
#include "modbus_slave.h"
#include "util/macros.h"
#include <stdlib.h>

/*virtual*/
uint8_t CModbusSlaveStub::GetSlaveAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetSlaveAddress());
	return m_uiSlaveAddress;
}

/*virtual*/
const char* CModbusSlaveStub::GetSlaveId() const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetSlaveId());
	return "";
}

/*virtual*/
const uint8_t* CModbusSlaveStub::GetSlaveIdExtra(unsigned* puiRetLength) const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetSlaveIdExtra(puiRetLength));
	return NULL;
}

/*virtual*/
ModbusResultCode CModbusSlaveStub::GetCoilStatus(unsigned uiCoilAddr, bool* pbRetValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetCoilStatus(uiCoilAddr, pbRetValue));
	return MBRC_ILLEGAL_FN;
}
/*virtual*/
ModbusResultCode CModbusSlaveStub::GetDiscreteInputs(unsigned uiInputAddr, bool* pbRetValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetDiscreteInputs(uiInputAddr, pbRetValue));
	return MBRC_ILLEGAL_FN;
}

/*virtual*/
ModbusResultCode CModbusSlaveStub::GetHoldingRegister(unsigned uiRegisterAddr, uint16_t* puiRetValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetHoldingRegister(uiRegisterAddr, puiRetValue));
	return MBRC_ILLEGAL_FN;
}

/*virtual*/
ModbusResultCode CModbusSlaveStub::GetInputRegister(unsigned uiRegisterAddr, uint16_t* puiRetValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlave::GetInputRegister(uiRegisterAddr, puiRetValue));
	return MBRC_ILLEGAL_FN;
}

/*virtual*/
ModbusResultCode CModbusSlaveStub::SetCoil(unsigned uiCoilAddr, bool bState)
{
	return MBRC_ILLEGAL_FN;
}

/*virtual*/
ModbusResultCode CModbusSlaveStub::SetHoldingRegister(unsigned uiRegisterAddr, uint16_t uiValue)
{
	return MBRC_ILLEGAL_FN;
}
