#ifndef MODBUS_SLAVE_H_INCLUDED
#define MODBUS_SLAVE_H_INCLUDED
#ifdef USE_MODBUS

#include <stdint.h>

typedef enum {
	MBFN_READCOILS          = 0x01,
	MBFN_READDISCRETEINPUTS = 0x02,
	MBFN_READHOLDINGREGS    = 0x03,
	MBFN_READINPUTREGS      = 0x04,

	MBFN_WRITESINGLECOIL    = 0x05,
	MBFN_WRITESINGLEREG     = 0x06,
	MBFN_WRITEMULTIPLECOILS = 0x0F,
	MBFN_WRITEMULTIPLEREGS  = 0x10,

	MBFN_REPORTSLAVEID     = 0x11,
} ModbusFunctionCode;

typedef enum {
	MBRC_OK,
	MBRC_ILLEGAL_FN,
	MBRC_ILLEGAL_ADDR,
	MBRC_ILLEGAL_VALUE,
	MBRC_FAILURE,
	MBRC_ACK,
	MBRC_BUSY,
	MBRC_NOT_ACK,
	MBRC_INTERNAL_ERROR,
} ModbusResultCode;

typedef struct {
	ModbusFunctionCode  uiFn;
	uint16_t            uiStartAddress;
	uint16_t            uiCount;
} ModbusCommunicationEntry;

class IModbusSlaveCommunicationDescription {
public: // IModbusSlaveCommunicationDescription
	virtual const char* GetName() const = 0;
	virtual unsigned GetCommunicationEntriesCount() const = 0;
	virtual const ModbusCommunicationEntry* GetCommunicationEntry(unsigned uiIndex) const = 0;
};

template<const char* name, unsigned nCommunicationEntriesCount>
class CModbusSlaveCommunicationDescriptionStorage :
		public IModbusSlaveCommunicationDescription
{
public: // IModbusSlaveCommunicationDescription
	virtual const char* GetName() const {return name;};
	virtual unsigned GetCommunicationEntriesCount() const {return nCommunicationEntriesCount;};
	virtual const ModbusCommunicationEntry* GetCommunicationEntry(unsigned uiIndex) const {return &aCommunicationEntries[uiIndex];};
public: // to allow init with {}
	static const ModbusCommunicationEntry aCommunicationEntries[nCommunicationEntriesCount];
};

class IModbusSlave
{
public: // IModbusSlave
	virtual uint8_t GetSlaveAddress() const = 0;
	virtual const char* GetSlaveId() const = 0;
	virtual const uint8_t* GetSlaveIdExtra(unsigned* puiRetLength) const = 0;

	virtual ModbusResultCode GetDiscreteInputs(unsigned uiInputAddr, bool* pbRetValue) const = 0;
	virtual ModbusResultCode GetCoilStatus(unsigned uiCoilAddr, bool* pbRetValue) const = 0;
	virtual ModbusResultCode GetInputRegister(unsigned uiRegisterAddr, uint16_t* puiRetValue) const = 0;
	virtual ModbusResultCode GetHoldingRegister(unsigned uiRegisterAddr, uint16_t* puiRetValue) const = 0;

	virtual ModbusResultCode SetCoil(unsigned uiCoilAddr, bool bState) = 0;
	virtual ModbusResultCode SetHoldingRegister(unsigned uiRegisterAddr, uint16_t uiValue) = 0;
};

class IModbusSlaveProxy :
		public IModbusSlave
{
public: // IModbusSlaveProxy
	virtual const IModbusSlaveCommunicationDescription* GetDescription() const = 0;
	virtual ModbusResultCode SetDiscreteInputs(unsigned uiInputAddr, bool bState) = 0;
	virtual ModbusResultCode SetInputRegister(unsigned uiRegisterAddr, uint16_t uiValue) = 0;
};


class CModbusSlaveStub:
		public IModbusSlave
{
public:
	void Init(uint16_t uiSlaveAddress) {m_uiSlaveAddress = uiSlaveAddress;};

public: // IModbusSlave
	virtual uint8_t GetSlaveAddress() const;
	virtual const char* GetSlaveId() const;
	virtual const uint8_t* GetSlaveIdExtra(unsigned* puiRetLength) const;

	virtual ModbusResultCode GetCoilStatus(unsigned uiCoilAddr, bool* pbRetValue) const;
	virtual ModbusResultCode GetDiscreteInputs(unsigned uiInputAddr, bool* pbRetValue) const;
	virtual ModbusResultCode GetHoldingRegister(unsigned uiRegisterAddr, uint16_t* puiRetValue) const;
	virtual ModbusResultCode GetInputRegister(unsigned uiRegisterAddr, uint16_t* puiRetValue) const;

	virtual ModbusResultCode SetCoil(unsigned uiCoilAddr, bool bState);
	virtual ModbusResultCode SetHoldingRegister(unsigned uiRegisterAddr, uint16_t uiValue);
private:
	unsigned m_uiSlaveAddress;
};

#endif // USE_MODBUS
#endif // MODBUS_SLAVE_H_INCLUDED
