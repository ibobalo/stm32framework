#include "stdafx.h"
#include "proc_modbus.h"
#include "hw/hw.h"
#include "hw/tick_timeout.h"
#include "hw/hw_macros.h"
#include "util/macros.h"
#include "util/utils.h"
#include "util/endians.h"
#include "util/pipes.h"
#include "tick_process.h"
#include <string.h>


TSerialConnectionParams g_aConnectionSettings[] = {
		{19200,   TSerialConnectionParams::stop2,  TSerialConnectionParams::parityNo},
		{9600,    TSerialConnectionParams::stop2,  TSerialConnectionParams::parityNo},
		{9600,    TSerialConnectionParams::stop1,  TSerialConnectionParams::parityNo},
		{9600,    TSerialConnectionParams::stop1,  TSerialConnectionParams::parityEven},
		{9600,    TSerialConnectionParams::stop1,  TSerialConnectionParams::parityOdd},
		{19200,   TSerialConnectionParams::stop1,  TSerialConnectionParams::parityNo},
		{19200,   TSerialConnectionParams::stop1,  TSerialConnectionParams::parityEven},
		{19200,   TSerialConnectionParams::stop1,  TSerialConnectionParams::parityOdd},
		{115200,  TSerialConnectionParams::stop1,  TSerialConnectionParams::parityNo},
		{115200,  TSerialConnectionParams::stop1,  TSerialConnectionParams::parityEven},
		{115200,  TSerialConnectionParams::stop1,  TSerialConnectionParams::parityOdd},
};


#define MODBUS_DEBUG_INFO(...) DEBUG_INFO("MODBUS: " __VA_ARGS__)
//#define MODBUS_DEBUG_INFO(...) {}


class CModbusFilterBase;            // (IDataFilter)
class   CModbusSlaveFilter;
class CModbusMasterRequestSource;      // (CDataSource)
class CModbusMasterResponceReceiver;   // (IDataReceiver)
class CModbusProcessorBase;
class   CModbusProcessorFSM;
class     CModbusProcessorFSMSlave;  // (IModmusSlaveProcessor)
class     CModbusProcessorFSMMaster; // (IModmusMasterProcessor)

struct ModbusRtuReqHeader {
	uint8_t  uiSlaveAddr;
	uint8_t  uiFn;
	uint16_t uiAddr;
	uint16_t uiCount;
};
struct ModbusRtuRespHeaderShort {
	uint8_t  uiSlaveAddr;
	uint8_t  uiFn;
};
struct ModbusRtuRespHeader {
	uint8_t  uiSlaveAddr;
	uint8_t  uiFn;
	uint8_t  uiN;
};
struct ModbusRtuErrorMsg {
	uint8_t  uiSlaveAddr;
	uint8_t  uiFn;
	uint8_t  uiErrorCode;
};

// Note, the result has low and high bytes swapped, so use it accordingly (or swap bytes)
void AccumulateCRC(uint16_t& uiCrc, const CConstChunk& ccData)
{
	for (unsigned pos = 0; pos < ccData.uiLength; ++pos) {
		uiCrc ^= (uint16_t)*(ccData.pData + pos);
		for (unsigned i = 8; i != 0; --i) {
			if ((uiCrc & 0x0001) != 0) {
				uiCrc >>= 1;
				uiCrc ^= 0xA001;
			} else {
				uiCrc >>= 1;
			}
		}
	}
}
// Note, the result has low and high bytes swapped, so use it accordingly (or swap bytes)
uint16_t CalcCRC(const CConstChunk& ccData)
{
	uint16_t uiCrc = 0xFFFF;
	AccumulateCRC(uiCrc, ccData);
	return uiCrc;
}

/*
 *
 *
 *   CModbusFilterBase
 *
 *
 */

class CModbusFilterBase :
		public CStreamFilter
{
public:
	void SetSlaveDevice(IModbusSlave* pSlave) {m_pSlave = pSlave;};
	void BindError(IStreamReceiver* pErrorReceiver) {m_pErrorReceiver = pErrorReceiver;};

protected:
	void BroadcastAndAccumulateCRC(uint16_t& uiCrc, const CConstChunk& ccData) {Broadcast(ccData); AccumulateCRC(uiCrc, ccData);}
	unsigned BroadcastError(const CConstChunk& ccData) {ASSERTE(m_pErrorReceiver); return m_pErrorReceiver->Push(ccData);};
	void BroadcastCRC(uint16_t uiCrc) {
		struct 	{uint8_t lo; uint8_t hi;} crc = {(uint8_t)(uiCrc & 0xFF), (uint8_t)(uiCrc >> 8)};
		CConstChunk ccCRC = {(const uint8_t*)&crc, sizeof(crc)};
		Broadcast(ccCRC);
	}

protected:

	IModbusSlave*     m_pSlave;
	IStreamReceiver*  m_pErrorReceiver;
};

/*
 *
 *
 *  CModbusSlaveFilter
 *
 *
 */

class CModbusSlaveFilter :
	public CModbusFilterBase
{
public: // IDataReceiver
	virtual unsigned Push(const CConstChunk& ccData);

private:
	void ProcessSlaveReadCoils(unsigned uiAddr, unsigned uiCount);
	void ProcessSlaveReadDiscreteInputs(unsigned uiAddr, unsigned uiCount);
	void ProcessSlaveReadHoldingRegisters(unsigned uiAddr, unsigned uiCount);
	void ProcessSlaveReadInputRegisters(unsigned uiAddr, unsigned uiCount);
	void ProcessSlaveReportSlaveId();

	void FillErrorResponce(ModbusFunctionCode eFnCode, ModbusResultCode eErrorCode);

};


unsigned CModbusSlaveFilter::Push(const CConstChunk& ccData)
{
	IMPLEMENTS_INTERFACE_METHOD(CStreamFilter::Push(ccData));

	if (ccData.uiLength < sizeof(ModbusRtuReqHeader)) {
		MODBUS_DEBUG_INFO("Pkt too short. Len:", ccData.uiLength);
		return 0;
	}
	const struct ModbusRtuReqHeader* pRqH = (const ModbusRtuReqHeader*)ccData.pData;

	if (pRqH->uiSlaveAddr != m_pSlave->GetSlaveAddress()) {
		MODBUS_DEBUG_INFO("Slave Addr do not match. Got|expected:",  ((uint32_t)pRqH->uiSlaveAddr << 8) | m_pSlave->GetSlaveAddress());
		return 0;
	}

	uint16_t uiGotCRC = (uint16_t)*((const uint8_t*)ccData.pData + ccData.uiLength - 2) | (uint16_t)*((const uint8_t*)ccData.pData + ccData.uiLength - 1) << 8;
	const CConstChunk ccBody = {ccData.pData, ccData.uiLength - 2};
	uint16_t uiExpectedCRC = CalcCRC(ccBody);
	if (uiGotCRC != uiExpectedCRC) {
		MODBUS_DEBUG_INFO("Pkt CRC error. Got|expected:",  ((uint32_t)uiGotCRC << 16) | uiExpectedCRC);
		return 0;
	}
	MODBUS_DEBUG_INFO("Pkt CRC match. Got|expected:",  ((uint32_t)uiGotCRC << 16) | uiExpectedCRC);


	switch (pRqH->uiFn) {
		case MBFN_READCOILS: { // Read Coil Status
			ProcessSlaveReadCoils(ntohs(pRqH->uiAddr), ntohs(pRqH->uiCount));
		}; break;
		case MBFN_READDISCRETEINPUTS: { // Read Discrete Inputs
			ProcessSlaveReadDiscreteInputs(ntohs(pRqH->uiAddr), ntohs(pRqH->uiCount));
		}; break;
		case MBFN_READHOLDINGREGS: { // Read Holding Registers
			ProcessSlaveReadHoldingRegisters(ntohs(pRqH->uiAddr), ntohs(pRqH->uiCount));
		}; break;
		case MBFN_READINPUTREGS: { // Read Input Registers
			ProcessSlaveReadInputRegisters(ntohs(pRqH->uiAddr), ntohs(pRqH->uiCount));
		}; break;
		case MBFN_REPORTSLAVEID: {
			ProcessSlaveReportSlaveId();
		}; break;
		default: {
			FillErrorResponce((ModbusFunctionCode)pRqH->uiFn, MBRC_ILLEGAL_FN);
		}
	}

	return true;
}

void CModbusSlaveFilter::ProcessSlaveReadCoils(unsigned uiAddr, unsigned uiCount)
{
	if (uiCount > 0 && uiCount <= 2000) {
		uint16_t uiCrc = 0xFFFF;
		struct ModbusRtuRespHeader RsH = {(uint8_t)m_pSlave->GetSlaveAddress(), (uint8_t)MBFN_READCOILS, (uint8_t)((uiCount + 7) / 8)};
		CConstChunk ccRsH = {(const uint8_t*)&RsH, sizeof(RsH)};
		BroadcastAndAccumulateCRC(uiCrc, ccRsH);
		uint8_t x = 0;
		unsigned uiBitMask = 1;
		unsigned uiLastAddr = uiAddr + uiCount;
		for (unsigned n = uiAddr; n < uiLastAddr; ++n) {
			if (uiBitMask == 0x100) {
				CConstChunk ccX = {(const uint8_t*)&x, sizeof(x)};
				BroadcastAndAccumulateCRC(uiCrc, ccX);
				uiBitMask = 1;
				x = 0;
			}
			bool bCoilStatus;
			ModbusResultCode eResult = m_pSlave->GetCoilStatus(n, &bCoilStatus);
			if (eResult == MBRC_OK) {
				if (bCoilStatus) {
					x |= uiBitMask;
				}
				uiBitMask <<= 1;
			} else {
				FillErrorResponce(MBFN_READCOILS, eResult);
				break;
			}
		};
		CConstChunk ccX = {(const uint8_t*)&x, sizeof(x)};
		BroadcastAndAccumulateCRC(uiCrc, ccX);
		BroadcastCRC(uiCrc);
	} else { // tx buffer overflow
		FillErrorResponce(MBFN_READCOILS, MBRC_ILLEGAL_VALUE);
	}
}
void CModbusSlaveFilter::ProcessSlaveReadDiscreteInputs(unsigned uiAddr, unsigned uiCount)
{
	if (uiCount > 0 && uiCount <= 2000) {
		uint16_t uiCrc = 0xFFFF;
		struct ModbusRtuRespHeader RsH = {(uint8_t)m_pSlave->GetSlaveAddress(), (uint8_t)MBFN_READDISCRETEINPUTS, (uint8_t)((uiCount + 7) / 8)};
		CConstChunk ccRsH = {(const uint8_t*)&RsH, sizeof(RsH)};
		BroadcastAndAccumulateCRC(uiCrc, ccRsH);
		uint8_t x = 0;
		CConstChunk ccX = {(const uint8_t*)&x, sizeof(x)};
		unsigned uiBitMask = 1;
		unsigned uiLastAddr = uiAddr + uiCount;
		for (unsigned n = uiAddr; n < uiLastAddr; ++n) {
			if (uiBitMask == 0x100) {
				BroadcastAndAccumulateCRC(uiCrc, ccX);
				uiBitMask = 1;
				x = 0;
			}
			bool bDiscreteInput;
			ModbusResultCode eResult = m_pSlave->GetDiscreteInputs(n, &bDiscreteInput);
			if (eResult == MBRC_OK) {
				if (bDiscreteInput) {
					x |= uiBitMask;
				}
				uiBitMask <<= 1;
			} else {
				FillErrorResponce(MBFN_READDISCRETEINPUTS, eResult);
				break;
			}
		}
		BroadcastAndAccumulateCRC(uiCrc, ccX);
		BroadcastCRC(uiCrc);
	} else { // tx buffer overflow
		FillErrorResponce(MBFN_READDISCRETEINPUTS, MBRC_ILLEGAL_VALUE);
	}
}
void CModbusSlaveFilter::ProcessSlaveReadHoldingRegisters(unsigned uiAddr, unsigned uiCount)
{
	if (uiCount > 0 && uiCount <= 125) {
		uint16_t uiCrc = 0xFFFF;
		struct ModbusRtuRespHeader RsH = {(uint8_t)m_pSlave->GetSlaveAddress(), (uint8_t)MBFN_READHOLDINGREGS, (uint8_t)(uiCount * 2)};
		CConstChunk ccRsH = {(const uint8_t*)&RsH, sizeof(RsH)};
		BroadcastAndAccumulateCRC(uiCrc, ccRsH);
		unsigned uiLastAddr = uiAddr + uiCount;
		for (unsigned n = uiAddr; n < uiLastAddr; ++n) {
			uint16_t uiHoldingValue;
			ModbusResultCode eResult = m_pSlave->GetHoldingRegister(n, &uiHoldingValue);
			if (eResult == MBRC_OK) {
				uiHoldingValue = htons(uiHoldingValue);
				CConstChunk ccHoldingValue = {(const uint8_t*)&uiHoldingValue, sizeof(uiHoldingValue)};
				BroadcastAndAccumulateCRC(uiCrc, ccHoldingValue);
			} else {
				FillErrorResponce(MBFN_READHOLDINGREGS, eResult);
				break;
			}
		}
		BroadcastCRC(uiCrc);
	} else { // tx buffer overflow
		FillErrorResponce(MBFN_READHOLDINGREGS, MBRC_ILLEGAL_VALUE);
	}
}
void CModbusSlaveFilter::ProcessSlaveReadInputRegisters(unsigned uiAddr, unsigned uiCount)
{
	if (uiCount > 0 && uiCount <= 125) {
		uint16_t uiCrc = 0xFFFF;
		struct ModbusRtuRespHeader RsH = {(uint8_t)m_pSlave->GetSlaveAddress(), (uint8_t)MBFN_READINPUTREGS, (uint8_t)(uiCount * 2)};
		CConstChunk ccRsH = {(const uint8_t*)&RsH, sizeof(RsH)};
		BroadcastAndAccumulateCRC(uiCrc, ccRsH);
		unsigned uiLastAddr = uiAddr + uiCount;
		for (unsigned n = uiAddr; n < uiLastAddr; ++n) {
			uint16_t uiInputValue;
			ModbusResultCode eResult = m_pSlave->GetInputRegister(n, &uiInputValue);
			if (eResult == MBRC_OK) {
				uiInputValue = htons(uiInputValue);
				CConstChunk ccInputValue = {(const uint8_t*)&uiInputValue, sizeof(uiInputValue)};
				BroadcastAndAccumulateCRC(uiCrc, ccInputValue);
			} else {
				FillErrorResponce(MBFN_READINPUTREGS, eResult);
				break;
			}
		}
		BroadcastCRC(uiCrc);
	} else { // tx buffer overflow
		FillErrorResponce(MBFN_READINPUTREGS, MBRC_ILLEGAL_VALUE);
	}
}

void CModbusSlaveFilter::ProcessSlaveReportSlaveId()
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuRespHeaderShort RqH = {m_pSlave->GetSlaveAddress(), MBFN_REPORTSLAVEID};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);

	const char* sSlaveId = m_pSlave->GetSlaveId();
	uint8_t uiNameLength = strnlen(sSlaveId, 255);
	CConstChunk ccLength = {(const uint8_t*)&uiNameLength, sizeof(uiNameLength)};
	BroadcastAndAccumulateCRC(uiCrc, ccLength);
	CConstChunk ccName = {(const uint8_t*)sSlaveId, uiNameLength};
	BroadcastAndAccumulateCRC(uiCrc, ccName);

	static const uint8_t uiStatus = 0xFF;
	CConstChunk ccStatus = {(const uint8_t*)&uiStatus, sizeof(uiStatus)};
	BroadcastAndAccumulateCRC(uiCrc, ccStatus);

	unsigned uiExtraLength;
	const uint8_t* sSlaveExtra = m_pSlave->GetSlaveIdExtra(&uiExtraLength);
	CConstChunk ccExtra = {(const uint8_t*)sSlaveExtra, uiExtraLength};
	BroadcastAndAccumulateCRC(uiCrc, ccExtra);

	BroadcastCRC(uiCrc);
}



void CModbusSlaveFilter::FillErrorResponce(ModbusFunctionCode eFnCode, ModbusResultCode uiErrorCode)
{
	struct ModbusRtuErrorMsg tmp = {(uint8_t)m_pSlave->GetSlaveAddress(), (uint8_t)(0x80 | (uint8_t)eFnCode), (uint8_t)uiErrorCode};
	CConstChunk ccTmp = {(const uint8_t*)&tmp, sizeof(tmp)};
	BroadcastError(ccTmp);
	uint16_t uiCrc = CalcCRC(ccTmp);
	struct 	{uint8_t lo; uint8_t hi;} crc = {(uint8_t)(uiCrc & 0xFF), (uint8_t)(uiCrc >> 8)};
	CConstChunk ccCRC = {(const uint8_t*)&crc, sizeof(crc)};
	BroadcastError(ccCRC);
}

/*
 *
 *
 *  CModbusProcessorBase
 *
 *
 */

class CModbusProcessorBase
{
public:
	void Init(ISerial* pSerial, ISerialRxNotifier* pRxNotifier, ISerialTxNotifier* pTxNotifier);
	void SetNextConnectionParamsPreset();
	void SetConnectionParamsPreset(unsigned nConnectionSettingsIndex);
	void SetConnectionParams(const TSerialConnectionParams pParams[], unsigned nCount = 1);
	void ResetErrors()             {m_nErrors = 0;};
protected:
	ISerial* GetSerial() const        {ASSERTE(m_pSerial); return m_pSerial;}
	unsigned GetErrorsCount() const   {return m_nErrors;}

	ISerial*                       m_pSerial;
	unsigned                       m_nConnectionSettingsIndex;
	unsigned                       m_nErrors;
};
void CModbusProcessorBase::Init(ISerial* pSerial, ISerialRxNotifier* pRxNotifier, ISerialTxNotifier* pTxNotifier) {
	m_nErrors = 0;
	m_pSerial = pSerial;
	m_pSerial->SetSerialRxNotifier(pRxNotifier);
	m_pSerial->SetSerialTxNotifier(pTxNotifier);
	ASSERTE(m_pSerial);
	SetConnectionParamsPreset(0);
	m_pSerial->Control(true);
}
void CModbusProcessorBase::SetNextConnectionParamsPreset()
{
	//switch to next connection settings
	++m_nConnectionSettingsIndex;
	m_nConnectionSettingsIndex %= ARRAY_SIZE(g_aConnectionSettings);

	SetConnectionParamsPreset(m_nConnectionSettingsIndex);
}

void CModbusProcessorBase::SetConnectionParamsPreset(unsigned nConnectionSettingsIndex)
{
	SetConnectionParams(&(g_aConnectionSettings[nConnectionSettingsIndex]));
}
void CModbusProcessorBase::SetConnectionParams(const TSerialConnectionParams pParams[], unsigned nCount)
{
	m_pSerial->SetConnectionParams(pParams);
	MODBUS_DEBUG_INFO("Baudrate:", pParams->uiBaudRate);
}


/*
 *
 *
 *   CModbusProcessorFSM
 *
 *
 */

class CModbusProcessorFSM:
		public ISerialRxNotifier,
		public ISerialTxNotifier,
		public CModbusProcessorBase,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;

public: // CModbusProcessorFSM
	virtual void Init(ISerial* pSerial);

public: // ISerialRxNotifier
	virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxDone(CIoChunk* pRetNextChunk);
	virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk);
public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);

protected:
	typedef enum {
		STATE_WaitingSilence,
		STATE_WaitingData,
		STATE_RxingAllData,
//		STATE_RxingDataTail,
		STATE_TxingAllData,
		STATE_Idle,
	} EFsmState;
	EFsmState m_eState;
protected:
	void SetState(EFsmState eState) {
		MODBUS_DEBUG_INFO("State:", (int)eState);
		m_eState = eState;
	}
	void StartWaitSilence(CIoChunk* pRetNextChunk = NULL) {
		MODBUS_DEBUG_INFO("Waiting silence...");
		m_IncomeCache.Reset();
		SetState(STATE_WaitingSilence);
		GetSerial()->SetRxByteTimeout(MILLI_TO_TICKS(5));  // todo better timeouts
		CIoChunk cc = m_IncomeCache.ReserveChunk();
		if (pRetNextChunk) {
			*pRetNextChunk = cc;
		} else {
			GetSerial()->StartDataRecv(cc);
		}
	}
	void ContinueWaitSilence(CIoChunk* pRetNextChunk = NULL) {
		MODBUS_DEBUG_INFO("Continue waiting silence");
		ASSERTE(m_eState == STATE_WaitingSilence);
		ASSERTE(pRetNextChunk);
		m_IncomeCache.CancelReserved();
		CIoChunk cc = m_IncomeCache.ReserveChunk();
		*pRetNextChunk = cc;
	}
	void FinaliseWaitSilence() {
		MODBUS_DEBUG_INFO("Waiting silence done");
		m_IncomeCache.CancelReserved();
	}
	void StartWaitData(CIoChunk* pRetNextChunk) {
		MODBUS_DEBUG_INFO("Waiting data...");
		m_IncomeCache.Reset();
		SetState(STATE_WaitingData);
		GetSerial()->SetRxByteTimeout(MILLI_TO_TICKS(100));  // todo better timeouts
		CIoChunk cc = m_IncomeCache.ReserveChunk(1);
		if (!pRetNextChunk) {
			GetSerial()->StartDataRecv(cc);
		} else {
			*pRetNextChunk = cc;
		}
	}
	void StartRxAllData(CIoChunk* pRetNextChunk) {
		ASSERTE(m_IncomeCache.GetReservedSize() == 1);
		m_IncomeCache.AppendReserved();
		SetState(STATE_RxingAllData);
		*pRetNextChunk = m_IncomeCache.ReserveChunk();
		MODBUS_DEBUG_INFO("RXing data...:", m_IncomeCache.GetReservedSize());
	}
	void ContinueRxAllData() {
		SetState(STATE_RxingAllData);
		GetSerial()->SetRxByteTimeout(MILLI_TO_TICKS(2));  // todo better timeouts
		GetSerial()->StartDataRecv(m_IncomeCache.ReserveChunk());
		MODBUS_DEBUG_INFO("RXing data...:", m_IncomeCache.GetReservedSize());
	}
	virtual void ProcessRxedPkt() {
		MODBUS_DEBUG_INFO("got pkt, len:", m_IncomeCache.GetCachedDataLength());
		SetState(STATE_Idle);
		ContinueNow();
	}
	bool StartTxData() {
		MODBUS_DEBUG_INFO("TXing pkt...:", m_OutgoingCache.GetCachedDataLength());
		GetSerial()->SetTxByteTimeout(2); // todo better timeouts
		SetState(STATE_TxingAllData);
		bool bResult = GetSerial()->StartDataSend(m_OutgoingCache.LockDataChunk());
		return bResult;
	}
	virtual void FinalizeTx() {
		MODBUS_DEBUG_INFO("TX finished");
		SetState(STATE_Idle);
		ContinueNow();
	}
	void Error(const char* reason, CIoChunk* pRetNextChunk = NULL) {
		MODBUS_DEBUG_INFO("Error");
		++m_nErrors;
		StartWaitSilence(pRetNextChunk);
	}

	CStreamCache<256,false,false>    m_IncomeCache;
	CStreamCache<256,false,false>    m_OutgoingCache;

};

bool CModbusProcessorFSM::NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxPart(uiBytesLeft, pRetNextChunk));
	MODBUS_DEBUG_INFO("Rx part, state:", (int)m_eState);
	bool bResult = false;
	unsigned uiReservedSize = m_IncomeCache.GetReservedSize();
	ASSERTE(uiReservedSize >= uiBytesLeft);
	unsigned uiDataLength = uiReservedSize - uiBytesLeft;
	if (!uiDataLength) {
		MODBUS_DEBUG_INFO("Rx part no data, len:", pRetNextChunk->uiLength);
		return true;
	}
	switch (m_eState) {
		case STATE_WaitingData:
			StartRxAllData(pRetNextChunk);
			break;
		case STATE_RxingAllData:
			bResult = true; // continue Rx
			break;
//		case STATE_RxingDataTail:
//			m_IncomeCache.AppendReserved(m_IncomeCache.GetReservedSize() - uiBytesLeft);
//			ProcessRxedPkt();
//			break;
		default:
			ASSERT("Invalid state for RX part");
			break;
	}
	return bResult;
}

bool CModbusProcessorFSM::NotifyRxDone(CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxDone(pRetNextChunk));
	bool bResult = true;
	MODBUS_DEBUG_INFO("Rx done, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_WaitingSilence:
			ContinueWaitSilence(pRetNextChunk);
			break;
		case STATE_WaitingData:
			StartRxAllData(pRetNextChunk);
			break;
		case STATE_RxingAllData:
			Error("Pkt too long", pRetNextChunk);
			break;
//		case STATE_RxingDataTail:
//			ProcessRxedPkt();
//			break;
		default:
			ASSERT("Invalid state for RX done");
			bResult = false;
			break;
	}
	return bResult;
}
bool CModbusProcessorFSM::NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxOverflow(ccData, pRetNextChunk));
	bool bResult = false;
	MODBUS_DEBUG_INFO("Rx overflow, state:", (int)m_eState);
	switch (m_eState) {
/*		case STATE_WaitingSilence:
		case STATE_WaitingData:
		case STATE_RxingAllData:
			Error("Rx owerflow");
			break;
		case STATE_TxingAllData:
			// ignore echoed data
			break;
*/		default:
//			ASSERT("Invalid state for RX overflow");
			break;
	}
	return bResult;
}
bool CModbusProcessorFSM::NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxTimeout(uiBytesLeft, pRetNextChunk));
	bool bContinue = true;
	MODBUS_DEBUG_INFO("Rx timeout, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_WaitingSilence:
			FinaliseWaitSilence();
			StartWaitData(pRetNextChunk);
			break;
		case STATE_WaitingData:
			break;
		case STATE_RxingAllData:
			m_IncomeCache.AppendReserved(m_IncomeCache.GetReservedSize() - uiBytesLeft);
//			SetState(STATE_RxingDataTail);
			ProcessRxedPkt();
			bContinue = false;
			break;
		default:
			ASSERT("Invalid state for RX timeout");
			bContinue = false;
			break;
	}
	return bContinue;
}
bool CModbusProcessorFSM::NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxError(uiBytesLeft, pRetNextChunk));
	bool bResult = false;
	MODBUS_DEBUG_INFO("Rx error, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_WaitingSilence:
		case STATE_WaitingData:
		case STATE_RxingAllData:
			m_IncomeCache.ReleaseData();
			m_IncomeCache.CancelReserved();
			Error("Rx error, income:");
			break;
		case STATE_Idle:
			break;
		default:
			ASSERT("Invalid state for RX error");
			break;
	}
	return bResult;
}
bool CModbusProcessorFSM::NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxIdle(uiBytesLeft, pRetNextChunk));
	bool bResult = false;
	MODBUS_DEBUG_INFO("Rx idle, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_RxingAllData:
//		case STATE_RxingDataTail:
			m_IncomeCache.AppendReserved(m_IncomeCache.GetReservedSize() - uiBytesLeft);
			ProcessRxedPkt();
			break;
		default:
			ASSERT("Invalid state for RX done");
			bResult = false;
			break;
	}
	return bResult;
}


bool CModbusProcessorFSM::NotifyTxDone(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
	MODBUS_DEBUG_INFO("Tx done, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_TxingAllData:
			m_OutgoingCache.ReleaseData();
			break;
		default:
			ASSERT("Invalid state for TX done");
			break;
	}
	return false;
}
bool CModbusProcessorFSM::NotifyTxComplete(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
	MODBUS_DEBUG_INFO("Tx complete, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_TxingAllData:
			FinalizeTx();
			break;
		default:
			ASSERT("Invalid state for TX done");
			break;
	}
	return false;
}
bool CModbusProcessorFSM::NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
	MODBUS_DEBUG_INFO("Tx timeout, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_TxingAllData:
			Error("Tx timeout");
			break;
		default:
			ASSERT("Invalid state for TX timeout");
			break;
	}
	return false;
}
bool CModbusProcessorFSM::NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
	MODBUS_DEBUG_INFO("Tx error, state:", (int)m_eState);
	switch (m_eState) {
		case STATE_TxingAllData:
			Error("Tx error");
			break;
		default:
			ASSERT("Invalid state for TX error");
			break;
	}
	return false;
}

void CModbusProcessorFSM::Init(ISerial* pSerial)
{
	m_IncomeCache.Init();
	m_OutgoingCache.Init();
	CParentProcess::Init(&g_TickProcessScheduller);
	CModbusProcessorBase::Init(pSerial, this, this);
}

/*
 *
 *  CModbusProcessorFSMSlave
 *
 */

class CModbusProcessorFSMSlave :
		public CModbusProcessorFSM,
		public IModbusSlaveProcessor
{
public: // IModbusSlaveProcessor
	virtual void Init(ISerial* pSerial, IModbusSlave* pSlave);
public: // TProcess
	virtual void SingleStep(CTimeType uiTime);
protected:
	bool StartTxErrMsg();
	virtual void ProcessRxedPkt();
	virtual void FinalizeTx();
protected:
	CModbusSlaveFilter          m_SlaveFilter;
	CStreamCache<10,false,false>     m_ErrorCache;
};

void CModbusProcessorFSMSlave::Init(ISerial* pSerial, IModbusSlave* pSlave)
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusSlaveProcessor::Init(pSerial, pSlave));
	CModbusProcessorFSM::Init(pSerial);

	ASSERTE(pSlave);
	m_SlaveFilter.SetSlaveDevice(pSlave);

	m_IncomeCache.Bind(&m_SlaveFilter);
	m_SlaveFilter.Bind(&m_OutgoingCache);
	m_SlaveFilter.BindError(&m_ErrorCache);

	ContinueNow();
}

void CModbusProcessorFSMSlave::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CModbusProcessorFSMSlave");

	if (m_nErrors > 10) {
		ResetErrors();
		SetNextConnectionParamsPreset();
	}

	if (m_IncomeCache.GetCachedDataLength()) {
		m_ErrorCache.ReleaseData();

		// process (filter) income packet
		MODBUS_DEBUG_INFO("Processing income:", m_IncomeCache.GetCachedDataLength());
		unsigned uiBytesProcessed = m_IncomeCache.Flush();
		m_IncomeCache.Reset();

		if (uiBytesProcessed) {
			if (m_ErrorCache.GetCachedDataLength()) {
				// send error message
				m_OutgoingCache.Reset();
				StartTxErrMsg();
			} else if (m_OutgoingCache.GetCachedDataLength()) {
				// send response packet
				StartTxData();
			} else {
				StartWaitSilence();
			}
		} else {
			StartWaitSilence();
		}
	} else {
		StartWaitSilence();
	}

	PROCESS_DEBUG_INFO("<<< Process: CModbusProcessorFSMSlave");
}

bool CModbusProcessorFSMSlave::StartTxErrMsg()
{
	GetSerial()->SetTxByteTimeout(2); // todo better timeouts
	bool bResult = GetSerial()->StartDataSend(m_ErrorCache.LockDataChunk());
	SetState(STATE_TxingAllData);
	return bResult;
}

void CModbusProcessorFSMSlave::ProcessRxedPkt()
{
	CModbusProcessorFSM::ProcessRxedPkt();
	ContinueAsap(); // process pkt in main thread
}

void CModbusProcessorFSMSlave::FinalizeTx()
{
	CModbusProcessorFSM::FinalizeTx();
	ContinueNow();
}

/*
 *
 * CModbusMasterRequestSource
 *
 */

class CModbusMasterRequestSource:
		public CStreamSource
{
public:
	void SetSlaveProxy(IModbusSlaveProxy* pSlaveProxy) {m_pSlaveProxy = pSlaveProxy; m_uiCommunicationEntryIndex = 0;}
	IModbusSlaveProxy* GetRequestedSlaveProxy() const {return (m_pSlaveProxy && m_uiCommunicationEntryIndex)?m_pSlaveProxy:NULL;}
	unsigned GetRequestedFn() const {ASSERTE(m_pSlaveProxy && m_uiCommunicationEntryIndex); return m_pSlaveProxy->GetDescription()->GetCommunicationEntry(m_uiCommunicationEntryIndex - 1)->uiFn;}
	unsigned GetRequestedAddr() const {ASSERTE(m_pSlaveProxy && m_uiCommunicationEntryIndex); return m_pSlaveProxy->GetDescription()->GetCommunicationEntry(m_uiCommunicationEntryIndex - 1)->uiStartAddress;}
	unsigned GetRequestedCount() const {ASSERTE(m_pSlaveProxy && m_uiCommunicationEntryIndex); return m_pSlaveProxy->GetDescription()->GetCommunicationEntry(m_uiCommunicationEntryIndex - 1)->uiCount;}
	bool GetIsDone() const {ASSERTE(m_pSlaveProxy && m_uiCommunicationEntryIndex); return m_uiCommunicationEntryIndex >= m_pSlaveProxy->GetDescription()->GetCommunicationEntriesCount();}
	ModbusResultCode ComposeAndBroadcast();

private:
	ModbusResultCode ComposeReadCoils(uint16_t uiRegAddress, uint16_t uiCount);
	ModbusResultCode ComposeReadDiscreteInputs(uint16_t uiRegAddress, uint16_t uiCount);
	ModbusResultCode ComposeReadHoldingRegisters(uint16_t uiRegAddress, uint16_t uiCount);
	ModbusResultCode ComposeReadInputRegisters(uint16_t uiRegAddress, uint16_t uiCount);

	ModbusResultCode ComposeWriteSingleCoil(uint16_t uiRegAddress);
	ModbusResultCode ComposeWriteSingleRegister(uint16_t uiRegAddress);
	ModbusResultCode ComposeWriteMultipleCoils(uint16_t uiRegAddress, uint16_t uiCount);
	ModbusResultCode ComposeWriteMultipleRegisters(uint16_t uiRegAddress, uint16_t uiCount);

	void BroadcastAndAccumulateCRC(uint16_t& uiCrc, const CConstChunk& ccData) {Broadcast(ccData); AccumulateCRC(uiCrc, ccData);}
	void BroadcastCRC(uint16_t uiCrc) {
		struct 	{uint8_t lo; uint8_t hi;} crc = {(uint8_t)(uiCrc & 0xFF), (uint8_t)(uiCrc >> 8)};
		CConstChunk ccCRC = {(const uint8_t*)&crc, sizeof(crc)};
		Broadcast(ccCRC);
	}
private:
	IModbusSlaveProxy*       m_pSlaveProxy;
	unsigned                 m_uiCommunicationEntryIndex;
};

ModbusResultCode CModbusMasterRequestSource::ComposeAndBroadcast()
{
	ModbusResultCode eResult = MBRC_ILLEGAL_FN;

	const IModbusSlaveCommunicationDescription* pCommunicationDescription = m_pSlaveProxy->GetDescription();
	ASSERTE(m_uiCommunicationEntryIndex < pCommunicationDescription->GetCommunicationEntriesCount());
	const ModbusCommunicationEntry* pCommunicationEntry = pCommunicationDescription->GetCommunicationEntry(m_uiCommunicationEntryIndex);

	switch (pCommunicationEntry->uiFn) {
		case MBFN_READCOILS: {
			eResult = ComposeReadCoils(pCommunicationEntry->uiStartAddress, pCommunicationEntry->uiCount);
		}; break;
		case MBFN_READDISCRETEINPUTS: {
			eResult = ComposeReadDiscreteInputs(pCommunicationEntry->uiStartAddress, pCommunicationEntry->uiCount);
		}; break;
		case MBFN_READHOLDINGREGS: {
			eResult = ComposeReadHoldingRegisters(pCommunicationEntry->uiStartAddress, pCommunicationEntry->uiCount);
		}; break;
		case MBFN_READINPUTREGS: {
			eResult = ComposeReadInputRegisters(pCommunicationEntry->uiStartAddress, pCommunicationEntry->uiCount);
		}; break;
		case MBFN_WRITESINGLECOIL: {
			eResult = ComposeWriteSingleCoil(pCommunicationEntry->uiStartAddress);
		}; break;
		case MBFN_WRITESINGLEREG: {
			eResult = ComposeWriteSingleRegister(pCommunicationEntry->uiStartAddress);
		}; break;
		case MBFN_WRITEMULTIPLECOILS: {
			eResult = ComposeWriteMultipleCoils(pCommunicationEntry->uiStartAddress, pCommunicationEntry->uiCount);
		}; break;
		case MBFN_WRITEMULTIPLEREGS: {
			eResult = ComposeWriteMultipleRegisters(pCommunicationEntry->uiStartAddress, pCommunicationEntry->uiCount);
		}; break;
		default:
			break;
	}
	++m_uiCommunicationEntryIndex;
	return eResult;
}
ModbusResultCode CModbusMasterRequestSource::ComposeReadCoils(uint16_t uiRegAddress, uint16_t uiCount)
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_READCOILS, htons(uiRegAddress), htons(uiCount)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}

ModbusResultCode CModbusMasterRequestSource::ComposeReadDiscreteInputs(uint16_t uiRegAddress, uint16_t uiCount)
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_READCOILS, htons(uiRegAddress), htons(uiCount)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}
ModbusResultCode CModbusMasterRequestSource::ComposeReadHoldingRegisters(uint16_t uiRegAddress, uint16_t uiCount)
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_READHOLDINGREGS, htons(uiRegAddress), htons(uiCount)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}
ModbusResultCode CModbusMasterRequestSource::ComposeReadInputRegisters(uint16_t uiRegAddress, uint16_t uiCount)
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_READINPUTREGS, htons(uiRegAddress), htons(uiCount)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}

ModbusResultCode CModbusMasterRequestSource::ComposeWriteSingleCoil(uint16_t uiRegAddress)
{
	bool bCoilStatus;
	ModbusResultCode eResult = m_pSlaveProxy->GetCoilStatus(uiRegAddress, &bCoilStatus);
	if (eResult != MBRC_OK) {
		return eResult;
	}

	MODBUS_DEBUG_INFO("Write single coil:", bCoilStatus);

	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_WRITESINGLECOIL, htons(uiRegAddress), bCoilStatus?htons((uint16_t)0xFF00):htons((uint16_t)0x0000)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);
	return MBRC_OK;
}

ModbusResultCode CModbusMasterRequestSource::ComposeWriteSingleRegister(uint16_t uiRegAddress)
{
	uint16_t uiRegValue;
	ModbusResultCode eResult = m_pSlaveProxy->GetHoldingRegister(uiRegAddress, &uiRegValue);
	if (eResult != MBRC_OK) {
		return eResult;
	}

	MODBUS_DEBUG_INFO("Write single reg:", uiRegValue);

	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_WRITESINGLEREG, htons(uiRegAddress), htons(uiRegValue)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}

ModbusResultCode CModbusMasterRequestSource::ComposeWriteMultipleCoils(uint16_t uiRegAddress, uint16_t uiCount)
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_WRITEMULTIPLECOILS, htons(uiRegAddress), htons(uiCount)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);

	uint8_t uiN = (uiCount + 7) / 8;
	CConstChunk ccN = {(const uint8_t*)&uiN, sizeof(uiN)};
	BroadcastAndAccumulateCRC(uiCrc, ccN);

	uint8_t uiByte = 0;
	CConstChunk ccByte = {(const uint8_t*)&uiByte, sizeof(uiByte)};
	unsigned uiBitMask = 1;
	unsigned uiEndAddress = uiRegAddress + uiCount;
	for (unsigned n = uiRegAddress; n < uiEndAddress; ++n) {
		if (uiBitMask == 0x100) {
			BroadcastAndAccumulateCRC(uiCrc, ccByte);
			uiBitMask = 0x01;
			uiByte = 0;
		}

		bool bCoilStatus;
		ModbusResultCode eResult = m_pSlaveProxy->GetCoilStatus(n, &bCoilStatus);
		if (eResult != MBRC_OK) {
			return eResult;
		}
		uiByte |= uiBitMask;
		uiBitMask <<= 1;
	}
	BroadcastAndAccumulateCRC(uiCrc, ccByte);
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}

ModbusResultCode CModbusMasterRequestSource::ComposeWriteMultipleRegisters(uint16_t uiRegAddress, uint16_t uiCount)
{
	uint16_t uiCrc = 0xFFFF;
	ModbusRtuReqHeader RqH = {m_pSlaveProxy->GetSlaveAddress(), MBFN_WRITEMULTIPLEREGS, htons(uiRegAddress), htons(uiCount)};
	CConstChunk ccRqH = {(const uint8_t*)&RqH, sizeof(RqH)};
	BroadcastAndAccumulateCRC(uiCrc, ccRqH);

	uint8_t uiN = uiCount * 2;
	CConstChunk ccN = {(const uint8_t*)&uiN, sizeof(uiN)};
	BroadcastAndAccumulateCRC(uiCrc, ccN);

	unsigned uiEndAddress = uiRegAddress + uiCount;
	uint16_t uiRegValue;
	CConstChunk ccRegValue = {(const uint8_t*)&uiRegValue, sizeof(uiRegValue)};
	for (unsigned n = uiRegAddress; n < uiEndAddress; ++n) {
		ModbusResultCode eResult = m_pSlaveProxy->GetHoldingRegister(n, &uiRegValue);
		if (eResult != MBRC_OK) {
			return eResult;
		}
		BroadcastAndAccumulateCRC(uiCrc, ccRegValue);
	}
	BroadcastCRC(uiCrc);
	return MBRC_OK;
}

/*
 *
 * CModbusMasterResponceReceiver
 *
 *
 */

class CModbusMasterResponceReceiver:
		public IStreamReceiver
{
public: // IDataReceiver
	virtual unsigned Push(const CConstChunk& ccData);

public:
	void SetRequestedContext(IModbusSlaveProxy* pRequestedSlaveProxy, unsigned uiRequestedFn, unsigned uiRequestedAddr, unsigned uiRequestedCount) {
		ASSERTE(pRequestedSlaveProxy);
		ASSERTE(uiRequestedCount);
		m_pRequestedSlaveProxy = pRequestedSlaveProxy;
		m_uiRequestedFn        = uiRequestedFn;
		m_uiRequestedAddr      = uiRequestedAddr;
		m_uiRequestedCount     = uiRequestedCount;
	}
private:
	void ProcessSlaveReadCoils(uint8_t uiN, const void* pData, unsigned uiSize);
	void ProcessSlaveReadDiscreteInputs(uint8_t uiN, const void* pData, unsigned uiSize);
	void ProcessSlaveReadHoldingRegisters(uint8_t uiN, const void* pData, unsigned uiSize);
	void ProcessSlaveReadInputRegisters(uint8_t uiN, const void* pData, unsigned uiSize);
	void ProcessSlaveWriteSingleCoil(uint16_t uiAddr, uint16_t uiValue);
	void ProcessSlaveWriteSingleRegister(uint16_t uiAddr, uint16_t uiValue);
	void ProcessSlaveWriteMultipleCoils(uint16_t uiAddr, uint16_t uiCount);
	void ProcessSlaveWriteMultipleRegisters(uint16_t uiAddr, uint16_t uiCount);
	void ProcessErrorResponce(uint8_t uiN);
private:
	IModbusSlaveProxy*  m_pRequestedSlaveProxy;
	unsigned            m_uiRequestedFn;
	unsigned            m_uiRequestedAddr;
	unsigned            m_uiRequestedCount;
};

unsigned CModbusMasterResponceReceiver::Push(const CConstChunk& ccData)
{
	IMPLEMENTS_INTERFACE_METHOD(IStreamReceiver::Push(ccData));

	if (ccData.uiLength < sizeof(struct ModbusRtuRespHeader)) {
		MODBUS_DEBUG_INFO("Pkt too short. Len:", ccData.uiLength);
		return 0;
	}
	const struct ModbusRtuRespHeaderShort* pResp = (const struct ModbusRtuRespHeaderShort*)ccData.pData;
	if (pResp->uiSlaveAddr != m_pRequestedSlaveProxy->GetSlaveAddress()) {
		MODBUS_DEBUG_INFO("Slave Addr do not match. Got|expected:",  ((uint32_t)pResp->uiSlaveAddr << 8) | m_pRequestedSlaveProxy->GetSlaveAddress());
		return 0;
	}
	if ((pResp->uiFn & 0x7F) != m_uiRequestedFn) {
		MODBUS_DEBUG_INFO("Fn do not match. Got|expected:",  ((uint32_t)pResp->uiFn << 8) | m_uiRequestedFn);
		return 0;
	}
	if (pResp->uiFn & 0x80) {
		const struct ModbusRtuRespHeader* pResp = (const struct ModbusRtuRespHeader*)ccData.pData;
		ProcessErrorResponce(pResp->uiN);
		return 0;
	}

	uint16_t uiGotCRC = (uint16_t)*((const uint8_t*)ccData.pData + ccData.uiLength - 2) | (uint16_t)*((const uint8_t*)ccData.pData + ccData.uiLength - 1) << 8;
	const CConstChunk ccBody = {ccData.pData, ccData.uiLength - 2};
	uint16_t uiExpectedCRC = CalcCRC(ccBody);
	if (uiGotCRC != uiExpectedCRC) {
		MODBUS_DEBUG_INFO("Pkt CRC error. Got|expected:",  ((uint32_t)uiGotCRC << 16) | uiExpectedCRC);
		return 0;
	}
	MODBUS_DEBUG_INFO("Pkt CRC match. Got|expected:",  ((uint32_t)uiGotCRC << 16) | uiExpectedCRC);

	const uint8_t* pPayload = (const uint8_t*)(pResp + 1);
	unsigned uiPayloadSize = ccData.uiLength - 2 - 2;

	switch (pResp->uiFn) {
		case MBFN_READCOILS: {
			ProcessSlaveReadCoils(*pPayload, pPayload + 1, uiPayloadSize - 1);
		}; break;
		case MBFN_READDISCRETEINPUTS: {
			ProcessSlaveReadDiscreteInputs(*pPayload, pPayload + 1, uiPayloadSize - 1);
		}; break;
		case MBFN_READHOLDINGREGS: {
			ProcessSlaveReadHoldingRegisters(*pPayload, pPayload + 1, uiPayloadSize - 1);
		}; break;
		case MBFN_READINPUTREGS: {
			ProcessSlaveReadInputRegisters(*pPayload, pPayload + 1, uiPayloadSize - 1);
		}; break;
		case MBFN_WRITESINGLECOIL: {
			const uint16_t* pWord = (const uint16_t*)(pPayload);
			ProcessSlaveWriteSingleCoil(ntohs(*pWord), ntohs(*(pWord + 1)));
		}; break;
		case MBFN_WRITESINGLEREG: {
			const uint16_t* pWord = (const uint16_t*)(pPayload);
			ProcessSlaveWriteSingleRegister(ntohs(*pWord), ntohs(*(pWord + 1)));
		}; break;
		case MBFN_WRITEMULTIPLECOILS: {
			const uint16_t* pWord = (const uint16_t*)(pPayload);
			ProcessSlaveWriteMultipleCoils(ntohs(*pWord), ntohs(*(pWord + 1)));
		}; break;
		case MBFN_WRITEMULTIPLEREGS: {
			const uint16_t* pWord = (const uint16_t*)(pPayload);
			ProcessSlaveWriteMultipleRegisters(ntohs(*pWord), ntohs(*(pWord + 1)));
		}; break;
		default: {
			break;
		}
	}

	return true;
}
void CModbusMasterResponceReceiver::ProcessSlaveReadCoils(uint8_t uiN, const void* pData, unsigned uiSize)
{
	unsigned uiExpectedN = (m_uiRequestedCount + 7) * 8;
	if (uiN == uiExpectedN) {
		const uint8_t* pByte = (const uint8_t*)pData;
		uint8_t uiByte = *pByte;
		unsigned uiBitMask = 1;
		unsigned uiLastAddr = m_uiRequestedAddr + m_uiRequestedCount;
		for (unsigned n = m_uiRequestedAddr; n < uiLastAddr; ++n) {
			if (uiBitMask == 0x100) {
				uiBitMask = 1;
				++pByte;
				uiByte = *pByte;
			}
			bool bCoilStatus = uiByte & uiBitMask;
			m_pRequestedSlaveProxy->SetCoil(n, bCoilStatus);
			uiBitMask <<= 1;
		};
	} else {
		MODBUS_DEBUG_INFO("Size do not match. Got|expected:",  ((uint32_t)uiN << 8) | uiExpectedN);
	}
}

void CModbusMasterResponceReceiver::ProcessSlaveReadDiscreteInputs(uint8_t uiN, const void* pData, unsigned uiSize)
{
	unsigned uiExpectedN = (m_uiRequestedCount + 7) * 8;
	if (uiN == uiExpectedN) {
		const uint8_t* pByte = (const uint8_t*)pData;
		uint8_t uiByte = *pByte;
		unsigned uiBitMask = 1;
		unsigned uiLastAddr = m_uiRequestedAddr + m_uiRequestedCount;
		for (unsigned n = m_uiRequestedAddr; n < uiLastAddr; ++n) {
			if (uiBitMask == 0x100) {
				uiBitMask = 1;
				++pByte;
				uiByte = *pByte;
			}
			bool bCoilStatus = uiByte & uiBitMask;
			m_pRequestedSlaveProxy->SetDiscreteInputs(n, bCoilStatus);
			uiBitMask <<= 1;
		}
	} else {
		MODBUS_DEBUG_INFO("Size do not match. Got|expected:",  ((uint32_t)uiN << 8) | uiExpectedN);
	}
}
void CModbusMasterResponceReceiver::ProcessSlaveReadHoldingRegisters(uint8_t uiN, const void* pData, unsigned uiSize)
{
	unsigned uiExpectedN = m_uiRequestedCount * 2;
	if (uiN == uiExpectedN) {
		const uint16_t* pWord = (const uint16_t*)pData;
		unsigned uiLastAddr = m_uiRequestedAddr + m_uiRequestedCount;
		for (unsigned n = m_uiRequestedAddr; n < uiLastAddr; ++n) {
			uint16_t uiHoldingValue = ntohs(*pWord);
			m_pRequestedSlaveProxy->SetHoldingRegister(n, uiHoldingValue);
			MODBUS_DEBUG_INFO("Read reg addr:", n);
			MODBUS_DEBUG_INFO("Read reg value:", uiHoldingValue);
			++pWord;
		}
	} else {
		MODBUS_DEBUG_INFO("Size do not match. Got|expected:",  ((uint32_t)uiN << 8) | uiExpectedN);
	}
}
void CModbusMasterResponceReceiver::ProcessSlaveReadInputRegisters(uint8_t uiN, const void* pData, unsigned uiSize)
{
	unsigned uiExpectedN = m_uiRequestedCount * 2;
	if (uiN == uiExpectedN) {
		const uint16_t* pWord = (const uint16_t*)pData;
		unsigned uiLastAddr = m_uiRequestedAddr + m_uiRequestedCount;
		for (unsigned n = m_uiRequestedAddr; n < uiLastAddr; ++n) {
			uint16_t uiHoldingValue = ntohs(*pWord);
			m_pRequestedSlaveProxy->SetInputRegister(n, uiHoldingValue);
			++pWord;
		}
	} else {
		MODBUS_DEBUG_INFO("Size do not match. Got|expected:",  ((uint32_t)uiN << 8) | uiExpectedN);
	}
}
void CModbusMasterResponceReceiver::ProcessSlaveWriteSingleCoil(uint16_t uiAddr, uint16_t uiValue)
{
	MODBUS_DEBUG_INFO("done single coil write addr:", uiAddr);
	MODBUS_DEBUG_INFO("done single coil write data:", uiValue);
}
void CModbusMasterResponceReceiver::ProcessSlaveWriteSingleRegister(uint16_t uiAddr, uint16_t uiValue)
{
	MODBUS_DEBUG_INFO("done single reg write addr:", uiAddr);
	MODBUS_DEBUG_INFO("done single reg write data:", uiValue);
}
void CModbusMasterResponceReceiver::ProcessSlaveWriteMultipleCoils(uint16_t uiAddr, uint16_t uiCount)
{
	MODBUS_DEBUG_INFO("done multiple coils write addr:", uiAddr);
	MODBUS_DEBUG_INFO("done multiple coils write cnt :", uiCount);
}
void CModbusMasterResponceReceiver::ProcessSlaveWriteMultipleRegisters(uint16_t uiAddr, uint16_t uiCount)
{
	MODBUS_DEBUG_INFO("done multiple regs write addr:", uiAddr);
	MODBUS_DEBUG_INFO("done multiple regs write cnt :", uiCount);
}
void CModbusMasterResponceReceiver::ProcessErrorResponce(uint8_t uiN)
{
	MODBUS_DEBUG_INFO("Slave error:",  uiN);
}


/*
 *
 *  CModbusProcessorFSMMaster
 *
 */

class CModbusProcessorFSMMaster :
		public CModbusProcessorFSM,
		public IModbusMasterProcessor
{
public: // IModbusMasterProcessor
	virtual void Init(ISerial* pSerial);
	virtual void SetRequestsPeriod(unsigned uiPeriodTicks);
	virtual void AddSlaveInspector(IModbusSlaveProxy* pSlaveProxy);
public: // TProcess
	virtual void SingleStep(CTimeType uiTime);
protected:
	bool ProceedToNextSlave();
	virtual void ProcessRxedPkt();
	virtual void FinalizeTx();

	CModbusMasterRequestSource       m_RequesterComposer;
	CModbusMasterResponceReceiver    m_ResponceParser;

	unsigned            m_nSlaveInspectorsCount;
	IModbusSlaveProxy*  m_apSlaveProxies[4];
	unsigned            m_uiSlaveIndexCurrent;
	CTimeout            m_toNextRequestTimeout;
};

void CModbusProcessorFSMMaster::Init(ISerial* pSerial)
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusMasterProcessor::Init(pSerial));
	CModbusProcessorFSM::Init(pSerial);

	m_RequesterComposer.Bind(&m_OutgoingCache);
	m_IncomeCache.Bind(&m_ResponceParser);

	m_nSlaveInspectorsCount = 0;
	m_uiSlaveIndexCurrent = 0;
	SetRequestsPeriod(MILLI_TO_TICKS(100));
}
void CModbusProcessorFSMMaster::SetRequestsPeriod(unsigned uiPeriodTicks)
{
	m_toNextRequestTimeout.Init(uiPeriodTicks);
	m_toNextRequestTimeout.Start();
}

void CModbusProcessorFSMMaster::AddSlaveInspector(IModbusSlaveProxy* pSlaveProxy)
{
	IMPLEMENTS_INTERFACE_METHOD(IModbusMasterProcessor::AddSlaveInspector(pSlaveProxy));
	if (pSlaveProxy) {
		ASSERTE(m_nSlaveInspectorsCount < ARRAY_SIZE(m_apSlaveProxies));

		if (m_nSlaveInspectorsCount < ARRAY_SIZE(m_apSlaveProxies)) {
			m_apSlaveProxies[m_nSlaveInspectorsCount] = pSlaveProxy;
			++m_nSlaveInspectorsCount;
		}
		ContinueNow();
	}
}

void CModbusProcessorFSMMaster::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CModbusProcessorFSMMaster");

	MODBUS_DEBUG_INFO("Master step, inspectors:", m_nSlaveInspectorsCount);

	if (m_nSlaveInspectorsCount) {
		ASSERTE(m_uiSlaveIndexCurrent < m_nSlaveInspectorsCount);

		if (m_IncomeCache.GetCachedDataLength()) { // response received
			m_ResponceParser.SetRequestedContext(
					m_RequesterComposer.GetRequestedSlaveProxy(),
					m_RequesterComposer.GetRequestedFn(),
					m_RequesterComposer.GetRequestedAddr(),
					m_RequesterComposer.GetRequestedCount()
			);
			m_IncomeCache.Flush();
			m_IncomeCache.Reset();
		} else if (m_toNextRequestTimeout.GetIsElapsed()){
			m_toNextRequestTimeout.Start();
			if (m_RequesterComposer.GetRequestedSlaveProxy() && !m_RequesterComposer.GetIsDone()) {
				m_RequesterComposer.ComposeAndBroadcast();
			} else if (ProceedToNextSlave()) {
				m_RequesterComposer.ComposeAndBroadcast();
			} else {
				MODBUS_DEBUG_INFO("No slaves to inspect");
			}
			if (m_OutgoingCache.GetCachedDataLength()) {
				StartTxData();
			}
		}
		ContinueDelay(m_toNextRequestTimeout.GetTimeLeft() + 1);
	}

	PROCESS_DEBUG_INFO("<<< Process: CModbusProcessorFSMMaster");
}

bool CModbusProcessorFSMMaster::ProceedToNextSlave()
{
	bool bResult = false;
	unsigned nMaxLoops = m_nSlaveInspectorsCount;
	while (nMaxLoops) {
		++m_uiSlaveIndexCurrent;
		if (m_uiSlaveIndexCurrent >= m_nSlaveInspectorsCount) {
			m_uiSlaveIndexCurrent = 0;
		}
		IModbusSlaveProxy* pSlaveProxy = m_apSlaveProxies[m_uiSlaveIndexCurrent];
		if (pSlaveProxy && pSlaveProxy->GetSlaveAddress() && pSlaveProxy->GetDescription()->GetCommunicationEntriesCount()) {
			m_RequesterComposer.SetSlaveProxy(pSlaveProxy);
			bResult = true;
			break;
		}
		--nMaxLoops;
	}
	return bResult;
}

void CModbusProcessorFSMMaster::ProcessRxedPkt()
{
	CModbusProcessorFSM::ProcessRxedPkt();
	ContinueAsap(); // process pkt in main thread
}

void CModbusProcessorFSMMaster::FinalizeTx()
{
	CModbusProcessorFSM::FinalizeTx();
	StartWaitData(NULL);
}

CModbusProcessorFSMSlave g_ModbusProcessorSlave;
CModbusProcessorFSMMaster g_ModbusProcessorMaster;

IModbusSlaveProcessor* GetModbusSlaveProcessor()
{
	return &g_ModbusProcessorSlave;
}
IModbusMasterProcessor* GetModbusMasterProcessor()
{
	return &g_ModbusProcessorMaster;
}
