#ifndef MODULES_PARAMS_H_INCLUDED
#define MODULES_PARAMS_H_INCLUDED

#include "net/net_params.h"
#include "triac/triac_params.h"
#include "can/can_params.h"

#define MODULES_PARAMS_LIST \
	NET_PARAMS_LIST \
	TRIAC_PARAMS_LIST \
	CAN_PARAMS_LIST \


#endif // MODULES_PARAMS_H_INCLUDED
