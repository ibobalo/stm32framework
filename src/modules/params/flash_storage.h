#ifndef _FLASH_STORAGE_H_INCLUDED
#define _FLASH_STORAGE_H_INCLUDED

//example:
// static const CFlashMemStorage<int, 100> ConfigMyValue;
// int my_value = 0;
// void init() { ConfigMyValue.Restore(&my_value); }
// void change(int new_value) { ConfigMyValue.Store(my_value); }


template<class T>
class CFlashMem
{
public:
	inline bool Set(T uiNewValue) const {return Empty() && CHW_MCU::Flash(const_cast<T*>(&m_value), &uiNewValue, sizeof(m_value)) && !Empty();}
	inline T Get() const {return m_value;}
	inline bool Empty() const {return m_value == EMPTYVALUE();}
private:
	static_assert((sizeof(T) & 1) == 0); // F0 unable to write 1-byte to flash
	static constexpr T EMPTYVALUE() {return T(-1);}
	volatile T m_value = EMPTYVALUE();
};

template<class T, unsigned MaxChangesCount>
class CFlashMemStorage
{
public:
	bool Restore(T* pParam) const {
		for (unsigned n = 0; n < MaxChangesCount; ++n) {
			const auto& x = storage[n];
			if (x.Empty())
				return (n != 0);
			*pParam = x.Get();
		}
		return true;
	}
	bool Store(const T* pParam) const {
		for (unsigned n = 0; n < MaxChangesCount; ++n) {
			auto& x = storage[n];
			if (x.Empty()) {
				return x.Set(*pParam);
			}
		}
		return false;
	}
	bool IsFull() const {
		return !storage[MaxChangesCount - 1].Empty();
	}
private:
	CFlashMem<T> storage[MaxChangesCount];
};


#endif // _FLASH_STORAGE_H_INCLUDED
