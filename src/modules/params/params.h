#ifndef PARAMS_H_INCLUDED
#define PARAMS_H_INCLUDED

#include "util/callback.h"
#include "util/macros.h"
#include <stdint.h>

#ifdef USE_APP_PARAMS
#include "app_params.h"
#else
#define APP_PARAMS_LIST
#endif

#include "../modules_params.h"

#define GLOBAL_PARAMS_LIST \
	MODULES_PARAMS_LIST \
	\
	PARAM(USB2UART_UART,                      uint8_t,   1) \
	PARAM(USB2UART_BAUDRATE,                  unsigned,  115200) \
	PARAM(USB2UART_STOPBITS,                  uint8_t,   1, "0 - 0.5, 1 - 1, 2 - 2, 3 - 1.5") \
	PARAM(USB2UART_PARITYBITS,                uint8_t,   0, "0 - None, 1 - Even, 2 - Odd") \
	\
	PARAM(HW_RFM69_ENABLE,                    bool,      false) \
	\
	PARAM(HW_MPU6050_ENABLE,                  bool,      false) \
	PARAM(HW_MPU6050_REFRESH_TIME_MS,         uint16_t,  1000) \
	\
	PARAM(HW_HMC5883_ENABLE,                  bool,      false) \
	PARAM(HW_HMC5883_REFRESH_TIME_MS,         uint16_t,  1000) \
	\
	APP_PARAMS_LIST


#define GET_PARAM_DEFAULT(name, ...) GetGlobalParamDefault_##name(__VA_ARGS__)
#define GET_PARAM_VALUE(name, ...) GetGlobalParamValue_##name(__VA_ARGS__)
#define SET_PARAM_VALUE(name, ...) SetGlobalParamValue_##name(__VA_ARGS__)
#define SET_PARAM_CALLBACK(name, cb) SetGlobalParamCallback_##name(cb)

#ifndef USE_PARAMS  // not USE_PARAMS, all params are const


#define FIXED_DECLARATION(name, t, default_value, ...) \
	typedef t GlobalParamType_##name; \
	inline void SetGlobalParamValue_##name(unsigned n, const GlobalParamType_##name value) {} \
	inline const GlobalParamType_##name GetGlobalParamDefault_##name() {return default_value;}; \
	inline const GlobalParamType_##name GetGlobalParamValue_##name() {return GetGlobalParamDefault_##name();}; \
	inline void SetGlobalParamCallback_##name(Callback<void(void)> cb) {}
#define FIXED_ARRAY_DECLARATION(name, t, length, default_values, ...) \
	typedef t GlobalParamType_##name; \
	inline void SetGlobalParamValue_##name(unsigned n, const GlobalParamType_##name value) {} \
	inline const GlobalParamType_##name GetGlobalParamDefault_##name(unsigned index) {static const GlobalParamType_##name name##_default[length] = {default_values}; return name##_default[index];} \
	inline const GlobalParamType_##name GetGlobalParamValue_##name(unsigned index) {return GetGlobalParamDefault_##name(index);} \
	inline void SetGlobalParamCallback_##name(Callback<void(void)> cb) {}

#define PARAM  FIXED_DECLARATION
#define PARAM_ARRAY  FIXED_ARRAY_DECLARATION
 GLOBAL_PARAMS_LIST
#undef PARAM
#undef PARAM_ARRAY

#else // USE_PARAMS

#define PARAM_DECLARATION(name, t, default_value, ...) \
	typedef t GlobalParamType_##name; \
	extern GlobalParamType_##name name##_value; \
	extern const GlobalParamType_##name name##_default; \
	extern Callback<void(void)> name##_cb; \
	void SetGlobalParamValue_##name(const GlobalParamType_##name val); \
	inline const GlobalParamType_##name GetGlobalParamDefault_##name() {return name##_default;}; \
	inline const GlobalParamType_##name GetGlobalParamValue_##name() {return name##_value;} \
	inline void SetGlobalParamCallback_##name(Callback<void(void)> cb) {name##_cb = cb;}
#define PARAM_ARRAY_DECLARATION(name, t, length, default_values, ...) \
	typedef t GlobalParamType_##name; \
	extern GlobalParamType_##name name##_value[length]; \
	extern const GlobalParamType_##name name##_default[length]; \
	extern Callback<void(void)> name##_cb; \
	void SetGlobalParamValue_##name(unsigned n, const GlobalParamType_##name value); \
	inline const GlobalParamType_##name GetGlobalParamDefault_##name(unsigned index) {return name##_default[index];} \
	inline const GlobalParamType_##name GetGlobalParamValue_##name(unsigned index) {return name##_value[index];} \
	inline void SetGlobalParamCallback_##name(Callback<void(void)> cb) {name##_cb = cb;}

#define PARAM  PARAM_DECLARATION
#define PARAM_ARRAY  PARAM_ARRAY_DECLARATION
 GLOBAL_PARAMS_LIST
#undef PARAM
#undef PARAM_ARRAY


#define PARAM_ID_ENUM(name, ...) \
	GLOBAL_PARAM_ID_##name,
typedef enum {
#define PARAM  PARAM_ID_ENUM
#define PARAM_ARRAY  PARAM_ID_ENUM
	GLOBAL_PARAMS_LIST
#undef PARAM
#undef PARAM_ARRAY
	GLOBAL_PARAMS_COUNT
} global_params_id_t;
typedef enum {
	GLOBAL_PARAMS_TYPE_bool,
	GLOBAL_PARAMS_TYPE_int,
		GLOBAL_PARAMS_TYPE_int8_t = GLOBAL_PARAMS_TYPE_int,
		GLOBAL_PARAMS_TYPE_int16_t = GLOBAL_PARAMS_TYPE_int,
		GLOBAL_PARAMS_TYPE_int32_t = GLOBAL_PARAMS_TYPE_int,
	GLOBAL_PARAMS_TYPE_unsigned,
		GLOBAL_PARAMS_TYPE_uint8_t = GLOBAL_PARAMS_TYPE_unsigned,
		GLOBAL_PARAMS_TYPE_uint16_t = GLOBAL_PARAMS_TYPE_unsigned,
		GLOBAL_PARAMS_TYPE_uint32_t = GLOBAL_PARAMS_TYPE_unsigned,
	GLOBAL_PARAMS_TYPE_float,
		GLOBAL_PARAMS_TYPE_double = GLOBAL_PARAMS_TYPE_float,
} global_params_type_t;

static inline unsigned GetGlobalParamsCount() {return (unsigned)GLOBAL_PARAMS_COUNT;};
const char* GetGlobalParamName(global_params_id_t eID);
global_params_type_t GetGlobalParamType(global_params_id_t eID);
unsigned    GetGlobalParamSize     (global_params_id_t eID);
unsigned    GetGlobalParamLength   (global_params_id_t eID); // for arrays
const char* GetGlobalParamDesc     (global_params_id_t eID);

bool GetGlobalParamDefault_bool    (global_params_id_t eID, bool& bResult);
bool GetGlobalParamDefault_int     (global_params_id_t eID, int& iResult);
bool GetGlobalParamDefault_unsigned(global_params_id_t eID, unsigned& uiResult);
bool GetGlobalParamDefault_float   (global_params_id_t eID, double& dResult);

bool GetGlobalParamValue_bool    (global_params_id_t eID, bool& bResult);
bool GetGlobalParamValue_int     (global_params_id_t eID, int& iResult);
bool GetGlobalParamValue_unsigned(global_params_id_t eID, unsigned& uiResult);
bool GetGlobalParamValue_float   (global_params_id_t eID, double& dResult);

bool SetGlobalParamValue_bool    (global_params_id_t eID, bool bValue);
bool SetGlobalParamValue_int     (global_params_id_t eID, int iValue);
bool SetGlobalParamValue_unsigned(global_params_id_t eID, unsigned uiValue);
bool SetGlobalParamValue_float   (global_params_id_t eID, float dValue);

void RestoreParamValues();
bool ClearParamsStorage();

#endif // USE_PARAMS

#endif /* PARAMS_H_INCLUDED */
