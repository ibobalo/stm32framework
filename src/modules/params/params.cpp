#include "stdafx.h"
#include "params.h"
#include "util/macros.h"
#include "hw/hw.h" // Flash_*
#include "hw/debug.h"
#include <stddef.h>
#include <string.h>
#include "stm32_linker.h"

///////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
	uint16_t  uiRecordType;  // 0x0000 - invalid record, 0xFFFF - unused, else eParamID + 1
	uint16_t  uiSize;
	uint8_t   auiValue[];
} ParamsPersistentStorageRecord_t;
#define INVALID_FLASH_RECORD_TYPE 0x0000
#define EMPTY_FLASH_RECORD_TYPE   0xFFFF
#define INVALID_FLASH_RECORD_SIZE 0xFFFF

const void* GetLastEEPROMParamValue(global_params_id_t eParamID, unsigned uiSize)
{
	const void* pResult = NULL;
	const uint8_t* pStorageBegin = GetLinkerSymbol(const uint8_t*, eeprom_data_start);
	const uint8_t* pStorageEnd = pStorageBegin + GetLinkerSymbol(unsigned, eeprom_data_size);
	const ParamsPersistentStorageRecord_t* pRecord = (const ParamsPersistentStorageRecord_t*)pStorageBegin;
	uint16_t uiRecordType = eParamID + 1;
	while (pRecord->uiRecordType != EMPTY_FLASH_RECORD_TYPE && (uint8_t*)pRecord < pStorageEnd) {
		const uint8_t* pValue = pRecord->auiValue;
		uint16_t uiAlignedDataSize = uiSize;
		IF_STM32F0(uiAlignedDataSize = SIZE_ALIGN(uiAlignedDataSize, 2));
		if (pRecord->uiRecordType == uiRecordType && (!uiSize || pRecord->uiSize == uiAlignedDataSize)) {
			pResult = pValue;
		}
		unsigned uiDataSize = ((pRecord->uiSize == INVALID_FLASH_RECORD_SIZE)?0:pRecord->uiSize);
		unsigned uiAlignedRecordSize = SIZE_ALIGN(sizeof(*pRecord) + uiDataSize, 4);
		pRecord = (ParamsPersistentStorageRecord_t*)((uint8_t*)pRecord + uiAlignedRecordSize);
	}
	return pResult;
}

bool StoreParamValueToEEPROM(global_params_id_t eParamID, const void* pNewValue, unsigned uiSize)
{
	bool bResult = false;
	const uint8_t* pStorageBegin = GetLinkerSymbol(const uint8_t*, eeprom_data_start);
	const uint8_t* pStorageEnd = pStorageBegin + GetLinkerSymbol(unsigned, eeprom_data_size);
	const ParamsPersistentStorageRecord_t* pRecord = (const ParamsPersistentStorageRecord_t*)pStorageBegin;
	uint16_t uiRecordType = eParamID + 1;
	while (pRecord->uiRecordType != EMPTY_FLASH_RECORD_TYPE && (uint8_t*)pRecord < pStorageEnd) {
		unsigned uiDataSize = ((pRecord->uiSize == INVALID_FLASH_RECORD_SIZE)?0:pRecord->uiSize);
		unsigned uiAlignedRecordSize = SIZE_ALIGN(sizeof(*pRecord) + uiDataSize, 4);
		pRecord = (ParamsPersistentStorageRecord_t*)((uint8_t*)pRecord + uiAlignedRecordSize);
	}
	const uint8_t* pValue = pRecord->auiValue;
	uint16_t uiAlignedDataSize = uiSize;
	IF_STM32F0(uiAlignedDataSize = SIZE_ALIGN(uiAlignedDataSize, 2));
	if (pValue + uiAlignedDataSize >= pStorageEnd) {
		DEBUG_INFO("Persistent storage overflow");
	} else {
		ParamsPersistentStorageRecord_t* pTargetRecord = (ParamsPersistentStorageRecord_t*)pRecord;
		bResult = CHW_MCU::Flash(&pTargetRecord->uiSize, &uiAlignedDataSize, 2);
		if (bResult) {
			bResult = CHW_MCU::Flash(pTargetRecord->auiValue, pNewValue, uiAlignedDataSize);
			if (bResult) {
				bResult = CHW_MCU::Flash(&pTargetRecord->uiRecordType, &uiRecordType, 2);
			}
		}

		if (!bResult) {
			uint16_t uiRecordType = INVALID_FLASH_RECORD_TYPE;
			CHW_MCU::Flash(&pTargetRecord->uiRecordType, &uiRecordType, 2);
		}
	}
	return bResult;
}

bool ClearParamsStorage()
{
	bool bResult = true;
	const uint8_t* pStorageBegin = GetLinkerSymbol(const uint8_t*, eeprom_data_start);
	const uint8_t* pStorageEnd = pStorageBegin + GetLinkerSymbol(unsigned, eeprom_data_size);
	unsigned uiFirstPage = PtrToPageNumber(pStorageBegin);
	unsigned uiLastPage = PtrToPageNumber(pStorageEnd - 1);
	for (unsigned n = uiFirstPage; n <= uiLastPage; ++n) {
		bResult = CHW_MCU::ErasePage(n) && bResult;
		CHW_MCU::KickIWDG();
	}
	RestoreParamValues();
	return bResult;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

#define PARAM_DEFINITION(name, t, default_value, ...) \
	GlobalParamType_##name name##_value; \
	const GlobalParamType_##name name##_default = default_value; \
	Callback<void(void)> name##_cb; \
	void SetGlobalParamValue_##name(GlobalParamType_##name val) { \
		if (name##_value != val) { \
			name##_value = val; \
			StoreParamValueToEEPROM(GLOBAL_PARAM_ID_##name, &name##_value, sizeof(name##_value)); \
			if (name##_cb) name##_cb(); \
		} \
	}
#define PARAM_ARRAY_DEFINITION(name, t, length, default_values, ...) \
	GlobalParamType_##name name##_value[length]; \
	const GlobalParamType_##name name##_default[length] = {default_values}; \
	Callback<void(void)> name##_cb; \
	void SetGlobalParamValue_##name(unsigned index, const GlobalParamType_##name val) { \
		STATIC_ASSERTE(length > 1, invalid_array_length); \
		if (name##_value[index] != val) { \
			name##_value[index] = val; \
			StoreParamValueToEEPROM(GLOBAL_PARAM_ID_##name, &name##_value, sizeof(name##_value)); \
			if (name##_cb) name##_cb(); \
		} \
	}
#define PARAM PARAM_DEFINITION
#define PARAM_ARRAY PARAM_ARRAY_DEFINITION
	GLOBAL_PARAMS_LIST
#undef PARAM
#undef PARAM_ARRAY

///////////////////////////////////////////////////////////////////////////////////////////////////
void RestoreParamValues()
{
#define PARAM_VALUE_RESTORER_DEFINITION(name, ...) \
	{ \
		const GlobalParamType_##name* p = (const GlobalParamType_##name*)GetLastEEPROMParamValue(GLOBAL_PARAM_ID_##name, sizeof(name##_value)); \
		if (!p) { p = &name##_default; } \
		if (name##_value != *p) { \
			name##_value = *p; \
			if (name##_cb) name##_cb(); \
		} \
	}
#define PARAM_ARRAY_VALUE_RESTORER_DEFINITION(name, ...) \
	{ \
		const GlobalParamType_##name* p = (const GlobalParamType_##name*)GetLastEEPROMParamValue(GLOBAL_PARAM_ID_##name, sizeof(name##_value)); \
		if (!p) { p = name##_default; } \
		if (memcmp(&name##_value, p, sizeof(name##_value)) != 0) { \
			memcpy(&name##_value, p, sizeof(name##_value)); \
			if (name##_cb) name##_cb(); \
		} \
	}
#define PARAM  PARAM_VALUE_RESTORER_DEFINITION
#define PARAM_ARRAY  PARAM_ARRAY_VALUE_RESTORER_DEFINITION
	GLOBAL_PARAMS_LIST
#undef PARAM
#undef PARAM_ARRAY
}

///////////////////////////////////////////////////////////////////////////////////////////////////


typedef void (void_setter)(void*);
typedef struct {
	const char* name;
	const char* desc;
	global_params_type_t type;
	unsigned size;
	unsigned length;
	void* dataptr;
	const void* defptr;
	void_setter* setter;
} param_descs_t;
param_descs_t param_descs[GLOBAL_PARAMS_COUNT] = {
#define PARAM(name, t, default_value, ...) \
	{#name, __VA_ARGS__ "", GLOBAL_PARAMS_TYPE_##t, sizeof(t), 0,      &name##_value, &name##_default, (void_setter*)&SetGlobalParamValue_##name},
#define PARAM_ARRAY(name, t, length, default_values, ...) \
	{#name, __VA_ARGS__ "", GLOBAL_PARAMS_TYPE_##t, sizeof(t), length, &name##_value, &name##_default, (void_setter*)&SetGlobalParamValue_##name},
	GLOBAL_PARAMS_LIST
#undef PARAM
#undef PARAM_ARRAY
};

///////////////////////////////////////////////////////////////////////////////////////////////////

const char* GetGlobalParamName(global_params_id_t eID)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		return desc.name;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

const char* GetGlobalParamDesc(global_params_id_t eID)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		return desc.desc;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

global_params_type_t GetGlobalParamType(global_params_id_t eID)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		return desc.type;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return GLOBAL_PARAMS_TYPE_bool;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

unsigned GetGlobalParamSize(global_params_id_t eID)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		return desc.size;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

unsigned GetGlobalParamLength(global_params_id_t eID)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		return desc.length;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
bool GetGlobalParamDefault (global_params_id_t eID, T& outValue)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		outValue = *(const T*) desc.defptr;
		return true;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
bool GetGlobalParamArrayDefault (global_params_id_t eID, unsigned nIndex, T& outValue)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		if (nIndex < desc.length) {
			outValue = ((const T*) desc.defptr)[nIndex];
			return true;
		}
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
bool GetGlobalParamValue (global_params_id_t eID, T& outValue)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		outValue = *(const T*) desc.dataptr;
		return true;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
bool GetGlobalParamArrayValue (global_params_id_t eID, unsigned nIndex, T& outValue)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		if (nIndex < desc.length) {
			outValue = ((const T*) desc.dataptr)[nIndex];
			return true;
		}
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
bool SetGlobalParamValue (global_params_id_t eID, T Value)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		ASSERTE(desc.size == sizeof(Value));
		ASSERTE(desc.length == 0);
		typedef void (setter_fn_type)(T);
		setter_fn_type* setter_fn = (setter_fn_type*) (desc.setter);
		(*setter_fn)(Value);
		return true;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
bool SetGlobalParamArrayValue (global_params_id_t eID, unsigned nIndex, T Value)
{
	if ((unsigned)eID < ARRAY_SIZE(param_descs)) {
		const param_descs_t& desc = param_descs[(unsigned)eID];
		ASSERTE(desc.size = sizeof(Value));
		ASSERTE(desc.length > 0);
		if (nIndex < desc.length) {
			typedef void (setter_fn_type)(unsigned, T);
			setter_fn_type* setter_fn = (setter_fn_type*) (desc.setter);
			(*setter_fn)(nIndex, Value);
			return true;
		} else {
			DEBUG_INFO("Invalid param index:", nIndex);
		}
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/*
bool GetGlobalParamDefault_bool(global_params_id_t eID, bool& bResult)
{
	switch (eID) {
#define PARAM_DEFAULT_GETTER_DEFINITION_BOOL(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_bool) { \
				bResult = GET_PARAM_DEFAULT(name); \
				return true;\
			} \
			break; \
		}
#define PARAM  PARAM_DEFAULT_GETTER_DEFINITION_BOOL
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool GetGlobalParamDefault_int(global_params_id_t eID, int& iResult)
{
	switch (eID) {
#define PARAM_DEFAULT_GETTER_DEFINITION_INT(name, t, default_value, ...) { \
		case GLOBAL_PARAM_ID_##name: \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_int) { \
				iResult = GET_PARAM_DEFAULT(name); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_DEFAULT_GETTER_DEFINITION_INT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool GetGlobalParamDefault_unsigned(global_params_id_t eID, unsigned& uiResult)
{
	switch (eID) {
#define PARAM_DEFAULT_GETTER_DEFINITION_UNSIGNED(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_unsigned) { \
				uiResult = GET_PARAM_DEFAULT(name); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_DEFAULT_GETTER_DEFINITION_UNSIGNED
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool GetGlobalParamDefault_float(global_params_id_t eID, double& dResult)
{
	switch (eID) {
#define PARAM_DEFAULT_GETTER_DEFINITION_FLOAT(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_float) { \
				dResult = GET_PARAM_DEFAULT(name); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_DEFAULT_GETTER_DEFINITION_FLOAT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

bool GetGlobalParamValue_bool(global_params_id_t eID, bool& bResult)
{
	switch (eID) {
#define PARAM_VALUE_GETTER_DEFINITION_BOOL(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_bool) { \
				bResult = GET_PARAM_VALUE(name); \
				return true;\
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_GETTER_DEFINITION_BOOL
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

bool GetGlobalParamValue_int(global_params_id_t eID, int& iResult)
{
	switch (eID) {
#define PARAM_VALUE_GETTER_DEFINITION_INT(name, t, default_value, ...) { \
		case GLOBAL_PARAM_ID_##name: \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_int) { \
				iResult = GET_PARAM_VALUE(name); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_GETTER_DEFINITION_INT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool GetGlobalParamValue_unsigned(global_params_id_t eID, unsigned& uiResult)
{
	switch (eID) {
#define PARAM_VALUE_GETTER_DEFINITION_UNSIGNED(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_unsigned) { \
				uiResult = GET_PARAM_VALUE(name); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_GETTER_DEFINITION_INT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool GetGlobalParamValue_float(global_params_id_t eID, double& dResult)
{
	switch (eID) {
#define PARAM_VALUE_GETTER_DEFINITION_FLOAT(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_float) { \
				dResult = GET_PARAM_VALUE(name); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_GETTER_DEFINITION_FLOAT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}

bool SetGlobalParamValue_bool(global_params_id_t eID, bool bValue)
{
	switch (eID) {
#define PARAM_VALUE_SETTER_DEFINITION_BOOL(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_bool) { \
				SetGlobalParamValue_##name(bValue); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_SETTER_DEFINITION_BOOL
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool SetGlobalParamValue_int(global_params_id_t eID, int iValue)
{
	switch (eID) {
#define PARAM_VALUE_SETTER_DEFINITION_INT(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_int) { \
				SetGlobalParamValue_##name(iValue); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_SETTER_DEFINITION_INT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool SetGlobalParamValue_unsigned(global_params_id_t eID, unsigned uiValue)
{
	switch (eID) {
#define PARAM_VALUE_SETTER_DEFINITION_UNSIGNED(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_unsigned) { \
				SetGlobalParamValue_##name(uiValue); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_SETTER_DEFINITION_UNSIGNED
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
bool SetGlobalParamValue_float(global_params_id_t eID, float dValue)
{
	switch (eID) {
#define PARAM_VALUE_SETTER_DEFINITION_FLOAT(name, t, default_value, ...) \
		case GLOBAL_PARAM_ID_##name: { \
			if (GLOBAL_PARAMS_TYPE_##t == GLOBAL_PARAMS_TYPE_float) { \
				SetGlobalParamValue_##name(dValue); \
				return true; \
			} \
			break; \
		}
#define PARAM  PARAM_VALUE_SETTER_DEFINITION_FLOAT
	GLOBAL_PARAMS_LIST
#undef PARAM
	default:
		break;
	}
	DEBUG_INFO("Invalid param id:", eID);
	return false;
}
*/
