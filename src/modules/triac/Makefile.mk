INCLUDE_PATH += $(if $(filter YES,$(USE_TRIAC)), \
	src/modules/triac \
)
SRC += $(if $(filter YES,$(USE_TRIAC)), $(addprefix src/modules/triac/, \
	zero_sync_pwm.cpp \
))
