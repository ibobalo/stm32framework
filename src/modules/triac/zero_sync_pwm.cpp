#include "stdafx.h"
#include "zero_sync_pwm.h"
#include "util/macros.h"
#include "util/utils.h"
#include "interfaces/led.h"
#include "interfaces/timer.h"
#include "hw/hw.h"
#include "hw/debug.h"
#include "hw/tick_timeout.h"
#include "params.h"
#include <stdint.h>

#define TRIAC_SYNC_TYPE_RAW
#define TRIAC_ZERO_FINE_STATS_COUNT                16
#define PULSING_FLT_DELAY_MS                       50
#define SYNC_FLT_DELAY_MS                          100
#define FREQ_THRESHOLD(TIMER_RERIOD)               (int)((TIMER_RERIOD) / 2)
#define STABLE_THRESHOLD(TIMER_RERIOD)             (int)((TIMER_RERIOD) / 2)
#define PHASE_AVERAGING_N                          2
#define FREQ_ADJUSTMENT_AVERAGING_N                2
#define PHASE_TUNING_GAIN(PHASE_ERROR)             ((PHASE_ERROR) / (2 * PHASE_AVERAGING_N))
#define FREQ_ADJUSTMENT_GAIN(AVG_PHASE_ERROR)      ((AVG_PHASE_ERROR) / (2 * PHASE_AVERAGING_N * FREQ_ADJUSTMENT_AVERAGING_N))
#ifdef TRIAC_EXT_ZERO
#define TRIAC_ZERO_SENSOR_OFFSET(TIMER_RERIOD)    (int)(GET_PARAM_VALUE(TRIAC_ZERO_EXT_SENSOR_OFFSET_MUL) * ((int)(TIMER_RERIOD)) / 1024L + GET_PARAM_VALUE(TRIAC_ZERO_EXT_SENSOR_OFFSET_ABS))
#define ZERO_SENSOR_MODE                          GET_PARAM_VALUE(TRIAC_ZERO_EXT_SENSOR_MODE)
#else
#define TRIAC_ZERO_SENSOR_OFFSET(TIMER_RERIOD)    (int)(GET_PARAM_VALUE(TRIAC_ZERO_SENSOR_OFFSET_MUL) * ((int)(TIMER_RERIOD)) / 1024L + GET_PARAM_VALUE(TRIAC_ZERO_SENSOR_OFFSET_ABS))
#define ZERO_SENSOR_MODE                          GET_PARAM_VALUE(TRIAC_ZERO_SENSOR_MODE)
#endif
#define TRIAC_ZERO_GUARD_OFFSET_PULSING(TIMER_RERIOD) (int)(GET_PARAM_VALUE(TRIAC_ZERO_GUARD_OFFSET) * ((int)(TIMER_RERIOD)) / 1024L)
#define TRIAC_ZERO_GUARD_OFFSET_FREQ(TIMER_RERIOD)    (int)(GET_PARAM_VALUE(TRIAC_ZERO_GUARD_OFFSET) * ((int)(TIMER_RERIOD)) / 1024L)
#define TRIAC_ZERO_GUARD_OFFSET_STABLE(TIMER_RERIOD)  (int)(GET_PARAM_VALUE(TRIAC_ZERO_GUARD_OFFSET) * ((int)(TIMER_RERIOD)) / 1024L)

#define TRIAC_ZERO_TOLERANCE_PULSING(TIMER_RERIOD) (int)((TIMER_RERIOD) / 4)
#define TRIAC_ZERO_TOLERANCE_FREQ(TIMER_RERIOD)    (int)((TIMER_RERIOD) / 4)
#define TRIAC_ZERO_TOLERANCE_STABLE(TIMER_RERIOD)  (int)((TIMER_RERIOD) / 16)
#define PERIOD_TOLERANCE_PULSING(TIMER_RERIOD)     ((TIMER_RERIOD) / 2)
#define PERIOD_TOLERANCE_PREQ(TIMER_RERIOD)        ((TIMER_RERIOD) / 4)
#define PERIOD_TOLERANCE_STABLE(TIMER_RERIOD)      ((TIMER_RERIOD) / 16)

//#define ZERO_DEBUG_INFO(...) DEBUG_INFO("ZERO:" __VA_ARGS__)
#define ZERO_DEBUG_INFO(...) {}

//extern
class CHW_MCU;

static const uint8_t g_Brezenham2[] = {
		0b00,  // 0
		0b10,  // 1
		0b11   // 2
};
static const uint8_t g_Brezenham4[] = {
		0b0000, // 0
		0b1000, // 1
		0b0101, // 2
		0b1101, // 3
		0b1111  // 4
};
static const uint8_t g_Brezenham8[] = {
		0b00000000, // 0
		0b10000000, // 1
		0b00010001, // 2
		0b10010010, // 3
		0b01010101, // 4
		0b10110101, // 5
		0b01110111, // 6
		0b11110111, // 7
		0b11111111  // 8 = 8
};
static const uint16_t g_Brezenham16[] = {
		0b0000000000000000, // 0
		0b1000000000000000, // 1
		0b0000000100000001, // 2
		0b0100000100000100, // 3
		0b0001000100010001, // 4
		0b0100100100010010, // 5
		0b0010010100100101, // 6
		0b1001010100101010, // 7
		0b0101010101010101, // 8
		0b1010101101010101, // 9
		0b0101101101011011, // 10
		0b1011011101101101, // 11
		0b0111011101110111, // 12
		0b1101111101111011, // 13
		0b0111111101111111, // 14
		0b1111111101111111, // 15
		0b1111111111111111  // 16
};
static const uint32_t g_Brezenham32[] = {
		0b00000000000000000000000000000000, // 0
		0b10000000000000000000000000000000, // 1
		0b00000000000000010000000000000001, // 2
		0b00001000000000010000000000100000, // 3
		0b00000001000000010000000100000001, // 4
		0b00010000010000010000001000001000, // 5
		0b00000100001000010000010000100001, // 6
		0b00100010000100010000100001000100, // 7
		0b00010001000100010001000100010001, // 8
		0b00100100100010010001000100100010, // 9
		0b00010010010010010001001001001001, // 10
		0b01001001001001010010010010010010, // 11
		0b00100101001001010010010100100101, // 12
		0b01010010100101010010100101001010, // 13
		0b00101010010101010010101001010101, // 14
		0b10010101010101010010101010101010, // 15
		0b01010101010101010101010101010101, // 16
		0b10101010101010110101010101010101, // 17
		0b01010101101010110101010110101011, // 18
		0b10101101011010110101011010110101, // 19
		0b01011011010110110101101101011011, // 20
		0b10110110110110110101101101101101, // 21
		0b01101101101101110110110110110111, // 22
		0b10111011011101110110111011011101, // 23
		0b01110111011101110111011101110111, // 24
		0b11011101111011110111011110111011, // 25
		0b01111011110111110111101111011111, // 26
		0b11101111101111110111110111110111, // 27
		0b01111111011111110111111101111111, // 28
		0b11111011111111110111111111011111, // 29
		0b01111111111111110111111111111111, // 30
		0b11111111111111110111111111111111, // 31
		0b11111111111111111111111111111111  // 32
};

bool M_OutOf_N(unsigned nActive, unsigned nTotal, unsigned nPhase)
{
	ASSERTE(nActive <= nTotal);

	switch (nTotal) {
	case 32:
		ASSERTE(nActive < ARRAY_SIZE(g_Brezenham32));
		return (g_Brezenham32[nActive] >> (nPhase%32)) & 0x1;
	case 16:
		ASSERTE(nActive < ARRAY_SIZE(g_Brezenham16));
		return (g_Brezenham16[nActive] >> (nPhase%16)) & 0x1;
	case 8:
		ASSERTE(nActive < ARRAY_SIZE(g_Brezenham8));
		return (g_Brezenham8[nActive] >> (nPhase%8)) & 0x1;
	case 4:
		ASSERTE(nActive < ARRAY_SIZE(g_Brezenham4));
		return (g_Brezenham4[nActive] >> (nPhase%4)) & 0x1;
	case 2:
		ASSERTE(nActive < ARRAY_SIZE(g_Brezenham2));
		return (g_Brezenham2[nActive] >> (nPhase%2)) & 0x1;
	default:
		ASSERT("Invalid TRIAC_INTERLEAVE");
	}
	return false;
}

float WaveInterleave(float dValue, unsigned nTotalParts, unsigned* puiResultActiveParts)
{
	float dResult;

	float dHighValue = dValue * nTotalParts;
	unsigned nActiveParts;

	if (dHighValue > 1.0) {
		nActiveParts = dHighValue + 1;

		ASSERTE(nActiveParts > 0);
	} else {
		nActiveParts = 1;
	}

	if (nActiveParts >= nTotalParts) {
		dResult = dValue;
	} else {
		dResult = dValue * nTotalParts / nActiveParts;
		*puiResultActiveParts = nActiveParts;
		ASSERTE(dResult >= dValue);
		ASSERTE(dResult <= 1.0);
	}


	return dResult;
}


class CTriacChannel :
		public ITriacChannel<float>
{
public:
	void Init(ITimerChannel* pChannel);
	void Deinit();
public: // IValueRequester
	virtual void SetValueSourceInterface(const IValueSource<float>* pValueSource);
public: // IValueTarget
	virtual void SetValue(float dValue);
public: // IValueSource
	virtual float GetValue() const;
public: // ITriacChannel
	virtual void SetBoostParams(unsigned nMaxBoostSamples, unsigned nMinInactivitySamples, unsigned nInactivitySamplesFor1BoostSample);
	virtual void SetInterleaveParams(unsigned nInterleaveParts, float fInterleaveThreshold);
	virtual void SetCalibrationTransformer(ITransformer<float>* pCalibrationTransformer);

public:
	void RequestSourceValue();
	void ReloadTimer(
			bool bForceRecalc,
			bool bAllowMiddle,
			unsigned uiZeroGuardOffset,
			unsigned uiChannelPhase
	);
	void Off();

private:
	float GetSourceValue() const {return m_fSourceValue;};
	bool SetSourceValue(float dValue);
	float GetCalibratedValue() const {return m_fCalibratedValue;};
	bool SetCalibratedValue(float dValue);
	bool GetIsRecalcReqiured() const {return m_bRecalcRequired;};
	bool SetInterleaveActivePart(unsigned uiValue);
	bool RecalcTimerDelay(float fValue, bool bAllowMiddle, unsigned uiZeroGuardOffset);
	unsigned GetInterleaveActivePart() const {return m_uiInterleaveActivePart;}

	ITimerChannel*      m_pTimerChannel;
	float               m_fActualValue;
	unsigned            m_uiInterleaveActivePart;
	const IValueSource* m_pValueSource;
	float               m_fSourceValue;
	ITransformer<float>* m_pCalibrationTransformer;
	float               m_fCalibratedValue;
	bool                m_bRecalcRequired;
	unsigned            m_nInactivitySamples;
	unsigned            m_nMaxBoostSamples;
	unsigned            m_nMinInactivitySamples;
	unsigned            m_nInactivitySamplesFor1BoostSample;
	unsigned            m_nInterleaveTotalParts;
	float               m_fInterleaveTreshold;
};

void CTriacChannel::Init(ITimerChannel* pChannel)
{
	ASSERTE(pChannel);
	m_pTimerChannel = pChannel;
	m_fActualValue = 0.0;
	m_uiInterleaveActivePart = 0.0;
	m_pValueSource = NULL;
	m_fSourceValue = 0.0;
	m_fCalibratedValue = 0.0;
	m_bRecalcRequired = true;
	m_nMaxBoostSamples = 0;
	m_nMinInactivitySamples = 0;
	m_nInactivitySamplesFor1BoostSample = 0;
	m_nInactivitySamples = 0;
	m_nInterleaveTotalParts = 0;
	m_fInterleaveTreshold = 0.0;
};

void CTriacChannel::Deinit()
{
}

void CTriacChannel::SetValueSourceInterface(const IValueSource<float>* pValueSource)
{
	IMPLEMENTS_INTERFACE_METHOD(IValueRequester::SetValueSourceInterface(pValueSource));
	ENTER_CRITICAL_SECTION;
	{
		m_pValueSource = pValueSource;
		if (m_pValueSource) {
			SetSourceValue(m_pValueSource->GetValue());
		}
	}
	LEAVE_CRITICAL_SECTION;
}

void CTriacChannel::SetValue(float dValue)
{
	IMPLEMENTS_INTERFACE_METHOD(IValueReceiver::SetValue(dValue));
	SetSourceValue(dValue);
}

float CTriacChannel::GetValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(IValueSource::GetValue());

	return m_fActualValue;
}

void CTriacChannel::SetBoostParams(unsigned nMaxBoostSamples, unsigned nMinInactivitySamples, unsigned nInactivitySamplesFor1BoostSample)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacChannel<float>::SetBoostParams(nMaxBoostSamples, nMinInactivitySamples, nInactivitySamplesFor1BoostSample));
	m_nMaxBoostSamples = nMaxBoostSamples;
	m_nMinInactivitySamples = nMinInactivitySamples;
	m_nInactivitySamplesFor1BoostSample = nInactivitySamplesFor1BoostSample;
	m_nInactivitySamples = m_nMinInactivitySamples + m_nMaxBoostSamples * m_nInactivitySamplesFor1BoostSample;
}
void CTriacChannel::SetInterleaveParams(unsigned nInterleaveParts, float fInterleaveThreshold)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacChannel<float>::SetInterleaveParams(nInterleaveParts, fInterleaveThreshold));
	m_nInterleaveTotalParts = nInterleaveParts;
	m_fInterleaveTreshold = fInterleaveThreshold;
}
void CTriacChannel::SetCalibrationTransformer(ITransformer<float>* pCalibrationTransformer)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacChannel<float>::SetCalibrationTransformer(pCalibrationTransformer));

	m_pCalibrationTransformer = pCalibrationTransformer;
}

void CTriacChannel::RequestSourceValue()
{
	if (m_pValueSource) {
		float dValue = m_pValueSource->GetValue();
		SetSourceValue(dValue);
	}
}

void CTriacChannel::ReloadTimer(
		bool bForceRecalc,
		bool bAllowMiddle,
		unsigned uiZeroGuardOffset,
		unsigned uiChannelPhase
) {
	float dValue = GetCalibratedValue();
	bool bBoost = false;
	if (dValue == 0.0) {
		if (m_nInactivitySamples < m_nMinInactivitySamples + m_nMaxBoostSamples * m_nInactivitySamplesFor1BoostSample) {
			++m_nInactivitySamples;
		}
		m_pTimerChannel->SetValue(m_pTimerChannel->GetTimerCounterMax());
	} else {
		if (!m_nInactivitySamples) {
			bBoost = false;
		} else if (m_nInactivitySamples < m_nMinInactivitySamples) {
			// boost not required or already done
			m_nInactivitySamples = 0;
			bBoost = false;
		} else {
			m_nInactivitySamples -= MIN2(m_nInactivitySamples, m_nInactivitySamplesFor1BoostSample);
			bBoost = true;
		}
		if (bBoost) {
			m_pTimerChannel->SetValue(0);
		} else {
			bool bChanged;
			if (m_bRecalcRequired || bForceRecalc) {
				m_bRecalcRequired = false;
				bChanged = RecalcTimerDelay(dValue, bAllowMiddle, uiZeroGuardOffset);
			} else {
				bChanged = false;
			}
			unsigned nInterleaveActivePart = GetInterleaveActivePart();
			if (bChanged || bForceRecalc || nInterleaveActivePart) {
				unsigned uiDelay = m_pTimerChannel->GetValue();
				bool bIsActivePhase = !nInterleaveActivePart || M_OutOf_N(nInterleaveActivePart, m_nInterleaveTotalParts, uiChannelPhase / 2);
				m_pTimerChannel->SetValue(bIsActivePhase ? uiDelay : m_pTimerChannel->GetTimerCounterMax());
			}
		}
	}
}

bool CTriacChannel::RecalcTimerDelay(float fValue, bool bAllowMiddle, unsigned uiZeroGuardOffset)
{
	unsigned uiInterleaveActiveParts = 0;

	unsigned uiDelay;
	if (fValue == 0.0) {
		m_fActualValue = 0.0;
		uiDelay = m_pTimerChannel->GetTimerCounterMax();
	} else if (fValue >= 1.0) {
		m_fActualValue = 1.0;
		uiDelay = 0;
	} else if (bAllowMiddle) {
		if (m_nInterleaveTotalParts && fValue < m_fInterleaveTreshold)  {
			fValue = WaveInterleave(fValue, m_nInterleaveTotalParts, &uiInterleaveActiveParts);
		}
		unsigned uiTriacsPeriod = m_pTimerChannel->GetTimerPeriod();
		uiDelay =  (unsigned) (fValue * uiTriacsPeriod);
		if (uiDelay > uiZeroGuardOffset) {
			uiDelay -= uiZeroGuardOffset;
			m_fActualValue = (float)uiDelay / uiTriacsPeriod / m_nInterleaveTotalParts * uiInterleaveActiveParts;
			uiDelay = uiTriacsPeriod - uiDelay;
		} else {
			m_fActualValue = 0.0;
			uiDelay = m_pTimerChannel->GetTimerCounterMax();
		}
	} else {
		m_fActualValue = 0.0;
		uiDelay = m_pTimerChannel->GetTimerCounterMax();
	}

	bool bChanged = (m_pTimerChannel->GetValue() != uiDelay);
	if (bChanged) {
		m_pTimerChannel->SetValue(uiDelay);
	}
	bChanged = SetInterleaveActivePart(uiInterleaveActiveParts) || bChanged;
	return bChanged;
}

void CTriacChannel::Off()
{
	ZERO_DEBUG_INFO("OFF");
	m_pTimerChannel->SetValue(m_pTimerChannel->GetTimerCounterMax());
	SetInterleaveActivePart(0);
}

bool CTriacChannel::SetSourceValue(float dValue)
{
	if (m_fSourceValue == dValue) {
		return false;
	}
	m_fSourceValue = dValue;
	return SetCalibratedValue((dValue > 0.0 && m_pCalibrationTransformer) ? m_pCalibrationTransformer->Transform(dValue) : dValue);
}

bool CTriacChannel::SetCalibratedValue(float dValue)
{
	if (m_fCalibratedValue == dValue) {
		return false;
	}
	m_fCalibratedValue = dValue;
	m_bRecalcRequired = true;
	ZERO_DEBUG_INFO("Triac: ch 0x:", (int)this);
	ZERO_DEBUG_INFO("       val %:", (int)(dValue * 100));
	return true;
}

bool CTriacChannel::SetInterleaveActivePart(unsigned uiValue)
{
	if (m_uiInterleaveActivePart == uiValue) {
		return false;
	}
	m_uiInterleaveActivePart = uiValue;
	ZERO_DEBUG_INFO("Interleave:", uiValue);
	return true;
}

class CTriacZeroSyncPWM_Base:
		public ITriacZeroSyncPWM<float>,
		public ITimerNotifier,
		public IDigitalInNotifier
{
public: // ITriacZeroSyncPWM
	virtual void Init(IDigitalInProvider* pZeroSource, ILed* pZeroLed);
	virtual void Deinit();

	virtual void RegisterSyncStateChangedCallback(NotifyCallback cb);
	virtual void RegisterZeroCallback(NotifyCallback cb);

	virtual ITriacChannel<float>* GetTriacChannelInterface(unsigned uChannelIdx);

public: // IExtiNotifier
	virtual void OnRise(); //zero sensor;
	virtual void OnFall(); //zero sensor;

protected:
	void CollectChannelValues();
	void ReloadChannelTimers();
	void OffChannels();
protected:
	unsigned GetTriacPeriod();
	bool ValidatePeriodClks(unsigned ulPeriodClks) const;
	void SetTriacPeriodClks(unsigned ulPeriodClks);
	void SetTriacTimerCounterClks(unsigned uiCounterClks);

	bool      m_bPeriodChanged;
	unsigned  m_nPowerWaveCounter;
	unsigned  m_uiZeroGuardOffsetClks;
	unsigned  m_uiPeriodMinClks;
	unsigned  m_uiPeriodMaxClks;
	unsigned  m_uiValidPeriodMinClks;
	unsigned  m_uiValidPeriodMaxClks;

	NotifyCallback    m_cbZeroSyncChangedCallback;
	NotifyCallback    m_cbZeroCallback;
	ILed*             m_pZeroLed;

	ITimer*       m_apTimers[TRIAC_TIMERS_COUNT];
	unsigned      m_nTimersCount;
	CTriacChannel m_aChannels[TRIAC_CHANNEL_COUNT];
	unsigned      m_nChannelsCount;

	IDigitalInProvider*  m_pZeroSource;
};

void CTriacZeroSyncPWM_Base::Init(IDigitalInProvider* pZeroSource, ILed* pZeroLed)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::Init(pZeroSource, pZeroLed));

	m_uiPeriodMinClks = CHW_MCU::GetCLKFrequency() / TRIAC_AC_FREQ_MAX / 2;
	m_uiPeriodMaxClks = CHW_MCU::GetCLKFrequency() / TRIAC_AC_FREQ_MIN / 2;
	m_uiValidPeriodMinClks = m_uiPeriodMinClks;
	m_uiValidPeriodMaxClks = m_uiPeriodMaxClks;
	m_uiZeroGuardOffsetClks = TRIAC_ZERO_GUARD_OFFSET_PULSING(m_uiPeriodMaxClks);

	unsigned n = 0;
	for (n = 0; n < ARRAY_SIZE(m_apTimers); ++n) {
		ITimer* pTimer = CHW_Brd::AcquireTriacTimerInterface(n);
		if (!pTimer) break;
		pTimer->SetPeriodClks(m_uiPeriodMaxClks );
		m_apTimers[n] = pTimer;
	}
	m_nTimersCount = n;

	for (n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		ITimerChannel* pTimerChannel = CHW_Brd::AcquireTriacTimerChannelInterface(n);
		if (!pTimerChannel) break;
		m_aChannels[n].Init(pTimerChannel);
	}
	m_nChannelsCount = n;

	// priority of timer wrap (1,0) must by higher then "zero cross" raise/fall EXTI (2,0)
	m_apTimers[0]->SetNotifier(this, 1, 0);

	for (n = 0; n < m_nTimersCount; ++n) {
		ITimer* pTimer = m_apTimers[n];
		pTimer->Start();
	}

	m_bPeriodChanged = true;
	m_nPowerWaveCounter = 0;
	m_uiZeroGuardOffsetClks = 0;

	m_cbZeroSyncChangedCallback = NotifyCallback();
	m_cbZeroCallback = NotifyCallback();
	m_pZeroLed = pZeroLed;
	if (m_pZeroLed) m_pZeroLed->Clear();

	m_pZeroSource = pZeroSource;
	m_pZeroSource->SetNotifier(this, 2, 0, false, true);    INDIRECT_CALL(OnFall());
};

void CTriacZeroSyncPWM_Base::Deinit()
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::Deinit());

	CHW_Brd::ReleaseTriacZeroSource();

	for (unsigned n = 0; n < m_nChannelsCount; ++n) {
		CHW_Brd::ReleaseTriacTimerChannelInterface(n);
	}
	m_nChannelsCount = 0;
	for (unsigned n = 0; n < m_nTimersCount; ++n) {
		CHW_Brd::ReleaseTriacTimerInterface(n);
	}
	m_nTimersCount = 0;
}

void CTriacZeroSyncPWM_Base::RegisterSyncStateChangedCallback(NotifyCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::RegisterSyncStateChangedCallback(cb));
	ENTER_CRITICAL_SECTION;
	{
		m_cbZeroSyncChangedCallback = cb;
	}
	LEAVE_CRITICAL_SECTION;
}

void CTriacZeroSyncPWM_Base::RegisterZeroCallback(NotifyCallback cb)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::RegisterZeroCallback(cb));
	ENTER_CRITICAL_SECTION;
	{
		m_cbZeroCallback = cb;
	}
	LEAVE_CRITICAL_SECTION;
}

ITriacChannel<float>* CTriacZeroSyncPWM_Base::GetTriacChannelInterface(unsigned uChannelIdx)
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::GetTriacChannelInterface(uChannelIdx));

	return (uChannelIdx < ARRAY_SIZE(m_aChannels)) ? &m_aChannels[uChannelIdx] : NULL;
}

void CTriacZeroSyncPWM_Base::CollectChannelValues()
{
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		m_aChannels[n].RequestSourceValue();
	}
}

void CTriacZeroSyncPWM_Base::ReloadChannelTimers()
{
	++m_nPowerWaveCounter;

	bool bAllowMiddle = (GetZeroQuality() >= 2);
#ifdef DEBUG
	bAllowMiddle = true;
#endif
	unsigned uiChannelPhase = m_nPowerWaveCounter;
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		m_aChannels[n].ReloadTimer(m_bPeriodChanged, bAllowMiddle, m_uiZeroGuardOffsetClks, uiChannelPhase);
		uiChannelPhase += 1 + TRIAC_CHANNEL_COUNT / 2 + GET_PARAM_VALUE(TRIAC_INTERLEAVE) / 2;
	}
	m_bPeriodChanged = false;
}


void CTriacZeroSyncPWM_Base::OffChannels()
{
	for (unsigned i = 0; i < ARRAY_SIZE(m_aChannels); ++i) {
		m_aChannels[i].Off();
	}
	m_bPeriodChanged = false;
}

bool CTriacZeroSyncPWM_Base::ValidatePeriodClks(unsigned uiPeriodClks) const
{
	return (uiPeriodClks >= m_uiValidPeriodMinClks && uiPeriodClks <= m_uiValidPeriodMaxClks);
}
void CTriacZeroSyncPWM_Base::SetTriacPeriodClks(unsigned uiPeriodClks)
{
	ZERO_DEBUG_INFO("Set period clks:", uiPeriodClks);
	for (unsigned n = 0; n < m_nTimersCount; ++n) {
		ITimer* pTimer = m_apTimers[n];
		pTimer->SetPeriodClks(uiPeriodClks);
	}
}

void CTriacZeroSyncPWM_Base::SetTriacTimerCounterClks(unsigned uiCounterClks)
{
	ZERO_DEBUG_INFO("Set counts clks:", uiCounterClks);
	for (unsigned n = 0; n < m_nTimersCount; ++n) {
		ITimer* pTimer = m_apTimers[n];
		pTimer->SetCounter(pTimer->ClksToCounts(uiCounterClks));
	}
}

unsigned CTriacZeroSyncPWM_Base::GetTriacPeriod()
{
	return m_apTimers[0]->GetPeriod();
}

void CTriacZeroSyncPWM_Base::OnRise()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInNotifier::OnRise());

	// do nothing. may be implemented in child classes
}

void CTriacZeroSyncPWM_Base::OnFall()
{
	IMPLEMENTS_INTERFACE_METHOD(IDigitalInNotifier::OnFall());

	// do nothing. may be implemented in child classes
}

#ifdef TRIAC_SYNC_TYPE_SMART
//////////////////////////////////////////////////////////////////////////////
//
//   Smart
//
//////////////////////////////////////////////////////////////////////////////

class CTriacZeroSyncPWM_Tune: public CTriacZeroSyncPWM_Base
{
public: // ITriacZeroSyncPWM
	virtual void Init(IDigitalInProvider* pZeroSource, ILed* pZeroLed);

	virtual unsigned GetZeroQuality() const;

public: // ITimerNotifier
	virtual void NotifyTimer();

public: // IExtiNotifier
	virtual void OnFall(); //zero sensor;

private:
	typedef enum {
		ZEROSYNC_NONE,
		ZEROSYNC_PULSING,
		ZEROSYNC_FREQ,
		ZEROSYNC_STABLE,
	} ZeroSyncType;

private:
	void SetZeroSyncState(ZeroSyncType eZeroSyncType);

	unsigned GetTriacsTimerIncWrapped() const;
	void ResetZeroStats();
	bool TuneFreq(unsigned uiNewTimerPeriod);
	bool TunePhase(int iPhaseError);
	bool AdjustFreq(int iPhaseErrorAvg);

private:

	ZeroSyncType m_eZeroSyncState;
	unsigned m_uiZeroFallTimerHalf;
	unsigned  m_uiZeroFallTolerantShiftedMin;
	unsigned  m_uiZeroFallExpectedShiftedTimer;
	unsigned  m_uiZeroFallTolerantShiftedMax;
	volatile
	unsigned  m_uiTimerWraps;
	unsigned  m_uiZeroFallPrevTimerWrapped;
	int       m_iErrorsSum;
	unsigned  m_uiErrorsCount;
	unsigned  m_uiTimerPeriodBase;
	int       m_iPhaseErrorSum;
	unsigned  m_uiPhaseErrorsCount;
	CTimeout m_toZeroPulsingTimeout;
	CTimeout m_toZeroSyncTimeout;
};

void CTriacZeroSyncPWM_Tune::Init(IDigitalInProvider* pZeroSource, ILed* pZeroLed)
{
	CTriacZeroSyncPWM_Base::Init(pZeroSource, pZeroLed);

	m_eZeroSyncState = ZEROSYNC_NONE;
	ResetZeroStats();
	m_iPhaseErrorSum = 0;
	m_uiPhaseErrorsCount = 0;
	m_toZeroPulsingTimeout.Init(MILLI_TO_TICKS(PULSING_FLT_DELAY_MS));
	m_toZeroPulsingTimeout.SetElapsed();
	m_toZeroSyncTimeout.Init(MILLI_TO_TICKS(SYNC_FLT_DELAY_MS));
	m_toZeroSyncTimeout.SetElapsed();
}

unsigned CTriacZeroSyncPWM_Tune::GetZeroQuality() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::GetZeroQuality());

	return (unsigned)m_eZeroSyncState;
}

void CTriacZeroSyncPWM_Tune::SetZeroSyncState(ZeroSyncType eZeroSyncType)
{
	ASSERTE(m_eZeroSyncState != eZeroSyncType);

	DEBUG_INFO("ZERO sync changed:", eZeroSyncType);

	m_eZeroSyncState = eZeroSyncType;
	m_bPeriodChanged = true;
	ResetZeroStats();
	if (m_pZeroLed) {
		switch (eZeroSyncType) {
			case ZEROSYNC_NONE:
				m_pZeroLed->Clear();
				break;
			case ZEROSYNC_PULSING:
				m_pZeroLed->Blink();
				break;
			case ZEROSYNC_FREQ:
				m_pZeroLed->FastBlink();
				break;
			case ZEROSYNC_STABLE:
				m_pZeroLed->Set();
				break;
		}
	}
	if (m_cbZeroSyncChangedCallback) m_cbZeroSyncChangedCallback();
}

void CTriacZeroSyncPWM_Tune::NotifyTimer()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());

	switch(m_eZeroSyncState) {
		case ZEROSYNC_NONE: {

		} break;
		case ZEROSYNC_PULSING: {
			++m_uiTimerWraps;
			ASSERTE(m_uiTimerWraps < 100);
			if (m_toZeroPulsingTimeout.GetIsElapsed()) {
				// zero pulsing lost
				SetZeroSyncState(ZEROSYNC_NONE);
			}
		} break;
		case ZEROSYNC_FREQ:
		case ZEROSYNC_STABLE: {
			if (m_toZeroSyncTimeout.GetIsElapsed()) {
				// zero sync lost
				SetZeroSyncState(ZEROSYNC_NONE);
				OffChannels();
			}
		} break;
	}

	CollectChannelValues();
	ReloadChannelTimers();

	if (m_cbZeroCallback) {m_cbZeroCallback();}
//	ZERO_DEBUG_INFO("WRAP:", m_uiTimerWraps);
//	ZERO_DEBUG_INFO("Timer:", CHW::GetTriacsTimer());
}

void CTriacZeroSyncPWM_Tune::OnFall()
{
	OVERRIDE_METHOD(CTriacZeroSyncPWM_Base::OnFall());

	// todo GET_PARAM_VALUE(TRIAC_ZERO_SENSOR_MODE)
	// sensor mode: 0 - FallEdge, 1 - RaiseEdge, 2 - HiCenter, 3 - LoCenter

	m_toZeroPulsingTimeout.Start();
	switch (m_eZeroSyncState) {

		case ZEROSYNC_NONE: {
			ASSERTE(!m_uiErrorsCount);
			SetZeroSyncState(ZEROSYNC_PULSING);
			m_uiZeroFallPrevTimerWrapped = 0;
		}; break;

		case ZEROSYNC_PULSING: {
			// collect period error. tune freq if cumulative error > 1/4*period. done if no freq tuning for 8 periods
			unsigned uiZeroFallTimerWrapped = GetTriacsTimerIncWrapped();
			if (m_uiZeroFallPrevTimerWrapped) {
				ASSERTE(uiZeroFallTimerWrapped > m_uiZeroFallPrevTimerWrapped);
				unsigned uiFallPeriod = uiZeroFallTimerWrapped - m_uiZeroFallPrevTimerWrapped;
				if (uiFallPeriod < CHW_MCU::GetTriacTimerPeriodMin()) {
					ZERO_DEBUG_INFO("Early Zero Fall (pulse) ignored. Period:", uiFallPeriod);
				} else if (uiFallPeriod < CHW_MCU::GetTriacTimerPeriodMax()) {
					m_uiZeroFallPrevTimerWrapped = uiZeroFallTimerWrapped;
					unsigned uiTimerPeriod = CHW_MCU::GetTriacTimerPeriod();
					int iPeriodError = uiTimerPeriod - uiFallPeriod;
					ZERO_DEBUG_INFO("Zero fall (pulsing). Period:", uiFallPeriod);
					m_iErrorsSum += iPeriodError;
					++m_uiErrorsCount;
					int iFreqThreshold = FREQ_THRESHOLD(uiTimerPeriod);
					bool bIsFreq = (m_iErrorsSum >= -iFreqThreshold && m_iErrorsSum <= iFreqThreshold);
					if (bIsFreq && m_uiErrorsCount < 8) {
						// do nothing
					} else {
						int iZeroPeriodDeviationAvg = m_iErrorsSum / (int)m_uiErrorsCount;
						TuneFreq(uiTimerPeriod - iZeroPeriodDeviationAvg);
						ZERO_DEBUG_INFO("Freq tuned, delta:", iZeroPeriodDeviationAvg);
						if (bIsFreq) {
							SetZeroSyncState(ZEROSYNC_FREQ);
							m_toZeroSyncTimeout.Start();
						} else {
							SetZeroSyncState(ZEROSYNC_NONE);
							m_toZeroSyncTimeout.Start();
						}
						m_uiZeroFallPrevTimerWrapped = 0;
					}
				} else {
					ZERO_DEBUG_INFO("Late Zero Fall (pulse). Period:", uiFallPeriod);
					SetZeroSyncState(ZEROSYNC_NONE);
				}
			} else {
				m_uiZeroFallPrevTimerWrapped = uiZeroFallTimerWrapped;
			}
		}; break;

		case ZEROSYNC_FREQ:
		case ZEROSYNC_STABLE: {
			// collect phase error. tune phase if cumulative error > 1/128*period. done if no freq tuning for 16 periods
			unsigned uiTimerPeriod = CHW_MCU::GetTriacTimerPeriod();
			unsigned uiZeroFallTimer = CHW_MCU::GetTriacsTimer();
			unsigned uiZeroFallTimerShifted = (uiZeroFallTimer < m_uiZeroFallTimerHalf)?uiZeroFallTimer + uiTimerPeriod:uiZeroFallTimer; // for comparison without +- care
			if (uiZeroFallTimerShifted < m_uiZeroFallTolerantShiftedMin) {
				ZERO_DEBUG_INFO("Early Zero Fall (sync) ignored. Timer:", (int)(uiZeroFallTimerShifted - uiTimerPeriod));
			} else if (uiZeroFallTimerShifted <= m_uiZeroFallTolerantShiftedMax) {
				int iFallPhaseError = uiZeroFallTimerShifted - m_uiZeroFallExpectedShiftedTimer;
//				m_uiZeroFallJitterSum += iFallPhaseError*iFallPhaseError;
				ZERO_DEBUG_INFO("Zero fall phase error:", iFallPhaseError);
				m_iErrorsSum += iFallPhaseError;
				++m_uiErrorsCount;
				int iStableThreshold = STABLE_THRESHOLD(uiTimerPeriod);
				bool bIsStable = (iFallPhaseError >= -iStableThreshold && iFallPhaseError <= iStableThreshold);
				if (m_uiErrorsCount < PHASE_AVERAGING_N && bIsStable) {
					// do nothing
				} else {
					// tune phase
					int iPhaseErrorAvg = m_iErrorsSum / (int)m_uiErrorsCount;
					if (TunePhase(iPhaseErrorAvg)) {
						ZERO_DEBUG_INFO("Timer phase tuned, avg delta:", iPhaseErrorAvg);
						if (bIsStable) {
							if (m_eZeroSyncState != ZEROSYNC_STABLE) {
								SetZeroSyncState(ZEROSYNC_STABLE);
							}
						} else {
							if (m_eZeroSyncState != ZEROSYNC_FREQ) {
								SetZeroSyncState(ZEROSYNC_FREQ);
							}
						}
					} else {
						ZERO_DEBUG_INFO("Timer phase tune limit.");
						SetZeroSyncState(ZEROSYNC_NONE);
					}
				}
				m_toZeroSyncTimeout.Start();
			} else if (!m_toZeroSyncTimeout.GetIsElapsed()) {
				ZERO_DEBUG_INFO("Late Zero Fall (sync) ignored. Timer:", (int)(uiZeroFallTimerShifted - uiTimerPeriod));
				ResetZeroStats();
			} else {
				ZERO_DEBUG_INFO("Sync lost");
				SetZeroSyncState(ZEROSYNC_NONE);
			}
		}; break;
	}
}

unsigned CTriacZeroSyncPWM_Tune::GetTriacsTimerIncWrapped() const
{
	// m_uiTimerWraps is volatile, so lets double check
	unsigned uiPeriod = CHW_Clocks::GetCLKCounter24();
	unsigned uiWraps1 = m_uiTimerWraps;
	unsigned uiTimer1 = CHW_MCU::GetTriacsTimer();
	unsigned uiWraps2 = m_uiTimerWraps;
	if (uiWraps1 == uiWraps2) return uiWraps1 * uiPeriod + uiTimer1;
	unsigned uiTimer2 = CHW_MCU::GetTriacsTimer();
	ASSERTE(uiWraps2 == m_uiTimerWraps); // instead uiWraps3
	return uiWraps2 * uiPeriod + uiTimer2;
}

void CTriacZeroSyncPWM_Tune::ResetZeroStats()
{
//	ZERO_DEBUG_INFO("Zero RESET:", m_uiTimerWraps);
	m_uiTimerWraps = 0;
	m_uiErrorsCount = 0;
	m_iErrorsSum = 0;
	m_uiZeroFallPrevTimerWrapped = 0x0FFFFFFF; // to force assert
}

// period (freq) tuning
bool CTriacZeroSyncPWM_Tune::TuneFreq(unsigned uiTimerPeriodNano)
{
//	ZERO_DEBUG_INFO("Timer period new value:", uiTimerPeriod);
	bool bResult = SetTriacPeriodNano(uiTimerPeriodNano);
	m_bPeriodChanged = true;
	if (!bResult) {
		uiTimerPeriodNano = GetTriacPeriodNano();
	}
	m_uiZeroFallTimerHalf = uiTimerPeriodNano / 2;

	int iSensorOffset = TRIAC_ZERO_SENSOR_OFFSET(uiTimerPeriodNano);
	unsigned uiTolerance = 0;

	switch (m_eZeroSyncState) {
		case ZEROSYNC_PULSING:
			uiTolerance = TRIAC_ZERO_TOLERANCE_PULSING(uiTimerPeriodNano);
			m_uiZeroGuardOffsetClks = TRIAC_ZERO_GUARD_OFFSET_PULSING(uiTimerPeriodNano);
			break;
		case ZEROSYNC_FREQ:
			uiTolerance = TRIAC_ZERO_TOLERANCE_FREQ(uiTimerPeriodNano);
			m_uiZeroGuardOffsetClks = TRIAC_ZERO_GUARD_OFFSET_FREQ(uiTimerPeriodNano);
			break;
		case ZEROSYNC_STABLE:
			uiTolerance = TRIAC_ZERO_TOLERANCE_STABLE(uiTimerPeriodNano);
			m_uiZeroGuardOffsetClks = TRIAC_ZERO_GUARD_OFFSET_STABLE(uiTimerPeriodNano);
			break;
		default:
			ASSERT("Invalid");
			break;
	}
	ASSERTE(iSensorOffset > -(int)m_uiZeroFallTimerHalf);
	ASSERTE(iSensorOffset < (int)m_uiZeroFallTimerHalf);
	ASSERTE(m_uiZeroGuardOffsetClks < uiTimerPeriodNano);
	m_uiZeroFallExpectedShiftedTimer = (uiTimerPeriodNano - iSensorOffset + m_uiZeroGuardOffsetClks) % uiTimerPeriodNano;
	ASSERTE(uiTolerance < m_uiZeroFallExpectedShiftedTimer);
	m_uiZeroFallTolerantShiftedMin = m_uiZeroFallExpectedShiftedTimer - uiTolerance;
	m_uiZeroFallTolerantShiftedMax = m_uiZeroFallExpectedShiftedTimer + uiTolerance;
	ResetZeroStats();

	if (m_eZeroSyncState <= ZEROSYNC_PULSING) {
		unsigned uiPeriodNano = (m_uiZeroFallExpectedShiftedTimer < uiTimerPeriodNano) ? m_uiZeroFallExpectedShiftedTimer : m_uiZeroFallExpectedShiftedTimer - uiTimerPeriodNano;
		SetTriacPeriodNano(uiPeriodNano);
	}

	m_uiTimerPeriodBase = uiTimerPeriodNano;
	m_iPhaseErrorSum = 0;
	m_uiPhaseErrorsCount = 0;

	return bResult;
}

//phase tuning by freq fine tuning
bool CTriacZeroSyncPWM_Tune::TunePhase(int iPhaseError)
{
	int iTimerPeriodTuning = PHASE_TUNING_GAIN(iPhaseError);
	m_iPhaseErrorSum += iPhaseError;
	++m_uiPhaseErrorsCount;
	bool bResult;

	unsigned uiNewTimerPeriod = m_uiTimerPeriodBase + iTimerPeriodTuning;
	unsigned uiPeriodNano = uiNewTimerPeriod;
	SetTriacPeriodNano(uiPeriodNano);
	ResetZeroStats();

	if (m_uiPhaseErrorsCount < FREQ_ADJUSTMENT_AVERAGING_N || !bResult) {
		// do nothing
	} else {
		int iPhaseErrorAvg = m_iPhaseErrorSum / FREQ_ADJUSTMENT_AVERAGING_N;
		m_iPhaseErrorSum = 0;
		m_uiPhaseErrorsCount = 0;
		bResult = AdjustFreq(iPhaseErrorAvg);
	}
	return bResult;
}

//phase tuning by freq fine tuning
bool CTriacZeroSyncPWM_Tune::AdjustFreq(int iPhaseErrorAvg)
{
	int iFreqAdjustment = FREQ_ADJUSTMENT_GAIN(iPhaseErrorAvg);
	ZERO_DEBUG_INFO("Freq adjustments:", iFreqAdjustment);
	bool bResult = TuneFreq(m_uiTimerPeriodBase + iFreqAdjustment);

	return bResult;
}
#endif // TRIAC_SYNC_TYPE_SMART

#ifdef TRIAC_SYNC_TYPE_RAW
//////////////////////////////////////////////////////////////////////////////
//
//   Raw
//
//////////////////////////////////////////////////////////////////////////////

class CTriacZeroSyncPWM_Raw: public CTriacZeroSyncPWM_Base
{
public: // ITriacZeroSyncPWM
	virtual void Init(IDigitalInProvider* pZeroSource, ILed* pZeroLed);

	virtual unsigned GetZeroQuality() const;

protected: // CTriacZeroSyncPWM_Base
	virtual void OnFall();

public: // CTriacZeroSyncPWM_Base
public: // ITimerNotifier
	virtual void NotifyTimer();

	void ProcessZeroTimeout();
	void ProcessZeroPeriod(unsigned uiZeroPeriodClks);
	void ResetZeroPeriod();
	void ChangeZeroPeriod(unsigned uiZeroPeriodClks);
	void AcquiredZero();
	void AcquiredPulsing();
	void LostPulsing();
	void LostZero();

	uint32_t m_uiZeroLastPulseClks;
	uint32_t m_uiZeroValidPulseClks;
	uint32_t m_uiZeroPeriodClks;
	uint16_t m_uiZeroPulsing;
	uint16_t m_uiZeroIdleSteps;
//	ITimer*  m_pHelperTimer;
//	class CTimerNotifierProxyHelper: public ITimerNotifier {
//		CTriacZeroSyncPWM_Raw* m_pParent;
//	public:
//		void Init(CTriacZeroSyncPWM_Raw* pParent) {m_pParent = pParent;}
//	public: // ITimerNotifier
//		virtual void NotifyTimer() {
//			IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());
//			return m_pParent->LostZero();
//		}
//	};
//	CTimerNotifierProxyHelper m_HelperNotifierProxy;
};

void CTriacZeroSyncPWM_Raw::Init(IDigitalInProvider* pZeroSource, ILed* pZeroLed)
{
	CTriacZeroSyncPWM_Base::Init(pZeroSource, pZeroLed);
	m_uiZeroLastPulseClks = 0;
	m_uiZeroValidPulseClks = 0;
	m_uiZeroPeriodClks = 0;
	m_uiZeroPulsing = 0;
	m_uiZeroIdleSteps = 0;
//	m_HelperNotifierProxy.Init(this);
//	m_pHelperTimer = CHW_Brd::GetHelperTimerInterface();
//	m_pHelperTimer->Init();
//	m_pHelperTimer->SetPeriodNano(1000000000/TRIAC_AC_FREQ_MIN/2);
//	m_pHelperTimer->SetNotifier(&m_HelperNotifierProxy);
}

unsigned CTriacZeroSyncPWM_Raw::GetZeroQuality() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITriacZeroSyncPWM::GetZeroQuality());

	return m_uiZeroPulsing;
}

void CTriacZeroSyncPWM_Raw::NotifyTimer()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerNotifier::NotifyTimer());

	ProcessZeroTimeout();

	CollectChannelValues();
	ReloadChannelTimers();

	if (m_cbZeroCallback) {m_cbZeroCallback();}
}

void CTriacZeroSyncPWM_Raw::ProcessZeroTimeout()
{
	if (m_uiZeroIdleSteps > 10) {
		// do notning
	} else if (m_uiZeroIdleSteps == 10) {
		LostZero();
		OffChannels();
		++m_uiZeroIdleSteps;
	} else if (m_uiZeroIdleSteps == 3) {
		LostPulsing();
		ResetZeroPeriod();
		OffChannels();
		++m_uiZeroIdleSteps;
	} else {
		++m_uiZeroIdleSteps;
	}
	ZERO_DEBUG_INFO("Timeout:", m_uiZeroIdleSteps);
}

void CTriacZeroSyncPWM_Raw::OnFall()
{
	OVERRIDE_METHOD(CTriacZeroSyncPWM_Base::OnFall());

	m_uiZeroLastPulseClks = CHW_MCU::GetCLKCounter32();
	ZERO_DEBUG_INFO("now clks:", m_uiZeroLastPulseClks);
	if (m_uiZeroPulsing) {
		unsigned uiZeroPeriodClks = m_uiZeroLastPulseClks - m_uiZeroValidPulseClks;
		if (ValidatePeriodClks(uiZeroPeriodClks)) {
			ProcessZeroPeriod(uiZeroPeriodClks);
		} else if (m_uiZeroIdleSteps > 1 && ValidatePeriodClks(uiZeroPeriodClks / m_uiZeroIdleSteps)) {
			ProcessZeroPeriod(uiZeroPeriodClks / m_uiZeroIdleSteps);
		} else if (m_uiZeroIdleSteps > 1 && ValidatePeriodClks(uiZeroPeriodClks / (m_uiZeroIdleSteps + 1))) {
			ProcessZeroPeriod(uiZeroPeriodClks / (m_uiZeroIdleSteps + 1));
		} else {
			ZERO_DEBUG_INFO("last clks:", m_uiZeroValidPulseClks);
			ZERO_DEBUG_INFO("Invalid period clks:", uiZeroPeriodClks);
		}
	} else {
		AcquiredZero();
	}
}

void CTriacZeroSyncPWM_Raw::ProcessZeroPeriod(unsigned uiZeroPeriodClks)
{
	unsigned uiToleranceClk = m_uiZeroPeriodClks / 32;
	if (uiZeroPeriodClks >= m_uiZeroPeriodClks - uiToleranceClk && uiZeroPeriodClks <= m_uiZeroPeriodClks + uiToleranceClk) {
		ZERO_DEBUG_INFO("Period tolerant clks:", uiZeroPeriodClks);
		ZERO_DEBUG_INFO("Period clks:", m_uiZeroPeriodClks);
	} else {
		ChangeZeroPeriod(uiZeroPeriodClks);
		ZERO_DEBUG_INFO("Period changed clks:", uiZeroPeriodClks);
	}

	AcquiredPulsing();
	unsigned uiNewTimerValueClks = (m_uiZeroPeriodClks - TRIAC_ZERO_SENSOR_OFFSET(m_uiZeroPeriodClks) + m_uiZeroGuardOffsetClks) % m_uiZeroPeriodClks;
	SetTriacTimerCounterClks(uiNewTimerValueClks);
//	ZERO_DEBUG_INFO("Offset:", uiNewTimerValueClks);
}

void CTriacZeroSyncPWM_Raw::ResetZeroPeriod()
{
	SetTriacPeriodClks(m_uiPeriodMaxClks);
	m_uiZeroPeriodClks = m_uiPeriodMaxClks;
	m_bPeriodChanged = true;
	m_uiValidPeriodMinClks = m_uiPeriodMinClks;
	m_uiValidPeriodMaxClks = m_uiPeriodMaxClks;
	m_uiZeroGuardOffsetClks = TRIAC_ZERO_GUARD_OFFSET_PULSING(m_uiPeriodMaxClks);
}

void CTriacZeroSyncPWM_Raw::ChangeZeroPeriod(unsigned uiZeroPeriodClks)
{
	unsigned uiZeroPeriodWindowClks = uiZeroPeriodClks / 16;
	SetTriacPeriodClks(uiZeroPeriodClks);
	m_uiZeroPeriodClks = uiZeroPeriodClks;
	m_bPeriodChanged = true;
	m_uiValidPeriodMinClks = MAX2(m_uiPeriodMinClks, uiZeroPeriodClks - uiZeroPeriodWindowClks) ;
	m_uiValidPeriodMaxClks = MIN2(m_uiPeriodMaxClks, uiZeroPeriodClks + uiZeroPeriodWindowClks) ;
	m_uiZeroGuardOffsetClks = TRIAC_ZERO_GUARD_OFFSET_PULSING(uiZeroPeriodClks);
}

void CTriacZeroSyncPWM_Raw::AcquiredZero()
{
	m_uiZeroIdleSteps = 0;
	m_uiZeroValidPulseClks = m_uiZeroLastPulseClks;
	if (m_uiZeroPulsing < 1) {
		ZERO_DEBUG_INFO("Zero");
		m_uiZeroPulsing = 1;
		if (m_pZeroLed) m_pZeroLed->Pulse(1);
		if (m_cbZeroSyncChangedCallback) m_cbZeroSyncChangedCallback();
	}
}

void CTriacZeroSyncPWM_Raw::AcquiredPulsing()
{
	m_uiZeroIdleSteps = 0;
	m_uiZeroValidPulseClks = m_uiZeroLastPulseClks;
	if (m_uiZeroPulsing < 3) {
		ZERO_DEBUG_INFO("Pulsing.");
		m_uiZeroPulsing = 3;
		if (m_pZeroLed) m_pZeroLed->Set();
		if (m_cbZeroSyncChangedCallback) m_cbZeroSyncChangedCallback();
	}
}

void CTriacZeroSyncPWM_Raw::LostPulsing()
{
	if (m_uiZeroPulsing > 1) {
		ZERO_DEBUG_INFO("Pulsing lost - invalid period");
		m_uiZeroValidPulseClks = m_uiZeroLastPulseClks;
		m_uiZeroPeriodClks = 0;
		m_uiZeroPulsing = 1;
		if (m_pZeroLed) m_pZeroLed->FastBlink();
		if (m_cbZeroSyncChangedCallback) m_cbZeroSyncChangedCallback();
	}
}

void CTriacZeroSyncPWM_Raw::LostZero()
{
	if (m_uiZeroPulsing) {
		ZERO_DEBUG_INFO("Zero lost - timeout");
		m_uiZeroLastPulseClks = 0;
		m_uiZeroValidPulseClks = 0;
		m_uiZeroPeriodClks = 0;
		m_uiZeroPulsing = 0;
		if (m_pZeroLed) m_pZeroLed->Clear();
		if (m_cbZeroSyncChangedCallback) m_cbZeroSyncChangedCallback();
	}
}
#endif // TRIAC_SYNC_TYPE_RAW

//////////////////////////////////////////////////////////////////////////////
//
//   Instance
//
//////////////////////////////////////////////////////////////////////////////

//CTriacZeroSyncPWM_Tune g_TriacPWM;

CTriacZeroSyncPWM_Raw g_TriacPWM;
ITriacZeroSyncPWM<float>* GetTriacZeroSyncPWMInterface()
{
	return &g_TriacPWM;
}
