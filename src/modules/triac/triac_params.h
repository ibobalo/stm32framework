#ifndef TRIAC_PARAMS_INCLUDED
#define TRIAC_PARAMS_INCLUDED


#define TRIAC_PARAMS_LIST \
	PARAM(TRIAC_INTERLEAVE,                   int,       0,  "0, 2, 4, 8, 16, 32"   ) \
	PARAM(TRIAC_INTERLEAVE_TRESHOLD,          float,     0.05/*0.3*/) \
	PARAM(TRIAC_ZERO_SENSOR_MODE,             int,       0,   "0 - FallEdge, 1 - RaiseEdge, 2 - HiCenter, 3 - LoCenter") \
	PARAM(TRIAC_ZERO_SENSOR_OFFSET_MUL,       int,       -80, "MUL * T/1024 + ABS") \
	PARAM(TRIAC_ZERO_SENSOR_OFFSET_ABS,       int,       0,   "MUL * T/1024 + ABS") \
	PARAM(TRIAC_ZERO_EXT_SENSOR_MODE,         int,       2,   "0 - FallEdge, 1 - RaiseEdge, 2 - HiCenter, 3 - LoCenter") \
	PARAM(TRIAC_ZERO_EXT_SENSOR_OFFSET_MUL,   int,       -260, "MUL * T/1024 + ABS")  /*50Hz-260 55Hz-380 */\
	PARAM(TRIAC_ZERO_EXT_SENSOR_OFFSET_ABS,   int,       0,   "MUL * T/1024 + ABS") \
	PARAM(TRIAC_ZERO_GUARD_OFFSET,            int,       0,   "* T/1024") \
	PARAM(TRIAC_INVERTED_OUTPUTS,             bool,      false) \

#endif // TRIAC_PARAMS_INCLUDED
