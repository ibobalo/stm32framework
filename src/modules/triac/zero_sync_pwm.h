#ifndef ZERO_SYNC_PWM_H_INCLUDED
#define ZERO_SYNC_PWM_H_INCLUDED

#include "util/callback.h"
#include "interfaces/common.h"
#include "interfaces/gpio.h"

#define TRIAC_TIMERS_COUNT   2
#define TRIAC_CHANNEL_COUNT  8

#define TRIAC_AC_FREQ_MIN 45
#define TRIAC_AC_FREQ_MAX 65
//#define TRIAC_INVERTED_OUTPUTS true

//extern
class ILed;

template<class T = float>
class ITriacChannel :
		public IValueSource<T>,    // actual out power
		public IValueReceiver<T>,  // command value
		public IValueRequester<T>  // command value
{/*
public: // IValueRequester
	virtual void SetValueSourceInterface(const IValueSource<float>* pValueSource) = 0;
public: // IValueTarget
	virtual void SetValue(float tValue) = 0;
public: // IValueSource
	virtual float GetValue() const = 0;
*/
public: // ITriacChannel
	virtual void SetBoostParams(unsigned nMaxBoostSamples, unsigned nMinInactivitySamples, unsigned nInactivitySamplesFor1BoostSample) = 0;
	virtual void SetInterleaveParams(unsigned nInterleaveParts, float fInterleaveThreshold) = 0;
	virtual void SetCalibrationTransformer(ITransformer<T>* pCalibrationTransformer) = 0;
};

template<class T=float>
class ITriacZeroSyncPWM
{
public:
	typedef Callback<void (void)> NotifyCallback;
public: // ITriacZeroSyncPWM
	virtual void Init(IDigitalInProvider* pZeroSource, ILed* pLed) = 0;
	virtual void Deinit() = 0;
	virtual void RegisterSyncStateChangedCallback(NotifyCallback cb) = 0;
	virtual void RegisterZeroCallback(NotifyCallback cb) = 0;

	virtual unsigned GetZeroQuality() const = 0;

	virtual ITriacChannel<T>* GetTriacChannelInterface(unsigned uChannelIdx) = 0;


	// template aliases
//	template<int n> CTriacChannel* GetTriacChannelInterface() {return GetTriacChannelInterface(n);};
//	template<int n> void SetValueSourceInterface(const IValueSource<float>* pValueSource) {CTriacChannel* pChannel = GetTriacChannelInterface<n>(); if (pChannel) pChannel->SetValueSourceInterface(pValueSource);};
//	template<int n> void SetTriacValue(float dValue) {CTriacChannel* pChannel = GetTriacChannelInterface<n>(); if (pChannel) pChannel->SetValue(dValue);};
//	template<int n> float GetTriacValue() const {CTriacChannel* pChannel = GetTriacChannelInterface<n>(); if (pChannel) return pChannel->GetValue();};
};

ITriacZeroSyncPWM<float>* GetTriacZeroSyncPWMInterface();

#endif /* ZERO_SYNC_PWM_H_INCLUDED */
