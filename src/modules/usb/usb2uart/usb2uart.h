#ifndef USB2UART_H_INCLUDED
#define USB2UART_H_INCLUDED

#include "piped_uart.h"

void ActivateUsbToUartBridge(unsigned uiBitrate, CHW_USART::StopBits eStopBits, CHW_USART::ParityBits eParityBits);

#endif // USB2UART_H_INCLUDED
