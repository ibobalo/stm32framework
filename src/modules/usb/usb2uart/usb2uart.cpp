#include "stdafx.h"
#include "hw/hw.h"
#include "util/macros.h"
#include "piped_uart.h"
#include <stdio.h>
#include <stdint.h>

class CSerialToSerialBridge
{
public:
	class CBridgeRxNotifier : public ISerialRxNotifier
	{
	public:
		void Init(ISerial* pSerialTarget) { m_pSerialTarget = pSerialTarget; }
	public: // ISerialRxNotifier
		virtual void NotifyRxDone() {
		}
		virtual void NotifyRxOverflow(uint8_t uiData) {
			DEBUG_INFO("Uart Rx overflow :", m_uiUSARTIndex);
		}
	public: // ISerialRxNotifier
		virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {
			//returns true in next chunk set

		}
		virtual bool NotifyRxDone(CIoChunk* pRetNextChunk) {
			//returns true in next chunk set
			// rx from usart. send to usb
			m_pSerialTarget->StartDataSend(m_Buffer, m_uiDataLength);
			return false;
		}
		virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {

		}
		virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) {

		}
		virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) = 0 {

		}
		virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk) {

		}
	private:
		ISerial* m_pSerialTarget;
	};
public:
	void Init(unsigned uiVCPIndex, unsigned uiUSARTIndex, unsigned uiBaudRate, CHW_USART::StopBits eStopBits, CHW_USART::ParityBits eParityBits) {
		m_uiVCPIndex = uiVCPIndex;
		m_uiUSARTIndex = uiUSARTIndex;
		m_uiDataLength = 0;
		ISerial* pUart = CHW_Brd::GetInitedUSART(uiUSARTIndex);
		ISerial* pVcp = CHW_MCU::GetUSBVCP(0);
		m_tUartRxNotifier.Init(pVcp);
		m_tVCPRxNotifier.Init(pUart);
		pUart->SetSerialRxNotifier(&m_tUartRxNotifier);
		pUart->SetSerialTxNotifier(&m_tUartTxNotifier);
		pUart->SetConnectionParams(uiBaudRate, eStopBits, eParityBits);
		pUart->StartDataRecv(m_Buffer, sizeof(m_Buffer));
		pVcp->SetSerialRxNotifier(&m_tVCPRxNotifier);
		pVcp->SetSerialTxNotifier(&m_tVCPTxNotifier);
	}
private:
//	TPipedUARTNotifier<256, 256>  m_PipedUartCallbacks;
	CBridgeRxNotifier  m_tUartRxNotifier;
	CBridgeRxNotifier  m_tVCPRxNotifier;

	unsigned   m_uiVCPIndex;
	unsigned   m_uiUSARTIndex;
	unsigned   m_uiDataLength;
	uint8_t    m_Buffer[256];
};

class CUsbToUartBridge :
		public ISerialNotifier
{
public:
	void Init(unsigned uiUSARTIndex, unsigned uiBaudRate, CHW_USART::StopBits eStopBits, CHW_USART::ParityBits eParityBits) {
		m_UartCallbacks.Init();
		CHW_Brd::InitUSART(uiUSARTIndex, &m_UartCallbacks);
		ISerial* pUart = CHW_Brd::GetUSART(uiUSARTIndex);
		pUart->SetConnectionParams(uiBaudRate, eStopBits, eParityBits);
		Callback<void (const void*, unsigned)> cbRx = BIND_MEM_CB(&ISerial::StartDataSend, pUart);
		CHW_USB::BindRxDoneCallback(cbRx);
		Callback<void (uint8_t uiData)> cbTx = BIND_FREE_CB(&CHW_USB::UsbByteSent);
		m_UartCallbacks.BindRxOverflowCallback(cbTx);
		pUart->StartDataRecv(NULL, 0); // enable overflow
	}
public: // IUSARTNotifier
	virtual void NotifyRxDone() = 0;
	virtual void NotifyRxOverflow(uint8_t uiData) = 0;
	virtual void NotifyRxTimeout(unsigned uiBytesLeft) = 0;
	virtual void NotifyRxError(unsigned uiBytesLeft) = 0;
	virtual void NotifyTxDone() = 0;
	virtual void NotifyTxTimeout(unsigned uiBytesLeft) = 0;
	virtual void NotifyTxError(unsigned uiBytesLeft) = 0;
private:
	unsigned   m_uiVCPIndex;
	unsigned   m_uiDataLength;
	uint8_t    m_Buffer[256];
};
CUsbToUartBridge g_UsbToUartBridge;
CUartToUsbBridge g_UartToUsbBridge;


void ActivateUsbToUartBridge(unsigned uiBitrate, CHW_USART::StopBits eStopBits, CHW_USART::ParityBits eParityBits)
{
#ifdef USE_NET
	unsigned uiSlipUartIndex = GET_PARAM_VALUE(SLIP_UART);
	if (uiSlipUartIndex) {
		// TODO deactivate slip
	}
#endif
	unsigned uiUsb2UartIndex = GET_PARAM_VALUE(USB2UART_UART);
	if (uiUsb2UartIndex) {
//		g_UsbToUartBridge.Init(uiUsb2UartIndex, GET_PARAM_VALUE(USB2UART_BAUDRATE), (CHW_USART::StopBits)GET_PARAM_VALUE(USB2UART_STOPBITS), (CHW_USART::ParityBits)GET_PARAM_VALUE(USB2UART_PARITYBITS));
		g_UsbToUartBridge.Init(uiUsb2UartIndex, uiBitrate, eStopBits, eParityBits);
	}
}
