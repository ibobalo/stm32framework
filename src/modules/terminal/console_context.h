#ifndef CONSOLE_CONTEXT_H_INCLUDED
#define CONSOLE_CONTEXT_H_INCLUDED

#include "util/core.h"
#include "util/chained_chunks.h"
#include <string.h>

class IConsoleContext;
//extern
class IStreamReceiver;
class IConsoleCommand;
template<class T> struct Less;
template<class Tkey, class Tdata, class Compare> struct TBTreeItem;
typedef TBTreeItem<const CConstChunk&, IConsoleCommand*, Less<const CConstChunk&>> CConsoleCommandMapItem;


class IConsoleContext
{
public: // IConsoleContext
	virtual unsigned BroadcastStdout(const CConstChunk& ccData) const = 0;
	virtual unsigned GetParamsCount() const = 0;
	virtual const CConstChunk& GetParam(unsigned n) const = 0;
	virtual const CConsoleCommandMapItem* GetCommandsMapRoot() const = 0;
};

class CConsoleContext:
		public IConsoleContext
{
public: // IConsoleContext
	virtual unsigned BroadcastStdout(const CConstChunk& ccData) const;
	virtual unsigned GetParamsCount() const;
	virtual const CConstChunk& GetParam(unsigned n) const = 0;
	virtual const CConsoleCommandMapItem* GetCommandsMapRoot() const;
public:
	void BindStdout(IStreamReceiver* pStreamReceiver) {m_pStdout = pStreamReceiver;}
	void SetCommandsMapRoot(const CConsoleCommandMapItem* pCommandsMapRoot) {m_pCommandsMapRoot = pCommandsMapRoot;}
	void Execute(const CConstChunk& ccAction) const;
protected:
	IStreamReceiver* m_pStdout;
	unsigned    m_nParams;
	const CConsoleCommandMapItem* m_pCommandsMapRoot;
};

class CConsoleSubContext:
		public CConsoleContext
{
public:
	void Init(const IConsoleContext* pMasterContext, unsigned uiParamsOffset, const CConsoleCommandMapItem* pCommandsMapRoot);
public: // IConsoleContext
	virtual unsigned BroadcastStdout(const CConstChunk& ccData) const;
	virtual unsigned GetParamsCount() const;
	virtual const CConstChunk& GetParam(unsigned n) const;
private:
	const IConsoleContext* m_pMasterContext;
	unsigned               m_nParamsOffset;
};

template<unsigned nMaxCommandLineLengh, unsigned nMaxParamsCount = 16>
class TConsoleContext:
		public CConsoleContext
{
public: // IConsoleContext
	virtual const CConstChunk& GetParam(unsigned n) const {IMPLEMENTS_INTERFACE_METHOD(IConsoleContext::GetParam(n)); ASSERTE(n < m_nParams); ASSERTE(n < ARRAY_SIZE(m_accParams)); return m_accParams[n];}
public:
	bool SetCommandLine(const CConstChunk& ccCommandLine) {
		CopyCommandLine(ccCommandLine);
		SplitCommandLineParams();
		return true;
	}
private:
	void CopyCommandLine(const CConstChunk& ccCommandLine) {
		unsigned l = MIN2(ccCommandLine.uiLength, nMaxCommandLineLengh);
		memcpy(m_aCommandLine, ccCommandLine.pData, l);
		m_ccCommandLine.Assign(m_aCommandLine, l);
	}
	void SplitCommandLineParams() {
		const uint8_t* p = m_ccCommandLine.pData;
		unsigned n = m_ccCommandLine.uiLength;
		CConstChunk* pccParam = m_accParams;
		pccParam->Clean();
		while (n > 0) {
			bool bIsDelimiter = (*p == ' ' || *p == '\t');
			if (!bIsDelimiter) {
				if (!pccParam->uiLength) {
					pccParam->pData = p;
				}
				++(pccParam->uiLength);
			} else if (pccParam->uiLength) {
				++pccParam;
				if (pccParam < &m_accParams[ARRAY_SIZE(m_accParams)]) {
					pccParam->Clean();
				} else {
					break;
				}
			}
			++p;
			--n;
		}
		m_nParams = pccParam - m_accParams + (pccParam < &m_accParams[ARRAY_SIZE(m_accParams)] && pccParam->uiLength ? 1 : 0);
	}
private:
	uint8_t     m_aCommandLine[nMaxCommandLineLengh];
	CConstChunk m_ccCommandLine;
	CConstChunk m_accParams[nMaxParamsCount];
};

#endif // TERMINAL_H_INCLUDED
