#ifndef CONSOLE_COMMAND_H_INCLUDED
#define CONSOLE_COMMAND_H_INCLUDED

#include "util/macros.h"
#include "util/utils.h"
#include "util/map.h"
#include "util/chained_chunks.h"
#include "terminal.h"
#include <stdio.h>
#include <stdint.h>

//extern
class IConsoleContext;

class IConsoleCommand
{
public: // IConsoleCommand
	virtual const CConstChunk& GetName() const = 0;
	virtual const CConstChunk& GetDescription() const = 0;
	virtual bool StartProcessing(const IConsoleContext* pContext) = 0; /* return true when done */
	virtual bool ContinueProcessing(const CConstChunk& ccInputLine) = 0; /* return true when done */
	virtual void TerminateProcessing() = 0;
};

class CConsoleCommandBase :
		public IConsoleCommand
{
public: /* IConsoleCommand */
	virtual bool StartProcessing(const IConsoleContext* pContext);
	virtual bool ContinueProcessing(const CConstChunk& ccInputLine);
	virtual void TerminateProcessing();
public: /* CConsoleCommandBase */
	virtual void Exec() = 0;
protected:
	void Stdout(const CConstChunk& cc);
	void StdoutEOL() {STR_CHUNK(ccEOL,"\r\n"); Stdout(ccEOL);}
	void StdoutI(int iValue) {ToString<> s(iValue); CConstChunk cc={(const uint8_t*)s.GetValue(), s.GetLength()}; Stdout(cc);}
	void StdoutU(unsigned uiValue) {ToString<> s(uiValue); CConstChunk cc={(const uint8_t*)s.GetValue(), s.GetLength()}; Stdout(cc);}
	void StdoutF(float fValue) {ToString<> s(fValue); CConstChunk cc={(const uint8_t*)s.GetValue(), s.GetLength()}; Stdout(cc);}
	const IConsoleContext* GetContext() const {return m_pContext;}
	unsigned GetParamsCount();
	const CConstChunk& GetParam(unsigned n);
	int GetParamI(unsigned nIdx, int iDefault);
	unsigned GetParamU(unsigned nIdx, unsigned uiDefault);
	float GetParamF(unsigned nIdx, float fDefault);
	bool HasNextParam() {return m_uiNextParamIndex + 1 < GetParamsCount();}
	const CConstChunk& GetNextParam() { return GetParam(++m_uiNextParamIndex); }
	int GetNextParamI(int iDefault) {return GetParamI(++m_uiNextParamIndex, iDefault);}
	unsigned GetNextParamU(unsigned uiDefault) {return GetParamU(++m_uiNextParamIndex, uiDefault);}
	float GetNextParamF(float fDefault) {return GetParamF(++m_uiNextParamIndex, fDefault);}
private:
	const IConsoleContext* m_pContext;
	unsigned m_uiNextParamIndex;
};

#define CONSOLE_COMMAND_DECLARATION(GRP, NAME) \
		class CConsoleCommand_##GRP##_##NAME : \
				public CConsoleCommandBase \
		{ \
		public: /* IConsoleCommand */ \
			virtual const CConstChunk& GetName() const {IMPLEMENTS_INTERFACE_METHOD(IConsoleCommand::GetName()); return CConsoleCommand_##GRP##_##NAME::ccName;} \
			virtual const CConstChunk& GetDescription() const {IMPLEMENTS_INTERFACE_METHOD(IConsoleCommand::GetDescription()); return CConsoleCommand_##GRP##_##NAME::ccDescription;} \
		public: /* CConsoleCommandBase */ \
			virtual void Exec(); \
			static const CConstChunk ccName; \
			static const CConstChunk ccDescription; \
		} g_ConsoleCommand_##GRP##_##NAME; \
		extern const CConsoleCommandMapItem g_ConsoleCommandMapItem_##GRP##_##NAME;
#define CONSOLE_COMMAND_BTREE(GRP, NAME, LESS_NAME, TOP_NAME, GREATER_NAME) \
		const CConsoleCommandMapItem g_ConsoleCommandMapItem_##GRP##_##NAME = { \
				CConsoleCommand_##GRP##_##NAME::ccName, &g_ConsoleCommand_##GRP##_##NAME, \
				&g_ConsoleCommandMapItem_##GRP##_##LESS_NAME, \
				&g_ConsoleCommandMapItem_##GRP##_##TOP_NAME, \
				&g_ConsoleCommandMapItem_##GRP##_##GREATER_NAME, \
		};
#define CONSOLE_COMMAND_DEFINITION(GRP, NAME, DESCRIPTION) \
	STR_CHUNK(CConsoleCommand_##GRP##_##NAME::ccName, #NAME); \
	STR_CHUNK(CConsoleCommand_##GRP##_##NAME::ccDescription, DESCRIPTION); \
	void CConsoleCommand_##GRP##_##NAME::Exec()

#endif // CONSOLE_COMMAND_H_INCLUDED
