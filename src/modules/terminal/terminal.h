#ifndef TERMINAL_H_INCLUDED
#define TERMINAL_H_INCLUDED

#include "piped_uart.h"

class IStreamLineEdit :
		public CStreamFilter
{
public: // IStreamLineEdit
	virtual void BindLineProcessor(IStreamReceiver* pStreamReceiver) = 0;
};

IStreamLineEdit* GetStreamLineEdit();

#endif // TERMINAL_H_INCLUDED
