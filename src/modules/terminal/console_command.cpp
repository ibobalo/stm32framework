#include "stdafx.h"
#include "console_command.h"
#include "console_context.h"


bool CConsoleCommandBase::StartProcessing(const IConsoleContext* pContext) {
	IMPLEMENTS_INTERFACE_METHOD(IConsoleCommand::StartProcessing(pContext));
	m_pContext = pContext;
	m_uiNextParamIndex = 0;
	Exec();
	return false;
}

bool CConsoleCommandBase::ContinueProcessing(const CConstChunk& ccInputLine) {
	IMPLEMENTS_INTERFACE_METHOD(IConsoleCommand::ContinueProcessing(ccInputLine));
	ASSERTE(false); // false returned from StartProcessing so it should not enter here
	return false;
}

void CConsoleCommandBase::TerminateProcessing() {
	IMPLEMENTS_INTERFACE_METHOD(IConsoleCommand::TerminateProcessing());
	// do nothing
}

void CConsoleCommandBase::Stdout(const CConstChunk& cc)
{
	m_pContext->BroadcastStdout(cc);
}

unsigned CConsoleCommandBase::GetParamsCount()
{
	return m_pContext->GetParamsCount();
}

const CConstChunk& CConsoleCommandBase::GetParam(unsigned n)
{
	return m_pContext->GetParam(n);
}

int CConsoleCommandBase::GetParamI(unsigned nIdx, int iDefault) {
	if (nIdx >= m_pContext->GetParamsCount()) return iDefault;
	const CConstChunk& cc = m_pContext->GetParam(nIdx);
	int iResult;
	if (NumberParserI(iResult, (const char*)cc.pData, cc.uiLength) == cc.uiLength) {
		return iResult;
	}
	return iDefault;
}

unsigned CConsoleCommandBase::GetParamU(unsigned nIdx, unsigned uiDefault) {
	if (nIdx >= m_pContext->GetParamsCount()) return uiDefault;
	const CConstChunk& cc = m_pContext->GetParam(nIdx);
	unsigned uiResult;
	return (NumberParserU(uiResult, (const char*)cc.pData, cc.uiLength) == cc.uiLength) ? uiResult : uiDefault;
}

float CConsoleCommandBase::GetParamF(unsigned nIdx, float fDefault) {
	if (nIdx >= m_pContext->GetParamsCount()) return fDefault;
	const CConstChunk& cc = m_pContext->GetParam(nIdx);
	float fResult;
	return (NumberParserF(fResult, (const char*)cc.pData, cc.uiLength) == cc.uiLength) ? fResult : fDefault;
}
