#include "stdafx.h"
#include "console_context.h"
#include "console_command.h"
#include "util/pipes.h"
#include "util/map.h"
#include <stdio.h>
#include <stdint.h>

unsigned CConsoleContext::BroadcastStdout(const CConstChunk& ccData) const
{
	IMPLEMENTS_INTERFACE_METHOD(BroadcastStdout(ccData));
	ASSERTE(m_pStdout);
	return m_pStdout->Push(ccData);
}

unsigned CConsoleContext::GetParamsCount() const
{
	IMPLEMENTS_INTERFACE_METHOD(IConsoleContext::GetParamsCount());
	return m_nParams;
}

const CConsoleCommandMapItem* CConsoleContext::GetCommandsMapRoot() const
{
	IMPLEMENTS_INTERFACE_METHOD(IConsoleContext::GetCommandsMapRoot());
	return m_pCommandsMapRoot;
}

void CConsoleContext::Execute(const CConstChunk& ccAction) const
{
	IConsoleCommand* pSubCommand = m_pCommandsMapRoot->GetDefault(ccAction, (IConsoleCommand*)NULL);
	if (pSubCommand) {
		pSubCommand->StartProcessing(this);
	} else {
		static STR_CHUNK(cc1, "\r\nUnknown command: \"");
		BroadcastStdout(cc1);
		BroadcastStdout(ccAction);
		static STR_CHUNK(cc2, "\". Available commands:\r\n");
		BroadcastStdout(cc2);
		for (const CConsoleCommandMapItem* it = m_pCommandsMapRoot->FirstItem(); it; it = it->NextItem()) {
			IConsoleCommand* pConsoleCommand = it->Get();
			static STR_CHUNK(ccTab, " ");
			BroadcastStdout(ccTab);
			BroadcastStdout(pConsoleCommand->GetName());
			static STR_CHUNK(ccSpace, " - ");
			BroadcastStdout(ccSpace);
			BroadcastStdout(pConsoleCommand->GetDescription());
			static STR_CHUNK(ccEOL, "\r\n");
			BroadcastStdout(ccEOL);
		}
		static STR_CHUNK(cc3, ".\r\n");
		BroadcastStdout(cc3);
	}
}


void CConsoleSubContext::Init(const IConsoleContext* pMasterContext, unsigned uiParamsOffset, const CConsoleCommandMapItem* pCommandsMapRoot)
{
	ASSERTE(pMasterContext);
	ASSERTE(uiParamsOffset <= pMasterContext->GetParamsCount());

	m_pMasterContext = pMasterContext;
	m_nParamsOffset = uiParamsOffset;
	m_pCommandsMapRoot = pCommandsMapRoot;
}

unsigned CConsoleSubContext::BroadcastStdout(const CConstChunk& ccData) const
{
	IMPLEMENTS_INTERFACE_METHOD(IConsoleContext::BroadcastStdout(ccData));
	return m_pMasterContext->BroadcastStdout(ccData);
}
unsigned CConsoleSubContext::GetParamsCount() const
{
	IMPLEMENTS_INTERFACE_METHOD(IConsoleContext::GetParamsCount());
	ASSERTE(m_pMasterContext);
	unsigned nMasterCount = m_pMasterContext->GetParamsCount();
	ASSERTE(nMasterCount >= m_nParamsOffset);
	return nMasterCount - m_nParamsOffset;
}

const CConstChunk& CConsoleSubContext::GetParam(unsigned n) const
{
	IMPLEMENTS_INTERFACE_METHOD(IConsoleContext::GetParam(n));
	unsigned nMasterCount = m_pMasterContext->GetParamsCount();
	ASSERTE(nMasterCount >= m_nParamsOffset);
	return m_pMasterContext->GetParam(nMasterCount - m_nParamsOffset);
}
