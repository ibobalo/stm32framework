INCLUDE_PATH += $(if $(filter YES,$(USE_TERMINAL)), \
	src/modules/terminal \
)
SRC += $(if $(filter YES,$(USE_TERMINAL)), $(addprefix src/modules/terminal/, \
	terminal.cpp \
	console_context.cpp \
	console_command.cpp \
))
