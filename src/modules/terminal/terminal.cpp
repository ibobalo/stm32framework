#include "stdafx.h"
#include "terminal.h"
#include "piped_uart.h"
#include "console_command.h"
#include "console_context.h"
#include <stdio.h>
#include <stdint.h>


template<unsigned uiLineSize>
class CStreamLineEdit :
		public IStreamLineEdit
{
public: // IStreamLineEdit
	void BindLineProcessor(IStreamReceiver* pStreamReceiver) {
		IMPLEMENTS_INTERFACE_METHOD(IStreamLineEdit::BindLineProcessor(pStreamReceiver));
		m_pLineReceiver = pStreamReceiver;
	}
public: // IStreamReceiver
	virtual unsigned Push(const CConstChunk& ccData) {
		IMPLEMENTS_INTERFACE_METHOD(IStreamReceiver::Push(ccData));
		const uint8_t* p = ccData.pData;
		unsigned n = ccData.uiLength;
		while (n) {
			ProcessInputCharacter(*p);
			++p;
			--n;
		}
		return ccData.uiLength;
	}

protected: // CStreamLineEdit
	virtual void ProcessInputCharacter(uint8_t ch) {
		switch (ch) {
			case 10:
			case 13: { // enter
				ProcessLine();
			} break;
			default: {
				AppendCharacter(ch);
			} break;
		}
	}
	virtual void Reset() {
		m_uiLength = 0;
	}
	virtual bool AppendCharacter(uint8_t ch) {
		if (m_uiLength < ARRAY_SIZE(m_aLine)) {
			m_aLine[m_uiLength] = ch;
			++m_uiLength;
			CConstChunk cc = {&ch, 1};
			Broadcast(cc);
			return true;
		}
		return false;
	}

protected:
	virtual bool ProcessLine() {
		STR_CHUNK(ccEol, "\r\n");
		Broadcast(ccEol);
		if (m_uiLength) {
			CConstChunk cc = {m_aLine, m_uiLength};
			if (m_pLineReceiver) m_pLineReceiver->Push(cc);
			Reset();
			return true;
		}
		return false;
	}
	uint8_t  m_aLine[uiLineSize];
	unsigned m_uiLength;

private:
	IStreamReceiver* m_pLineReceiver;
};

template<unsigned uiLineSize>
class CStreamLineEditBackspace :
		public CStreamLineEdit<uiLineSize>
{
	typedef CStreamLineEdit<uiLineSize> CParentLineEdit;
protected:
	virtual void ProcessInputCharacter(uint8_t ch) {
		switch (ch) {
			case 127: { // backspace
				ProcessBackspace();
			} break;
			default: {
				CParentLineEdit::ProcessInputCharacter(ch);
			} break;
		}
	}
	virtual bool ProcessBackspace() {
		if (CParentLineEdit::m_uiLength) {
			--CParentLineEdit::m_uiLength;
			STR_CHUNK(cc, "\b \b");
			CParentLineEdit::Broadcast(cc);
			return true;
		}
		return false;
	}
};

#define ESC     27
#define ESC_CH  '\x1B'
#define ESC_STR "\x1B"

template<unsigned uiLineSize>
class CStreamLineEditANSI :
		public CStreamLineEditBackspace<uiLineSize>
{
	typedef CStreamLineEditBackspace<uiLineSize> CParentLineEdit;
	typedef enum {
		ESC_STATE_NONE = 0 << 8,
		ESC_STATE_ESC  = 1 << 8,
		ESC_STATE_CSI  = 2 << 8,
		ESC_STATE_O    = 3 << 8,
	} ESC_STATE;
public: // IStreamReceiver
	virtual void ProcessInputCharacter(uint8_t ch) {
		switch (m_eEscState | ch) {
			case 127: { // backspace
				ProcessBackspace();
			} break;
			case ESC: { // escape
				m_eEscState = ESC_STATE_ESC;
			} break;
			case ESC_STATE_ESC + '[': { // ESC + '[' = Control Sequence Introducer
				m_eEscState = ESC_STATE_CSI;
				m_nCsiN[0] = 0;
				m_nCsiNIdx = 0;
			} break;
			case ESC_STATE_CSI + '0':
			case ESC_STATE_CSI + '1':
			case ESC_STATE_CSI + '2':
			case ESC_STATE_CSI + '3':
			case ESC_STATE_CSI + '4':
			case ESC_STATE_CSI + '5':
			case ESC_STATE_CSI + '6':
			case ESC_STATE_CSI + '7':
			case ESC_STATE_CSI + '8':
			case ESC_STATE_CSI + '9': {
				if (m_nCsiNIdx < ARRAY_SIZE(m_nCsiN)) {
					m_nCsiN[m_nCsiNIdx] *= 10;
					m_nCsiN[m_nCsiNIdx] += ch - '0';
				}
			} break;
			case ESC_STATE_CSI + ';': {
				++m_nCsiNIdx;
				if (m_nCsiNIdx < ARRAY_SIZE(m_nCsiN)) {
					m_nCsiN[m_nCsiNIdx] = 0;
				}
			} break;
			case ESC_STATE_CSI + 'A': { // cursor UP
				m_eEscState = ESC_STATE_NONE;
				ProcessCursorUp();
			} break;
			case ESC_STATE_CSI + 'B': { // cursor DOWN
				m_eEscState = ESC_STATE_NONE;
				ProcessCursorDown();
			} break;
			case ESC_STATE_CSI + 'C': { // cursor FORWARD
				m_eEscState = ESC_STATE_NONE;
				ProcessCursorForward();
			} break;
			case ESC_STATE_CSI + 'D': { // cursor BACK
				m_eEscState = ESC_STATE_NONE;
				ProcessCursorBack();
			} break;
			case ESC_STATE_CSI + '~': {
				m_eEscState = ESC_STATE_NONE;
				switch (m_nCsiN[0]) {
					case 1: { // Home
						ProcessCursorHome();
					} break;
					case 2: { // Ins
					} break;
					case 3: { // Del
						ProcessDel();
					} break;
					case 4: { // End
						ProcessCursorEnd();
					} break;
					case 5: { // PgUp
					} break;
					case 6: { // PgDn
					} break;
					case 15: { // F5
						ProcessFn(5);
					} break;
				}
			} break;
			case ESC_STATE_ESC + 'O': {
				m_eEscState = ESC_STATE_O;
			} break;
			case ESC_STATE_O + 'P':
			case ESC_STATE_O + 'Q':
			case ESC_STATE_O + 'R':
			case ESC_STATE_O + 'S': {
				m_eEscState = ESC_STATE_NONE;
				ProcessFn(1 + ch - 'P');
			} break;
			default: {
				m_eEscState = ESC_STATE_NONE;
				CParentLineEdit::ProcessInputCharacter(ch);
			} break;
		}
	}
protected:
	virtual void Reset() {
		CParentLineEdit::Reset();
		m_uiCursorPos = 0;
	}
	virtual bool AppendCharacter(uint8_t ch) {
		if (m_uiCursorPos == CParentLineEdit::m_uiLength) {
			if (CParentLineEdit::AppendCharacter(ch)) {
				++m_uiCursorPos;
				return true;
			}
		} else if (CParentLineEdit::m_uiLength < ARRAY_SIZE(CParentLineEdit::m_aLine)) {
			for (unsigned n = CParentLineEdit::m_uiLength; n > m_uiCursorPos; --n) {
				CParentLineEdit::m_aLine[n] = CParentLineEdit::m_aLine[n - 1];
			}
			CParentLineEdit::m_aLine[m_uiCursorPos] = ch;
			++CParentLineEdit::m_uiLength;
			STR_CHUNK( ccPre,
					ESC_STR "[s"  /* save position */
					ESC_STR "[K"  /* erase to end of line */
			);
			CParentLineEdit::Broadcast(ccPre);
			CConstChunk ccLine = {CParentLineEdit::m_aLine + m_uiCursorPos, CParentLineEdit::m_uiLength - m_uiCursorPos};
			CParentLineEdit::Broadcast(ccLine);
			STR_CHUNK( ccPost,
					ESC_STR "[u"  /* restore position */
					ESC_STR "[C"  /* cursor forward */
			);
			CParentLineEdit::Broadcast(ccPost);
			++m_uiCursorPos;
			return true;
		}
		return false;
	}
	virtual bool ProcessBackspace() {
		if (m_uiCursorPos == CParentLineEdit::m_uiLength) {
			if (CParentLineEdit::ProcessBackspace()) {
				--m_uiCursorPos;
				return true;
			}
		} else if (m_uiCursorPos) {
			--m_uiCursorPos;
			return ProcessDel();
		}
		return false;
	}
	virtual bool ProcessDel() {
		if (m_uiCursorPos < CParentLineEdit::m_uiLength) {
			for (unsigned n = m_uiCursorPos; n < CParentLineEdit::m_uiLength - 1; ++n) {
				CParentLineEdit::m_aLine[n] = CParentLineEdit::m_aLine[n + 1];
			}
			CParentLineEdit::m_aLine[CParentLineEdit::m_uiLength - 1] = ' ';
			RedrawLine();
			--CParentLineEdit::m_uiLength;
			return true;
		}
		return false;
	}
	virtual bool ProcessCursorUp() {
		STR_CHUNK(cc, ESC_STR "[2K"); // erase all Line
		CParentLineEdit::Broadcast(cc);
		Reset();
		return true;
	};
	virtual bool ProcessCursorDown() {
		STR_CHUNK(cc, ESC_STR "[2K"); // erase all Line
		CParentLineEdit::Broadcast(cc);
		Reset();
		return true;
	};
	virtual bool ProcessCursorForward() {
		if (m_uiCursorPos < CParentLineEdit::m_uiLength) {
			++m_uiCursorPos;
			STR_CHUNK(cc, ESC_STR "[C"); // cursor forward
			CParentLineEdit::Broadcast(cc);
			return true;
		}
		return false;
	};
	virtual bool ProcessCursorBack() {
		if (m_uiCursorPos) {
			--m_uiCursorPos;
			STR_CHUNK(cc, ESC_STR "[D"); // cursor back
			CParentLineEdit::Broadcast(cc);
			return true;
		}
		return false;
	};
	virtual bool ProcessCursorHome() {
		if (m_uiCursorPos) {
			m_uiCursorPos = 0;
			STR_CHUNK(cc, "\r"); // cursor to line start
			CParentLineEdit::Broadcast(cc);
			return true;
		}
		return false;
	};
	virtual bool ProcessCursorEnd() {
		if (m_uiCursorPos != CParentLineEdit::m_uiLength) {
			SetCursorPos(CParentLineEdit::m_uiLength);
			return true;
		}
		return false;
	};
	virtual bool ProcessFn(unsigned n) {
		return false;
	};
private:
	void SetCursorPos(unsigned uiPosToSet) {
		ASSERTE(CParentLineEdit::m_uiLength >= m_uiCursorPos);
		ASSERTE(CParentLineEdit::m_uiLength >= uiPosToSet);
		if (m_uiCursorPos > uiPosToSet) {
			STR_CHUNK(ccBack10, ESC_STR "[10D"); // Back x 10
			STR_CHUNK(ccBack4, ESC_STR "[4D"); // Back x 4
			STR_CHUNK(ccBack1, ESC_STR "[D"); // Back x 1
			while (m_uiCursorPos >= uiPosToSet + 10) {
				m_uiCursorPos -= 10;
				CParentLineEdit::Broadcast(ccBack10);
			}
			while (m_uiCursorPos >= uiPosToSet + 4) {
				m_uiCursorPos -= 4;
				CParentLineEdit::Broadcast(ccBack4);
			}
			while (m_uiCursorPos > uiPosToSet) {
				m_uiCursorPos -= 1;
				CParentLineEdit::Broadcast(ccBack1);
			}
		} else if (m_uiCursorPos < uiPosToSet) {
			STR_CHUNK(ccForward10, ESC_STR "[10C"); // Forward x 10
			STR_CHUNK(ccForward4,  ESC_STR "[4C"); // Forward x 4
			STR_CHUNK(ccForward1,  ESC_STR "[C"); // Forward x 1
			while (m_uiCursorPos + 10 <= uiPosToSet) {
				m_uiCursorPos += 10;
				CParentLineEdit::Broadcast(ccForward10);
			}
			while (m_uiCursorPos + 4 <= uiPosToSet) {
				m_uiCursorPos += 4;
				CParentLineEdit::Broadcast(ccForward4);
			}
			while (m_uiCursorPos < uiPosToSet) {
				m_uiCursorPos += 1;
				CParentLineEdit::Broadcast(ccForward1);
			}
		}
	}
	void RedrawLine() {
		ASSERTE(CParentLineEdit::m_uiLength >= m_uiCursorPos);
		unsigned uiCursorPosToSet = m_uiCursorPos;
		STR_CHUNK(cc, ESC_STR "[2K\r"); // erase all Line + move cursor to start of line
		CParentLineEdit::Broadcast(cc);
		CConstChunk ccLine = {CParentLineEdit::m_aLine, CParentLineEdit::m_uiLength};
		CParentLineEdit::Broadcast(ccLine);
		m_uiCursorPos = CParentLineEdit::m_uiLength;
		SetCursorPos(uiCursorPosToSet);
	}
private:
	ESC_STATE m_eEscState;
	unsigned m_nCsiN[5];
	unsigned m_nCsiNIdx;
	unsigned m_uiCursorPos;
};

CStreamLineEditANSI<128> g_StreamLineEditANSI;

IStreamLineEdit* GetStreamLineEdit() { return &g_StreamLineEditANSI; }
