#ifndef TRANSFORMER_H_INCLUDED
#define TRANSFORMER_H_INCLUDED

#include "util/pair.h"
#include "interfaces/common.h"

ITransformer<float>* AcquireOffsetScaleTransformer(const pair<float>* pScaleOffset);
ITransformer<float>* AcquireLinearTransformer(const pair<float, float>* InOutArray, unsigned uiLength);
ITransformer<float>* AcquirePolynomeTransformer(const float* pCoeffArray, unsigned uiLength);

#endif // TRANSFORMER_H_INCLUDED
