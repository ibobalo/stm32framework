#include "stdafx.h"
#include "transformer.h"
#include "util/core.h"
#include "util/macros.h" // ASSERT IMPLEMENTS_INTERFACE_METHOD
#include <math.h>

#ifndef SCALE_TRANSFORMERS
# define SCALE_TRANSFORMERS  8
#endif
#ifndef LINEAR_TRANSFORMERS
# define LINEAR_TRANSFORMERS  8
#endif
#ifndef POLYNOME_TRANSFORMERS
# define POLYNOME_TRANSFORMERS 1
#endif


template<class T>
class CScaleTransformer:
		public ITransformer<T>
{
public: // ITransformer
	virtual T Transform(const T tInValue) const;
public:
	void Init(const pair<T>* pScaleOffset);
private:
	const pair<T,T>*    m_pScaleOffset;
};

template<class T>
void CScaleTransformer<T>::Init(const pair<T,T>* pScaleOffset)
{
	ASSERTE(pScaleOffset);
	m_pScaleOffset = pScaleOffset;
}

template<class T>
T CScaleTransformer<T>::Transform(const T tInValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(ITransformer<T>::Transform(tInValue));

	ASSERTE(m_pScaleOffset > 0);

	return tInValue * m_pScaleOffset->first + m_pScaleOffset->second;
}


template<class T>
class CLinearTransformer:
		public ITransformer<T>
{
public: // ITransformer
	virtual T Transform(const T tInValue) const;
public: // ILinearTransformer
	void Init(const pair<T, T>* pInOutArray, unsigned uiLength);
private:
	const pair<T, T>* m_pInOutArray;
	unsigned          m_uiLength;
};

template<class T>
void CLinearTransformer<T>::Init(const pair<T, T>* pInOutArray, unsigned uiLength)
{
	ASSERTE(pInOutArray);
	ASSERTE(uiLength > 0);

	m_pInOutArray = pInOutArray;
	m_uiLength = uiLength;
}

template<class T>
T CLinearTransformer<T>::Transform(const T tInValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(ITransformer<T>::Transform(tInValue));

	ASSERTE(m_uiLength > 0);

	if (tInValue < m_pInOutArray[0].first) {
		return m_pInOutArray[0].second;
	} else if (tInValue >= m_pInOutArray[m_uiLength - 1].first) {
		return m_pInOutArray[m_uiLength - 1].second;
	} {
		for (unsigned n = 1; n < m_uiLength; ++n) {
			if (tInValue < m_pInOutArray[n].first) {
				T x1 = m_pInOutArray[n - 1].first;
				T x2 = m_pInOutArray[n].first;
				T y1 = m_pInOutArray[n - 1].second;
				T y2 = m_pInOutArray[n].second;
				return y1 + (tInValue - x1) * (y2 - y1) / (x2 - x1);
			}
		}
	}
	ASSERT("calc flt");
	return (T)0;
}


template<class T>
class CPolynomeTransformer:
		public ITransformer<T>
{
public: // ITransformer
	virtual T Transform(const T tInValue) const;
public: // IPolynomeTransformer
	virtual void Init(const T* pCoeffArray, unsigned uiLength);
private:
	const T*       m_pCoeffArray;
	unsigned       m_uiLength;
};

template<class T>
void CPolynomeTransformer<T>::Init(const T* pCoeffArray, unsigned uiLength)
{
	ASSERTE(pCoeffArray);
	ASSERTE(uiLength > 0);

	m_pCoeffArray = pCoeffArray;
	m_uiLength = uiLength;
}

template<class T>
T CPolynomeTransformer<T>::Transform(const T tInValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(ITransformer<T>::Transform(tInValue));

	ASSERTE(m_uiLength > 0);

	T tResult = m_pCoeffArray[0];
	T tValuePower = tInValue;
	for (unsigned n = 1; n < m_uiLength; ++n) {
		tResult += m_pCoeffArray[n] * tValuePower;
		tValuePower *= tInValue;
	}

	return tResult;
}

CScaleTransformer<float> g_aScaleTransformers[SCALE_TRANSFORMERS];
unsigned g_nScaleTransformerUsed;
CLinearTransformer<float> g_aLinearTransformers[LINEAR_TRANSFORMERS];
unsigned g_nLinearTransformerUsed;
CPolynomeTransformer<float> g_aPolynomeTransformers[POLYNOME_TRANSFORMERS];
unsigned g_nPolynomeTransformerUsed;

ITransformer<float>* AcquireOffsetScaleTransformer(const pair<float>* pScaleOffset)
{
	ASSERTE(g_nScaleTransformerUsed < ARRAY_SIZE(g_aScaleTransformers)); // define LINEAR_TRANSFORMERS
	CScaleTransformer<float>* pResult = &g_aScaleTransformers[g_nScaleTransformerUsed];
	++g_nScaleTransformerUsed;
	pResult->Init(pScaleOffset);
	return pResult;
}

ITransformer<float>* AcquireLinearTransformer(const pair<float, float>* pInOutArray, unsigned uiLength)
{
	ASSERTE(g_nLinearTransformerUsed < ARRAY_SIZE(g_aLinearTransformers)); // define LINEAR_TRANSFORMERS
	CLinearTransformer<float>* pResult = &g_aLinearTransformers[g_nLinearTransformerUsed];
	++g_nLinearTransformerUsed;
	pResult->Init(pInOutArray, uiLength);
	return pResult;
}

ITransformer<float>* AcquirePolynomeTransformer(const float* pCoeffArray, unsigned uiLength)
{
	ASSERTE(g_nPolynomeTransformerUsed < ARRAY_SIZE(g_aPolynomeTransformers)); // define POLYNOME_TRANSFORMERS
	CPolynomeTransformer<float>* pResult = &g_aPolynomeTransformers[g_nPolynomeTransformerUsed];
	++g_nPolynomeTransformerUsed;
	pResult->Init(pCoeffArray, uiLength);
	return pResult;
}

