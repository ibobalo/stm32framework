#ifndef PROCESS_RAMP_H_INCLUDED
#define PROCESS_RAMP_H_INCLUDED

#include "util/pair.h"
#include "util/callback.h"
#include "interfaces/common.h"
#include "hw/tick_timesource.h"
#include "transformer.h"

template<class T>
class IRamp :
		public IValueSource<T>,
		public IValueProvider<T>
{
//public:
//	typedef Callback<void (float)> SetValueCallback;
//public: // IValueSource
//	virtual float GetValue() const = 0;
//public: // IValueProvider
//	virtual void SetValueTargetInterface(IValueTarget<float>* pValueSource) = 0;
public: // IRamp
	virtual void SingleStep(ticktime_t uiTime) = 0;

public: // IRamp
	virtual void AssignOutputTransformer(const ITransformer<T>* pOutputTransformer) = 0;
	virtual void SetValueInstant(T tValue) = 0;
	virtual void SetValueWithEndTime(T tFinalValue, ticktime_t ttFinalTime) = 0;
	virtual void SetValueWithPeriod(T tValue, ticktime_t ttPeriod) = 0;

//	virtual T GetOutValue() const = 0;
	virtual T GetCurrentValue() const = 0;
	virtual T GetFinalValue() const = 0;
	virtual void Stop() = 0;
	virtual bool IsStopped() const = 0;
};

IRamp<float>* AcquireRampProcess(const float& tPrecition, ticktime_delta_t tdMinTimeDelta, const ITransformer<float>* pOutputTransformer = 0);
IRamp< tripple<float, float, float> >* AcquireRamp3Process(const tripple<float, float, float>& tPrecition, ticktime_delta_t tdMinTimeDelta, const ITransformer<tripple<float, float, float>>* pOutputTransformer = 0);

#endif // PROCESS_RAMP_H_INCLUDED
