INCLUDE_PATH += $(if $(filter YES,$(USE_RAMP)), \
	src/modules/ramp \
)
SRC += $(if $(filter YES,$(USE_RAMP)), $(addprefix src/modules/ramp/, \
	proc_ramp.cpp \
	transformer.cpp \
))
USE_MATHLIB += $(if $(filter YES,$(USE_RAMP)), \
	YES \
)
