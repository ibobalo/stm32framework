#include "stdafx.h"
#include "proc_ramp.h"
#include "hw/hw.h"
#include "hw/debug.h"
#include "util/macros.h"
#include "util/utils.h"
#include "tick_process.h"
//#include "zero_sync_pwm.h"
#include <math.h>

#ifndef RAMPS_COUNT
# ifdef TRIAC_CHANNEL_COUNT
#  define RAMPS_COUNT TRIAC_CHANNEL_COUNT
# else
#  define RAMPS_COUNT 8
# endif
#endif

template<class T>
class CRampProcess :
	public IRamp<T>,
	public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;

	void Init(const T& tPrecition, ticktime_delta_t tdMinTimeDelta, const ITransformer<T>* pOutputTransformer = NULL);

public: // IRamp
	virtual void AssignOutputTransformer(const ITransformer<T>* pOutputTransformer);
//	virtual void RegisterValueCallback(SetValueCallback cbSetValueCallback);

	virtual void SetValueInstant(T tValue);
	virtual void SetValueWithEndTime(T tFinalValue, ticktime_t ttFinalTime);
	virtual void SetValueWithPeriod(T tValue, ticktime_t ttPeriod);
//	virtual void SetValueWithRate(T dValue, double dRate);

//	virtual T GetOutValue() const;
	virtual T GetCurrentValue() const;
	virtual T GetFinalValue() const;
	virtual void Stop();
	virtual bool IsStopped() const;
public: // IValueSource
	virtual T GetValue() const;
public: // IValueProvider
	virtual void SetValueTargetInterface(IValueReceiver<T>* pValueTarget);
public: //CTickProcess
	virtual void SingleStep(ticktime_t uiTime);

private:
	ticktime_delta_t CalcTimeStep(T tValueRange, ticktime_delta_t dDuration) const;
	void Recalc(ticktime_t uiTime);
	void InternalSetValue(T tValue);
private:
	T                 m_tValuePrecision;
	ticktime_delta_t  m_tdMinTimeDelta;
	const ITransformer<T>* m_pOutputTransformer;

	bool              m_bStopped;
	T                 m_tFinalValue;
	ticktime_t        m_ttFinalTime;
	T                 m_tValueRange;
	ticktime_t        m_ttStartTime;

	T                 m_tCurrentValue;
	T                 m_tOutValue;
	ticktime_delta_t  m_tdTimeStep;
	T                 m_tValueDeltaPerTimeUnit;
//	ticktime_t        m_ttPrevTime;

	IValueReceiver<T>*  m_pValueTarget;
};

template<class T>
void CRampProcess<T>::Init(const T& tPrecition, ticktime_delta_t tdMinTimeDelta, const ITransformer<T>* pOutputTransformer /*= NULL*/)
{
	CParentProcess::Init(&g_TickProcessScheduller);
	m_tValuePrecision = tPrecition;
	m_tdMinTimeDelta = tdMinTimeDelta;
	m_pOutputTransformer = pOutputTransformer;
	m_bStopped = true;
	m_ttStartTime = 0;
//	m_ttPrevTime = 0;
	m_ttFinalTime = 0;
}

template<class T>
void CRampProcess<T>::AssignOutputTransformer(const ITransformer<T>* pOutputTransformer /*= NULL*/)
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::AssignOutputTransformer(pOutputTransformer));

	m_pOutputTransformer = pOutputTransformer;
}

//template<class T>
//T CRampProcess<T>::GetOutValue() const
//{
//	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::GetOutValue());
//	return m_tOutValue;
//}

template<class T>
T CRampProcess<T>::GetCurrentValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::GetCurrentValue());
	return m_tCurrentValue;
}

template<class T>
T CRampProcess<T>::GetFinalValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::GetFinalValue());
	return m_tFinalValue;
}

template<class T>
void CRampProcess<T>::Stop()
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::Stop());
	m_tFinalValue = m_tCurrentValue;
	m_bStopped = true;
	Cancel();
}

template<class T>
bool CRampProcess<T>::IsStopped() const
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::IsStopped());
	return (m_bStopped);
}

template<class T>
T CRampProcess<T>::GetValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(IValueSource<T>::GetValue());
	return m_tOutValue;
}

template<class T>
void CRampProcess<T>::SetValueTargetInterface(IValueReceiver<T>* pValueTarget)
{
	IMPLEMENTS_INTERFACE_METHOD(IValueProvider<T>::SetValueTargetInterface(pValueTarget));
	m_pValueTarget = pValueTarget;
}


template<class T>
void CRampProcess<T>::SingleStep(ticktime_t ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CRampProcess");

	if (m_bStopped) {
		// do nothing
	} else {
		Recalc(ttTime);
	}

	PROCESS_DEBUG_INFO("<<< Process: CRampProcess");
}

template<class T>
void CRampProcess<T>::SetValueInstant(T tValue)
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::SetValueInstant(tValue));
	InternalSetValue(tValue);
	Stop();
//	DEBUG_INFO("Dimmer Value Instant (%):", 100.0*dValue);
}

template<class T>
void CRampProcess<T>::SetValueWithPeriod(T tValue, ticktime_t ttPeriod) {
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::SetValueWithPeriod(tValue, ttPeriod));
	SetValueWithEndTime(tValue, GetTimeNow() + ttPeriod);
};
template<class T>
void CRampProcess<T>::SetValueWithEndTime(T tFinalValue, ticktime_t ttFinalTime)
{
	IMPLEMENTS_INTERFACE_METHOD(IRamp<T>::SetValueWithEndTime(tFinalValue, ttFinalTime));
	if (tFinalValue != m_tCurrentValue) {
		ticktime_t ttNowTime = GetTimeNow();
		ticktime_delta_t tdDuration = (ticktime_delta_t)(ttFinalTime - ttNowTime);
		if (tdDuration <= 0) {
			InternalSetValue(m_tFinalValue);
			Stop();
		} else {
			m_bStopped = false;
			m_ttFinalTime = ttFinalTime;
			m_tFinalValue = tFinalValue;
			m_ttStartTime = ttNowTime;
			m_tValueRange = tFinalValue - m_tCurrentValue;
			m_tdTimeStep = CalcTimeStep(m_tValueRange, tdDuration);
			DEBUG_INFO("Ramp start with dt:", m_tdTimeStep);
			ticktime_t ttContinueTime = m_ttStartTime + m_tdTimeStep;
			if (ttContinueTime > m_ttFinalTime) ttContinueTime = m_ttFinalTime;
			ContinueAt(ttContinueTime);
		}
	} else {
		Stop();
	}
}

template<class T>
ticktime_delta_t CRampProcess<T>::CalcTimeStep(T tValueRange, ticktime_delta_t dDuration) const
{
	return m_tdMinTimeDelta;
}
template<>
ticktime_delta_t CRampProcess<float>::CalcTimeStep(float fValueRange, ticktime_delta_t dDuration) const
{
	if (m_tValuePrecision <= 0) {
		return m_tdMinTimeDelta;
	}
	float fValueDeltaSteps = fValueRange / m_tValuePrecision;
	if (fValueDeltaSteps < 0.0) fValueDeltaSteps = -fValueDeltaSteps;
	ticktime_delta_t tdTimeStep = 1 + (ticktime_delta_t)fValueDeltaSteps;
	if (tdTimeStep < m_tdMinTimeDelta) tdTimeStep = m_tdMinTimeDelta;
	return m_tdMinTimeDelta;
}

template<class T>
void CRampProcess<T>::Recalc(ticktime_t ttTime)
{
	ticktime_delta_t tdDuration = (ticktime_delta_t)(m_ttFinalTime - m_ttStartTime);
	ticktime_delta_t tdTimeLeft = (ticktime_delta_t)(m_ttFinalTime - ttTime);
	ASSERTE(tdDuration > 0);
	if (tdTimeLeft <= 0) {
		InternalSetValue(m_tFinalValue);
		Stop();
		DEBUG_INFO("Ramp done");
	} else if (tdTimeLeft >= tdDuration) {
		InternalSetValue(m_tFinalValue - m_tValueRange);
		ContinueAt(m_ttStartTime + m_tdTimeStep);
	} else {
		T tNewValue = m_tFinalValue - m_tValueRange * tdTimeLeft / tdDuration;
		InternalSetValue(tNewValue);
		ticktime_t ttContinueTime = ttTime + m_tdTimeStep;
		if (ttContinueTime > m_ttFinalTime) ttContinueTime = m_ttFinalTime;
		ContinueAt(ttContinueTime);
	}
}

template<class T>
void CRampProcess<T>::InternalSetValue(T tValue)
{
	if (m_tCurrentValue != tValue) {
		m_tCurrentValue = tValue;
		m_tOutValue = m_pOutputTransformer ? m_pOutputTransformer->Transform(tValue) : tValue;
		if (m_pValueTarget) m_pValueTarget->SetValue(m_tOutValue);
	}
}

//////////////////////////////////////////////////////////////////////////////

CRampProcess<float> g_aRampProcesses[RAMPS_COUNT];
static unsigned nRampProcessesUsed = 0;

IRamp<float>* AcquireRampProcess(const float& tPrecition, ticktime_delta_t tdMinTimeDelta, const ITransformer<float>* pOutputTransformer)
{
	ASSERTE(nRampProcessesUsed < ARRAY_SIZE(g_aRampProcesses));
	CRampProcess<float>* pRamp = &(g_aRampProcesses[nRampProcessesUsed]);
	++nRampProcessesUsed;
	pRamp->Init(tPrecition, tdMinTimeDelta, pOutputTransformer);
	return pRamp;
}

CRampProcess< tripple<float, float, float> > g_aRamp3Processes[RAMPS_COUNT];
static unsigned nRamp3ProcessesUsed = 0;

IRamp< tripple<float, float, float> >* AcquireRamp3Process(const tripple<float, float, float>& tPrecition, ticktime_delta_t tdMinTimeDelta, const ITransformer<tripple<float, float, float>>* pOutputTransformer)
{
	ASSERTE(nRamp3ProcessesUsed < ARRAY_SIZE(g_aRamp3Processes));
	CRampProcess< tripple<float, float, float> >* pRamp3 = &(g_aRamp3Processes[nRamp3ProcessesUsed]);
	++nRamp3ProcessesUsed;
	pRamp3->Init(tPrecition, tdMinTimeDelta, pOutputTransformer);
	return pRamp3;
}
