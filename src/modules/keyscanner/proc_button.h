#pragma once

#include "util/callback.h"
#include "tick_process.h"



typedef enum {BUTTON_PRESS, BUTTON_HOLD, BUTTON_RELEASE, BUTTON_CLICK, BUTTON_MULTICLICK} EButtonEvent;

template<class TStaticValueSource, bool bInvert = false, ticktime_delta_t tdHold = 200, ticktime_delta_t tdClicks = 500, ticktime_delta_t tdDebouncing = 20>
class TStaticButtonController :
		public CTickProcess
{
public:
	typedef Callback<void (EButtonEvent, ticktime_delta_t)> CBtnEventCallback;
public:
	void Init(CBtnEventCallback cbBtnState) {
		CTickProcess::Init(&g_TickProcessScheduller);
		ASSERTE(cbBtnState);
		m_cbBtnState = cbBtnState;
		if (tdClicks) m_nClicks = 0;
		m_bBtnLastState = false;
		m_ttBtnLastPressTime = 0;
		ContinueDelay(tdDebouncing);
	}
public: // CTickProcess
	virtual void SingleStep(CTimeType ttTime);

public:
	static bool IsPressed() {return m_bBtnLastState;}

private:
	static CBtnEventCallback m_cbBtnState;
	static bool             m_bBtnLastState;
	static unsigned         m_nClicks;
	static CTimeType        m_ttBtnLastPressTime;
	static CTimeType        m_ttBtnLastReleaseTime;
};


template<class TStaticValueSource, bool bInvert, ticktime_delta_t tdHold, ticktime_delta_t tdClicks, ticktime_delta_t tdDebouncing>
typename TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::CBtnEventCallback
	TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::m_cbBtnState;
template<class TStaticValueSource, bool bInvert, ticktime_delta_t tdHold, ticktime_delta_t tdClicks, ticktime_delta_t tdDebouncing>
unsigned TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::m_nClicks;
template<class TStaticValueSource, bool bInvert, ticktime_delta_t tdHold, ticktime_delta_t tdClicks, ticktime_delta_t tdDebouncing>
bool TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::m_bBtnLastState;
template<class TStaticValueSource, bool bInvert, ticktime_delta_t tdHold, ticktime_delta_t tdClicks, ticktime_delta_t tdDebouncing>
typename TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::CTimeType
	TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::m_ttBtnLastPressTime;
template<class TStaticValueSource, bool bInvert, ticktime_delta_t tdHold, ticktime_delta_t tdClicks, ticktime_delta_t tdDebouncing>
typename TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::CTimeType
	TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::m_ttBtnLastReleaseTime;


template<class TStaticValueSource, bool bInvert, ticktime_delta_t tdHold, ticktime_delta_t tdClicks, ticktime_delta_t tdDebouncing>
void TStaticButtonController<TStaticValueSource, bInvert, tdHold, tdClicks, tdDebouncing>::SingleStep(CTimeType ttTime)
{
	bool bBtnState = (bInvert != TStaticValueSource::GetValue());
	if (bBtnState && !m_bBtnLastState) {
		ticktime_delta_t tdReleased = ttTime - m_ttBtnLastReleaseTime;
		if (tdReleased > tdDebouncing) {
			m_bBtnLastState = bBtnState;
			m_ttBtnLastPressTime = ttTime;
			m_cbBtnState(BUTTON_PRESS, tdReleased);
		}
	} else if (!bBtnState && m_bBtnLastState) {
		ticktime_delta_t tdPressed = ttTime - m_ttBtnLastPressTime;
		if (tdPressed > tdDebouncing) {
			m_bBtnLastState = bBtnState;
			m_ttBtnLastReleaseTime = ttTime;
			m_cbBtnState(BUTTON_RELEASE, tdPressed);
			if (tdClicks) {
				m_cbBtnState(BUTTON_CLICK, ++m_nClicks);
			}
		}
	} else if (bBtnState) {
		ticktime_delta_t tdPressed = ttTime - m_ttBtnLastPressTime;
		if (tdPressed > tdHold) {
			m_cbBtnState(BUTTON_HOLD, tdPressed);
		}
	} else if (tdClicks && m_nClicks) {
		ticktime_delta_t tdReleased = ttTime - m_ttBtnLastReleaseTime;
		if (tdReleased > tdClicks) {
			m_cbBtnState(BUTTON_MULTICLICK, m_nClicks);
			m_nClicks = 0;
		}
	}
	ContinueAt(ttTime + 1 + tdDebouncing / 2);
}
