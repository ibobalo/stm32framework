#ifndef PROCESS_KEYSCANNER_H_INCLUDED
#define PROCESS_KEYSCANNER_H_INCLUDED

#include "interfaces/led.h"
#include "interfaces/key.h"
#include "hw/tick_timesource.h"

IKeyboard<ticktime_t>* GetKeyboardInterface();
ILeds* GetLedsInterface();

#endif // PROCESS_KEYSCANNER_H_INCLUDED
