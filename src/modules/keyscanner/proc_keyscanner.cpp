#include "stdafx.h"
#include "proc_keyscanner.h"
#include "hw/hw.h"
#include "interfaces/notify.h"
#include "util/macros.h"
#include "util/utils.h"
#include "tick_process.h"

#define KEY_INVERT true
#define KEY_HOLD_TIME 500
#define KEY_DEBOUCE_TIME 100


class CLedInterfaceWrapper;
class CKeyLedScannerProcess;
class CKeyController;


class CLedInterfaceWrapper:
		public ILed
{
public:
	typedef ticktime_t CTimeType;
	typedef ticktime_delta_t CTimeDeltaType;
	constexpr static CTimeDeltaType tdBlinkDuration = 512;
	constexpr static CTimeDeltaType tdFastBlinkDuration = 128;
public:
	void Init(CKeyLedScannerProcess* pScanner) {m_apLedOutput = NULL; m_eState = LED_OFF; m_ttEndTime = 0; m_pScanner = pScanner;};
	void Acquire(IDigitalOut* pLedOutput) {ASSERTE(!m_apLedOutput); m_apLedOutput = pLedOutput; m_eState = LED_OFF;};
	void Release() { ASSERTE(m_apLedOutput); m_apLedOutput = NULL; m_eState = LED_OFF;};
	CTimeType Update(CTimeType ttTime);

public: // ILed
	virtual void Set();
	virtual void Clear();
	virtual void Blink(unsigned nCount);
	virtual void FastBlink(unsigned nCount);
	virtual void Pulse(unsigned nDuration);

private:
	typedef enum {LED_OFF = 0, LED_ON = 1, LED_BLINK, LED_FAST_BLINK, LED_BLINK_COUNT, LED_FAST_BLINK_COUNT} EState;
	IDigitalOut*           m_apLedOutput;
	CTimeType              m_ttEndTime;
	CKeyLedScannerProcess* m_pScanner;
	EState m_eState;
};



class CKeyController :
		public IKey<ticktime_t>
{
public:
	typedef ticktime_t CTimeType;
public:
	void Init(const IValueSource<bool>* pKeyStateSource, bool bInvert, CTimeType ttBtnHoldTime, CTimeType ttDebouncingTime);
	IKeyboardHandler<ticktime_t>::EKeyEvent ProcessSingleStep_Key(CTimeType ttTime);

public: // IKey<ticktime_t>
	virtual bool IsPressed() const;
	virtual void AssignKeyHandler(IKeyHandler<ticktime_t>* pHandler);
	virtual void UnassignKeyHandler();

private:
	void ProcessButtonPress(CTimeType ttTime);
	void ProcessButtonRelease(CTimeType ttTime);
	void ProcessButtonHold(CTimeType ttTime);
private:
	const IValueSource<bool>* m_pKeyStateSource;
	bool          m_bInvertState;
	bool          m_bBtnLastState;
	CTimeType     m_ttBtnLastPressTime;
	CTimeType     m_ttBtnLastReleaseTime;
	int           m_ttBtnHoldTime;
	int           m_ttDebouncingTime;
	IKeyHandler<ticktime_t>*  m_pHandler;
};

void CKeyController::Init(const IValueSource<bool>* pKeyStateSource, bool bInvert, CTimeType ttBtnHoldTime, CTimeType ttDebouncingTime)
{
	m_pKeyStateSource = pKeyStateSource;
	m_bInvertState = bInvert;
	m_bBtnLastState = false;
	m_ttBtnLastPressTime = 0;
	m_ttBtnHoldTime = ttBtnHoldTime;
	m_ttDebouncingTime = ttDebouncingTime;
	m_pHandler = NULL;
}

bool CKeyController::IsPressed() const
{
	IMPLEMENTS_INTERFACE_METHOD(IKey::IsPressed());
	return m_bBtnLastState;
}

void CKeyController::AssignKeyHandler(IKeyHandler<ticktime_t>* pHandler)
{
	IMPLEMENTS_INTERFACE_METHOD(IKey::AssignKeyHandler(pHandler));

	m_pHandler = pHandler;
}

void CKeyController::UnassignKeyHandler()
{
	IMPLEMENTS_INTERFACE_METHOD(IKey::UnassignKeyHandler());

	m_pHandler = NULL;
}

IKeyboardHandler<ticktime_t>::EKeyEvent CKeyController::ProcessSingleStep_Key(CTimeType ttTime)
{
	IKeyboardHandler<ticktime_t>::EKeyEvent eResult = IKeyboardHandler<ticktime_t>::KE_NONE;
	bool bBtnState = m_pKeyStateSource && (m_bInvertState != m_pKeyStateSource->GetValue());
	if (bBtnState && !m_bBtnLastState) {
		if ((int)(ttTime - m_ttBtnLastReleaseTime) > m_ttDebouncingTime) {
			m_bBtnLastState = bBtnState;
			m_ttBtnLastPressTime = ttTime;
			ProcessButtonPress(ttTime);
			eResult = IKeyboardHandler<ticktime_t>::KE_PRESS;
		}
	} else if (!bBtnState && m_bBtnLastState) {
		if ((int)(ttTime - m_ttBtnLastPressTime) > m_ttDebouncingTime) {
			m_bBtnLastState = bBtnState;
			m_ttBtnLastReleaseTime = ttTime;
			ProcessButtonRelease(ttTime);
			eResult = IKeyboardHandler<ticktime_t>::KE_RELEASE;
		}
	} else if (bBtnState) {
		if ((int)(ttTime - m_ttBtnLastPressTime) > m_ttBtnHoldTime) {
			ProcessButtonHold(ttTime);
			eResult = IKeyboardHandler<ticktime_t>::KE_HOLD;
		}
	}
	return eResult;
}

void CKeyController::ProcessButtonPress(CTimeType ttTime)
{
	if (m_pHandler) m_pHandler->OnButtonPress(ttTime);
}

void CKeyController::ProcessButtonRelease(CTimeType ttTime)
{
	if (m_pHandler) m_pHandler->OnButtonRelease(ttTime);
}

void CKeyController::ProcessButtonHold(CTimeType ttTime)
{
	if (m_pHandler) m_pHandler->OnButtonHold(ttTime);
}




class CKeyLedScannerProcess :
		public IKeyboard<ticktime_t>,
		public ILeds,
		public CTickProcess
{
public:
	typedef CTickProcess CParentProcess;
public: // IKeyboard<ticktime_t>
	virtual void Init();
	virtual void SetAllowedKeysMask(uint32_t uiMaskAllowed);
	virtual void Control(bool bEnable);
	virtual unsigned GetKeysCount() const;
	virtual IKey<ticktime_t>* GetKeyInterface(unsigned nKey);
	virtual void AssignKeyboardHandler(IKeyboardHandler<ticktime_t>* pGroupHandler);

public: // ILeds
	virtual ILed* AcquireLedInterface(unsigned nIndex);
	virtual void ReleaseLedInterface(unsigned nIndex);

public: // CTickProcess
	virtual void SingleStep(CTimeType uiTime);

private:
	void OnKeysChanged(uint16_t uiKeys); // callback from CHW_MCU

	CTimeType UpdateAllButtons(CTimeType ttTime);
	CTimeType UpdateAllLeds(CTimeType ttTime);

	bool         m_bEnabled;
	CLedInterfaceWrapper m_aLedInterfaceWrapper[LEDS_COUNT];

	ticktime_delta_t m_tdKeyTimeDelta;
	uint32_t m_uiKeysAllowedMask;
	CKeyController m_aKeyControllers[KEYS_COUNT];
	IKeyboardHandler<ticktime_t>* m_pHandler;
};



// return true if further Updates required
CLedInterfaceWrapper::CTimeType CLedInterfaceWrapper::Update(CTimeType ttTime)
{
	CTimeType ttNextTime = m_ttEndTime;
	if (m_ttEndTime && ttTime >= m_ttEndTime) {
		ttNextTime = 0;
		m_ttEndTime = 0;
		m_eState = LED_OFF;
	}
	switch (m_eState) {
		case LED_ON:
		case LED_OFF:
			m_apLedOutput->SetValue(m_eState == LED_ON);
			break;
#ifdef LEDS_HAS_HW_BLINK
		case LED_BLINK: break;
		case LED_FAST_BLINK: break;
#else // not LEDS_HAS_HW_BLINK
		case LED_BLINK: {
			m_apLedOutput->SetValue((ttTime / tdBlinkDuration) & 1);
			ttNextTime = SIZE_ALIGN(ttTime, tdBlinkDuration) + tdBlinkDuration;
		} break;
		case LED_FAST_BLINK: {
			m_apLedOutput->SetValue((ttTime / tdFastBlinkDuration) & 1);
			ttNextTime = SIZE_ALIGN(ttTime, tdFastBlinkDuration) + tdFastBlinkDuration;
		} break;
#endif // LEDS_HAS_HW_BLINK
		case LED_BLINK_COUNT: {
			CTimeDeltaType tdTimeLeft = m_ttEndTime - ttTime;
			m_apLedOutput->SetValue((tdTimeLeft / tdBlinkDuration) & 1);
			ttNextTime = ttTime + tdBlinkDuration;
		} break;
		case LED_FAST_BLINK_COUNT: {
			CTimeDeltaType tdTimeLeft = m_ttEndTime - ttTime;
			m_apLedOutput->SetValue((tdTimeLeft / tdFastBlinkDuration) & 1);
			ttNextTime = ttTime + tdFastBlinkDuration;
		} break;
	}
	return ttNextTime;
}

/*virtual*/
void CLedInterfaceWrapper::Set()
{
	IMPLEMENTS_INTERFACE_METHOD(ILed::Set());
	m_eState = LED_ON;
	m_ttEndTime = 0;
	m_apLedOutput->Set();
}

/*virtual*/
void CLedInterfaceWrapper::Clear() {
	IMPLEMENTS_INTERFACE_METHOD(ILed::Clear());
	m_eState = LED_OFF;
	m_ttEndTime = 0;
	m_apLedOutput->Reset();
}

/*virtual*/
void CLedInterfaceWrapper::Blink(unsigned nCount)
{
	IMPLEMENTS_INTERFACE_METHOD(ILed::Blink(nCount));
	if (!nCount) {
		m_eState = LED_BLINK;
		m_ttEndTime = 0;
	} else {
		m_eState = LED_BLINK_COUNT;
		m_ttEndTime = m_pScanner->GetTimeNow() + nCount * tdBlinkDuration * 2;
	}
	m_pScanner->ContinueNow();
}

/*virtual*/
void CLedInterfaceWrapper::FastBlink(unsigned nCount)
{
	IMPLEMENTS_INTERFACE_METHOD(ILed::FastBlink(nCount));
	if (!nCount) {
		m_eState = LED_FAST_BLINK;
		m_ttEndTime = 0;
	} else {
		m_eState = LED_FAST_BLINK_COUNT;
		m_ttEndTime = m_pScanner->GetTimeNow() + nCount * tdFastBlinkDuration * 2;
	}
	m_pScanner->ContinueNow();
}

/*virtual*/
void CLedInterfaceWrapper::Pulse(unsigned nDuration)
{
	IMPLEMENTS_INTERFACE_METHOD(ILed::Pulse(nDuration));
	m_eState = LED_ON;
	m_ttEndTime = m_pScanner->GetTimeNow() + (ticktime_delta_t)nDuration;
	m_pScanner->ContinueNow();
}



/* virtual */
void CKeyLedScannerProcess::Init()
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyboard<ticktime_t>::Init());
	IMPLEMENTS_INTERFACE_METHOD(ILeds::Init());

	CParentProcess::Init(&g_TickProcessScheduller);
	for (unsigned n = 0; n < LEDS_COUNT; ++n) {
		m_aLedInterfaceWrapper[n].Init(this);
	}
	for (unsigned n = 0; n < KEYS_COUNT; ++n) {
		m_aKeyControllers[n].Init(CHW_Brd::AcquireKbdInputInterface(n), KEY_INVERT, KEY_HOLD_TIME, KEY_DEBOUCE_TIME);
	}
	m_bEnabled = false;
	SetAllowedKeysMask(0xFFFFFFFF);
	m_tdKeyTimeDelta = CTimeSource::GetDeltaFromMilli(20);

#ifdef KBD_HAS_CALLBACK
	CHW_Brd::SetKbdChangeCallback(BIND_MEM_CB(&CKeyLedScannerProcess::OnKeysChanged, this));
#endif // KBD_HAS_CALLBACK

	Control(true);
}

/* virtual */
void CKeyLedScannerProcess::SetAllowedKeysMask(uint32_t uiMaskAllowed)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyboard<ticktime_t>::SetAllowedKeysMask(uiMaskAllowed));
	m_uiKeysAllowedMask = uiMaskAllowed;
}

/* virtual */
void CKeyLedScannerProcess::Control(bool bEnable)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyboard<ticktime_t>::Control(bEnable));

	if (bEnable) {
		if (!m_bEnabled) {
			m_bEnabled = true;
			ContinueNow();
		}
	} else {
		m_bEnabled = false;
		Cancel();
	}
}

/* virtual */
void CKeyLedScannerProcess::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CParentProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CKeyLedScannerProcess");


	CTimeType ttNextTimeByLeds = UpdateAllLeds(ttTime);

	CTimeType ttNextTimeByKeys = UpdateAllButtons(ttTime);

	CTimeType ttNextTime = !ttNextTimeByLeds ? ttNextTimeByKeys : (!ttNextTimeByKeys ? ttNextTimeByLeds : MIN2(ttNextTimeByKeys, ttNextTimeByLeds));
	if (ttNextTime) ContinueAt(ttNextTime);

	PROCESS_DEBUG_INFO("<<< Process: CKeyLedScannerProcess");
}

/* virtual */
ILed* CKeyLedScannerProcess::AcquireLedInterface(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(ILeds::AcquireLedInterface(nIndex));

	if (nIndex < LEDS_COUNT) {
		IDigitalOut* pOutput = CHW_Brd::AcquireLedOutputInterface(nIndex);
		m_aLedInterfaceWrapper[nIndex].Acquire(pOutput);
		return &m_aLedInterfaceWrapper[nIndex];
	}
	return NULL;
}

/* virtual */
void CKeyLedScannerProcess::ReleaseLedInterface(unsigned nIndex)
{
	IMPLEMENTS_INTERFACE_METHOD(ILeds::ReleaseLedInterface(nIndex));

	if (nIndex < LEDS_COUNT) {
		CHW_Brd::ReleaseLedOutputInterface(nIndex);
		m_aLedInterfaceWrapper[nIndex].Release();
	}
}

/* virtual */
unsigned CKeyLedScannerProcess::GetKeysCount() const
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyboard<ticktime_t>::GetKeysCount());

	return KEYS_COUNT;
}

/* virtual */
IKey<ticktime_t>* CKeyLedScannerProcess::GetKeyInterface(unsigned nKey)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyboard<ticktime_t>::GetKeyInterface(nKey));

	return (nKey < KEYS_COUNT) ? &m_aKeyControllers[nKey] : NULL;
}

/* virtual */
void CKeyLedScannerProcess::AssignKeyboardHandler(IKeyboardHandler<ticktime_t>* pGroupHandler)
{
	IMPLEMENTS_INTERFACE_METHOD(IKeyboard<ticktime_t>::AssignKeyboardHandler(pGroupHandler));
	m_pHandler = pGroupHandler;
}

CKeyLedScannerProcess::CTimeType CKeyLedScannerProcess::UpdateAllButtons(CTimeType ttTime)
{
	for (unsigned n = 0; n < KEYS_COUNT; ++n) {
		if (m_uiKeysAllowedMask & (0x1 << n)) {
			IKeyboardHandler<ticktime_t>::EKeyEvent eKeyEvent = m_aKeyControllers[n].ProcessSingleStep_Key(ttTime);
			if (eKeyEvent != IKeyboardHandler<ticktime_t>::KE_NONE) {
				if (m_pHandler) m_pHandler->OnKeyEvent(n, eKeyEvent, ttTime);
			}
		}
	}
	return (KEYS_COUNT && m_uiKeysAllowedMask) ? ttTime + m_tdKeyTimeDelta : 0;
}

CKeyLedScannerProcess::CTimeType CKeyLedScannerProcess::UpdateAllLeds(CTimeType ttTime)
{
	CTimeType ttNextTime = 0;
	for (unsigned n = 0; n < LEDS_COUNT; ++n) {
		CTimeType tt = m_aLedInterfaceWrapper[n].Update(ttTime);
		if (tt && (tt < ttNextTime || !ttNextTime)) {
			ttNextTime = tt;
		}
	}
	return ttNextTime;
}

void CKeyLedScannerProcess::OnKeysChanged(uint16_t uiKeys)
{
	if (m_bEnabled) {
		ContinueNow();
	}
}


CKeyLedScannerProcess g_KeysLedsScannerProcess;

IKeyboard<ticktime_t>* GetKeyboardInterface() { return &g_KeysLedsScannerProcess; };
ILeds* GetLedsInterface() { return &g_KeysLedsScannerProcess; };
