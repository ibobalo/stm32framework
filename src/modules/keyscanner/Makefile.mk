USE_PROCESS_SCHEDULLER += $(if $(filter YES,$(USE_KEYSCANNER)), \
	YES \
)
INCLUDE_PATH += $(if $(filter YES,$(USE_KEYSCANNER)), \
	src/modules/keyscanner \
)
SRC += $(if $(filter YES,$(USE_KEYSCANNER)), $(addprefix src/modules/keyscanner/, \
	proc_keyscanner.cpp \
))
