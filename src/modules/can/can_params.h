#ifndef CAN_PARAMS_INCLUDED
#define CAN_PARAMS_INCLUDED

#define CAN_PARAMS_LIST \
	PARAM(CAN_BAUDRATE,                 unsigned,  250000) \
	PARAM(CAN_BASE_ADDRESS,             unsigned,  0x0100) \

#endif // CAN_PARAMS_INCLUDED
