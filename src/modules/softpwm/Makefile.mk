INCLUDE_PATH += $(if $(filter YES,$(USE_SOFTPWM)), \
	src/modules/softpwm \
)
SRC += $(if $(filter YES,$(USE_SOFTPWM)), $(addprefix src/modules/softpwm/, \
	proc_softpwm.cpp \
))
