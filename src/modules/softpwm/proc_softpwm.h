#ifndef PROCESS_SOFTPWM_H_INCLUDED
#define PROCESS_SOFTPWM_H_INCLUDED

#include "interfaces/gpio.h"
#include "interfaces/timer.h"

class ITimerSoft :
		public ITimer
{
public: //ITimerSoft
		virtual ITimerChannel* AcquireChannelInterface(IDigitalOut* pDigitalOut, unsigned uiValue, bool bInverted) = 0;
		virtual void ReleaseChannelInterface(ITimerChannel* pChannel) = 0;
};
ITimerSoft* AcquireSoftPwmTimer();
void ReleaseSoftPwmTimer();

#endif // PROCESS_SOFTPWM_H_INCLUDED
