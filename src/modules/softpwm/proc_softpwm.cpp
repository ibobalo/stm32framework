#include "stdafx.h"
#include "proc_softpwm.h"
#include "tick_process.h"

#ifndef SOFT_PWM_CHANNELS
# define SOFT_PWM_CHANNELS 10
#endif

class CSoftPWMTimerProcess;
class CSoftPWMTimerChannel;

class CSoftPWMTimerChannel :
	public ITimerChannel
{
public:
	void Init(unsigned nChannel, CSoftPWMTimerProcess* pParentSoftTimer, IDigitalOut* pDigitalOut, unsigned uiValue, bool bInverted = false);
	void Deinit();
public: // ITimerChannel
	virtual void SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0);
	virtual void Enable(bool bEnable = true);
	virtual void SetOutputInvertion(bool bInvert);
	virtual unsigned GetTimerPeriod() const;
	virtual unsigned GetTimerCounterMax() const;
public: // ITimerChannel::IValueSource<unsigned>
	virtual unsigned GetValue() const;
public: // ITimerChannel::IValueReceiver<unsigned>
	virtual void SetValue(unsigned uiValue);
public:
	bool IsConfigured() const {return m_pDigitalOut || m_pNotifier;}
	void OnCounter(unsigned uiCounter);
private:
	void UpdateOutput(bool bState);
	unsigned               m_nChannel;
	CSoftPWMTimerProcess*  m_pParentSoftTimer;
	IDigitalOut*           m_pDigitalOut;
	ITimerChannelNotifier* m_pNotifier;
	unsigned               m_uiValue;
	bool                   m_bInverted;
	bool                   m_bEnabled;
};

class CSoftPWMTimerProcess :
	public ITimerSoft,
	public CTickProcess
{
public:
//	virtual ~CSoftPWMProcess();
	void Init();
	void Deinit();
	void AssignChannelDOut(unsigned uiChannel, IDigitalOut* pDigitalOut);

public: // ITimerSoft
	virtual ITimerChannel* AcquireChannelInterface(IDigitalOut* pDigitalOut, unsigned uiValue, bool bInverted);
	virtual void ReleaseChannelInterface(ITimerChannel*pChannel);
public: // ITimerSoft::ITimer
	virtual void SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority = 0, unsigned uiSubPriority = 0);
	virtual void SetPeriodClks(uint64_t ullPeriodClks);
	virtual void SetPrescaler(unsigned ulPrescaler);
	virtual void SetPeriod(unsigned ulPeriod);
	virtual void SetCounter(unsigned uiCounter);
	virtual unsigned GetCounter() const;
	virtual unsigned GetCounterMax() const;
	virtual unsigned GetPeriod() const;
	virtual unsigned ClksToCounts(unsigned uiClks) const;
	virtual void Start();
	virtual void Stop();
	virtual void Idle();
	virtual void Work();

public: // CTickProcess::TProcess<CTickTimeSource>
	virtual void SingleStep(CTimeType uiTime);

private:
	ITimerNotifier* m_pNotifier;
	CTimeType       m_uiLastTime;
	unsigned        m_uiClksPerTick;
	unsigned        m_uiPeriod;
	unsigned        m_uiCounter;
	CSoftPWMTimerChannel m_aChannels[SOFT_PWM_CHANNELS];
};

void CSoftPWMTimerChannel::Init(unsigned nChannel, CSoftPWMTimerProcess* pParentSoftTimer, IDigitalOut* pDigitalOut, unsigned uiValue, bool bInverted)
{
	m_nChannel = nChannel;
	m_pParentSoftTimer = pParentSoftTimer;
	m_pDigitalOut = pDigitalOut;
	m_pNotifier = NULL;
	m_uiValue = uiValue;
	m_bInverted = bInverted;
	m_bEnabled = true;
}

void CSoftPWMTimerChannel::Deinit()
{
	m_pParentSoftTimer = NULL;
	m_pDigitalOut = NULL;
	m_pNotifier = NULL;
}

/*virtual*/
void CSoftPWMTimerChannel::SetNotifier(ITimerChannelNotifier* pNotifier, unsigned uiGroupPriority /*= 0*/, unsigned uiSubPriority /*= 0*/)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority));
	m_pNotifier = pNotifier;
}

/*virtual*/
void CSoftPWMTimerChannel::Enable(bool bEnable /*= true*/)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::Enable(bEnable));
	m_bEnabled = bEnable;
	if (!bEnable) {
		UpdateOutput(false);
	} else {
		UpdateOutput(m_pParentSoftTimer->GetCounter() <= m_uiValue);
	}
}

/*virtual*/
void CSoftPWMTimerChannel::SetOutputInvertion(bool bInvert)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::SetOutputInvertion(bInvert));
	m_bInverted = bInvert;
	if (m_bEnabled) {
		UpdateOutput(m_pParentSoftTimer->GetCounter() <= m_uiValue);
	}
}

/*virtual*/
unsigned CSoftPWMTimerChannel::GetValue() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::IValueSource<unsigned>::GetValue());
	return m_uiValue;
}

/*virtual*/
void CSoftPWMTimerChannel::SetValue(unsigned uiValue)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::IValueReceiver<unsigned>::SetValue(uiValue));
	m_uiValue = uiValue;
}

/*virtual*/
unsigned CSoftPWMTimerChannel::GetTimerPeriod() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::GetTimerPeriod());
	return m_pParentSoftTimer->GetPeriod();
}

/*virtual*/
unsigned CSoftPWMTimerChannel::GetTimerCounterMax() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerChannel::GetTimerCounterMax());
	return m_pParentSoftTimer->GetCounterMax();
}

void CSoftPWMTimerChannel::OnCounter(unsigned uiCounter)
{
	if (!m_bEnabled) return;

	if (m_pNotifier && uiCounter == m_uiValue) {
		m_pNotifier->NotifyTimerChannel(m_nChannel);
	}
	UpdateOutput(uiCounter <= m_uiValue);
}

void CSoftPWMTimerChannel::UpdateOutput(bool bValue)
{
	if (m_pDigitalOut) {
		m_pDigitalOut->SetValue(m_bInverted != bValue);
	}
}

void CSoftPWMTimerProcess::Init()
{
	CTickProcess::Init(&g_TickProcessScheduller);
	m_pNotifier = NULL;
	m_uiLastTime = 0;
	m_uiClksPerTick = CHW_Clocks::GetCLKFrequency() / CHW_Clocks::GetSysTickFrequency();
	m_uiPeriod = 1;
	m_uiCounter = 0;
}

void CSoftPWMTimerProcess::Deinit()
{
	Cancel();
}

/*virtual*/
ITimerChannel* CSoftPWMTimerProcess::AcquireChannelInterface(IDigitalOut* pDigitalOut, unsigned uiValue, bool bInverted)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::AcquireChannelInterface(pDigitalOut, uiValue, bInverted));
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		CSoftPWMTimerChannel* pChannel = &m_aChannels[n];
		if (!pChannel->IsConfigured()) {
			pChannel->Init(n, this, pDigitalOut, uiValue, bInverted);
			return pChannel;
		}
	}
	return NULL;
}

/*virtual*/
void CSoftPWMTimerProcess::ReleaseChannelInterface(ITimerChannel* pTimerChannel)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ReleaseChannelInterface(pTimerChannel));
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		CSoftPWMTimerChannel* pChannel = &m_aChannels[n];
		if (pTimerChannel == pChannel) {
			ASSERTE(pChannel->IsConfigured());
			pChannel->Deinit();
			return;
		}
	}
	ASSERT("invalid channel");
}

/*virtual*/
void CSoftPWMTimerProcess::SetNotifier(ITimerNotifier* pNotifier, unsigned uiGroupPriority, unsigned uiSubPriority)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::SetNotifier(pNotifier, uiGroupPriority, uiSubPriority));
	m_pNotifier = pNotifier;
}

/*virtual*/
void CSoftPWMTimerProcess::SetPeriodClks(uint64_t ullPeriodClks)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::SetPeriodClks(ullPeriodClks));
	m_uiPeriod = ullPeriodClks / m_uiClksPerTick;
}
/*virtual*/
void CSoftPWMTimerProcess::SetPrescaler(unsigned ulPrescaler)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::SetPrescaler(ulPrescaler));
	// TODO ?
}
/*virtual*/
void CSoftPWMTimerProcess::SetPeriod(unsigned ulPeriod)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::SetPeriod(ulPeriod));
	m_uiPeriod = ulPeriod;
}

/*virtual*/
void CSoftPWMTimerProcess::SetCounter(unsigned uiCounter)
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::SetCounter(uiCounter));
	m_uiCounter = uiCounter;
}
/*virtual*/
unsigned CSoftPWMTimerProcess::GetCounter() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::GetCounter());
	return m_uiCounter;
}
/*virtual*/
unsigned CSoftPWMTimerProcess::GetCounterMax() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::GetCounterMax());
	return 0xFFFF;
}
/*virtual*/
unsigned CSoftPWMTimerProcess::GetPeriod() const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::GetPeriod());
	return m_uiPeriod;
}
/*virtual*/
unsigned CSoftPWMTimerProcess::ClksToCounts(unsigned uiClks) const
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::ClksToCounts(uiClks));
	return uiClks / m_uiClksPerTick;
}

/*virtual*/
void CSoftPWMTimerProcess::Start()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::Start());
	m_uiLastTime = GetTimeNow();
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		m_aChannels[n].Enable(true);
	}
	ContinueAt(m_uiLastTime + 1);
}

/*virtual*/
void CSoftPWMTimerProcess::Stop()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::Stop());
	Cancel();
}

/*virtual*/
void CSoftPWMTimerProcess::Idle()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::Idle());
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		m_aChannels[n].Enable(false);
	}
}

/*virtual*/
void CSoftPWMTimerProcess::Work()
{
	IMPLEMENTS_INTERFACE_METHOD(ITimerSoft::ITimer::Work());
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		m_aChannels[n].Enable(true);
	}
}


void CSoftPWMTimerProcess::SingleStep(CTimeType uiTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(uiTime));
//	TriacPWM.ProcessTimers();
	CTimeDelta dt = uiTime - m_uiLastTime;
	if (dt > (CTimeDelta)m_uiPeriod) dt = dt % m_uiPeriod;
	unsigned uiCounter = (m_uiCounter > (unsigned)dt) ? (m_uiCounter - dt) : (m_uiCounter + m_uiPeriod - dt);
	for (unsigned n = 0; n < ARRAY_SIZE(m_aChannels); ++n) {
		m_aChannels[n].OnCounter(uiCounter);
	}
	m_uiCounter = uiCounter;
	m_uiLastTime = uiTime;
	ContinueAt(uiTime + 1);
}

CSoftPWMTimerProcess g_SoftPwmTimer;
ITimerSoft* AcquireSoftPwmTimer() {g_SoftPwmTimer.Init(); return &g_SoftPwmTimer;}
void ReleaseSoftPwmTimer() {g_SoftPwmTimer.Deinit();}
