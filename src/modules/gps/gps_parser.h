#ifndef GPS_NMEA_H_INCLUDED
#define GPS_NMEA_H_INCLUDED

#include "gps.h"

#define GPSMSGMAXPARAMS 20
#define GPSUBXMAXPKT 2048

class CGpsParser:
		public CGpsInfo
{
public:
	typedef enum {PR_CONTINUE, PR_DONE, PR_ERROR} EParseResult;
public:
	void Init();

protected:
	EParseResult Parse(char ch);
	void ParserReset();
	bool ScheduleResponce(const void* pData, unsigned nDataLength);
	bool GetScheduledResponce(const uint8_t** pRetData, unsigned *pRetDataLength) const;
	void DoneScheduledResponce();

private: // NMEA
	void ParserNMEAReset();
	void ParserNMEAStart();
	EParseResult ParseNMEA(char ch);
	void ProcessNMEAMsg();
	void ProcessGGA(ESatSystem eTalker, double dFixUtcTime, double dLat, char cLatNS, double dLon, char cLonEW, unsigned uiFixQuality, unsigned nSatelites, double dHorDilution, double dAntHeight, double dGeoHeight, unsigned uiDiffAge, unsigned uiDiffId);
	void ProcessGLL(ESatSystem eTalker, double dLat, char cLatNS, double dLon, char cLonEW, double dFixUtcTime, char cStatusVA, char cModeNADE);
	void ProcessGSA(ESatSystem eTalker, char cModeMA, unsigned nFixType, unsigned auiSatID[12], double dPosDilution, double dHorDilution, double dVerDilution);
	void ProcessGSV(ESatSystem eTalker, unsigned nMsgCount, unsigned nMsgIndex, unsigned nSatInView, unsigned auiSatId[4], unsigned auiSatElev[4], unsigned auiSatAzim[4], unsigned auiCNdB[4]);
	void ProcessRMC(ESatSystem eTalker, double dFixUtcTime, char cStatusVA, double dLat, char cLatNS, double dLon, char cLonEW, double dSpeedKnots, double dCourse, unsigned uiFixUtcDate, double dMagnetic, char cMagneticDirectionEW, char cModeNADE);
	void ProcessVTG(ESatSystem eTalker, double dTrueCourse, double dMagCourse, double dSpeedKnots, char cModeNADE);
	void ProcessZDA(ESatSystem eTalker, double dUtcTime, unsigned uiUtcDateDay, unsigned uiUtcDateMonth, unsigned uiUtcDateYear);


private:
	CTimeType       m_ttPacketStartTime;
	typedef enum {
		PT_UNKNOWN,
		PT_GGA, // GPS FIX DATA
		PT_GLL, // LATITUDE AND LONGITUDE, WITH TIME OF POSITION FIX AND STATUS
		PT_GSA, // GPS DOP AND ACTIVE SATELLITES
		PT_GSV, // GPS SATELLITE IN VIEW
		PT_RMC, // RECOMMANDED MINIMUM SPECIFIC GPS/TRANSIT DATA
		PT_VTG, // COURSE OVER GROUND AND GROUND SPEED
		PT_ZDA, // TIME AND DATE
	} ENMEAPacketType;
	enum {
		GP_NMEA,
		GP_UBX
	} m_eGPSProtocol;
	const void*     m_pRequestData;
	unsigned        m_nRequestDataLength;

private:
	int             m_eNMEAState;

	ESatSystem      m_eNMEAPacketTalker;
	ENMEAPacketType m_eNMEAPacketType;
	unsigned        m_nNMEAParamsCount;
	typedef struct {
		char        cChar;
		unsigned    uiMain;
		unsigned    uiMainLength;
		unsigned    uiFract;
		unsigned    uiFractLength;

		void Zero() {cChar = 0; uiMain = uiMainLength = uiFract = uiFractLength = 0;}
		void Accumulate(char ch) {uiMain *= 10; uiMain += ch - '0'; ++uiMainLength;}
		void AccumulateFract(char ch) {uiFract *= 10; uiFract += ch - '0'; ++uiFractLength;}
		double GetFract() const {double f = uiFract; for (unsigned n = uiFractLength; n; --n) {f /= 10;} return f;}
		double GetDouble() const {return (cChar == '-') ? (- GetFract() - uiMain) : (GetFract() + uiMain);}
		int    GetInt() const {return (cChar == '-') ? (-uiMain) : (uiMain);}
	} TData;
	TData    m_aNMEAParamsData[GPSMSGMAXPARAMS];
	uint8_t  m_uiNMEAMsgCrc;


private:
	void ParserUBXReset();
	EParseResult ParseUBX(char ch);
	void ProcessUBXMsg();

private: // UBX
	int         m_eUBXPktParserState;

	uint16_t    m_nUBXPktPayloadLengthReceived;
	uint8_t     m_uiUBXPktCrcA;
	uint8_t     m_uiUBXPktCrcB;
	int64_t     m__pad;
	uint8_t     m_aUBXPkt[GPSUBXMAXPKT];

	unsigned    m_nUBXConfigStep;
	bool        m_bUBXGotVersion;
};

#endif // GPS_NMEA_H_INCLUDED
