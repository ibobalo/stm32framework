#include "stdafx.h"
#include <stdint.h>
#include <math.h>

#include "gps_parser.h"
#include "util/calendar.h"

//#define GPS_DEBUG_INFO(...) DEBUG_INFO("GPS:" __VA_ARGS__)
#define GPS_DEBUG_INFO(...) {}


typedef enum {
	NMEA_START,   // $
	NMEA_TALKER,
	NMEA_TALKER_G,
	NMEA_TYPE,
	NMEA_TYPE_G,
		NMEA_TYPE_GG,
			NMEA_TYPE_GGA,
		NMEA_TYPE_GL,
			NMEA_TYPE_GLL,
		NMEA_TYPE_GS,
			NMEA_TYPE_GSA,
			NMEA_TYPE_GSV,
	NMEA_TYPE_R,
		NMEA_TYPE_RM,
			NMEA_TYPE_RMC,
	NMEA_TYPE_V,
		NMEA_TYPE_VT,
			NMEA_TYPE_VTG,
	NMEA_TYPE_Z,
		NMEA_TYPE_ZD,
			NMEA_TYPE_ZDA,
	NMEA_DATA_MAIN, // data - main part
	NMEA_DATA_FRAC, // data - fractional part
	NMEA_CRC_LO,  //
	NMEA_CRC_HI,  //
	NMEA_TAIL_R,  //
	NMEA_TAIL_N,  //
} ENMEAParserState;

void CGpsParser::ParserNMEAReset()
{
	m_eNMEAState = NMEA_START;
	m_eNMEAPacketTalker = PS_UNKNOWN;
	m_eNMEAPacketType = PT_UNKNOWN;
	m_nNMEAParamsCount = 0;
	m_aNMEAParamsData[0].Zero();
	m_uiNMEAMsgCrc = 0;
}

void CGpsParser::ParserNMEAStart()
{
	ParserReset();
	m_ttPacketStartTime = g_TickProcessScheduller.GetTimeNow();
	m_eNMEAState = NMEA_TALKER;
	GPS_DEBUG_INFO("message start");
}

CGpsParser::EParseResult CGpsParser::ParseNMEA(char ch)
{
	EParseResult eResult = PR_CONTINUE;

	switch (m_eNMEAState) {
		case NMEA_START:
			if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TALKER:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'G') {m_eNMEAState = NMEA_TALKER_G;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TALKER_G:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'P') {m_eNMEAState = NMEA_TYPE; m_eNMEAPacketTalker = PS_GP;}
			else if (ch == 'A') {m_eNMEAState = NMEA_TYPE; m_eNMEAPacketTalker = PS_GA;}
			else if (ch == 'L') {m_eNMEAState = NMEA_TYPE; m_eNMEAPacketTalker = PS_GL;}
			else if (ch == 'B') {m_eNMEAState = NMEA_TYPE; m_eNMEAPacketTalker = PS_GB;}
			else if (ch == 'N') {m_eNMEAState = NMEA_TYPE; m_eNMEAPacketTalker = PS_GN;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'G') {m_eNMEAState = NMEA_TYPE_G;}
			else if (ch == 'R') {m_eNMEAState = NMEA_TYPE_R;}
			else if (ch == 'V') {m_eNMEAState = NMEA_TYPE_V;}
			else if (ch == 'Z') {m_eNMEAState = NMEA_TYPE_Z;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_G:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'G') {m_eNMEAState = NMEA_TYPE_GG;}
			else if (ch == 'L') {m_eNMEAState = NMEA_TYPE_GL;}
			else if (ch == 'S') {m_eNMEAState = NMEA_TYPE_GS;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GG:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'A') {m_eNMEAState = NMEA_TYPE_GGA;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GGA:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_GGA;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GL:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'L') {m_eNMEAState = NMEA_TYPE_GLL;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GLL:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_GLL;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GS:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'A') {m_eNMEAState = NMEA_TYPE_GSA;}
			else if (ch == 'V') {m_eNMEAState = NMEA_TYPE_GSV;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GSA:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_GSA;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_GSV:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_GSV;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_R:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'M') {m_eNMEAState = NMEA_TYPE_RM;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_RM:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'C') {m_eNMEAState = NMEA_TYPE_RMC;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_RMC:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_RMC;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_V:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'T') {m_eNMEAState = NMEA_TYPE_VT;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_VT:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'G') {m_eNMEAState = NMEA_TYPE_VTG;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_VTG:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_VTG;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_Z:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'D') {m_eNMEAState = NMEA_TYPE_ZD;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_ZD:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == 'A') {m_eNMEAState = NMEA_TYPE_ZDA;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TYPE_ZDA:
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; m_eNMEAPacketType = PT_ZDA;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_DATA_MAIN: // argument - main part
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch >= '0' && ch <= '9') {m_aNMEAParamsData[m_nNMEAParamsCount].Accumulate(ch);}
			else if (ch == '-') {m_aNMEAParamsData[m_nNMEAParamsCount].cChar = ch;}
			else if (ch == '.') {m_eNMEAState = NMEA_DATA_FRAC;}
			else if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; ++m_nNMEAParamsCount; if (m_nNMEAParamsCount < ARRAY_SIZE(m_aNMEAParamsData)) {m_aNMEAParamsData[m_nNMEAParamsCount].Zero();} else {ParserReset();}}
			else if (ch == '*') {m_eNMEAState = NMEA_CRC_HI; ++m_nNMEAParamsCount; m_uiNMEAMsgCrc ^= (uint8_t)ch;}
			else if (ch == '$') {ParserNMEAStart();}
			else if (m_aNMEAParamsData[m_nNMEAParamsCount].uiMainLength == 0 && !m_aNMEAParamsData[m_nNMEAParamsCount].cChar) {m_aNMEAParamsData[m_nNMEAParamsCount].cChar = ch;}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_DATA_FRAC: // argument - fractional part
			m_uiNMEAMsgCrc ^= (uint8_t)ch;
			if (ch >= '0' && ch <= '9') {m_aNMEAParamsData[m_nNMEAParamsCount].AccumulateFract(ch);}
			else if (ch == ',') {m_eNMEAState = NMEA_DATA_MAIN; ++m_nNMEAParamsCount; if (m_nNMEAParamsCount < ARRAY_SIZE(m_aNMEAParamsData)) {m_aNMEAParamsData[m_nNMEAParamsCount].Zero();} else {ParserReset();}}
			else if (ch == '*') {m_eNMEAState = NMEA_CRC_HI; ++m_nNMEAParamsCount; m_uiNMEAMsgCrc ^= (uint8_t)ch;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_CRC_HI:  //
			if (ch >= '0' && ch <= '9') {m_eNMEAState = NMEA_CRC_LO; m_uiNMEAMsgCrc -= (ch - '0') << 4;}
			else if (ch >= 'A' && ch <= 'F') {m_eNMEAState = NMEA_CRC_LO; m_uiNMEAMsgCrc -= (ch - 'A' + 10) << 4;}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_CRC_LO:  //
			if (ch >= '0' && ch <= '9') {m_eNMEAState = NMEA_TAIL_R; m_uiNMEAMsgCrc -= (ch - '0');}
			else if (ch >= 'A' && ch <= 'F') {m_eNMEAState = NMEA_TAIL_R; m_uiNMEAMsgCrc -= (ch - 'A' + 10);}
			break;
		case NMEA_TAIL_R:  //
			if (ch == '\r') {m_eNMEAState = NMEA_TAIL_N;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
		case NMEA_TAIL_N:  //
			if (ch == '\n') {if (m_uiNMEAMsgCrc == 0) {ProcessNMEAMsg();} eResult = PR_DONE;}
			else if (ch == '$') {ParserNMEAStart();}
			else {eResult = PR_ERROR;}
			break;
	}
	if (eResult == PR_ERROR) {
		if (m_eNMEAState != NMEA_START) {
			GPS_DEBUG_INFO("Parse error:", ch);
			GPS_DEBUG_INFO("Parse error state:", m_eNMEAState);
		}
		ParserReset();
//	} else {
//		GPS_DEBUG_INFO("Parse ok:", ch);
	}
	return eResult;
}

void CGpsParser::ProcessNMEAMsg()
{
	m_ttLastPktTime = m_ttPacketStartTime;
	switch (m_eNMEAPacketType) {
		case PT_GGA: // GPS FIX DATA
			GPS_DEBUG_INFO("GPGGA:", m_nNMEAParamsCount);
			ProcessGGA(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].GetDouble() : 0.0, // dFixUtcTime
					(m_nNMEAParamsCount > 1) ? m_aNMEAParamsData[1].GetDouble() : 0.0, // dLat,
					(m_nNMEAParamsCount > 2) ? m_aNMEAParamsData[2].cChar : '\0',      // cLatNS,
					(m_nNMEAParamsCount > 3) ? m_aNMEAParamsData[3].GetDouble() : 0.0, // dLon,
					(m_nNMEAParamsCount > 4) ? m_aNMEAParamsData[4].cChar : '\0',      // cLonEW,
					(m_nNMEAParamsCount > 5) ? m_aNMEAParamsData[5].uiMain : 0,        // uiFixQuality,
					(m_nNMEAParamsCount > 6) ? m_aNMEAParamsData[6].uiMain : 0,        // nSatelites,
					(m_nNMEAParamsCount > 7) ? m_aNMEAParamsData[7].GetDouble() : 0.0, // dHorDilusion,
					(m_nNMEAParamsCount > 8) ? m_aNMEAParamsData[8].GetDouble() : 0.0, // dAntHeight,
					(m_nNMEAParamsCount > 10) ? m_aNMEAParamsData[10].GetDouble() : 0.0, // dGeoHeight,
					(m_nNMEAParamsCount > 12) ? m_aNMEAParamsData[12].uiMain : 0,        // uiDiffAge,
					(m_nNMEAParamsCount > 13) ? m_aNMEAParamsData[13].uiMain : 0       // uiDiffId;
			);
			break;
		case PT_GLL: // LATITUDE AND LONGITUDE, WITH TIME OF POSITION FIX AND STATUS
			GPS_DEBUG_INFO("GPLL:", m_nNMEAParamsCount);
			ProcessGLL(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].GetDouble() : 0.0, // dLat,
					(m_nNMEAParamsCount > 1) ? m_aNMEAParamsData[1].cChar : '\0',      // cLatNS,
					(m_nNMEAParamsCount > 2) ? m_aNMEAParamsData[2].GetDouble() : 0.0, // dLon,
					(m_nNMEAParamsCount > 3) ? m_aNMEAParamsData[3].cChar : '\0',      // cLonEW,
					(m_nNMEAParamsCount > 4) ? m_aNMEAParamsData[4].GetDouble() : 0.0, // dFixUtcTime
					(m_nNMEAParamsCount > 5) ? m_aNMEAParamsData[5].cChar : '\0',      // cStatusVA,
					(m_nNMEAParamsCount > 6) ? m_aNMEAParamsData[6].cChar : '\0'       // cModeNADE
			);
			break;
		case PT_GSA: { // GPS DOP AND ACTIVE SATELLITES
			GPS_DEBUG_INFO("GPGSA:", m_nNMEAParamsCount);
			unsigned auiSatID[12];
			for (unsigned n = 0; n < 12; ++n) {
				auiSatID[n] = (m_nNMEAParamsCount > 2 + n) ? m_aNMEAParamsData[2 + n].uiMain : 0;
			};
			ProcessGSA(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].cChar : '\0',        // cModeMA
					(m_nNMEAParamsCount > 1) ? m_aNMEAParamsData[1].uiMain : 0,          // nFixType,
					auiSatID,                                            // uiSatID1[12],
					(m_nNMEAParamsCount > 14) ? m_aNMEAParamsData[14].GetDouble() : 0.0, // dPosDilution,
					(m_nNMEAParamsCount > 15) ? m_aNMEAParamsData[15].GetDouble() : 0.0, // dHorDilution,
					(m_nNMEAParamsCount > 16) ? m_aNMEAParamsData[16].GetDouble() : 0.0  // double dVerDilution
			);
			} break;
		case PT_GSV: { // GPS SATELLITE IN VIEW
			GPS_DEBUG_INFO("GPGSV:", m_nNMEAParamsCount);
			unsigned auiSatId[4], auiSatElev[4], auiSatAzim[4], auiCNdB[4];
			for (unsigned n = 0; n < 4; ++n) {
				unsigned n4 = n*4;
				auiSatId[n]   = (m_nNMEAParamsCount > 3 + n4) ? m_aNMEAParamsData[3 + n4].uiMain : 0;
				auiSatElev[n] = (m_nNMEAParamsCount > 4 + n4) ? m_aNMEAParamsData[4 + n4].uiMain : 0;
				auiSatAzim[n] = (m_nNMEAParamsCount > 5 + n4) ? m_aNMEAParamsData[5 + n4].uiMain : 0;
				auiCNdB[n]    = (m_nNMEAParamsCount > 6 + n4) ? m_aNMEAParamsData[6 + n4].uiMain : 0;
			}
			ProcessGSV(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].uiMain : 0,        // nMsgCount,
					(m_nNMEAParamsCount > 1) ? m_aNMEAParamsData[1].uiMain : 0,        // nMsgIndex,
					(m_nNMEAParamsCount > 2) ? m_aNMEAParamsData[2].uiMain : 0,        // nSatInView,
					auiSatId,                                          // auiSatId[4],
					auiSatElev,                                        // auiSatElev[4],
					auiSatAzim,                                        // aiSatAzim[4],
					auiCNdB                                            // auiCNdB1[4]
			);
			} break;
		case PT_RMC: // RECOMMENDED MINIMUM SPECIFIC GPS/TRANSIT DATA
			GPS_DEBUG_INFO("GPRMC:", m_nNMEAParamsCount);
			ProcessRMC(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].GetDouble() : 0.0, // dFixUtcTime
					(m_nNMEAParamsCount > 1) ? m_aNMEAParamsData[1].cChar : '\0',      // cStatusVA,
					(m_nNMEAParamsCount > 2) ? m_aNMEAParamsData[2].GetDouble() : 0.0, // dLat,
					(m_nNMEAParamsCount > 3) ? m_aNMEAParamsData[3].cChar : '\0',      // cLatNS,
					(m_nNMEAParamsCount > 4) ? m_aNMEAParamsData[4].GetDouble() : 0.0, // dLon,
					(m_nNMEAParamsCount > 5) ? m_aNMEAParamsData[5].cChar : '\0',      // cLonEW,
					(m_nNMEAParamsCount > 6) ? m_aNMEAParamsData[6].GetDouble() : 0.0, // dSpeedKnots,
					(m_nNMEAParamsCount > 7) ? m_aNMEAParamsData[7].GetDouble() : 0.0, // dCourse,
					(m_nNMEAParamsCount > 8) ? m_aNMEAParamsData[8].uiMain : 0,        // uiFixUtcDate,
					(m_nNMEAParamsCount > 9) ? m_aNMEAParamsData[9].GetDouble() : 0.0, // dMagnetic,
					(m_nNMEAParamsCount > 10) ? m_aNMEAParamsData[10].cChar : '\0',    // cMagneticDirectionEW,
					(m_nNMEAParamsCount > 11) ? m_aNMEAParamsData[11].cChar : '\0'     // cModeNADE
			);
			break;
		case PT_VTG: // COURSE OVER GROUND AND GROUND SPEED
			GPS_DEBUG_INFO("GPVTG:", m_nNMEAParamsCount);
			ProcessVTG(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].GetDouble() : 0.0, // dTrueCourse,
					(m_nNMEAParamsCount > 2) ? m_aNMEAParamsData[2].GetDouble() : 0.0, // dMagCourse,
					(m_nNMEAParamsCount > 4) ? m_aNMEAParamsData[4].GetDouble() : 0.0, // dSpeedKnots,
					(m_nNMEAParamsCount > 7) ? m_aNMEAParamsData[7].cChar : '\0'       // cModeNADE
			);
			break;
		case PT_ZDA: // TIME AND DATE
			GPS_DEBUG_INFO("GPZDA:", m_nNMEAParamsCount);
			ProcessZDA(
					m_eNMEAPacketTalker,
					(m_nNMEAParamsCount > 0) ? m_aNMEAParamsData[0].GetDouble() : 0.0, // dUtcTime,
					(m_nNMEAParamsCount > 1) ? m_aNMEAParamsData[1].GetInt() : 0,      // uiUtcDateDay,
					(m_nNMEAParamsCount > 2) ? m_aNMEAParamsData[2].GetInt() : 0,      // uiUtcDateMonth,
					(m_nNMEAParamsCount > 3) ? m_aNMEAParamsData[3].GetInt() : 0       // uiUtcDateYear
			);
			break;
		default:
			ASSERT("INVALID PKT TYPE");
	}
	NotifyPacket();
}

#define SET(tgt, val, changed) { \
	__typeof__ (tgt) _val = (val); \
	if (tgt != _val) {tgt = _val; changed = true;} \
}

#define PREPARE_SETS \
		bool bFixedChanged = false; \
		bool bTimeChanged = false; \
		bool bPosChanged = false; \
		bool bSatChanged = false;
#define SET_FIX(tgt, val) SET(tgt, val, bFixedChanged)
#define SET_TIME(tgt, val) SET(tgt, val, bTimeChanged)
#define SET_POS(tgt, val) SET(tgt, val, bPosChanged)
#define SET_SAT(tgt, val) SET(tgt, val, bSatChanged)
#define NOTIFY_CHANGES \
		if (bFixedChanged) {NotifyFixed();} \
		if (bTimeChanged) {NotifyTimeUpdate();} \
		if (bPosChanged) {NotifyPosUpdate();} \
		if (bSatChanged) {NotifySatUpdate();}

void CGpsParser::ProcessGGA(
	ESatSystem eTalker,
	double dFixUtcTime,
	double dLat, char cLatNS,
	double dLon, char cLonEW,
	unsigned uiFixQuality, unsigned nSatelites, double dHorDilution,
	double dAntHeight, double dGeoHeight,
	unsigned uiDiffAge, unsigned uiDiffId
) {
	PREPARE_SETS
	SET_FIX(m_bFixed, (uiFixQuality >= 1));
	if (m_bFixed) {
		SET_POS(m_uiFixTimeStamp, dFixUtcTime);
		SET_SAT(m_nSatsUsed, nSatelites)
		SET_POS(m_dLLAPosLat, DEGTORAD(DDmm2DD((cLatNS == 'N') ? dLat : -dLat)));
		SET_POS(m_dLLAPosLon, DEGTORAD(DDmm2DD((cLonEW == 'E') ? dLon : -dLon)));
		SET_POS(m_dLLAPosDilution, dHorDilution);
		SET_POS(m_dLLAHeight, dAntHeight);
		m_ttLLAPosUpdateTime = m_ttPacketStartTime;
		m_ttLLAHeightUpdateTime = m_ttPacketStartTime;
	}
	NOTIFY_CHANGES
}

void CGpsParser::ProcessGLL(
		ESatSystem eTalker,
		double dLat, char cLatNS,
		double dLon, char cLonEW,
		double dFixUtcTime,
		char cStatusVA, char cModeNADE
) {
	PREPARE_SETS
	SET_FIX(m_bFixed, (cStatusVA == 'A' && cModeNADE != 'N'));
	if (m_bFixed) {
		SET_POS(m_uiFixTimeStamp, dFixUtcTime);
		SET_POS(m_dLLAPosLat, DEGTORAD(DDmm2DD((cLatNS == 'N') ? dLat : -dLat)));
		SET_POS(m_dLLAPosLon, DEGTORAD(DDmm2DD((cLonEW == 'E') ? dLon : -dLon)));
	}
	NOTIFY_CHANGES
}

void CGpsParser::ProcessGSA(
	ESatSystem eTalker,
	char cModeMA, unsigned nFixType,
	unsigned auiSatID[12],
	double dPosDilution, double dHorDilution, double dVerDilution)
{
	GPS_DEBUG_INFO("GSA:", eTalker);
	PREPARE_SETS
	if (nFixType < 2) {
		SET_FIX(m_bFixed, false);
	}
	SET_POS(m_dLLAPosDilution, dPosDilution);
	SET_POS(m_dLLAHorDilution, dHorDilution);
	SET_POS(m_dLLAVerDilution, dVerDilution);
	for (unsigned n = 0; n < 12; ++n) {
		SET_SAT(m_auiSatID[n], auiSatID[n]);
	}
	NOTIFY_CHANGES
}

void CGpsParser::ProcessGSV(
	ESatSystem eTalker,
	unsigned nMsgCount, unsigned nMsgIndex, unsigned nSatInView,
	unsigned auiSatId[4], unsigned auiSatElev[4], unsigned auiSatAzim[4], unsigned auiCNdB[4]
) {
	GPS_DEBUG_INFO("GSV:", eTalker);
	PREPARE_SETS
	for (unsigned n = 0; n < 4; ++n) {
		unsigned nSatId = auiSatId[n];
		unsigned nSatIdx = nSatId - 1;
		GPS_DEBUG_INFO("GSV SatID:", nSatId);
		if (auiSatId && nSatIdx < ARRAY_SIZE(m_aSatInfos)) {
			SET_SAT(m_aSatInfos[nSatIdx].eSystem,      eTalker);
			SET_SAT(m_aSatInfos[nSatIdx].uiElev,       auiSatElev[n]);
			SET_SAT(m_aSatInfos[nSatIdx].uiAzim,       auiSatAzim[n]);
			SET_SAT(m_aSatInfos[nSatIdx].uiCNdB,       auiCNdB[n]);
			SET_SAT(m_aSatInfos[nSatIdx].ttUpdateTime, m_ttPacketStartTime);
		}
	}
	unsigned nSatsInView = 0;
	for (unsigned n = 0; n < ARRAY_SIZE(m_aSatInfos); ++n) {
		if (m_aSatInfos[n].eSystem != PS_UNKNOWN) {
			CTimeDelta dt = m_ttPacketStartTime - m_aSatInfos[n].ttUpdateTime;
			if (dt < 3000) {
				++nSatsInView;
			} else {
				m_aSatInfos[n].eSystem = PS_UNKNOWN;
			}
		}
	}
	SET_SAT(m_nSatsInView, nSatsInView);
	NOTIFY_CHANGES
}

void CGpsParser::ProcessRMC(
	ESatSystem eTalker,
	double dFixUtcTime, char cStatusVA,
	double dLat, char cLatNS,
	double dLon, char cLonEW,
	double dSpeedKnots, double dCourse,
	unsigned uiFixUtcDate,
	double dMagnetic, char cMagneticDirectionEW,
	char cModeNADE
) {
	GPS_DEBUG_INFO("RMC:", eTalker);
	PREPARE_SETS
	SET_FIX(m_bFixed, (cStatusVA == 'A' && cModeNADE != 'N'));
	if (m_bFixed) {
		SET_POS(m_uiFixTimeStamp, dFixUtcTime);
		SET_POS(m_dLLAPosLat, DEGTORAD(DDmm2DD((cLatNS == 'N') ? dLat : -dLat)));
		SET_POS(m_dLLAPosLon, DEGTORAD(DDmm2DD((cLonEW == 'E') ? dLon : -dLon)));
		m_ttLLAPosUpdateTime = m_ttPacketStartTime;
		SET_POS(m_dLLASpeed, KNOTSTOMPERS(dSpeedKnots));
		SET_POS(m_dLLACourse, DEGTORAD(dCourse));
		m_ttLLASpeedUpdateTime = m_ttPacketStartTime;
		SET_POS(m_uiFixDateStamp, uiFixUtcDate);
	}
	NOTIFY_CHANGES
}

void CGpsParser::ProcessVTG(
	ESatSystem eTalker,
	double dTrueCourse,
	double dMagCourse,
	double dSpeedKnots,
	char cModeNADE
) {
	GPS_DEBUG_INFO("VTG:", eTalker);
	PREPARE_SETS
	if (cModeNADE == 'N') {
		SET_FIX(m_bFixed, false);
	}
	if (m_bFixed) {
		SET_POS(m_dLLASpeed, KNOTSTOMPERS(dSpeedKnots));
		SET_POS(m_dLLACourse, DEGTORAD(dTrueCourse));
		m_ttLLASpeedUpdateTime = m_ttPacketStartTime;
	}
	NOTIFY_CHANGES
}

void CGpsParser::ProcessZDA(
		ESatSystem eTalker,
		double dUtcTime,
		unsigned uiUtcDateDay,
		unsigned uiUtcDateMonth,
		unsigned uiUtcDateYear
) {
	unsigned t = dUtcTime;
	unsigned s = t % 100;
	unsigned m = (t / 100) % 100;
	unsigned h = (t / 10000) % 100;
	if (s > 59) return;
	if (m > 59) return;
	if (h > 23) return;
	if (uiUtcDateDay <= 0 || uiUtcDateDay > 31) return;
	if (uiUtcDateMonth <= 0 || uiUtcDateMonth > 12) return;
	if (uiUtcDateYear < 1970 || uiUtcDateYear > 9999) return;
	PREPARE_SETS
	SET_TIME(m_uiUtcTime, ToMilliSeconds(h,m,s));
	SET_TIME(m_uiUtcDate, ToDaysSince1970(uiUtcDateYear, uiUtcDateMonth, uiUtcDateDay));
	NOTIFY_CHANGES
}
