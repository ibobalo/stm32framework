#include "stdafx.h"
#include <stdint.h>
#include <math.h>
#include "gps_processor.h"

//#define GPS_DEBUG_INFO(...) DEBUG_INFO("GPS:" __VA_ARGS__)
#define GPS_DEBUG_INFO(...)

void CGpsProcessor::Init(ISerial* pSerial)
{
	CGpsParser::Init();

	m_pSerial = pSerial;
	m_pSerial->SetSerialRxNotifier(this);
	m_pSerial->SetSerialTxNotifier(this);
	m_nReceivedCharsCount = 0;
	m_nParsedCharsCount = 0;
	CIoChunk recvChunk = {(volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE};
	m_pSerial->StartDataRecv(recvChunk);
	m_bTxBusy = false;
}

/*virtual*/
bool CGpsProcessor::NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk) //returns true in next chunk set
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxPart(uiBytesLeft, pRetNextChunk));
	GPS_DEBUG_INFO("Rx left:", uiBytesLeft);
	m_nReceivedCharsCount = GPSMSGMAXSIZE - uiBytesLeft;
	bool bReset = ParseReceived();
	if (bReset || uiBytesLeft < 5) {
		m_nReceivedCharsCount = 0;
		m_nParsedCharsCount = 0;
		pRetNextChunk->Assign((volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE);
	}
	return true;
}
/*virtual*/
bool CGpsProcessor::NotifyRxDone(CIoChunk* pRetNextChunk) //returns true in next chunk set
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxDone(pRetNextChunk));

	m_nReceivedCharsCount = GPSMSGMAXSIZE;
	GPS_DEBUG_INFO("Rx DONE, parsing:", m_nReceivedCharsCount);
	ParseReceived();

	m_nReceivedCharsCount = 0;
	m_nParsedCharsCount = 0;
	pRetNextChunk->Assign((volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE);

	return true;
}
/*virtual*/
bool CGpsProcessor::NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxIdle(uiBytesLeft, pRetNextChunk));

	m_nReceivedCharsCount = GPSMSGMAXSIZE - uiBytesLeft;
	GPS_DEBUG_INFO("Rx IDLE, parsing:", m_nReceivedCharsCount);
	bool bReset = ParseReceived();

	if (bReset || uiBytesLeft < 5) {
		GPS_DEBUG_INFO("Rx reset");
		m_nReceivedCharsCount = 0;
		m_nParsedCharsCount = 0;
		pRetNextChunk->Assign((volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE);
	}
	return true;
}
/*virtual*/
bool CGpsProcessor::NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxTimeout(uiBytesLeft, pRetNextChunk));

	ParserReset();

	m_nReceivedCharsCount = 0;
	m_nParsedCharsCount = 0;
	pRetNextChunk->Assign((volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE);

	return true;
}
/*virtual*/
bool CGpsProcessor::NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxError(uiBytesLeft, pRetNextChunk));
	GPS_DEBUG_INFO("comm error");

	ParserReset();

	m_nReceivedCharsCount = 0;
	m_nParsedCharsCount = 0;
	pRetNextChunk->Assign((volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE);

	return true;
}
/*virtual*/
bool CGpsProcessor::NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxOverflow(ccData, pRetNextChunk));

	ParserReset();

	m_nReceivedCharsCount = 0;
	m_nParsedCharsCount = 0;
	pRetNextChunk->Assign((volatile unsigned char*)m_acMessageBuf, GPSMSGMAXSIZE);

	return true;
}

/*virtual*/
bool CGpsProcessor::NotifyTxDone(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
	m_bTxBusy = false;
	DoneScheduledResponce();
	return false;
}
/*virtual*/
bool CGpsProcessor::NotifyTxComplete(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
	return false;
}
/*virtual*/
bool CGpsProcessor::NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
	m_bTxBusy = false;
	return false;
}
/*virtual*/
bool CGpsProcessor::NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
	m_bTxBusy = false;
	return false;
}

bool CGpsProcessor::ParseReceived()
{
	bool bReset = false;
	for (unsigned n = m_nParsedCharsCount; n < m_nReceivedCharsCount; ++n) {
		char cReceivedChar = m_acMessageBuf[n];
		GPS_DEBUG_INFO("Rx:", cReceivedChar);
		EParseResult eResult = Parse(cReceivedChar);
		if (eResult != PR_CONTINUE) bReset = true;
	}
	m_nParsedCharsCount = m_nReceivedCharsCount;
	if (bReset) ProcessRequest();
	return bReset;
}

void CGpsProcessor::ProcessRequest()
{
	CConstChunk TxChunk;
	if (!m_bTxBusy && GetScheduledResponce(&TxChunk.pData, &TxChunk.uiLength)) {
		m_bTxBusy = true;
		if (!m_pSerial->StartDataSend(TxChunk)) {
			m_bTxBusy = false;
		}
	}
}

CGpsProcessor g_GpsProcessor;

CGpsProcessor* AcquireGpsProcessor(ISerial* pSerial) {g_GpsProcessor.Init(pSerial); return &g_GpsProcessor;}
