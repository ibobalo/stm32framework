USE_PROCESS_SCHEDULLER += $(if $(filter YES,$(USE_GPS)), \
	YES \
)
USE_MATHLIB += $(if $(filter YES,$(USE_GPS)), \
	YES \
)
USE_USART += $(if $(filter YES,$(USE_GPS)), \
	YES \
)

INCLUDE_PATH += $(if $(filter YES,$(USE_GPS)), \
	src/modules/gps \
)

SRC += $(if $(filter YES,$(USE_GPS)), $(addprefix src/modules/gps/, \
	gps.cpp \
	gps_parser.cpp \
	gps_parser_nmea.cpp \
	gps_parser_ubx.cpp \
	$(if $(filter YES,$(USE_GPS_RAW)), gps_parser_ubx_raw.cpp,) \
	gps_processor.cpp \
))
