#include "stdafx.h"
#include <stdint.h>
#include <math.h>

#include "gps_parser.h"

//#define GPS_DEBUG_INFO(...) DEBUG_INFO("GPS:" __VA_ARGS__)
#define GPS_DEBUG_INFO(...) {}

void CGpsParser::Init()
{
	CGpsInfo::Init();

	m_eGPSProtocol = GP_NMEA;
	m_pRequestData = NULL;
	m_nRequestDataLength = 0;

	ParserReset();
}

CGpsParser::EParseResult CGpsParser::Parse(char ch)
{
	EParseResult eResult = PR_ERROR;
	if (m_eGPSProtocol == GP_NMEA) {
		eResult = ParseNMEA(ch);
		if (eResult != PR_ERROR) {
			// do nothing
		} else {
			eResult = ParseUBX(ch);
			if (eResult != PR_ERROR) {
				m_eGPSProtocol = GP_UBX;
			}
		}
	} else if (m_eGPSProtocol == GP_UBX) {
		eResult = ParseUBX(ch);
		if (eResult != PR_ERROR) {
			// do nothing
		} else {
			eResult = ParseNMEA(ch);
			if (eResult != PR_ERROR) {
				m_eGPSProtocol = GP_NMEA;
			}
		}
	}
	return eResult;
}

void CGpsParser::ParserReset()
{
	m_ttPacketStartTime = 0;
	if (m_eGPSProtocol == GP_NMEA) {ParserNMEAReset();}
	if (m_eGPSProtocol == GP_UBX) {ParserUBXReset();}
}

bool CGpsParser::ScheduleResponce(const void* pData, unsigned nDataLength)
{
	if (m_nRequestDataLength) return false; //busy

	m_pRequestData = pData;
	m_nRequestDataLength = nDataLength;
	return true;
}

bool CGpsParser::GetScheduledResponce(const uint8_t** pRetData, unsigned *pRetDataLength) const
{
	if (!m_nRequestDataLength) return false;

	*pRetData = (const uint8_t*)m_pRequestData;
	*pRetDataLength = m_nRequestDataLength;

	return true;
}

void CGpsParser::DoneScheduledResponce()
{
	m_nRequestDataLength = 0;
}
