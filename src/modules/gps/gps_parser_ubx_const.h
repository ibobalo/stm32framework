#ifndef GPS_PARSER_UBX_CONST_H_INCLUDED
#define GPS_PARSER_UBX_CONST_H_INCLUDED

#include "util/endians.h"

#define UBX_KEY(CLASS, ID) (((CLASS)<<8) | (ID))

#define UBX_CLASS_NAV 0x01
#define UBX_CLASS_RXM 0x02
#define UBX_CLASS_TRK 0x03
#define UBX_CLASS_INF 0x04
#define UBX_CLASS_ACK 0x05
#define UBX_CLASS_CFG 0x06
#define UBX_CLASS_MON 0x0A
#define UBX_CLASS_AID 0x0B
#define UBX_CLASS_TIM 0x0D
#define UBX_CLASS_LOG 0x21

typedef int8_t UBX_I1;
typedef uint8_t UBX_U1;
typedef uint8_t UBX_X1;
typedef LE_I16 UBX_I2;
typedef LE_U16 UBX_U2;
typedef LE_U16 UBX_X2;
typedef LE_I32 UBX_I4;
typedef LE_U32 UBX_U4;
typedef LE_U32 UBX_X4;
typedef LE_I64 UBX_I8;
typedef LE_U64 UBX_U8;
typedef float UBX_R4;
typedef double UBX_R8;
typedef char UBX_CH;

typedef struct {
	uint8_t     nMsgClass;
	uint8_t     nMsgId;
	uint16_t    nPayloadLength;
} UBX_MSG_BASE_t;

#define UBX_PKT_DEFINITION(NAME, CLASS, ID, STRUCT) \
	static const uint16_t UBX_KEY_##NAME = UBX_KEY(CLASS, ID); \
	typedef struct : public UBX_MSG_BASE_t { \
		STRUCT \
	} UBX_MSG_##NAME##_t;


#endif /* GPS_PARSER_UBX_CONST_H_INCLUDED */
