#include "stdafx.h"
#include <string.h>
#include <math.h>
#include "gps.h"

//#define GPS_DEBUG_INFO(...) DEBUG_INFO("GPS:" __VA_ARGS__)
#define GPS_DEBUG_INFO(...)

#define WGS84a 6378137.0L
#define WGS84b 6356752.3142L
#define WGS84e2 6.69437999014E-3L
#define WGS84es2 6.73949674228E-3L
#define WGS84a2 (WGS84a * WGS84a)
#define WGS84b2 (WGS84b * WGS84b)
#define WGS84e4 (WGS84e2 * WGS84e2)

double DDmm2DD(const double& dDD)
{
	double dDegr, dMinutes;
	dMinutes = modf(dDD * 0.01, &dDegr);
	return dDegr + dMinutes * (100.0 / 60.0);
}

void CGpsInfo::Init()
{
	m_nSatsInView = 0;
	m_nSatsUsed = 0;
}

bool CGpsInfo::CalcLLAIfNecessary()
{
	if (!GetIsFixed()) return false;
	if (m_ttECEFPosUpdateTime > m_ttLLAPosUpdateTime) {
		ECEFtoLLA(m_dECEFPosX, m_dECEFPosY, m_dECEFPosZ, &m_dLLAPosLat, &m_dLLAPosLon, &m_dLLAHeight);
		m_ttLLAPosUpdateTime = m_ttLLAHeightUpdateTime = m_ttECEFPosUpdateTime;
	}
	return true;
}

bool CGpsInfo::CalcECEFIfNecessary()
{
	if (!GetIsFixed()) return false;
	if (m_ttLLAPosUpdateTime > m_ttECEFPosUpdateTime) {
		LLAtoECEF(m_dLLAPosLat, m_dLLAPosLon, m_dLLAHeight, &m_dECEFPosX, &m_dECEFPosY, &m_dECEFPosZ);
		m_ttECEFPosUpdateTime = m_ttLLAPosUpdateTime;
	}
	return true;
}

bool CGpsInfo::GetPosLLA(double* pdRetLat, double* pdRetLon, double* pdRetHeight) const
{
	if (!GetIsFixed() || GetIsLLAExpired()) return false;
	if (pdRetLat) *pdRetLat = m_dLLAPosLat;
	if (pdRetLon) *pdRetLon = m_dLLAPosLon;
	if (pdRetLon) *pdRetHeight = m_dLLAHeight;
	return true;
}

bool CGpsInfo::GetPosECEF(double* pdRetX, double* pdRetY, double* pdRetZ) const
{
	if (!GetIsFixed() || GetIsECEFExpired()) return false;
	if (pdRetX) *pdRetX = m_dECEFPosX;
	if (pdRetY) *pdRetY = m_dECEFPosY;
	if (pdRetZ) *pdRetZ = m_dECEFPosZ;
	return true;
}

bool CGpsInfo::GetPOVDiffLLA(const CGpsInfo* pTgtPos, double* pdRetLatDiff, double* pdRetLonDiff, double* pdRetHeight) const
{
	double dTgtLat, dTgtLon, dTgtHeight;
	if (!GetIsLLAExpired() && pTgtPos->GetPosLLA(&dTgtLat, &dTgtLon, &dTgtHeight)) {
		CalcLLA2Diff(
				m_dLLAPosLat, m_dLLAPosLon, m_dLLAHeight,
				dTgtLat, dTgtLon, dTgtHeight,
				pdRetLatDiff, pdRetLonDiff, pdRetHeight
		);
		return true;
	}
	return false;
}

bool CGpsInfo::GetPOVDiffMetersLLA(const CGpsInfo* pTgtPos, double* pdRetLatDiffMeters, double* pdRetLonDiffMeters, double* pdRetHeightMeters) const
{
	double dLatDiff, dLonDiff, dLatCenter;
	if (GetCenterLLA(pTgtPos, &dLatCenter, NULL, NULL) && GetPOVDiffLLA(pTgtPos, &dLatDiff, &dLonDiff, pdRetHeightMeters)) {
		double dMetersPerRadianLat, dMetersPerRadianLon;
		CalcMetersPerRadian(dLatCenter, &dMetersPerRadianLat, &dMetersPerRadianLon);
		if (pdRetLatDiffMeters) *pdRetLatDiffMeters = dLatDiff * dMetersPerRadianLat;
		if (pdRetLonDiffMeters) *pdRetLonDiffMeters = dLonDiff * dMetersPerRadianLon;
		return true;
	}
	return false;
}

bool CGpsInfo::GetCenterLLA(const CGpsInfo* pTgtPos, double* pdRetCenterLat, double* pdRetCenterLon, double* pdRetCenterHeight) const
{
	double dTgtLat, dTgtLon, dTgtHeight;
	if (!GetIsLLAExpired() && pTgtPos->GetPosLLA(&dTgtLat, &dTgtLon, &dTgtHeight)) {
		CalcLLA2Center(
				m_dLLAPosLat, m_dLLAPosLon, m_dLLAHeight,
				dTgtLat, dTgtLon, dTgtHeight,
				pdRetCenterLat, pdRetCenterLon, pdRetCenterHeight
		);
		return true;
	}
	return false;
}

bool CGpsInfo::GetPOVDiffECEF(const CGpsInfo* pTgtPos, double* pdRetXDiff, double* pdRetYDiff, double* pdRetZDiff) const
{
	double dTgtX, dTgtY, dTgtZ;
	if (!GetIsECEFExpired() && pTgtPos->GetPosECEF(&dTgtX, &dTgtY, &dTgtZ)) {
		CalcECEF2Diff(
				m_dECEFPosX, m_dECEFPosY, m_dECEFPosZ,
				dTgtX, dTgtY, dTgtZ,
				pdRetXDiff, pdRetYDiff, pdRetZDiff
		);
		return true;
	}
	return false;
}

bool CGpsInfo::GetPOVAzimuthElevationDistance(const CGpsInfo* pTgtPos, double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance) const
{
	if (!pTgtPos) return false;
	if (!GetIsFixed() || !pTgtPos->GetIsFixed()) return false;
	double dTgtX, dTgtY, dTgtZ;
	double dTgtLat, dTgtLon, dTgtHeight;
	if (!GetIsECEFExpired() && pTgtPos->GetPosECEF(&dTgtX, &dTgtY, &dTgtZ)) {
		ECEF2toAzimuthElevationDistance(
				m_dECEFPosX, m_dECEFPosY, m_dECEFPosZ,
				dTgtX, dTgtY, dTgtZ,
				pdRetAzimuth, pdRetElevation, pdRetDistance
		);
		return true;
	} else if (!GetIsLLAExpired() && pTgtPos->GetPosLLA(&dTgtLat, &dTgtLon, &dTgtHeight)) {
		LLA2toAzimuthElevationDistance(
				m_dLLAPosLat, m_dLLAPosLon, m_dLLAHeight,
				dTgtLat, dTgtLon, dTgtHeight,
				pdRetAzimuth, pdRetElevation, pdRetDistance
		);
		return true;
	}
	return false;
}

void CalcLLA2Diff(
		double lat1, double lon1, double height1,
		double lat2, double lon2, double height2,
		double* pdRetLatDiff, double* pdRetLonDiff, double* pdRetHeightDiff
) {
	if (pdRetLatDiff) {
		*pdRetLatDiff = lat1 - lat2;
	}
	if (pdRetLonDiff) {
		double dLonDiff = lon1 - lon2;
		// fix longitude 179.9/-179.9 Avg to 0
		if (dLonDiff > M_PI /*180.0*/) {dLonDiff -= 2 * M_PI /*360.0*/;}
		else if (dLonDiff < -M_PI /*-180.0*/) {dLonDiff += 2 * M_PI /*360.0*/;}
		*pdRetLonDiff = dLonDiff;
	}
	if (pdRetHeightDiff) {
		*pdRetHeightDiff = height1 - height2;
	}
}

void CalcLLA2Center(
		double lat1, double lon1, double height1,
		double lat2, double lon2, double height2,
		double* pdRetCenterLat, double* pdRetCenterLon, double* pdRetCenterHeight
) {
	double dLatDiff, dLonDiff, dHeightDiff;
	CalcLLA2Diff(lat1, lon1, height1, lat2, lon2, height2, &dLatDiff, &dLonDiff, &dHeightDiff);

	if (pdRetCenterLat) {
		*pdRetCenterLat = lat1 + dLatDiff * 0.5;
	}
	if (pdRetCenterLon) {
		*pdRetCenterLon = lon1 + dLonDiff * 0.5;
	}
	if (pdRetCenterHeight) {
		*pdRetCenterHeight = height1 + dHeightDiff * 0.5;
	}
}

void CalcMetersPerRadian(double dLat, double* pdRetMetersPerRadianLat, double* pdRetMetersPerRadianLon)
{
	if (pdRetMetersPerRadianLat) *pdRetMetersPerRadianLat = RADTODEG(111132.954) - RADTODEG(559.822) * cos(2 * dLat) + RADTODEG(1.175) * cos(4 * dLat);
	if (pdRetMetersPerRadianLon) *pdRetMetersPerRadianLon = RADTODEG(111412.84) * cos(dLat) - RADTODEG(93.5 * cos(3 * dLat));
}

void ECEFtoLLA(double x, double y, double z, double* pdRetLat, double* pdRetLon, double* pdRetHeight)
{
	// https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_ECEF_to_geodetic_coordinates
	double x2 = x * x;
	double y2 = y * y;
	double z2 = z * z;
	double r = sqrt(x2 + y2);
	double r2 = r * r;
	double E2 = WGS84a2 - WGS84b2;
	double F = 54 * WGS84b * WGS84b * z2;
	double z2_1_e2 = z2 * (1 - WGS84e2);
	double G = r2 + z2_1_e2 - WGS84e2 * E2;
	double G2 = G * G;
	double G3 = G2 * G;
	double C = WGS84e4 * F * r2 / G3;
	double C2 = C * C;
	double S = pow(1 + C + sqrt(C2 + 2 * C), 1.0L / 3);
	double tmp = (S + 1.0L / S + 1);
	double P = F / (3 * tmp * tmp * G2);
	double Q = sqrt(1 + 2 * WGS84e4 * P);
	double invQ = 1.0 / Q;
	double invQp1 = 1.0 / (1 + Q);
	double r0 = -P * WGS84e2 * r * invQp1 + sqrt(0.5 * WGS84a2 * (1 + invQ) - P * z2_1_e2 * invQ * invQp1 - 0.5 * P * r2);
	double dr = r - WGS84e2 * r0;
	double dr2 = dr * dr;
	double U = sqrt(dr2 + z2);
	double V = sqrt(dr2 + z2_1_e2);
	double b2divaV = WGS84b2 / WGS84a / V;
	double Z0 = z * b2divaV;
	*pdRetHeight = U * (1 - b2divaV);
	*pdRetLat  = atan((z + WGS84es2 * Z0) / r);
	*pdRetLon  = atan2(y, x);
}

void LLAtoECEF(double lat, double lon, double height, double* pdRetX, double* pdRetY, double* pdRetZ)
{
	// https://microem.ru/files/2012/08/GPS.G1-X-00006.pdf
	double sinlat = sin(lat);
	double sinlon = sin(lon);
	double coslat = cos(lat);
	double coslon = cos(lon);
	double N = WGS84a / sqrt(1 - WGS84e2 * sinlat * sinlat);
	if (pdRetX) {
		*pdRetX = (N + height) * coslat * coslon;
	}
	if (pdRetY) {
		*pdRetY = (N + height) * coslat * sinlon;
	}
	if (pdRetZ) {
		*pdRetZ = (WGS84b2 / WGS84a2 * N + height) * sinlat;
	}
}

void ECEF2toAzimuthElevationDistance(
		double x1, double y1, double z1,
		double x2, double y2, double z2,
		double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance
) {
	double dx = x2 - x1;
	double dy = y2 - y1;
	double dz = z2 - z1;
	return ECEFDiffToAzimuthElevationDistance(
			x1, y1, z1,
			dx, dy, dz,
			pdRetAzimuth, pdRetElevation, pdRetDistance
	);
}

void ECEFDiffToAzimuthElevationDistance(
		double x, double y, double z,
		double dx, double dy, double dz,
		double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance
) {
	// http://gis.stackexchange.com/questions/58923/calculate-view-angle
	double x2 = x*x;
	double y2 = y*y;
	double z2 = z*z;
	double dx2 = dx*dx;
	double dy2 = dy*dy;
	double dz2 = dz*dz;
	double dxyz = sqrt(dx2 + dy2 + dz2);
	double rxy = sqrt(x2 + y2);
	double rxyz = sqrt(x2 + y2 + z2);
	if (pdRetAzimuth) {
		*pdRetAzimuth = atan2(
				(-z*x*dx - z*y*dy + (x2+y2)*dz) / (rxy * rxyz * dxyz),
				(-y*dx + x*dy) / (rxy * dxyz)
		);
	}
	if (pdRetElevation) {*pdRetElevation = acos((x*dx + y*dy + z*dz) / (rxyz * dxyz));}
	if (pdRetDistance) {*pdRetDistance = dxyz;}
}

void LLA2toAzimuthElevationDistance(
		double lat1, double lon1, double height1,
		double lat2, double lon2, double height2,
		double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance
) {
	double dLatDiff, dLonDiff, dHeightDiff;
	CalcLLA2Diff(lat1, lon1, height1, lat2, lon2, height2, &dLatDiff, &dLonDiff, &dHeightDiff);
	double dLatCenter;
	CalcLLA2Center(lat1, lon1, height1, lat2, lon2, height2, &dLatCenter, NULL, NULL);
	double dMetersPerRadianLat, dMetersPerRadianLon;
	CalcMetersPerRadian(dLatCenter, &dMetersPerRadianLat, &dMetersPerRadianLon);
	double dLatDiffMeters = dLatDiff * dMetersPerRadianLat;
	double dLonDiffMeters = dLonDiff * dMetersPerRadianLon;
	if (pdRetDistance) {
		*pdRetDistance = sqrt(dLatDiffMeters * dLatDiffMeters + dLonDiffMeters * dLonDiffMeters + dHeightDiff * dHeightDiff);
	}
	if (pdRetAzimuth) {
		*pdRetAzimuth = RADTODEG(atan2(dLatDiffMeters, dLonDiffMeters));
	}
	if (pdRetElevation) {
		*pdRetElevation = RADTODEG(atan2(sqrt(dLatDiffMeters * dLatDiffMeters + dLonDiffMeters * dLonDiffMeters), dHeightDiff));
	}
}

void CalcECEF2Diff(
		double x1, double y1, double z1,
		double x2, double y2, double z2,
		double* pdRetXDiff, double* pdRetYDiff, double* pdRetZDiff
) {
	if (pdRetXDiff) *pdRetXDiff = x2 - x1;
	if (pdRetYDiff) *pdRetYDiff = y2 - y1;
	if (pdRetZDiff) *pdRetZDiff = z2 - z1;
}

void CalcECEF2Center(
		double x1, double y1, double z1,
		double x2, double y2, double z2,
		double* pdRetCenterX, double* pdRetCenterY, double* pdRetCenterZ
) {
	if (pdRetCenterX) *pdRetCenterX = (x1 + x2) * 0.5;
	if (pdRetCenterY) *pdRetCenterY = (y1 + y2) * 0.5;
	if (pdRetCenterZ) *pdRetCenterZ = (z1 + z2) * 0.5;
}

