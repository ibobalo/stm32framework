#include "stdafx.h"
#include <stdint.h>
#include <math.h>
#include "gps_parser.h"
#include "gps_parser_ubx_const.h"

#include "rtklib.h"
# define IF_GPSENA(...) __VA_ARGS__
# define IF_SBSENA(...) __VA_ARGS__
#ifdef ENAGLO
# define IF_GLOENA(...) __VA_ARGS__
#else
# define IF_GLOENA(...)
#endif
#ifdef ENAGAL
# define IF_GALENA(...) __VA_ARGS__
#else
# define IF_GALENA(...)
#endif
#ifdef ENAQZS
# define IF_QZSENA(...) __VA_ARGS__
#else
# define IF_QZSENA(...)
#endif
#ifdef ENACMP
# define IF_CMPENA(...) __VA_ARGS__
#else
# define IF_CMPENA(...)
#endif
#ifdef ENALEO
# define IF_LEOENA(...) __VA_ARGS__
#else
# define IF_LEOENA(...)
#endif

// RAW packets info:
//    http://wiki.openstreetmap.org/wiki/UbloxRAW
//    http://www.rtklib.com/
// https://gnss.co/2014/07/17/rtklib-on-raspberry-pi/
// https://habrahabr.ru/post/244475/

#define UBX_DEBUG_INFO(...) DEBUG_INFO("GPS UBX:" __VA_ARGS__)

UBX_PKT_DEFINITION(NAV_CLOCK, UBX_CLASS_NAV, 0x22, {
		UBX_U4 iTOW_ms;
		UBX_I4 clkB_ns;
		UBX_I4 clkD_nss;
		UBX_I4 tAcc_ns;
		UBX_I4 fAcc_pss;
})

typedef struct _TRK_TRKD5_observation {
	UBX_U8 ts;               // +0  transmission time  /2^32/1000
	UBX_U8 adr;              // +8  /2^32
	UBX_U4 dop;              // +16 /2^10/4
	UBX_U1 reserved[32 - 16 - 4];
} TRK_TRKD5_observation_t;
STATIC_ASSERTE(sizeof(TRK_TRKD5_observation_t) == 32, TRK_TRKD5_observation_invalid_size);

typedef struct _TRK_TRKD5_v0v3_observation : public _TRK_TRKD5_observation {
	UBX_U1 reserved01[32 - sizeof(TRK_TRKD5_observation_t)];
	UBX_U2 snr;              // +32 /2^8
	UBX_U1 prn;              // +34
	UBX_U1 reserved02[41 - 34 - 1];
	UBX_X1 qualityIndicator; // +41  &7
	UBX_U1 reserved62[54 - 41 - 1];
	UBX_X1 flag;             // +54  tracking status
} TRK_TRKD5_v0v3_observation_t;
STATIC_ASSERTE(sizeof(TRK_TRKD5_v0v3_observation_t) == 56, TRKD5_v0v3_observation_invalid_size);

typedef struct _TRK_TRKD5_v6_observation : public _TRK_TRKD5_observation {
	UBX_U1 reserved61[32 - sizeof(TRK_TRKD5_observation_t)];
	UBX_U2 snr;              // +32 /2^8
	UBX_U1 reserved62[41 - 32 - 2];
	UBX_X1 qualityIndicator; // +41  &7
	UBX_U1 reserved63[54 - 41 - 1];
	UBX_X1 flag;             // +54  tracking status
	UBX_U1 reserved64[56 - 54 - 1];
	UBX_U1 sys;              // +56  0:GPS,1:SBS,2:GAL,3:CMP,5:QZS,6:GLO
	UBX_U1 prn;              // +57
	UBX_U1 reserved65[59 - 57 - 1];
	UBX_U1 frq;              // +59  -7
	UBX_U1 reserved66[64 - 59 - 1];
} TRK_TRKD5_v6_observation_t;
STATIC_ASSERTE(sizeof(TRK_TRKD5_v6_observation_t) == 64, TRKD5_v6_observation_invalid_size);

UBX_PKT_DEFINITION(TRK_TRKD5, UBX_CLASS_TRK, 0x0A, {
		union {
			struct {
				UBX_U1 version; /* msg version: v0:72+N*56+M*16 v3:80+N*56+M*16 v6:80+N*64+M*20 */
			} common;
			struct {
				UBX_U1 reserved[72];
				TRK_TRKD5_v0v3_observation_t obsData[];
			} v0;
			struct {
				UBX_U1 reserved[80];
				TRK_TRKD5_v0v3_observation_t obsData[];
			} v3;
			struct {
				UBX_U1 reserved[80];
				TRK_TRKD5_v6_observation_t obsData[];
			} v6;
		};
})

UBX_PKT_DEFINITION(TRK_SFRBX, UBX_CLASS_TRK, 0x0F, {
		// TODO
})

UBX_PKT_DEFINITION(TRK_MEAS, UBX_CLASS_TRK, 0x10, {
		UBX_U1 nChannels;
		// TODO
})

const static double gpst0[]={1980,1, 6,0,0,0}; /* gps time reference */
const static double gst0 []={1999,8,22,0,0,0}; /* galileo system time reference */
const static double bdt0 []={2006,1, 1,0,0,0}; /* beidou time reference */

unsigned RTKLIB_satno(uint8_t sys, unsigned prn)
{
	switch (sys) {
		case SYS_GPS: return (MINPRNGPS<=prn && prn<=MAXPRNGPS) ? (prn-MINPRNGPS+1) : 0;
	IF_GLOENA(case SYS_GLO: return (MINPRNGLO<=prn && prn<=MAXPRNGLO) ? (NSATGPS+prn-MINPRNGLO+1) : 0;)
	IF_GALENA(case SYS_GAL: return (MINPRNGAL<=prn && prn<=MAXPRNGAL) ? (NSATGPS+NSATGLO+prn-MINPRNGAL+1) : 0;)
	IF_QZSENA(case SYS_QZS: return (MINPRNQZS<=prn && prn<=MAXPRNQZS) ? (NSATGPS+NSATGLO+NSATGAL+prn-MINPRNQZS+1) : 0;)
	IF_CMPENA(case SYS_CMP: return (MINPRNCMP<=prn && prn<=MAXPRNCMP) ? (NSATGPS+NSATGLO+NSATGAL+NSATQZS+prn-MINPRNCMP+1) : 0;)
	IF_SBSENA(case SYS_SBS: return (MINPRNSBS<=prn && prn<=MAXPRNSBS) ? (NSATGPS+NSATGLO+NSATGAL+NSATQZS+NSATCMP+NSATLEO+prn-MINPRNSBS+1) : 0;)
	}
	return 0;
}

/* add time --------------------------------------------------------------------
* add time to gtime_t struct
* args   : gtime_t t        I   gtime_t struct
*          double sec       I   time to add (s)
* return : gtime_t struct (t+sec)
*-----------------------------------------------------------------------------*/
gtime_t RTKLIB_timeadd(gtime_t t, double sec)
{
    double tt;
    t.sec+=sec; tt=floor(t.sec); t.time+=(int)tt; t.sec-=tt;
    return t;
}

/* time difference -------------------------------------------------------------
* difference between gtime_t structs
* args   : gtime_t t1,t2    I   gtime_t structs
* return : time difference (t1-t2) (s)
*-----------------------------------------------------------------------------*/
double RTKLIB_timediff(gtime_t t1, gtime_t t2)
{
    return difftime(t1.time,t2.time)+t1.sec-t2.sec;
}

/* convert calendar day/time to time -------------------------------------------
* convert calendar day/time to gtime_t struct
* args   : double *ep       I   day/time {year,month,day,hour,min,sec}
* return : gtime_t struct
* notes  : proper in 1970-2037 or 1970-2099 (64bit time_t)
*-----------------------------------------------------------------------------*/
extern gtime_t RTKLIB_epoch2time(const double *ep)
{
    const int doy[]={1,32,60,91,121,152,182,213,244,274,305,335};
    gtime_t time={0};
    int days,sec,year=(int)ep[0],mon=(int)ep[1],day=(int)ep[2];

    if (year<1970||2099<year||mon<1||12<mon) return time;

    /* leap year if year%4==0 in 1901-2099 */
    days=(year-1970)*365+(year-1969)/4+doy[mon-1]+day-2+(year%4==0&&mon>=3?1:0);
    sec=(int)floor(ep[5]);
    time.time=(time_t)days*86400+(int)ep[3]*3600+(int)ep[4]*60+sec;
    time.sec=ep[5]-sec;
    return time;
}

/* time to gps time ------------------------------------------------------------
* convert gtime_t struct to week and tow in gps time
* args   : gtime_t t        I   gtime_t struct
*          int    *week     IO  week number in gps time (NULL: no output)
* return : time of week in gps time (s)
*-----------------------------------------------------------------------------*/
double RTKLIB_time2gpst(gtime_t t, int *week)
{
    gtime_t t0=RTKLIB_epoch2time(gpst0);
    time_t sec=t.time-t0.time;
    int w=(int)(sec/(86400*7));

    if (week) *week=w;
    return (double)(sec-w*86400*7)+t.sec;
}

/* gps time to time ------------------------------------------------------------
* convert week and tow in gps time to gtime_t struct
* args   : int    week      I   week number in gps time
*          double sec       I   time of week in gps time (s)
* return : gtime_t struct
*-----------------------------------------------------------------------------*/
extern gtime_t RTKLIB_gpst2time(int week, double sec)
{
    gtime_t t=RTKLIB_epoch2time(gpst0);

    if (sec<-1E9||1E9<sec) sec=0.0;
    t.time+=86400*7*week+(int)sec;
    t.sec=sec-(int)sec;
    return t;
}

/* get current time in utc -----------------------------------------------------
* get current time in utc
* args   : none
* return : current time in utc
*-----------------------------------------------------------------------------*/
static double timeoffset_=0.0;        /* time offset (s) */
gtime_t RTKLIB_timeget(ticktime_t ttTicks)
{
	gtime_t loc;
	loc.time = ttTicks / CHW_Clocks::GetSysTickFrequency();
	loc.sec = ttTicks % CHW_Clocks::GetSysTickFrequency();
    loc.sec /= CHW_Clocks::GetSysTickFrequency();
    return RTKLIB_timeadd(loc, timeoffset_);
}
gtime_t RTKLIB_timeget(void)
{
	return RTKLIB_timeget(CHW_Clocks::GetSysTick());
}

void CGpsParser::ProcessUBXMsgRaw(unsigned uiMsgKey)
{
	gtime_t raw_time = RTKLIB_timeget(m_ttPacketStartTime);
	switch (uiMsgKey) {
		case UBX_KEY_NAV_CLOCK: { // Clock Solution
			const UBX_MSG_NAV_CLOCK_t* pMsg = (const UBX_MSG_NAV_CLOCK_t*)m_aUBXPkt;
			if (m_nUBXPktMsgPayloadLength == sizeof(*pMsg)) {
				UBX_DEBUG_INFO("NAV-CLOCK:", (int)pMsg->iTOW_ms);
			}
		} break;
		case 0x0309: { // unknown
			ASSERT("unknown");
		} break;
		case UBX_KEY_TRK_TRKD5: { // RAW TRK-TRKD5 requested by "B5 62 06 01 03 00 03 0A 01 18 5D" (req.baudrate 115200)
			const UBX_MSG_TRK_TRKD5_t* pMsg = (const UBX_MSG_TRK_TRKD5_t*)m_aUBXPkt;
			raw.obs.n = 0;
			switch (pMsg->common.version) {
			case 6: { // ublox7
				unsigned N = (m_nUBXPktMsgPayloadLength - sizeof(pMsg->v6)) / sizeof(pMsg->v6.obsData[0]);
				IIF(NSATGLO)( double utc_gpst = timediff(gpst2utc(raw_time),raw_time); )
				double tr = -1.0;
				for (unsigned n = 0; n < N; ++n) {
					if ((pMsg->v6.obsData[n].qualityIndicator & 7) < 4) continue;
					double t = pMsg->v6.obsData[n].ts * P2_32 * 0.001;
					IF_GLOENA( if (pMsg->v6.obsData[n].sys == 6) t -= 10800.0 + utc_gpst; )
					if (t > tr) tr = t;
				}
				if (tr >= 0.0) {
					tr = 0.1 * ROUND((tr + 0.08)*10);
				    /* adjust week handover */
					int week;
				    double t = RTKLIB_time2gpst(raw_time, &week);
				    if      (tr < t-302400.0) week--;
				    else if (tr > t+302400.0) week++;
				    gtime_t time = RTKLIB_gpst2time(week,tr);

					for (unsigned n = 0; n < N; ++n) {
						uint8_t qi = pMsg->v3.obsData[n].qualityIndicator & 7;
						if (qi < 4) continue;
						double ts = pMsg->v6.obsData[n].ts * P2_32 * 0.001;
						unsigned prn = pMsg->v6.obsData[n].prn;
						unsigned sys = pMsg->v6.obsData[n].sys;
						switch(sys) {
						IF_GPSENA(case 0: sys = SYS_GPS; break;)
						IF_SBSENA(case 1: sys = SYS_SBS; break;)
						IF_GALENA(case 2: sys = SYS_GAL; break;)
						IF_CMPENA(case 3: sys = SYS_CMP; break;)
						IF_QZSENA(case 5: sys = SYS_QZS; prn += MINPRNQZS - 1; break;)
						IF_GLOENA(case 6: sys = SYS_GLO; t -= 10800.0 + utc_gpst; break;)
						default: continue;
						}
						unsigned nSat = RTKLIB_satno(sys, prn);
						if (!nSat) continue;
						/* signal travel time */
						double tau = tr - ts;
						if      (tau<-302400.0) tau += 604800.0;
						else if (tau> 302400.0) tau -= 604800.0;
						uint8_t flag = pMsg->v3.obsData[n].flag;
						double adr;
						if (qi < 6) {
							adr = 0.0;
						} else {
							adr = pMsg->v3.obsData[n].adr * P2_32;
							if (flag & 0x01) adr += 0.5;
						}
				        double dop = pMsg->v3.obsData[n].dop * P2_10 * 0.25;
				        double snr = pMsg->v3.obsData[n].snr * P2_8;

						if (snr <= 10.0) ProcessUBXSatLock(nSat);
				        /* check phase lock */
				        if (flag&0x08) {
							ProcessUBXObservationData(nSat, time, tau * CLIGHT, -adr, dop, snr * 4, sys==SYS_CMP?CODE_L1I:CODE_L1C);
				        }
					}
				}
			} break;
			case 3: { // ublox7
				unsigned N = (m_nUBXPktMsgPayloadLength - sizeof(pMsg->v3)) / sizeof(pMsg->v3.obsData[0]);
				double tr = -1.0;
				for (unsigned n = 0; n < N; ++n) {
					if ((pMsg->v3.obsData[n].qualityIndicator & 7) < 4) continue;
					double t = pMsg->v3.obsData[n].ts * P2_32 * 0.001;
					if (t > tr) tr = t;
				}
				if (tr >= 0.0) {
					tr = 0.1 * ROUND((tr + 0.08)*10);
				    /* adjust week handover */
					int week;
				    double t = RTKLIB_time2gpst(raw_time, &week);
				    if      (tr < t-302400.0) week--;
				    else if (tr > t+302400.0) week++;
				    gtime_t time = RTKLIB_gpst2time(week,tr);

					for (unsigned n = 0; n < N; ++n) {
						uint8_t qi = pMsg->v3.obsData[n].qualityIndicator & 7;
						if (qi < 4) continue;
						unsigned prn = pMsg->v3.obsData[n].prn;
						unsigned nSat = RTKLIB_satno(prn < MINPRNSBS ? SYS_GPS : SYS_SBS, prn);
						if (!nSat) continue;
						double ts = pMsg->v3.obsData[n].ts * P2_32 * 0.001;
				        /* signal travel time */
						double tau = tr - ts;
						if      (tau<-302400.0) tau += 604800.0;
						else if (tau> 302400.0) tau -= 604800.0;
						uint8_t flag = pMsg->v3.obsData[n].flag;
						double adr;
						if (qi < 6) {
							adr = 0.0;
						} else {
							adr = pMsg->v3.obsData[n].adr * P2_32;
							if (flag & 0x01) adr += 0.5;
						}
				        double dop = pMsg->v3.obsData[n].dop * P2_10 * 0.25;
				        double snr = pMsg->v3.obsData[n].snr * P2_8;

						if (snr <= 10.0) ProcessUBXSatLock(nSat);
				        /* check phase lock */
				        if (flag&0x08) {
							ProcessUBXObservationData(nSat, time, tau * CLIGHT, -adr, dop, snr * 4, CODE_L1C);
				        }
						raw.time = time;
					}
				}
			} break;
			case 0: { // ublox7
			} break;
			}
//			input_ubx(); decode_ubx(); decode_trkd5()
//			ASSERT("TRK-TRKD5 test");
		} break;
		case UBX_KEY_TRK_SFRBX: { // RAW TRK-SFRBX requested by "B5 62 06 01 03 00 03 0F 01 1D 67" (req.baudrate 115200)
//			const UBX_MSG_TRK_SFRBX_t* pMsg = (const UBX_MSG_TRK_SFRBX_t*)m_aUBXPktPayload;
//			input_ubx();
//			decode_rxmsfrbx();
//			ASSERT("TRK-SFRBX test");
		} break;
		default:
			UBX_DEBUG_INFO("Pkt ignored, type:", uiMsgKey);
	}

	switch (m_nUBXConfigStep) {
	case 100: { // RAW enable TRK_TRKD5
		static const uint8_t UBX_RAW_TRK_TRKD5_REQUEST[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x03, 0x0A, 0x01, 0x18, 0x5D};
		if (ScheduleResponce(UBX_RAW_TRK_TRKD5_REQUEST, sizeof(UBX_RAW_TRK_TRKD5_REQUEST))) {
			++m_nUBXConfigStep;
		}
	} break;
	case 101: { // RAW enable TRK_SFRBX
		static const uint8_t UBX_RAW_TRK_SFRBX_REQUEST[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x03, 0x0F, 0x01, 0x1D, 0x67};
		if (ScheduleResponce(UBX_RAW_TRK_SFRBX_REQUEST, sizeof(UBX_RAW_TRK_SFRBX_REQUEST))) {
			++m_nUBXConfigStep;
		}
	} break;
	case 102: { // enable NAV-CLOCK
		static const uint8_t UBX_NAV_CLOCK_REQUEST[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x22, 0x01, 0x2E, 0x87};
		if (ScheduleResponce(UBX_NAV_CLOCK_REQUEST, sizeof(UBX_NAV_CLOCK_REQUEST))) {
			++m_nUBXConfigStep;
		}
	} break;
	case 103: { // enable NAV-SVINFO
		static const uint8_t UBX_NAV_SVINFO_REQUEST[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x30, 0x01, 0x3C, 0xA3};
		if (ScheduleResponce(UBX_NAV_SVINFO_REQUEST, sizeof(UBX_NAV_SVINFO_REQUEST))) {
			++m_nUBXConfigStep;
		}
	} break;
	}
}

void CGpsParser::ProcessUBXSatLock(unsigned nSat)
{
   	raw.lockt[nSat][1] = 1.0;
}

void CGpsParser::ProcessUBXObservationData(unsigned nSat, gtime_t dTime, double dDistance, double dAdr, float dDoppler, uint8_t snr, uint8_t code)
{
	unsigned n = raw.obs.n;

	raw.obs.data[n].time=dTime;
	raw.obs.data[n].sat=nSat;
	raw.obs.data[n].P[0]=dDistance;
	raw.obs.data[n].L[0]=-dAdr;
	raw.obs.data[n].D[0]=dDoppler;
	raw.obs.data[n].SNR[0]=snr;
	raw.obs.data[n].code[0]=code;
	raw.obs.data[n].LLI[0]=raw.lockt[nSat-1][1]>0.0?1:0;
	raw.lockt[nSat-1][1]=0.0;

	for (unsigned j=1; j<NFREQ+NEXOBS; j++) {
		raw.obs.data[n].L[j]=raw.obs.data[n].P[j]=0.0;
		raw.obs.data[n].D[j]=0.0;
		raw.obs.data[n].SNR[j]=raw.obs.data[n].LLI[j]=0;
		raw.obs.data[n].code[j]=CODE_NONE;
	}
}
