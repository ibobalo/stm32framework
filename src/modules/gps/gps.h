#ifndef GPS_H_INCLUDED
#define GPS_H_INCLUDED

#include "tick_process.h"

#define DEGTORAD(X)                    ((X) * M_PI / 180)
#define RADTODEG(X)                    ((X) * 180 / M_PI)
#define KNOTSTOMPERS(X)                ((X)*1852)
#define METERS_PER_DEGREE_LON(LAT_RAD) (111412.84 * cos(LAT_RAD) - 93.5 * cos(3 * (LAT_RAD))
#define METERS_PER_DEGREE_LAT(LAT_RAD) (111132.954 - 559.822 * cos(2 * (LAT_RAD)) + 1.175 * cos(4 * (LAT_RAD)))

double DDmm2DD(const double& dDD);
void CalcMetersPerRadian(double dLat, double* pdRetMetersPerRadianLat, double* pdRetMetersPerRadianLon);
void CalcLLA2Diff(
		double lat1, double lon1, double height1,
		double lat2, double lon2, double height2,
		double* pdRetLatDiff, double* pdRetLonDiff, double* pdRetHeightDiff
);
void CalcLLA2Center(
		double lat1, double lon1, double height1,
		double lat2, double lon2, double height2,
		double* pdRetCenterLat, double* pdRetCenterLon, double* pdRetCenterHeight
);
void ECEFtoLLA(double x, double y, double z, double* pdRetLat, double* pdRetLon, double* pdRetHeight);
void LLAtoECEF(double lat, double lon, double height, double* pdRetX, double* pdRetY, double* pdRetZ);
void ECEF2toAzimuthElevationDistance(
		double x1, double y1, double z1,
		double x2, double y2, double z2,
		double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance
);
void ECEFDiffToAzimuthElevationDistance(
		double x, double y, double z,
		double dx, double dy, double dz,
		double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance
);
void LLA2toAzimuthElevationDistance(
		double lat1, double lon1, double height1,
		double lat2, double lon2, double height2,
		double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance
);
void CalcECEF2Diff(
		double x1, double y1, double z1,
		double x2, double y2, double z2,
		double* pdRetXDiff, double* pdRetYDiff, double* pdRetZDiff
);
void CalcECEF2Center(
		double x1, double y1, double z1,
		double x2, double y2, double z2,
		double* pdRetCenterX, double* pdRetCenterY, double* pdRetCenterZ
);

class IGpsNotifier
{
public: // IGpsNotifier
	virtual void OnGpsFixed() = 0;
	virtual void OnGpsTimeUpdate() = 0;
	virtual void OnGpsPosUpdate() = 0;
	virtual void OnGpsSatUpdate() = 0;
	virtual void OnGpsPkt() = 0;
};

class CGpsInfo
{
public:
	typedef CTickProcessScheduler::CTimeType CTimeType;
	typedef CTickProcessScheduler::CTimeDelta CTimeDelta;
	typedef enum {
		PS_UNKNOWN,
		PS_GP, // GPS
		PS_GA, // Galileo
		PS_GL, // GLONASS
		PS_GB, // BeiDou
		PS_GN, // Mixed GPS and GLONASS
	} ESatSystem;
	typedef struct {
		ESatSystem eSystem;
		unsigned uiElev;
		unsigned uiAzim;
		unsigned uiCNdB;
		CTickProcessScheduler::CTimeType ttUpdateTime;
	} TSatInfo;
public:
	void RegisterGpsNotifier(IGpsNotifier* pNotifier) {m_pNotifier = pNotifier;}

	bool GetIsExpired() const {return g_TickProcessScheduller.GetTimeNow() > m_ttLastPktTime + 5000;}
	unsigned GetSatsInView() const {return m_nSatsInView;}
	unsigned GetSatsUsed() const {return m_nSatsUsed;}
	const TSatInfo& GetSatInfo(unsigned nSatIdx) const {ASSERTE(nSatIdx < ARRAY_SIZE(m_aSatInfos)); return m_aSatInfos[nSatIdx];}
	unsigned GetFixTimestamp() const {return m_uiFixTimeStamp;}
	unsigned GetFixDateStamp() const {return m_uiFixDateStamp;}
	unsigned GetUtcTime() const {return m_uiUtcTime;}
	unsigned GetUtcDate() const {return m_uiUtcDate;}

	bool GetIsFixed() const {return m_bFixed;}
	CTimeType GetPosUpdateTime() const {return m_ttLLAPosUpdateTime;}
	const double& GetPosLat() const {return m_dLLAPosLat;}
	const double& GetPosLon() const {return m_dLLAPosLon;}
	const double& GetPDOP() const {return m_dLLAPosDilution;}
	const double& GetHDOP() const {return m_dLLAHorDilution;}
	const double& GetVDOP() const {return m_dLLAVerDilution;}
	bool CalcLLAIfNecessary();
	bool CalcECEFIfNecessary();
	bool GetIsLLAExpired() const {return g_TickProcessScheduller.GetTimeNow() > m_ttLLAPosUpdateTime + 5000;}
	bool GetPosLLA(double* pdRetLat, double* pdRetLon, double* pdRetHeight) const;
	bool GetIsECEFExpired() const {return g_TickProcessScheduller.GetTimeNow() > m_ttECEFPosUpdateTime + 5000;}
	bool GetPosECEF(double* pdRetX, double* pdRetY, double* pdRetZ) const;

	bool GetPOVAzimuthElevationDistance(const CGpsInfo* pTgtPos, double* pdRetAzimuth, double* pdRetElevation, double* pdRetDistance) const;
	bool GetPOVDiffLLA(const CGpsInfo* pTgtPos, double* pdRetLatDiff, double* pdRetLonDiff, double* pdRetHeight) const;
	bool GetPOVDiffMetersLLA(const CGpsInfo* pTgtPos, double* pdRetLatDiffMeters, double* pdRetLonDiffMeters, double* pdRetHeightMeters) const;
	bool GetCenterLLA(const CGpsInfo* pTgtPos, double* pdRetCenterLat, double* pdRetCenterLon, double* pdRetCenterHeight) const;

	bool GetPOVDiffECEF(const CGpsInfo* pTgtPos, double* pdRetXDiff, double* pdRetYDiff, double* pdRetZDiff) const;

protected:
	void Init();
	void NotifyFixed() const {if (m_pNotifier) m_pNotifier->OnGpsFixed();}
	void NotifyTimeUpdate() const {if (m_pNotifier) m_pNotifier->OnGpsTimeUpdate();}
	void NotifyPosUpdate() const {if (m_pNotifier) m_pNotifier->OnGpsPosUpdate();}
	void NotifySatUpdate() const {if (m_pNotifier) m_pNotifier->OnGpsSatUpdate();}
	void NotifyPacket() const {if (m_pNotifier) m_pNotifier->OnGpsPkt();}

	IGpsNotifier* m_pNotifier;

	unsigned   m_nSatsInView;
	TSatInfo   m_aSatInfos[64];
	unsigned   m_nSatsUsed;
	unsigned   m_auiSatID[12];

	unsigned   m_uiUtcTime; //secs from midnight
	unsigned   m_uiUtcDate; //days since 1970

	bool       m_bFixed;
	unsigned   m_uiFixTimeStamp;
	unsigned   m_uiFixDateStamp;
	CTimeType  m_ttLLAPosUpdateTime;
	double     m_dLLAPosLat;               // rad
	double     m_dLLAPosLon;               // rad
	double     m_dLLAPosDilution;
	double     m_dLLAHorDilution;
	double     m_dLLAVerDilution;
	CTimeType  m_ttLLAHeightUpdateTime;
	double     m_dLLAHeight;               // m
	double     m_dLLASpeed;                // m/s
	double     m_dLLACourse;               // rad
	CTimeType  m_ttLLASpeedUpdateTime;
	double     m_dECEFPosX;                // m
	double     m_dECEFPosY;                // m
	double     m_dECEFPosZ;                // m
	double     m_dECEFDilution;
	CTimeType  m_ttECEFPosUpdateTime;
	double     m_dECEFVelX;                // m/s
	double     m_dECEFVelY;                // m/s
	double     m_dECEFVelZ;                // m/s
	CTimeType  m_ttECEFSpeedUpdateTime;
	CTimeType  m_ttLastPktTime;
};

#endif // GPS_H_INCLUDED
