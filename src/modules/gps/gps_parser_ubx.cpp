#include "stdafx.h"
#include <stdint.h>
#include <math.h>
#include "gps_parser.h"
#include "gps_parser_ubx_const.h"

#define UBX_DEBUG_INFO(...) DEBUG_INFO("GPS UBX:" __VA_ARGS__)

UBX_PKT_DEFINITION(NAV_BASE, 0x00, 0x01,
)
UBX_PKT_DEFINITION(NAV_POSECEF, UBX_CLASS_NAV, 0x01,
		UBX_U4 iTOW_ms;
		UBX_I4 ecefX_cm;
		UBX_I4 ecefY_cm;
		UBX_I4 ecefZ_cm;
		UBX_U4 pAcc_cm;
)
UBX_PKT_DEFINITION(NAV_POSLLH, UBX_CLASS_NAV, 0x02,
		UBX_U4 iTOW_ms;
		UBX_I4 lon_deg7;
		UBX_I4 lat_deg7;
		UBX_I4 height_mm;
		UBX_I4 hMSL_mm;
		UBX_U4 hAcc_mm;
		UBX_U4 vAcc_mm;
)
UBX_PKT_DEFINITION(NAV_STATUS, UBX_CLASS_NAV, 0x03,
		UBX_U4 iTOW_ms;
		UBX_U1 gpsFix;
		UBX_X1 flags;
		UBX_X1 fixStat;
		UBX_X1 flags2;
		UBX_U4 ttff;
		UBX_U4 msss;
)
UBX_PKT_DEFINITION(NAV_SOL, UBX_CLASS_NAV, 0x06,
		UBX_U4 iTOW_ms;
		UBX_I4 fTOW_ns;
		UBX_I2 week;
		UBX_U1 gpsFix;
		UBX_X1 flags;
		UBX_I4 ecefX_cm;
		UBX_I4 ecefY_cm;
		UBX_I4 ecefZ_cm;
		UBX_U4 pAcc_cm;
		UBX_I4 ecefVX_cmps;
		UBX_I4 ecefVY_cmps;
		UBX_I4 ecefVZ_cmps;
		UBX_U4 sAcc_cmps;
		UBX_U2 pDOP_x100;
		UBX_U1 reserved1;
		UBX_U1 numSV;
		UBX_U4 reserved2;
)
UBX_PKT_DEFINITION(NAV_PVT, UBX_CLASS_NAV, 0x07,
		UBX_U4 iTOW_ms;
		UBX_U2 year;
		UBX_U1 month;
		UBX_U1 day;
		UBX_I1 hour;
		UBX_I1 min;
		UBX_I1 sec;
		UBX_X1 valid;
		UBX_U4 tAcc_ns;
		UBX_I4 nano_ns;
		UBX_U1 fixType;
		UBX_X1 flags;
		UBX_U1 reserved1;
		UBX_U1 numSV;
		UBX_I4 lon_deg7;
		UBX_I4 lat_deg7;
		UBX_I4 height_mm;
		UBX_I4 hMSL_mm;
		UBX_U4 hAcc_mm;
		UBX_U4 vAcc_mm;
		UBX_I4 velN_mms;
		UBX_I4 velE_mms;
		UBX_I4 velD_mms;
		UBX_I4 gSpeed_mms;
		UBX_I4 heading_deg5;
		UBX_U4 sAcc_mms;
		UBX_U4 headingAcc_deg5;
		UBX_U2 pDOP_x100;
		UBX_X2 reserved2;
		UBX_U4 reserved3;
)

UBX_PKT_DEFINITION(NAV_VELNED, UBX_CLASS_NAV, 0x12,
		UBX_U4 iTOW_ms;
		UBX_I4 velN_cms;
		UBX_I4 velE_cms;
		UBX_I4 velD_cms;
		UBX_U4 speed_cms;
		UBX_U4 gSpeed_cms;
		UBX_I4 heading_deg5;
		UBX_U4 sAcc_cms;
		UBX_U4 headingAcc_deg5;
)

UBX_PKT_DEFINITION(INF_ERROR, UBX_CLASS_INF, 0x00,
		char str[];
)

UBX_PKT_DEFINITION(INF_WARNING, UBX_CLASS_INF, 0x01,
		char str[];
)

UBX_PKT_DEFINITION(INF_NOTICE, UBX_CLASS_INF, 0x02,
		char str[];
)

UBX_PKT_DEFINITION(INF_TEST, UBX_CLASS_INF, 0x03,
		char str[];
)

UBX_PKT_DEFINITION(INF_DEBUG, UBX_CLASS_INF, 0x04,
		char str[];
)

UBX_PKT_DEFINITION(MON_IO, UBX_CLASS_MON, 0x02,
		struct {
			UBX_U4 rxBytes;
			UBX_U4 txBytes;
			UBX_U2 parityErrs;
			UBX_U2 framingErrs;
			UBX_U2 overrunErrs;
			UBX_U2 breakCond;
			UBX_U1 rxBusy;
			UBX_U1 txBusy;
			UBX_U2 reserved;
		} io[];
)

UBX_PKT_DEFINITION(MON_VER, UBX_CLASS_MON, 0x04,
		UBX_CH swVersion[30];
		UBX_CH hwVersion[10];
		struct {
			UBX_CH extension[30];
		} ext[];
)

UBX_PKT_DEFINITION(MON_MSGPP, UBX_CLASS_MON, 0x06,
		UBX_U2 msg1[8];
		UBX_U2 msg2[8];
		UBX_U2 msg3[8];
		UBX_U2 msg4[8];
		UBX_U2 msg5[8];
		UBX_U2 msg6[8];
		UBX_U4 skipped[6];
)

UBX_PKT_DEFINITION(MON_HW, UBX_CLASS_MON, 0x09,
		UBX_X4 pinSel;
		UBX_X4 pinBank;
		UBX_X4 pinDir;
		UBX_X4 pinVal;
		UBX_U2 noisePerMS;
		UBX_U2 agtCnt;
		UBX_U1 aStatus;
		UBX_U1 aPower;
		UBX_X1 flags;
		UBX_U1 reserved1;
		UBX_X4 usedMask;
		UBX_U1 VP[17];
		UBX_U1 jamInd;
		UBX_U2 reserved3;
		UBX_U4 pinIrq;
		UBX_U4 pullH;
		UBX_U4 pullL;
)

UBX_PKT_DEFINITION(MON_HW2, UBX_CLASS_MON, 0x0B,
		UBX_I1 ofsI;
		UBX_U1 magI;
		UBX_I1 ofsQ;
		UBX_U1 magQ;
		UBX_U1 cfgSource;
		UBX_U1 reserved0[3];
		UBX_U4 lowLevCfg;
		UBX_U4 reserved1[2];
		UBX_U4 portStatus;
		UBX_U4 reserved2;
)


typedef enum {
	UBX_START,    // 0xB5
	UBX_START2,   // 0x62
	UBX_CLASS,
	UBX_ID,
	UBX_LEN_LO,
	UBX_LEN_HI,
	UBX_PAYLOAD,
	UBX_CRC_A,
	UBX_CRC_B,
} EParserState;


void CGpsParser::ParserUBXReset()
{
	m_eUBXPktParserState = UBX_START;
	m_nUBXPktPayloadLengthReceived = 0;
	m_uiUBXPktCrcA = 0;
	m_uiUBXPktCrcB = 0;
	UBX_MSG_NAV_BASE_t* pMsgHdr = (UBX_MSG_NAV_BASE_t*)m_aUBXPkt;
	pMsgHdr->nMsgClass = 0;
	pMsgHdr->nMsgId = 0;
	pMsgHdr->nPayloadLength = 0;
}

CGpsParser::EParseResult CGpsParser::ParseUBX(char ch)
{
	EParseResult eResult = PR_CONTINUE;
	const uint8_t uiByte = (uint8_t)ch;
	UBX_MSG_NAV_BASE_t* pMsgHdr = (UBX_MSG_NAV_BASE_t*)m_aUBXPkt;

	switch (m_eUBXPktParserState) {
		case UBX_START:
			if (uiByte == 0xB5) {m_eUBXPktParserState = UBX_START2; m_ttPacketStartTime = g_TickProcessScheduller.GetTimeNow(); UBX_DEBUG_INFO("message start");}
			else {eResult = PR_ERROR;}
			break;
		case UBX_START2:
			if (uiByte == 0x62) {m_eUBXPktParserState = UBX_CLASS; ASSERTE(m_uiUBXPktCrcA == 0); ASSERTE(m_uiUBXPktCrcB == 0);}
			else {eResult = PR_ERROR;}
			break;
		case UBX_CLASS:
			pMsgHdr->nMsgClass = uiByte;
			m_eUBXPktParserState = UBX_ID;
			m_uiUBXPktCrcA += uiByte;
			m_uiUBXPktCrcB += m_uiUBXPktCrcA;
			break;
		case UBX_ID:
			pMsgHdr->nMsgId = uiByte;
			m_uiUBXPktCrcA += uiByte;
			m_uiUBXPktCrcB += m_uiUBXPktCrcA;
			ASSERTE(pMsgHdr->nPayloadLength == 0);
			m_eUBXPktParserState = UBX_LEN_LO;
			break;
		case UBX_LEN_LO:
			m_eUBXPktParserState = UBX_LEN_HI;
			m_uiUBXPktCrcA += uiByte;
			m_uiUBXPktCrcB += m_uiUBXPktCrcA;
			pMsgHdr->nPayloadLength = uiByte;
			break;
		case UBX_LEN_HI:
			pMsgHdr->nPayloadLength = pMsgHdr->nPayloadLength + (uiByte << 8);
			m_uiUBXPktCrcA += uiByte;
			m_uiUBXPktCrcB += m_uiUBXPktCrcA;
			if (pMsgHdr->nPayloadLength <= sizeof(m_aUBXPkt) - sizeof(pMsgHdr)) {m_eUBXPktParserState = UBX_PAYLOAD; ASSERTE(m_nUBXPktPayloadLengthReceived == 0);}
			else {eResult = PR_ERROR; UBX_DEBUG_INFO("msg too long :", pMsgHdr->nPayloadLength);}
			break;
		case UBX_PAYLOAD:
			ASSERTE(m_nUBXPktPayloadLengthReceived < sizeof(m_aUBXPkt) - sizeof(pMsgHdr));
			m_aUBXPkt[m_nUBXPktPayloadLengthReceived] = uiByte;
			m_uiUBXPktCrcA += uiByte;
			m_uiUBXPktCrcB += m_uiUBXPktCrcA;
			++m_nUBXPktPayloadLengthReceived;
			if (m_nUBXPktPayloadLengthReceived >= pMsgHdr->nPayloadLength) {m_eUBXPktParserState = UBX_CRC_A;}
			break;
		case UBX_CRC_A:
			if (m_uiUBXPktCrcA == uiByte) {m_eUBXPktParserState = UBX_CRC_B;}
			else {eResult = PR_ERROR;}
			break;
		case UBX_CRC_B:
			if (m_uiUBXPktCrcB == uiByte) {ProcessUBXMsg(); ParserReset(); eResult = PR_DONE;}
			else {eResult = PR_ERROR;}
			break;
	}
	if (eResult == PR_ERROR && m_eUBXPktParserState != UBX_START) {UBX_DEBUG_INFO("message error state:", m_eUBXPktParserState); ParserUBXReset();}
	return eResult;
}

#define SET(tgt, val, changed) { \
	__typeof__ (tgt) _val = (val); \
	if (tgt != _val) {tgt = _val; changed = true;} \
}
#define PREPARE_SETS \
		bool bFixedChanged = false; \
		bool bTimeChanged = false; \
		bool bPosChanged = false; \
		bool bSatChanged = false;
#define SET_FIX(tgt, val) SET(tgt, val, bFixedChanged)
#define SET_TIME(tgt, val) SET(tgt, val, bTimeChanged)
#define SET_POS(tgt, val) SET(tgt, val, bPosChanged)
#define SET_SAT(tgt, val) SET(tgt, val, bSatChanged)
#define NOTIFY_CHANGES \
		if (bFixedChanged) {NotifyFixed();} \
		if (bTimeChanged) {NotifyTimeUpdate();} \
		if (bPosChanged) {NotifyPosUpdate();} \
		if (bSatChanged) {NotifySatUpdate();}

void CGpsParser::ProcessUBXMsg()
{
	if (GetIsExpired()) { m_nUBXConfigStep = 0; m_bUBXGotVersion = false; }
	PREPARE_SETS
	m_ttLastPktTime = m_ttPacketStartTime;
	UBX_MSG_NAV_BASE_t* pMsgHdr = (UBX_MSG_NAV_BASE_t*)m_aUBXPkt;
	unsigned uiMsgKey = UBX_KEY(pMsgHdr->nMsgClass, pMsgHdr->nMsgId);
	switch (uiMsgKey) {
		case UBX_KEY_NAV_POSECEF: { //  Position Solution in ECEF
			const UBX_MSG_NAV_POSECEF_t* pMsg = (const UBX_MSG_NAV_POSECEF_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				UBX_DEBUG_INFO("NAV-POSECEF:", (int)pMsg->iTOW_ms);
				SET_POS(m_uiFixTimeStamp, pMsg->iTOW_ms/1000)
				SET_POS(m_dECEFPosX, 1E-2L * pMsg->ecefX_cm);
				SET_POS(m_dECEFPosY, 1E-2L * pMsg->ecefY_cm);
				SET_POS(m_dECEFPosZ, 1E-2L * pMsg->ecefZ_cm);
				SET_POS(m_dECEFDilution, 1E-2L * pMsg->pAcc_cm);
				m_ttECEFPosUpdateTime = m_ttPacketStartTime;
				NOTIFY_CHANGES
			} else {
				UBX_DEBUG_INFO("NAV-POSLLH error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_NAV_POSLLH: { // Geodetic Position Solution
			const UBX_MSG_NAV_POSLLH_t* pMsg = (const UBX_MSG_NAV_POSLLH_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				UBX_DEBUG_INFO("NAV_POSLLH:", (int)pMsg->iTOW_ms);
				SET_POS(m_uiFixTimeStamp, pMsg->iTOW_ms/1000)
				SET_POS(m_dLLAPosLat, DEGTORAD(1E-7L * pMsg->lat_deg7));
				SET_POS(m_dLLAPosLon, DEGTORAD(1E-7L * pMsg->lon_deg7));
				m_ttLLAPosUpdateTime = m_ttPacketStartTime;
				SET_POS(m_dLLAHeight, 0.001L * pMsg->height_mm);
				m_ttLLAHeightUpdateTime = m_ttPacketStartTime;
				SET_POS(m_dLLAHorDilution, 0.001L * pMsg->hAcc_mm);
				SET_POS(m_dLLAVerDilution, 0.001L * pMsg->vAcc_mm);
				NOTIFY_CHANGES
			} else {
				UBX_DEBUG_INFO("NAV-POSLLH error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_NAV_STATUS: { // Receiver Navigation Status
			const UBX_MSG_NAV_STATUS_t* pMsg = (const UBX_MSG_NAV_STATUS_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				UBX_DEBUG_INFO("NAV-STATUS:", pMsg->gpsFix);
				SET_FIX(m_bFixed, (pMsg->flags & 0x01)); //gpsFixOk
				NOTIFY_CHANGES
			} else {
				UBX_DEBUG_INFO("NAV-STATUS error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_NAV_SOL: { // Navigation Solution Information
			const UBX_MSG_NAV_SOL_t* pMsg = (const UBX_MSG_NAV_SOL_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				UBX_DEBUG_INFO("NAV_POSECEF:", (int)pMsg->iTOW_ms);
				SET_POS(m_uiFixTimeStamp, pMsg->iTOW_ms/1000)
				SET_POS(m_dECEFPosX, 1E-2L * pMsg->ecefX_cm);
				SET_POS(m_dECEFPosY, 1E-2L * pMsg->ecefY_cm);
				SET_POS(m_dECEFPosZ, 1E-2L * pMsg->ecefZ_cm);
				SET_POS(m_dECEFDilution, 1E-2L * pMsg->pAcc_cm);
				m_ttECEFPosUpdateTime = m_ttPacketStartTime;
				SET_POS(m_dECEFVelX, 1E-2L * pMsg->ecefVX_cmps);
				SET_POS(m_dECEFVelY, 1E-2L * pMsg->ecefVY_cmps);
				SET_POS(m_dECEFVelZ, 1E-2L * pMsg->ecefVZ_cmps);
				m_ttECEFSpeedUpdateTime = m_ttPacketStartTime;
				SET_SAT(m_nSatsUsed, pMsg->numSV);
				NOTIFY_CHANGES
			} else {
				UBX_DEBUG_INFO("NAV-POSLLH error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_NAV_PVT: { // Navigation Position Velocity Time Solution
			const UBX_MSG_NAV_PVT_t* pMsg = (const UBX_MSG_NAV_PVT_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				UBX_DEBUG_INFO("NAV_PVT:", (int)pMsg->iTOW_ms);
				SET_FIX(m_bFixed, (pMsg->flags & 0x01)); //gnssFixOK
				SET_POS(m_uiFixTimeStamp, pMsg->iTOW_ms/1000)
				SET_POS(m_dLLAPosLat, DEGTORAD(1E-7L * pMsg->lat_deg7));
				SET_POS(m_dLLAPosLon, DEGTORAD(1E-7L * pMsg->lon_deg7));
				m_ttLLAPosUpdateTime = m_ttPacketStartTime;
				SET_POS(m_dLLAHeight, 0.001L * pMsg->height_mm);
				m_ttLLAHeightUpdateTime = m_ttPacketStartTime;
				SET_POS(m_dLLAHorDilution, 0.001L * pMsg->hAcc_mm);
				SET_POS(m_dLLAVerDilution, 0.001L * pMsg->vAcc_mm);
				SET_SAT(m_nSatsUsed, pMsg->numSV);
				SET_POS(m_dLLASpeed, 0.001L * pMsg->gSpeed_mms);
				SET_POS(m_dLLACourse, DEGTORAD(1E-5L * pMsg->heading_deg5));
				m_ttLLASpeedUpdateTime = m_ttPacketStartTime;
				NOTIFY_CHANGES
			} else {
				UBX_DEBUG_INFO("NAV-PVT error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_NAV_VELNED: { // Velocity Solution in NED
			const UBX_MSG_NAV_VELNED_t* pMsg = (const UBX_MSG_NAV_VELNED_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				UBX_DEBUG_INFO("NAV-VELNED:", (int)pMsg->iTOW_ms);
				SET_POS(m_dLLASpeed, 0.01L * pMsg->gSpeed_cms);
				SET_POS(m_dLLACourse, DEGTORAD(1E-5L * pMsg->heading_deg5));
				m_ttLLASpeedUpdateTime = m_ttPacketStartTime;
			}
		} break;
		case UBX_KEY_MON_VER: { // I/O Subsystem Status requested by "B5 62 0A 04 00 00 0E 34"
			const UBX_MSG_MON_VER_t* pMsg = (const UBX_MSG_MON_VER_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				m_bUBXGotVersion = true;
				const unsigned nExtensions = (pMsg->nPayloadLength - sizeof(*pMsg)) / sizeof(pMsg->ext[0]);
				UBX_DEBUG_INFO("MON-VER, exts:", nExtensions);
				for (unsigned n = 0; n < nExtensions; ++n) {
					// todo
				}
			} else {
				UBX_DEBUG_INFO("MON-VER error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_MON_IO: { // I/O Subsystem Status
			const UBX_MSG_MON_IO_t* pMsg = (const UBX_MSG_MON_IO_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= (sizeof(*pMsg) - sizeof(*pMsgHdr))) {
				const unsigned nIOs = (pMsg->nPayloadLength - sizeof(*pMsg)) / (sizeof(pMsg->io[0]));
				UBX_DEBUG_INFO("MON-IO, N:", nIOs);
				for (unsigned n = 0; n < nIOs; ++n) {
					//pMsg->io[n].rxBytes
				}
			} else {
				UBX_DEBUG_INFO("MON-IO error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_INF_ERROR: { // ASCII String output, indicating an error
			const UBX_MSG_INF_ERROR_t* pMsg = (const UBX_MSG_INF_ERROR_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= (sizeof(*pMsg) - sizeof(*pMsgHdr))) {
				//lasterror.assign(pMsg->str, pMsg->nPayloadLength)
				UBX_DEBUG_INFO("INF-ERROR reported. length:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_INF_WARNING: { // ASCII String output, indicating a warning
			const UBX_MSG_INF_WARNING_t* pMsg = (const UBX_MSG_INF_WARNING_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= (sizeof(*pMsg) - sizeof(*pMsgHdr))) {
				//lastwarning.assign(pMsg->str, pMsg->nPayloadLength)
				UBX_DEBUG_INFO("INF-WARNING reported. length:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_INF_NOTICE: { // ASCII String output, with informational contents
			const UBX_MSG_INF_NOTICE_t* pMsg = (const UBX_MSG_INF_NOTICE_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= (sizeof(*pMsg) - sizeof(*pMsgHdr))) {
				//lastnotice.assign(pMsg->str, pMsg->nPayloadLength)
				UBX_DEBUG_INFO("INF-NOTICE reported. length:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_INF_TEST: { // ASCII String output, indicating test output
			const UBX_MSG_INF_TEST_t* pMsg = (const UBX_MSG_INF_TEST_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= (sizeof(*pMsg) - sizeof(*pMsgHdr))) {
				//lasttest.assign(pMsg->str, pMsg->nPayloadLength)
				UBX_DEBUG_INFO("INF-TEST reported. length:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_INF_DEBUG: { // ASCII String output, indicating debug output
			const UBX_MSG_INF_DEBUG_t* pMsg = (const UBX_MSG_INF_DEBUG_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength >= (sizeof(*pMsg) - sizeof(*pMsgHdr))) {
				//lasttest.assign(pMsg->str, pMsg->nPayloadLength)
				UBX_DEBUG_INFO("INF-DEBUG reported. length:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_MON_MSGPP: { // Message Parse and Process Status
			const UBX_MSG_MON_MSGPP_t* pMsg = (const UBX_MSG_MON_MSGPP_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				// pMsg->
			} else {
				UBX_DEBUG_INFO("MON-MSGPP error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_MON_HW: { // Hardware Status
			const UBX_MSG_MON_HW_t* pMsg = (const UBX_MSG_MON_HW_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				// pMsg->jamInd
			} else {
				UBX_DEBUG_INFO("MON-HW error:", pMsg->nPayloadLength);
			}
		} break;
		case UBX_KEY_MON_HW2: { // Extended Hardware Status
			const UBX_MSG_MON_HW2_t* pMsg = (const UBX_MSG_MON_HW2_t*)m_aUBXPkt;
			if (pMsg->nPayloadLength == sizeof(*pMsg) - sizeof(*pMsgHdr)) {
				// pMsg->cfgSource
			} else {
				UBX_DEBUG_INFO("MON-HW2 error:", pMsg->nPayloadLength);
			}
		} break;
		default:
#ifdef USE_GPS_RAW
			ProcessUBXMsgRaw(uiMsgKey);
#endif
			UBX_DEBUG_INFO("Pkt ignored, type:", uiMsgKey);
	}
	NotifyPacket();

	switch (m_nUBXConfigStep) {
	case 0: { // baudrate and protocols for UART1 using $PUBX (if inNMEA allowed)
		static const char UBX_BAUDRATE_REQUEST[] = "$PUBX,41,1,0001,0001,115200,0*1C\r\n";
		if (ScheduleResponce(UBX_BAUDRATE_REQUEST, sizeof(UBX_BAUDRATE_REQUEST) - 1)) {
			++m_nUBXConfigStep;
		}
	} break;
	case 1: { // baudrate and protocols for UART1 using CFG-PRT (if inUBX allowed)
		static const uint8_t UBX_BAUDRATE_REQUEST[] = {
				0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00,
				0x00, 0xC2, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB8, 0x42};
		if (ScheduleResponce(UBX_BAUDRATE_REQUEST, sizeof(UBX_BAUDRATE_REQUEST) - 1)) {
			++m_nUBXConfigStep;
		}
	} break;
	case 2: { // MON-VER poll
		static const uint8_t UBX_VERSION_REQUEST[] = {0xB5, 0x62, 0x0A, 0x04, 0x00, 0x00, 0x0E, 0x34};
		if (ScheduleResponce(UBX_VERSION_REQUEST, sizeof(UBX_VERSION_REQUEST))) {
			++m_nUBXConfigStep;
		}
	} break;
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
	case 9: { // wait MON-VER info
		if (m_bUBXGotVersion) {
			m_nUBXConfigStep = 100; // raw
		}
	} break;
	case 10: { // repeat ver info pool
		m_nUBXConfigStep = 2;
	} break;
	}
}
