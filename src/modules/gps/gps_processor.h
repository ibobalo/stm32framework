#ifndef GPS_PROCESSOR_H_INCLUDED
#define GPS_PROCESSOR_H_INCLUDED

#include "interfaces/serial.h"
#include "gps_parser.h"

#define GPSMSGMAXSIZE   90

class CGpsProcessor:
		public CGpsParser,
		public ISerialRxNotifier,
		public ISerialTxNotifier
{
public:
	void Init(ISerial* pSerial);

public: // ISerialRxNotifier
	virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk); //returns true in next chunk set
	virtual bool NotifyRxDone(CIoChunk* pRetNextChunk); //returns true in next chunk set
	virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk);
public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);

private:
	bool ParseReceived();
	void ProcessRequest();

private:
	ISerial*      m_pSerial;
	volatile char m_acMessageBuf[GPSMSGMAXSIZE];
	unsigned      m_nReceivedCharsCount;
	unsigned      m_nParsedCharsCount;
	bool          m_bTxBusy;

};

CGpsProcessor* AcquireGpsProcessor(ISerial* pSerial);

#endif // GPS_PROCESSOR_H_INCLUDED
