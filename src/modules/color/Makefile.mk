INCLUDE_PATH += $(if $(filter YES,$(USE_COLOR)), \
	src/modules/color \
)

SRC += $(if $(filter YES,$(USE_COLOR)), $(addprefix src/modules/color/, \
	color.cpp \
))

USE_MATHLIB += $(if $(filter YES,$(USE_COLOR)), \
	YES \
)

USE_TRANSFORMER += $(if $(filter YES,$(USE_COLOR)), \
	YES \
)
