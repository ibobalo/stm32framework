#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

#include "util/pair.h"
#include "modules/ramp/transformer.h"

typedef tripple<float, float, float> CColorRGB;
typedef tripple<float, float, float> CColorHSV;
static inline CColorRGB make_color_rgb(float fR, float fG, float fB) {return make_tripple(fR, fG, fB);}
static inline CColorHSV make_color_hsv(float fH, float fS, float fV) {return make_tripple(fH, fS, fV);}

typedef tripple<uint8_t, uint8_t, uint8_t> CColorRGB8;
typedef tripple<uint8_t, uint8_t, uint8_t> CColorHSV8;
static inline CColorRGB8 make_color_rgb8(uint8_t fR, uint8_t fG, uint8_t fB) {return make_tripple(fR, fG, fB);}
static inline CColorHSV8 make_color_hsv8(uint8_t fH, uint8_t fS, uint8_t fV) {return make_tripple(fH, fS, fV);}

CColorRGB HsvToRgb(const CColorHSV tInValue);
CColorRGB8 Hsv8ToRgb8(const CColorHSV8 tInValue);

ITransformer< CColorRGB, CColorHSV >* GetHsvToRgbTransformer();
ITransformer< CColorRGB8, CColorHSV8 >* GetHsv8ToRgb8Transformer();

#endif // COLOR_H_INCLUDED
