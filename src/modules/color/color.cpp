#include "stdafx.h"
#include "color.h"
#include "transformer.h"
#include "util/macros.h" // ASSERT IMPLEMENTS_INTERFACE_METHOD
#include <math.h>

CColorRGB HsvToRgb(const CColorHSV tInValue)
{
	float H = tInValue.first;
	float S = tInValue.second;
	float V = tInValue.third;

	unsigned Hi = floorf(H * 6.0);
	float Vmin = (1.0 - S) * V;
	float fInt;
	float a = (V - Vmin) * modff(H * 6, &fInt);
	float Vinc = Vmin + a;
	float Vdec = V - a;
	switch(Hi) {
		case 0: return make_color_rgb(V,    Vinc, Vmin); break;
		case 1: return make_color_rgb(Vdec, V,    Vmin); break;
		case 2: return make_color_rgb(Vmin, V,    Vinc); break;
		case 3: return make_color_rgb(Vmin, Vdec, V   ); break;
		case 4: return make_color_rgb(Vinc, Vmin, V   ); break;
		case 5: return make_color_rgb(V,    Vmin, Vdec); break;
	}
	return make_color_rgb((float)0.0, (float)0.0, (float)0.0);
}

CColorRGB8 Hsv8ToRgb8(const CColorHSV8 tInValue)
{
	unsigned H = tInValue.first * 6;     // [0,1530]
	unsigned S = tInValue.second;        // [0,255]
	unsigned V = tInValue.third;         // [0,255]

	unsigned Hi = H / 256;               // [0,5]
	unsigned Hf = H % 256;               // [0,255]
	unsigned Vmin = (255 - S) * V;       // [0,65535]
	unsigned a = (V * 256 - Vmin) * Hf / 65536;  // [0,255] V * (1 + S) * Hf
	unsigned Vinc = Vmin + a;            // [0,255]
	unsigned Vdec = V - a;               // [0,255]
	switch(Hi) {
		case 0: return make_color_rgb8(V,    Vinc, Vmin); break;
		case 1: return make_color_rgb8(Vdec, V,    Vmin); break;
		case 2: return make_color_rgb8(Vmin, V,    Vinc); break;
		case 3: return make_color_rgb8(Vmin, Vdec, V   ); break;
		case 4: return make_color_rgb8(Vinc, Vmin, V   ); break;
		case 5: return make_color_rgb8(V,    Vmin, Vdec); break;
	}
	return make_color_rgb8(0,0,0);
}
class CHsvToRgbTransformer:
		public ITransformer<CColorHSV, CColorRGB>
{
public: // ITransformer
	virtual CColorRGB Transform(const CColorHSV tInValue) const;
};

CColorRGB CHsvToRgbTransformer::Transform(const CColorHSV tInValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(ITransformer::Transform(tInValue));

	return HsvToRgb(tInValue);
}

class CHsv8ToRgb8Transformer:
		public ITransformer<CColorHSV8, CColorRGB8>
{
public: // ITransformer
	virtual CColorRGB8 Transform(const CColorHSV8 tInValue) const;
};

CColorRGB8 CHsv8ToRgb8Transformer::Transform(const CColorHSV8 tInValue) const
{
	IMPLEMENTS_INTERFACE_METHOD(ITransformer::Transform(tInValue));

	return Hsv8ToRgb8(tInValue);
}

CHsvToRgbTransformer g_HsvToRgbTransformer;
CHsv8ToRgb8Transformer g_Hsv8ToRgb8Transformer;

ITransformer<CColorHSV, CColorRGB>* GetHsvToRgbTransformer()
{
	return &g_HsvToRgbTransformer;
}
ITransformer<CColorHSV8, CColorRGB8>* GetHsv8ToRgb8Transformer()
{
	return &g_Hsv8ToRgb8Transformer;
}
