#ifndef NET_INTERFACE_H_INCLUDED
#define NET_INTERFACE_H_INCLUDED

#include "util/callback.h"
#include "util/chained_chunks.h"
#include <stdint.h>
#include <stddef.h>

/*
 * usage:
 * class CProtocolProcessor_Echo {
 * public:
 *  void Init(INetworkInterface pEn) {m_pEn = pEn; pEn->Init(NULL, BIND_MEM_CB(&CProtocolProcessor::OnPktReceived, this);};
 *  void OnPktReceived(unsigned uiPktDataSize) {m_uiRxSize = uiPktDataSize; ContinueAsap();};
 *  void OnTxDoneCallback(bool bTxSucceed)     {m_bBusy = false; ContinueAsap();};
 *  void SingleStep() {
 *  if (m_uiRxSize && !m_bBusy) {
 *    memcpy(   m_TgtMac,    m_pEn->GetRxedFrameSourceMacAddr(), 6);
 *    uint16_t uiFrameType = m_pEn->GetRxedFrameType();
 *    unsigned uiRxDataLen = m_pEn->GetRxedFrameSize();
 *    memcpy(   m_Buf,       m_pEn->GetRxedFrameData(), uiRxDataLen);
 *    m_pEn->ReleaseRxedFrame();
 *    if (PushTxPkt(m_TgtMac, m_Buf, uiFrameType, uiRxDataLen, BIND_MEM_CB(&CProtocolProcessor::OnTxDoneCallback, this))) {m_bBusy = true; m_uiRxSize = 0;};
 *  }
 */

class INetworkInterface
{
public:
	typedef Callback<void (unsigned)> CallbackOnPktReceived;
	typedef Callback<void (bool)>     CallbackOnLinkChanged;
	typedef Callback<void (void)>     CallbackOnIncomeCheck;
	typedef Callback<void (bool)>     CallbackOnTxDone;
	typedef struct {
		uint8_t b1,b2,b3,b4,b5,b6;
	} mac_addr_t;
public:
	virtual void BindNetworkInterface(CallbackOnPktReceived cbOnPktReceived = CallbackOnPktReceived(), CallbackOnLinkChanged cbOnLimkChanged = CallbackOnLinkChanged()) = 0;
	virtual void SetPktReceivedCallback(CallbackOnPktReceived cbOnPktReceived = CallbackOnPktReceived()) = 0;
	virtual void SetLinkChangedCallback(CallbackOnLinkChanged cbOnLinkChanged = CallbackOnLinkChanged()) = 0;
	virtual void SetIncomeCheckCallback(CallbackOnIncomeCheck cbOnIncomeCheck = CallbackOnIncomeCheck()) = 0;
	virtual void SetMacAddress(const mac_addr_t*) = 0;
	virtual const mac_addr_t* GetMacAddress() const = 0;
	virtual unsigned GetMTU() const = 0;

	virtual void ScheduleRefresh() = 0;
	virtual void Reset() = 0;

	virtual bool TxPkt(const mac_addr_t* TargetMACadr, uint16_t uiFrameType, CChainedConstChunks* pHeadChunk, CallbackOnTxDone cbOnTxDoneCallback = CallbackOnTxDone()) = 0;

	virtual unsigned          GetRxedFrameSize() const = 0;
	virtual uint16_t          GetRxedFrameType() const = 0;
	virtual const void*       GetRxedFrameData() const = 0;
	virtual const mac_addr_t* GetRxedFrameSourceMacAddr() const = 0;
	virtual const mac_addr_t* GetRxedFrameTargetMacAdr() const = 0;
	virtual void ReleaseRxedFrame() = 0;
};

#endif //NET_INTERFACE_H_INCLUDED
