#ifndef NET_STACK_H_INCLUDED
#define NET_STACK_H_INCLUDED

#include "net_if_en.h"
#include <stdint.h>
#include <stddef.h>

#define ETH_TYPE_ARP        htons((uint16_t)0x0806)
#define ETH_TYPE_IP         htons((uint16_t)0x0800)

#define IP_ADDR(a1,a2,a3,a4) (INetworkStack::ip_addr_t)((a1<<24) | (a2<<16) | (a3<<8) | (a4) )

// extern
class ILed;

class INetworkUdpListener
{
public: // INetworkUdpListener
	virtual const CChainedConstChunks* FilterIncomeUDP(const uint8_t* pData, unsigned uiSize) = 0;
};

class INetworkStack
{
public:
	typedef INetworkInterface::mac_addr_t mac_addr_t;
	typedef uint32_t ip_addr_t;

public:
	virtual void Init(INetworkInterface* pInterface, ip_addr_t uiIpAddr, ip_addr_t IpMask, ip_addr_t IpGwAddr) = 0;
	virtual void SetIpChecksumVerification(bool bVerification) = 0;

	virtual ip_addr_t GetIpAddress() const = 0;
	virtual ip_addr_t GetIpMask() const = 0;
	virtual ip_addr_t GetIpGwAddress() const = 0;

	virtual unsigned GetIPMTU() const = 0;
	virtual unsigned GetUDPMTU() const = 0;

	virtual bool SendFrame(const mac_addr_t* pTgtMacAddr, uint16_t uiFrameType, const CChainedConstChunks* pChain) = 0;
	virtual bool SendArp(ip_addr_t uiIpAddr) = 0;
	virtual bool SendIP(ip_addr_t uiIpAddr, uint8_t uiProtocol, const CChainedConstChunks* pChain) = 0;
	virtual bool SendUDP(ip_addr_t uiIpAddr, uint16_t uiPort, const CChainedConstChunks* pChain) = 0;

	virtual bool RegisterUDPListener(unsigned uiPort, INetworkUdpListener* pListener) = 0;
	virtual void SetPingLed(ILed* pLed) = 0;
	virtual void SetRxLed(ILed* pLed) = 0;
	virtual void SetTxLed(ILed* pLed) = 0;
	virtual void SetLiveLed(ILed* pLed) = 0;
};

INetworkStack* GetNetworkStackInterface();

#endif //NET_STACK_H_INCLUDED
