#ifndef NET_UARTSLIP_H_INCLUDED
#define NET_UARTSLIP_H_INCLUDED

#include "../net_if_en.h"
#include "hw/mcu/hw_uart.h"
#include "tick_process.h"

extern INetworkInterface* GetNetworkInterfaceUartSlip();
extern void SetNetworkInterfaceUartSlipParams(unsigned uiUartIndex, const TSerialConnectionParams* pParams, CTickProcessScheduler* pScheduller);

#endif //NET_UARTSLIP_H_INCLUDED
