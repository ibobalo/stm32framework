#include "stdafx.h"
#include "net_uartslip.h"
#include "hw/hw.h"
#include "hw/hw_macros.h"
#include "hw/debug.h"
#include "interfaces/notify.h"
#include "util/macros.h"
#include "util/utils.h"
#include "util/endians.h"
#include "../net_stack.h"

#define SLIP_END_CODE      0xC0
#define SLIP_ESC_CODE      0xDB
#define SLIP_ESC_END_CODE  0xDC
#define SLIP_ESC_ESC_CODE  0xDD

#define SLIP_RX_BUFF_SIZE 8
#define SLIP_MTU          1500

#define UARTSLIP_DEBUG_INFO(...) DEBUG_INFO("SLIP:" __VA_ARGS__)
//#define UARTSLIP_DEBUG_INFO(...) {}

class CNetworkInterfaceUartSlip :
		public INetworkInterface,
		public ISerialRxNotifier,
		public ISerialTxNotifier,
		public ISerialConnectionParamsNotifier,
		public CTickProcess
{
public:
	typedef INetworkInterface::mac_addr_t mac_addr_t;
public:
	// ~CNetworkInterfaceUartSlip();
	void InitUart(unsigned uiUartIndex, const TSerialConnectionParams* pParams, CProcessScheduller* pScheduller);
public: // INetworkInterface
	virtual void BindNetworkInterface(CallbackOnPktReceived cbOnPktReceived = CallbackOnPktReceived(), CallbackOnLinkChanged cbOnLinkChanged = CallbackOnLinkChanged());
	virtual void SetPktReceivedCallback(CallbackOnPktReceived cbOnPktReceived = CallbackOnPktReceived());
	virtual void SetLinkChangedCallback(CallbackOnLinkChanged cbOnLinkChanged = CallbackOnLinkChanged());
	virtual void SetIncomeCheckCallback(CallbackOnIncomeCheck cbOnIncomeCheck = CallbackOnIncomeCheck());
	virtual void SetMacAddress(const mac_addr_t* MacAaddress);
	virtual const mac_addr_t* GetMacAddress() const;
	virtual unsigned GetMTU() const;
	virtual void ScheduleRefresh();
	virtual void Reset();
	virtual bool TxPkt(const mac_addr_t* TargetMACadr, uint16_t uiFrameType, const CChainedConstChunks* pHeadChunk, CallbackOnTxDone cbOnTxDoneCallback = CallbackOnTxDone());
	virtual unsigned          GetRxedFrameSize() const;
	virtual uint16_t          GetRxedFrameType() const;
	virtual const void*       GetRxedFrameData() const;
	virtual const mac_addr_t* GetRxedFrameSourceMacAddr() const;
	virtual const mac_addr_t* GetRxedFrameTargetMacAdr() const;
	virtual void ReleaseRxedFrame();

public: // ISerialRxNotifier
	virtual bool NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxDone(CIoChunk* pRetNextChunk);
	virtual bool NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk);
	virtual bool NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk);
public: // ISerialTxNotifier
	virtual bool NotifyTxDone(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxComplete(CConstChunk* pRetNextChunk);
	virtual bool NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);
	virtual bool NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk);

public: // ISerialConnectionParamsNotifier
	virtual void NotifyConnectionParams(const TSerialConnectionParams* pParams);

public: // IProcess
	virtual void SingleStep(CTimeType ttTime);

public:
	void ProcessRxedSlip();
	void ProcessRxedIpPkt();
	void ProcessIpPktRxError();
	void ProcessIpPktTxDone();
	void ProcessIpPktTxError();
private:
	void ProcessRxedUart(unsigned uiLength);
	unsigned ProcessRxedSlip(unsigned uiLength);
	void KeepUnprocessedSlipTail(unsigned uiSlipFrameUnprocessedTailLength, unsigned uiLength);
	void NextRxUart();
	void ScheduleIpPktProcessing();

private:
	ISerial*    m_pUart;
	mac_addr_t m_MacAddr;
	mac_addr_t m_RxMacAddr;
	unsigned   m_uiSlipFrameKeepLength;
	uint8_t    m_aRxedSlipFrameBuffer[SLIP_RX_BUFF_SIZE];
	unsigned   m_uiRxRequestedLength;
	bool       m_bSlipDecodingStateESC;
	unsigned   m_uiRxedIpPktLength;
	uint8_t    m_aRxedIpPktBuffer[SLIP_MTU];
	bool       m_bRxedIpPktLocked;
	const CChainedConstChunks*  m_pTxingChunk;
	unsigned                    m_uiTxingOffset;
	unsigned   m_uiRxStreamByteTimeoutTicks;

	CallbackOnPktReceived m_cbOnPktReceivedCallback;
	CallbackOnLinkChanged m_cbOnLinkChangedCallback;
	CallbackOnIncomeCheck m_cbOnIncomeCheckCallback;
	CallbackOnTxDone      m_cbOnTxDoneCallback;
};


CNetworkInterfaceUartSlip g_DeviceUartSlip;
INetworkInterface* GetNetworkInterfaceUartSlip() {return &g_DeviceUartSlip;};
void SetNetworkInterfaceUartSlipParams(unsigned uiUartIndex, const TSerialConnectionParams* pParams, CTickProcessScheduler* pScheduller)
{
	g_DeviceUartSlip.InitUart(uiUartIndex, pParams, pScheduller);
}

void CNetworkInterfaceUartSlip::InitUart(unsigned uiUartIndex, const TSerialConnectionParams* pParams, CProcessScheduller* pScheduller)
{
	Init(pScheduller);
	m_uiRxStreamByteTimeoutTicks = 1;
	m_pUart = CHW_Brd::AcquireSerialInterface(uiUartIndex);
	m_pUart->SetSerialRxNotifier(this);
	m_pUart->SetSerialTxNotifier(this);
	m_pUart->SetConnectionParams(pParams);
	NextRxUart();
	DEBUG_INFO("SLIP: Init port:", uiUartIndex);
	DEBUG_INFO("SLIP: Init baudrate:", pParams->uiBaudRate);
}

void CNetworkInterfaceUartSlip::BindNetworkInterface(CallbackOnPktReceived cbOnPktReceived /*= CallbackOnPktReceived()*/, CallbackOnLinkChanged cbOnLinkChanged /*= CallbackOnLinkChanged()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::BindNetworkInterface(cbOnPktReceived));

	SetPktReceivedCallback(cbOnPktReceived);
	SetLinkChangedCallback(cbOnLinkChanged);
}
void CNetworkInterfaceUartSlip::SetPktReceivedCallback(CallbackOnPktReceived cbOnPktReceived /*= CallbackOnPktReceived()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::SetPktReceivedCallback(cbOnPktReceived));

	m_cbOnPktReceivedCallback = cbOnPktReceived;
}
void CNetworkInterfaceUartSlip::SetLinkChangedCallback(CallbackOnLinkChanged cbOnLinkChanged /*= CallbackOnLinkChanged()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::SetLinkChangedCallback(cbOnLinkChanged));

	m_cbOnLinkChangedCallback = cbOnLinkChanged;
}
void CNetworkInterfaceUartSlip::SetIncomeCheckCallback(CallbackOnIncomeCheck cbOnIncomeCheck /*= CallbackOnIncomeCheck()*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::SetIncomeCheckCallback(cbOnIncomeCheck));

	m_cbOnIncomeCheckCallback = cbOnIncomeCheck;
}
void CNetworkInterfaceUartSlip::SetMacAddress(const mac_addr_t* MacAaddress)
{
	m_MacAddr = *MacAaddress;
}
const INetworkInterface::mac_addr_t* CNetworkInterfaceUartSlip::GetMacAddress() const
{
	return &m_MacAddr;
}
unsigned CNetworkInterfaceUartSlip::GetMTU() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetMTU());
	return ARRAY_SIZE(m_aRxedIpPktBuffer);
}
void CNetworkInterfaceUartSlip::ScheduleRefresh()
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::ScheduleRefresh());
	// DO NOTNING
}

void CNetworkInterfaceUartSlip::Reset()
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::Reset());
	// DO NOTNING
}

bool CNetworkInterfaceUartSlip::TxPkt(const mac_addr_t* TargetMACadr, uint16_t uiFrameType, const CChainedConstChunks* pHeadChunk, CallbackOnTxDone cbOnTxDoneCallback /*= NullCallback*/)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::TxPkt(TargetMACadr, uiFrameType, pHeadChunk, cbOnTxDoneCallback));
	bool bResult = true;
	ASSERTE(pHeadChunk);
	ENTER_CRITICAL_SECTION;
	{
		if (!m_pTxingChunk) {
			m_pTxingChunk = pHeadChunk;
		} else {
			bResult = false;
		}
	}
	LEAVE_CRITICAL_SECTION;
	if (bResult) {
		m_uiTxingOffset = 0;
		m_cbOnTxDoneCallback = cbOnTxDoneCallback;
		UARTSLIP_DEBUG_INFO("TX FRAME START. Size:", CollectChainTotalLength(pHeadChunk));
		static const uint8_t g_SLIP_START_SEQUENCE[] = {SLIP_END_CODE};
		static const CConstChunk cc = {g_SLIP_START_SEQUENCE, ARRAY_SIZE(g_SLIP_START_SEQUENCE)};
		bResult = m_pUart->StartDataSend(cc);
	} else {
		UARTSLIP_DEBUG_INFO("TX FRAME SKIP. Size:", CollectChainTotalLength(pHeadChunk));
	}
	return bResult;
}
unsigned CNetworkInterfaceUartSlip::GetRxedFrameSize() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameSize());
	ASSERTE(m_bRxedIpPktLocked);
	return m_uiRxedIpPktLength;
}
uint16_t  CNetworkInterfaceUartSlip::GetRxedFrameType() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameType());
	return ETH_TYPE_IP;
}
const INetworkInterface::mac_addr_t* CNetworkInterfaceUartSlip::GetRxedFrameSourceMacAddr() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameSourceMacAddr());
	return &m_RxMacAddr;
}
const INetworkInterface::mac_addr_t* CNetworkInterfaceUartSlip::GetRxedFrameTargetMacAdr() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameTargetMacAdr());
	return &m_MacAddr;
}
const void* CNetworkInterfaceUartSlip::GetRxedFrameData() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::GetRxedFrameData());
	ASSERTE(m_bRxedIpPktLocked);
	return m_aRxedIpPktBuffer;
}

void CNetworkInterfaceUartSlip::ReleaseRxedFrame()
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkInterface::ReleaseRxedFrame());

	ASSERTE(m_bRxedIpPktLocked);
	ASSERTE(m_uiRxedIpPktLength);

	UARTSLIP_DEBUG_INFO("Released, size:", m_uiRxedIpPktLength);
	m_bRxedIpPktLocked = false;
	m_uiRxedIpPktLength = 0;
//	NextRxUart(); assume we always release pkt inside processing callback
}

bool CNetworkInterfaceUartSlip::NotifyRxPart(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxPart(uiBytesLeft, pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Rx part");
	return false;
}

bool CNetworkInterfaceUartSlip::NotifyRxDone(CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxDone(pRetNextChunk));

	unsigned uiRxedLength = m_uiRxRequestedLength;
	UARTSLIP_DEBUG_INFO("Rx UART, rxbytes:", uiRxedLength);
	ProcessRxedUart(uiRxedLength);

	m_uiRxRequestedLength = ARRAY_SIZE(m_aRxedSlipFrameBuffer) - m_uiSlipFrameKeepLength;
	pRetNextChunk->Assign(m_aRxedSlipFrameBuffer + m_uiSlipFrameKeepLength, m_uiRxRequestedLength);

	return true;
}
bool CNetworkInterfaceUartSlip::NotifyRxOverflow(const CConstChunk& ccData, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxOverflow(ccData, pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Rx UART overflow");

	m_uiSlipFrameKeepLength = 0;
	m_bSlipDecodingStateESC = false;

	m_uiRxRequestedLength = ARRAY_SIZE(m_aRxedSlipFrameBuffer) - m_uiSlipFrameKeepLength;
	pRetNextChunk->Assign(m_aRxedSlipFrameBuffer + m_uiSlipFrameKeepLength, m_uiRxRequestedLength);

	if (m_cbOnIncomeCheckCallback) m_cbOnIncomeCheckCallback();

	return true;
}

bool CNetworkInterfaceUartSlip::NotifyRxError(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxError(uiBytesLeft, pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Rx UART error");
	unsigned uiRxedLength = m_uiRxRequestedLength - uiBytesLeft;
	ProcessRxedUart(uiRxedLength);
	m_uiSlipFrameKeepLength = 0; // ignore tail if exists
	m_bSlipDecodingStateESC = false;
	if (!m_bRxedIpPktLocked) {
		UARTSLIP_DEBUG_INFO("incomplete Ip data droped by Rx UART error:", m_uiRxedIpPktLength);
		m_uiRxedIpPktLength = 0;
	}
	m_uiRxRequestedLength = ARRAY_SIZE(m_aRxedSlipFrameBuffer) - m_uiSlipFrameKeepLength;
	pRetNextChunk->Assign(m_aRxedSlipFrameBuffer + m_uiSlipFrameKeepLength, m_uiRxRequestedLength);
	return true;
}
bool CNetworkInterfaceUartSlip::NotifyRxIdle(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxIdle(uiBytesLeft, pRetNextChunk));

	unsigned uiRxedLength = m_uiRxRequestedLength - uiBytesLeft;
	UARTSLIP_DEBUG_INFO("Rx UART idle, rxbytes:", uiRxedLength);
	ProcessRxedUart(uiRxedLength);

	m_uiRxRequestedLength = ARRAY_SIZE(m_aRxedSlipFrameBuffer) - m_uiSlipFrameKeepLength;
	pRetNextChunk->Assign(m_aRxedSlipFrameBuffer + m_uiSlipFrameKeepLength, m_uiRxRequestedLength);

	return true;
}
bool CNetworkInterfaceUartSlip::NotifyRxTimeout(unsigned uiBytesLeft, CIoChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialRxNotifier::NotifyRxTimeout(uiBytesLeft, pRetNextChunk));
	unsigned uiRxedLength = m_uiRxRequestedLength - uiBytesLeft;
	UARTSLIP_DEBUG_INFO("Rx UART timeout, rxbytes:", uiRxedLength);
	ProcessRxedUart(uiRxedLength);
	if (uiRxedLength) {
		if (!m_bRxedIpPktLocked) {
			unsigned uiSlipFrameUnprocessedTailLength = ProcessRxedSlip(m_uiSlipFrameKeepLength + uiRxedLength);
			if (uiSlipFrameUnprocessedTailLength) KeepUnprocessedSlipTail(uiSlipFrameUnprocessedTailLength, uiRxedLength);
		} else {
			UARTSLIP_DEBUG_INFO("SLIP: Rx lost (locked) bytes:", uiRxedLength);
			m_uiSlipFrameKeepLength = 0;
			m_bSlipDecodingStateESC = false;
		}
	}
	m_uiRxRequestedLength = ARRAY_SIZE(m_aRxedSlipFrameBuffer) - m_uiSlipFrameKeepLength;
	pRetNextChunk->Assign(m_aRxedSlipFrameBuffer + m_uiSlipFrameKeepLength, m_uiRxRequestedLength);
	return true;
}

bool CNetworkInterfaceUartSlip::NotifyTxDone(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxDone(pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Next tx, offs:", m_uiTxingOffset);
	bool bResult = false;

	if (m_pTxingChunk) {
		if (m_uiTxingOffset >= m_pTxingChunk->uiLength) {
			m_pTxingChunk = m_pTxingChunk->GetNextItem();
			m_uiTxingOffset = 0;
		}
		if (m_pTxingChunk) {
			const uint8_t* pData = m_pTxingChunk->pData + m_uiTxingOffset;
			unsigned uiTxingChunkBytesLeft = m_pTxingChunk->uiLength - m_uiTxingOffset;
			unsigned uiTxingLength = uiTxingChunkBytesLeft;
			if (pData[0] == SLIP_END_CODE) {
				UARTSLIP_DEBUG_INFO("Tx ESC_END");
				static const uint8_t g_SLIP_ESC_END_SEQUENCE[] = {SLIP_ESC_CODE, SLIP_ESC_END_CODE};
				pRetNextChunk->Assign(g_SLIP_ESC_END_SEQUENCE, ARRAY_SIZE(g_SLIP_ESC_END_SEQUENCE));
				m_uiTxingOffset += 1;
				bResult = true;
			} else if (pData[0] == SLIP_ESC_CODE) {
				UARTSLIP_DEBUG_INFO("Tx ESC_ESC");
				static const uint8_t g_SLIP_ESC_ESC_SEQUENCE[] = {SLIP_ESC_CODE, SLIP_ESC_ESC_CODE};
				pRetNextChunk->Assign(g_SLIP_ESC_ESC_SEQUENCE, ARRAY_SIZE(g_SLIP_ESC_ESC_SEQUENCE));
				m_uiTxingOffset += 1;
				bResult = true;
			} else {
				for (unsigned n = 1; n < uiTxingLength; ++n) {
					const uint8_t uiByte = pData[n];
					if (uiByte == SLIP_END_CODE || uiByte == SLIP_ESC_CODE) {
						uiTxingLength = n;
						break;
					}
				}
				m_uiTxingOffset += uiTxingLength;
				UARTSLIP_DEBUG_INFO("Tx DATA:", uiTxingLength);
				pRetNextChunk->Assign(pData, uiTxingLength);
				bResult = true;
			}
		} else {
			UARTSLIP_DEBUG_INFO("Tx END");
			static const uint8_t g_SLIP_END_SEQUENCE[] = {SLIP_END_CODE};
			pRetNextChunk->Assign(g_SLIP_END_SEQUENCE, ARRAY_SIZE(g_SLIP_END_SEQUENCE));
			bResult = true;
		}
	} else {
		ProcessIpPktTxDone();
	}
	return bResult;
}
bool CNetworkInterfaceUartSlip::NotifyTxComplete(CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxComplete(pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Next tx, offs:", m_uiTxingOffset);
	bool bResult = false;
	return bResult;
}
bool CNetworkInterfaceUartSlip::NotifyTxError(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxError(uiBytesLeft, pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Tx UART error, bytesLeft:", uiBytesLeft);
	ProcessIpPktTxError();
	return false;
}
bool CNetworkInterfaceUartSlip::NotifyTxTimeout(unsigned uiBytesLeft, CConstChunk* pRetNextChunk)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialTxNotifier::NotifyTxTimeout(uiBytesLeft, pRetNextChunk));
	UARTSLIP_DEBUG_INFO("Tx UART timeout, bytesLeft:", uiBytesLeft);
	ProcessIpPktTxError();
	return false;
}
void CNetworkInterfaceUartSlip::NotifyConnectionParams(const TSerialConnectionParams* pParams)
{
	IMPLEMENTS_INTERFACE_METHOD(ISerialConnectionParamsNotifier::NotifyConnectionParams(pParams));
	m_uiRxStreamByteTimeoutTicks = 1 + MILLI_TO_TICKS(1000 * 5 * 11 / pParams->uiBaudRate);
}

void CNetworkInterfaceUartSlip::SingleStep(CTimeType ttTime)
{
	IMPLEMENTS_INTERFACE_METHOD(CTickProcess::SingleStep(ttTime));
	PROCESS_DEBUG_INFO(">>> Process: CNetworkInterfaceUartSlip");

	ASSERTE(m_bRxedIpPktLocked);
	ProcessRxedIpPkt();
	ASSERTE(!m_bRxedIpPktLocked && "MUST BE RELEASED INSIDE CALLBACK"); // or we have to change NextRxUart from ReleaseRxedFrame

	PROCESS_DEBUG_INFO("<<< Process: CNetworkInterfaceUartSlip");
}

void CNetworkInterfaceUartSlip::ProcessRxedUart(unsigned uiRxedLength)
{
	if (!m_bRxedIpPktLocked) {
		unsigned uiSlipFrameUnprocessedTailLength = ProcessRxedSlip(m_uiSlipFrameKeepLength + uiRxedLength);
		if (uiSlipFrameUnprocessedTailLength) KeepUnprocessedSlipTail(uiSlipFrameUnprocessedTailLength, uiRxedLength);
	} else {
		UARTSLIP_DEBUG_INFO("Rx lost (locked)", uiRxedLength);
		m_uiSlipFrameKeepLength = 0;
		m_bSlipDecodingStateESC = false;
	}
}

// return length on undecoded tail
unsigned CNetworkInterfaceUartSlip::ProcessRxedSlip(unsigned uiLength)
{
	for(unsigned n = 0; n < uiLength && !m_bRxedIpPktLocked; ++n) {
		uint8_t uiByte = m_aRxedSlipFrameBuffer[n];
//		UARTSLIP_DEBUG_INFO("Rx byte:", uiByte);
		if (!m_bSlipDecodingStateESC) {
			switch (uiByte) {
				case SLIP_END_CODE:
					if (m_uiRxedIpPktLength) {
						ScheduleIpPktProcessing();
						return uiLength - n - 1; // move unprocessed data tail to buffer start
					}
					break;
				case SLIP_ESC_CODE:
					m_bSlipDecodingStateESC = true;
					break;
				default:
					if (m_uiRxedIpPktLength >= ARRAY_SIZE(m_aRxedIpPktBuffer)) {
						UARTSLIP_DEBUG_INFO("SLIP: Rx pkt overflow");
						ProcessIpPktRxError();
					}
					m_aRxedIpPktBuffer[m_uiRxedIpPktLength] = uiByte;
					++m_uiRxedIpPktLength;
					break;
			}
		} else {
			m_bSlipDecodingStateESC = false;
			switch (uiByte) {
				case SLIP_ESC_END_CODE:
					m_aRxedIpPktBuffer[m_uiRxedIpPktLength] = SLIP_END_CODE;
					++m_uiRxedIpPktLength;
					break;
				case SLIP_ESC_ESC_CODE:
					m_aRxedIpPktBuffer[m_uiRxedIpPktLength] = SLIP_ESC_CODE;
					++m_uiRxedIpPktLength;
					break;
				default:
					UARTSLIP_DEBUG_INFO("SLIP: invalid code escaped:", uiByte);
					ProcessIpPktRxError();
			}
		}
	}
	return 0;
}

void CNetworkInterfaceUartSlip::KeepUnprocessedSlipTail(unsigned uiSlipFrameUnprocessedTailLength, unsigned uiLength)
{
	m_uiSlipFrameKeepLength = 0;
	for (unsigned n = uiLength - uiSlipFrameUnprocessedTailLength; n < uiLength; ++n) {
		m_aRxedSlipFrameBuffer[m_uiSlipFrameKeepLength] = m_aRxedSlipFrameBuffer[n];
		++m_uiSlipFrameKeepLength;
	}
}
void CNetworkInterfaceUartSlip::NextRxUart()
{
//	UARTSLIP_DEBUG_INFO("Next rx. keep:", m_uiSlipFrameKeepLength);
	m_uiRxRequestedLength = ARRAY_SIZE(m_aRxedSlipFrameBuffer) - m_uiSlipFrameKeepLength;
//	if (m_uiRxedIpPktLength + m_uiRxRequestedLength <= 16) {
//		m_pUart->SetRxByteTimeout(MILLI_TO_TICKS(5000));
//	} else {
//		m_pUart->SetRxByteTimeout(m_uiRxStreamByteTimeoutTicks);
//	}
	CIoChunk cc = {m_aRxedSlipFrameBuffer + m_uiSlipFrameKeepLength, m_uiRxRequestedLength};
	m_pUart->StartDataRecv(cc);
	m_uiSlipFrameKeepLength = 0;
}

void CNetworkInterfaceUartSlip::ScheduleIpPktProcessing()
{
	ASSERTE(!m_bRxedIpPktLocked);
	m_bRxedIpPktLocked = true;
	ContinueNow();
}

void CNetworkInterfaceUartSlip::ProcessRxedIpPkt()
{
	if (m_uiRxedIpPktLength && m_cbOnPktReceivedCallback) {
		UARTSLIP_DEBUG_INFO("Rx IP Pkt, size:", m_uiRxedIpPktLength);
		m_cbOnPktReceivedCallback(m_uiRxedIpPktLength);
	} else {
		m_uiRxedIpPktLength = 0;
		m_bRxedIpPktLocked = false;
	}
}

void CNetworkInterfaceUartSlip::ProcessIpPktRxError()
{
	UARTSLIP_DEBUG_INFO("Rx Pkt error");
	m_uiRxedIpPktLength = 0;
}

void CNetworkInterfaceUartSlip::ProcessIpPktTxDone()
{
	UARTSLIP_DEBUG_INFO("Tx IP Pkt done");
	if (m_cbOnTxDoneCallback) { m_cbOnTxDoneCallback(true); m_cbOnTxDoneCallback = CallbackOnTxDone(); }
}

void CNetworkInterfaceUartSlip::ProcessIpPktTxError()
{
	m_pTxingChunk = NULL;
	m_uiTxingOffset = 0;
	if (m_cbOnTxDoneCallback) { m_cbOnTxDoneCallback(false); m_cbOnTxDoneCallback = CallbackOnTxDone(); }
}
