SRC += $(if $(filter YES,$(USE_NET)), $(addprefix src/modules/net/, \
	net_stack.cpp \
	net_init.cpp \
))

include $(addsuffix /Makefile.mk,$(call SUBMKS,src/modules/net))
