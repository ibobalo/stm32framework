#ifndef NET_PARAMS_INCLUDED
#define NET_PARAMS_INCLUDED

#define NET_PARAMS_LIST \
	PARAM(LAN_ENABLED,                        bool,      true) \
	PARAM(LAN_MAC_Address_Hi,                 uint16_t,  0x00) \
	PARAM(LAN_MAC_Address_Lo,                 uint32_t,  0x00) \
	PARAM(LAN_IP_Address_1,                   uint8_t,   192) \
	PARAM(LAN_IP_Address_2,                   uint8_t,   168) \
	PARAM(LAN_IP_Address_3,                   uint8_t,   2) \
	PARAM(LAN_IP_Address_4,                   uint8_t,   2) \
	\
	PARAM(SLIP_UART,                          uint8_t,   1) \
	PARAM(SLIP_BAUDRATE,                      unsigned,  0, "0 - Auto, recommended - 115200") \
	PARAM(SLIP_STOPBITS,                      uint8_t,   1, "0 - 0.5, 1 - 1, 2 - 2, 3 - 1.5") \
	PARAM(SLIP_PARITYBITS,                    uint8_t,   0, "0 - None, 1 - Even, 2 - Odd") \
	PARAM(SLIP_IP_Address_1,                  uint8_t,   192) \
	PARAM(SLIP_IP_Address_2,                  uint8_t,   168) \
	PARAM(SLIP_IP_Address_3,                  uint8_t,   2) \
	PARAM(SLIP_IP_Address_4,                  uint8_t,   2) \

#endif // NET_PARAMS_INCLUDED
