#ifndef UDP_PARAMS_H_INCLUDED
#define UDP_PARAMS_H_INCLUDED

#include "../net_stack.h"

void InitUDPParamsListener(INetworkStack* pNetworkStack, unsigned uiPort);

#endif // UDP_PARAMS_H_INCLUDED
