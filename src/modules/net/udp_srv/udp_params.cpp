#include "stdafx.h"
#include "udp_params.h"
#include "util/core.h"
#include "util/macros.h"
#include "util/endians.h"
#include "util/utils.h"
#include "hw/debug.h"
#include "params.h"
#include "stm32_linker.h"
#include <stdint.h>
//#include <string.h>

// declarations
class CUDPParamsListener :
		public INetworkUdpListener
{
private:
	enum {
		QUERY_TYPE_PARAMS_COUNT = 1,
		QUERY_TYPE_PARAM_DETAILS = 2,
		QUERY_TYPE_PARAM_SET = 3,
		QUERY_TYPE_PARAM_DETAILS_EX = 4,
		QUERY_TYPE_PARAM_DESCR = 5,
		QUERY_TYPE_STORAGE_CLEAR = 6,
	};
	enum {
		RESPONCE_TYPE_PARAMS_COUNT = QUERY_TYPE_PARAMS_COUNT,
		RESPONCE_TYPE_PARAM_DETAILS = QUERY_TYPE_PARAM_DETAILS,
		RESPONCE_TYPE_PARAM_DETAILS_EX = QUERY_TYPE_PARAM_DETAILS_EX,
		RESPONCE_TYPE_PARAM_DESCR = QUERY_TYPE_PARAM_DESCR,
		RESPONCE_TYPE_STORAGE_CLEAR = QUERY_TYPE_STORAGE_CLEAR,
	};
	enum {
		VALUE_TYPE_NONE = 0,
		VALUE_TYPE_BOOL = 1,
		VALUE_TYPE_INT = 2,
		VALUE_TYPE_UINT = 3,
		VALUE_TYPE_FLOAT = 4,
	};

	typedef struct __attribute__ ((packed)) {
		uint16_t                       uiSequence;
		uint16_t                       uiReqType;
	} UdpRequest_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
	} UdpRequest_ParamsCount_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		unsigned                       uiParamId;
	} UdpRequest_ParamDetails_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		unsigned                       uiParamId;
		uint16_t                       uiValueLength;
	} UdpRequest_ParamSet_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		uint32_t                       uiDescrId;
	} UdpRequest_ParamDescr_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
	} UdpRequest_StorageClear_Header_t;
	typedef union {
		UdpRequest_Header_t              hdr;
		UdpRequest_ParamsCount_Header_t  req_params_count;
		UdpRequest_ParamDetails_Header_t req_details;
		struct __attribute__ ((packed)) {
			UdpRequest_ParamSet_Header_t     req_set;
			union {
				bool                       value_bool;
				int                        value_int;
				unsigned                   value_uint;
				uint64_t                   value_double;
			};
		};
		UdpRequest_ParamDescr_Header_t   req_descr;
		UdpRequest_StorageClear_Header_t req_storage_clear;
	} UdpRequest_t;

	typedef struct __attribute__ ((packed)) {
		uint16_t                     uiSequence;
		uint16_t                     uiRespType;
	} UdpReply_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		unsigned                     uiCount;
	} UdpReply_ParamsCount_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		unsigned                     uiParamId;
		uint16_t                     uiValueType;
		uint16_t                     uiValueLength;
		uint16_t                     uiNameLength;
	} UdpReply_ParamDetails_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		unsigned                     uiParamId;
		uint16_t                     uiValueType;
		uint16_t                     uiValueLength;
		uint16_t                     uiNameLength;
		uint32_t                     uiDescrId;
	} UdpReply_ParamDetailsEx_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint32_t                     uiDescrId;
		uint16_t                     uiLength;
	} UdpReply_ParamDescr_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
	} UdpReply_StorageClear_Header_t;

	typedef union {
		UdpReply_Header_t              hdr;
		UdpReply_ParamsCount_Header_t  params_count;
		struct __attribute__ ((packed)) {
			UdpReply_ParamDetails_Header_t param_details_hdr;
			union {
				bool                       value_bool;
				int                        value_int;
				unsigned                   value_uint;
				uint64_t                   value_double;
			};
		};
		struct __attribute__ ((packed)) {
			UdpReply_ParamDetailsEx_Header_t param_details_ex_hdr;
			union {
				bool                       values_bool[2];
				int                        values_int[2];
				unsigned                   values_uint[2];
				uint64_t                   values_double[2];
			};
		};
		UdpReply_ParamDescr_Header_t     param_descr_hdr;
		UdpReply_StorageClear_Header_t   storage_clear;
	} UdpReply_t;
	UdpReply_t m_UdpReplyBuffer;

	CChainedConstChunks m_UdpReplyChunks[3];
	INetworkStack*      m_pNetworkStack;
	unsigned            m_uiMaxValueAndNameLength;
	unsigned            m_uiMaxDescrLength;
public:
	void Init(INetworkStack* pNetworkStack) {
		m_pNetworkStack = pNetworkStack;
		m_uiMaxValueAndNameLength = (m_pNetworkStack->GetUDPMTU() - sizeof(UdpReply_ParamDetails_Header_t)) - 1;
		m_uiMaxDescrLength = (m_pNetworkStack->GetUDPMTU() - sizeof(UdpReply_ParamDescr_Header_t)) - 1;
	};
	void ReplyParamsCount(unsigned uiSequence) {
		m_UdpReplyBuffer.hdr.uiSequence = htons(uiSequence);
		m_UdpReplyBuffer.hdr.uiRespType = htons(RESPONCE_TYPE_PARAMS_COUNT);
		m_UdpReplyBuffer.params_count.uiCount = htonl(GetGlobalParamsCount());
		m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.params_count));
		m_UdpReplyChunks[0].Break();
	};
	void ReplyParamDetails(unsigned uiSequence, global_params_id_t eParamId, bool bExtraInfo) {
		const char* pszName = GetGlobalParamName(eParamId);
		if (pszName) {
			m_UdpReplyBuffer.hdr.uiSequence = htons(uiSequence);
			m_UdpReplyBuffer.hdr.uiRespType = htons(bExtraInfo?RESPONCE_TYPE_PARAM_DETAILS_EX:RESPONCE_TYPE_PARAM_DETAILS);
			if (bExtraInfo) {
				m_UdpReplyBuffer.param_details_ex_hdr.uiParamId = htonl((unsigned)eParamId);
				m_UdpReplyBuffer.param_details_ex_hdr.uiValueType = htons(VALUE_TYPE_NONE);
			} else {
				m_UdpReplyBuffer.param_details_hdr.uiParamId = htonl((unsigned)eParamId);
				m_UdpReplyBuffer.param_details_hdr.uiValueType = htons(VALUE_TYPE_NONE);
			}
			global_params_type_t eParamType = GetGlobalParamType(eParamId);
			unsigned uiValueSize = 0;
			switch (eParamType) {
				case GLOBAL_PARAMS_TYPE_bool: {
					bool bValue;
					if (GetGlobalParamValue_bool(eParamId, bValue)) {
						if (bExtraInfo) {
							m_UdpReplyBuffer.param_details_ex_hdr.uiValueType = htons(VALUE_TYPE_BOOL);
							m_UdpReplyBuffer.values_bool[0] = bValue?1:0;
							if (GetGlobalParamDefault_bool(eParamId, bValue)) {
								m_UdpReplyBuffer.values_bool[1] = bValue?1:0;
							}
						} else {
							m_UdpReplyBuffer.param_details_hdr.uiValueType = htons(VALUE_TYPE_BOOL);
							m_UdpReplyBuffer.value_bool = bValue?1:0;
						}
						uiValueSize = sizeof(bool);
					}
				} break;
				case GLOBAL_PARAMS_TYPE_int: {
					int iValue;
					if (GetGlobalParamValue_int(eParamId, iValue)) {
						if (bExtraInfo) {
							m_UdpReplyBuffer.param_details_ex_hdr.uiValueType = htons(VALUE_TYPE_INT);
							m_UdpReplyBuffer.values_int[0] = htonl(iValue);
							if (GetGlobalParamDefault_int(eParamId, iValue)) {
								m_UdpReplyBuffer.values_int[1] = htonl(iValue);
							}
						} else {
							m_UdpReplyBuffer.param_details_hdr.uiValueType = htons(VALUE_TYPE_INT);
							m_UdpReplyBuffer.value_int = htonl(iValue);
						}
						uiValueSize = sizeof(iValue);
					}
				} break;
				case GLOBAL_PARAMS_TYPE_unsigned: {
					unsigned uiValue;
					if (GetGlobalParamValue_unsigned(eParamId, uiValue)) {
						if (bExtraInfo) {
							m_UdpReplyBuffer.param_details_ex_hdr.uiValueType = htons(VALUE_TYPE_UINT);
							m_UdpReplyBuffer.values_uint[0] = htonl(uiValue);
							if (GetGlobalParamDefault_unsigned(eParamId, uiValue)) {
								m_UdpReplyBuffer.values_uint[1] = htonl(uiValue);
							}
						} else {
							m_UdpReplyBuffer.param_details_hdr.uiValueType = htons(VALUE_TYPE_UINT);
							m_UdpReplyBuffer.value_uint = htonl(uiValue);
						}
						uiValueSize = sizeof(uiValue);
					}
				} break;
				case GLOBAL_PARAMS_TYPE_float: {
					double dValue;
					if (GetGlobalParamValue_float(eParamId, dValue)) {
						if (bExtraInfo) {
							m_UdpReplyBuffer.param_details_ex_hdr.uiValueType = htons(VALUE_TYPE_FLOAT);
							m_UdpReplyBuffer.values_double[0] = htond(dValue);
							if (GetGlobalParamDefault_float(eParamId, dValue)) {
								m_UdpReplyBuffer.values_double[1] = htond(dValue);
							}
						} else {
							m_UdpReplyBuffer.param_details_hdr.uiValueType = htons(VALUE_TYPE_FLOAT);
							m_UdpReplyBuffer.value_double = htond(dValue);
						}
						uiValueSize = sizeof(dValue);
					}
				} break;
			}
			if (bExtraInfo) {
				const char* pszDesc = GetGlobalParamDesc(eParamId);
				unsigned uiNameLength = strnlen(pszName, m_uiMaxValueAndNameLength - uiValueSize * 2);
				if (pszDesc && !pszDesc[0]) pszDesc = NULL;
				m_UdpReplyBuffer.param_details_ex_hdr.uiValueLength = htons(uiValueSize);
				m_UdpReplyBuffer.param_details_ex_hdr.uiNameLength = htons(uiNameLength);
				m_UdpReplyBuffer.param_details_ex_hdr.uiDescrId = htonl((uint32_t)pszDesc);
				m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.param_details_ex_hdr) + uiValueSize * 2);
				m_UdpReplyChunks[0].Append(&m_UdpReplyChunks[1]);
				m_UdpReplyChunks[1].Assign((const uint8_t*)pszName, uiNameLength);
				m_UdpReplyChunks[1].Break();
			} else {
				unsigned uiNameLength = strnlen(pszName, m_uiMaxValueAndNameLength - uiValueSize);
				m_UdpReplyBuffer.param_details_hdr.uiValueLength = htons(uiValueSize);
				m_UdpReplyBuffer.param_details_hdr.uiNameLength = htons(uiNameLength);
				m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.param_details_hdr) + uiValueSize);
				m_UdpReplyChunks[0].Append(&m_UdpReplyChunks[1]);
				m_UdpReplyChunks[1].Assign((const uint8_t*)pszName, uiNameLength);
				m_UdpReplyChunks[1].Break();
			}
		} else {
			ReplyParamsCount(uiSequence);
		}
	};
	void ReplyParamDescr(unsigned uiSequence, unsigned uiDescrId) {
		const char* p = (const char*)uiDescrId;
		unsigned uiLength = 0;
		if (1
		 && p >= GetLinkerSymbol(const char*, ro_data_start)
		 && p < GetLinkerSymbol(const char*, ro_data_end)
		) {
			uiLength = strnlen(p, m_uiMaxDescrLength);
			if (p + uiLength < GetLinkerSymbol(const char*, ro_data_end)) {
				// ok
			} else {
				uiLength = 0;
			}
		};
		m_UdpReplyBuffer.hdr.uiSequence = htons(uiSequence);
		m_UdpReplyBuffer.hdr.uiRespType = htons(RESPONCE_TYPE_PARAM_DESCR);
		m_UdpReplyBuffer.param_descr_hdr.uiDescrId = htonl(uiDescrId);
		m_UdpReplyBuffer.param_descr_hdr.uiLength = htons(uiLength);
		m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.param_descr_hdr));
		if (uiLength) {
			m_UdpReplyChunks[0].Append(&m_UdpReplyChunks[1]);
			m_UdpReplyChunks[1].Assign((const uint8_t*)p, uiLength);
			m_UdpReplyChunks[1].Break();
		} else {
			m_UdpReplyChunks[0].Break();
		}
	}
	void ReplyStorageClear(unsigned uiSequence) {
		ClearParamsStorage();
		m_UdpReplyBuffer.hdr.uiSequence = htons(uiSequence);
		m_UdpReplyBuffer.hdr.uiRespType = htons(RESPONCE_TYPE_STORAGE_CLEAR);
		m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.storage_clear));
		m_UdpReplyChunks[0].Break();
	};
public: // INetworkUdpListener
	virtual const CChainedConstChunks* FilterIncomeUDP(const uint8_t* pData, unsigned uiSize);
} g_UDPParamsListener;

const CChainedConstChunks* CUDPParamsListener::FilterIncomeUDP(const uint8_t* pData, unsigned uiSize)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkUdpListener::FilterIncomeUDP(pData, uiSize));
	if (uiSize >= sizeof(UdpRequest_Header_t)) {
		const UdpRequest_t* pUdpRequest = (const UdpRequest_t*)pData;
		unsigned uiSequence = ntohs(pUdpRequest->hdr.uiSequence);
		unsigned uiRequestType = ntohs(pUdpRequest->hdr.uiReqType);

		switch (uiRequestType) {
			case QUERY_TYPE_PARAMS_COUNT: {
				if (uiSize >= sizeof(pUdpRequest->req_params_count)) {
					ReplyParamsCount(uiSequence);
					return m_UdpReplyChunks;
				}
			} break;
			case QUERY_TYPE_PARAM_DETAILS: {
				if (uiSize >= sizeof(pUdpRequest->req_details)) {
					global_params_id_t eParamId = (global_params_id_t)ntohl(pUdpRequest->req_details.uiParamId);
					ReplyParamDetails(uiSequence, eParamId, false);
					return m_UdpReplyChunks;
				}
			} break;
			case QUERY_TYPE_PARAM_DETAILS_EX: {
				if (uiSize >= sizeof(pUdpRequest->req_details)) {
					global_params_id_t eParamId = (global_params_id_t)ntohl(pUdpRequest->req_details.uiParamId);
					ReplyParamDetails(uiSequence, eParamId, true);
					return m_UdpReplyChunks;
				}
			} break;
			case QUERY_TYPE_PARAM_DESCR: {
				if (uiSize >= sizeof(pUdpRequest->req_descr)) {
					uint32_t uiDescrId = ntohl(pUdpRequest->req_descr.uiDescrId);
					ReplyParamDescr(uiSequence, uiDescrId);
					return m_UdpReplyChunks;
				}
			} break;
			case QUERY_TYPE_PARAM_SET: {
				if (uiSize >= sizeof(pUdpRequest->req_set)) {
					global_params_id_t eParamId = (global_params_id_t)ntohl(pUdpRequest->req_set.uiParamId);
					unsigned uiValueLength = ntohs(pUdpRequest->req_set.uiValueLength);
					DEBUG_INFO("Config param changing:", eParamId);
					switch (GetGlobalParamType(eParamId)) {
						case GLOBAL_PARAMS_TYPE_bool: {
							if (uiValueLength == sizeof(bool)) SetGlobalParamValue_bool(eParamId, pUdpRequest->value_bool);
							else DEBUG_INFO("Invalid size:", uiValueLength);
						} break;
						case GLOBAL_PARAMS_TYPE_int: {
							if (uiValueLength == sizeof(int)) SetGlobalParamValue_int(eParamId, ntohl(pUdpRequest->value_int));
							else DEBUG_INFO("Invalid size:", uiValueLength);
						} break;
						case GLOBAL_PARAMS_TYPE_unsigned: {
							if (uiValueLength == sizeof(unsigned)) SetGlobalParamValue_unsigned(eParamId, ntohl(pUdpRequest->value_uint));
							else DEBUG_INFO("Invalid size:", uiValueLength);
						} break;
						case GLOBAL_PARAMS_TYPE_float: {
							if (uiValueLength == sizeof(double)) SetGlobalParamValue_float(eParamId, ntohd(pUdpRequest->value_double));
							else DEBUG_INFO("Invalid size:", uiValueLength);
						}
					}
					ReplyParamDetails(uiSequence, eParamId, false);
					return m_UdpReplyChunks;
				}
			} break;
			case QUERY_TYPE_STORAGE_CLEAR: {
				if (uiSize >= sizeof(pUdpRequest->req_storage_clear)) {
					ReplyStorageClear(uiSequence);
					return m_UdpReplyChunks;
				}
			} break;
		}
	}
	return NULL; // no reply for wrong size
};

void InitUDPParamsListener(INetworkStack* pNetworkStack, unsigned uiPort)
{
	g_UDPParamsListener.Init(pNetworkStack);
	pNetworkStack->RegisterUDPListener(uiPort, &g_UDPParamsListener);
	DEBUG_INFO("Config param listener started. UDP Port:", uiPort);
}
