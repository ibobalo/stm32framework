#include "stdafx.h"
#include "hw/hw.h"
#include "udp_flash.h"
#include "util/core.h"
#include "util/macros.h"
#include "util/endians.h"
#include "hw/debug.h"
#include "hw/hw_macros.h"
#include "params.h"
#include <stdint.h>
#include <string.h>
#include "stm32_linker.h"

// declarations
class CUDPFlashListener :
		public INetworkUdpListener
{
private:
	enum {
		QUERY_TYPE_GET_INFO = 1,
		QUERY_TYPE_UPLOAD_UPDATE = 2,
		QUERY_TYPE_VERIFY_UPDATE = 3,
		QUERY_TYPE_DOWNLOAD_UPDATE = 4,
		QUERY_TYPE_RESET = 5,
	};
	enum {
		RESPONCE_TYPE_GET_INFO = QUERY_TYPE_GET_INFO,
		RESPONCE_TYPE_UPLOAD_UPDATE = QUERY_TYPE_UPLOAD_UPDATE,
		RESPONCE_TYPE_VERIFY_UPDATE = QUERY_TYPE_VERIFY_UPDATE,
		RESPONCE_TYPE_DOWNLOAD_UPDATE = QUERY_TYPE_DOWNLOAD_UPDATE,
	};

//	QUERY_HEADER_FORMAT = "!HH"  # seq, query_type
//	QUERY_UPLOAD_FW_FORMAT = "!LL"  # offset, size
	typedef struct __attribute__ ((packed)) {
		uint16_t                       uiSequence;
		uint16_t                       uiReqType;
	} UdpRequest_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
	} UdpRequest_GetInfo_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		uint32_t                       uiOffset;
		uint32_t                       uiSize;
//		uint8_t                        data[];
	} UdpRequest_UploadUpdate_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		uint32_t                       uiOffset;
		uint32_t                       uiSize;
//		uint8_t                        data[];
	} UdpRequest_VerifyUpdate_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		uint32_t                       uiOffset;
		uint32_t                       uiSize;
	} UdpRequest_DownloadUpdate_Header_t;
	typedef union {
		UdpRequest_Header_t                 hdr;
		UdpRequest_GetInfo_t                req_get_info;
		UdpRequest_UploadUpdate_Header_t    req_upload_update;
		UdpRequest_VerifyUpdate_Header_t    req_verify_update;
		UdpRequest_DownloadUpdate_Header_t  req_download_update;
	} UdpRequest_t;

//	RESPONCE_HEADER_FORMAT="!HH" # seq, query_type
//	RESPONCE_GET_INFO_FORMAT="!HHH6s" # McuId, Rev, FlashSizeKB, UniqID
//	RESPONCE_UPLOAD_UPDATE_FORMAT="!L" # commited_size
//	RESPONCE_VERIFY_UPDATE_FORMAT="!L" # commited_size
//	RESPONCE_DOWNLOAD_UPDATE_FORMAT="!LL" # offset, size
	typedef struct __attribute__ ((packed)) {
		uint16_t                     uiSequence;
		uint16_t                     uiRespType;
	} UdpReply_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint16_t                     uiMcuID;
		uint16_t                     uiRev;
		uint16_t                     uiFlashSizeKB;
		uint8_t                      auiUniqId[6];
	} UdpReply_GetInfo_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint32_t                     uiCommitedSize;
	} UdpReply_UploadUpdate_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint32_t                     uiCommitedSize;
	} UdpReply_VerifyUpdate_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint32_t                     uiOffset;
		uint32_t                     uiSize;
//		uint8_t                      data[];
	} UdpReply_DownloadUpdate_t;


	typedef union {
		UdpReply_Header_t           hdr;
		UdpReply_GetInfo_t          get_info;
		UdpReply_UploadUpdate_t     upload_update;
		UdpReply_VerifyUpdate_t     verify_update;
		UdpReply_DownloadUpdate_t   download_update;
	} UdpReply_t;
	UdpReply_t m_UdpReplyBuffer;

	CChainedConstChunks m_UdpReplyChunk;
	INetworkStack*      m_pNetworkStack;
	unsigned            m_uiCommitedOffset;
public:
	void Init(INetworkStack* pNetworkStack) {
		m_pNetworkStack = pNetworkStack;
		m_UdpReplyChunk.Break();
		m_uiCommitedOffset = 0;
	};
	void ReplyGetInfo(unsigned uiSequence) {
		uint16_t uiFlashSizeKB = *(uint16_t*)STM32F4_F_ID;
		m_UdpReplyBuffer.hdr.uiSequence = htons((uint16_t)uiSequence);
		m_UdpReplyBuffer.hdr.uiRespType = htons((uint16_t)RESPONCE_TYPE_GET_INFO);
		m_UdpReplyBuffer.get_info.uiMcuID = htons((uint16_t)DBGMCU_GetDEVID());
		m_UdpReplyBuffer.get_info.uiRev = htons((uint16_t)DBGMCU_GetREVID());
		m_UdpReplyBuffer.get_info.uiFlashSizeKB = htons(uiFlashSizeKB);
		memcpy(&m_UdpReplyBuffer.get_info.auiUniqId, (const uint8_t*)STM32F4_U_ID, 6) ;
		m_UdpReplyChunk.Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.get_info));
	};
	void ReplyUploadUpdate(unsigned uiSequence) {
		m_UdpReplyBuffer.hdr.uiSequence = htons((uint16_t)uiSequence);
		m_UdpReplyBuffer.hdr.uiRespType = htons((uint16_t)RESPONCE_TYPE_UPLOAD_UPDATE);
		m_UdpReplyBuffer.upload_update.uiCommitedSize = htonl((uint32_t)m_uiCommitedOffset);
		m_UdpReplyChunk.Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.upload_update));
	};
	void ReplyVerifyUpdate(unsigned uiSequence) {
		m_UdpReplyBuffer.hdr.uiSequence = htons((uint16_t)uiSequence);
		m_UdpReplyBuffer.hdr.uiRespType = htons((uint16_t)RESPONCE_TYPE_VERIFY_UPDATE);
		m_UdpReplyBuffer.upload_update.uiCommitedSize = htonl((uint32_t)m_uiCommitedOffset);
		m_UdpReplyChunk.Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.upload_update));
	};
	void DoVerifyAndFlash(unsigned uiOffset, unsigned uiSize, const uint8_t* pSrcData, bool bDoFlash)
	{
		if (uiOffset > m_uiCommitedOffset) {
			DEBUG_INFO("Flash over LAN error - invalid offset");
			return;
		}
		if (uiOffset + uiSize >= GetLinkerSymbol(unsigned, reserved_size)) {
			DEBUG_INFO("Flash over LAN error - invalid size");
			return;
		}
		if (bDoFlash) FLASH_Unlock();
		do {
			/* update area must be pre-cleared by bootloader */
			FLASH_Status status;
			unsigned uiEndOffset = uiOffset + uiSize;
			uint8_t* pDst = GetLinkerSymbol(uint8_t*, reserved_start) + uiOffset;
			const uint8_t* pSrc = pSrcData;
			for (m_uiCommitedOffset = uiOffset; m_uiCommitedOffset < uiEndOffset; ) {
				if (1
				 && !((unsigned)pSrc & 0x03) /* source aligned to 4 */
				 && !((unsigned)pDst & 0x03) /* destination aligned to 4 */
				 && (uiEndOffset - m_uiCommitedOffset >= 4) /* not a tail */
				) {
					if (*(uint32_t*)pDst == *(uint32_t*)pSrc) {
						// 4 byte verify
					} else if (bDoFlash
						&& *(uint32_t*)pDst == 0xFFFFFFFFUL
						&& (status = FLASH_ProgramWord((uint32_t)pDst, *(uint32_t*)pSrc)) == FLASH_COMPLETE
						&& *(uint32_t*)pDst == *(uint32_t*)pSrc
					) { // 4 byte flash
						CHW_MCU::KickIWDG();
					} else {
						ASSERTE(false && "flash 4 failed");
						break;
					}
					pSrc += 4; pDst += 4; m_uiCommitedOffset += 4;
				} else {
					if (*pDst == *pSrc) {
						// 1 byte verify
					} else if (bDoFlash
					    && *pDst == 0xFF
					    && (status = FLASH_ProgramByte((uint32_t)pDst, *pSrc)) == FLASH_COMPLETE
					    && *pDst == *pSrc
					) { // 1 byte flash
						CHW_MCU::KickIWDG();
					} else {
						ASSERTE(false && "flash 1 failed");
						break;
					}
					pSrc += 1; pDst += 1; m_uiCommitedOffset += 1;
				}
			}
		} while (false);
		if (bDoFlash) FLASH_Lock();
	};
	void DoReset() {
		NVIC_SystemReset();
	};
public: // INetworkUdpListener
	virtual const CChainedConstChunks* FilterIncomeUDP(const uint8_t* pData, unsigned uiSize);
} g_UDPFlashListener;

const CChainedConstChunks* CUDPFlashListener:: FilterIncomeUDP(const uint8_t* pData, unsigned uiSize)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkUdpListener::FilterIncomeUDP(pData, uiSize));
	if (uiSize >= sizeof(UdpRequest_Header_t)) {
		const UdpRequest_t* pUdpRequest = (const UdpRequest_t*)pData;
		unsigned uiSequence = ntohs(pUdpRequest->hdr.uiSequence);
		unsigned uiRequestType = ntohs(pUdpRequest->hdr.uiReqType);

		switch (uiRequestType) {
			case QUERY_TYPE_GET_INFO: {
				if (uiSize >= sizeof(pUdpRequest->req_get_info)) {
					ReplyGetInfo(uiSequence);
					return &m_UdpReplyChunk;
				}
			} break;
			case QUERY_TYPE_UPLOAD_UPDATE: {
				if (uiSize >= sizeof(pUdpRequest->req_upload_update)) {
					unsigned uiOffset = ntohl(pUdpRequest->req_upload_update.uiOffset);
					unsigned uiSize = ntohl(pUdpRequest->req_upload_update.uiSize);
					const uint8_t* pData = (const uint8_t*)(&(pUdpRequest->req_upload_update) + 1);
					DoVerifyAndFlash(uiOffset, uiSize, pData, true);
					ReplyUploadUpdate(uiSequence);
					return &m_UdpReplyChunk;
				}
			} break;
			case QUERY_TYPE_VERIFY_UPDATE: {
				if (uiSize >= sizeof(pUdpRequest->req_verify_update)) {
					unsigned uiOffset = ntohl(pUdpRequest->req_verify_update.uiOffset);
					unsigned uiSize = ntohl(pUdpRequest->req_verify_update.uiSize);
					const uint8_t* pData = (const uint8_t*)(&(pUdpRequest->req_verify_update) + 1);
					DoVerifyAndFlash(uiOffset, uiSize, pData, false);
					ReplyVerifyUpdate(uiSequence);
					return &m_UdpReplyChunk;
				}
			} break;
			case QUERY_TYPE_RESET: {
				DoReset();
				return NULL;
			} break;
		}
	}
	return NULL; // no reply for wrong size
};

void InitUDPFlashListener(INetworkStack* pNetworkStack, unsigned uiPort)
{
	g_UDPFlashListener.Init(pNetworkStack);
	pNetworkStack->RegisterUDPListener(uiPort, &g_UDPFlashListener);
	DEBUG_INFO("Flash listener started. UDP Port:", uiPort);
}
