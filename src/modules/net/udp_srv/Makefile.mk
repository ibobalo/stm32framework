USE_NET_UDPSRV_FLASH += $(if $(filter YES,$(USE_NET_UDPSRV)), \
	YES \
)
USE_NET_UDPSRV_PARAMS += $(if $(filter YES,$(USE_NET_UDPSRV)), $(if $(filter YES,$(USE_PARAMS)), \
	YES \
))
USE_NET_UDPSRV_DEBUGINFO += $(if $(filter YES,$(USE_NET_UDPSRV)), $(if $(filter YES,$(DEBUG)), \
	YES \
))
SRC += $(if $(filter YES,$(USE_NET_UDPSRV)), $(addprefix src/modules/net/udp_srv/, \
	udp_flash.cpp \
	$(if $(filter YES,$(USE_PARAMS)),udp_params.cpp) \
	$(if $(filter YES,$(DEBUG)),udp_debug_info.cpp) \
))
