#ifndef UDP_DEBUG_INFO_H_INCLUDED
#define UDP_DEBUG_INFO_H_INCLUDED

#include "../net_stack.h"

void InitUDPDebugInfoListener(INetworkStack* pNetworkStack, unsigned uiPort);

#endif // UDP_DEBUG_INFO_H_INCLUDED
