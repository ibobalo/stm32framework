#include "stdafx.h"
#include "udp_debug_info.h"
#include "util/core.h"
#include "util/macros.h"
#include "util/endians.h"
#include "hw/debug.h"
#include "stm32_linker.h"
#include <string.h>

// declarations
class CUDPDebugInfoListener :
		public INetworkUdpListener
{
private:
	enum {
		QUERY_TYPE_DEBUG_INFO = 1,
		QUERY_TYPE_MSG_TEXT = 2
	};
	typedef struct __attribute__ ((packed)) {
		uint16_t                       uiSequence;
		uint16_t                       uiReqType;
	} UdpRequest_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		uint16_t                       uiMromMsgIndex;
	} UdpRequest_DebugInfo_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpRequest_Header_t            hdr;
		uint32_t                       uiMsgId;
	} UdpRequest_MsgText_Header_t;
	typedef struct __attribute__ ((packed)) {
		union {
			UdpRequest_Header_t            hdr;
			UdpRequest_DebugInfo_Header_t  req_debug_info;
			UdpRequest_MsgText_Header_t    req_msg_text;
		};
	} UdpRequest_t;

	enum {
		RESPONCE_TYPE_DEBUG_INFO = QUERY_TYPE_DEBUG_INFO,
		RESPONCE_TYPE_MSG_TEXT = QUERY_TYPE_MSG_TEXT,
	};
	typedef struct __attribute__ ((packed)) {
		uint16_t                     uiSequence;
		uint16_t                     uiRespType;
	} UdpReply_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint16_t                     uiCount;
		uint16_t                     uiItemSize;
		uint16_t                     uiNextHead;
	} UdpReply_DebugInfo_Header_t;
	typedef struct __attribute__ ((packed)) {
		UdpReply_Header_t            hdr;
		uint32_t                     uiMsgId;
		uint16_t                     uiLength;
	} UdpReply_MsgText_Header_t;

	typedef struct __attribute__ ((packed)) {
		union {
			UdpReply_Header_t            hdr;
			UdpReply_DebugInfo_Header_t  debug_info_hdr;
			UdpReply_MsgText_Header_t    msg_text_hdr;
		};
	} UdpReply_t;
	UdpReply_t m_UdpReplyBuffer;

	CChainedConstChunks m_UdpReplyChunks[2];
	INetworkStack*      m_pNetworkStack;
	unsigned            m_uiMaxMsgCount;
	unsigned            m_uiMaxMsgTextLength;
public:
	void Init(INetworkStack* pNetworkStack) {
		m_pNetworkStack = pNetworkStack;
		m_uiMaxMsgCount = (m_pNetworkStack->GetUDPMTU() - sizeof(m_UdpReplyBuffer.debug_info_hdr)) / get_debug_info_record_size();
		m_uiMaxMsgTextLength = (m_pNetworkStack->GetUDPMTU() - sizeof(m_UdpReplyBuffer.msg_text_hdr)) - 1;
		m_UdpReplyChunks[1].Break();
	};
public: // INetworkUdpListener
	virtual const CChainedConstChunks* FilterIncomeUDP(const uint8_t* pData, unsigned uiSize);
} g_UDPDebugInfoListener;

const CChainedConstChunks* CUDPDebugInfoListener::FilterIncomeUDP(const uint8_t* pData, unsigned uiSize)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkUdpListener::FilterIncomeUDP(pData, uiSize));
	if (uiSize >= sizeof(UdpRequest_Header_t)) {
		const UdpRequest_t* pUdpRequest = (const UdpRequest_t*)pData;
		unsigned uiSequence = ntohs(pUdpRequest->hdr.uiSequence);

		switch (ntohs(pUdpRequest->hdr.uiReqType)) {
			case QUERY_TYPE_DEBUG_INFO: {
				if (uiSize >= sizeof(pUdpRequest->req_debug_info)) {
					unsigned uiFromIndex = get_debug_info_wrapped_index(ntohs(pUdpRequest->req_debug_info.uiMromMsgIndex));
					const void* pMsgs = NULL;
					unsigned uiMsgCount = LIMIT(get_debug_info_internals(uiFromIndex, &pMsgs), 0U, m_uiMaxMsgCount);
					m_UdpReplyBuffer.hdr.uiSequence = htons((uint16_t)uiSequence);
					m_UdpReplyBuffer.hdr.uiRespType = htons((uint16_t)RESPONCE_TYPE_DEBUG_INFO);
					m_UdpReplyBuffer.debug_info_hdr.uiCount = htons((uint16_t)uiMsgCount);
					m_UdpReplyBuffer.debug_info_hdr.uiItemSize = htons((uint16_t)get_debug_info_record_size());
					m_UdpReplyBuffer.debug_info_hdr.uiNextHead = htons((uint16_t)get_debug_info_wrapped_index(uiFromIndex + uiMsgCount));
					m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.debug_info_hdr));
					if (uiMsgCount) {
						m_UdpReplyChunks[0].Append(&m_UdpReplyChunks[1]);
						m_UdpReplyChunks[1].Assign((const uint8_t*)pMsgs, get_debug_info_record_size() * uiMsgCount);
					} else {
						m_UdpReplyChunks[0].Break();
					}
					return m_UdpReplyChunks;
				}
			} break;
			case QUERY_TYPE_MSG_TEXT: {
				if (uiSize >= sizeof(pUdpRequest->req_msg_text)) {
					const char* p = (const char*)ntohl(pUdpRequest->req_msg_text.uiMsgId);
					unsigned uiLength = 0;
					if (1
					 && p >= GetLinkerSymbol(const char*, ro_data_start)
					 && p < GetLinkerSymbol(const char*, ro_data_end)
					) {
						uiLength = strnlen(p, m_uiMaxMsgTextLength);
						if (p + uiLength < GetLinkerSymbol(const char*, ro_data_end)) {
							// ok
						} else {
							uiLength = 0;
						}
					};
					m_UdpReplyBuffer.hdr.uiSequence = htons((uint16_t)uiSequence);
					m_UdpReplyBuffer.hdr.uiRespType = htons((uint16_t)RESPONCE_TYPE_MSG_TEXT);
					m_UdpReplyBuffer.msg_text_hdr.uiMsgId = pUdpRequest->req_msg_text.uiMsgId;
					m_UdpReplyBuffer.msg_text_hdr.uiLength = htons((uint16_t)uiLength);
					m_UdpReplyChunks[0].Assign((const uint8_t*)&m_UdpReplyBuffer, sizeof(m_UdpReplyBuffer.msg_text_hdr));
					if (uiLength) {
						m_UdpReplyChunks[0].Append(&m_UdpReplyChunks[1]);
						m_UdpReplyChunks[1].Assign((const uint8_t*)p, uiLength);
					} else {
						m_UdpReplyChunks[0].Break();
					}
					return m_UdpReplyChunks;
				}
			} break;
		}
	}
	return NULL; // no reply for wrong size
};

void InitUDPDebugInfoListener(INetworkStack* pNetworkStack, unsigned uiPort)
{
	g_UDPDebugInfoListener.Init(pNetworkStack);
	pNetworkStack->RegisterUDPListener(uiPort, &g_UDPDebugInfoListener);
}
