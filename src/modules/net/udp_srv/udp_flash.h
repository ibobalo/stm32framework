#ifndef UDP_FLASH_H_INCLUDED
#define UDP_FLASH_H_INCLUDED

#include "../net_stack.h"

void InitUDPFlashListener(INetworkStack* pNetworkStack, unsigned uiPort);

#endif // UDP_FLASH_H_INCLUDED
