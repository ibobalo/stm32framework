#include "stdafx.h"
#include "net_init.h"
#include "params.h"
#include "hw/hw.h"

#ifdef USE_NET
#  include "net/net_stack.h"
#  ifdef USE_NET_UDPSRV_PARAMS
#    include "net/udp_srv/udp_params.h"
#  endif
#  ifdef USE_NET_UDPSRV_FLASH
#    include "net/udp_srv/udp_flash.h"
#  endif
#  ifdef USE_NET_UDPSRV_DEBUGINFO
#    include "net/udp_srv/udp_debug_info.h"
#  endif
#endif

void net_init_cfg(void)
{
#ifdef USE_NET
	INetworkInterface* pInterface = NULL;
	INetworkStack::ip_addr_t ip_addr, gw;
	bool bIpChecksumVerification;
#ifdef USE_NET_SLIP
	unsigned uiSlipUartIndex = GET_PARAM_VALUE(SLIP_UART);
	if (uiSlipUartIndex) {
		TSerialConnectionParams params = {GET_PARAM_VALUE(SLIP_BAUDRATE), (ISerial::StopBits)GET_PARAM_VALUE(SLIP_STOPBITS), (ISerial::ParityBits)GET_PARAM_VALUE(SLIP_PARITYBITS)};
		pInterface = CHW_Brd::GetNetworkInterfaceSlip(uiSlipUartIndex, &params);
		ip_addr = IP_ADDR(
				GET_PARAM_VALUE(SLIP_IP_Address_1),
				GET_PARAM_VALUE(SLIP_IP_Address_2),
				GET_PARAM_VALUE(SLIP_IP_Address_3),
				GET_PARAM_VALUE(SLIP_IP_Address_4)
		);
		gw = IP_ADDR(
				GET_PARAM_VALUE(SLIP_IP_Address_1),
				GET_PARAM_VALUE(SLIP_IP_Address_2),
				GET_PARAM_VALUE(SLIP_IP_Address_3),
				1
		);
		bIpChecksumVerification = true;
	}
#endif //USE_NET_SLIP
#ifdef USE_NET_ENC28J60
	if (!pInterface) {
		pInterface = CHW_Brd::GetNetworkInterfaceLan();
		ip_addr = IP_ADDR(
				GET_PARAM_VALUE(LAN_IP_Address_1),
				GET_PARAM_VALUE(LAN_IP_Address_2),
				GET_PARAM_VALUE(LAN_IP_Address_3),
				GET_PARAM_VALUE(LAN_IP_Address_4)
		);
		gw = IP_ADDR(
				GET_PARAM_VALUE(LAN_IP_Address_1),
				GET_PARAM_VALUE(LAN_IP_Address_2),
				GET_PARAM_VALUE(LAN_IP_Address_3),
				1
		);
		bIpChecksumVerification = false;
	}
#endif //USE_NET_ENC28J60
	if (pInterface) {
		GetNetworkStackInterface()->Init(pInterface, ip_addr, IP_ADDR(255,255,255,0), gw);
		GetNetworkStackInterface()->SetIpChecksumVerification(bIpChecksumVerification);
	}
#endif //USE_NET
}

void net_init_srvs(void)
{
#ifdef USE_NET_UDPSRV_PARAMS
		InitUDPParamsListener(GetNetworkStackInterface(), 5020);
#endif
#ifdef USE_NET_UDPSRV_FLASH
		InitUDPFlashListener(GetNetworkStackInterface(), 5022);
#endif
#ifdef USE_NET_UDPSRV_DEBUGINFO
		InitUDPDebugInfoListener(GetNetworkStackInterface(), 5015);
#endif
}
