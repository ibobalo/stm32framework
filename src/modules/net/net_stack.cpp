#include "stdafx.h"
#include "net_stack.h"
#include "util/core.h"
#include "util/macros.h"
#include "util/utils.h"
#include "util/endians.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "modules/keyscanner/proc_keyscanner.h" // ILed

typedef INetworkInterface::mac_addr_t mac_addr_t;
typedef INetworkStack::ip_addr_t ip_addr_t;

#define MAX_UDP_LISTENERS  8
#define ARP_CACHE_SIZE     2

#define ARP_HW_TYPE_ETH      htons((uint16_t)0x0001)
#define ARP_PROTO_TYPE_IP    htons((uint16_t)0x0800)
#define ARP_TYPE_REQUEST     htons((uint16_t)1)
#define ARP_TYPE_RESPONSE    htons((uint16_t)2)

#define IP_PROTOCOL_ICMP     1
#define IP_PROTOCOL_TCP      6
#define IP_PROTOCOL_UDP      17
#define IP_PACKET_TTL		 64

#define ICMP_TYPE_ECHO_RQ    8
#define ICMP_TYPE_ECHO_RPLY  0

INetworkInterface::mac_addr_t  g_MacAddrBroadcast = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
INetworkStack::ip_addr_t       g_IpAddrBroadcast = 0xFFFFFFFF;

typedef struct __attribute__ ((packed)) arp_message_ip {
	uint16_t    hw_type;
	uint16_t    proto_type;
	uint8_t     hw_addr_len;
	uint8_t     proto_addr_len;
	uint16_t    type;
	mac_addr_t  mac_addr_from;
	ip_addr_t   ip_addr_from;
	mac_addr_t  mac_addr_to;
	ip_addr_t   ip_addr_to;
} arp_message_ip_t;

typedef struct __attribute__ ((packed)) ip_packet {
    uint8_t ver_head_len;
    uint8_t tos;
    uint16_t total_len;
    uint16_t fragment_id;
    uint16_t flags_framgent_offset;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t cksum;
    ip_addr_t from_addr;
    ip_addr_t to_addr;
//    uint8_t data[];
} ip_packet_t;

typedef struct __attribute__ ((packed)) icmp_echo_packet {
    uint8_t type;
    uint8_t code;
    uint16_t cksum;
    uint16_t id;
    uint16_t seq;
//    uint8_t data[];
} icmp_echo_packet_t;

typedef struct __attribute__ ((packed)) udp_packet {
    uint16_t from_port;
    uint16_t to_port;
    uint16_t len;
    uint16_t cksum;
//    uint8_t data[];
} udp_packet_t;

typedef struct __attribute__ ((packed)) udp_pseudo_header {
    ip_addr_t from_addr;
    ip_addr_t to_addr;
    uint8_t   zero_pad;
    uint8_t   protocol;
    uint16_t  len;
} udp_pseudo_header_t;

class CNetworkStack :
		public INetworkStack
{
public:
	typedef INetworkInterface::mac_addr_t mac_addr_t;
	typedef INetworkStack::ip_addr_t ip_addr_t;
public: // INetworkStack
	virtual void Init(INetworkInterface* pInterface, ip_addr_t IpAddr, ip_addr_t IpMask, ip_addr_t IpGwAddr);
	virtual void SetIpChecksumVerification(bool bVerification);

	virtual ip_addr_t GetIpAddress() const;
	virtual ip_addr_t GetIpMask() const;
	virtual ip_addr_t GetIpGwAddress() const;

	virtual unsigned GetIPMTU() const;
	virtual unsigned GetUDPMTU() const;

	virtual bool SendFrame(const mac_addr_t* pTgtMacAddr, uint16_t uiFrameType, const CChainedConstChunks* pChain);
	virtual bool SendArp(ip_addr_t IpAddr);
	virtual bool SendIP(ip_addr_t IpAddr, uint8_t uiProtocol, const CChainedConstChunks* pChain);
	virtual bool SendUDP(ip_addr_t IpAddr, uint16_t uiPort, const CChainedConstChunks* pChain);

	virtual bool RegisterUDPListener(unsigned uiPort, INetworkUdpListener* pListener);
	virtual void SetPingLed(ILed* pLed);
	virtual void SetRxLed(ILed* pLed);
	virtual void SetTxLed(ILed* pLed);
	virtual void SetLiveLed(ILed* pLed);

private: // callbacks
	void OnLinkChanged(bool bLinkState);
	void OnTxDone(bool bTxSucceed);
	void OnPktReceived(unsigned uiPktDataSize);

private:
	void ArpPktFilter(const void* pData, unsigned uiSize);
	void  ArpRequestFilter(const arp_message_ip_t *pArpRequestMsg);
	void  ArpReplyFilter(const arp_message_ip_t *pArpRequestMsg);
	void ARPAnnounce();
	void IPPacketFilter(const void* pData, unsigned uiSize);
	void  ICMPPacketFilter(const void* pData, unsigned uiSize);
	void  UDPPacketFilter(const void* pData, unsigned uiSize);
	void   NotifyUdpReceived(unsigned uiPort, const uint8_t* pData, unsigned uiSize);
	void  TCPPacketFilter(const void* pData, unsigned uiSize);

	void ReplyFrame(unsigned uiSize);
	void ReplyFrame(const CChainedConstChunks* pReplyChain);
	void ReplyArp(const arp_message_ip_t* pArpRequestMsg, const mac_addr_t* pMacAddr, ip_addr_t IpAddr);
	void ReplyIP(unsigned uiSize);
	void ReplyIP(const CChainedConstChunks* pReplyChain);
	void ReplyUDP(const CChainedConstChunks* pReplyChain);

	void ClearArpCache();
	void UpdateArpCache(ip_addr_t IpAddr, const mac_addr_t* MacAddr);
	mac_addr_t* QueryArpCache(ip_addr_t IpAddr);

	uint16_t ChecksumUdpReply(ip_addr_t to_addr, const CChainedConstChunks* pUDPReplyChain);
	uint16_t ChecksumUdpRequest(const uint8_t* pData, unsigned uiDataLength, ip_addr_t from_addr, ip_addr_t to_addr);

private:
	INetworkInterface*    m_pInterface;
	ip_addr_t             m_IpAddr;
	ip_addr_t             m_IpMask;
	ip_addr_t             m_IpGwAddr;
	ip_addr_t             m_IpBroadcastAddr;
	mac_addr_t            m_ReplyMacAddr;
	bool                  m_bVerifyIpChecksums;
	union {
		arp_message_ip_t  m_ArpReplyMsg;

		struct __attribute__ ((packed)) {
			ip_packet_t           m_IpReplyPkt;
			union {
				icmp_echo_packet_t    m_IcmpReply;
				udp_packet_t          m_UdpReply;
			};
		};
		uint8_t           m_ReplyBuffer[1600];
	};

	struct {
		ip_addr_t   IpAddr;
		mac_addr_t  MacAddr;

		void Assign(ip_addr_t _IpAddr, mac_addr_t _MacAddr) {IpAddr = _IpAddr; MacAddr = _MacAddr;};
	}                     m_aArpCache[ARP_CACHE_SIZE];
	unsigned              m_nArpCacheNextIdx;

	CChainedConstChunks   m_FrameReplyChunk;
	CChainedConstChunks   m_ArpReplyChunk;
	CChainedConstChunks   m_IpReplyChunk;
	CChainedConstChunks   m_UdpReplyChunk;

	struct {
		unsigned              port;
		INetworkUdpListener*  pListener;
	} m_cbUdpListeners[MAX_UDP_LISTENERS];
	ILed*                 m_pLedPing;
	ILed*                 m_pLedRx;
	ILed*                 m_pLedTx;
	ILed*                 m_pLedLive;
};



uint32_t ChecksumAccumulate(uint32_t sum, uint8_t byte)
{
	return sum += byte;
}
uint32_t ChecksumAccumulate(uint32_t sum, uint16_t word)
{
	return sum += word;
}
uint32_t ChecksumAccumulate(uint32_t sum, ip_addr_t ip_addr)
{
	return sum += ((ip_addr >> 16) & 0xFFFF) + (ip_addr & 0xFFFF);
}

uint32_t ChecksumAccumulate(uint32_t sum, const uint8_t *buf, size_t len)
{
	if (sum & 0x80000000) { // even flag
		sum &= 0x7FFFFFFF; // clear even flag
		sum += (uint16_t) *buf;
		buf += 1;
		len -= 1;
	}
	while (len >= 2) {
		sum += ((uint16_t) *buf << 8) | *(buf + 1);
		buf += 2;
		len -= 2;
	}
	if (len) {
		sum += (uint16_t) *buf << 8;
		sum |= 0x80000000;
	}
	return sum;
}
uint16_t ChecksumFinalize(uint32_t sum)
{
	sum &= 0x7FFFFFFF; // clear even flag
	while (sum >> 16) {
		sum = (sum & 0xffff) + (sum >> 16);
	}
	return ~htons((uint16_t) sum);
}
uint16_t ip_cksum(uint32_t sum, const uint8_t *buf, size_t len)
{
	return ChecksumFinalize(  ChecksumAccumulate(sum, buf, len)  );
}

void CNetworkStack::Init(INetworkInterface* pInterface, ip_addr_t IpAddr, ip_addr_t IpMask, ip_addr_t IpGwAddr)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::Init(pInterface, IpAddr, IpMask, IpGwAddr));

	ASSERTE(pInterface);
	m_pInterface = pInterface;
	m_IpAddr = htonl(IpAddr);
	m_IpMask = htonl(IpMask);
	m_IpGwAddr = htonl(IpGwAddr);
	m_IpBroadcastAddr = m_IpAddr & ~m_IpMask;
	m_pInterface->BindNetworkInterface(
			BIND_MEM_CB(&CNetworkStack::OnPktReceived, this),
			BIND_MEM_CB(&CNetworkStack::OnLinkChanged, this)
	);
	m_bVerifyIpChecksums = false;
	ClearArpCache();
}
void CNetworkStack::SetIpChecksumVerification(bool bVerification)
{
	m_bVerifyIpChecksums = bVerification;
}

ip_addr_t CNetworkStack::GetIpAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::GetIpAddress());
	return ntohl(m_IpAddr);
}

ip_addr_t CNetworkStack::GetIpMask() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::GetIpMask());
	return ntohl(m_IpMask);
}

ip_addr_t CNetworkStack::GetIpGwAddress() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::GetIpGwAddress());
	return ntohl(m_IpGwAddr);
}

void CNetworkStack::OnLinkChanged(bool bLinkState)
{
	if (bLinkState) {
		ARPAnnounce();
	}
}

void CNetworkStack::OnTxDone(bool bTxSucceed)
{
	// TODO release reply buffer
}

unsigned CNetworkStack::GetIPMTU() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::GetIPMTU());
	return m_pInterface->GetMTU() - sizeof(ip_packet);
}
unsigned CNetworkStack::GetUDPMTU() const
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::GetUDPMTU());
	return m_pInterface->GetMTU() - sizeof(ip_packet) - sizeof(udp_packet);
}

bool CNetworkStack::SendFrame(const mac_addr_t* pTgtMacAddr, uint16_t uiFrameType, const CChainedConstChunks* pChain)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SendFrame(pTgtMacAddr, uiFrameType, pChain));

	m_pInterface->TxPkt(pTgtMacAddr, uiFrameType, pChain, BIND_MEM_CB(&CNetworkStack::OnTxDone, this));

	if (m_pLedTx) m_pLedTx->Pulse(1);
	return true;
}

bool CNetworkStack::SendArp(ip_addr_t IpAddr)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SendArp(IpAddr));

	m_ArpReplyMsg.hw_type = ARP_HW_TYPE_ETH;
	m_ArpReplyMsg.proto_type = ARP_PROTO_TYPE_IP;
	m_ArpReplyMsg.hw_addr_len = 6;
	m_ArpReplyMsg.proto_addr_len = 4;
	m_ArpReplyMsg.type = ARP_TYPE_REQUEST;
	m_ArpReplyMsg.mac_addr_from = *m_pInterface->GetMacAddress();
	m_ArpReplyMsg.ip_addr_from = m_IpAddr;
	m_ArpReplyMsg.mac_addr_to = g_MacAddrBroadcast;
	m_ArpReplyMsg.ip_addr_to = IpAddr;

	m_ArpReplyChunk.Assign((const uint8_t*)&m_ArpReplyMsg, sizeof(arp_message_ip_t));
	m_ArpReplyChunk.Break();

	return SendFrame(&g_MacAddrBroadcast, ETH_TYPE_ARP, &m_ArpReplyChunk);
}

bool CNetworkStack::SendIP(ip_addr_t IpAddr, uint8_t uiProtocol, const CChainedConstChunks* pIpChain)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SendIP(IpAddr, uiProtocol, pIpChain));

	const mac_addr_t* pTgtMacAddr = NULL;

	// ARP resolver
	pTgtMacAddr = QueryArpCache(IpAddr);

	if (!pTgtMacAddr) {
		SendArp(IpAddr);
		return false;
	}

	return SendFrame(pTgtMacAddr, ETH_TYPE_IP, pIpChain);
}

bool CNetworkStack::SendUDP(ip_addr_t IpAddr, uint16_t uiPort, const CChainedConstChunks* pChain)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SendUDP(IpAddr, uiPort, pChain));

	return SendIP(IpAddr, IP_PROTOCOL_UDP, pChain);
}

bool CNetworkStack::RegisterUDPListener(unsigned uiPort, INetworkUdpListener* pListener)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::RegisterUDPListener(uiPort, pListener));

	for (unsigned n = 0; n < MAX_UDP_LISTENERS; ++n) {
		if (!m_cbUdpListeners[n].pListener) {
			m_cbUdpListeners[n].port = uiPort;
			m_cbUdpListeners[n].pListener = pListener;
			return true;
		}
	}
	ASSERT("Increase MAX_UDP_LISTENERS");
	return false;
}

void CNetworkStack::SetPingLed(ILed* pLed)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SetPingLed(pLed));
	m_pLedPing = pLed;
}

void CNetworkStack::SetTxLed(ILed* pLed)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SetTxLed(pLed));
	m_pLedTx = pLed;
}

void CNetworkStack::SetRxLed(ILed* pLed)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SetRxLed(pLed));
	m_pLedRx = pLed;
}

void CNetworkStack::SetLiveLed(ILed* pLed)
{
	IMPLEMENTS_INTERFACE_METHOD(INetworkStack::SetLiveLed(pLed));

	m_pLedLive = pLed;
	m_pInterface->SetIncomeCheckCallback(BIND_MEM_CB(&ILed::Pulse<1>, pLed));
}

void CNetworkStack::ClearArpCache()
{
	for (unsigned n = 0; n < ARP_CACHE_SIZE; ++n) {
		m_aArpCache[n].IpAddr = 0;
	}
	m_nArpCacheNextIdx = 0;
}

void CNetworkStack::UpdateArpCache(ip_addr_t IpAddr, const mac_addr_t* pMacAddr)
{
	mac_addr_t *pExistingMacAddr = QueryArpCache(IpAddr);
	if (pExistingMacAddr) {
		// update
		*pExistingMacAddr = *pMacAddr;
	} else {
		// insert new
		m_aArpCache[m_nArpCacheNextIdx].Assign(IpAddr, *pMacAddr);
		if (++m_nArpCacheNextIdx >= ARP_CACHE_SIZE) {m_nArpCacheNextIdx = 0;}
	}
}

mac_addr_t* CNetworkStack::QueryArpCache(ip_addr_t IpAddr)
{
	for (unsigned n = 0; n < ARP_CACHE_SIZE; ++n) {
		if (m_aArpCache[n].IpAddr == IpAddr) {
			return &m_aArpCache[n].MacAddr;
		}
	}
	return NULL;
}

void CNetworkStack::OnPktReceived(unsigned uiPktDataSize)
{
	uint16_t uiFrameType = m_pInterface->GetRxedFrameType();
	if (uiFrameType == ETH_TYPE_IP) {
		IPPacketFilter(m_pInterface->GetRxedFrameData(), m_pInterface->GetRxedFrameSize());
	} else if (uiFrameType == ETH_TYPE_ARP) {
		ArpPktFilter(m_pInterface->GetRxedFrameData(), m_pInterface->GetRxedFrameSize());
	}
	m_pInterface->ReleaseRxedFrame();
	if (m_pLedRx) m_pLedRx->Pulse(1);
}

void CNetworkStack::ArpPktFilter(const void* pData, unsigned uiSize)
{
	if (uiSize >= sizeof(arp_message_ip_t)) {
		const arp_message_ip_t *msg = (arp_message_ip_t *)pData;
		if (   (msg->hw_type    == ARP_HW_TYPE_ETH)
			&& (msg->proto_type == ARP_PROTO_TYPE_IP)
			&& (msg->hw_addr_len == 6)
			&& (msg->proto_addr_len == 4)
		) {
			if (msg->type == ARP_TYPE_REQUEST) {
				ArpRequestFilter(msg);
			} else {
				ArpReplyFilter(msg);
			}
		}
	}
}

void CNetworkStack::ArpRequestFilter(const arp_message_ip_t* pArpRequestMsg)
{
	if (1
		&& (pArpRequestMsg->ip_addr_to == m_IpAddr)
	) {
		UpdateArpCache(pArpRequestMsg->ip_addr_from, &pArpRequestMsg->mac_addr_from);
		ReplyArp(pArpRequestMsg, m_pInterface->GetMacAddress(), m_IpAddr);
	}
}

void CNetworkStack::ArpReplyFilter(const arp_message_ip_t* pArpRequestMsg)
{
	UpdateArpCache(pArpRequestMsg->ip_addr_from, &pArpRequestMsg->mac_addr_from);
}

void CNetworkStack::ARPAnnounce()
{
	SendArp(m_IpAddr);
}

void CNetworkStack::IPPacketFilter(const void* pData, unsigned uiSize)
{
	if (m_pInterface->GetRxedFrameSize() >= sizeof(ip_packet_t)) {
		const ip_packet_t *pkt = (ip_packet_t *)(m_pInterface->GetRxedFrameData());
		if ((pkt->ver_head_len == 0x45) && (pkt->to_addr == m_IpAddr || pkt->to_addr == m_IpBroadcastAddr)) {
			if (!m_bVerifyIpChecksums || ip_cksum(0, (uint8_t*)pkt, sizeof(ip_packet_t)) == 0) {

//				UpdateArpCache(pkt->from_addr, m_pInterface->GetRxedFrameSourceMacAddr());

				const uint8_t* pData = (const uint8_t*)(pkt + 1);
				unsigned uiLength = ntohs(pkt->total_len) - sizeof(ip_packet_t);
				switch (pkt->protocol) {
				case IP_PROTOCOL_ICMP:
					if (!m_bVerifyIpChecksums || ip_cksum(0, pData, uiLength) == 0) {
						ICMPPacketFilter(pData, uiLength);
					}
					break;
				case IP_PROTOCOL_UDP:
					if (!m_bVerifyIpChecksums || ChecksumUdpRequest(pData, uiLength, pkt->from_addr, pkt->to_addr) == 0) {
						UDPPacketFilter(pData, uiLength);
					}
					break;
				case IP_PROTOCOL_TCP:
					TCPPacketFilter(pData, uiLength);
					break;
				}
			}
		}
	}
}

void  CNetworkStack::ICMPPacketFilter(const void* pData, unsigned uiSize)
{
	if (uiSize >= sizeof(icmp_echo_packet_t)) {
		const icmp_echo_packet_t* pIcmpRequest = (const icmp_echo_packet_t*)pData;
		unsigned uiIcmpRequestDataSize = uiSize - sizeof(icmp_echo_packet_t);
		if (pIcmpRequest->type == ICMP_TYPE_ECHO_RQ) {
			if (m_pLedPing) m_pLedPing->Pulse(1);
			icmp_echo_packet_t* pIcmpReply = (icmp_echo_packet_t*)&(m_IcmpReply);
			const unsigned uiReplyIcmpDataCapacity = ARRAY_SIZE(m_ReplyBuffer) - sizeof(ip_packet_t) - sizeof(icmp_echo_packet_t);
			uint8_t* pIcmpReplyData = (uint8_t*)(pIcmpReply + 1);
			const uint8_t* pIcmpRequestData = (const uint8_t*)(pIcmpRequest + 1);
			unsigned uiIcmpReplyDataSize = MIN2(uiIcmpRequestDataSize, uiReplyIcmpDataCapacity);
			*pIcmpReply = *pIcmpRequest;
			pIcmpReply->type = ICMP_TYPE_ECHO_RPLY;
			pIcmpReply->cksum += 8; /* only one field changed, so dirty hack with checksum */
			memcpy(pIcmpReplyData, pIcmpRequestData, uiIcmpReplyDataSize);
			ReplyIP(sizeof(*pIcmpReply) + uiIcmpReplyDataSize);
		}
	}
}

void  CNetworkStack::UDPPacketFilter(const void* pData, unsigned uiSize)
{
	if (uiSize >= sizeof(udp_packet_t)) {
		const udp_packet_t* pUdpRequest = (const udp_packet_t*)pData;
		uint16_t uiUdpLength = ntohs(pUdpRequest->len);
		if (uiUdpLength <= uiSize) {
			uint16_t uiToPort = ntohs(pUdpRequest->to_port);
			const uint8_t* pUdpData = (const uint8_t*)(pUdpRequest + 1);
			unsigned uiUdpRequestDataSize = uiUdpLength - sizeof(udp_packet_t);
			NotifyUdpReceived(uiToPort, pUdpData, uiUdpRequestDataSize);
		}
	}
}

void CNetworkStack::NotifyUdpReceived(unsigned uiPort, const uint8_t* pData, unsigned uiSize)
{
	for (unsigned n = 0; n < MAX_UDP_LISTENERS; ++n) {
		if (m_cbUdpListeners[n].port == uiPort && m_cbUdpListeners[n].pListener) {
			const CChainedConstChunks* pReplyChain = m_cbUdpListeners[n].pListener->FilterIncomeUDP(pData, uiSize);
			if (pReplyChain) {
				ReplyUDP(pReplyChain);
			}
			break;
		}
	}
}

void  CNetworkStack::TCPPacketFilter(const void* pData, unsigned uiSize)
{
	// TODO
}

void CNetworkStack::ReplyFrame(unsigned uiSize)
{
	ASSERTE(uiSize <= ARRAY_SIZE(m_ReplyBuffer));
	m_ReplyMacAddr = *m_pInterface->GetRxedFrameSourceMacAddr();
	m_FrameReplyChunk.Assign(m_ReplyBuffer, uiSize);
	m_FrameReplyChunk.Break();
	m_pInterface->TxPkt(&m_ReplyMacAddr, m_pInterface->GetRxedFrameType(), &m_FrameReplyChunk, BIND_MEM_CB(&CNetworkStack::OnTxDone, this));
	if (m_pLedTx) m_pLedTx->Pulse(1);
}

void CNetworkStack::ReplyFrame(const CChainedConstChunks* pReplyChain)
{
	m_ReplyMacAddr = *m_pInterface->GetRxedFrameSourceMacAddr();
	m_pInterface->TxPkt(&m_ReplyMacAddr, m_pInterface->GetRxedFrameType(), pReplyChain, BIND_MEM_CB(&CNetworkStack::OnTxDone, this));
	if (m_pLedTx) m_pLedTx->Pulse(1);
}

void CNetworkStack::ReplyArp(const arp_message_ip_t* pArpRequestMsg, const mac_addr_t* pMacAddr, ip_addr_t IpAddr)
{
	m_ArpReplyMsg.hw_type = pArpRequestMsg->hw_type;
	m_ArpReplyMsg.proto_type = pArpRequestMsg->proto_type;
	m_ArpReplyMsg.hw_addr_len = pArpRequestMsg->hw_addr_len;
	m_ArpReplyMsg.proto_addr_len = pArpRequestMsg->proto_addr_len;
	m_ArpReplyMsg.type = ARP_TYPE_RESPONSE;
	m_ArpReplyMsg.mac_addr_from = *pMacAddr;
	m_ArpReplyMsg.ip_addr_from = IpAddr;
	m_ArpReplyMsg.mac_addr_to = pArpRequestMsg->mac_addr_from;
	m_ArpReplyMsg.ip_addr_to = pArpRequestMsg->ip_addr_from;

	ReplyFrame(sizeof(arp_message_ip_t));
}

void CNetworkStack::ReplyIP(unsigned uiSize)
{
	ip_packet_t* pIpPacket = (ip_packet_t*)m_ReplyBuffer;
	const ip_packet_t *pRequestIpPkt = (const ip_packet_t *)(m_pInterface->GetRxedFrameData());
	pIpPacket->ver_head_len = 0x45;
	pIpPacket->tos = pRequestIpPkt->tos;
    pIpPacket->total_len = htons((uint16_t)(uiSize + sizeof(ip_packet_t)));
    pIpPacket->fragment_id = 0;
    pIpPacket->flags_framgent_offset = 0;
    pIpPacket->ttl = IP_PACKET_TTL;
    pIpPacket->protocol = pRequestIpPkt->protocol;
    pIpPacket->cksum = 0;
    pIpPacket->to_addr = pRequestIpPkt->from_addr;
    pIpPacket->from_addr = m_IpAddr;
    pIpPacket->cksum = ip_cksum(0, (uint8_t*)pIpPacket, sizeof(ip_packet_t));

	ReplyFrame(uiSize + sizeof(ip_packet_t));
}

void CNetworkStack::ReplyIP(const CChainedConstChunks* pReplyChain)
{
	ip_packet_t* pIpPacket = (ip_packet_t*)m_ReplyBuffer;
	const ip_packet_t *pRequestIpPkt = (const ip_packet_t *)(m_pInterface->GetRxedFrameData());
	pIpPacket->ver_head_len = 0x45;
	pIpPacket->tos = pRequestIpPkt->tos;
    pIpPacket->total_len = htons((uint16_t)(CollectChainTotalLength(pReplyChain) + sizeof(ip_packet_t)));
    pIpPacket->fragment_id = 0;
    pIpPacket->flags_framgent_offset = 0;
    pIpPacket->ttl = IP_PACKET_TTL;
    pIpPacket->protocol = pRequestIpPkt->protocol;
    pIpPacket->cksum = 0;
    pIpPacket->to_addr = pRequestIpPkt->from_addr;
    pIpPacket->from_addr = m_IpAddr;
    pIpPacket->cksum = ip_cksum(0, (uint8_t*)pIpPacket, sizeof(ip_packet_t));

	m_IpReplyChunk.Assign((const uint8_t*)pIpPacket, sizeof(ip_packet_t));
	m_IpReplyChunk.Append(pReplyChain);

	ReplyFrame(&m_IpReplyChunk);
}

void CNetworkStack::ReplyUDP(const CChainedConstChunks* pReplyChain)
{
	unsigned uiSize = CollectChainTotalLength(pReplyChain);
	const ip_packet_t *pRequestIpPkt = (const ip_packet_t *)(m_pInterface->GetRxedFrameData());
	const udp_packet_t* pUdpRequest = (const udp_packet_t*)(pRequestIpPkt + 1);
	ip_packet_t* pIpPacket = (ip_packet_t*)m_ReplyBuffer;
	udp_packet_t* pUdpReply = (udp_packet_t*)(pIpPacket + 1);
	pUdpReply->from_port = pUdpRequest->to_port;
	pUdpReply->to_port = pUdpRequest->from_port;
	pUdpReply->len = htons((uint16_t)(sizeof(udp_packet_t) + uiSize));
	pUdpReply->cksum = 0;

	m_UdpReplyChunk.Assign((const uint8_t*)pUdpReply, sizeof(udp_packet_t));
	m_UdpReplyChunk.Append(pReplyChain);

	pUdpReply->cksum = ChecksumUdpReply(pRequestIpPkt->from_addr, &m_UdpReplyChunk);

	ReplyIP(&m_UdpReplyChunk);
}

uint16_t CNetworkStack::ChecksumUdpReply(ip_addr_t to_addr, const CChainedConstChunks* pUdpReplyChain)
{
	udp_pseudo_header_t udp_pseudo_header;
	udp_pseudo_header.from_addr = m_IpAddr;
	udp_pseudo_header.to_addr = to_addr;
	udp_pseudo_header.zero_pad = 0;
	udp_pseudo_header.protocol = IP_PROTOCOL_UDP;
	udp_pseudo_header.len = htons((uint16_t)CollectChainTotalLength(pUdpReplyChain));

	uint32_t cksum = ChecksumAccumulate(0, (const uint8_t*)&udp_pseudo_header, sizeof(udp_pseudo_header));
	const CChainedConstChunks* pChunk = pUdpReplyChain;
	while (pChunk) {
		cksum = ChecksumAccumulate(cksum, pChunk->pData, pChunk->uiLength);
		pChunk = pChunk->GetNextItem();
	}
	return ChecksumFinalize( cksum );
}
uint16_t CNetworkStack::ChecksumUdpRequest(const uint8_t* pData, unsigned uiDataLength, ip_addr_t from_addr, ip_addr_t to_addr)
{
	udp_pseudo_header_t udp_pseudo_header;
	udp_pseudo_header.from_addr = from_addr;
	udp_pseudo_header.to_addr = to_addr;
	udp_pseudo_header.zero_pad = 0;
	udp_pseudo_header.protocol = IP_PROTOCOL_UDP;
	udp_pseudo_header.len = htons((uint16_t)uiDataLength);

	uint32_t cksum = ChecksumAccumulate(0, (const uint8_t*)&udp_pseudo_header, sizeof(udp_pseudo_header));
	cksum = ChecksumAccumulate(cksum, pData, uiDataLength);
	return ChecksumFinalize( cksum );
}

CNetworkStack g_NetworkStack;
INetworkStack* GetNetworkStackInterface()
{
	return &g_NetworkStack;
}
