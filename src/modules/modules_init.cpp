#include "stdafx.h"
#include "hw/hw.h"
#include "util/macros.h"
#ifdef USE_PROCESS_SCHEDULLER
#  include "tick_process.h"
#endif
#include "piped_uart.h"
#ifdef USE_RAMP
#  include "proc_ramp.h"
#  include "transformer.h"
#endif
#ifdef USE_TRIAC
#  include "zero_sync_pwm.h"
#endif
#ifdef USE_KEYSCANNER
#  include "proc_keyscanner.h"
#endif
#ifdef USE_MODBUS
#  include "proc_modbus.h"
#endif
#ifdef USE_PARAMS
#  include "params.h"
#endif
#ifdef USE_NET
#  include "net/net_stack.h"
#  include "net/net_init.h"
#endif
#ifdef USE_USB2UART
#  include "usb2uart.h"
#endif
#include <stdio.h>
#include <stdint.h>

extern "C" {

void modules_init(void)
{
#ifdef USE_PARAMS
	RestoreParamValues();
#endif

#ifdef USE_KEYSCANNER
	GetKeyboardInterface()->Init();
#endif

#ifdef USE_NET
	net_init_cfg();
	net_init_srvs();

	if (GetNetworkStackInterface()) {
#ifdef LED_NET_LIVE
		GetNetworkStackInterface()->SetLiveLed(GetLedsInterface()->AcquireLedInterface(LED_NET_LIVE));
#endif
#ifdef LED_NET_RX
		GetNetworkStackInterface()->SetRxLed(GetLedsInterface()->AcquireLedInterface(LED_NET_RX));
#endif
#ifdef LED_NET_TX
		GetNetworkStackInterface()->SetTxLed(GetLedsInterface()->AcquireLedInterface(LED_NET_TX));
#endif
#ifdef LED_NET_PING
		GetNetworkStackInterface()->SetPingLed(GetLedsInterface()->AcquireLedInterface(LED_NET_PING));
#endif 
	}

#endif // USE_NET
}

} // extern "C"
