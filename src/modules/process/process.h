#ifndef PROCESS_H_INCLUDED
#define PROCESS_H_INCLUDED

#include "stdafx.h"
#include "hw/mcu/mcu.h"
#include "util/timesource.h"
#include <stddef.h>

template<class TimeSource> class TProcess;
template<class TimeSource> class TProcessScheduller;

template<class TimeSource>
class TProcess
{
public:
	typedef TimeSource CTimeSource;
	typedef typename TimeSource::time_t CTimeType;
	typedef typename TimeSource::time_delta_t CTimeDelta;
	typedef TProcessScheduller<TimeSource> CProcessScheduller;
public:
//	virtual ~IProcess();

	void Init(CProcessScheduller* pScheduller) {m_pScheduller = pScheduller; pNextProcess = NULL; ppPrevProcess = &pNextProcess;}
public: // TProcess<TimeSource>
	virtual void SingleStep(CTimeType ttTime) = 0;
public:
	void ContinueNow() {m_pScheduller->ScheduleProcessNow(this);}
	void ContinueAsap() {m_pScheduller->ScheduleProcessAsap(this);}
	void ContinueAfter(const typename CProcessScheduller::CProcessType* pAfterProcess) {m_pScheduller->ScheduleProcessAfter(this, pAfterProcess);}
	void ContinueAt(CTimeType ttTime) {m_pScheduller->ScheduleProcess(this, ttTime);}
	void ContinueDelay(CTimeDelta tdTimeDelta) {ContinueAt(GetTimeNow() + tdTimeDelta);}
	void Cancel() {m_pScheduller->CancelProcess(this);}
	CTimeType GetTimeNow() const {return m_pScheduller->GetTimeNow();}
	bool GetIsYieldRequested() const {return m_pScheduller->GetIsYieldRequested();}

private:
	CProcessScheduller*  m_pScheduller;
protected:
	bool IsScheduled() const {return ppPrevProcess != &pNextProcess;}
	friend class TProcessScheduller<TimeSource>;
	TProcess*   pNextProcess;
	TProcess**  ppPrevProcess;
	CTimeType   ttTimeToProcess;
};

template<class TimeSource>
class TProcessScheduller
{
public:
	typedef TimeSource CTimeSource;
	typedef typename TimeSource::time_t CTimeType;
	typedef typename TimeSource::time_delta_t CTimeDelta;
	typedef TProcess<TimeSource>  CProcessType;
public:
	void Init() {m_pFirstProcessScheduled = NULL; m_ppLastProcessScheduled = &m_pFirstProcessScheduled; m_bYieldRequested = false;};
public:
	CTimeType GetTimeNow() const {return CTimeSource::GetTimeNow();}
	bool GetIsYieldRequested() const {return m_bYieldRequested;};
	void ScheduleProcess(CProcessType* pProcess, CTimeType ttTimeToProcess) {
		ENTER_CRITICAL_SECTION;
		{
			if (IsProcessScheduled(pProcess)) Erase(pProcess);
			pProcess->ttTimeToProcess = ttTimeToProcess;
			CProcessType* pBeforeProcess = m_pFirstProcessScheduled;
			while (pBeforeProcess != NULL) {
				if ((int)(pBeforeProcess->ttTimeToProcess - ttTimeToProcess) > 0) {
					// insert before
					Insert(pProcess, pBeforeProcess);
//					INDIRECT_CALL(pProcess->SingleStep(ttTimeToProcess));
					break;
				} else {
					pBeforeProcess = pBeforeProcess->pNextProcess;
				}
			}
			if (pBeforeProcess == NULL) {
				Append(pProcess);
//				INDIRECT_CALL(pProcess->SingleStep(ttTimeToProcess));
			}
		}
		LEAVE_CRITICAL_SECTION;
	};
	void ScheduleProcessNow(CProcessType* pProcess) {ScheduleProcess(pProcess, GetTimeNow());}
	void ScheduleProcessAfter(CProcessType* pProcess, const CProcessType* pAfterProcess) {
		ENTER_CRITICAL_SECTION;
		{
			if (IsProcessScheduled(pAfterProcess)) {
				if (IsProcessScheduled(pProcess)) Erase(pProcess);
				pProcess->ttTimeToProcess = pAfterProcess->ttTimeToProcess;
				CProcessType* pBeforeProcess = pAfterProcess->pNextProcess;
				if (pBeforeProcess) {
					Insert(pProcess, pBeforeProcess);
				} else {
					Append(pProcess);
				}
//				INDIRECT_CALL(pProcess->SingleStep(pProcess->ttTimeToProcess));
			} else {
				ScheduleProcessAsap(pProcess);
			}
		}
		LEAVE_CRITICAL_SECTION;
	};
	void ScheduleProcessAsap(CProcessType* pProcess) {
		ENTER_CRITICAL_SECTION;
		{
			if (IsProcessScheduled(pProcess)) Erase(pProcess);
			pProcess->ttTimeToProcess = 0;
			CProcessType* pFirstProcess = m_pFirstProcessScheduled;
			if (pFirstProcess != NULL) {
				Insert(pProcess, pFirstProcess);
//				INDIRECT_CALL(pProcess->SingleStep(ttTimeToProcess));
			} else {
				Append(pProcess);
//				INDIRECT_CALL(pProcess->SingleStep(ttTimeToProcess));
			}
		}
		m_bYieldRequested = true;
		LEAVE_CRITICAL_SECTION;
	}
	CProcessType* Pop(CTimeType ttTimeNow) {
		CProcessType* pResult = NULL;
		ENTER_CRITICAL_SECTION;
		{
			CProcessType* pProcess = m_pFirstProcessScheduled;
			if (pProcess && (int)(ttTimeNow - pProcess->ttTimeToProcess) >= 0) {
				if (IsProcessScheduled(pProcess)) Erase(pProcess);
				pResult = pProcess;
			}
		}
		LEAVE_CRITICAL_SECTION;
		return pResult;
	}
	void CancelProcess(CProcessType* pProcess) {
		ENTER_CRITICAL_SECTION;
		{
			if (IsProcessScheduled(pProcess)) Erase(pProcess);
		}
		LEAVE_CRITICAL_SECTION;
	};
	bool ProcessNextProcess() {
		CTimeType ttTimeNow = GetTimeNow();
		CProcessType* pProcess = Pop(ttTimeNow);
		if (!pProcess) {
			return false;
		}
		pProcess->SingleStep(GetTimeNow());
		m_bYieldRequested = false;
		return true;
	}

private:
	bool IsProcessScheduled(const CProcessType* pProcess) const { return pProcess->IsScheduled();}
	void Append(CProcessType* pProcess) {
		ASSERTE(pProcess);
		ASSERTE(pProcess->pNextProcess == NULL);
		ASSERTE(pProcess->ppPrevProcess == &pProcess->pNextProcess);
		pProcess->ppPrevProcess = m_ppLastProcessScheduled;
		*m_ppLastProcessScheduled = pProcess;
		m_ppLastProcessScheduled = &pProcess->pNextProcess;
//		ASSERTE(CheckList());
	}
	void Insert(CProcessType* pProcess, CProcessType* pBeforeProcess) {
//		ASSERTE(CheckList());
		ASSERTE(pProcess);
		ASSERTE(pProcess->pNextProcess == NULL);
		ASSERTE(pProcess->ppPrevProcess == &pProcess->pNextProcess);
		ASSERTE(pBeforeProcess != NULL);
		pProcess->pNextProcess = pBeforeProcess;
		pProcess->ppPrevProcess = pBeforeProcess->ppPrevProcess;
		*pBeforeProcess->ppPrevProcess = pProcess;
		pBeforeProcess->ppPrevProcess = &pProcess->pNextProcess;
//		ASSERTE(CheckList());
	};
	void Erase(CProcessType* pProcess) {
		ASSERTE(IsProcessScheduled(pProcess));
//		ASSERTE(CheckList());
		ASSERTE(pProcess != NULL);
		ASSERTE(m_pFirstProcessScheduled != NULL);
		CProcessType* pNextProcess = pProcess->pNextProcess;
		if (pNextProcess) {
			pNextProcess->ppPrevProcess = pProcess->ppPrevProcess;
		} else {
			// last
			ASSERTE(m_ppLastProcessScheduled == &pProcess->pNextProcess);
			m_ppLastProcessScheduled = pProcess->ppPrevProcess;
		}
		*pProcess->ppPrevProcess = pNextProcess;
		pProcess->ppPrevProcess = &pProcess->pNextProcess;
		pProcess->pNextProcess = NULL;
		pProcess->ttTimeToProcess = 0;
//		ASSERTE(CheckList());
	};
	bool CheckList() const {
		const CProcessType* pProcess = m_pFirstProcessScheduled;
		if (pProcess) {
			ASSERTE(pProcess->ppPrevProcess == &m_pFirstProcessScheduled);
			do {
				CProcessType* pNextProcess = pProcess->pNextProcess;
				if (pNextProcess) {
					ASSERTE(pNextProcess->ppPrevProcess == &pProcess->pNextProcess);
				} else {
					ASSERTE(m_ppLastProcessScheduled == &pProcess->pNextProcess);
				}
				pProcess = pProcess->pNextProcess;
			} while (pProcess != NULL);
		}
		return true;
	};

	CProcessType*        m_pFirstProcessScheduled;
	CProcessType**       m_ppLastProcessScheduled;
	bool                 m_bYieldRequested;
};

#endif // PROCESS_H_INCLUDED
