#ifndef TICK_PROCESS_H_INCLUDED
#define TICK_PROCESS_H_INCLUDED

#include "process.h"
#include "tick_timesource.h"

//#define PROCESS_DEBUG_INFO(...) DEBUG_INFO(__VA_ARGS__)
#define PROCESS_DEBUG_INFO(...) {}

typedef TProcess<CTickTimeSource> CTickProcess;
typedef TProcessScheduller<CTickTimeSource> CTickProcessScheduler;

extern CTickProcessScheduler g_TickProcessScheduller;


#endif // TICK_PROCESS_H_INCLUDED
