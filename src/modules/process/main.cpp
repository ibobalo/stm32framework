#include "stdafx.h"
#include "tick_process.h"
#include "hw.h"

// globals

CTickProcessScheduler g_TickProcessScheduller;

extern "C" {

extern void modules_init(void); // modules/modules_init.cpp
extern void app_init(void); // app/xxx/app_init.cpp

int main(void)
{
	g_TickProcessScheduller.Init();

	CHW_Brd::Init();

	modules_init();
	app_init();

	CHW_MCU::EnableIWDG();
#ifdef USE_WWDG
	CHW_MCU::EnableWWDG();
#endif

	while (1) {
		//DEBUG_INFO("Main loop");
		if (!g_TickProcessScheduller.ProcessNextProcess()) {
			CHW_MCU::Idle();
		}
		CHW_MCU::KickIWDG();
#ifdef USE_WWDG
		CHW_MCU::KickWWDG();
#endif
	}

	return 0;
}

} // extern "C"
