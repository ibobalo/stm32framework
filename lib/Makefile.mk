MCU_FAMILY=$(strip \
	$(if $(filter STM32F0%,$(MPU)),0xx) \
	$(if $(filter STM32F4%,$(MPU)),4xx) \
)

LIBCMSIS_PATH=lib/CMSIS
LIBSTM32FAMILY_PATH=lib/CMSIS/Device/ST/STM32F$(MCU_FAMILY)
LIBSTDPERIPH_PATH=lib/STM32F$(MCU_FAMILY)_StdPeriph_Driver
LIBUSB_DEVICE_PATH=lib/STM32_USB_Device_Library
LIBUSB_OTG_PATH=lib/STM32_USB_OTG_Driver
LIBUSB_HOST_PATH=lib/STM32_USB_HOST_Library

INCLUDE_PATH += \
	$(LIBCMSIS_PATH)/Include \
	$(LIBSTM32FAMILY_PATH)/Include \
	$(LIBSTDPERIPH_PATH)/inc \
	$(LIBSTDPERIPH_PATH)/src

MCU_BASENAME=stm32f$(MCU_FAMILY)

SRC += $(addprefix $(LIBSTM32FAMILY_PATH)/, \
	Source/Templates/system_stm32f$(MCU_FAMILY).c \
)

SRC += $(addprefix $(LIBSTDPERIPH_PATH)/src/, \
	$(MCU_BASENAME)_syscfg.c \
	$(MCU_BASENAME)_rcc.c \
	$(MCU_BASENAME)_iwdg.c \
	$(MCU_BASENAME)_wwdg.c \
	$(MCU_BASENAME)_gpio.c \
	$(MCU_BASENAME)_exti.c \
	$(MCU_BASENAME)_tim.c \
	$(MCU_BASENAME)_adc.c \
	$(MCU_BASENAME)_i2c.c \
	$(MCU_BASENAME)_spi.c \
	$(MCU_BASENAME)_dma.c \
	$(MCU_BASENAME)_usart.c \
	$(MCU_BASENAME)_can.c \
	$(MCU_BASENAME)_flash.c \
	$(MCU_BASENAME)_dbgmcu.c \
	$(if $(filter 0xx,$(MCU_FAMILY)), \
		$(MCU_BASENAME)_misc.c \
	) \
	$(if $(filter 4xx,$(MCU_FAMILY)), \
		misc.c \
	) \
)
USE_STDPERIPH_DRIVER += YES

USE_USB += $(if $(filter YES,$(USE_USB_OTG)), \
	YES \
)
SRC += $(if $(filter YES,$(USE_USB_OTG)), $(addprefix $(LIBUSB_OTG_PATH)/src/, \
	usb_core.c \
	usb_dcd_int.c \
	usb_dcd.c \
))
INCLUDE_PATH += $(if $(filter YES,$(USE_USB_OTG)), \
	$(LIBUSB_OTG_PATH)/inc \
)

USE_USB += $(if $(filter YES,$(USE_USB_HOST)), \
	YES \
)
INCLUDE_PATH += $(if $(filter YES,$(USE_USB_HOST)), \
	$(LIBUSB_HOST_PATH)/Core/inc \
)

SRC += $(if $(filter YES,$(USE_USB)), $(addprefix $(LIBUSB_DEVICE_PATH)/Core/src/, \
	usbd_core.c \
	usbd_ioreq.c \
	usbd_req.c \
))
SRC += $(if $(filter YES,$(USE_USB)), $(addprefix $(LIBUSB_DEVICE_PATH)/Class/cdc/src/, \
	usbd_cdc_core.c \
))
INCLUDE_PATH += $(if $(filter YES,$(USE_USB)), \
	$(LIBUSB_DEVICE_PATH)/Core/inc \
	$(LIBUSB_DEVICE_PATH)/Class/cdc/inc \
)
