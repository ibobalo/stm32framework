help: helpprereq helpusage
helpprereq:
	@echo ==== prepreq ==========
	@echo download stlink utility from
	@echo    http://www.st.com/web/en/catalog/tools/PF258168
	@echo    http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/utility/stsw-link004.zip
	@echo download st-util
	@echo    http://www.emb4fun.de/archive/stlink/index.html
	@echo    http://www.emb4fun.de/archive/stlink/download/stlink-20130324-win.zip
	@echo download gcc-arm
	@echo    https://launchpad.net/gcc-arm-embedded
	@echo download and install python
	@echo    https://www.python.org/downloads/
	@echo install python modules: pyserial minimalmodbus
	@echo    pip install pyserial minimalmodbus
	@echo linux compiler:
	@echo    sudo apt-get install gcc-arm-none-eabi binutils-arm-none-eabi libnewlib-arm-none-eabi
	@echo linux stlink:
	@echo    sudo apt-get install cmake libusb-1.0 libusb-1.0.0-dev
	@echo    git clone https://github.com/texane/stlink.git
	@echo    cd stlink && make release && cd build/Release && sudo make install
helpusage:
	@echo ==== usage ==========
	@echo make release
	@echo make flash_dimmer_debug
	@echo make APP=dimmer flash_release
	@echo make dfu
	@echo make dfu_release
	@echo make APP=dimmer gdb BRK=main DSPL=n
	@echo make flash_dimmer_release reset term

OPTIMIZATION?=$(if $(filter YES,$(DEBUG)),$(if $(filter $(FLASH_SIZE),8 16 32),SIZE,DEBUG),LTO)
#TOOLS_VERSION=-5.4.16q3

ifeq ($(OS),Windows_NT)
TOOLS_PATH=C:/Utils/GNU_ARM$(TOOLS_VERSION)/bin/
TMP_PATH=$(TEMP)
STUTIL_PATH=C:/Utils/stlink/bin/
STLINK_CLI="C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility\ST-LINK_CLI.exe" -c SWD
STLINK_FLASH=  $(STLINK_CLI) -V -TVolt -OB IWDG_SW=1 -P
STLINK_VERIFY= $(STLINK_CLI) -TVolt -CmpFile
STLINK_PROTECT=$(STLINK_CLI) -OB RDP=1
STLINK_DISABLEBOOT0=$(STLINK_CLI) -OB nBoot0_Sw_Cfg=0
STLINK_RESET=  $(STLINK_CLI) -V -TVolt -Rst
STLINK_CHECK=  $(STLINK_CLI) -rOB | find "RDP" | find "Level 1" >NUL && echo YES
SIGROK_PATH=C:\"Program Files (x86)"\sigrok
DFU="$(ProgramFiles)\STMicroelectronics\Software\DfuSe v3.0.4\Bin\DfuSeCommand.exe"  -c -d --fn 
IF_STUTIL_STARTED=(@TASKLIST | Find "st-util.exe")
DO_STUTIL_START=(start "ST-LINK util" /MIN $(STUTIL_PATH)st-util -p 4242 --no-reset > st-util.log && timeout /t 3 /nobreak > NUL & type st-util.log)
DO_STUTIL_STOP=(taskkill /F /IM st-util.exe 2>NUL & $(RM) st-util.log 2>NUL)
NULLERROR=$1 2>NUL
NULLOUTPUT=$1 >NUL 2>NUL
FIX=$(subst /,\,$1)
MKDIR=if not exist $(call FIX, $1) mkdir $(call FIX, $1)
RM=del /Q
RMDIR=rmdir /Q /S
#CP=robocopy $(call FIX, $(dir $1) $(dir $2)) $(notdir $1) >nul & IF %ERRORLEVEL% LSS 8 SET ERRORLEVEL = 0
CP=copy $(call FIX, $1 $2) >nul
MV=cmd /C move /Y $(call FIX, $1) $(call FIX, $2) >nul
ECHO=@echo $(subst >,^>,$(subst <,^<,$(subst |,^|,$(subst &,^&,$1))))
ECHO_n=echo|set/p z=$(subst >,^>,$(subst <,^<,$(subst |,^|,$(subst &,^&,$1))))
CAT=$(strip $(shell type $(call FIX,$1)))
TRUE=(VER>NUL)
else
#TOOLS_PATH=/home/ubuntu/arm-2013.05/bin/
TMP_PATH=/var/tmp
#STUTIL_PATH=~/stlink/
SIGROK_PATH=~/sigrok
DFU=dfu-util -a 0 --dfuse-address $(FLASH_START) -D
IF_STUTIL_STARTED=(ps | grep "st-util")
DO_STUTIL_START=($(STUTIL_PATH)st-util -p 4242 -v4 --no-reset >st-util.log &)
DO_STUTIL_STOP=(ps -lA | grep st-util) && (killall st-util; sleep 2) ; (ps -lA | grep st-util) && (killall -9 st-util; sleep 2); $(RM) st-util.log 2>/dev/null; true
NULLERROR=$1 2>/dev/null
NULLOUTPUT=$1 >/dev/null 2>/dev/null
FIX=$1
MKDIR=mkdir -p $1
RM=rm -f
RMDIR=rm -Rf
CP=cp -p $1 $2
MV=mv $1 $2
ECHO=echo $(subst >,\>,$(subst <,\<,$(subst |,\|,$(subst &,\&,$(subst #,\#,$1)))))
ECHO_n=echo -n $(subst >,\>,$(subst <,\<,$(subst |,\|,$(subst &,\&,$(subst #,\#,$1)))))
CAT=$(strip $(shell cat $(call FIX,$1)))
TRUE=true
endif
MAKE_CMD=$(MAKE) --no-print-directory --silent
SUBMKS=$(patsubst %/Makefile.mk,%,$(wildcard $(addsuffix /*/Makefile.mk,$1)))
CHECKPYTHON = $(if $(filter YES,$(shell python -c "import sys; sys.stdout.write('YES')")),,$(error python required))
COMPARE_FILES = python -c "import sys, os.path, filecmp; f1 = sys.argv[1]; f2 = sys.argv[2]; exit(os.path.isfile(f1) and os.path.isfile(f2) and filecmp.cmp(f1,f2, False))" $(call FIX,$1) $(call FIX,$2)
PY_PRINT=$(strip $(shell python -c "import sys,binascii; sys.stdout.write($(subst ",\",$1))"))
CALCULATE=$(call PY_PRINT,('{:$(strip $2)}'.format($1)))
STFLASH=$(call FIX,$(STUTIL_PATH)st-flash) write
STDEVID=$(call FIX,$(call NULLERROR, $(STUTIL_PATH)st-info --chipid))
STFLASHSIZE=$(call FIX,$(call NULLERROR, $(STUTIL_PATH)st-info --flash))
STPROTECTED=$(call FIX,$(call NULLOUTPUT, $(STUTIL_PATH)st-flash read $(TMP_PATH)/mpuob.bin  $(OPTIONBYTE_ADDR) 1) && python tools/printhex.py -n 1 $(TMP_PATH)/mpuob.bin)
STSERIAL=$(if $1, $(shell $(call FIX,$(call NULLOUTPUT, $(STUTIL_PATH)st-flash read $(TMP_PATH)/mpuid.bin $1 12) && python tools/printhex.py -n 12 $(TMP_PATH)/mpuid.bin)))
SIGROK_CLI=$(call FIX,$(SIGROK_PATH)sigrok-cli/sigrok-cli)
SIGROK_PULSEVIEW=$(call FIX,$(SIGROK_PATH)/PulseView/pulseview)
SIGROK_FILE=$(call FIX, $(TMP_PATH)/sigrok_temp)
START=$(call ECHO_n,-- $(notdir $@) --)
SPACE :=
SPACE +=
define NEWLINE

	
endef

TARGET_INFO += \
	APP=$(APP) \
	BOARD=$(BOARD) \
	MPU=$(MPU) \
	FLASH_SIZE=$(FLASH_SIZE) \
	RAM_SIZE=$(RAM_SIZE) \
	PINS=$(MPU_PINCODE):$(MPU_PINS_COUNT) \
	VER=$(GIT_TIMESTAMP) \

STLINK_INFO += \
	STLINK_DEVID=$(STLINK_DEVID) \
	STLINK_FLASHSIZE=$(STLINK_FLASHSIZE) \
	STLINK_PROTECTED=$(STLINK_PROTECTED) \
	STLINK_UID=$(STLINK_UID)

.DEFAULT_GOAL=debug
BASE_PATH?=$(if $(filter-out $(abspath $(CURDIR))/%,$(abspath $(SRC))),..,.)
OUT_PATH?=$(TMP_PATH)/$(APP)-$(BOARD)-$(MPU)/$(if $(filter YES,$(DEBUG)),debug,release)
TARGET?=$(OUT_PATH)/$(APP)-$(BOARD)-$(MPU)

include gmsl
include src/Makefile.mk
include lib_conf/Makefile.mk
include lib/Makefile.mk

STDEVID_ONCE=$(strip $(shell $(STDEVID)))
STLINK_FLASHSIZE_ONCE=$(strip $(if $(STLINK_DEVID), $(shell $(STFLASHSIZE))))
STLINK_PROTECTED_ONCE=$(strip $(if $(STLINK_DEVID), $(shell $(STLINK_CHECK))))
STLINK_UID_ONCE=$(strip $(if $(STLINK_DEVID), $(if $(filter YES, $(STLINK_PROTECTED)),,$(call STSERIAL, $(call UID_ADDR_FOR_DEVID,$(STLINK_DEVID))))))
STLINK_DEVID=$(call memoize,STDEVID_ONCE)
STLINK_FLASHSIZE=$(call memoize,STLINK_FLASHSIZE_ONCE)
STLINK_PROTECTED=$(call memoize,STLINK_PROTECTED_ONCE)
STLINK_UID=$(call memoize,STLINK_UID_ONCE)

.PHONY: stlinkinfo
stlinkinfo:
	@$(TRUE) $(foreach I, $(STLINK_INFO), && $(call ECHO,$(strip $(I)))) 

ifeq ($(DEBUG),)
.PHONY: %_gdb gdb_% %_gdbsession gdbsession_%
%_gdb gdb_% %_gdbsession gdbsession_%:
	$(call ECHO,##### $@ = debug $@ #####)
	@$(MAKE_CMD) DEBUG=YES $@
.PHONY: %_debug debug_%
%_debug debug_%:
	$(call ECHO,##### $@ = debug $* #####)
	@$(MAKE_CMD) DEBUG=YES $*
.PHONY: %_release release_%
%_release release_%:
	$(call ECHO,##### $@ = release $* #####)
	@$(MAKE_CMD) DEBUG=NO $*
endif
ifeq ($(APP),)
.PHONY: $(APPS_ALLOWED)
$(APPS_ALLOWED):
	$(call ECHO,##### $@ = application $@ #####)
	@$(MAKE_CMD) APP=$@ all 
.PHONY: $(addprefix %_,$(APPS_ALLOWED))
$(addprefix %_,$(APPS_ALLOWED)):
	$(call ECHO,##### $@ = $* for app $(subst $*_,,$@) #####)
	@$(MAKE_CMD) APP=$(subst $*_,,$@) $*
.PHONY: $(addsuffix _%,$(APPS_ALLOWED))
$(addsuffix _%,$(APPS_ALLOWED)):
	$(call ECHO,##### $@ = $* for app $(subst _$*,,$@) #####)
	@$(MAKE_CMD) APP=$(subst _$*,,$@) $*
endif
.PHONY: stutil_start
stutil_start:
	@$(IF_STUTIL_STARTED) || $(DO_STUTIL_START) && $(IF_STUTIL_STARTED)
.PHONY: stutil_stop
stutil_stop:
	-@$(IF_STUTIL_STARTED) && $(DO_STUTIL_STOP)
.PHONY: reset
ifneq ($(STLINK_RESET),)
reset: | stutil_stop
	$(STLINK_RESET)
else
reset:
	@$(IF_STUTIL_STARTED) || $(DO_STUTIL_START) && $(IF_STUTIL_STARTED)
	$(GDB) $(GDBFLAGS_RESET)
	-$(DO_STUTIL_STOP)
endif
.PHONY: term
term:
	python -m serial.tools.miniterm --ask $(TERM_OPTS)

.PHONY: sigrok
sigrok:
	$(SIGROK_CLI) --driver ols:conn=COM10 \
		--config samplerate=5m:rle=on:captureratio=0 \
		--samples 28672 \
		--output-format srzip \
		--output-file $(SIGROK_FILE)
	$(SIGROK_PULSEVIEW) $(SIGROK_FILE)
.PHONY: sigrok_i2c
sigrok_i2c:
	$(SIGROK_CLI) --driver ols:conn=COM10 \
		--config samplerate=2m:rle=on:captureratio=0 \
		--samples 28672 \
		--channels 0=SCL,1=SDA \
		--triggers SCL=1 \
		--output-format srzip \
		--output-file $(SIGROK_FILE).srzip
	$(SIGROK_CLI) --input-file $(SIGROK_FILE).srzip \
		--protocol-decoders i2c:address_format=unshifted \
		--protocol-decoder-annotations i2c=start:repeat-start:address-read:address-write:data-read:data-write:ack:nack:stop | more
	$(SIGROK_PULSEVIEW) $(SIGROK_FILE).srzip


ifeq ($(APP),)
#$(warning APP empty for $(MAKECMDGOALS))

flash gdb gdbsession:
	$(call ECHO,##### $@ for first app from [$(APPS_SUGGESTED)])
	@$(MAKE_CMD) APP=$(firstword $(APPS_SUGGESTED)) $@

%:
	$(call ECHO,##### $@ for all apps [$(APPS_SUGGESTED)])
	@$(foreach app,$(APPS_SUGGESTED), $(MAKE_CMD) APP=$(app) $@ && ) $(call ECHO,all apps done) 

else ifeq ($(DEBUG),)
#$(warning DEBUG empty for $(MAKECMDGOALS))

.PHONY: debug release gdb gdbsession flash
debug:
	$(call ECHO,##### $@ = all debug #####)
	@$(MAKE_CMD) DEBUG=YES all
release:
	$(call ECHO,##### $@ = all release #####)
	@$(MAKE_CMD) DEBUG=NO all
gdb gdbsession:
	$(call ECHO,##### $@ = gdb debug #####)
	@$(MAKE_CMD) DEBUG=YES $@
flash:
	$(call ECHO,##### $@ = flash release #####)
	@$(MAKE_CMD) DEBUG=NO flash
%:
	$(call ECHO,##### $@ = all modes from DEBUG=[YES NO] for app $(APP) #####)
	@$(foreach mode,YES NO, @$(MAKE_CMD) DEBUG=$(mode) $@ && ) $(call ECHO,all debug modes done) 

else ifeq ($(BOARD),)
ifeq ($(BOARDS),)
$(error BOARD not defined. Hint: add BOARD=.. or BOARDS=.. or BOARD_PATH=.. definition to app Makefile.mk)
endif

flash gdb:
	$(call ECHO,##### $@ for app $(APP) for first board from [$(BOARDS)] #####)
	@$(MAKE_CMD) BOARD=$(firstword $(BOARDS)) $@
%:
	$(call ECHO,##### $@ for app $(APP) for all boards [$(BOARDS)] #####)
	@$(foreach board,$(BOARDS), $(MAKE_CMD) BOARD=$(board) $@ && ) $(call ECHO,all boards done) 

else ifeq ($(MPU),)
#$(warning MPU empty for $(MAKECMDGOALS))

flash gdb:
	$(call ECHO,##### $@ for app $(APP) for board $(BOARD) for first MPU from [$(MPUS)] #####)
	@$(MAKE_CMD) MPU=$(firstword $(MPUS)) $@
%:
	$(call ECHO,##### $@ for app $(APP) for board $(BOARD) for all MPUs [$(MPUS)] #####)
	@$(foreach mpu,$(MPUS), $(MAKE_CMD) MPU=$(mpu) $@ && ) $(call ECHO,all MPUs done) 


else

.PHONY: clean
clean: | stutil_stop
	-$(call NULLOUTPUT, $(RMDIR) $(call FIX, $(OUT_PATH)))
	-$(call NULLOUTPUT, $(RM) $(call FIX, $(BASE_PATH)/git-timestamp.h))
#	-@$(RM) $(call FIX, $(OBJS) $(DEPS) $(PRES) $(ASMS) $(LSTS) $(TARGET).map $(TARGET).elf $(TARGET).dfu.elf $(TARGET).dfu.bin $(TARGET).update.bin $(TARGET).update.hex $(TARGET).update.dfu)

#$(warning DEBUG=$(DEBUG))
#$(warning APP=$(APP))
#$(warning BOARD=$(BOARD))
#$(warning MPU=$(MPU))
#$(warning UID_ADDR=$(UID_ADDR))
#$(warning OUT_PATH=$(OUT_PATH))
#$(warning SERIAL=$(STSERIAL))
#$(warning UID=$(STLINK_UID))
#$(warning PROTECTED=$(STPROTECTED))
#$(warning PROTECTED=$(STLINK_PROTECTED))
#$(warning OUT_PATH=$(OUT_PATH))

TOOLS_PREFIX=$(call FIX,$(TOOLS_PATH)arm-none-eabi-)
USE_PRECOMPILED_HEADERS?=YES

FLAG_CORTEX=$(strip \
	$(if $(filter STM32F0%,$(MPU)),cortex-m0) \
	$(if $(filter STM32F4%,$(MPU)),cortex-m4) \
)

COMMONFLAGS+=-mcpu=$(FLAG_CORTEX) -mthumb -ffreestanding -nostartfiles -fno-math-errno
COMMONFLAGS+=$(if $(filter LTO,$(OPTIMIZATION)), -flto, -nodefaultlibs $(if $(filter YES,$(USE_STDLIB)),,-nostdlib) )
#COMMONFLAGS+= -fshort-wchar
COMMONFLAGS+= $(if $(filter STM32F4%,$(MPU)),-mfloat-abi=hard -mfpu=fpv4-sp-d16)
#COMMONFLAGS+= -nostdinc
OPTFLAGS=$(if $(filter DEBUG,$(OPTIMIZATION)), -Og, $(if $(filter SIZE,$(OPTIMIZATION)), -Os, $(if $(filter YES,$(DEBUG)), -Og, $(if $(filter $(FLASH_SIZE),8 16 32), -Os, -O2))))
PPFLAGS=$(addprefix -D,$(DEFINES)) $(addprefix -I,$(patsubst $(abspath $(BASE_PATH))%,$(BASE_PATH)%,$(abspath $(INCLUDE_PATH))))
CFLAGS+=-ffunction-sections -fdata-sections -Wall -Werror -c -fmessage-length=0 -fmax-errors=2 $(COMMONFLAGS)
#CFLAGS+=-Wextra
CFLAGS+=$(PPFLAGS)
CFLAGS+= $(if $(filter YES,$(DEBUG)),-g3 -gdwarf-2, -g3 -gdwarf-2) $(OPTFLAGS)
ASFLAGS+=-mcpu=$(FLAG_CORTEX) -gdwarf-2 -gdwarf-2
ASFLAGS+=$(INCLUDE_PATH)
CCFLAGS+=$(CFLAGS) -std=gnu11
CPPFLAGS+=$(CFLAGS) -fno-exceptions -fno-rtti -fno-threadsafe-statics -std=gnu++11
DEPFLAGS=-MMD -MP -MF $(basename $@).d.tmp -MT $(subst $(OUT_PATH),$$(OUT_PATH),$@)
LSTFLAGS=-Wa,-adhlns="$(basename $@).lst"
LDFLAGS+=-Wl,--gc-sections,-Map,$(basename $@).map -fwhole-program
LDFLAGS+= $(if $(filter YES,$(DEBUG)),-g3 -gdwarf-2, -g3 -gdwarf-2)  $(OPTFLAGS)
#LDFLAGS+=$(if $(filter YES,$(USE_MATHLIB)),-lm) $(if $(filter YES,$(USE_STDLIB)),-lc -lstdc++ -lgcc,)
LDFLAGS+=$(COMMONFLAGS)

CC=$(call FIX,   "$(TOOLS_PREFIX)gcc")
CPP=$(call FIX,  "$(TOOLS_PREFIX)g++")
LD=$(call FIX,   "$(TOOLS_PREFIX)g++")
PP=$(call FIX,   "$(TOOLS_PREFIX)cpp")
AS=$(call FIX,   "$(TOOLS_PREFIX)as")
SIZE=$(call FIX, "$(TOOLS_PREFIX)size")
OBJCOPY=$(call FIX,  "$(TOOLS_PREFIX)objcopy")
GDB=$(call FIX,  "$(TOOLS_PREFIX)gdb")
COMMIT_DEP=$(call MV,$(basename $@).d.tmp,$(basename $@).d)
DOLOG=$(call ECHO,$1) >> $(TARGET).log && $1

USE_ASSERTS ?= YES
FLASH_START ?= 0x08000000
FLASH_PROTECTION?=$(if $(filter YES,$(DEBUG)),,YES)
FLASH_DISABLEBOOT0?=NO

SRC_VFD = \
	vfd_pwm.cpp \
	proc_ramp.cpp

USES = $(sort $(foreach V, $(filter USE_%, $(.VARIABLES)), $(if $(filter YES,$($(V))),$(V))))

DEFINES += \
	$(USES) \
	$(call substr,$(MPU),1,9) \
	$(if $(filter YES,$(DEBUG)), DEBUG \
		$(if $(filter YES,$(USE_ASSERTS)), USE_FULL_ASSERT,) \
	,) \
	
ifneq ($(filter-out $(abspath $(BASE_PATH))/%,$(abspath $(LD_MAP))),)
$(error LD_MAP $(notdir $(LD_MAP)) is out of BASE_PATH )
endif
ifneq ($(filter-out $(abspath $(BASE_PATH))/%,$(abspath $(SRC))),)
INVALID_SRCS=$(notdir $(filter-out $(abspath $(BASE_PATH))/%,$(abspath $(SRC))))
$(error SRC $(firstword $(INVALID_SRCS))$(if $(word 2,$(INVALID_SRCS)), ... [total $(words $(INVALID_SRCS))]) is out of BASE_PATH)
endif

LDS  = $(addprefix $(OUT_PATH)/,$(addsuffix .ld,$(basename $(patsubst $(abspath $(BASE_PATH))/%,%,$(abspath $(LD_MAP))))))
OBJS = $(addprefix $(OUT_PATH)/,$(addsuffix .o,$(basename $(patsubst $(abspath $(BASE_PATH))/%,%,$(abspath $(SRC))))))
DEPS = $(OBJS:.o=.d)
PRES = $(OBJS:.o=.o.pre)
ASMS = $(OBJS:.o=.o.asm)
LSTS = $(OBJS:.o=.lst)

OUT_PATH_TREE=$(sort $(dir $(OBJS) $(DEPS) $(ASMS) $(LSTS) $(PRES)))
$(OBJS) $(DEPS) $(ASMS) $(LSTS) $(PRES): | $(OUT_PATH_TREE)
$(OUT_PATH_TREE):
	$(call MKDIR, $@)
FORCE:

.PHONY: assemblers preprocessed
assemblers: $(ASMS) $(LSTS)
preprocessed: $(PRES)

.PHONY: all
all: $(TARGET).bin $(TARGET).hex $(TARGET).s19 $(TARGET).dfu
all: $(LSTS) $(PRES)

$(TARGET).elf: $(OBJS) $(LDS)
.INTERMEDIATE: $(LDS)

$(OBJS) $(LDS) $(LSTS) $(PRES): $(TARGET).defs

$(OUT_PATH)/%.d: ;
.PRECIOUS: $(OUT_PATH)/%.d

# .S -> .o
$(OUT_PATH)/%.o: $(BASE_PATH)/%.S
	$(START)
	$(call DOLOG,$(AS) $(ASFLAGS) $< -o $@)
# .c -> .o .d .lst
$(OUT_PATH)/%.o $(OUT_PATH)/%.lst: $(BASE_PATH)/%.c $(OUT_PATH)/%.d
	$(START)
	$(call DOLOG,$(CC) $(CCFLAGS) $< -o $(basename $@).o $(DEPFLAGS) $(LSTFLAGS))
	$(call DOLOG,$(COMMIT_DEP))
# .cpp -> .o .d .lst
$(OUT_PATH)/%.o $(OUT_PATH)/%.lst: $(BASE_PATH)/%.cpp $(OUT_PATH)/%.d
	$(START)
	$(call DOLOG,$(CPP) $(CPPFLAGS) $< -o $(basename $@).o $(DEPFLAGS) $(LSTFLAGS))
	$(call DOLOG,$(COMMIT_DEP))

# precompiled headers
CCFLAGS+=-include stdafx.h #required anyway for lib_conf
ifeq ($(USE_PRECOMPILED_HEADERS),YES)
CPPFLAGS+=-include stdafx.hpp
STDAFX=src/stdafx.h src/stdafx.hpp
GCHS=$(addprefix $(OUT_PATH)/,$(addsuffix .gch, $(patsubst $(abspath $(BASE_PATH))/%,%,$(abspath $(STDAFX))))) 
$(OBJS): $(GCHS)
# .h -> .gch
$(OUT_PATH)/%.h.gch: $(BASE_PATH)/%.h $(OUT_PATH)/%.d
	$(START)
	$(call DOLOG,$(CC) $(CCFLAGS) $< -o $(basename $@).gch $(DEPFLAGS))
	$(call DOLOG,$(COMMIT_DEP))
# .hpp -> .gch
$(OUT_PATH)/%.hpp.gch: $(BASE_PATH)/%.hpp $(OUT_PATH)/%.d
	$(START)
	$(call DOLOG,$(CPP) $(CPPFLAGS) $< -o $(basename $@).gch $(DEPFLAGS))
	$(call DOLOG,$(COMMIT_DEP))
endif

# .c -> .o.pre
$(OUT_PATH)/%.o.pre: $(BASE_PATH)/%.c
	$(START)
	$(call DOLOG,$(CC) $(CCFLAGS) -E $< -o $@)
# .cpp -> .o.pre
$(OUT_PATH)/%.o.pre: $(BASE_PATH)/%.cpp
	$(START)
	$(call DOLOG,$(CPP) $(CPPFLAGS) -E $< -o $@)

# .c -> .o.asm
$(OUT_PATH)/%.o.asm: $(BASE_PATH)/%.c
	$(START)
	$(call DOLOG,$(CC) $(CCFLAGS) -S $< -o $@)
# .cpp -> .o.asm
$(OUT_PATH)/%.o.asm: $(BASE_PATH)/%.cpp
	$(START)
	$(call DOLOG,$(CPP) $(CPPFLAGS) -S $< -o $@)

# .lds -> .ld
.PRECIOUS: $(OUT_PATH)/%.ld
$(OUT_PATH)/%.ld: $(BASE_PATH)/%.lds
	$(START)
	$(call DOLOG,$(PP) $(PPFLAGS) -E -P $< -o $@)

$(OUT_PATH)/%.elf:
	$(START)
	$(call ECHO,-)
	$(call DOLOG,$(LD) $(filter-out %.ld,$^) $(addprefix -T, $(firstword  $(filter %.ld,$^))) $(LDFLAGS) -o $@)
	$(SIZE) -B $@

%.defs: %.info
	$(call ECHO,OPTIMIZATION: $(OPTIMIZATION))>$@.temp
	($(call ECHO,DEFINES:) $(foreach D, $(DEFINES), && $(call ECHO,  $(strip $(D)))))>>$@.temp
	($(call ECHO,INCLUDE_PATH:) $(foreach P, $(INCLUDE_PATH), && $(call ECHO,  $(strip $(P)))))>>$@.temp
	($(call ECHO,SRC:) $(foreach S, $(SRC), && $(call ECHO,  $(strip $(S)))))>>$@.temp
	($(call ECHO,MAKEFILE_LIST:) $(foreach M, $(filter-out %.d, $(MAKEFILE_LIST)), && $(call ECHO,  $(strip $(M)))))>>$@.temp
	-$(call COMPARE_FILES,$@.temp,$@) && $(call MV,$@.temp,$@)
%.info: FORCE
	($(TRUE) $(foreach I, $(TARGET_INFO), && $(call ECHO,$(strip $(I)))) $(foreach U, $(USES), && $(call ECHO,$(U)=$(strip $($(U))))))>$@
%.bin: %.elf
	$(START)
	$(call DOLOG,$(OBJCOPY) --strip-all -O binary $< $@)
%.hex: %.elf
	$(START)
	$(call DOLOG,$(OBJCOPY) -Oihex $< $@)
%.s19: %.elf
	$(START)
	$(call DOLOG,$(OBJCOPY) -Osrec $< $@)
%.dfu: $(TARGET).bin
	$(START)
	$(call DOLOG,python "tools/dfu.py" -D 0x483:0xDF11 -b $(FLASH_START):$< $@)
%.o: %.d

.PHONY: targetinfo
targetinfo: FORCE
	$(call DOLOG,$(TRUE) $(foreach I, $(TARGET_INFO), && $(call ECHO,$(strip $(I))))) 

ifneq ($(MAKECMDGOALS),clean)
GIT_TIMESTAMP:=$(shell python tools/git-timestamp.py $(BASE_PATH))
$(OBJS): | $(BASE_PATH)/git-timestamp.h
INCLUDE_PATH += $(BASE_PATH)
$(BASE_PATH)/git-timestamp.h: $(OUT_PATH)/git-timestamp.$(GIT_TIMESTAMP).h
	-$(call COMPARE_FILES,$<,$@) && $(call CP,$<,$@)
$(OUT_PATH)/git-timestamp.%.h:
	$(START)
	$(call DOLOG,$(foreach f, $(filter-out $@, $(wildcard $(OUT_PATH)/git-timestamp.*)),$(RM) $(call FIX,$f) &&) $(call ECHO,#define GIT_TIMESTAMP $(subst .,,$(suffix $(basename $@))))> $@)
endif

include $(wildcard $(DEPS))

GDB_BREAKPOINTS += \
	$(if $(filter 0,$(MPU_SERIE)), Reset_Handler;x/t(0x40021024);echo|LPW.WWDG.IWDG.SFT.POR.PIN.OBL...\n;p/x(*0x40021024|=(1<<24))) \
	$(if $(filter 4,$(MPU_SERIE)), Reset_Handler;x/t(0x40023874);echo|LPW.WWDG.IWDG.SFT.POR.PIN.BOR...\n;p/x(*0x40023874|=(1<<24))) \
	Default_Handler;echo|ICSR=;p/x(SCB->ICSR&SCB_ICSR_VECTACTIVE_Msk);echo|AcliveIrq(EIsrEntryId)\n;bt \
	$(BRK)

GDB_DISPLAYS = $(DSPL)
GDBFLAGS= \
	--ex="target extended-remote :4242"
GDBFLAGS_FLASH= \
	$(GDBFLAGS) \
	--ex="load $<" \
	--ex="disconnect" \
	--ex="q"
GDBFLAGS_RESET= \
	$(GDBFLAGS) \
	--ex=kill \
	--ex=dettach \
	--ex="disconnect" \
	--ex="q"
GDBFLAGS_DEBUG += \
	--ex="p /x (DBGMCU->IDCODE & 0x0FFF") \
	$(if $(UID_ADDR),--ex="p /x *(CHW_MCU::uniqid_t*)$(UID_ADDR)")
GDBCMD_BRK= \
	$(call ECHO,br $(firstword $1))>>$@ $(NEWLINE) \
	$(call ECHO,commands)>>$@ $(NEWLINE) \
		$(foreach c, $(wordlist 2,$(words $1),$1), $(call ECHO,$c)>>$@ $(NEWLINE)) \
	$(call ECHO,end)>>$@ $(NEWLINE)
GDBCMD_DISPL=$(call ECHO,display $1)>>$@ $(NEWLINE)

.PHONY: gdb gdbsession
gdb: flash gdbsession
.INTERMEDIATE: $(TARGET).gdb.script
$(TARGET).gdb.script:	
	$(START)
	$(call ECHO,source gdb_macros)>$@
	$(call ECHO,$(if $(wildcard $(APP_PATH)/gdb_macros),source,#no) $(APP_PATH)/gdb_macros)>>$@ 
	$(call ECHO,$(if $(wildcard $(BOARD_PATH)/gdb_macros),source,#no) $(BOARD_PATH)/gdb_macros)>>$@ 
	$(foreach b,$(GDB_BREAKPOINTS), $(call GDBCMD_BRK, $(subst ;, ,$b)))
	$(foreach d,$(GDB_DISPLAYS), $(call GDBCMD_DISPL, $(subst ;, ,$d)))
gdbsession: $(TARGET).elf $(TARGET).gdb.script | targetinfo
	@$(IF_STUTIL_STARTED) || $(DO_STUTIL_START) && $(IF_STUTIL_STARTED)
	$(GDB) $< $(GDBFLAGS) -x $(TARGET).gdb.script
	$(DO_STUTIL_STOP)

.PHONY: flash flashprotect flashdisableboot0
flash: stlinkinfo
flash:
	$(MAKE_CMD) $(TARGET).flashedver.$(STLINK_UID) $(if $(filter YES,$(FLASH_PROTECTION)),flashprotect) $(if $(filter YES,$(FLASH_DISABLEBOOT0)),flashdisableboot0)
%.flashedver.: FORCE

$(TARGET).flashedver.%: $(TARGET).hex
	$(START)
	-@$(IF_STUTIL_STARTED) && $(DO_STUTIL_STOP)
	$(if $(and $(STLINK_DEVID), $(filter $(STLINK_DEVID),$(MPU_DEVID))),,$(error ERROR! Connected chip $(STLINK_DEVID) not match $(MPU_DEVID)))
	$(if $(STLINK_FLASH), $(STLINK_FLASH) $< $(FLASH_START), $(if $(STFLASH), $(STFLASH) $< $(FLASH_START), ($(IF_STUTIL_STARTED) || $(DO_STUTIL_START)) && $(IF_STUTIL_STARTED) && $(GDB) $(GDBFLAGS_FLASH)))
	-$(DO_STUTIL_STOP)
	$(call ECHO,$(STLINK_DEVID) $(STLINK_UID)) > $@
flashprotect:
	$(START)
	$(STLINK_PROTECT)
flashdisableboot0:
	$(START)
	$(STLINK_DISABLEBOOT0)

.PHONY: dfu
dfu: $(TARGET).dfu
	$(START)
	$(DFU) $< 

endif
