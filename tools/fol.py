"""
	Firmware Over Lan
	pack, encrypt and upload firmware to STM32
	STM32 must use compatible UDP listener to receive
	firmware image into reserved area of flash memory
	and special bootloader to decrypd the image and flash
	firmware to main memory
"""
import sys, os, os.path
from time import sleep
import struct
from udpcomm import UdpProtocolTwisted, CommunicationAnimator
import socket

UDP_IP = "192.168.2.2"
UDP_PORT = 5022
UDP_TIMEOUT_MS = 100

QUERY_HEADER_FORMAT = "!HH"  # seq, query_type
QUERY_UPLOAD_UPDATE_FORMAT = "!LL"  # offset, size
QUERY_VERIFY_UPDATE_FORMAT = "!LL"  # offset, size
QUERY_DOWNLOAD_UPDATE_FORMAT = "!L" # offset
(
	QUERY_TYPE_GET_INFO, QUERY_TYPE_UPLOAD_UPDATE, QUERY_TYPE_VERIFY_UPDATE, QUERY_TYPE_DOWNLOAD_UPDATE, QUERY_TYPE_RESET
) = range(1, 6) 

RESPONCE_HEADER_FORMAT="!HH" # seq, query_type
RESPONCE_GET_INFO_FORMAT="!HHH6s" # McuId, Rev, FlashSizeKB, UniqID
RESPONCE_UPLOAD_UPDATE_FORMAT="!L" # commited_size
RESPONCE_VERIFY_UPDATE_FORMAT="!L" # commited_size
RESPONCE_DOWNLOAD_UPDATE_FORMAT="!LL" # offset, size
(
	RESPONCE_TYPE_GET_INFO, RESPONCE_TYPE_UPLOAD_UPDATE, RESPONCE_TYPE_VERIFY_UPDATE, RESPONCE_TYPE_DOWNLOAD_UPDATE
) = range(1, 5) 

Stm32McuIDs = {
		0x423: "STM32F401xB/C",
		0x433: "STM32F401xD/E",
		0x413: "STM32F405xx/07xx and STM32F415xx/17xx",
		0x419: "STM32F42xxx and STM32F43xxx",
}
Stm32Rev = {
		0x1000: "A",
		0x1001: "Z",
		0x1007: "1",
}

class FolDatagramProtocolBase(object):
	QUERY_HEADER_FORMAT = "!H"  # seq, query_type

class FolDatagramProtocolGetInfo(object):
	QUERY_HEADER_FORMAT = "!H"  # seq, query_type
	def __init__(self, context):
		self.context = context
	def ComposeUdpPacket(self):
		return ComposeUdpPacketGetInfo()
	def ComposeUdpPacketGetInfo(self):
		return struct.format(FolDatagramProtocolBase.QUERY_HEADER_FORMAT, QUERY_TYPE_GET_INFO)
	def ParseUdpPacket(self, data, looptime):
		context = self.teledata
		context.connected = True;
		(
			context.cpuUsage,
			context.minClksIdle, context.maxClksIdle, context.avgClksIdle,
			context.minClksUsed, context.maxClksUsed, context.avgClksUsed,
			context.ADCSteering, context.ADCcurrentTotal, context.ADCcurrentRR, context.ADCcurrentLR, context.ADCcurrentRF, context.ADCcurrentLF,
			context.accX, context.accY, context.accZ,
			context.gyroX, context.gyroY, context.gyroZ,
			context.magX, context.magY, context.magZ,
			context.temp,
			context.currentTotal, context.currentRR, context.currentLR, context.currentRF, context.currentLF,
			context.encSteer, context.posSteer, context.velSteer, context.velRR, context.velLR, context.velRF, context.velLF,
			context.commSteer, context.commRR, context.commLR, context.commRF, context.commLF,
		) = struct.unpack(self.RESPONCE_FORMAT, data)
		context.cpuUsage /= 10.0
		context.currentTotal /= float(0x7FFF)
		context.currentRR /= float(0x7FFF)
		context.currentLR /= float(0x7FFF)
		context.currentRF /= float(0x7FFF)
		context.currentLF /= float(0x7FFF)
		context.posSteer /= float(0x7FFF)
		context.velSteer /= float(0x7FFF)
		context.velRR /= float(0x7FFF)
		context.velLR /= float(0x7FFF)
		context.velRF /= float(0x7FFF)
		context.velLF /= float(0x7FFF)
		self.teledata = context
		return True
	def OnConnectionTimeout(self):
# 		print "on timeout"
		self.teledata.connected = False;
	def OnConnectionError(self):
# 		print "on err"
		self.teledata.connected = False;

class Stm32FirmwareUploader:
	def __init__(self, args):
		self.args = args
		self.udp_host = self.args.host
		self.udp_port = self.args.port
		self.timeout = self.args.timeout
		self.socket = None
		self.ok = 0
		self.animation = 0
		self.seq = 0
		self.commited_size = 0

	def Connect(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#		self.socket.bind(("", self.udp_port + 1))
		self.socket.settimeout(self.timeout)

	def Send(self, msg):
		if msg:
			if not self.socket:
				self.Connect()
			bytes_send = self.socket.sendto(msg, (self.udp_host, self.udp_port))
	
	def Recv(self):
		while True: 
			(udp_data, address) = self.socket.recvfrom(2048)
			if self.ParseUdpPacket(udp_data):
				self.ok = 10
				print "|/-\\"[self.animation%4], "		 \r",
				self.animation = self.animation + 1
				break

	def SendRecv(self, pkt, norecv=False):
		try:
			self.Send(pkt)
			if not norecv:
				self.Recv()
		except socket.timeout:
			sleep(0.5)
			pass

	def Close(self):
		self.socket.close()
		self.socket = None

	def GetInfo(self):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt = struct.pack(QUERY_HEADER_FORMAT, self.seq, QUERY_TYPE_GET_INFO);
		self.SendRecv(pkt)

	def Upload(self, data):
		self.commited_size = 0
		chunk_size = 1024
		while True:
			self.seq = (self.seq + 1) & 0xFFFF
			chunk_size = min( len(data) - self.commited_size, 1024 )
			if chunk_size > 0:
				pkt = (struct.pack(QUERY_HEADER_FORMAT, self.seq, QUERY_TYPE_UPLOAD_UPDATE)
					+ struct.pack(QUERY_UPLOAD_UPDATE_FORMAT, self.commited_size, chunk_size)
					+ data[self.commited_size : self.commited_size + chunk_size])
				print "\r  uploading %8d-%8d of %8d"%(self.commited_size, self.commited_size + chunk_size, len(data)),
				self.SendRecv(pkt)
			else:
				print "\nDone.\n"
				break

	def Verify(self, data):
		self.commited_size = 0
		chunk_size = 1024
		while True:
			self.seq = (self.seq + 1) & 0xFFFF
			chunk_size = min( len(data) - self.commited_size, 1024 )
			if chunk_size > 0:
				pkt = (struct.pack(QUERY_HEADER_FORMAT, self.seq, QUERY_TYPE_VERIFY_UPDATE)
					+ struct.pack(QUERY_VERIFY_UPDATE_FORMAT, self.commited_size, chunk_size)
					+ data[self.commited_size : self.commited_size + chunk_size])
				print "\r  verifying %8d-%8d of %8d"%(self.commited_size, self.commited_size + chunk_size, len(data)),
				self.SendRecv(pkt)
			else:
				print "\nDone.\n"
				break

	def Reset(self):
		print "Reset"
		self.seq = (self.seq + 1) & 0xFFFF
		pkt = struct.pack(QUERY_HEADER_FORMAT, self.seq, QUERY_TYPE_RESET);
		self.SendRecv(pkt, True)

	def ParseUdpPacket(self, udp_data):
		(seq, responce_type) = struct.unpack_from(RESPONCE_HEADER_FORMAT, udp_data)
		header_size = struct.calcsize(RESPONCE_HEADER_FORMAT) 
		if seq == self.seq:
			if responce_type == RESPONCE_TYPE_GET_INFO:
				(McuId, Rev, FlashSizeKB, UniqID) = struct.unpack_from(RESPONCE_GET_INFO_FORMAT, udp_data, header_size)
				print "McuId : %04X (%s)"%(McuId, Stm32McuIDs.get(McuId, "?")) 
				print "Rev   : %04x (%s)"%(Rev, Stm32Rev.get(Rev))
				print "Flash : %dKB"%FlashSizeKB
				print "UniqID: %s"%UniqID.encode('hex_codec')
			elif responce_type == RESPONCE_TYPE_UPLOAD_UPDATE:
				(self.commited_size,) = struct.unpack_from(RESPONCE_UPLOAD_UPDATE_FORMAT, udp_data, header_size)
			elif responce_type == RESPONCE_TYPE_VERIFY_UPDATE:
				(self.commited_size,) = struct.unpack_from(RESPONCE_VERIFY_UPDATE_FORMAT, udp_data, header_size)
			else:
				raise StandardError("invalid respoce type")
			return True
		print "[seq]         \r",
		return False

def parse_args():			
	from argparse import ArgumentParser
	parser = ArgumentParser(description='STM32 UDP Firmware uploader.')
	parser.add_argument("-a", "--host", "--address", help="target host ip name or address",
					default = UDP_IP)
	parser.add_argument("-p", "--port", help="target ip port (UDP)",
					type = int, default = UDP_PORT)
	parser.add_argument("-t", "--timeout", help="ip communication timeout (ms)",
					type = int, default = UDP_TIMEOUT_MS)
	parser.add_argument("-d", "--download", "--dump", help="filename to store downloaded firmware update") 
	parser.add_argument("-r", "--reset", help="reset target", action="store_true")
	parser.add_argument("-v", "--verify", help="verify uploaded firmware update", action="store_true")
	parser.add_argument("update_file", help="filename of firmware update file to upload",
					default = "", nargs='?') 
	args = parser.parse_args()
	args.timeout /= 1000.0
	return args

def main():
# 	def_update_bin = os.path.expandvars("""$TEMP/ROVER/debug/ROVER-STM32.update.bin""")
	args = parse_args()
	fu = Stm32FirmwareUploader(args)
	fu.GetInfo()
	if args.download:
		data = fu.Download()
		file(args.download, 'wb').write(data)
	if args.update_file:
		if args.verify:
			fu.Verify(file(args.update_file, 'rb').read())
		else:
			fu.Upload(file(args.update_file, 'rb').read())
	if (args.reset):
		fu.Reset()

if __name__ == '__main__':
	main();
