import struct
from udpcomm import UdpProtocolTwisted, CommunicationAnimator
from twisted.internet.task import LoopingCall
from twisted.internet import reactor

UDP_IP = "192.168.1.1"
UDP_PORT = 5007
PERIOD_MS = 1000
TIMEOUT_MS = 5000

def partition(string, size):
	if len(string) % size:
		parts = len(string) / size + 1
	else:
		parts = len(string) / size
	return [string[index*size:index*size+size] for index in range(parts)]

def hex_dump(string, start_address, address_width = 8, bytes_per_line = 16):
	parts = partition(string, bytes_per_line)
	rule = printable()
	for index in range(len(parts)):
		print '%0*X: %s |%s'%(
			  address_width, start_address + index * bytes_per_line,
			  pad_right(convert_hex(parts[index]), 47), \
			  convert_print(parts[index], rule))

def printable():
	return ''.join([chr(byte) for byte in range(256) \
					if len(repr(chr(byte))) == 3 or byte == ord('\\')])

def pad_right(string, length, padding=' '):
		return string + padding[0] * (length - len(string))

def convert_hex(string):
	return ' '.join([hex(ord(character))[2:].upper().zfill(2) \
					 for character in string])

def convert_print(string, rule):
	return ''.join([character in rule and character \
					or '.' for character in string])

class TelemetryData:
	def __init__(self):
		self.connected = False
		self.cpuUsage = 0
		self.minClksIdle, self.maxClksIdle, self.avgClksIdle = 0,0,0
		self.minClksUsed, self.maxClksUsed, self.avgClksUsed = 0,0,0
		self.ADCSteering, self.ADCcurrentTotal, self.ADCcurrentRR, self.ADCcurrentLR, self.ADCcurrentRF, self.ADCcurrentLF = 0,0,0,0,0,0
		self.accX, self.accY, self.accZ = 0,0,0
		self.gyroX, self.gyroY, self.gyroZ = 0,0,0
		self.magX, self.magY, self.magZ  = 0,0,0
		self.temp  = 0
		self.currentTotal, self.currentRR, self.currentLR, self.currentRF, self.currentLF = 0,0,0,0,0
		self.encSteer, self.posSteer, self.velSteer, self.velRR, self.velLR, self.velRF, self.velLF = 0,0,0,0,0,0,0
		self.commSteer, self.commRR, self.commLR, self.commRF, self.commLF = 0,0,0,0,0

class TelemetryDatagramProtocolBase(object):
	QUERY_FORMAT = "" # nothing
	RESPONCE_FORMAT = "!H3L3L 6h 10h 5h lh5h 5b" # cpu adcs, acc, curr, enc,pos,spd, comm 

	def __init__(self, context):
		self.teledata = context
	def ComposeUdpPacket(self):
		return ""
	def ParseUdpPacket(self, data, looptime):
		context = self.teledata
		context.connected = True;
		(
			context.cpuUsage,
			context.minClksIdle, context.maxClksIdle, context.avgClksIdle,
			context.minClksUsed, context.maxClksUsed, context.avgClksUsed,
			context.ADCSteering, context.ADCcurrentTotal, context.ADCcurrentRR, context.ADCcurrentLR, context.ADCcurrentRF, context.ADCcurrentLF,
			context.accX, context.accY, context.accZ,
			context.gyroX, context.gyroY, context.gyroZ,
			context.magX, context.magY, context.magZ,
			context.temp,
			context.currentTotal, context.currentRR, context.currentLR, context.currentRF, context.currentLF,
			context.encSteer, context.posSteer, context.velSteer, context.velRR, context.velLR, context.velRF, context.velLF,
			context.commSteer, context.commRR, context.commLR, context.commRF, context.commLF,
		) = struct.unpack(self.RESPONCE_FORMAT, data)
		context.cpuUsage /= 10.0
		context.currentTotal /= float(0x7FFF)
		context.currentRR /= float(0x7FFF)
		context.currentLR /= float(0x7FFF)
		context.currentRF /= float(0x7FFF)
		context.currentLF /= float(0x7FFF)
		context.posSteer /= float(0x7FFF)
		context.velSteer /= float(0x7FFF)
		context.velRR /= float(0x7FFF)
		context.velLR /= float(0x7FFF)
		context.velRF /= float(0x7FFF)
		context.velLF /= float(0x7FFF)
		self.teledata = context
		return True
	def OnConnectionTimeout(self):
# 		print "on timeout"
		self.teledata.connected = False;
	def OnConnectionError(self):
# 		print "on err"
		self.teledata.connected = False;

class ControlDatagramProtocolTwisted(UdpProtocolTwisted, TelemetryDatagramProtocolBase):
	def __init__(self, udp_host, udp_port, period, timeout, context, on_data = None, on_error = None, on_restored = None, on_tx = None):
# 		print "proto init", period, timeout
		UdpProtocolTwisted.__init__(self, udp_host, udp_port, period, timeout, on_data = on_data, on_error = on_error, on_restored = on_restored, on_tx = on_tx)
		TelemetryDatagramProtocolBase.__init__(self, context)

class TelemetryCommunicator():
	def __init__(self, host, port, period, timeout, teledata, on_data = None, on_error = None, on_restored = None, on_tx = None):
		self.avg_loop_time = 0.0
		self.timeout = timeout
		self.OnStateChanged = None 
		self.OnConnectionLost = None 
		self.OnConnectionEstablished = None
		self.protocol = ControlDatagramProtocolTwisted(host, port, period, timeout, teledata, on_data = on_data, on_error = on_error, on_restored = on_restored, on_tx = on_tx)
		self.reset_call = None
	def Start(self):
		self.protocol.Start()
	def Stop(self):
		self.protocol.Stop()

class Stm32TelemetryApp:
	def __init__(self, host, port, period, timeout):
		self.teledata = TelemetryData()
		self.animator = CommunicationAnimator()
		self.comm = TelemetryCommunicator(host, port, period, timeout, self.teledata,
							on_data=self.print_state, on_error=self.print_state, on_restored=self.print_state,
							on_tx=self._on_tx)
		print "AntibugUtit UDP telemetry %s:%d" % (host, port)

	def run(self):
		self.print_header()
		self.comm.Start()  
# 		LoopingCall(self.print_state).start(1.0 / 4)
		reactor.run()

	def print_header(self):
		print " CPU |     clocks idle    |     clocks busy    |curr. TOTAL|    STEER      |       RR      |       LR      |       RF      |       LF"
		print "  %  |  min    avg    max |  min    avg    max | val   ADC |enc  pos   vel |vel   cur   ADC|vel   cur   ADC|vel   cur   ADC|vel   cur   ADC"

	def print_state(self):
		data = self.teledata
		print ( "\r"
			+ "%5.1f %6d %6d %6d %6d %6d %6d "
			+ "%5.3f %5d "
			+ "%3d %5.2f %5.2f "
			+ "%4.2f %5.3f %4d "
			+ "%4.2f %5.3f %4d "
			+ "%4.2f %5.3f %4d "
			+ "%4.2f %5.3f %4d "
#			+ "A:%+.3f,%+.3f,%+.3f G:%+.3f,%+.3f,%+.3f"
		) % (
			data.cpuUsage,
			data.minClksIdle, data.avgClksIdle, data.maxClksIdle, data.minClksUsed, data.avgClksUsed, data.maxClksUsed,
			data.currentTotal, data.ADCcurrentTotal,
			data.encSteer, data.posSteer, data.velSteer,
			data.velRR, data.currentRR, data.ADCcurrentRR,
			data.velLR, data.currentLR, data.ADCcurrentLR,
			data.velRF, data.currentRF, data.ADCcurrentRF,
			data.velLF, data.currentLF, data.ADCcurrentLF,
#			teledata.accX, teledata.accY, teledata.accZ, teledata.gyroX, teledata.gyroY, teledata.gyroZ
		),
		if data.connected:
			self.animator.AnimateOk()
		else:
			self.animator.AnimateErr()

	def _on_tx(self, seq):
		if not self.teledata.connected:
			self.print_state() 

	def main(self):
		try:
			self.run()
		except KeyboardInterrupt:
			print "\nExit"

def parse_args():
	from argparse import ArgumentParser
	parser = ArgumentParser(description='STM32 UDP params manager.')
	parser.add_argument("-a", "--host", "--address", help="target host ip name or address",
					default = UDP_IP)
	parser.add_argument("-p", "--port", help="target ip port (UDP)",
					type = int, default = UDP_PORT)
	parser.add_argument("-T", "--period", help="ip communication period (ms)",
					type = int, default = PERIOD_MS)
	parser.add_argument("-t", "--timeout", help="ip communication timeout (ms)",
					type = int, default = TIMEOUT_MS)
	args = parser.parse_args()
	args.period /= 1000.0 # ms to s 
	args.timeout /= 1000.0 # ms to s
	return args 

def run(args):
	a = Stm32TelemetryApp(args.host, args.port, args.period, args.timeout)
	a.main()

if __name__ == '__main__':
	args = parse_args()
	run(args)
