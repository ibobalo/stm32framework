#!/usr/bin/env python
"""
	Crypted Firmware Update
	pack and encrypt firmware
	STM32 must use compatible UDP listener to receive
	firmware image into reserved area of flash memory
	and special bootloader to decrypd the image and flash
	firmware to main memory
"""
from __future__ import print_function
import sys, os, os.path, struct, hashlib
from base64 import encode
import intelhex

MAX_SEGMENT_SIZE = 16 * 1024

META_HEADER_FORMAT    = "<LBxxx"  # size, type
( # type
	MT_NONE, MT_SEGMENT, MT_ERASE, MT_UNIQIDS, MT_MCUIDS, MT_SHA256, MT_CRYPTO, MT_Z, 
) = range(0, 8)

META_SEGMENT_FORMAT   = "<L"     # offset
META_ERASE_FORMAT     = "<B"     # sector
META_UNIQIDS_FORMAT   = "<12s"   # UniqID
META_MCUIDS_FORMAT    = "<H"     # McuID
META_SHA256_FORMAT    = "<32s"   # sha256_digest
META_CRYPTO_FORMAT    = "<L16s"  # unpaded_size, salt,

def log(*args):
	if verbose:
		print(*args)

class FirmwareUpdatePacker:
	def __init__(self, **args):
		self.filename = args.get("filename", "-")
		self.password = args.get("password", "")
		self.use_zip  = args.get("use_zip", False)
		self.use_aes  = args.get("use_aes", False)
		self.aes_key  = args.get("aes_key", "")
		self.mcu_ids  = args.get("mcu_ids", []) 
		self.uniq_ids = args.get("uniq_ids", [])
		self.erase_params = args.get("erase_params", False)

	def Encode(self):
		total_size, segments = self.LoadHexSegments(self.filename) if self.filename else (1, []) 
		chunks = []
		if self.mcu_ids:
			data = self.MetaMcuIds(self.mcu_ids)
			data = self.HashSha256(data)
			if self.use_aes and self.aes_key:
				data = self.Crypt(data)
			chunks.append(data)
			log("{:3d} mcus: [{:d}] {:d}".format(len(chunks), len(self.mcu_ids), len(data)))
		if self.uniq_ids:
			data = self.MetaUniqIds(self.uniq_ids)
			data = self.HashSha256(data)
			if self.use_aes and self.aes_key:
				data = self.Crypt(data)
			chunks.append(data)
			log("{:3d} uids: [{:d}] {:d}".format(len(chunks), len(self.uniq_ids), len(data)))
		if self.erase_params:
			data = self.EraseSectors([1])
			data = self.HashSha256(data)
			if self.use_aes and self.aes_key:
				data = self.Crypt(data)
			chunks.append(data)
			log("{:3d} ~par: {:d}".format(len(chunks), len(data)))
		for address, size, data in segments:
			data = self.HashSha256(data)
			if self.use_zip:
				data = self.CompressZ(data)
			if self.use_aes and self.aes_key:
				data = self.Crypt(data)
			chunks.append(data) 
			log(("{:3d} {}sub: 0x{:08X} - 0x{:08X}  {:d}K ({:d}K"+(",{:2d}%)" if self.use_zip else ")")).format(
					len(chunks), "Z" if self.use_zip else " ", 
					address, address + size - 1,
					(len(data) - 1) / 1024 + 1,
					(size - 1) / 1024 + 1, 100 * len(data) / size))
		res = self.HashSha256("".join(chunks))
		log("UPDATE: {:d} ({:d}KB, {:2d}%)".format(len(res), (len(res) - 1)/1024 + 1, 100 * len(res) / total_size))
		return res
		

	def LoadHexSegments(self, filename):
		log("loading ", filename)
		total_size, segments = 0, []
		ih = intelhex.IntelHex()
		try:
			ih.loadhex(filename)
		except IOError:
			print("File read failed", filename)
			sys.exit(1)
		for hex_segment_start, hex_segment_stop in ih.segments():
			log("segment: 0x%08X - 0x%08X  (%dK)"%(hex_segment_start, hex_segment_stop, (hex_segment_stop - hex_segment_start) / 1024 + 1))
			current_address = hex_segment_start
			while current_address < hex_segment_stop:
				sub_size = min(hex_segment_stop - current_address, MAX_SEGMENT_SIZE) 
				sub_data_str = "".join([chr(ih[a]) for a in range(current_address, current_address + sub_size)])
				wrapped = struct.pack(META_SEGMENT_FORMAT, current_address) + sub_data_str
				meta = struct.pack(META_HEADER_FORMAT, len(wrapped), MT_SEGMENT) + wrapped
				segments.append((current_address, sub_size, meta)) 
				current_address = current_address + sub_size
				total_size += sub_size
		return total_size, segments

	def HashSha256(self, data):
		digest = hashlib.sha256(data).digest()
		signed = struct.pack(META_SHA256_FORMAT, digest) + data
		return struct.pack(META_HEADER_FORMAT, len(signed), MT_SHA256) + signed
		
	def CompressZ(self, data):
		import zlib
		z = zlib.compress(data, 9)
# 		log("Z {:d}->{:d}, {:2d}%".format(len(data), len(z), 100 * len(z) / len(data)))
		return struct.pack(META_HEADER_FORMAT, len(z), MT_Z) + z

	def Crypt(self, data):
		import pyaes
		iv = os.urandom(16)
		aes = pyaes.AESModeOfOperationCFB(self.aes_key, iv, 16)
		pre_len = len(data)
		extra_len = pre_len % 16
		if extra_len:
			data = data + "\xFF" * (16 - extra_len) # pad data with zeroes
		ciphertext = "".join([aes.encrypt(data[n*16:(n+1)*16]) for n in xrange(0, len(data) / 16)])
		ciphred_len = len(ciphertext)
# 		log("AES:iv=%s, d=%s..(%d), c=%s..(%d)"%(iv.encode('hex_codec'), data[0:16].encode('hex_codec'), pre_len, ciphertext[0:16].encode('hex_codec'), ciphred_len))
		crypted = struct.pack(META_CRYPTO_FORMAT, pre_len, iv) + ciphertext
		return struct.pack(META_HEADER_FORMAT, len(crypted), MT_CRYPTO) + crypted

	def MetaMcuIds(self, mcu_ids):
		ids_list = "".join([struct.pack(META_MCUIDS_FORMAT, id) for id in mcu_ids])
		return struct.pack(META_HEADER_FORMAT, len(ids_list), MT_MCUIDS) + ids_list

	def MetaUniqIds(self, uniq_ids):
		ids_list = "".join([struct.pack(META_UNIQIDS_FORMAT, id) for id in uniq_ids])
		return struct.pack(META_HEADER_FORMAT, len(ids_list), MT_UNIQIDS) + ids_list

	def EraseSectors(self, sectors_to_erase):
		erases = "".join([struct.pack(META_ERASE_FORMAT, id) for id in sectors_to_erase])
		return struct.pack(META_HEADER_FORMAT, len(erases), MT_ERASE) + erases

def parse_args():			
	from argparse import ArgumentParser
	parser = ArgumentParser(description='STM32 UDP Firmware uploader.')
	parser.add_argument("-o", "--output", help="output file name",
					default = "") 
	parser.add_argument("--output_hex", help="hex format output file name",
					default = "") 
	parser.add_argument("-a", "--hex_offset", help="build a hex file with given offset", type=lambda x: int(x,0),
					default = 0)
	parser.add_argument("--output_bin", help="bin format output file name",
					default = "") 
	parser.add_argument("-p", "--password", help="cryptography password - AES key is sha256 hash of the password",
					default = "")
	zip_group = parser.add_mutually_exclusive_group(required=False) 
	zip_group.add_argument("-z", "--zip", help="compress update data with zip",
					dest="use_zip", action='store_true')
	zip_group.add_argument("--no-zip", help="do not compress update data with zip",
					dest="use_zip", action='store_false')
	parser.set_defaults(use_zip=True)
	aes_group = parser.add_mutually_exclusive_group(required=False) 
	aes_group.add_argument("--aes", help="protect update with AES-256 crypto",
					dest="use_aes",  action='store_true')
	aes_group.add_argument("--no-aes", help="protect update with AES-256 crypto",
					dest="use_aes",  action='store_false')
	parser.set_defaults(use_aes=True)
	parser.add_argument("-k", "--aes_key", help="cryptography binary key (256 bits hexdumped)",
					default = "") 
	parser.add_argument("--keyfile", help="cryptography binary key file",
					default = "")
	parser.set_defaults(mcu_ids=[])
	parser.add_argument("-D", "--device", action="append", dest="mcu_ids",
					help="allow flash to specified MCU Id's, defaults - allow all", metavar="DEVICE")
	parser.set_defaults(uniq_ids=[])
	parser.add_argument("-U", "--uniqID", action="append", dest="uniq_ids",
					help="allow flash to devices with Unique IDs, defaults - allow all devices", metavar="UNIQID")
	parser.set_defaults(erase_params=False)
	parser.add_argument("-E", "--erase_params", help="erase pseudo-eeprom params storage",
					action='store_true')
	parser.add_argument("hex_file", help="filename of firmware (intel hex format file) to upload",
					default = "", nargs="?") 
	parser.set_defaults(verbose=False)
	parser.add_argument("-v", "--verbose", help="print logs",
					action='store_true') 
	return parser.parse_args()

def process_args(args):
	global verbose
	verbose = args.verbose
	if args.use_aes:
		if args.aes_key:
			args.aes_key = args.aes_key.decode("hex_codec")
		elif args.password:
			args.aes_key = hashlib.sha256(args.password.strip()).digest()
	 		log(" password based aes_key:", args.aes_key.encode("hex_codec"))
		else:
			print(" neither AES key nor password specified")
			exit(1)
	args.mcu_ids = [int(id, 0) for id in args.mcu_ids]
	args.uniq_ids = [id.decode('hex_codec') for id in args.uniq_ids]
	if args.output:
		out_type = os.path.splitext(args.output)[1]
		if out_type == ".hex":
			args.output_hex = args.output
		if out_type == ".bin":
			args.output_bin = args.output
	else:
		args.output_hex = os.path.splitext(args.hex_file)[0] + ".update.hex"
		args.output_bin = os.path.splitext(args.hex_file)[0] + ".update.bin"
	if args.output_hex and not args.hex_offset:
		print(" hex offset not specified")
		exit(1)
	return args

def main():
	args = parse_args()
	args = process_args(args)
	fp = FirmwareUpdatePacker(filename = args.hex_file, **args.__dict__)
	data = fp.Encode();
	if args.output_bin:
		file(args.output_bin, 'wb').write(data)
	if args.output_hex:
		ih = intelhex.IntelHex()
# 		ih.set_mode(32)
		ih.puts(args.hex_offset, data)
		ih.write_hex_file(args.output_hex)

if __name__ == '__main__':
	main();
