import struct
import fnmatch, re
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

UDP_HOST = "192.168.1.1"
UDP_PORT = 5020
TIMEOUT_MS = 100
HAS_DETAILS_EX = 1

"""
	class Stm32Param:
	
	representation of STM32 system parameter
	
	constructor:
		Stm32Param(id, name, type, value, default, desc)

		id - unique identifier (numeric index)
		name - symbolic name (string)
		type - (1: bool, 2: int, 3: value_uint, 4: float)
		value - parameter actual value
		default - parameter default value
		desc - descriprion (string)
"""
class Stm32Param:
	def __init__(self, id, name, type, value, default=None, descr_id=None, descr=None):
		self.id = id
		self.name = name
		self.type = type
		self.value = value
		self.default = default
		self.descr_id = descr_id
		self.descr = descr

	def has_default(self):
		return not self.default == None
	def has_descr(self):
		return bool(self.descr)
	def is_default(self):
		return self.has_default() and self.value == self.default
		
	def __str__(self):
		return "%3d %s%s%s%s" % (
				self.id, self.name,
				("" if     self.is_default()  else " %s"%str(self.value)),
				("" if not self.has_default() else " [%s]"%str(self.default)),
				("" if not self.has_descr()   else " \"%s\""%self.descr),
		)

"""
	interface Stm32ParamsContext:
	
	access methods to dictionary of known STM32 system parameters.
"""
class Stm32ParamsContext(object):
	#pure virtual
	def SetParamsCount(self, params_count):
		raise NotImplementedError
	def GetParamsCount(self):
		raise NotImplementedError
	def SetParamDetails(self, param):
		raise NotImplementedError
	def SetDescription(self, descr_id, descr_text):
		raise NotImplementedError
	def GetParamDetails(self, param_id):
		raise NotImplementedError
	def GetKnownParams(self):
		raise NotImplementedError
	def GetUnknownParams(self):
		raise NotImplementedError
	def GetUnknownDescrs(self):
		raise NotImplementedError

"""
	class Stm32ParamsDatagramProtocolBase:
	
	base constatnts and UDP packet composer/parser.
	
	Stm32ParamsDatagramProtocolBase(context)
	
	context - reference to Stm32ParamsContext implementing instance 
"""
class Stm32ParamsDatagramProtocolBase():
	def __init__(self, context):
		self.context = context
		self.seq = 0
		self.retry = None

	QUERY_TYPE_PARAMS_COUNT = 1
	QUERY_TYPE_PARAM_DETAILS = 2
	QUERY_TYPE_PARAM_SET = 3
	QUERY_TYPE_PARAM_DETAILS_EX = 4
	QUERY_TYPE_PARAM_DESCR = 5
	QUERY_TYPE_STORAGE_CLEAR = 6
	QUERY_HEADER_FORMAT = "!HH"  # seq, type
	QUERY_PARAM_DETAILS_FORMAT = "!L" # param_id
	QUERY_PARAM_SET_HEADER_FORMAT = "!LH" # param_id, value_len
	QUERY_PARAM_DETAILS_EX_FORMAT = "!L" # param_id
	QUERY_PARAM_DESCR_EX_FORMAT = "!L" # descr_id
	
	RESPONCE_TYPE_PARAMS_COUNT = QUERY_TYPE_PARAMS_COUNT 
	RESPONCE_TYPE_PARAM_DETAILS = QUERY_TYPE_PARAM_DETAILS 
	RESPONCE_TYPE_PARAM_DETAILS_EX = QUERY_TYPE_PARAM_DETAILS_EX
	RESPONCE_TYPE_PARAM_DESCR = QUERY_TYPE_PARAM_DESCR
	RESPONCE_TYPE_STORAGE_CLEAR = QUERY_TYPE_STORAGE_CLEAR
	RESPONCE_HEADER_FORMAT="!HH" # seq, type
	RESPONCE_PARAMS_COUNT_FORMAT="!L" #count
	RESPONCE_PARAMS_DETAIL_HEADER_FORMAT="!LHHH" #param_id, value_type, value_len, name_len
	RESPONCE_PARAMS_DETAIL_EX_HEADER_FORMAT="!LHHHL" #param_id, value_type, value_len, name_len, desc_id
	RESPONCE_PARAMS_DETAIL_NAME_FORMAT="!%ds" #name_chars...
	RESPONCE_PARAM_DESCR_HEADER_FORMAT="!LH" #descr_id, descr_len, 
	RESPONCE_PARAM_DESCR_DATA_FORMAT="!%ds" #descr_chars...

	VALUE_FORMATS = {
		1:"!?", #value_bool
		2:"!l", #value_int
		3:"!L", #value_uint
		4:"!d", #value_float
	}
	VALUE_COVERTERS = {
		1:lambda v: str(v).lower() == "true" or (str(v).isdigit() and int(v) != 0),
		2:int,
		3:long,
		4:float,
	}
	def ComposeUdpPacket_ParamsCount(self):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_PARAMS_COUNT)
		return pkt_data
	def ComposeUdpPacket_ParamDetails(self, param_id):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_PARAM_DETAILS) \
				 + struct.pack(self.QUERY_PARAM_DETAILS_FORMAT, param_id)
		return pkt_data
	def ComposeUdpPacket_ParamDetailsEx(self, param_id):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_PARAM_DETAILS_EX) \
				 + struct.pack(self.QUERY_PARAM_DETAILS_EX_FORMAT, param_id)
		return pkt_data
	def ComposeUdpPacket_ParamSet(self, param_id, new_param_value):
		self.seq = (self.seq + 1) & 0xFFFF
		param = self.context.GetParamDetails(param_id)
		packed_value = struct.pack(self.VALUE_FORMATS[param.type], self.VALUE_COVERTERS[param.type](new_param_value))
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_PARAM_SET) \
				 + struct.pack(self.QUERY_PARAM_SET_HEADER_FORMAT, param_id, len(packed_value)) \
				 + packed_value
		return pkt_data
	def ComposeUdpPacket_ParamDescr(self, descr_id):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_PARAM_DESCR) \
				 + struct.pack(self.QUERY_PARAM_DESCR_EX_FORMAT, descr_id)
		return pkt_data
	def ComposeUdpPacket_StorageClear(self):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_STORAGE_CLEAR)
		return pkt_data
	def ParsePktData(self, format_string):
		size = struct.calcsize(format_string)
		if len(self.udp_data) - self.udp_data_start < size:
			raise "Invalid responce" 
		res = struct.unpack_from(format_string, self.udp_data, self.udp_data_start)
		self.udp_data_start = self.udp_data_start + size
		return res
	def ParseUdpPacket(self, data):
		self.udp_data = data
		self.udp_data_start = 0
		(seq, responce_type) = self.ParsePktData(self.RESPONCE_HEADER_FORMAT)
		if seq == self.seq:
			{
				self.RESPONCE_TYPE_PARAMS_COUNT:     self.ParseUdpPacket_ParamsCount,
				self.RESPONCE_TYPE_PARAM_DETAILS:    self.ParseUdpPacket_ParamDetails,
				self.RESPONCE_TYPE_PARAM_DETAILS_EX: self.ParseUdpPacket_ParamDetailsEx,
				self.RESPONCE_TYPE_PARAM_DESCR:      self.ParseUdpPacket_ParamDescr,
				self.RESPONCE_TYPE_STORAGE_CLEAR:    self.ParseUdpPacket_StorageClear,
			}.get(responce_type, self.ParseUdpPacket_InvalidType)()
			return True
		return False
	def ParseUdpPacket_ParamsCount(self):
		(params_count,) = self.ParsePktData(self.RESPONCE_PARAMS_COUNT_FORMAT)
		self.context.SetParamsCount(params_count)
	def ParseUdpPacket_ParamDetails(self):
		(param_id, value_type, value_len, name_len) = self.ParsePktData(self.RESPONCE_PARAMS_DETAIL_HEADER_FORMAT)
		(param_value,) = self.ParsePktData(self.VALUE_FORMATS[value_type])
		(param_name,) = self.ParsePktData(self.RESPONCE_PARAMS_DETAIL_NAME_FORMAT % name_len)
		param = Stm32Param(param_id, param_name, value_type, param_value)
		self.context.SetParamDetails(param)
	def ParseUdpPacket_ParamDetailsEx(self):
		(param_id, value_type, value_len, name_len, descr_id) = self.ParsePktData(self.RESPONCE_PARAMS_DETAIL_EX_HEADER_FORMAT)
		(param_value,) = self.ParsePktData(self.VALUE_FORMATS[value_type])
		(param_default,) = self.ParsePktData(self.VALUE_FORMATS[value_type])
		(param_name,) = self.ParsePktData(self.RESPONCE_PARAMS_DETAIL_NAME_FORMAT % name_len)
		param = Stm32Param(param_id, param_name, value_type, param_value, param_default, descr_id)
		self.context.SetParamDetails(param)
	def ParseUdpPacket_ParamDescr(self):
		(descr_id, descr_len) = self.ParsePktData(self.RESPONCE_PARAM_DESCR_HEADER_FORMAT)
		(descr_text,) = self.ParsePktData(self.RESPONCE_PARAM_DESCR_DATA_FORMAT % descr_len)
		self.context.SetDescription(descr_id, descr_text)
	def ParseUdpPacket_StorageClear(self):
		self.context.SetParamsCount(0)
	def ParseUdpPacket_InvalidType(self):
		raise StandardError("invalid respoce type")

"""
	class Stm32ParamsDatagramProtocolTwisted:
	
	protocol implementation for twisted reactor library
"""
class Stm32ParamsDatagramProtocolTwisted(Stm32ParamsDatagramProtocolBase, DatagramProtocol):
	def __init__(self, udp_host, udp_port, timeout, use_extended_requests, context):
		Stm32ParamsDatagramProtocolBase.__init__(self, context)
		self.udp_host = udp_host 
		self.udp_port = udp_port 
		self.timeout  = timeout
		self.has_details_ex = use_extended_requests
		self.rx_sock = None
		self.retries_count = 0
		self.done_callback, self.done_callback_args = None, {}
		self.retry_callback, self.retry_callback_args = None, {}
		self._install_listener()
		print "proto init", self.udp_host, ':', self.udp_port, self.timeout * 1000, 'ms'
	def __del__(self):
		self._uninstall_listener()
	def SetDoneCallback(self, callback, *args):
		self.done_callback, self.done_callback_args = callback, args
	def SetRetryCallback(self, callback, *args):
		self.retry_callback, self.retry_callback_args = callback, args
	def Stop(self): 
		print "protocol stop"
		self._uninstall_listener()
		if self.retry:
			self.retry.cancel()
			self.retry = None
	def sendParamsCountRequestDatagram(self):
# 		print 'n',
		if self.rx_sock and self.transport:
			datagram = self.ComposeUdpPacket_ParamsCount()
			self.transport.write(datagram)
		else:
			self._install_listener()
		self._init_retry(self.sendParamsCountRequestDatagram)
	def sendParamDetailsRequestDatagram(self, param_id):
# 		print 'd',
		if self.rx_sock:
			datagram = (self.ComposeUdpPacket_ParamDetailsEx if self.has_details_ex else self.ComposeUdpPacket_ParamDetails)(param_id) 
			self.transport.write(datagram)
		self._init_retry(self.sendParamDetailsRequestDatagram, param_id)
	def sendParamSetRequestDatagram(self, param_id, new_param_value):
# 		print 's',
		if self.rx_sock:
			datagram = self.ComposeUdpPacket_ParamSet(param_id, new_param_value)
			self.transport.write(datagram)
		self._init_retry(self.sendParamSetRequestDatagram, param_id, new_param_value)
	def sendParamDescrRequestDatagram(self, descr_id):
# 		print 's',
		if self.rx_sock:
			datagram = self.ComposeUdpPacket_ParamDescr(descr_id) 
			self.transport.write(datagram)
		self._init_retry(self.sendParamDescrRequestDatagram, descr_id)
	def sendClearStorageRequestDatagram(self):
# 		print 'c',
		if self.rx_sock:
			datagram = self.ComposeUdpPacket_StorageClear()
			self.transport.write(datagram)
		self._init_retry(self.sendClearStorageRequestDatagram)
	def startProtocol(self):
		self.transport.connect(self.udp_host, self.udp_port)
		print "proto connect"
	def datagramReceived(self, datagram, host):
# 		print 'v'
		if self.ParseUdpPacket(datagram):
			self.retry.cancel()
			self.retry = None
			if self.done_callback: self.done_callback(self, *self.done_callback_args)
			self.retries_count = 0
	def _install_listener(self):
		if not self.rx_sock:
			try:
				self.rx_sock = reactor.listenUDP(self.udp_port + 1, self) # Catch Responces to  datagramReceived
				print "rx_sock installed"
			except Exception, exc:
				print "rx_sock faulted", exc
				pass 
	def _uninstall_listener(self):
		if self.rx_sock:
			try:
				self.rx_sock.stopListening()
				print "rx_sock uninstalled"
			except Exception, exc:
				print "rx_sock faulted", exc
				pass 
			self.rx_sock = None
	def connectionRefused(self):
		print "twisted connectionRefused"
		self._uninstall_listener()
		self.OnConnectionError()
	def _init_retry(self, fn, *args):
		self._install_listener()
		if self.timeout:
			self.retry = reactor.callLater(self.timeout, fn, *args)
			if self.retries_count and self.retry_callback: self.retry_callback(self, *self.retry_callback_args)
			self.retries_count = self.retries_count + 1

class Stm32ParamsStorage(Stm32ParamsContext):
	def __init__(self):
		self.params_count = 0
		self.known_params = {}
		self.unknown_ids = set()
		self.param_descrs = {}
		self.unknown_descrs = set()
	# Stm32ParamsContext pure virtuals
	def SetParamsCount(self, params_count):
		self.params_count = params_count
		self.unknown_ids = set(xrange(0, self.params_count)) - set(self.known_params.keys())
	def GetParamsCount(self):
		return self.params_count
	def SetParamDetails(self, param):
# 		print "got param", param.id, "with descr id", param.descr_id
		if param.descr_id:
			if self.param_descrs.has_key(param.descr_id):
				param.descr = self.param_descrs[param.descr_id]
			else:
				self.unknown_descrs.add(param.descr_id)
# 				print " unknown descr id", param.descr_id, self.unknown_descrs 
		self.known_params[param.id] = param
		if param.id in self.unknown_ids:
			self.unknown_ids.remove(param.id)
	def SetDescription(self, descr_id, descr_text):
# 		print "got descr", descr_id, descr_text
		if descr_id in self.unknown_descrs:
			self.unknown_descrs.remove(descr_id)
			self.param_descrs[descr_id] = descr_text
			for param in self.known_params.values():
				if param.descr_id == descr_id:
					param.descr = descr_text
	def GetParamDetails(self, param_id):
		return self.known_params.get(param_id)
	def GetKnownParams(self):
		return self.known_params
	def GetUnknownParams(self):
		return self.unknown_ids
	def GetUnknownDescrs(self):
		return self.unknown_descrs

class Stm32ParamsFsm(object):
	def __init__(self, protocol, storage):
		self.protocol = protocol
		self.storage = storage
		self.done_callback, self.done_callback_args = None, {}
		self.retry_callback, self.retry_callback_args = None, {}
		self.protocol.SetDoneCallback(self.OnTransactionSucceed)
		self.protocol.SetRetryCallback(self.OnTransactionRetry)
	def SetDoneCallback(self, callback, *args):
		self.done_callback = callback 
		self.done_args = args
	def SetRetryCallback(self, callback, *args):
		self.retry_callback = callback 
		self.retry_args = args
	def OnTransactionSucceed(self, protocol):
		if self.done_callback:
			self.done_callback(*self.done_args)
	def OnTransactionRetry(self, protocol):
		if self.retry_callback:
			self.retry_callback(*self.retry_args)
	def Stop(self):
		self.protocol.Stop()

class Stm32ParamsFsm_ClearStorage(Stm32ParamsFsm):
	def Start(self, done_callback, *done_args):
		self.SetDoneCallback(done_callback, *done_args)
		reactor.callWhenRunning(self.protocol.sendClearStorageRequestDatagram) ## continue in self.OnTransactionSucceed 

class Stm32ParamsFsm_ReadParamsCount(Stm32ParamsFsm):
	def Start(self, done_callback, *done_args):
		self.SetDoneCallback(done_callback, *done_args)
		reactor.callWhenRunning(self.protocol.sendParamsCountRequestDatagram) ## continue in self.OnTransactionSucceed 

class Stm32ParamsFsm_ReadParamDetails(Stm32ParamsFsm):
	def Start(self, param_id, done_callback, *done_args):
		self.SetDoneCallback(done_callback, *done_args)
		reactor.callWhenRunning(self.protocol.sendParamDetailsRequestDatagram, param_id) ## continue in self.OnTransactionSucceed 
	def OnTransactionSucceed(self, protocol):
		for descr_id in self.storage.GetUnknownDescrs():
			protocol.sendParamDescrRequestDatagram(descr_id)
			break 
		else:
			super(Stm32ParamsFsm_ReadParamDetails, self).OnTransactionSucceed(protocol) 

class Stm32ParamsFsm_WriteParamValue(Stm32ParamsFsm):
	def Start(self, param_id, param_value, done_callback, *done_args):
		self.SetDoneCallback(done_callback, *done_args)
		reactor.callWhenRunning(self.protocol.sendParamSetRequestDatagram, param_id, param_value) ## continue in self.OnTransactionSucceed

class Stm32ParamsFsm_ReadAllParamsDetails(Stm32ParamsFsm):
	def __init__(self, protocol, storage):
		super(Stm32ParamsFsm_ReadAllParamsDetails, self).__init__(protocol, storage)
		self.step_callback = None
		self.step_args = {}
	def Start(self, done_callback, *done_args):
		self.SetDoneCallback(done_callback, *done_args)
		reactor.callWhenRunning(self.OnTransactionSucceed, self.protocol) ## continue in self.OnTransactionSucceed 
	def SetStepCallback(self, callback, *args):
		self.step_callback = callback 
		self.step_args = args
	def OnTransactionSucceed(self, protocol):
		for descr_id in self.storage.GetUnknownDescrs():
			self.protocol.sendParamDescrRequestDatagram(descr_id)
			if self.step_callback: self.step_callback(*self.step_args)
			return
		for param_id in self.storage.GetUnknownParams():
			self.protocol.sendParamDetailsRequestDatagram(param_id)
			if self.step_callback: self.step_callback(*self.step_args)
			return 
		super(Stm32ParamsFsm_ReadAllParamsDetails, self).OnTransactionSucceed(protocol) 
		
class Stm32ParamsManager(object):
	def __init__(self, args):
		self.args = args
		self.storage = Stm32ParamsStorage()
		self.fsm = None
		self.protocol = Stm32ParamsDatagramProtocolTwisted(self.args.host, self.args.port, self.args.timeout, self.args.use_extended_requests, self.storage)
		self.animation = 0
		
	def FilterParamsByName(self, name_filter_re):
		result = []
		for param in self.storage.GetKnownParams().values():
			if name_filter_re.match(param.name):
				result.append(param.id)
		return result
	def PrintParam(self, param_id):
		param = self.storage.GetParamDetails(param_id)
		print "%3d %s" % (param.id, param.name), param.value
	def PrintParamEx(self, param_id):
		param = self.storage.GetParamDetails(param_id)
		if not (self.args.changed_only and param.is_default()):
			print param 
	def PrintAllParams(self):
		for param_id in self.storage.GetKnownParams().keys():
			self.PrintParam(param_id)
	def PrintParams(self, params_list):
		for param_id in params_list:
			self.PrintParamEx(param_id)
	
	def main(self):
		try:
			if self.args.clear_storage:
				self.fsm = Stm32ParamsFsm_ClearStorage(self.protocol, self.storage)
				self.fsm.Start(self.clear_done_callback)
			elif not self.args.param_id is None:
				self.fsm = Stm32ParamsFsm_ReadParamDetails(self.protocol, self.storage)
				self.fsm.Start(self.args.param_id, self.read_done_callback)
			else:
				self.fsm = Stm32ParamsFsm_ReadParamsCount(self.protocol, self.storage)
				self.fsm.Start(self.count_done_callback)
			reactor.run()
		except KeyboardInterrupt:
			print "              \nExit"
	def amination_callback(self):
		self.animation = (self.animation + 1)%4
		print "%s\r"%"|/-\\"[self.animation],
	def count_done_callback(self):
		self.fsm = Stm32ParamsFsm_ReadAllParamsDetails(self.protocol, self.storage)
		self.fsm.SetStepCallback(self.amination_callback)
		self.fsm.Start(self.read_done_callback)
	def read_done_callback(self):
		if self.args.param_re:
			params_list = self.FilterParamsByName(self.args.param_re)
		else:
			params_list = self.storage.GetKnownParams()
		self.PrintParams(params_list)

		if len(params_list) == 1 and self.args.value:
			param_id = params_list.keys()[0]
			param = self.storage.GetParamDetails(param_id)
			new_param_value = self.protocol.VALUE_COVERTERS[param.type](self.args.value)
			if param.value == new_param_value:
				print "done."
				reactor.stop()
			else:
				print "changing..."
				self.fsm = Stm32ParamsFsm_WriteParamValue(self.protocol, self.storage)
				self.fsm.Start(param_id, new_param_value, self.write_done_callback)
		else:
			reactor.stop()
	def write_done_callback(self):
		self.fsm = Stm32ParamsFsm_ReadParamDetails(self.protocol, self.storage)
		self.fsm.Start(self.args.param_id, self.read_done_callback)
	def clear_done_callback(self):
		print "storage erased"
		reactor.stop()
		


def parse_args(default_address = UDP_HOST, default_port = UDP_PORT, default_timeout_ms = TIMEOUT_MS, use_extended_requests = HAS_DETAILS_EX):
	from argparse import ArgumentParser
	parser = ArgumentParser(description='STM32 UDP params manager.')
	parser.add_argument("-a", "--host", "--address", help="target host ip name or address",
					default = default_address)
	parser.add_argument("-p", "--port", help="target ip port (UDP)",
					type = int, default = default_port)
	parser.add_argument("-t", "--timeout", help="ip communication timeout (ms)",
					type = int, default = default_timeout_ms)
	parser.add_argument("-c", "--changed-only", help="show only changed params", action='store_true')
	parser.add_argument("-o", "--old-api", help="use old api version (no params desc etc)", action='store_true')
	parser.add_argument("-e", "--regexp", help="use perl regexp for param name filter", action='store_true')
	parser.add_argument("--clear-storage", help="clear params storage (eeprom)", action='store_true')
	parser.add_argument("param", help="parameter name, wildcards allowed",
					nargs = "?", default = "") 
	parser.add_argument("value", help="new value to set",
					nargs = "?", default = "")
	
	args = parser.parse_args()
	args.timeout /= 1000.0 # ms to s
	args.use_extended_requests = not args.old_api and use_extended_requests
	if args.param:
		if args.param.isdigit():
			args.param_id, args.param_re = int(args.param), None
		elif args.regexp:  
			args.param_id, args.param_re = None, re.compile(args.param)
		else:
			args.param_id, args.param_re = None, re.compile(fnmatch.translate(args.param))
	else:
		args.param_id, args.param_re = None, None

	return args

import sys
if __name__ == "__main__":
	print "params ", sys.argv
	args = parse_args()
	a = Stm32ParamsManager(args)
	a.main()
