import sys, os, os.path
import pypi_import

with pypi_import.pypi_import():
	import onvif

mycam = onvif.ONVIFCamera('192.168.1.10', 80, 'admin', '', '/etc/onvif/wsdl/')

# Get Hostname
resp = mycam.devicemgmt.GetHostname()
print 'My camera`s hostname: ' + str(resp.Hostname)

# Get system date and time
dt = mycam.devicemgmt.GetSystemDateAndTime()
tz = dt.TimeZone
year = dt.UTCDateTime.Date.Year
hour = dt.UTCDateTime.Time.Hour

print year, hour, tz
