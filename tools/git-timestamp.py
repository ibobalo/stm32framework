#!/usr/bin/env python
import sys, subprocess, datetime, os.path, argparse

FORMAT='%y%m%d%H%M%S'

args = argparse.Namespace()
sysencoding = sys.stdout.encoding or 'utf-8'
logfile = None

def log(*a):
	if args.verbose:
		global logfile
		if logfile is None:
			if args.logfile:
				logfile = file(os.path.join(args.path, args.logfile), 'w')
			if args.output == '-':
				logfile = sys.stderr
			else:
				logfile = sys.stdout
		logfile.write(" ".join([str(a) for a in a]) + '\n')
	
class GitTimestampBuider:
	GIT_CHANGES = "git status --porcelain --ignore-submodules=untracked --untracked-files=no"
	GIT_ROOT = "git rev-parse --show-toplevel"
	GIT_DATE = "git show -s --format=%cD"
	GIT_SUBMODULES = "git submodule--helper list"
	GIT_ALLFILES = "git ls-files --recurse-submodules"

	def __init__(self, cwd = '.'):
		self.recentPath = ""
		self.changed = False
		try:
			gitdate = subprocess.check_output(GitTimestampBuider.GIT_DATE).decode(sysencoding).strip()
			log("repo date:", gitdate)
#			self.recentDate = datetime.datetime.strptime(gitdate, '%a, %d %b %Y %H:%M:%S %z')
#			self.recentDate.replace(tzinfo=None)
			self.recentDate = datetime.datetime.strptime(gitdate[:25].strip(), '%a, %d %b %Y %H:%M:%S')
# 			log(gitdate, "/", gitdate[26], "/", str(gitdate[27:31]), gitdate[27:29], gitdate[29:31])
			if gitdate[26] == '+':
				tzoffset = datetime.timedelta(hours=int(gitdate[27:29]), minutes=int(gitdate[29:31]))
				self.recentDate = self.recentDate - tzoffset
# 				log("+", tzoffset, "UTC:", self.recentDate)
			elif gitdate[26] == '-':
				tzoffset = datetime.timedelta(hours=int(gitdate[27:29]), minutes=int(gitdate[29:31]))
				self.recentDate = self.recentDate + tzoffset
# 				log("-", tzoffset, "UTC:", self.recentDate)
# 			else:
# 				log("z", "UTC:", self.recentDate)
			self._build(cwd = cwd)
		except subprocess.CalledProcessError as err:
			if err.returncode == 128: # not a git repo
				log(cwd, "Is not a git repo")
				self.recentDate = datetime.datetime.now()
			else:
				raise err
		except Exception as err:
			log("GIT TIMESTAMP for", cwd, "FAULT:", err)
			self.recentDate = datetime.datetime.now()
		
	def _build(self, cwd = ".", prefix = ""):
		log('building for', cwd)
#		gitallfiles = subprocess.check_output(GitTimestampBuider.GIT_ALLFILES, cwd = cwd).strip()
		submodules = [l.split()[3] for l in filter(None, subprocess.check_output(GitTimestampBuider.GIT_SUBMODULES, cwd = cwd).decode(sysencoding).strip().split('\n'))]
		log(prefix, "subs:", submodules)
		changes = subprocess.check_output(GitTimestampBuider.GIT_CHANGES, cwd = cwd).decode(sysencoding).strip()
		if changes:
			self.changed = True
			rootpath = subprocess.check_output(GitTimestampBuider.GIT_ROOT, cwd = cwd).decode(sysencoding).strip()
			for l in changes.splitlines():
				x,y,p = l[0], l[1], l[2:].strip().strip('\"')
# 				log("x:'%s' y:'%s' p:'%s'"%(x,y,p))
				if x == 'D' or y == 'D': continue
				if x == 'C' or x == 'R': p = p.split('->')[1].strip().strip('\"')
				path = os.path.join(rootpath, p)
				if p in submodules:
#				if os.path.isdir(recentPath):
					self._build(cwd = path, prefix = "/".join(filter(None, [prefix, p])))
				else:
					fileDate = datetime.datetime.utcfromtimestamp(os.path.getmtime(path))
					log(fileDate.strftime(FORMAT), x, y, "/".join(filter(None, [prefix, p])))
					if (fileDate > self.recentDate):
						self.recentDate = fileDate
						self.recentPath = p

	def getTimestamp(self, format = FORMAT):
		timestamp = self.recentDate.strftime(format)
		if self.changed:
			return timestamp[:-1] + 'C'
		return timestamp

	def getRecentPath(self):
		return self.recentPath

def parseCommandLine():
	parser = argparse.ArgumentParser(description='Build version timestamp based on git repository status and local changed files.')
	parser.add_argument('--verbose', '-v', default = False,  help='verbose output', action='store_true')
	parser.add_argument('--logfile', help='log to file')
	parser.add_argument('--format', '-f', default = FORMAT, help='output timestamp format, default ' + FORMAT)
	parser.add_argument('--output', '-o', default = '-', help='output file, use - for stdout (default)')
	parser.add_argument('--directory', '--reposotory', '-C', nargs='?', default='.', help='repository path')
	parser.add_argument('--files', '--sources', help='files filter')
	parser.add_argument('path', nargs='?', default='.', help='repository path')

	return parser.parse_args()

args = parseCommandLine()
log('GIT TIMESTAMP start for:', args.path)
b = GitTimestampBuider(cwd = args.path)
(sys.stdout if args.output == '-' else file(args.output,'w')).write(b.getTimestamp(format = args.format))
