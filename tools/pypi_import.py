import sys, os, os.path, __builtin__
from time import sleep
"""
	automaticaly download and install missed packages
	directly from your python scripts.
	it will use eazy_install and interactive
	privileges promotions for "admin" or "root" privileges
	usage:
		import pypi_import
		with pypi_import.pypi_import():
			import ihex
"""

__import__org__ = __builtin__.__import__
__import__level__ = 0

class pypi_import:
	def __enter__(self):
		global __import__level__
		__import__level__ = 0
		__builtin__.__import__ = pypi_import_or_install
		return self
	def __exit__(self, type, value, traceback):
		__builtin__.__import__ = __import__org__
	
def pypi_import_or_install(name, globals=globals(), locals=locals(), fromlist=[], level=-1):
	global __import__level__
	try:
# 		print "pypi importing", name, "from", fromlist
		__import__level__ = __import__level__ + 1 
		res = __import__org__(name, globals=globals, locals=locals, fromlist=fromlist, level=level)
		__import__level__ = __import__level__ - 1 
		return res
	except ImportError, exc:
		if __import__level__ != 1: 
			__import__level__ = __import__level__ - 1 
			raise exc
		print "package %s import failed [%s]."%(name, exc)
		OnMissedImport(name)

def IsUserAdmin():
	if sys.platform == 'win32':
		import ctypes
		return ctypes.windll.shell32.IsUserAnAdmin()
	return os.getuid() == 0

def RunPyAsAdmin(py_args):
	if IsUserAdmin():
		print "already admin. run:\n  [sys.executable] + py_args"
		import subprocess
		return subprocess.call([sys.executable] + py_args)
	if sys.platform == 'win32':
		try:
			import win32con, win32event, win32process, win32com.shell.shellcon, win32com.shell.shell
		except ImportError:
			print "unable to self promote to admin privilages. install pywin32 from\n  http://sourceforge.net/projects/pywin32/files/pywin32"
			sleep(20)
			sys.exit(1)
		params =  '"%s"' % ' '.join(['"%s"' % x for x in ["/c", sys.executable] + py_args])
		procHandle = win32com.shell.shell.ShellExecuteEx(lpVerb='runas', nShow=win32con.SW_SHOW, fMask=win32com.shell.shellcon.SEE_MASK_NOCLOSEPROCESS, lpFile="cmd", lpParameters=params)['hProcess']
		win32event.WaitForSingleObject(procHandle, win32event.INFINITE)
		return win32process.GetExitCodeProcess(procHandle)
	import subprocess
	return subprocess.call(["sudo"] + [sys.executable] + py_args)

def PromoteMeToAdmin():
	ASADMIN = 'asadmin'
	recursive_fork = (sys.argv[-1] == ASADMIN)
	if IsUserAdmin():
		return recursive_fork
	if recursive_fork:
		print "Recursive promotion loop"
		sys.exit(1)
	res = RunPyAsAdmin(sys.argv + [ASADMIN])
	print "executing done, exit code", res
	if res:
		sys.exit(res)
	return False

def OnMissedImport(module):
	package = module.split('.')[0]
	print "installing package %s ..."%package, "for import", module
	if package == 'setuptools':
		print "installing setuptools for other packages easy installation" 
		setuptools_url = "https://bootstrap.pypa.io/ez_setup.py"
		import urllib2
		from tempfile import NamedTemporaryFile
		f = NamedTemporaryFile(suffix='.py', delete=False)
		f.write(urllib2.urlopen(setuptools_url).read())
		f.close()
		res = RunPyAsAdmin([f.name, "||", "pause"])
		os.remove(f.name)
		if res:
			print "installing setuptools failed" 
			time.sleep(30)
			sys.exit(res)
#			print "unable automatic package installation. install setuptools from\n  https://pypi.python.org/pypi/setuptools"
#			print "or manualy install package \"%s\" from PyPI\n  https://pypi.python.org/pypi/%s"%(package, package)
#			sys.exit(1)
	else:
		try:
			from setuptools.command import easy_install
		except ImportError:
			OnMissedImport("setuptools")
		from setuptools.command import easy_install
		print "trying to promote privilegies for python package install"
		recursive_fork = PromoteMeToAdmin()
		if IsUserAdmin():
			try:
				easy_install.main( ["-U", package])
				print "package '%s' installed. python restart required"%package
			except Exception, exc:
				from traceback import print_exc
				print_exc(10)
				sleep(30)
				sys.exit(1)
			except SystemExit, exc:
				print exc
				sleep(30)
				sys.exit(1)
		sleep(5)
		sys.exit(0) # exit recursive fork 


if __name__ == '__main__':
	print "this is pure utility package, not for stand-alone usage"
	print __doc__
