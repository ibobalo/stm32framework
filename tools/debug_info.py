import struct
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

UDP_HOST = "192.168.1.1"
UDP_PORT = 5015
TIMEOUT_MS = 300

class Stm32DebugInfoLine(object):
	def __init__(self, time, message, value=None, file=None, line=None):
		self.time    = time
		self.message = message
		self.value   = value
		self.file    = file
		self.line   = line
	def has_value(self):
		return not self.value is None
	def __str__(self):
		if self.has_value():
			return "%2.6f %s %s"%(self.time, self.message, self.value)
		return "%2.6f %s"%(self.time, self.message)

class Stm32DebugInfoContext(object):
	#pure virtual
	def AppendDebugInfoLine(self, line):
		raise NotImplementedError

class Stm32DebugInfoDatagramProtocolBase():
	def __init__(self, context):
		self.context = context
		self.seq = 0
		self.retry = None
		self.next_from = 0
		self.debug_infos = []
		self.known_msg_ids_map = {}
		self.unknown_msg_ids = []
		self.TS = 48000000.0

	def _push_known_messages(self):
		ids_map = self.known_msg_ids_map
		n = 0
		l = len(self.debug_infos)
		try:
			while n < l:
				time, msg_id, value, file_id, line = (list(self.debug_infos[n]) + [None, None])[:5]
				msg_text = ids_map[msg_id]
				if not msg_text.endswith(self.VALUE_INDICATOR): value = None
				debug_info_line = Stm32DebugInfoLine(
							time    = time / self.TS,
							message = msg_text,
							value   = value,
							file    = (ids_map[file_id] if file_id else None),
							line    = line
							)
				self.context.AppendDebugInfoLine(debug_info_line)
				n = n + 1
		except KeyError, err:
			msg_id = self.debug_infos[n][1]
			if msg_id not in ids_map:
				self.unknown_msg_ids.append(msg_id)
			else:
				file_id = self.debug_infos[n][3]
				if file_id not in ids_map:
					self.unknown_msg_ids.append(file_id)
				else:
					raise err
		if n:
			del self.debug_infos[0:n]
	def has_unknown_ids(self):
		return len(self.unknown_msg_ids) > 0

	QUERY_HEADER_FORMAT = "!HH"  # seq, type
	QUERY_DEBUG_INFO_FORMAT = "!H" # from_index
	QUERY_MSG_TEXT_FORMAT = "!L"  # msg_id
	QUERY_TYPE_DEBUG_INFO = 1
	QUERY_TYPE_MSG_TEXT = 2
	RESPONCE_TYPE_DEBUG_INFO = QUERY_TYPE_DEBUG_INFO 
	RESPONCE_TYPE_MSG_TEXT = QUERY_TYPE_MSG_TEXT 
	RESPONCE_HEADER_FORMAT="!HH" # seq, type
	RESPONCE_DEBUG_INFO_HEADER_FORMAT="!HHH" #count, item_size, next_index
	RESPONCE_DEBUG_INFO_DATA_FORMAT="<LLl" #time, msg_id, value
	RESPONCE_DEBUG_INFO_DATA_SIZE=struct.calcsize(RESPONCE_DEBUG_INFO_DATA_FORMAT)
	RESPONCE_DEBUG_INFO_DATAEX_FORMAT="<LLlLL" #time, msg_id, value, file_id, line
	RESPONCE_DEBUG_INFO_DATAEX_SIZE=struct.calcsize(RESPONCE_DEBUG_INFO_DATAEX_FORMAT)
	RESPONCE_MSG_TEXT_HEADER_FORMAT="!LH" #msg_id, msg_len, 
	RESPONCE_MSG_TEXT_DATA_FORMAT="!%ds" #msg_chars...

	VALUE_INDICATOR = ':'
	VALUE_FORMATS = {
		'%': (lambda v: float(v)/100),
		}
	VALUE_DEFAULT_FORMAT = int

	def ComposeUdpPacket_NextDebugInfo(self):
		self.seq = (self.seq + 1) & 0xFFFF
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_DEBUG_INFO) \
				 + struct.pack(self.QUERY_DEBUG_INFO_FORMAT, self.next_from)
		return pkt_data
	def ComposeUdpPacket_MessageText(self):
		self.seq = (self.seq + 1) & 0xFFFF
		msg_id = self.unknown_msg_ids[0]
		pkt_data = struct.pack(self.QUERY_HEADER_FORMAT, self.seq, self.QUERY_TYPE_MSG_TEXT) \
				 + struct.pack(self.QUERY_MSG_TEXT_FORMAT, msg_id)
		return pkt_data
	def ComposeUdpPacket_AUTO(self):
		if self.unknown_msg_ids:
			return self.ComposeUdpPacket_MessageText()
		return self.ComposeUdpPacket_NextDebugInfo()
	def ParsePktData(self, format_string):
		size = struct.calcsize(format_string)
		if len(self.udp_data) - self.udp_data_start < size:
			raise "Invalid responce" 
		res = struct.unpack_from(format_string, self.udp_data, self.udp_data_start)
		self.udp_data_start = self.udp_data_start + size
		return res
	def ParseUdpPacket_DebugInfo_Data(self, records_count, record_size):
		for n in xrange(records_count):
			self.debug_infos.append(self.ParsePktData(self.RESPONCE_DEBUG_INFO_DATA_FORMAT))
	def ParseUdpPacket_DebugInfo_DataEx(self, records_count, record_size):
		for n in xrange(records_count):
			self.debug_infos.append(self.ParsePktData(self.RESPONCE_DEBUG_INFO_DATAEX_FORMAT))
	def ParseUdpPacket_DebugInfo_InvalidSize(self, records_count, record_size):
		raise StandardError("invalid record size %d not in [%d,%d]."%(record_size, self.RESPONCE_DEBUG_INFO_DATA_SIZE, self.RESPONCE_DEBUG_INFO_DATAEX_SIZE))

	ResponceDebugInfoSizeParsers = {
				RESPONCE_DEBUG_INFO_DATA_SIZE:    ParseUdpPacket_DebugInfo_Data,
				RESPONCE_DEBUG_INFO_DATAEX_SIZE:  ParseUdpPacket_DebugInfo_DataEx,
			}
	ResponceDebugInfoSizeParserInvalid = ParseUdpPacket_DebugInfo_InvalidSize
	def ParseUdpPacket_DebugInfo(self):
		(records_count, record_size, next_from) = self.ParsePktData(self.RESPONCE_DEBUG_INFO_HEADER_FORMAT)
		parser = self.ResponceDebugInfoSizeParsers.get(record_size, self.ResponceDebugInfoSizeParserInvalid)
		parser(self, records_count, record_size)
		self.next_from = next_from
	def ParseUdpPacket_MsgText(self):
		(msg_id, msg_length) = self.ParsePktData(self.RESPONCE_MSG_TEXT_HEADER_FORMAT)
		(msg_text, ) = self.ParsePktData(self.RESPONCE_MSG_TEXT_DATA_FORMAT % msg_length)
		self.known_msg_ids_map[msg_id] = msg_text
		self.unknown_msg_ids.remove(msg_id)
	def ParseUdpPacket_InvalidType(self):
		raise StandardError("invalid respoce type")

	ResponceTypeParsers = {
				RESPONCE_TYPE_DEBUG_INFO:     ParseUdpPacket_DebugInfo,
				RESPONCE_TYPE_MSG_TEXT:       ParseUdpPacket_MsgText,
			}
	ResponceInvalidTypeParser = ParseUdpPacket_InvalidType
	def ParseUdpPacket(self, data):
		self.udp_data = data
		self.udp_data_start = 0
		(seq, responce_type) = self.ParsePktData(self.RESPONCE_HEADER_FORMAT)
		if seq == self.seq:
			parser = self.ResponceTypeParsers.get(responce_type, self.ResponceInvalidTypeParser)
			parser(self)
			return True
		return False

"""
	class Stm32DebugInfoDatagramProtocolTwisted:
	
	protocol implementation for twisted reactor library
"""
class Stm32DebugInfoDatagramProtocolTwisted(Stm32DebugInfoDatagramProtocolBase, DatagramProtocol):
	def __init__(self, udp_host, udp_port, timeout, context):
		Stm32DebugInfoDatagramProtocolBase.__init__(self, context)
		self.udp_host = udp_host 
		self.udp_port = udp_port 
		self.timeout  = timeout
		self.rx_sock = None
		self.retries_count = 0
		self.done_callback, self.done_callback_args = None, {}
		self.retry_callback, self.retry_callback_args = None, {}
		self._install_listener()
		print "proto init", self.udp_host, ':', self.udp_port, self.timeout * 1000, 'ms'
	def SetDoneCallback(self, callback, **args):
		self.done_callback, self.done_callback_args = callback, args
	def SetRetryCallback(self, callback, **args):
		self.retry_callback, self.retry_callback_args = callback, args
	def Start(self):
		self.sendRequestDatagram()
	def Continue(self, force_send = False):
		self.retries_count = 0
		if force_send or self.has_unknown_ids():
			self.sendRequestDatagram()
		else:
			self._init_retry(self.Continue, True)
	def Stop(self): 
		print "protocol stop"
		self._uninstall_listener()
		if self.retry:
			self.retry.cancel()
			self.retry = None
	def startProtocol(self):
		self.transport.connect(self.udp_host, self.udp_port)
		print "proto connect"
	def datagramReceived(self, datagram, host):
# 		print 'v'
		if self.ParseUdpPacket(datagram):
			self.retry.cancel()
			self.retry = None
			if self.done_callback: self.done_callback(**self.done_callback_args)
			self._push_known_messages()
			self.Continue()
	def sendRequestDatagram(self):
# 		print 'n',
		if self.rx_sock:
			datagram = self.ComposeUdpPacket_AUTO()
			self.transport.write(datagram)
		self._init_retry(self.sendRequestDatagram)
	def _install_listener(self):
		if not self.rx_sock:
			try:
				self.rx_sock = reactor.listenUDP(self.udp_port + 1, self) # Catch Responces to  datagramReceived
				print "rx_sock installed"
			except Exception, exc:
				print "rx_sock faulted", exc
				pass 
	def _uninstall_listener(self):
		if self.rx_sock:
			try:
				self.rx_sock.stopListening()
				print "rx_sock uninstalled"
			except Exception, exc:
				print "rx_sock faulted", exc
				pass 
			self.rx_sock = None
	def _init_retry(self, fn, *args):
		self._install_listener()
		if self.timeout:
			self.retry = reactor.callLater(self.timeout, fn, *args)
			if self.retries_count and self.retry_callback: self.retry_callback(**self.retry_callback_args)
			self.retries_count = self.retries_count + 1

class Stm32DebugInfoLoader (Stm32DebugInfoContext):
	def __init__(self, host, port, timeout):
		self.protocol = Stm32DebugInfoDatagramProtocolTwisted(host, port, timeout, self)
		self.protocol.SetDoneCallback(self.AnimateOk)
		self.protocol.SetRetryCallback(self.AnimateErr)
		self.animation_ok = 0
		self.animation_err = 0

	def AppendDebugInfoLine(self, line):
		print str(line)
		self.AnimateOk()

	def Stop(self):
		self.protocol.Stop()
	
	def AnimateOk(self):
		animation = ["[=---]", "[-=--]", "[--=-]", "[---=]", "[---#]", "[--#-]", "[-#--]", "[#---]"]
		self.animation_ok = (self.animation_ok + 1)%len(animation)
		print "%s\r"%animation[self.animation_ok],
	def AnimateErr(self):
		animation = ["[=x  ]", "[-X  ]"]
		self.animation_err = (self.animation_err + 1)%len(animation)
		print "%s\r"%animation[self.animation_err],
		
	def main(self):
		print "STM32 UDP debug info %s:%d" % (self.protocol.udp_host, self.protocol.udp_port)
		try:
			self.protocol.Start()
			reactor.run()
		except KeyboardInterrupt:
			print "              \nExit"


def parse_args():
	from argparse import ArgumentParser
	parser = ArgumentParser(description='STM32 UDP params manager.')
	parser.add_argument("-a", "--host", "--address", help="target host ip name or address",
					default = UDP_HOST)
	parser.add_argument("-p", "--port", help="target ip port (UDP)",
					type = int, default = UDP_PORT)
	parser.add_argument("-t", "--timeout", help="ip communication timeout (ms)",
					type = int, default = TIMEOUT_MS)

	args = parser.parse_args()
	args.timeout /= 1000.0 # ms to s

	return args

if __name__ == "__main__":
	args = parse_args()
	a = Stm32DebugInfoLoader(args.host, args.port, args.timeout)
	a.main()
		
