from time import sleep, time
from math import ceil, atan2, sqrt, pi
import struct
from traceback import print_exc
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.internet.task import LoopingCall


class UdpProtocolSample(object):
	def ComposeUdpPacket(self):
		return "txpacketdata"
	def ParseUdpPacket(self, data):
		return True

class UdpProtocolTwisted(DatagramProtocol):
	SEQ_FORMAT = "!H" # seq 
	SEQ_SIZE = struct.calcsize(SEQ_FORMAT)
	def __init__(self, udp_host, udp_port, period, timeout, on_data = None, on_error = None, on_restored = None, on_tx = None):
# 		print "proto init", period, timeout
		self.udp_host     = udp_host 
		self.udp_tx_port  = udp_port 
		self.udp_rx_port  = udp_port + 1 
		self.period       = period
		self.period_call  = None
		self.timeout      = timeout
		self.rx_timeout_call = None
		self.on_data      = on_data
		self.on_error     = on_error
		self.on_restored  = on_restored
		self.on_tx        = on_tx
		self.connected    = False
		self.listener     = None 
		self.retry        = None
		self.tx_call    = None
		self.seq = 0
		self.tx_times = []
	def __del__(self):
		self._uninstall_listener()
	def _install_listener(self):
		try:
			self.listener = reactor.listenUDP(self.udp_rx_port, self)
			# Catch Responces to  datagramReceived
# 			print "listener installed", self.udp_rx_port
			return True
		except Exception, exc:
			print "listener faulted", exc
			self._process_error()
			return False
	def _uninstall_listener(self):
		try:
			if self.listener:
				self.listener.stopListening()
# 				print "listener uninstalled"
		except Exception, exc:
			print "listener faulted", exc
			pass 
		self.listener = None
	def _do_tx(self):
		try:
			if self.transport:
				self.seq = (self.seq + 1) & 0xFFFF
				self.tx_times.append((self.seq, time()))
				self.tx_times = self.tx_times[:50]
# 				print "\nTX: %d!"%self.seq, self.tx_times
				datagram = struct.pack(self.SEQ_FORMAT, self.seq) + self.ComposeUdpPacket()
				self.transport.write(datagram)
				if self.on_tx: self.on_tx(self.seq)
# 				print "txed", len(datagram)
		except Exception, exc:
			print "tx faulted", exc
			self._process_error()
	def _start_tx_loop(self):
		self.tx_call = LoopingCall(self._do_tx)
		self.tx_call.start(self.period)
	def _stop_tx_loop(self):
		if self.tx_call is not None:
			self.tx_call.stop()
			self.tx_call = None
	def _restart_rx_timeout(self):
		self._cancel_rx_timeout()
		if self.timeout:
			self.rx_timeout_call = reactor.callLater(self.timeout, self._process_rx_timeout)
	def _process_rx_timeout(self):
		self.rx_timeout_call = None
		if self.OnConnectionTimeout: self.OnConnectionTimeout()
	def _cancel_rx_timeout(self):
		if self.rx_timeout_call is not None:
			self.rx_timeout_call.cancel()
			self.rx_timeout_call = None
	def _process_error(self):
		if self.connected:
			self.connected = False
			if self.on_error: self.on_error()
			self.OnConnectionError()
	def startProtocol(self):
		self.transport.connect(self.udp_host, self.udp_tx_port)
# 		print "proto started", self.period
	def datagramReceived(self, datagram, host):
# 		print "rcvd:", len(datagram)
		rx_seq, = struct.unpack(self.SEQ_FORMAT, datagram[:self.SEQ_SIZE])
# 		print "\nRX: len=%d, seq=%d: "%(len(datagram), rx_seq), self.tx_times
		while self.tx_times:
			tx_seq, tx_time = self.tx_times[0]
			if tx_seq == rx_seq:
# 				print " %d == %d"%(tx_seq, rx_seq)
				self.tx_times.pop(0)
				now_time = time()
				loop_time = now_time - tx_time
				if self.ParseUdpPacket(datagram[self.SEQ_SIZE:], loop_time):
					self._restart_rx_timeout()
					if not self.connected:
						self.connected = True
						if self.on_restored: self.on_restored()
					if self.on_data: self.on_data()
			elif tx_seq < rx_seq:
# 				print " %d < %d"%(tx_seq, rx_seq)
				self.tx_times.pop(0)
			else:
# 				print " %d > %d"%(tx_seq, rx_seq)
				break
# 		print "Error"
		return False
	def connectionRefused(self):
		print "twisted connectionRefused"
		self._uninstall_listener()
		self._process_error()
	def Start(self): 
# 		print "protocol start"
		if self._install_listener():
			self._start_tx_loop()
	def Stop(self): 
# 		print "protocol stop"
		self._stop_tx_loop()
		self._cancel_rx_timeout()
		self._uninstall_listener()

class CommunicationAnimator():
	animation_seq_ok = ["[=---]", "[>---]", "[->--]", "[-->-]", "[--->]", "[---=]", "[---<]", "[--<-]", "[-<--]", "[<---]"]
	animation_seq_err = ["[ X  ]", "[-X  ]", "[~X  ]", "[=X  ]"]
	def __init__(self, default_speed = 1):
		self._animation_speed = default_speed
		self._n_err = 0
		self._n_err = 0
	def AnimateOk(self, animation_speed = None):
		if animation_speed is None: animation_speed = self._animation_speed
		seq = self.animation_seq_ok
		self._n_err = (self._n_err + animation_speed)%len(seq)
		print "%s"%seq[int(self._n_err)],
	def AnimateErr(self, animation_speed = None):
		if animation_speed is None: animation_speed = self._animation_speed
		seq = self.animation_seq_err
		self._n_err = (self._n_err + animation_speed)%len(seq)
		print "%s"%seq[int(self._n_err)],

		
