from time import sleep, time
from math import ceil, atan2, sqrt, pi
import struct
from udpcomm import UdpProtocolTwisted, CommunicationAnimator
from twisted.internet.task import LoopingCall
from twisted.internet import reactor

from traceback import print_exc

UDP_HOST = "192.168.1.1"
UDP_PORT = 5005
PERIOD_MS = 100
TIMEOUT_MS = 1000

"""
	class ControlData:
	
	representation of control values
"""
class ControlData(object):
	def __init__(self):
		self.throttle = 0.0
		self.steering = 0.0
		self.rotation = 0.0
		self.elevation = 0.0
		self.zoom = 0.0
		self.buttons = [0] * 16
"""
	class StateData:
	
	representation of unit current state values
"""
class StateData(object):
	def __init__(self):
		self.connected = False
		self.throttle = 0.0
		self.steering = 0.0
		self.rotation = 0.0
		self.elevation = 0.0
		self.zoom = 0.0
		self.flag1 = 0
		self.flag2 = 0

"""
	interface ControlContext:
	
	access methods to set of control values.
"""
class ControlContext(object):
	#pure virtual
	def GetControlData(self):
		raise NotImplementedError

"""
	interface StateContext:
	
	access methods to report actual unit state values.
"""
class StateContext(object):
	#pure virtual
	def SetActualState(self, state_data, loop_time):
		raise NotImplementedError

"""
	class ControlDatagramProtocolBase:
	
	base constatnts and UDP packet composer/parser.
	
	ControlDatagramProtocolBase(context)
	
	context - reference to ControlContext implementing instance 
"""
class ControlDatagramProtocolBase(object):
	COMMAND_FORMAT = "!10h 8b" # sent_seq, thr, steer, rot, elev, zoom, res1-5, flags[8] 
	RESPONCE_FORMAT = "!5h 2b" # sent_seq, thr, steer, rot, elev, flags[2] 
	def __init__(self, control_context, state_context):
		self.control_context = control_context
		self.state_context = state_context
		self.retry = None
	def ComposeUdpPacket(self):
		control_data = self.control_context.GetControlData()
		return struct.pack(self.COMMAND_FORMAT,
				control_data.throttle * 32767, control_data.steering * 32767,
				control_data.rotation * 32767, control_data.elevation * 32767,
				control_data.zoom * 32767,
				0, 0, 0, 0, 0,
				sum( [ control_data.buttons[n] << n for n in xrange(0, min(8, len(control_data.buttons))) ] ), # flags 1
				0, 0, 0, 0, 0, 0, 0 #flags 2-8
		)
	def ParseUdpPacket(self, data, loop_time):
		state = StateData()
		(throttle, steering, rotation, elevation, zoom, flag1, flag2) = struct.unpack(self.RESPONCE_FORMAT, data)
		state.connected = True
		state.throttle = throttle / 32767.0
		state.steering = steering / 32767.0
		state.rotation = rotation / 32767.0
		state.elevation = elevation / 32767.0
		state.zoom = zoom / 32767.0
		self.state_context.SetActualState(state, loop_time)
# 				print "rx seq", rx_seq, "t", loop_time, now_time, tx_time
# 				print "  OK"
		return True
	def OnConnectionTimeout(self):
		state = StateData()
		state.connected = False
		self.state_context.SetActualState(state, self.timeout)
	def OnConnectionError(self):
		state = StateData()
		state.connected = False
		self.state_context.SetActualState(state, self.timeout)

"""
	class Stm32ParamsDatagramProtocolTwisted:
	
	protocol implementation for twisted reactor library
"""
class ControlDatagramProtocolTwisted(UdpProtocolTwisted, ControlDatagramProtocolBase):
	def __init__(self, udp_host, udp_port, period, timeout, control_context, state_context, on_data = None, on_error = None, on_restored = None, on_tx = None):
# 		print "proto init", period, timeout
		UdpProtocolTwisted.__init__(self, udp_host, udp_port, period, timeout, on_data = on_data, on_error = on_error, on_restored = on_restored, on_tx = on_tx)
		ControlDatagramProtocolBase.__init__(self, control_context, state_context)

class AntibugUtitCommunicator(ControlContext, StateContext):
	def __init__(self, host, port, period, timeout, on_data = None, on_error = None, on_restored = None, on_tx = None):
		super(AntibugUtitCommunicator, self).__init__()
		self.control_data = ControlData()
		self.player_state = StateData()
		self.avg_loop_time = 0.0
		self.timeout = timeout
		self.protocol = ControlDatagramProtocolTwisted(host, port, period, timeout, self, self, on_data = on_data, on_error = on_error, on_restored = on_restored, on_tx = on_tx)
		self.reset_call = None
		self.OnStateChanged = None 
		self.OnConnectionLost = None 
		self.OnConnectionEstablished = None
	#virtual ControlContext implementations
	def GetControlData(self):
		return self.control_data
	#virtual StateContext implementations
	def SetActualState(self, state, loop_time):
		self.avg_loop_time = self.avg_loop_time + 0.3*(loop_time - self.avg_loop_time)
		if not self.player_state == state:
			connection_changed = not(self.player_state.connected == state.connected)
			self.player_state = state
			if connection_changed:
				if state.connected:
					if self.OnConnectionEstablished: self.OnConnectionEstablished()
				else: 
					if self.OnConnectionLost: self.OnConnectionLost()
			if self.OnStateChanged: self.OnStateChanged()
		if not state.connected and not self.reset_call:
			self.reset_call = reactor.callLater(self.timeout, self.Reset) 
		if state.connected and self.reset_call:
			self.reset_call.cancel()
			self.reset_call = None
	def Reset(self):
		self.reset_call = None
		self.protocol.Stop()
		self.protocol.Start()
	def Start(self):
		self.protocol.Start()
	def Stop(self):
		self.protocol.Stop()

class CommandLineKeyReader(object):
	def __init__(self):
		try:
			import termios
			# POSIX system. Create and return a getch that manipulates the tty.
			import sys, tty
			def _getch():
				fd = sys.stdin.fileno()
				old_settings = termios.tcgetattr(fd)
				try:
					tty.setraw(fd)
					ch = sys.stdin.read(1)
				finally:
					termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
				return ch
			self.getch = _getch
		except ImportError:
			# Non-POSIX. Return msvcrt's (Windows') getch.
			import msvcrt
			def _getch():
				if msvcrt.kbhit():
					ch = msvcrt.getch()
					code = ord(ch)
					if code == 0x00:
						code = ord(msvcrt.getch()) # read second byte for function key
						return {0x3B:'F1', 0x54:'ShiftF1', 0x5E:'CtrlF1', 0x68:'AltF1', 0x6B:'AltF4'}.get(code, '00.%02x'%code)
					elif code == 0xE0:
						code = ord(msvcrt.getch()) # read second byte for function key
						return {0x49:'PgUp', 0x51:'PgDn', 0x47:'Home', 0x48:'Up', 0x4B:'Left', 0x4D:'Right', 0x4F:'End', 0x50:'Down', 0x52:'Ins', 0x53:'Del'}.get(code, 'E0.%02x'%code)
					return {27:'Esc', 13:'Enter', 8:'Backspace', 9:'Tab'}.get(code, ch)
				else:
					return ''
			self.getch = _getch
	def __call__(self):
		return self.getch()

class AntibugUtitControlCommandLineApp():
	keys_to_buttons_remap = {'`':0, '~':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, '0':10, '-':11, '_':11, '=':12, '+':12}
	def __init__(self, host, port, period, timeout):
		self.key_reader = CommandLineKeyReader()
		self.comm = comm = AntibugUtitCommunicator(host, port, period, timeout, on_data = self.print_state)
		self.animator = CommunicationAnimator(0.5)
		comm.OnStateChanged = self.OnStateChanged 
		comm.OnConnectionLost = self.OnConnectionLost 
		comm.OnConnectionEstablished = self.OnConnectionEstablished
		print "AntibugUtit UDP controller %s:%d" % (host, port)
		print "Use WASD or arrows keys to control movement, SPACE for stop, Enter for center"
	def run(self):
		self.comm.Start()  
		self.print_state()
		LoopingCall(self.tick).start(1.0 / 10)
# 		LoopingCall(self.print_state).start(1.0 / 4)
		reactor.run()
	def tick(self):
		key = self.key_reader().lower()
		if key:
			control_data = self.comm.control_data
			if key in ('w', 'up'): 
				control_data.throttle = min(control_data.throttle + 0.1,  1.0)
			elif key in ('s', 'down'):
				control_data.throttle = max(control_data.throttle - 0.1, -1.0)
			elif key in ('a', 'left'): 
				control_data.steering = max(control_data.steering - 0.2, -1.0)
			elif key in ('d', 'right'): 
				control_data.steering = min(control_data.steering + 0.2,  1.0)
			elif key == ' ': #space
				control_data.throttle = 0.0
				control_data.steering = 0.0
			elif key == 'enter': 
				control_data.steering = 0.0
			elif key in self.keys_to_buttons_remap.keys():
				n = self.keys_to_buttons_remap[key]
				control_data.buttons[n] = not control_data.buttons[n]
			elif key == 'esc' or key == 'altf4': 
				reactor.stop()
			else: print 'key:', key, 'ignored' 
			self.comm.control_data = control_data
	def OnStateChanged(self):
		self.print_state(0)
	def OnConnectionLost(self):
		self.print_state(0)
	def OnConnectionEstablished(self):
		self.print_state(0)
	def print_state(self, animation_speed=None):
			control_data = self.comm.control_data
			print "\rCtrl: %+4d%% %+4d%% %s" % (int(control_data.throttle*100.0), int(control_data.steering*100.0), "".join((".","X")[x] for x in control_data.buttons)),
			actual_state = self.comm.player_state
			state_text = " time:%4dms Unit: %+4d%% %+4d%% " % (self.comm.avg_loop_time*1000, int(actual_state.throttle*100.0), int(actual_state.steering*100.0))  
			if actual_state.connected:
				print state_text,
				self.animator.AnimateOk(animation_speed)
			else:
				print "NO CONNECTION".center(len(state_text), '-'),
				self.animator.AnimateErr(animation_speed)
				

def parse_args():
	from argparse import ArgumentParser
	parser = ArgumentParser(description='STM32 UDP params manager.')
	parser.add_argument("-a", "--host", "--address", help="target host ip name or address",
					default = UDP_HOST)
	parser.add_argument("-p", "--port", help="target ip port (UDP)",
					type = int, default = UDP_PORT)
	parser.add_argument("-T", "--period", help="ip communication period (ms)",
					type = int, default = PERIOD_MS)
	parser.add_argument("-t", "--timeout", help="ip communication timeout (ms)",
					type = int, default = TIMEOUT_MS)
	args = parser.parse_args()
	args.period /= 1000.0 # ms to s 
	args.timeout /= 1000.0 # ms to s
	return args 

def run(args):
	a = AntibugUtitControlCommandLineApp(args.host, args.port, args.period, args.timeout)
	a.run()

if __name__ == '__main__':
	args = parse_args()
	run(args)
