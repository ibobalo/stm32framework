#!/usr/bin/env python
import sys, os
from optparse import OptionParser

parser = OptionParser(usage="%prog [options] binary_file")
parser.add_option("-n", "--count", "--size", type="int", dest="size",
    help="print specified bytes count", default=0)
parser.add_option("-s", "--separator", dest="separator",
    help="separator string", default="")
(options, args) = parser.parse_args()

if not args:
	parser.error("filename required")

n = 0
with open(args[0], 'rb') as f:
	while True:
		b = f.read(1024)
		l = len(b)
		if not l: break
		if options.size:
			if n >= options.size: break
			if n + l > options.size:
				b = b[:options.size - n]
		if isinstance(b, str): b = [ord(c) for c in b]
		if n:
			sys.stdout.write(options.separator)
		sys.stdout.write(options.separator.join(["%02X"%c for c in b]))
		n = n + l
