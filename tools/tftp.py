import os.path
from time import sleep
import pypi_import

with pypi_import.pypi_import():
	import tftpy

TFTP_IP = '192.168.1.1'
TFTP_PORT = 69

args = None
def parse_args():			
	from argparse import ArgumentParser
	parser = ArgumentParser(description='TFTP Firmware uploader (client or server). Primary for uploading openwrt firmware')
	parser.add_argument("-a", "--host", "--address", help="target host ip name or address")
	parser.add_argument("-p", "--port", help="target ip port",
					type = int, default = TFTP_PORT)
	parser.add_argument("-t", "--timeout", help="communication timeout",
					type = int, default = 10)
	parser.add_argument("firmware_file", help="filename of firmware update file to upload")
	return parser.parse_args()

def server_open(name):
	print "opening", name
	if name == "root_uImage":
		return open(args.firmware_file, 'rb')
	return None

def main():
	global args
	args = parse_args()
	if args.host:
		client = tftpy.TftpClient(args.host, args.port)
		client.upload('root_uImage', args.firmware_file, timeout=args.timeout)
	else:
		import socket
		print "Starting TFTP server", [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")], ":", args.port
		server = tftpy.TftpServer(os.path.basename(args.firmware_file), server_open)
		server.listen('0.0.0.0', args.port)

if __name__ == '__main__':
	main();
